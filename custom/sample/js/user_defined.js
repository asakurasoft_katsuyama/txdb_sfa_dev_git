// JavaScriptカスタマイズサンプル
//
// 想定するDB構造
// DB1: test table
//     文字列1行フィールド(キー:文字列)
//     数値フィールド(キー:数値)
//     数値フィールド(キー:変数)
//     計算フィールド(キー:計算、計算式に「数値+変数」が設定されている)
//     文字列1行フィールド(キー:コピー先)
//     空白フィールド(キー:空白)
//     日付フィールド(キー:日付)
//     注意書きフィールド(キー:注意書き)
//     選択肢フィールド(キー:ラジオボタン)
//         選択肢1, 選択肢2 を持つ
//     プルダウンフィールド(キー:ドロップダウン)
//         選択肢1, 選択肢2 を持つ
//     参照コピーフィールド(キー:参照コピー)
//         参照コピー設定
//             DB1 の「参照コピー」からDB2の「コード」を参照する
//             DB1 の「コピー先」にDB2の「名前」をコピーする
// DB2: 参照コピーの参照先
//     文字列1行フィールド(キー:コード)
//     文字列1行フィールド(キー:名前)
//
// 想定するデータ
// DB2:
//    コード A001 のデータが存在する
//    コードに重複データが存在しない

(function(){
    // 環境変数ここから. 環境に合わせて変更してください。
    var testTableId = 1556; // 対象テーブルid(DB id)
    // 環境変数ここまで.

    // データ参照画面のロードイベントハンドラ
    function handleLoadView(context) {
        var row = context.row;
        var $ = context.$;

        if (row.getTableId() != testTableId) {
            return; // 対象テーブルでなければ何もしない
        }
        console.log(row);
        console.log(row.getNo()); // result: non-null

        // フィールドタイプの取得
        console.log(row.getField('文字列').getType());
        console.log(row.getField('数値').getType());
        console.log(row.getField('計算').getType());
        console.log(row.getField('ラジオボタン').getType());
        console.log(row.getField('ドロップダウン').getType());
        console.log(row.getField('日付').getType());
        console.log(row.getField('参照コピー').getType());

        // フィールド値の取得
        console.log(row.getField('文字列').getValue()); // result: 123
        console.log(row.getField('数値').getValue()); // result: 123
        console.log(row.getField('計算').getValue()); // result: 123
        console.log(row.getField('ラジオボタン').getValue()); // result: { name: 'xxx' } or null
        console.log(row.getField('ドロップダウン').getValue()); // result: { name: 'xxx' } or null
        console.log(row.getField('日付').getValue()); // result: 2015-09-05
        console.log(row.getField('参照コピー').getValue()); // result: { value: '' }

        // データヘッダーへのボタン追加
        var headerMenuSpace = $(row.getHeaderMenuSpaceElement());
        var button1 = $('<button>ボタン1</button>');
        headerMenuSpace.append(button1);
    }

    // データ更新or登録画面のロードイベントハンドラ
    function handleLoadUpdate(context) {
        var row = context.row;
        var $ = context.$;

        if (row.getTableId() != testTableId) {
            return; // 対象テーブルでなければ何もしない
        }
        console.log(row);
        console.log(row.getNo()); // result: null on create, non-null on update
        console.log(row.isCopy()); // result: true on copy from a row/form

        // フィールドタイプの取得
        console.log(row.getField('文字列').getType());
        console.log(row.getField('数値').getType());
        console.log(row.getField('計算').getType());
        console.log(row.getField('ラジオボタン').getType());
        console.log(row.getField('ドロップダウン').getType());
        console.log(row.getField('日付').getType());
        console.log(row.getField('参照コピー').getType());

        // フィールド値の書き換え
        row.getField('数値').setValue(2);
        row.getField('変数').setValue(5);
        try {
            row.getField('計算').setValue(6);
        } catch (err) {
            console.log(err);
        }
        row.getField('ラジオボタン').setValue({name:'選択肢2'});
        row.getField('ドロップダウン').setValue({name:'選択肢2'});
        console.log(row.getField('ドロップダウン').getValue()); // result: { name: '選択肢2' }
        row.getField('ドロップダウン').setValue(null); // フィールドを空にする場合
        console.log(row.getField('ドロップダウン').getValue()); // result: null
        row.getField('日付').setValue('2015-09-05');
        row.getField('参照コピー').setValue({value:'A001'});
        //row.getField('参照コピー').lookup(); // ルックアップ実行
        //row.getField('参照コピー').clear(); // ルックアップクリア
        //row.calculate(); //再計算
        // ルックアップ実行
        row.getField('参照コピー').lookup({success: function(){
            console.log('lookup success!');
            // ルックアップが成功した場合のみ再計算
            row.calculate({success: function(){
                console.log('calculation success!');
                console.log(row.getField('計算').getValue());
            }});
        }});

        // データヘッダーへのボタン追加
        var headerMenuSpace = $(row.getHeaderMenuSpaceElement());
        if (headerMenuSpace.children().length == 0) {
            var button1 = $('<button>ボタン1</button>');
            headerMenuSpace.append(button1);
        } else {
            console.log('headerMenuSpace already initialized');
        }

        // 空白フィールドへのボタン追加
        var space = $(row.getSpaceFieldElement('空白'));
        console.log(space);
        var button2 = $('<button>ボタン2</button>');
        space.append(button2);

        // 注意書きフィールドへのボタン追加
        var label = $(row.getLabelFieldElement('注意書き'));
        console.log(label);
        var button3 = $('<button>ボタン3</button>');
        label.append(button3);
    }


    //データ参照画面のロードイベントハンドラの登録
    txdb.on('load.view.row.table', function(context) {
        console.log('handle load.view.row.table')
        handleLoadView(context);
    });

    //データ更新画面のロードイベントハンドラの登録
    txdb.on('load.update.row.table', function(context) {
        console.log('handle load.update.row.table')
        handleLoadUpdate(context);
    });

    //データ登録画面のロードイベントハンドラの登録
    txdb.on('load.create.row.table', function(context) {
        console.log('handle load.create.row.table')
        handleLoadUpdate(context);
    });
})();
