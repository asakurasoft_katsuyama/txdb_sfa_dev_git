<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
{include file="include/head.tpl" title="データ表示" features=['lightbox', 'select2']}
<link rel="stylesheet" type="text/css" href="/css/lightbox/ekko-lightbox.min.css">
<link rel="stylesheet" type="text/css" href="/css/page/data_index.css?{get_ver_query()|escape}">
<link rel="stylesheet" type="text/css" href="/css/page/data_view.css?{get_ver_query()|escape}">
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="home"}
</div>

<div id="content" class="db-list_content" >

	{if !$table->isChild()}
		{** 通知 **********}
		<nav class="page-nav clearfix affix-bar" data-spy="affix" data-offset-top="100" data-bind="visible: receipt() || readState()" style="display: none;">

			<!-- ko if: readState() -->
			<div class="notification">
				<div class="row">
					<form class="form-inline">
						<div class="notification-btns col-xs-12">
							<!-- ko if: receipt() -->
							<div class="notification-btn notification-btn-back">
								<a data-bind="click: openNotifications"><i class="notification-btn-icon fa fa-arrow-left"></i> <span class="notification-btn-text">通知一覧へ戻る</span></a>
							</div>
							<!-- /ko -->

							<!-- ko ifnot: readState().readOn -->
								<div class="notification-btn notification-btn-read">
									<a data-bind="click: $root.toggleRead.bind($root, true)"><i class="notification-btn-icon fa fa-check"></i> <span class="notification-btn-text">閲覧済みにする</span></a>
								</div>

								<!-- ko if: readCheckWithComment -->
								<div class="notification-btn notification-btn-comment">
									<input type="text" class="form-control" data-bind="value: readCheckComment">
								</div>
								<!-- /ko -->

								<div class="notification-btn notification-btn-with-comment">
									<div class="checkbox" style="display: inline-block">
										<label>
											<input type="checkbox" class="" data-bind="checked: readCheckWithComment">
											コメントを付ける
										</label>
									</div>
								</div>
							<!-- /ko -->
							<!-- ko if: readState().readOn -->
								{*<button data-bind="click: $root.toggleRead.bind($root, false)" class="btn btn-default pull-left">未読に戻す</button>*}
								<div class="notification-text notification-text-check">
									<span class="read-state"><i class="fa fa-check" ></i> 閲覧済み</span>
								</div>
								<div class="notification-text notification-text-comment">
								<!-- ko if : readState().comment == null -->
									<a href="#" data-bind="click: $root.openReadCommentEdit.bind($root, readState())">コメント追加</a>
								<!-- /ko -->
								<!-- ko if : readState().comment != null -->
									<span class="read-comment"><span data-bind="text: readState().comment"></span></span>
									<a href="#" data-bind="click: $root.openReadCommentEdit.bind($root, readState())">コメント編集</a>
								<!-- /ko -->
								</div>
							<!-- /ko -->
						</div>
					</form>
				</div>
			</div>
			<!-- /ko -->

			<!-- ko if: receipt() && readState() == null -->
			<div class="notification">
				<div class="row">
					<div class="notification-btns col-xs-4">
						<div class="notification-btn notification-btn-back">
							<a data-bind="click: openNotifications"><i class="notification-btn-icon fa fa-arrow-left"></i> <span class="notification-btn-text">通知一覧へ戻る</span></a>
						</div>
						<!-- ko ifnot: receipt().isRead -->
						<div class="notification-btn notification-btn-read">
							<a data-bind="click: $root.toggleNotificationRead.bind($root, receipt())"><i class="notification-btn-icon fa fa-check"></i> <span class="notification-btn-text">通知を既読にする</span></a>
						</div>
						<!-- /ko -->
						<!-- ko if: receipt().isRead -->
						<div class="notification-btn notification-btn-unread">
							<a data-bind="click: $root.toggleNotificationRead.bind($root, receipt())"><i class="notification-btn-icon fa fa-undo"></i> <span class="notification-btn-text">通知を未読に戻す</span></a>
						</div>
						<!-- /ko -->
					</div>
					<div class="notification-info col-xs-4">
						<span class="notification-time-wrapper">
							<span class="notification-time" data-bind="text: receipt().notification.notifiedOn "></span>
						</span>
						<span class="notification-notifier-wrapper">
						by
						<!-- ko if: receipt().notification.notifier -->
							<span class="notification-notifier" data-bind="text: receipt().notification.notifier.screenName "></span>
						<!-- /ko -->
						<!-- ko ifnot: receipt().notification.notifier -->
							<span class="notification-notifier notification-notifier-deleted">*削除されたユーザー*</span>
						<!-- /ko -->
						</span>
					</div>
					<div class="col-xs-4">
						<div class="notification-message">
							<span data-bind="text: receipt().notification.message "></span>
						</div>
					</div>
				</div>
			</div>
		<!-- /ko --><!-- receipt -->
		</nav>
		{** /通知 **********}
	{/if}

	<div class="container">
		{if get_config('app', 'features.breadcrumb_view', 1) }
		<ol class="breadcrumb">
			<li><a href="/my/">データベース入力</a></li>
			<li><a href="/my/data/?t={$table->getId()}">{$table->getName()|escape}</a></li>
		  	<li class="active">{$row->getValue($table->getRownum())|escape}</li>
		</ol>
		{/if}
		{if get_config('app', 'features.headding_view', 1) }
		<div class="database-title">
			<h3>{$table->getName()|escape}</h3>
		</div>
		{/if}
		<nav class="page-nav">
			{if $isWritable && !$table->isChild() }
			<a class="btn btn-primary" href="/my/data/edit?t={$table->getId()}&r={$row->getId()|escape}&offset={$offset|escape}">編集</a>
			<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalDelete">削除</button>
			<a class="btn btn-default save" href="/my/data/create?t={$table->getId()|escape}">新規作成</a>
			<a class="btn btn-default save" href="/my/data/replicate?t={$table->getId()}&r={$row->getId()|escape}">コピーして新規作成</a>
			{/if}
			<button class="btn btn-default" data-bind="disable: disabledPrev, click: openPrev"><i class="fa fa-backward"></i> 前のデータ</button>
			<button class="btn btn-default" data-bind="disable: disabledNext, click: openNext">次のデータ <i class="fa fa-forward"></i></button>
			<a class="btn btn-warning" href="/my/data/?t={$table->getId()}">{if $table->isCalendarEnabled()}カレンダー・{/if}一覧に戻る</a>
			<div id="row-header-menu-space" class="extension-point"></div>
		</nav><br class="clearfix" />

		<div class="panel panel-default data-view-panel">
			<div class="panel-body">
				<div class="form">
				<!--  ko foreach: fields -->
					<div class="row">
					<!--  ko foreach: $data -->
						<div data-bind="css2: $data.span, css2Prefix: 'col-sm-', attr:{ 'data-fieldcode': $data.field.code}">
							<!--  ko with: $data.field -->
								<!-- ko template: { name: function() { return 'view-'+type+'-tpl';}, afterRender: afterRender.bind($data) } -->
			  					<!-- /ko -->
							<!--  /ko -->
						</div>
					<!--  /ko -->
					</div>
				<!--  /ko -->
				</div>
			</div>
		</div>

		{if $table->isReadCheckEnabled() && !$table->isChild() }
			<div class="read-check" data-bind="with: readList">
				<h3>回覧板</h3>
				<div class="panel panel-default">
					<table class="table table-striped">
						<thead>
							<tr>
								<th class="col-state">閲覧済み</th>
								<th class="col-user">ユーザー</th>
								<th class="col-read-on">閲覧日</th>
								<th class="col-comment" colspan="2">コメント</th>
							</tr>
						</thead>
						<tbody data-bind="foreach: rows">
							<tr>
								<td class="col-state">
									<!-- ko if: readOn !== null -->
									<i class="fa fa-check" ></i>
									<!-- /ko -->
								</td>
								<td class="col-user">
									<!-- ko if: readUser -->
									<span data-bind="text: readUser.screenName"></span>
									<!-- /ko -->
									<!-- ko ifnot: readUser -->
									<span>*削除されたユーザー*</span>
									<!-- /ko -->
								</td>
								<td class="col-read-on" data-bind="text: ko.utils.formatDate(readOn, 'YYYY-MM-DD HH:mm')"></td>
								<td class="col-comment">
									<span class="pull-left" data-bind="text: comment"></span><br>
									{*<!-- ko if: canEdit -->
									<button class="btn btn-sm btn-primary pull-left" data-bind="click: $root.openReadCommentEdit.bind($root, $data)">編集</button>
									<!-- /ko -->*}
								</td>
							</tr>
						</tbody>
					</table>
					<!-- /ko -->
				</div>
				<ul class="pager">
					<li data-bind="css: { disabled: !hasPrev() } "><a href="#" data-bind=" click: prevList">前へ</a></li>
					<li data-bind="css: { disabled: !hasNext() } "><a href="#" data-bind=" click: nextList ">次へ</a></li>
				</ul>
			</div>
		{/if}

		{if $table->isCommentEnabled() && !$table->isChild() }
		<div class="comments">
			<h3>コメント</h3>
			<div class="panel panel-default">
				<div class="comment-form-wrapper">
					<!-- ko if: commentError -->
					<div class="alert alert-warning" data-bind="text :commentError"></div>
					<!-- /ko -->
					<form class="comment-form" data-bind="submit: postComment">
						<div class="form-group">
							<label>コメント</label>
							<textarea data-bind="value:commentText" class="form-control" rows="4"></textarea>
						</div>
						<div class="form-group">
							<label>通知先追加</label>
							<select class="" data-bind="foreach: userGroupOptions, optionsText: 'name', optionsValue: 'id', selectedOptions: notificationTargets, select2: true" multiple="true">
								<optgroup data-bind="attr: { label: label}, foreach: children">
									<option data-bind="text: name, value: id" ></option>
								</optgroup>
							</select>
						</div>
						<button type="submit" class="btn btn-primary">送信する</button>
					</form>
				</div><!-- comment-form-wrapper -->
				<div class="comment-list-wrapper" data-bind="visible: comments().length > 0">
					<!-- ko foreach: comments -->
					<div class="comment">
						<div class="comment-heading clearfix">
							<h4>
								<!-- ko if: postedUser -->
								<span data-bind="text: postedUser.screenName"></span>
								<!-- /ko -->
								<!-- ko ifnot: postedUser -->
								<span>*削除されたユーザー*</span>
								<!-- /ko -->
							</h4>
							<div data-bind="text: postedTime" class="posted-time"></div>
						</div>
						<hr>
						<div class="clearfix">
							<div class="comment-content">
								<div data-bind="foreach: notificationTargets " class="notification-targets">
									@<span data-bind="text: name"></span>

								</div>
								<p class="text" data-bind="text: text" style="white-space: pre-wrap"></p>
							</div>
							<!-- ko if: isDeletable -->
							<div class="nav">
								<button type="submit" class="btn btn-default" data-bind="click:$parent.deleteComment.bind($parent)">削除する</button>
							</div>
							<!-- /ko -->
						</div>
					</div>
				<!-- /ko -->
				</div><!-- comment-list-wrapper -->
			</div><!-- panel-comments -->
		</div>
		{/if}

	</div>
</div>
<div id="footer">
	{include file="include/footer.tpl"}
</div>

<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">データの削除</h4>
			</div>
			<div class="modal-body">
				このデータを削除します。よろしいですか？
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-bind="click: remove">削除する</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
			</div>
		</div>
	</div>
</div>

{include file="my/data/data_view_types.tpl"}
{include file="my/data/data_index_types.tpl"}
{include file="my/data/data_view_read_comment_dialog.tpl"}
{include file="include/js_bottom.tpl" features=['lightbox', 'select2', 'moment']}
<script type="text/javascript" src="/js/lightbox/ekko-lightbox.min.js"></script>
<script type="text/javascript" src="/js/view/base/list_model.js?{get_ver_query()|escape}"></script>
<script type="text/javascript" src="/js/view/data_view.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	viewModel = new ViewModel({$table->getId()|escape}, {$row->getId()|escape}, {$offset|escape}, {$notificationId|escape});
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
