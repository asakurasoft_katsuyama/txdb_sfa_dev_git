var TsukaeruCloudDBTerms = (function($){
	function confirm(name, expire, url, service){
		var dfd = $.Deferred();
		if (url === false) {
			dfd.resolve();
			return dfd.promise();
		}
		var cookieName =  name || 'tsukaeru_cloud_db_terms_agreement_v_1_0_0';
		var cookieExpire =  expire ? expire : 0 ;
		var terms = url || '//tsukaerudb.asakurasoft7.jp/db_terms.html' ;
		var serviceName = service || '使えるくらうどDB' ;
		var agreed = Cookies.get(cookieName);
		if (agreed) {
			dfd.resolve();
			return dfd.promise();
		}
		var modalHtml = (function () {/*
<div class="modal fade" id="termsModal" tabindex="-1" role="dialog" aria-labelledby="termsModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>-->
        <h4 class="modal-title" id="termsModalLabel">利用規約の確認</h4>
      </div>
      <div class="modal-body">
        <p>「%service%」をご利用いただくためには、以下の利用規約に同意していただく必要があります。</p>
        <div id="iframe-wrapper" class="row">
        <iframe class="col-xs-12" style="padding:0;margin:0 2%;width:96%;border:solid 1px #ccc;" src="%url%" frameborder="0"></iframe>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default save terms-ok" data-dismiss="modal">同意する</button>
        <button type="button" class="btn btn-primary terms-ng">同意しない</button>
      </div>
    </div>
  </div>
</div>
		*/}).toString().match(/\n([\s\S]*)\n/)[1].replace(/%url%/mg, terms).replace(/%service%/mg, serviceName);
		var modal = $(modalHtml);
		modal.modal({
			keyboard: false,
			backdrop: 'static'
		}).on('click', '.terms-ok', function(){
			if (cookieExpire) {
				Cookies.set(cookieName, 'agreed', { expires: cookieExpire });
			} else {
				Cookies.set(cookieName, 'agreed');
			}
			dfd.resolve();
		}).on('click', '.terms-ng', function(){
			modal.modal('hide');
			dfd.reject();
			location.href = '/my/logout';
		});
		return dfd.promise();
	}
	return { confirm: confirm };
})(jQuery);
