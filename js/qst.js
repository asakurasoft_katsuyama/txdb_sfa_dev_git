/*
 * qst.js
 *
 * @version 0.1.0
 * @author Hiroyuki OHARA
 */
/*! qst.js v0.1.0 H.OHARA 2015 @license MIT */
(function (root, globalName, factory) {
  if (typeof define === "function" && define.amd) {
    define(['js-cookie'], factory);
  } else if (typeof exports === "object") {
    module.exports = factory(require('js-cookie'));
  } else {
    root[globalName] = factory(root.Cookies);
  }
})(this, "qst", function (Cookies) {
  "use strict"
   var qst, root = this || (0, eval)('this'),
     localStorage = root.localStorage,
     sessionStorage = root.sessionStorage,
     JSON = root.JSON;

  // Helper functions
  function inherit(child, parent, obj) {
    var proto = parent.prototype;
    child.prototype = Object.create(proto, {
      constructor: { value: child, enumerable: false, writable: true, configurable: true }
    });
    if (parent._inherit_) parent._inherit_.call(parent, child);
    child.super_ = parent;
    child._super_ = proto;
    if (obj) methods(child, obj);
    return child;
  }

  function methods(klass, obj) {
    var k, proto = klass.prototype;
    for (k in obj) proto[k] = obj[k];
  }

  // abstract class
  function AbstractStorage(prefix){
    this.prefix = prefix;
  };

  methods(AbstractStorage, {
    set: function set(key, value){
      this._set(this.prefix + key, value);
    },
    get: function get(key){
      return this._get(this.prefix + key);
    },
    getJSON: function get(key){
      return this._getJSON(this.prefix + key);
    },
    remove: function remove(key){
      return this._remove(this.prefix + key);
    }
/*  ,
    _set: function _set(key, value){
      // override
    },
    _get: function _get(key){
      // override
    },
    _getJSON: function _getJSON(key){
      // override
    },
    _remove: function _remove(key){
      // override
    }
*/
  });

  // AbstractLSStorage
  function AbstractLSStorage(prefix, storage){
    this._storage = storage;
    AbstractStorage.call(this, prefix);
  };

  inherit(AbstractLSStorage, AbstractStorage, {
    _set: function _set(key, value){
      if (typeof value === 'object') {
        value = JSON.stringify(value);
      }
      this._storage.setItem(key, value);
    },
    _get: function _get(key){
      return this._storage.getItem(key);
    },
    _getJSON: function _getJSON(key){
      return JSON.parse(this._get(key));
    },
    _remove: function _remove(key){
      this._storage.removeItem(key);
    }
  });

  // localStorage
  function LStorage(prefix){
    AbstractLSStorage.call(this, prefix, localStorage);
  };
  inherit(LStorage, AbstractLSStorage);

  // sessionStorage
  function SStorage(prefix){
    AbstractLSStorage.call(this, prefix, sessionStorage);
  };
  inherit(SStorage, AbstractLSStorage);

  // Cookie
  function CStorage(prefix, options){
    AbstractStorage.call(this, prefix);
    if (options) this.options = options;
  };

  inherit(CStorage, AbstractStorage, {
    _set: function _set(key, value){
      Cookies.set(key, value, this.options);
    },
    _get: function _get(key){
      return Cookies.get(key);
    },
    _getJSON: function _getJSON(key){
      return Cookies.getJSON(key);
    },
    _remove: function _remove(key){
      if (this.options && this.options.path) {
        Cookies.remove(key, {path: this.options.path});
        return;
      }
      Cookies.remove(key);
    }
  });

  qst = {
    LStorage: LStorage,
    SStorage: SStorage,
    CStorage: CStorage,
    _storage: null,
    prefix: '',
    type: 'session',
    cookieOptions: undefined,
    storage: function storage(){
      if (!this._storage) {
        if (this.type === "session") {
          this._storage = sessionStorage ?
            new SStorage(this.prefix) : new CStorage(this.prefix, this.cookieOptions) ;
        } else if (this.type === "local") {
          this._storage = localStorage ?
            new LStorage(this.prefix) : new CStorage(this.prefix, this.cookieOptions) ;
        } else {
          this._storage = new CStorage(this.prefix, this.cookieOptions) ;
        }
      }
      return this._storage;
    },
    get: function get(key) {
      var st = this.storage();
      return st.get(key);
    },
    set: function set(key, value) {
      var st = this.storage();
      st.set(key, value);
    },
    getJSON: function get(key) {
      var st = this.storage();
      return st.getJSON(key);
    },
    remove: function remove(key) {
      var st = this.storage();
      return st.remove(key);
    }
  }

  return qst;
});