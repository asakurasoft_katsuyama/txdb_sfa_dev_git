/*
 * jQuery File Upload Plugin JS Example 8.9.1
 * https://github.com/blueimp/jQuery-File-Upload
 *
 * Copyright 2010, Sebastian Tschan
 * https://blueimp.net
 *
 * Licensed under the MIT license:
 * http://www.opensource.org/licenses/MIT
 */

/* global $, window */

$(function () {
    'use strict';




    var singleUpload = true;


    // Initialize the jQuery File Upload widget:
    $('#fileupload').fileupload({
        // Uncomment the following to send cross-domain cookies:
        //xhrFields: {withCredentials: true},
    	autoUpload : true,
        url: '/my/table/uploadIcon.json',
        deleteUrl: '/my/table/uploadIcon.json',
        filesContainer: $('table.files'),
        uploadTemplateId: null,
        downloadTemplateId: null,
        uploadTemplate: function (o) {
            var rows = $();
            $.each(o.files, function (index, file) {
                var row = $('<tr class="template-upload">' +
                    //'<td><span class="preview"></span></td>' +
                    '<td><p class="name"></p>' +
                    '<div class="error"></div>' +
                    '</td>' +
                    //'<td><p class="size"></p>' +
                    //'<div class="progress"></div>' +
                    //'</td>' +
                    '<td>' +
//                    (!index && !o.options.autoUpload ?
//                        '<button class="start" disabled>Start</button>' : '') +
//                    (!index ? '<button class="btn btn-default cancel">キャンセル</button>' : '') +
                    '</td>' +
                    '</tr>');
                row.find('.name').text(file.name);
                //row.find('.size').text(o.formatFileSize(file.size));
                if (file.error) {
                    row.find('.error').text(file.error);
                }
                rows = rows.add(row);
            });
            return rows;
        },
        downloadTemplate: function (o) {
            var rows = $();
            $.each(o.files, function (index, file) {
                var row = $('<tr class="template-download">' +
                    //'<td><span class="preview"></span></td>' +
                    '<td><p class="name"></p>' +
                    (file.error ? '<div class="error"></div>' : '') +
                    '</td>' +
                    //'<td><span class="size"></span></td>' +
                    '<td><button class="delete">Delete</button></td>' +
                    '</tr>');
                row.find('.size').text(o.formatFileSize(file.size));
                if (file.error) {
                    row.find('.name').text(file.name);
                    row.find('.error').text(file.error);
                } else {
                    row.find('.name').append($('<a></a>').text(file.originalName));
                    //if (file.thumbnailUrl) {
                    //    row.find('.preview').append(
                    //        $('<a></a>').append(
                    //            $('<img>').prop('src', file.thumbnailUrl)
                    //        )
                    //    );
                    //}
                    //row.find('a')
                    //    .attr('data-gallery', '')
                    //    .prop('href', file.url);
                    row.find('.delete')
                        .attr('data-type', file.deleteType)
                        .attr('data-url', file.deleteUrl);
                }
                rows = rows.add(row);
            });
            return rows;
        }
    }).bind('fileuploadadd', function (e, data) {
        var fileCount = $('#fileupload .files').find('.cancel,.delete').length;
        if (singleUpload && fileCount >= 1) {
            //console.log("The max number of files is 1");
            for (var i=0; i<fileCount; i++) {
            	$('#fileupload .files').find('.cancel,.delete')[i].click();
            }
        }
    });
    // Enable iframe cross-domain access via redirect option:
    $('#fileupload').fileupload(
        'option',
        'redirect',
        window.location.href.replace(
            /\/[^\/]*$/,
            '/cors/result.html?%s'
        )
    );

    if (window.location.hostname === 'blueimp.github.io') {
        // Demo settings:
        $('#fileupload').fileupload('option', {
            url: '//jquery-file-upload.appspot.com/',
            // Enable image resizing, except for Android and Opera,
            // which actually support image resizing, but fail to
            // send Blob objects via XHR requests:
            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            maxFileSize: 5000000,
            acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
        });
        // Upload server status check for browsers with CORS support:
        if ($.support.cors) {
            $.ajax({
                url: '//jquery-file-upload.appspot.com/',
                type: 'HEAD'
            }).fail(function () {
                $('<div class="alert alert-danger"/>')
                    .text('Upload server currently unavailable - ' +
                            new Date())
                    .appendTo('#fileupload');
            });
        }
    } else {
        // Load existing files:
        $('#fileupload').addClass('fileupload-processing');
        $.ajax({
            // Uncomment the following to send cross-domain cookies:
            //xhrFields: {withCredentials: true},
            url: $('#fileupload').fileupload('option', 'url'),
            dataType: 'json',
            context: $('#fileupload')[0]
        }).always(function () {
            $(this).removeClass('fileupload-processing');
        }).done(function (result) {
            $(this).fileupload('option', 'done')
                .call(this, $.Event('done'), {result: result});
        });
    }

});
