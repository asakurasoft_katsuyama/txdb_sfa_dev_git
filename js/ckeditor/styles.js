﻿/**
 * Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

// This file contains style definitions that can be used by CKEditor plugins.
//
// The most common use for it is the "stylescombo" plugin, which shows a combo
// in the editor toolbar, containing all styles. Other plugins instead, like
// the div plugin, use a subset of the styles on their feature.
//
// If you don't have plugins that depend on this file, you can simply ignore it.
// Otherwise it is strongly recommended to customize this file to match your
// website requirements and design properly.

CKEDITOR.stylesSet.add( 'default', [
	/* Block Styles */

	{ name: '見出し',		element: 'h1' },
	{ name: '中見出し',		element: 'h2' },
	{ name: '小さい見出し',		element: 'h3'},
	{ name: '段落',		element: 'p' },
	{ name: '注釈',		element: 'p', styles: { 'font-size': '10px' } }

]);

