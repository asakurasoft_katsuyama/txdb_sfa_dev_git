var ViewModel = ListModel.extend({
	init: function(data) {
		this._super();
		var self = this;
		
		// Fields -----------
		this.searchQuery = ko.observable(data.searchQuery);
		this.offset(data.offset);
		this.rowLimit(data.limit);
		
		// Initialize -----------
		this.load();
	},
	load: function() {
		var params = {q:this.searchQuery(), limit:this.rowLimit(), offset:this.offset()};
		$.getJSON('/admin/user/index.json', params, function(json) {
			this.rows(json.result.rows);
			this.rowCount(json.result.rowCount);
			this.totalRowCount(json.result.totalRowCount);
			$('body,html').stop().scrollTop(0);
		}.bind(this));
	},
	openEdit: function(i, sender) {
		document.location.href="/admin/user/modify?"+$.param({u:sender.id, offset:i});
	},
	openDelete: function(i, sender) {
		document.location.href="/admin/user/delete?"+$.param({u:sender.id, offset:i});
	},
	search: function() {
		this.offset(0);
		this.load();
	},
	clearSearch: function() {
		this.offset(0);
		this.searchQuery('');
		this.load();
	}
});
