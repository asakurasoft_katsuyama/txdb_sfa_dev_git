var pageScrollHelper = {
	params: function(key) {
		var result = {};
		location.search.substr(1).split("&").forEach(function(item) {
			var s = item.split("="), k = s[0], v = s[1] && decodeURIComponent(s[1]);
			result[k] = v;
		});
		if (key) return result[key];
		return result;
	},
	init : function () {
		var self = this;
		$(window).on('beforeunload', function(){
			var tid = self.params('t');
			var top = $(window).scrollTop();
			var left = $(window).scrollLeft();
			qst.set('scrollposition_'+tid, {top: top, left: left});
		});
	},
	reload : function () {
		var tid = this.params('t');
		var pos = qst.getJSON('scrollposition_'+tid);
		var top = (pos && pos.top) ? parseInt(pos.top, 10) : 0 ;
		var left = (pos && pos.left) ? parseInt(pos.left, 10) : 0 ;
		if (top != 0 || left != 0) {
			window.scrollTo(left, top);
		}
	}
};
$(function(){
	pageScrollHelper.init();
});

//
// 抽出条件モーダル
//
var FilterModalModal = FilterModel.extend({
	init: function(parent) {
		this._super({tableId: parent.tableId});
		this.tableId = parent.tableId;
		this.parent = parent;
		this.savefilterModal = new SavefilterModal(this);
		this.filterlistModal = new FilterlistModal(this);
	},
	open: function() {
		this.errors([]);
		$.getJSON('/my/data/criteria.json?'+$.param({t: this.tableId }), $.proxy(this.criteriaCallback, this));
	},
	close: function() {
		$('#filterModal').modal('hide');
	},
	openSavefilter: function(sender) {
		this.savefilterModal.open();
	},
	openFilterlist: function(sender) {
		this.filterlistModal.open();
	},
	criteriaCallback: function(json) {
		var criteria = json.result;
		var self = this;
		this.reset();
		this.boolOperator(criteria.boolOperator);
		if (criteria.sort.length > 0) {
			this.sortTerms($.map(criteria.sort, function(obj) {
				return new SortTerm(obj);
			}));
		}
		if (criteria.conditions.length > 0) {
			this.conditionTerms($.map(criteria.conditions, function(obj) {
				return new ConditionTerm(obj, self);
			}));
		}
		// Show modal.
		$('#filterModal').modal();
	},
	apply: function() {
		var self = this,
			viewModel = this.parent;
		var tryToApply = this.serialize();
		$.post('/my/data/save_criteria.json', {table_id:this.parent.tableId, criteria: ko.toJSON(tryToApply) }, function(json) {
			if (json.status == 'success') {
				self.close();
				viewModel.offset(0);
				viewModel.filter(null);
				viewModel.load();
			} else if (json.status == 'error') {
				self.errors(json.errors);
			} else {
				throw json.status;
			}
		});
	}
});

var SavefilterModal = Class.extend({
	init: function(parent) {
		var self = this;
		self.parent = parent;
		self.errors = ko.observableArray([]);
		self.name = ko.observable(null);
		self.filters = ko.observableArray([]);
	},
	saveAs: function(sender) {
		var self = this,
			parent = this.parent;
		var params = {table_id: parent.tableId, name:self.name(), criteria: ko.toJSON(self.parent.serialize()) };
		$.post('/my/data/save_filter.json', params, function(json) {
			if (json.status == 'success') {
				parent.parent.filterOptions.push(json.result);
				parent.parent.filter(json.result.id);
				parent.close();
				self.close();
			} else if (json.status == 'error') {
				self.errors(json.errors);
			} else {
				throw json.status;
			}
		}, 'json');
	},
	saveOverwrite: function(sender) {
		var self = this,
			parent = this.parent;
		var params = {table_id: parent.tableId, criteria: ko.toJSON(parent.serialize()), filter: sender.id };
		$.post('/my/data/save_filter.json', params, function(json) {
			if (json.status == 'success') {
				parent.parent.filter(json.result.id);
				parent.parent.load();	// Force update.
				parent.close();
				self.close();
			} else if (json.status == 'error') {
				self.errors(json.errors);
			} else {
				throw json.status;
			}
		}, 'json');
	},
	open: function() {
		var self = this,
			parent = this.parent;

		this.name(null);
		this.errors([]);
		$.getJSON('/my/data/filters.json?'+$.param({table_id: parent.parent.tableId}), function(json) {
			var options = $.map(json.result, function(filter) {
				return { id: filter.id, name: filter.name};
			});
			self.filters(options);
			$('#savefilterModal').modal();
		});
	},
	close: function() {
		$('#savefilterModal').modal('hide');
	}
});

var FilterEntry = Class.extend({
	init: function(data, parent) {
		this.parent = parent;
		this.id = data.id;
		this.name = ko.observable(data.name);
		this.nameOrg = data.name;
		this.isEditing = ko.computed(function() {
			return parent.editing() == this;
		}, this);
	}
});

//
// 抽出条件一覧モーダル
//
var FilterlistModal = Class.extend({
	init: function(parent) {
		this.parent = parent;
		this.errors = ko.observableArray([]);
		this.filters = ko.observableArray([]);
		this.editing = ko.observable(null);
		this.removefilterModal = new RemovefilterModal(this);
	},
	edit: function(sender) {
		this.editing(sender);
	},
	save: function(sender) {
		var self = this;
		this.errors([]);
		$.getJSON('/my/data/save_filter_name.json?'+$.param({table_id: this.parent.parent.tableId, filter_id: sender.id, name: sender.name() }), function(json) {
			if (json.status == 'success') {
				this.editing(null);
				var found = self.parent.parent.indexOfFilterOption(json.result.id);
				self.parent.parent.filterOptions.splice(found, 1, json.result);
			} else if (json.status == 'error') {
				this.errors(json.errors);
			}
		}.bind(this));
	},
	cancel: function(sender) {
		sender.name(sender.nameOrg);
		this.editing(null);
	},
	remove: function(sender) {
		this.removefilterModal.open(sender.id);
	},
	open: function() {
		$('#filterlistModal').modal();
		this.load();
	},
	load: function() {
		var self = this,
			parent = this.parent;
		this.errors([]);
		this.editing(null);
		$.getJSON('/my/data/filters.json?'+$.param({table_id: parent.parent.tableId}), function(json) {
			var options = $.map(json.result, function(filter) {
				return new FilterEntry(filter, self);
			});
			self.filters(options);
		});
	},
	close: function() {
		$('#filterlistModal').modal('hide');
	}
});

//
// 抽出条件の削除
//
var RemovefilterModal = Class.extend({
	init: function(parent) {
		this.parent = parent;
		this.filterId = undefined;
	},
	open: function(filterId) {
		this.filterId = filterId;
		$('#removefilterModal').modal();
	},
	close: function() {
		$('#removefilterModal').modal('hide');
	},
	remove: function() {
		$.getJSON('/my/data/remove_filter.json?'+$.param({table_id: this.parent.parent.parent.tableId, filter_id: this.filterId }), function(json) {
			if (json.status == 'success') {
				var found = this.parent.parent.parent.indexOfFilterOption(this.filterId );
				this.parent.parent.parent.filterOptions.splice(found, 1);
				this.close();
				this.parent.load();
			}
		}.bind(this));
	}
});

//
// グラフ選択モーダル
//
var SubtotalModal = Class.extend({
	init: function(parent) {
		this.parent = parent;
		this.func = ko.observable(null);
		this.sumableFields = ko.observableArray(null);
		this.fieldOptions = ko.computed(this.computeFieldOptions, this);
		this.field = ko.observable(null);	// 集計するカラム
		this.funcOptions = ko.computed(this.computeFuncOptions, this);
	},
	open: function(selected) {
		var self = this,
			params = { table_id: this.parent.tableId, filter: 'sumable' };
		$.getJSON('/my/data/columns.json', params, function(json) {
			self.sumableFields(json.result);
		});
		this.selected = selected;
		$('#subtotalModal').modal();
	},
	computeFieldOptions: function() {
		var func = this.func(),
			sumableFields = this.sumableFields();	// Lazy loading.
		if (func === 'sum') {
			return sumableFields;
		} else {
			return null;
		}
	},
	computeFuncOptions: function() {
		var sumableFields = this.sumableFields(),
			baseOptions = [ {id:'count', name: 'データ数のカウント'},
		                     {id:'sum', name: '合計'}];
		// 集計可能なカラムタイプがない場合は、「合計」を出さない
		if (sumableFields && sumableFields.length > 0) {
			return  baseOptions;
		} else {
			return  [ baseOptions[0] ];
		}
	},
	close: function() {
		$('#subtotalModal').modal('hide');
	},
	apply: function() {
		var params = {t: this.parent.tableId, fn: this.func(), af: this.field(), gf: this.selected };
		document.location.href = '/my/data/subtotal?'+ $.param(params);
	}
});

var TruncateModal = Class.extend({
	init: function(parent) {
		this.parent = parent;
		this.error = ko.observable(null);
		this.message = ko.observable(null);
	},
	open: function() {
		$('#truncateModal').modal();
	},
	close: function() {
		$('#truncateModal').modal('hide');
	},
	execute: function(sender) {
		var self = this;
		$.getJSON('/my/data/truncate.json?'+$.param({table_id:self.parent.tableId}), function() {
			self.parent.load();
			self.close();
		});
	}
});

//
// 単一カラムによるソート指定ソート
//
var SingleSortModel = FilterModel.extend({
	init: function(parent) {
		this._super({tableId: parent.tableId});
		this.tableId = parent.tableId;
		this.parent = parent;
		this.singleSortTerm = null;
	},
	sortByColumn: function(data) {
		this.singleSortTerm = new SortTerm(data, this);
		$.getJSON('/my/data/criteria.json?'+$.param({t: this.tableId }), $.proxy(this.criteriaCallback, this));
	},
	criteriaCallback: function(json) {
		var criteria = json.result;
		var self = this;
		this.boolOperator(criteria.boolOperator);
		this.sortTerms.removeAll();
		this.sortTerms.push(this.singleSortTerm);
		this.conditionTerms.removeAll();
		if (criteria.conditions.length > 0) {
			this.conditionTerms($.map(criteria.conditions, function(obj) {
				return new ConditionTerm(obj, self);
			}));
		}
		this.apply();
	},
	apply: function() {
		var self = this,
		viewModel = this.parent;
		var tryToApply = this.serialize();
		$.post('/my/data/save_criteria.json', {table_id:this.parent.tableId, criteria: ko.toJSON(tryToApply) }, function(json) {
			if (json.status == 'success') {
				viewModel.offset(0);
//				if (viewModel.filter() != -1) // all
					viewModel.filter(null);
				viewModel.load();
			} else {
				throw json.status;
			}
		});
	}
});

//
// 閲覧リスト
//
var ReadListModal = Class.extend({
	init: function (parent) {
		this.parent = parent;
		//this.list = ko.observableArray([]);
		this.rowId = 0;
		this.list = new ListModel();
		this.list.load = this.load.bind(this);
	},
	open: function(id) {
		this.rowId = id;
		$('#readListModal').modal();
		this.list.offset(0);
		this.load();
	},
	load: function() {
		var self = this;
		$.getJSON('/my/data/read_list.json?'+$.param({table_id:self.parent.tableId,
			row_id:this.rowId, offset:self.list.offset(), limit: 20 }))
		  .done(function(json) {
			  self.list.rows(json.data.rows);
			  self.list.columns(json.data.columns);
			  self.list.offset(json.data.offset);
			  self.list.rowCount(json.data.rowCount);
			  self.list.rowLimit(json.data.rowLimit);
			  self.list.totalRowCount(json.data.totalRowCount);
		  });
	}
});

var Calendar = Class.extend({
	init: function(data) {
		var self = this;
		self.inited = false;
		self.tableId = data.tableId;
		self.dateEntries = ko.observableArray([]);
		self.userEntries = ko.observableArray([]);
		self.calendarData = null;
		self.userOffset = ko.observable(0);
		self.userCount = ko.observable(0);
		//self.criteria = null;
		//self.criteriaTemp = ko.observableArray([]);
		self.startDate = ko.observable(null);
		self.userGroupOptions = ko.observableArray([]);
		self.userGroups= [];
		self.userGroupsTemp = ko.observableArray([]);

		//self.startDate.subscribe(function(value) {
		//	console.log("subscribe", value)
		//});
		self.userTotalCount = ko.observable(0);
		self.hasPrevUser = ko.computed(function () {
			return self.userOffset() > 0;
		});
		this.hasNextUser = ko.computed(function () {
			return self.userOffset() + self.userCount() < self.userTotalCount();
		});
	},
	setStartDate: function() {
		var self = this;
		if (self.inited)	// 初回ロードが完了するまで無視する
			self.loadCalendar();
	},
	load: function() {
		var self = this;
		$.when(
			$.getJSON('/my/data/user_options.json'),
			$.getJSON('/my/data/group_options.json'),
			$.getJSON('/my/data/calendar_criteria.json', { t: self.tableId})
		).done(function(res1, res2, res3) {
			self.userGroupOptions(self.createCompositeOptions(res1[0].result, res2[0].result));
			self.userGroups = res3[0].result.userGroups;
			self.userGroupsTemp($.map(res3[0].result.userGroups, function(val) {
				return (val ? (val.type+":"+(val.id===null ? '':val.id)) : null);
			}));
			self.startDate(res3[0].result.startDate);
			self.userOffset(res3[0].result.offset);
			self.loadCalendar();
		});

	},
	loadCalendar: function(scrolling) {
		var self = this;
		var data = {
			t: self.tableId,
			start_date: self.startDate(),
			offset: self.userOffset(),
			userGroups: ko.toJSON(self.userGroups)
		};
		$.getJSON('/my/data/index_calendar.json', data)
		  .done(function(json) {
			var currDate = (moment()).format('YYYY-MM-DD');
			var localTimeFix = function(item) {
				item.isToday = currDate === item.date;
				return item;
			};
			self.startDate(json.result.startDate);
			self.dateEntries(ko.utils.arrayMap(json.result.dateEntries, localTimeFix));
			self.userEntries(ko.utils.arrayMap(json.result.userEntries, function(user){
				user.dateEntries = ko.utils.arrayMap(user.dateEntries, localTimeFix);
				return user;
			}));
			self.userOffset(json.result.userOffset);
			self.userCount(json.result.userCount);
			self.userTotalCount(json.result.userTotalCount);
			self.calendarData = json.result;
			self.inited = true;	// 初回ロードが完了
			if (scrolling) {
				var destination = $('.calendar-block');
				if (destination.length > 0) {
					var top = destination.eq(0).offset().top;
					$('body,html').stop().scrollTop(top);
				}
			} else {
				pageScrollHelper.reload();
			}
		});
	},
	applyFilter: function() {
		var self = this,
		  userGroupsOrg = self.userGroups;

		self.userGroups = $.map(self.userGroupsTemp(), function(entry) {
			var parts = entry.split(':');
			return {
				id: parseInt(parts[1], 10),
				type: parts[0]
			};
		});

		self.userOffset(0);
		self.loadCalendar();
	},
	addData: function(user, date) {
		//console.log(arguments);
		document.location.href='/my/data/create?'+ decodeURIComponent($.param({
			t: this.tableId,
			intent: {
				date: date.date,
				user: user.id
			}
		}));
	},
	viewData: function(data) {
		//console.log(arguments);
		document.location.href='/my/data/view?'+ decodeURIComponent($.param({
			t: this.tableId,
			r: data.rowId
		}));
	},
	nextWeek: function() {
		var self = this;
		if (self.calendarData !== null) {
			self.startDate(self.calendarData.nextStartDate); // Fire change event.
			//self.loadCalendar();
		}
	},
	prevWeek: function() {
		var self = this;
		if (self.calendarData !== null) {
			self.startDate(self.calendarData.prevStartDate); // Fire change event.
			//self.loadCalendar();
		}
	},
	nextUser: function() {
		var self = this;
		if (self.calendarData !== null) {
			self.userOffset(self.userOffset() + 10);
			self.loadCalendar(true);
		}
	},
	prevUser: function() {
		var self = this;
		if (self.calendarData !== null) {
			self.userOffset(Math.max(0, self.userOffset() - 10));
			self.loadCalendar(true);
		}
	},
	createCompositeOptions: function(users, groups) {
		var wrapId = function(options, type) {
			var newOptions = [];
			for (var i = 0; i < options.length; i++) {
				newOptions.push({
					id: type+":"+options[i].id,
					name: options[i].name
				});
			}
			return newOptions;
		};
		return [
			new OptionGroup('ユーザー', wrapId(users, "user")),
			new OptionGroup('グループ', wrapId(groups, "group"))
		];
	}
});

var ViewModel = ListModel.extend({
	init: function(data, parent) {
		this._super();
		var self = this;

		// Fields -----------
		this.tableId = data.tableId;
		this.view = data.view;
		this.calendar = new Calendar(data);
		this.truncateModal = new TruncateModal(this);
		this.filterModal = new FilterModalModal(this);
		this.subtotalModal = new SubtotalModal(this);
		this.readListModal = new ReadListModal(this);

		this.filterOptions = ko.observableArray([]);
		this.offset(data.offset);
		this.rowLimitOptions = ko.observableArray([20,50,100]);
		this.rowLimit(data.limit);
		this.rowLimit.subscribe(function() {
			//console.log("Limit");
			this.load();
		}, this);

		this.fixedWidthMode = ko.observable(false);
		this.tableWidth = ko.observable(null);
		this.fixedColumn = 0;
		this.initHeader = false;

		this.filter = ko.observable(data.filter);
		this.filter.subscribe(function(value) {
			//console.log("filter");
			if (value == -1) { // all
				$.post('/my/data/clear_criteria.json', {table_id:self.tableId}, function(json) {
					if (json.status == 'success') {
						self.offset(0);
						self.load();
					} else if (json.status == 'error') {
						self.errors(json.errors);
					} else {
						throw json.status;
					}
				});
				return;
			}
			this.load();
		}, this);

		this.singleSortModel = new SingleSortModel(this);
		this.singleSortTerm = ko.observable(null);

		this.updateFilter = function() {
			$.getJSON('/my/data/filters.json?'+$.param({table_id: self.tableId}), function(json) {
				self.filterOptions([
				                    {id:null, name:''},
				                    {id:-1, name:'（すべて）'} // all
				                    ].concat(json.result));
			});
		};

		// Initialize -----------
		this.updateFilter();
	},
	addFixedColumn: function() {
		this.fixedColumn++;
	},
	indexOfFilterOption: function(id) {
		var options = this.filterOptions(),
			found = -1;
		for (var i=0;i<options.length; i++) {
			if (options[i].id == id) {
				return i;
			}
		}
		return -1;
	},
	load: function() {
		var self = this,
		  defaultWidth = {
			  multitext: 300,
			  richtext: 300,
			  file: 150,
			  else_: 100
		  };

		if (this.view == 'calendar') {

			this.calendar.load();

		} else {
			var params = {t:this.tableId, offset:this.offset(), limit:this.rowLimit(), filter: this.filter() };
			$.when(
			  $.getJSON('/my/data/index.json', params),
			  $.getJSON('/my/data/view_columns.json', params)
			  //$.getJSON('/my/api/v1/get_table.json', {id:self.tableId})
			).done(function(res1, res2) {
				  var i,
					viewColumns = res2[0].result, widthMap = {},
					widthSpecified = false,
					isSmallDevice = navigator.userAgent.match(/iPhone|iPad|Android/i);
				  for (i = 0; i < viewColumns.length; i++) {
					  if (viewColumns[i].width)
						  widthSpecified = true;
					  widthMap[viewColumns[i].column.id] = viewColumns[i].width || viewColumns[i].defaultWidth;
				  }

				  var fixedWidthMode = !isSmallDevice && widthSpecified,
					cumclativeWidh = 0;

				  //self.isReadCheckTarget(self.readCheckSettings && self.readCheckSettings.isReadCheckTarget);
				  self.rows(res1[0].result.rows);
				  self.columns($.map(res1[0].result.columns, function(column) {
					  column.columnWidth = widthMap[column.id];
					  cumclativeWidh += (widthMap[column.id] || 0);
					return column;
				  }));

				  self.fixedWidthMode(fixedWidthMode);
				  self.tableWidth(cumclativeWidh + (50 * self.fixedColumn));

				  //if (!isSmallDevice && widthSpecified) {
					//  self.tableWidth(cumclativeWidh + (50 * 3));
				  //}
				  self.rowCount(res1[0].result.rowCount);
				  self.totalRowCount(res1[0].result.totalRowCount);
				  self.singleSortTerm(res1[0].criteria.sort.length == 1 ? res1[0].criteria.sort[0] : null);
				  //$('body,html').stop().scrollTop(0);
				  $('body,html').stop();
				  if (!self.initHeader) {
				  	$('.table-fixed-header').fixedHeader();
					  self.initHeader = true;
				  }
				  //var toNumber = function(s) {
					//  if (!s ) return 0;
					//  return window.parseInt(s.replace(/px$/, ''), 10);
				  //};

				  //var cumclativeWidh = 0;
				  //$('[data-target=table] thead th').each(function() {
					//  cumclativeWidh += toNumber($(this).css('width'));
					//  console.log(cumclativeWidh);
				  //});
				  //if (!isSmallDevice && widthSpecified) {
					//  $('[data-target=table]').css('width', cumclativeWidh);
				  //}
				  pageScrollHelper.reload();
			  });
			//$.getJSON('/my/data/index.json', params, function(json) {
			//	this.rows(json.result.rows);
			//	this.columns(json.result.columns);
			//	this.rowCount(json.result.rowCount);
			//	this.totalRowCount(json.result.totalRowCount);
			//	this.singleSortTerm(json.criteria.sort.length == 1 ? json.criteria.sort[0] : null);
			//	$('body,html').stop().scrollTop(0);
			//}.bind(this));
		}

	},
	openView: function(i, sender) {
		document.location.href="/my/data/view?"+$.param({t:this.tableId, r:sender.id, offset:i});
	},
	openEdit: function(i, sender) {
		document.location.href="/my/data/edit?"+$.param({t:this.tableId, r:sender.id, offset:i});
	},
	openTruncate: function(sender, ev) {
		this.truncateModal.open();
	},
	openFilter: function(sender, ev) {
		this.filterModal.open();
	},
	openSubtotal: function(sender, ev) {
		this.subtotalModal.open(sender.id);
	},
	openReadList: function(row) {
		this.readListModal.open(row.id);
	},
	downloadFile: function(file, ev) {
		document.location.href = file.url + '/download/' + encodeURIComponent(file.fileName);
	},
	namedFileUrl: function(file) {
		return file.url + '/raw/' + encodeURIComponent(file.fileName);
	},
	sortClassName: function(sender) { // column
		var self = this;
		return ko.computed(function(){
			var sortTerm = self.singleSortTerm();
			if (sortTerm && sortTerm.column.id == sender.id) {
				return sortTerm.isReverse ? 'sort-desc' : 'sort-asc';
			}
			return 'sort-none'; // neather asc nor desc
		});
	},
	sortByColumn: function(sender) { // column
		var self = this;
		var filter = self.singleSortModel;
		var sortTerm = self.singleSortTerm();
		var data = {};
		data.column = sender;
		if (sortTerm && sortTerm.column.id == sender.id && !sortTerm.isReverse) {
			data.isReverse = true;
		}
		filter.sortByColumn(data);
	},
	afterBinding: function() {
		//console.log("afterBinding");
		this.load();
	},
	importDisabled: function() {
		return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	},
	exportDisabled: function() {
		return navigator.userAgent.match(/iPhone|iPad|iPod/i);
	},
	changeView: function() {
		document.location.href='/my/data/?'+ $.param({
		  t: this.tableId,
			view: this.view === 'list' ? 'calendar' : 'list'
		});
	},
	toggleRead: function(row) {
		//console.log(row);
		//console.log(arguments);
		var self = this;
		if (row.hasRead) return false;
		$.post('/my/data/update_read.json', {
			  table_id: self.tableId,
			  row_id: row.id,
			  value: !row.hasRead
		  },
		  function(json) {

			  if (json.status == 'success') {
			  } else {
				  throw json.status;
			  }

			  $.each(self.rows(), function(i, row_) {
				  if (row_.id === row.id) {
					  row_.readUsersCount = json.data.readUsersCount;
					  row_.hasRead = json.data.hasRead;
					  self.rows.splice(i, 1);
					  self.rows.splice(i, 0, row_);
					  return false;
				  }
			  });

		});
	}
});
