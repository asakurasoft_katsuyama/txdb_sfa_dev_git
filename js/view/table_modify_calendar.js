var ViewModel  = function(tableId) {
	var self = this;
	self._tableId = tableId;

	self.dateColumnId = ko.observable(null);
	self.userColumnId = ko.observable(null);
	self.startTimeColumnId = ko.observable(null);
	self.endTimeColumnId = ko.observable(null);
	self.titleColumnId = ko.observable(null);
	self.isCalendarEnabled= ko.observable(false);
	self.errors = ko.observableArray([]);

	self.userColumnOptions= ko.observableArray([]);
	self.dateColumnOptions= ko.observableArray([]);
	self.timeColumnOptions= ko.observableArray([]);
	self.titleColumnOptions= ko.observableArray([]);

	self.save = function() {
		self.errors([]);
		var data = {
			id: self._tableId,
			dateColumnId: self.dateColumnId(),
			userColumnId: self.userColumnId(),
			startTimeColumnId: self.startTimeColumnId(),
			endTimeColumnId: self.endTimeColumnId(),
			titleColumnId: self.titleColumnId(),
			isCalendarEnabled: self.isCalendarEnabled()
		};
		$.post('/my/table/save_calendar_settings.json', data,
			function(json) {
				if (json.status == 'success') {
					document.location.href="/my/table/modify?"+$.param({t:json.result});
				} else if (json.status == 'error') {
					self.errors(json.errors);
					$(window).scrollTop(0);
				} else {
					throw json.status;
				}
			}
		);
	};
	
	self._load = function() {
		$.when(
			$.getJSON('/my/table/load_info.json', {t:self._tableId}),
			$.getJSON('/my/table/columns.json', {t:self._tableId})
		).done(function(res1, res2) {
			var tableJson = res1[0].result;
			var columnsJson = res2[0].result;
			var userColumnOptions = [{id:null, label:'フィールドを選択'}],
				dateColumnOptions = [{id:null, label:'フィールドを選択'}],
				timeColumnOptions = [{id:null, label:'（指定なし）'}],
				titleColumnOptions = [{id:null, label:'フィールドを選択'}];

			for (var i= 0; i < columnsJson.length; i++) {
				if (columnsJson[i].rolesInCalendar.indexOf('user') != -1) {
					userColumnOptions.push(columnsJson[i]);
				}
				if (columnsJson[i].rolesInCalendar.indexOf('date') != -1) {
					dateColumnOptions.push(columnsJson[i]);
				}
				if (columnsJson[i].rolesInCalendar.indexOf('time') != -1) {
					timeColumnOptions.push(columnsJson[i]);
				}
				if (columnsJson[i].rolesInCalendar.indexOf('title') != -1) {
					titleColumnOptions.push(columnsJson[i]);
				}
			}
			self.userColumnOptions(userColumnOptions);
			self.dateColumnOptions(dateColumnOptions);
			self.timeColumnOptions(timeColumnOptions);
			self.titleColumnOptions(titleColumnOptions);

			if (tableJson['calendarSettings']) {
				self.dateColumnId(tableJson.calendarSettings.dateColumn ? tableJson.calendarSettings.dateColumn.id : null);
				self.userColumnId(tableJson.calendarSettings.userColumn ? tableJson.calendarSettings.userColumn.id : null);
				self.startTimeColumnId(tableJson.calendarSettings.startTimeColumn ? tableJson.calendarSettings.startTimeColumn.id : null);
				self.endTimeColumnId(tableJson.calendarSettings.endTimeColumn ? tableJson.calendarSettings.endTimeColumn.id : null);
				self.titleColumnId(tableJson.calendarSettings.titleColumn ? tableJson.calendarSettings.titleColumn.id : null);
			}
			self.isCalendarEnabled(tableJson.isCalendarEnabled);
		});
		//$.getJSON('/my/table/load_info.json?'+$.param({t:self._tableId}), function(json) {
		//	if (json.result['calendarSettings']) {
		//		self.dateColumnId(json.result.calendarSettings.dateColumnId);
		//		self.userColumnId(json.result.calendarSettings.userColumnId);
		//		self.startTimeColumnId(json.result.calendarSettings.startTimeColumnId);
		//		self.endTimeColumnId(json.result.calendarSettings.endTimeColumnId);
		//		self.titleColumnId(json.result.calendarSettings.titleColumnId);
		//	}
		//	self.isCalendarEnabled(json.result.isCalendarEnabled);
		//});
	};
	
	self._load();

};
