var ViewModel = ListModel.extend({
    init: function(data) {
        this._super();
        var self = this;
        // Fields -----------
        this.loadingDone = ko.observable(false);
        this.read = ko.observable(data.read);
        this.offset(data.offset);
        this.rowLimit(data.limit);
        this.read.subscribe(function(){
            self.offset(0);
            self.load(false, false, true);
        });
        this.allChecked = ko.observable(false);
        // Initialize -----------
        this.load(false, true);
    },
    load: function(nowrap, useAnchor, clear) {
        if (clear) {
            this.loadingDone(false);
            this.rows([]);
            this.rowCount(0);
            this.totalRowCount(0);
        }
        txdb.refreshNotificationsCount();
        var params = {
            read:this.read(),
            limit:this.rowLimit(),
            offset:this.offset()
        };
        $.getJSON('/my/notification/index.json', params, function(json) {
            this.allChecked(false);
            this.loadingDone(true);
            this.rows($.map(json.result.rows, function(receipt, i) {
                receipt.checked = ko.observable(false);
                return receipt;
            }));
            this.rowCount(json.result.rowCount);
            this.totalRowCount(json.result.totalRowCount);
            if (nowrap) {
                return;
            }
            var top = 0;
            if (useAnchor & location.hash != null & location.hash != '') {
                var anchor = this.getNearestAnchor();
                if (anchor) {
                    top = anchor.offset().top;
                }
            }
            $('body,html').stop().scrollTop(top);
        }.bind(this));
    },
    getNearestAnchor : function() {
        var goal = location.hash.replace('#','');
        var nearest = null;
        for (var i=0; i<this.rows().length; i++) {
            var current = this.rows()[i].notification.id
            if (nearest == null) {
                nearest = current
            }
            var nearestDiff = goal - nearest;
            var currentDiff = goal - current;
            if (currentDiff == 0) {
                nearest = current;
                break;
            }
            if (Math.abs(currentDiff) < Math.abs(nearestDiff)) {
                nearest = current;
            }
        }
        if (nearest) {
            var nearestId = 'notification_' + nearest;
            return $('#' + nearestId);
        } else {
            null;
        }
    },
    toggleRead: function(data) {
        var self = this;
        $.post('/my/notification/toggle_read.json',
            {
                'id': data.notification.id,
                'is_read': !data.isRead,
            },
            function(){
                self.onReadToggled(data);
            }, 'json')
    },
    openData: function(i, data) {
        //if (history.replaceState) {
        //    var anchor = location.protocol +
        //        "//" + location.host + location.pathname + location.search
        //        + "#" + data.notification.id;
        //    history.replaceState(null, document.title, anchor);
        //}
        document.location.href="/my/data/view?" +$.param({
            r:data.notification.row.id,
            t:data.notification.table.id,
            n:data.notification.id
        });
    },
    onReadToggled: function(data) {
        this.load(true);
        var element = $('#notification_'+data.notification.id);
        var wrapper = $('#notification__wrapper_'+data.notification.id);
        wrapper.css({ height: element.height() });
        element.animate( {'height':'0px', 'opacity':0}, 'fast', 'linear');
    },
    toggleAllCheck: function() {
        var allChecked = this.allChecked();
        $.each(this.rows(), function(i, receipt){
            receipt.checked(!allChecked);
        });
        return true;
    },
    updateAllCheck: function() {
        this.allChecked(this.isAllChecked());
        return true;
    },
    toggleReadInBatch: function() {
        var self = this;
        var ids = $.map(this.rows(), function(row, i) {
            if (row.checked()) {
                return row.notification.id;
            } else {
                return null;
            }
        });

        if (ids.length == 0) {
            alert("通知が選択されていません。");
            return false;
        }

        var msg;
        if (this.read() == 'unread') {
            msg = ids.length +
                " 件を既読にします。よろしいですか?";
        } else {
            msg = ids.length +
                " 件を未読に戻します。よろしいですか?";
        }
        if (!confirm(msg)) {
            return false;
        }
        $.post('/my/notification/toggle_read.json',
            {
                'id': ids,
                'is_read': self.read() == 'unread' ? true : false,
            },
            function(){
                self.load(true);
            }, 'json')
        return true;
    },
    isAllChecked: function() {
        var self = this;
        if (self.rows().length == 0) {
            return false;
        }
        var found = false;
        $.each(self.rows(), function(i, receipt) {
            if (!receipt.checked()) {
                found = true;
                return false;
            }
        });
        return !found;
    }
});
