var SortTerm = Class.extend({
	init: function(data, parent){
		this.parent = parent;
		this.id = ko.observable(data.column ? data.column.id : null);
		this.type = ko.observable(data['isReverse'] ? 1 : 0);
	}
});

var OptionGroup = Class.extend({
	init: function(label, children){
		this.label = label;
		this.children = children;
	}
});

var ConditionTerm = Class.extend({
	init: function(data, parent) {
		var self = this,
			dateFuncOptions = [
				{ id: null, name: '任意の日付', valueType: 'date' },
				{ id: 'now', name: '今日', valueType: false },
				{ id: 'last_month', name: '先月', valueType: 'days' },
				{ id: 'current_month', name: '今月', valueType: 'days' },
				{ id: 'current_year', name: '今年', valueType: false }
			],
			datetimeFuncOptions = [
				{ id: null, name: '任意の日時', valueType: 'datetime' },
				{ id: 'now', name: '当時刻', valueType: false },
				{ id: 'today', name: '今日', valueType: false },
				{ id: 'last_month', name: '先月', valueType: 'days' },
				{ id: 'current_month', name: '今月', valueType: 'days' },
				{ id: 'current_year', name: '今年', valueType: false }
			],
			datePresetColumns = ['datetime', 'date', 'created_on', 'updated_on', 'datecalc'],
			unselected = '';
		this.parent = parent;
		this.id = ko.observable(data['column'] ? data['column'].id : null);
		this.id.subscribe(function(id) {
			// Clear following conditions.
			self.operator(null);
			self.value(null);
			self.values([]);
			self.datePreset(null);
		});
		this.idText = ko.computed(function(){
			var id = self.id();
			var selected = ko.utils.arrayFirst(self.parent.searchableOptions(), function (item) {
				return item.id == id;
			});
			if (selected) return selected.label;
		});
		this.operator = ko.observable(data['operator'] ? data['operator'] : null);	// 演算子
		this.operatorOptions = ko.computed(function() {
			var id = this.id(),
				column = this.parent.searchableOptionFor(id),
				self = this;
			if (column == null) return false;
			return $.map(column.searchableOperators, function(operator) {
					return { id: operator, name: self.getOperatorLabel(operator) };
				});
		}, this);
		this.operatorText = ko.computed(function(){
			var id = self.operator();
			var options = self.operatorOptions();
			var selected = ko.utils.arrayFirst(options, function (item) {
				return item.id == id;
			});
			if (selected) return selected.name;
			return options ? options[0].name : null ;
		});
		this.datePreset = ko.observable(null);
		this.datePreset.subscribe(function(id) {
			self.value(null);
		});
		this.valueOptionsDays = this.getDaysOptions();
		this.datePresetOptions = ko.computed(function()	{
			var id = self.id(),
			column = this.parent.searchableOptionFor(id);
			if (column == null) return false;
			if (column.type == 'date') {
				return dateFuncOptions;
			} else if (column.type == 'datetime' ||
					column.type == 'created_on' ||
					column.type == 'updated_on') {
				return datetimeFuncOptions;
			} else if (column.type == 'datecalc') {
				if (column.dateFormat == 'datetime') {
					return datetimeFuncOptions;
				} else if (column.dateFormat == 'date') {
					return dateFuncOptions;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}, this);
		this.datePresetOptionText = ko.computed(function(){
			var id = self.datePreset();
			var options = self.datePresetOptions();
			var selected = ko.utils.arrayFirst(options, function (item) {
				return item.id == id;
			});
			if (selected) return selected.name;
			return options ? options[0].name : null ;
		});
		this.value = ko.observable(null);
		if (data['column'] && data['value'] && datePresetColumns.indexOf(data.column.type) != -1) {
			// Expand expression.
			var p;
			if ((p = data.value.indexOf('/')) != -1) {
				self.datePreset(data.value.substr(p + 1));	// 設定する順番があるので注意!
				self.value(data.value.substr(0, p));
			} else {
				self.value(data.value);
			}
		} else {
			self.value(data['value'] ? data.value : null);
		}

		this.daysOptionText = ko.computed(function(){
			var id = self.value();
			var options = self.valueOptionsDays;
			var selected = ko.utils.arrayFirst(options, function (item) {
				return item.id == id;
			});
			if (selected) return selected.name;
			return options ? options[0].name : null ;
		});

		self.values = ko.observableArray(null);	// 検索値（複数）
		if ('value' in data && data.value instanceof Array) {
			//var column = self.parent.searchableOptionFor(self);
			if (data['column'].type == 'user_selector') {
				self.values($.map(data['value'], function(val) {
					return (val ? (val.type+":"+(val.id===null ? '':val.id)) : null);
				}));
				//console.log(self.values());
			} else {
				self.values($.map(data['value'], function(val) {
					return (val == null ? unselected : val.id);
				}));
			}
		}

		self.valueType = ko.computed(function() {
			var id = self.id(),
				column = this.parent.searchableOptionFor(id),
				datePreset = self.datePreset();
			if (column == null) return null;
			switch (column.type) {
			case 'checkbox':
			case 'radio':
			case 'dropdown':
			case 'created_by':
			case 'updated_by':
				return 'options';
			case 'user_selector':
				return 'userGroupOptions';
			case 'time':
				return 'time';
			case 'datecalc':
				if (column.dateFormat == 'time') return 'time';
			case 'date':
			case 'datetime':
			case 'datecalc':
			case 'created_on':
			case 'updated_on':
			//case 'datecalc':
				return self.datePresetOptionFor(datePreset).valueType;
			default:
				return 'text';
			}
		}, this);

		self.valueOptionsOptions = ko.computed(function() {	// オプション（checkbox, radio, dropdown）
			var id = self.id(),
				column = this.parent.searchableOptionFor(id);
			if (column == null) return [];
			if (column.type == 'checkbox' ||
				column.type == 'radio' ||
				column.type == 'dropdown') {
				return [ { id: unselected, name: '未選択' }].concat(column.options);
			} else if (column.type == 'created_by' ||
					column.type == 'updated_by') {
				return parent._userOptionsCache;
			} else if (column.type == 'user_selector') {
				return self.createCompositeOptions(parent._userOptionsCache, parent._groupOptionsCache);
			}
		}, this);

		self.userGroupOptions = ko.computed(function() {
			var id = self.id(),
				column = this.parent.searchableOptionFor(id);
			if (column == null) return [];
			if (column.type == 'user_selector') {
				return self.createCompositeOptions(parent._userOptionsCache, parent._groupOptionsCache);
			}
		}, this);
	},
	createCompositeOptions: function(users, groups) {
		var wrapId = function(options, type) {
			var newOptions = [];
			for (var i = 0; i < options.length; i++) {
				newOptions.push({
					id: type+":"+options[i].id,
					name: options[i].name
				});
			}
			return newOptions;
		};
		return [
				new OptionGroup('ユーザー', [{ id: 'user:', name: '未選択'}].concat(wrapId(users, "user"))),
				new OptionGroup('グループ', wrapId(groups, "group"))
			];
	},
	getOperatorLabel: function(operator) {
		return {
			'=': '＝ (等しい)',
			'!=':'≠ (等しくない)',
			'like':'キーワードを含む',
			'not like':'キーワードを含まない',
			'>=':'≧ (以上)',
			'<=':'≦ (以下)',
			'<':'＜ (未満)',
			'>':'＞ (超える)',
			'contains': 'いずれかを含む',
			'not contains': 'いずれも含まない'
		}[operator];
	},
	datePresetOptionFor: function(id)	{
		var found = null;
		$.each(this.datePresetOptions(), function(i, option) {
			if (option.id == id)
				found = option;
		});
		return found;
	},
	serialize: function() {
		var unselected = '';
		if (this.id() == null)
			return null;
		var column = this.parent.searchableOptionFor(this.id());
		var value = null;
		if (column.hasOptions) {
			value = $.map(this.values(), function(value) {
				return value == unselected ? unselected : { id: value };
			});
		} else if (column.type == 'user_selector') {
			value = $.map(this.values(), function(value) {
				value.match(/^(user|group):(.*)$/);
				return { id: RegExp.$2, type: RegExp.$1 };
			});
		} else if (column.type == 'created_by' ||
				column.type == 'updated_by') {
			value = $.map(this.values(), function(value) {
				return { id: value };
			});
		} else if (!String.isEmpty(this.datePreset())) {
			value = (this.value()||'')+'/'+this.datePreset();
		} else {
			value = this.value();
		}
		return {
			column: { id: this.id(), type: column.type },
			operator: this.operator(),
			value: value
		};
	},
	getDaysOptions: function() {
		var options = [];
		options.push({ id:null ,name: 'すべて' });
		for(var i=1;i <= 31;i++)
			options.push({id:i.toString() ,name: i+'日'});
		options.push({ id: 'last_day' ,name:'末日'});
		return options;
	}
});


var FilterModel = Class.extend({
	init: function(options) {
		var self = this,
		sortTypeOptions = [ { id: '0', name: '昇順' }, { id: '1', name: '降順' } ],
		boolTypeOptions = [ { id: 'and', name: '下記条件のすべてに一致する' }, { id: 'or', name: '下記条件のいずれかに一致する'} ];

		// Fields ------
		self.tableId = options.tableId;
//		self.parent = parent;
//		self.filer = ko.observable(null);
//		self.savefilterModal = new SavefilterModal(self);
		self.sortableOptions = ko.observableArray([]);
		self.searchableOptions = ko.observableArray([]);
		self.errors = ko.observableArray([]);
		self.boolOperator = ko.observable(null);
		self.boolOperatorText = ko.computed(function(){
			var selected = ko.utils.arrayFirst(boolTypeOptions, function (item) {
				return item.id == self.boolOperator();
			});
			if (selected) return selected.name;
		});
		self.boolTypeOptions = boolTypeOptions;
		self._userOptionsCache = null;
		self._groupOptionsCache = null;
		self.sortTypeOptions = sortTypeOptions;
		self.sortTerms = ko.observableArray([]);
		self.conditionTerms = ko.observableArray([]);

		// Methods --------------
		self.reset();

		// Fetch searchable/conditional column options.
		$.getJSON('/my/data/columns.json?'+ $.param({ table_id: self.tableId }), function(json) {
			var sortableOptions = [ { id:null, name:"選択してください"} ],
				searchableOptions = [ { id:null, name:"選択してください"} ];
			$.each(json.result, function(i, column) {
				if (column.isSortable)
					sortableOptions.push(column);
				if (column.searchableOperators.length > 0)
					searchableOptions.push(column);
			});
			self.sortableOptions(sortableOptions);
			self.searchableOptions(searchableOptions);
		});

		// Preload user options.
		$.getJSON('/my/data/user_options.json', function(json) {
			self._userOptionsCache = json.result;
		});
		// Preload group options.
		$.getJSON('/my/data/group_options.json', function(json) {
			self._groupOptionsCache = json.result;
		});
	},
	serialize: function() {
		var criteria = {
			conditions: [],
			sort: [],
			boolOperator: this.boolOperator()
		};

		$.each(this.sortTerms(), function() {
			if (this.id() != null)
				criteria.sort.push({ column: { id: this.id() }, isReverse: this.type() == 1 });
		});

		$.each(this.conditionTerms(), function() {
			var obj = this.serialize();
			if (obj != null) {
				criteria.conditions.push(obj);
			}
		});
		return criteria;
	},
	reset: function() {
		this.sortTerms([ new SortTerm({}, this) ]);
		this.conditionTerms([ new ConditionTerm({}, this) ]);
	},
	removeSortTerm: function(data) {
		this.sortTerms.remove(data);
		if (this.sortTerms().length == 0) {
			this.sortTerms.push(new SortTerm({}, this));
		}
	},
	addConditionTerm: function(data) {
		var p = this.conditionTerms.indexOf(data);
		this.conditionTerms.splice(p + 1, 0, new ConditionTerm({}, this));
	},
	removeConditionTerm: function(data) {
		this.conditionTerms.remove(data);
		if (this.conditionTerms().length == 0) {
			this.conditionTerms.push(new ConditionTerm({}, this));
		}
	},
	addSortTerm: function(data) {
		var p = this.sortTerms.indexOf(data);
		this.sortTerms.splice(p + 1, 0, new SortTerm({}, this));
	},
	searchableOptionFor: function(id) {
		var found = null;
		$.each(this.searchableOptions(), function(i, column) {
			if (column.id == id && column.id != null)
				found = column;
		});
		return found;
	}
});
