
var ListModel = Class.extend({
	init: function() {
		var self = this;
//		this.options = $.extend({
//			forceScroll: true
//		}, options);
		
//		this.load = this.options.load;
		this.columns = ko.observableArray();
		this.rows = ko.observableArray();
		this.rowCount = ko.observable(0);
		this.offset = ko.observable(0);
		this.totalRowCount = ko.observable(0);
		this.rowLimit = ko.observable(20);
		this.hasPrev = ko.computed(function() {
			return self.offset() > 0; 
		});
		this.hasNext = ko.computed(function() {
			return self.offset() + self.rowCount() < self.totalRowCount();
		});
		this.offsetStart = ko.computed(function() {
			return self.rowCount() > 0 ? self.offset() + 1 : 0; 
		});
		this.offsetEnd = ko.computed(function() {
			return self.offset() + self.rowCount(); 
		});
	},
	nextList: function() {
		if (!this.hasNext())
			return;
		this.offset(this.offset() + parseInt(this.rowLimit(), 10));
		this.load();
//		if (this.options.forceScroll)
//			$(window).scrollTop(0);
	},
	prevList: function() {
		if (!this.hasPrev())
			return;
		this.offset(Math.max(0, this.offset() - parseInt(this.rowLimit(), 10)));
		this.load();
//		if (this.options.forceScroll)
//			$(window).scrollTop(0);
	},
	load: function() {
		// Overrideit
	}
});
