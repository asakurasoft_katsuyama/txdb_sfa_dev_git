var ViewModel  = function(userId) {
	self = this;
	self.id = userId;
	self.screenName = ko.observable('');
	self.loginId = ko.observable('');
	self.password = ko.observable('');
	self.profile = ko.observable('');
	self.seq = ko.observable(null);
	self.errors = ko.observableArray([]);
//	self.loginIdDuplicationError = ko.computed(function () {
//		var loginId = self.loginId();
//		if (self._isLoginIdDuplicated(loginId) ) {
//			return 'エラー';
//		} else {
//			return null;
//		}
//	}).extend({rateLimit: 500});
	
//	self.validate = function () {
//		var result = true;
////		if (self._isLoginIdDuplicated(self.loginId(), false)) {
////			result = false;
////		}
//		if (String.isEmpty(self.screenName())) {
//			self.errors.push({ message: '表示名を入力してください。' });
//			result = false;
//		}
//		if (String.isEmpty(self.screenName())) {
//			self.errors.push({ message: '表示名を入力してください。' });
//			result = false;
//		}
//		if (String.isEmpty(self.screenName())) {
//			self.errors.push({ message: '表示名を入力してください。' });
//			result = false;
//		}
//		return result;
//	}
	
	self.save = function() {
		self.errors.removeAll();
//		var json = ko.toJSON(viewModel);
		var data = ko.toJSON({
			id: self.id,
			screenName: self.screenName(),
			loginId: self.loginId(),
			password: self.password(),
			profile: self.profile(),
			seq: self.seq()
		});
		$.post('/admin/user/modify.json', {data:data}, function(json) {
			if (json.status == 'success') {
				document.location.href="/admin/user/";// ?"+$.param({offset: self.offset});
			} else if (json.status == 'error') {
				self.errors(json.errors);
				$(window).scrollTop(0);
			} else {
				throw json.status;
			}
		});
	}
	
	self.cancel = function() {
		document.location.href="/admin/user/";// ?"+$.param({offset: self.offset});
	}
	
	self._load = function() {
		if (self.id) {
			$.getJSON('/admin/user/load.json?'+$.param({u:self.id}), function(json) {
				self.screenName(json.result.screenName);
				self.loginId(json.result.loginId);
				self.password(json.result.password);
				self.profile(json.result.profile);
				self.seq(json.result.seq);
			});
		}
	}
	self._load();

};

