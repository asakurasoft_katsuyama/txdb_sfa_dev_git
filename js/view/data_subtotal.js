var ViewModel = Class.extend({
	init: function(data, parent){
		var self = this;
		this.tableId = data.t;
		this.error = ko.observable(null);
		this.message = ko.observable(null);
		this.list = new ListModel();
		$.getJSON('/my/data/subtotal.json', $.param(data), this.render.bind(this));
	},
	render: function(json) {
		
//		this.list.columns(json.result.columns);
//		this.list.rows(json.result.rows);
		this.list.columns(json.result.list.columns);
		this.list.rows(json.result.list.rows);
		
//		var data = $.map(json.result.rows, function(row) {
//			return { label: row.values[0].text, value: row.values[1].value };
//        });
		var data = $.map(json.result.editedList, function(row) {
			return { label: row.text, value: row.value };
		});
        
		nv.addGraph(function() {
		  var chart = nv.models.pieChart()
		      .x(function(d) { return d.label })
		      .y(function(d) { return d.value })
		      .showLabels(true);

		    d3.select("#chart svg")
		        .datum(data)
		        .transition().duration(350)
		        .call(chart);
		    nv.utils.windowResize(chart.update);
		  return chart;
		});
	},
	openBack: function() {
		history.back();
	}
});

