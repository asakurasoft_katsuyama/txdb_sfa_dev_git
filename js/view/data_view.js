
var OptionGroup = Class.extend({
	init: function(label, children){
		this.label = label;
		this.children = children;
	}
});


var Field = Class.extend({
	init: function(data, parent) {
		var self = this;
		self.parent = parent;
		self.id = data.column.id;
		self.code = data.column.code;
		self.type = data.column.type;
		self.label = data.column.label;
		self.value = data.value;
		self.text = data.text;
		self.html = data.html;
		self.labelHtml = data.column.labelHtml;
		self.showsLabel = data.column.showsLabel || false;
		self.optionsAlignment = data.column.optionsAlignment || null
		self.sizeWidth = data.column.sizeWidth || null
		self.sizeHeight = data.column.sizeHeight || null
		self.position = { x:data.column.positionX || 1, y:data.column.positionY || 1 };
		self.showsField = data.column.showsField || false;
		self.error = ko.observable(false);
		self.layoutParentId = data.column.layoutParentId || null;
		self.matrix = ko.observableArray([]); // for tab
		self.tabs = ko.observableArray([]); // for tab_group
		self.activeTab = ko.observable(null); // for tab_group
		self.tabs.subscribe(function(tabs){
			if (tabs.length > 0) {
				self.activeTab(tabs[0]);
			}
		});
		self.isRendered = ko.observable(false);
	},
	idName: function() {
		return 'col_' + this.id;
	},
	afterRender: function() {
		// Override.
		this.isRendered(true);
	},
	layout: function(allFields) {
		// override
	},
	initAfterBinding: function() {
	}
});

var FileField = Field.extend({
	init: function(data, parent){
		this._super(data, parent);
	}
});

var RelationalField = Field.extend({
	init: function(data, parent){
		this._super(data, parent);
		this.list = new ListModel();
		this.aggregateList = new ListModel();
		this.list.load = this.loadList.bind(this);
		this.referenceTable = data.column.referenceTable;
	},
	loadList: function () {
		$.post('/my/data/search_relational.json', { t:this.parent.tableId, c:this.id, row_id: this.parent.rowId, offset: this.list.offset() },
				this.renderList.bind(this), 'json');
	},
	renderList: function(json) {
		if (json.status === 'success') {
			this.list.rows(json.result.rows);
			this.list.columns(json.result.columns);
			this.list.offset(json.result.offset);
			this.list.rowCount(json.result.rowCount);
			this.list.rowLimit(json.result.rowLimit);
			this.list.totalRowCount(json.result.totalRowCount);

			this.aggregateList.rows(json.aggregate_result.rows);
			this.aggregateList.columns(json.aggregate_result.columns);
			this.aggregateList.offset(json.aggregate_result.offset);
			this.aggregateList.rowCount(json.aggregate_result.rowCount);
			this.aggregateList.rowLimit(json.aggregate_result.rowLimit);
			this.aggregateList.totalRowCount(json.aggregate_result.totalRowCount);
		} else if (json.status === 'error') {
			this.error(json.errors[0].message);
		}
		this.isRendered(true);
	},
	initAfterBinding: function() {
		this.loadList();
	},
	openReferer: function(data) {
		window.open('/my/data/view?' + $.param({ t:this.referenceTable.id, r: data.id }));
	},
	afterRender: function(elements) {
		// not rendered yet
	}
});
var RelationalInputField = Field.extend({
	init: function(data, parent){
		this._super(data, parent);
		this.list = new ListModel();
		this.list.load = this.loadList.bind(this);
		this.referenceTable = data.column.referenceTable;
	},
	loadList: function () {
		var params = {
			t:this.parent.tableId,
			c:this.id,
			row_id: this.parent.rowId,
			as_list: true
		};
		$.post('/my/data/search_relational_input.json', params,
			this.renderList.bind(this), 'json');
	},
	renderList: function(json) {
		if (json.status === 'success') {
			this.list.rows(json.result.rows);
			this.list.columns(json.result.columns);
			this.list.offset(json.result.offset);
			this.list.rowCount(json.result.rowCount);
			this.list.rowLimit(json.result.rowLimit);
			this.list.totalRowCount(json.result.totalRowCount);
		} else if (json.status === 'error') {
			this.error(json.errors[0].message);
		}
		this.isRendered(true);
	},
	initAfterBinding: function() {
		this.loadList();
	},
	openReferer: function(data) {
		var name='tx_data_view_' + data.id;
		var features='menubar=no,location=yes,resizable=yes,scrollbars=yes,status=yes';
		features+=',top=0,left=0';
		window.open('/my/data/view?' + $.param({ t:this.referenceTable.id, r: data.id, is_child: true }), name, features);
	},
	afterRender: function(elements) {
		// not rendered yet
	}
});
var LookupField = Field.extend({
	init: function(data, parent){
		this._super(data, parent);
		this.key = data.value ? data.value.key : null;
		this.referenceTable = data.column.referenceTable;
		this.isEmpty = ko.computed(function(){
			return !data.text;
		});
	},
	openReferer: function () {
		window.open('/my/data/view?' + $.param({ t:this.referenceTable.id, r: this.key }));
	}
});

var TabGroupField = Field.extend({
	layout: function (allFields) {
		var self = this;
		var fields = [], maxTabs = 1, tabs = [];
		$.each(allFields, function(i, field) {
			if (field.layoutParentId == self.id) {
				fields.push(field);
			}
		});
		$.each(fields, function(i, field) {
			if (field.showsField) {
				col = field.position.x;
				maxTabs = Math.max(maxTabs, col);
			}
		});
		// Sort by position.
		fields.sort(function(a, b) {
			if (a.position.x < b.position.x) return -1;
			if (a.position.x > b.position.x) return 1;
		});
		$.each(fields, function(i, field) {
			tabs.push(field);
		});
		self.tabs(tabs);
	}
});

var TabField = Field.extend({
	layout: function (allFields) {
		var self = this;
		var fields = [], maxCols = 1, maxRows = 1, matrix = [], unit, result;
		$.each(allFields, function(i, field) {
			if (field.layoutParentId == self.id) {
				fields.push(field);
			}
		});
		result = txdb.maxColsRows(fields, false);
		fields = result.fields;
		maxCols = result.maxCols;
		maxRows = result.maxRows;

		for (var i = 0; i < maxRows; i++)
			matrix[i] = [];
		unit = Math.floor(12/maxCols);
		$.each(fields, function(i, field) {
			var span = 12;
			if (field.sizeWidth) {
				span = field.sizeWidth * unit;
			}
			matrix[field.position.y - 1].push({ span: span ,field: field });
		});
		self.matrix(matrix);
	}
});

//
//
//
var ReadCommentModal = Class.extend({
	init: function (parent, callback) {
		var self = this;
		self.parent = parent;
		self.comment = ko.observable(null);
		//self.readList = 1;
		self.callback = callback;
	},
	open: function(readLog) {
		var self = this;
		self.comment(readLog.comment);
		$('#readCommentModal').modal();
	},
	close: function() {
		$('#readCommentModal').modal('hide');
	},
	update: function () {
		var self = this;
		$.post('/my/data/update_read.json?', {
			table_id: self.parent.tableId,
			row_id  : self.parent.rowId,
			value: true,
			comment: self.comment()
		})
		.done(function(json) {
			  self.callback();
			  self.close();
		});
	}
});

var ViewModel = Class.extend({
	init: function(tableId, rowId, offset, notificationId, isChild) {
		var self = this;
		this.isChild = isChild || false;
		this.offset = offset;
		this.tableId = tableId;
		this.rowId = rowId || null;	// Sets when edit,
		this.rowNo = null;
		this.notificationId = notificationId || null;
		this.fields = ko.observableArray([]);	// All fields.
		this._fields = ko.observableArray([]);	// All fields flatly.
		this.disabledNext = ko.observable(false);
		this.disabledPrev = ko.observable(false);
		this.readList = new ListModel();
		this.readList.load = this.loadReadList.bind(this);
		this.userGroupOptions= ko.observableArray([]);
		this.comments= ko.observableArray([]);
		this.notificationTargets= ko.observableArray([]);
		this.commentText = ko.observable(null);
		this.commentError = ko.observable(null);
		this.receipt = ko.observable(null);
		this.readState = ko.observable(null);
		this.readCheckWithComment = ko.observable(false);
		this.readCheckComment = ko.observable(null);
		this.readCommentModal = new ReadCommentModal(this, function() {
			self.loadReadList();
			self.loadReadState();
		});
		this.load();

	},
	load: function() {
		var self = this;
		$.getJSON('/my/api/v1/get_table.json?'+$.param({id:self.tableId}), function(tableJson) {
			$.getJSON('/my/data/fields.json?'+$.param({t:self.tableId, r:self.rowId}), self.render.bind(self));
			self.loadComment();
			self.loadUserGroupOptions();
			self.loadNotification();
			if (tableJson.data.isReadCheckEnabled) {
				self.loadReadList();
				self.loadReadState();
			}
		});
	},
	loadComment: function() {
		var self = this;
		$.getJSON('/my/data/comments.json?'+$.param({table_id:this.tableId, row_id:this.rowId}))
			.done(this.renderComment.bind(this));
	},
	loadReadList: function() {
		var self = this;
		$.getJSON('/my/data/read_list.json?'+$.param({table_id:this.tableId, row_id:this.rowId,offset:self.readList.offset() }))
		  .done(this.renderReadList.bind(this));
	},
	loadNotification: function() {
		if (this.notificationId) {
			var self = this;
			$.getJSON('/my/data/notification.json?'+$.param({table_id:this.tableId, row_id:this.rowId, notification_id:this.notificationId}))
				.done(this.renderNotification.bind(this));
		}
	},
	loadReadState: function() {
		var self = this;
		$.getJSON('/my/data/read_state.json?'+$.param({table_id:this.tableId, row_id:this.rowId}))
		  .done(function(json) {
			  if (json.data.isTarget) {
				  self.readState(json.data.state);
			  } else {
				  self.readState(null);
			  }
		  });
	},
	loadUserGroupOptions: function() {
		var self = this;
		$.when(
			$.getJSON('/my/data/user_options.json'),
			$.getJSON('/my/data/group_options.json')
		).done(function(res1, res2) {
				var options = self.createCompositeOptions(res1[0].result, res2[0].result);
				self.userGroupOptions(options);
			});
	},
	render: function(json) {
		var self = this;
		var fields = [], maxCols = 1, maxRows = 1, matrix = [], unit, result;
		$.each(json.result.values, function(i, data) {
			var field = self.createField(data, self);
			if (field.showsField) {
				fields.push(field);
			}
		});
		result = txdb.maxColsRows(fields, true);
		fields = result.fields;
		maxCols = result.maxCols;
		maxRows = result.maxRows;

		for (var i = 0; i < maxRows; i++)
			matrix[i] = [];
		unit = Math.floor(12/maxCols);
		$.each(fields, function(i, field) {
			if (field.layoutParentId) {
				return;
			}
			var span = 12;
			if (field.sizeWidth) {
				span = field.sizeWidth * unit;
			}
			matrix[field.position.y - 1].push({ span: span ,field: field });
		});

		$.each(fields, function(i, field) {
			field.layout(fields);
		});

		self.fields(matrix);
		$.each(fields, function(i, field) {
			field.initAfterBinding();
		});

		self.rowNo = json.result.no;
		self._fields(fields);
		var interval = window.setInterval(
			function() {
				var isRendered = true;
				$.each(self._fields(), function(i, field) {
					if (field.isRendered() == false) {
						isRendered = false;
						return false;
					}
				});
				if (isRendered) {
					window.clearInterval(interval);
					txdb.trigger('load.view.row.table', {
						row: new RowInterface(self)
					});
				}
			}, 500
		);
	},
	renderComment: function(json) {
		this.comments(json.result.rows);
	},
	renderNotification: function(json) {
		this.receipt(json.result);
	},
	renderReadList: function(json) {
		this.readList.rows(json.data.rows);
		this.readList.columns(json.data.columns);
		this.readList.offset(json.data.offset);
		this.readList.rowCount(json.data.rowCount);
		this.readList.rowLimit(json.data.rowLimit);
		this.readList.totalRowCount(json.data.totalRowCount);
	},
	createField: function(data, parent) {
		var map = {
			'relational': RelationalField,
			'relational_input': RelationalInputField,
			'lookup': LookupField,
			'file': FileField,
			'tab_group': TabGroupField,
			'tab': TabField
		};
		return new (map[data.column.type] || Field)(data, parent);
	},
	openNext: function() {
		this.openPrevNext(+1);
	},
	openPrev: function() {
		this.openPrevNext(-1);
	},
	openPrevNext: function(inc) {
		var self = this;
		$.getJSON('/my/data/offset_row.json?' +$.param({t:self.tableId, offset:self.offset + inc}), function(json) {
			if (json.status == 'success') {
				if (json.result) {
					location.href  = '/my/data/view?' + $.param({t:self.tableId, r:json.result, offset: self.offset + inc});
				} else {
					inc < 0 ? self.disabledPrev(true) : self.disabledNext(true);
				}
			} else {
				throw json.status;
			}
		});
	},
	remove: function() {
		var self = this;
		$.getJSON('/my/data/remove.json?' +$.param({t:self.tableId, r:self.rowId}), function() {
			location.href = '/my/data/?'+$.param({t:self.tableId});
		});
	},
	downloadFile: function(file, ev) {
		document.location.href = file.url + '/download/' + encodeURIComponent(file.fileName);
	},
	namedFileUrl: function(file) {
		return file.url + '/raw/' + encodeURIComponent(file.fileName);
	},
	postComment: function() {
		var self = this;
		self.commentError(null);
		var data = {
			text: this.commentText(),
			row_id: this.rowId,
			table_id: this.tableId,
			notificationTargets: ko.toJSON($.map(this.notificationTargets(), function(value) {
				var parts = value.split(':');
				return { type: parts[0], id: parseInt(parts[1], 10)};
			}))
		};
		$.post('/my/data/create_comment.json', data)
			.done(function(json) {
				if (json['status'] == 'success') {
					self.commentText(null);
					self.notificationTargets([]);
					self.loadComment();
				} else {
					self.commentError(json.errors[0].message);
				}
			});
	},
	deleteComment: function(target) {
		var self = this;
		if (window.confirm('本当に削除しますか？')) {
			var params = {table_id:self.tableId, row_id:self.rowId, comment_id:target.id};
			$.getJSON('/my/data/delete_comment.json', params).done(function() {
				self.comments.remove(target);
			});
		}
	},
	createCompositeOptions: function(users, groups) {
		var wrapId = function(options, type) {
			var newOptions = [];
			for (var i = 0; i < options.length; i++) {
				newOptions.push({
					id: type+":"+options[i].id,
					name: options[i].name
				});
			}
			return newOptions;
		};
		return [
			new OptionGroup('ユーザー', wrapId(users, "user")),
			new OptionGroup('グループ', wrapId(groups, "group"))
		];
	},
	openNotifications: function(data) {
		//history.back();
		document.location.href="/my/notification/#" + this.notificationId;
	},
	toggleNotificationRead: function(data) {
		var self = this;
		$.post('/my/data/toggle_notification_read.json',
			{
				'id': data.notification.id,
				'is_read': !data.isRead,
			}
			, 'json').done(function(json){
				self.renderNotification(json);
				txdb.refreshNotificationsCount();
			});
	},
	toggleRead: function(flg) {
		var self = this;
		$.post('/my/data/update_read.json', {
			table_id: self.tableId,
			row_id: self.rowId,
			value: flg,
			comment: self.readCheckWithComment() ? self.readCheckComment() : null,
			notification_id : self.notificationId
		}, 'json').done(function (json) {
			self.loadReadList();
			self.loadReadState();
			txdb.refreshNotificationsCount();
		});
	},
	openReadCommentEdit: function(data) {
		var self = this;
		self.readCommentModal.open(data);
	}
});

// see base.js txdb.RowReadInterface
var RowInterface = Class.extend({
	init: function(form) {
		this.form = form;
		this.fields = {};
		var fields = form._fields();
		for (var i = 0; i < fields.length; i++) {
			if (this._isSupportedFieldType(fields[i].type)) {
				this.fields[fields[i].code] = this._createFieldInterface(fields[i], this);
			}
		}
	},
	getTableId : function() { // interface
		return this.form.tableId;
	},
	getNo: function() { // interface
		return this.form.rowNo;
	},
	getHeaderMenuSpaceElement: function(){ // interface
		return $('#row-header-menu-space').get(0);
	},
	getField: function(code){ // interface
		return this.fields[code];
	},
	getSpaceFieldElement: function(code){ // interface
		return this.getField(code)._getExtensionPointElement();
	},
	getLabelFieldElement: function(code){ // interface
		return this.getField(code)._getExtensionPointElement();
	},
	_isSupportedFieldType: function(type){
		switch(type){
			case 'rownum':
			case 'text':
			case 'number':
			case 'calc':
			case 'radio':
			case 'dropdown':
			case 'date':
			case 'lookup':
			case 'label':
			case 'space':
				return true;
				break;
			default:
				return false;
		}
	},
	_createFieldInterface: function(field, parent) {
		return new ({
			'lookup': LookupFieldInterface,
			'dropdown': DropdownFieldInterface
		}[field.type] || FieldInterface)(field, parent);
	}
});

// see base.js txdb.FieldRowReadInterface
var FieldInterface = Class.extend({
	init: function(field, parentModel){
		this.field = field;
		this.row = parentModel;
	},
	getType: function(){ // interface
		return this.field.type;
	},
	getValue: function(){ // interface
		return this.field.value;
	},
	_get$Element: function(){
		return $('[data-fieldcode='+this.field.code+']')
			.eq(0);
	},
	_getExtensionPointElement: function(){
		return this._get$Element().find('.extension-point').get(0) ;
	}
});

var DropdownFieldInterface = FieldInterface.extend({
	getValue: function(){
		if (this.field.value == null) {
			return null;
		}
		return { name: this.field.value.name };
	}
});

var RadioFieldInterface = DropdownFieldInterface.extend({
});

var LookupFieldInterface = FieldInterface.extend({
	getValue: function(){
		return {
			//key: this.field.key,
			value: this.field.value
		};
	}
});
