var ViewModel  = function(data) {
	var self = this;
	this.message = ko.observable(null);
	this.tableId = data.tableId;
	this.charset = ko.observable('Shift_JIS');
	this.heading = ko.observable(false);
	this.toImport = ko.observable(false);
	this.disableHeading = ko.observable(false);
	this.toImport.subscribe(function(newVal) {
		if (newVal == true) {
			self.heading(true);
			self.disableHeading(true);
		} else {
			self.disableHeading(false);
		}
	});

	this.doExport = function(sender, ev) {
		self.message(null);
//		$('<iframe>')
//			.attr('src', '/my/data/export.csv?'+$.param({table_id: self.tableId, charset: self.charset(), heading: self.heading(), importing: self.toImport() }))
//			.css({width:1, height: 1, position: 'absolute', top: -99999})
//			.appendTo('body');
		$.getJSON('/my/data/export.json', {table_id: self.tableId, charset: self.charset(), heading: self.heading(), importing: self.toImport() }, function(json) {
			if (json.status === 'success') {
				self.message(json.message.message);
				document.location.href = '/my/data/download.csv/'+encodeURIComponent(json.result.fileName)+'?'+ $.param({table_id: self.tableId, file_key:json.result.fileKey})
			}
		});
	};
//	self.load = function() {
//		$.getJSON('/my/data/fields.json?', $.param({t:self.tableId, r:self.rowId}), self.render, 'json');
//	};
};

