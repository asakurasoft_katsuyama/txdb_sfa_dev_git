var ViewModel = ListModel.extend({
	init: function(data) {
		this._super();
		var self = this;
		
		// Fields -----------
		this.searchQuery = ko.observable(data.searchQuery);
		this.criterialUserId = ko.observable(data.criterialUserId);
		this.initialCriterialUserId = data.criterialUserId;
		this.userOptions = ko.observableArray([]);
		this.totalRowCount = ko.observable('');
		// Initialize -----------
		this.load();
	},
	load: function() {
		var params = {q:this.searchQuery(), criterial_user_id:this.criterialUserId() };
		$.getJSON('/admin/group/index.json', params, function(json) {
			this.rows(json.result.rows);
			this.totalRowCount(json.result.rows.length);
		}.bind(this));

		$.getJSON('/admin/group/user_options.json', params, function(json) {
			var options = json.result;
			options.unshift({
				id: null,
				name: ""
			});
			this.userOptions(options);
			this.criterialUserId(this.initialCriterialUserId); //再設定
		}.bind(this));
	},
	openEdit: function(i, sender) {
		document.location.href="/admin/group/modify?"+$.param({g:sender.id});
	},
	openEditUsers: function(i, sender) {
		document.location.href="/admin/group/users?"+$.param({g:sender.id});
	},
	openDelete: function(i, sender) {
		document.location.href="/admin/group/delete?"+$.param({g:sender.id});
	},
	search: function() {
		this.load();
	}
});
