var ViewModel = Class.extend({
	init: function(groupId) {
		//this._super();
		var self = this;

		self.groupId = groupId;
		// Fields -----------
		//this.userOptions = ko.observableArray([]);
		this.leftUserOptions = ko.observableArray([]);
		this.rightUserOptions = ko.observableArray([]);
		this.leftSelection = ko.observableArray([]);
		this.message = ko.observable(null);
		this.rightSelection = ko.observableArray([]);
		this.enableAdd = ko.computed(function() {
			return self.leftSelection().length  > 0;
		});
		this.enableRemove = ko.computed(function() {
			return self.rightSelection().length  > 0;
		});
		// Initialize -----------
		this.load();
	},
	load: function() {
		var self = this;
		$.when(
			$.getJSON('/admin/group/user_options.json', {}),
			$.getJSON('/admin/group/users.json', { g:self.groupId })
		).done(function(r1, r2) {
				var allUsers = r1[0].result,
					groupUsers = r2[0].result,
					leftUsers = [],
					rightUsers = [];
				for (var i= 0; i < allUsers.length; i++){
					var found = false;
					for (var j = 0; j < groupUsers.length; j++) {
						if (allUsers[i].id === groupUsers[j].id) {
							found = true;
							break;
						}
					}
					(found ? rightUsers : leftUsers).push(allUsers[i]);
				}
				self.leftUserOptions(leftUsers);
				self.rightUserOptions(rightUsers);
			});

	},
	save: function(i, sender) {
		var self = this;
		self.message(null);
		$.post('/admin/group/update_users.json', {group_id: self.groupId,
			user_ids: ko.utils.arrayMap(self.rightUserOptions(), function(x) {
				return x.id;
			})
		}).done(function(json) {
			if (json.status = 'success') {
				self.message('更新されました。');
			}
		});
	},
	add: function() {
		var self = this;
		self.message(null);

		ko.utils.arrayForEach(this.leftSelection(), function(x) {
			self.rightUserOptions.push(x);
			self.leftUserOptions.remove(x);
		});
		self.rightUserOptions.sort(function(left, right) {
			return left.name == right.name ? 0 : (left.name < right.name ? -1 : 1) });
		this.leftSelection([]);
		this.rightSelection([]);
	},
	remove: function() {
		var self = this;

		self.message(null);
		ko.utils.arrayForEach(this.rightSelection(), function(x) {
			self.leftUserOptions.push(x);
			self.rightUserOptions.remove(x);
		});
		self.leftUserOptions.sort(function(left, right) {
			return left.name == right.name ? 0 : (left.name < right.name ? -1 : 1) });
		this.leftSelection([]);
		this.rightSelection([]);
	},
	//openEditUsers: function(i, sender) {
	//	document.location.href="/admin/group/users?"+$.param({g:sender.id});
	//},
	//openDelete: function(i, sender) {
	//	document.location.href="/admin/group/delete?"+$.param({g:sender.id});
	//},
	search: function() {
		this.load();
	}
});
