if (!Array.prototype.indexOf) {
	Array.prototype.indexOf = function(obj, start) {
		for (var i = (start || 0), j = this.length; i < j; i++) {
			if (this[i] === obj) { return i; }
		}
		return -1;
	}
}

if (!Function.prototype.bind) {
	Function.prototype.bind = function (oThis) {
		if (typeof this !== "function") {
			// closest thing possible to the ECMAScript 5 internal IsCallable function
			throw new TypeError("Function.prototype.bind - what is trying to be bound is not callable");
		}

		var aArgs = Array.prototype.slice.call(arguments, 1),
				fToBind = this,
				fNOP = function () {},
				fBound = function () {
					return fToBind.apply(this instanceof fNOP && oThis
							 ? this
							 : oThis,
						 aArgs.concat(Array.prototype.slice.call(arguments)));
				};

		fNOP.prototype = this.prototype;
		fBound.prototype = new fNOP();

		return fBound;
	};
}

/* Simple JavaScript Inheritance
 * By John Resig http://ejohn.org/
 * MIT Licensed.
 */
// Inspired by base2 and Prototype
(function(){
  var initializing = false, fnTest = /xyz/.test(function(){xyz;}) ? /\b_super\b/ : /.*/;

  // The base Class implementation (does nothing)
  this.Class = function(){};

  // Create a new Class that inherits from this class
  Class.extend = function(prop) {
    var _super = this.prototype;

    // Instantiate a base class (but only create the instance,
    // don't run the init constructor)
    initializing = true;
    var prototype = new this();
    initializing = false;

    // Copy the properties over onto the new prototype
    for (var name in prop) {
      // Check if we're overwriting an existing function
      prototype[name] = typeof prop[name] == "function" &&
        typeof _super[name] == "function" && fnTest.test(prop[name]) ?
        (function(name, fn){
          return function() {
            var tmp = this._super;

            // Add a new ._super() method that is the same method
            // but on the super-class
            this._super = _super[name];

            // The method only need to be bound temporarily, so we
            // remove it when we're done executing
            var ret = fn.apply(this, arguments);
            this._super = tmp;

            return ret;
          };
        })(name, prop[name]) :
        prop[name];
    }

    // The dummy class constructor
    function Class() {
      // All construction is actually done in the init method
      if ( !initializing && this.init )
        this.init.apply(this, arguments);
    }

    // Populate our constructed prototype object
    Class.prototype = prototype;

    // Enforce the constructor to be what we expect
    Class.prototype.constructor = Class;

    // And make this class extendable
    Class.extend = arguments.callee;

    return Class;
  };
})();


String.isEmpty = function(val) {
	return val ==null || val == '';
};
ko.utils.formatDate = function(date, format) {
	if (date === null) return null;
	return moment(date).format(format);
};
ko.utils.escapeHtml = function(string)
{
	return String(string).replace(/[&<>"'\/]/g, function (s) {
		return {
		    "&": "&amp;",
		    "<": "&lt;",
		    ">": "&gt;",
		    '"': '&quot;',
		    "'": '&#39;',
		    "/": '&#x2F;'
		  }[s];
	});
}
// http://stackoverflow.com/questions/16063797/knockout-js-lazy-load-options-for-select
ko.bindingHandlers['populateInitialValue'] = {
    init: function(element, valueAccessor, allBindingsAccessor) {
       var bindings = allBindingsAccessor(),
           options = ko.utils.unwrapObservable(bindings.options),
           optionsValue = bindings.optionsValue,
           value = ko.utils.unwrapObservable(bindings.value),
           initialValue;

        if (options && !options.length) {
            if (optionsValue) {
                initialValue = {};
                initialValue[optionsValue] = value;
            }
            else {
                initialValue = value;
            }

            bindings.options.push(initialValue);
        }
    }
};
ko.bindingHandlers['datepicker'] = {
	init: function (element, valueAccessor, allBindingsAccessor, context) {
		var value = valueAccessor();
		element.value = window.ko.utils.unwrapObservable(value);
		$(element).parent().datetimepicker({ pickTime: false,  format: 'YYYY-MM-DD',
			minDate: new Date("Jan 1, 1000"), useCurrent: false, language: 'ja' });
		$(element).parent().on("change db.change dp.error dp.hide", function (event) {
			if (window.ko.isObservable(value)) {
				var thedate = $(element).parent().data("DateTimePicker").getDate();
				value(thedate != null ? moment(thedate).format('YYYY-MM-DD') : null);
			}
		});
		var data = $(element).parent().data("DateTimePicker");
		if ('disable' in allBindingsAccessor()) {
			if (allBindingsAccessor().disable()) {data.disable();} else {data.enable();}
		}
		$(element).parent().find('.datetime-clear').click(function(e){
			e.stopPropagation();
			e.preventDefault();
			data.setDate(null);
		});
	},
	update: function (element, valueAccessor, allBindingsAccessor, context) {
		var value = valueAccessor();
		element.value = window.ko.utils.unwrapObservable(value);
		var data = $(element).parent().data("DateTimePicker");
		if ('disable' in allBindingsAccessor()) {
			if (allBindingsAccessor().disable()) {data.disable();} else {data.enable();}
		}
	}
};
ko.bindingHandlers['timepicker'] = {
	init: function (element, valueAccessor, allBindingsAccessor, context) {
		var value = valueAccessor();
		element.value = window.ko.utils.unwrapObservable(value);
		$(element).parent().datetimepicker({ pickDate: false, format: 'HH:mm',
			useCurrent: false, language: 'ja' });
		$(element).parent().on("change db.change dp.error dp.hide", function (event) {
			var value = valueAccessor();
			if (window.ko.isObservable(value)) {
				var thedate = $(element).parent().data("DateTimePicker").getDate();
				value(thedate != null ? moment(thedate).format('HH:mm') : null);
			}
		});
		var data = $(element).parent().data("DateTimePicker");
		if ('disable' in allBindingsAccessor()) {
			if (allBindingsAccessor().disable()) {data.disable();} else {data.enable();}
		}
		$(element).parent().find('.datetime-clear').click(function(e){
			e.stopPropagation();
			e.preventDefault();
			data.setDate(null);
		});
	},
	update: function (element, valueAccessor, allBindingsAccessor, context) {
		var value = valueAccessor();
		element.value = window.ko.utils.unwrapObservable(value);
		var data = $(element).parent().data("DateTimePicker");
		if ('disable' in allBindingsAccessor()) {
			if (allBindingsAccessor().disable()) {data.disable();} else {data.enable();}
		}
	}
};
ko.bindingHandlers['ajaxLoadingSpy'] = {
	init: function (element, valueAccessor, allBindingsAccessor, context) {
		var value = valueAccessor();
		$(document).ajaxStart(function() {
			value(true);
		});
		$(document).ajaxStop(function() {	// All requests have completed..
			value(false);
		});
	}
};
ko.bindingHandlers['datepicker_v2'] = {
	init: function (element, valueAccessor, allBindingsAccessor, context) {
		ko.bindingHandlers['datepicker'].init(element, valueAccessor, allBindingsAccessor, context);
	},
	update: function (element, valueAccessor, allBindingsAccessor, context) {
		var value = valueAccessor();
		element.value = window.ko.utils.unwrapObservable(value);
		var data = $(element).parent().data("DateTimePicker");
		//console.log(data, value);
		data.setDate(element.value);
		if ('disable' in allBindingsAccessor()) {
			if (allBindingsAccessor().disable()) {data.disable();} else {data.enable();}
		}
	}
};
ko.bindingHandlers['datetimepicker'] = {
	init: function (element, valueAccessor, allBindingsAccessor, context) {
		var value = valueAccessor();
		element.value = window.ko.utils.unwrapObservable(value);
		var sideBySide = ($(element).width() + $(element).offset().left) > 500
						|| ($(element).width() + $(element).offset().left + 650) < $(document).width();
		$(element).parent().datetimepicker({ format: 'YYYY-MM-DD HH:mm',
			minDate: new Date("Jan 1, 1000"), sideBySide: sideBySide, useCurrent: false, language: 'ja' });
		$(element).parent().on("change db.change dp.error dp.hide", function (event) {
			var value = valueAccessor();
			if (window.ko.isObservable(value)) {
				var thedate = $(element).parent().data("DateTimePicker").getDate();
				value(thedate != null ? moment(thedate).format('YYYY-MM-DD HH:mm') : null);
			}
		});
		var data = $(element).parent().data("DateTimePicker");
		if ('disable' in allBindingsAccessor()) {
			if (allBindingsAccessor().disable()) {data.disable();} else {data.enable();}
		}
		$(element).parent().find('.datetime-clear').click(function(e){
			e.stopPropagation();
			e.preventDefault();
			data.setDate(null);
		});
	},
	update: function (element, valueAccessor, allBindingsAccessor, context) {
		var value = valueAccessor();
		element.value = window.ko.utils.unwrapObservable(value);
		var data = $(element).parent().data("DateTimePicker");
		if ('disable' in allBindingsAccessor()) {
			if (allBindingsAccessor().disable()) {data.disable();} else {data.enable();}
		}
	}
};
ko.bindingHandlers['select2'] = {
	init: function (element, valueAccessor, allBindingsAccessor, context) {
		var param = ko.utils.unwrapObservable(valueAccessor())
			allBindings = allBindingsAccessor(),
			options = { placeholder: '選択してください', allowClear: true },
			obj = null,
			lookupKey = null;

		if (typeof param == 'object') {
			obj = param.options || {};
			options = $.extend({}, options , obj);
//			lookupKey = param.lookupKey;
		}

		if (obj && obj.data) {
// https://github.com/ivaynberg/select2/wiki/Knockout.js-Integration
//			$(element).select2(options);
//			if (lookupKey && obj.data.results) {
//				var value = ko.utils.unwrapObservable(allBindings.value);
//				$(element).select2('data', ko.utils.arrayFirst(obj.data.results, function(item) {
//					return item[lookupKey] === value;
//				}));
//			}
			param.firstCallWithDataOption = true;
			setTimeout(function() { $(element).select2(options); }, 0);
		} else {
			setTimeout(function() { $(element).select2(options); }, 0);
		}

		ko.utils.domNodeDisposal.addDisposeCallback(element, function() {
			$(element).select2('destroy');
		});

	},
	update: function(element, valueAccessor) {
		var param = ko.utils.unwrapObservable(valueAccessor());
		if (typeof param == 'object' && param.firstCallWithDataOption == true) {
			param.firstCallWithDataOption = false;
			return;
		}
		$(element).trigger('change');
	}
};
ko.bindingHandlers['lightbox'] = {
	init: function (element, valueAccessor, allBindingsAccessor, context) {
	    var options = valueAccessor();
	    if ($(element).is('a') && options['source'])
	    	$(element).attr('href', options.source);
	    $(element).click(function(ev) {
	    	$(this).ekkoLightbox(options);
	    	ev.preventDefault();
	    });
	}
};

ko.bindingHandlers['css2'] = {
	update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
        var field = valueAccessor();
        var cssName = (allBindingsAccessor().css2Prefix || '') + field;
        ko.bindingHandlers.css.update(element, function() { return cssName; });
    }
};

// http://stackoverflow.com/questions/16284447/inline-ckeditor-integration-with-knockoutjs
// http://ericpanorel.net/2013/07/29/knockoutjs-binding-handler-for-ckeditor-4-inline/
// http://stackoverflow.com/questions/10713155/knockout-ckeditor-single-page-app
var _richeditor_seq =0;
ko.bindingHandlers['richeditor'] = {
	init: function (element, valueAccessor, allBindingsAccessor, viewModel) {
		window.setTimeout(function() {
			var id = $(element).attr("id");
			if (!id) {
				id = 'ckeditor_'+(_richeditor_seq++);
				$(element).attr("id", id);
			}
			var options = allBindingsAccessor().richTextOptions || {};
			var modelValue = valueAccessor();
			var value = ko.utils.unwrapObservable(valueAccessor());
			if ($(element).attr('rows')) {
				options.height = ($(element).attr('rows') * 20) + 'px';
			}

			if (!CKEDITOR.env.isCompatible) {
				$('<div class="form-control-static"></div>').html(value).insertAfter($(element));
				$(element).val(value).hide();
				return;
			}

			$(element).html(value);
			$(element).ckeditor(options);
			// wire up the blur event to ensure our observable is properly updated
			CKEDITOR.instances[id].focusManager.blur = function () {
				var e = this._;
				var ckValue = e.editor.getData();
				if (e.editor.checkDirty()) {
					var self = valueAccessor();
					if (ko.isWriteableObservable(self) && (ko.utils.unwrapObservable(self) !==ckValue)) {
						self(ckValue);
					}
					e.editor.resetDirty();
				}
				ckValue = null;
			};
			// handle disposal (if KO removes by the template binding)
			ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
				if (CKEDITOR.instances[id]){
					CKEDITOR.remove(CKEDITOR.instances[id]);
				}
			});
		}, 1);
	},
	update: function (element, valueAccessor, allBindingsAccessor, viewModel) {
		var id = $(element).attr("id");
		var value = ko.utils.unwrapObservable(valueAccessor()),
		existingEditor = CKEDITOR.instances && CKEDITOR.instances[id];
		if (existingEditor) {
			if (value !== existingEditor.getData()) {
				existingEditor.setData(value, function () {
					this.checkDirty(); // true
				});
			}
		}
	}
};

ko.bindingHandlers['tab'] = {
	init: function (element, valueAccessor, allBindingsAccessor, context) {
		var value = valueAccessor();
		if ('afterRender' in value) {
			value.afterRender();
		}
	}
};

ko.bindingHandlers['closablePopover'] = {
	update: function(element, valueAccessor, allBindingsAccessor, viewModel) {
		var options = ko.utils.unwrapObservable(valueAccessor());
		var shows = ko.utils.unwrapObservable(allBindingsAccessor().closablePopoverShows) || false;
		var elementId = element.id;
		var title = ko.utils.unwrapObservable(options.title) || null;
		var content = ko.utils.unwrapObservable(options.content) || null;
		options = $.extend(true, {}, options, { html: true, trigger: 'manual' });
		if (shows) {
			$(element).popover('destroy');
			if (elementId && content) {
				var closeButton = '<button type="button" class="close" onclick="$(&quot;#'+elementId+'&quot;).popover(&quot;destroy&quot;);">&times;</button>'
				if (title) {
					options.title = title + closeButton;
				} else {
					options.content = content + closeButton;
				}
				$(element).popover(options);
				$(element).popover('show');
			}
		} else {
			$(element).popover('destroy');
		}
	}
};

ko.bindingHandlers['inverseChecked'] = {
	init: function(element, valueAccessor, allBindingsAccessor) {
		var value = valueAccessor();
		var interceptor = ko.computed({
			read: function() {
					return !value();
			},
			write: function(newValue) {
					value(!newValue);
			},
			disposeWhenNodeIsRemoved: element
		});

		var newValueAccessor = function() { return interceptor; };
		ko.utils.domData.set(element, "newValueAccessor", newValueAccessor);
		ko.bindingHandlers.checked.init(element, newValueAccessor, allBindingsAccessor);
	},
	update: function(element, valueAccessor) {
		ko.bindingHandlers.checked.update(element, ko.utils.domData.get(element, "newValueAccessor"));
	}
};

ko.bindingHandlers['typeahead'] = {
	init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext){
		if(bindingContext.$root.readOnly) return;
		var tableId = bindingContext.$root.tableId;
		var colId = bindingContext.$data.id;
		var datasetName = 'engine';
		var suggestLimit = 10;

		var engine = new Bloodhound({
			datumTokenizer: function(d){
				return Bloodhound.tokenizers.whitespace(d.value);
			},
			queryTokenizer: Bloodhound.tokenizers.whitespace,
			remote: {
				url: '/my/data/search_typeahead.json',
				replace: function(url, query){
						return url + '?' + $.param({ t:tableId, c:colId, searchWord: query });
				},
				filter: function(json){
					if(json.status == 'success'){
						var dupCheck = [];
						return $.map(json.result.rows, function(val){
							if ($.isPlainObject(val.values[0].value)) {
								if($.inArray(val.values[0].value.value, dupCheck) >= 0){
									return null;
								}
								dupCheck.push(val.values[0].value.value);
								return {value: val.values[0].value.value};
							}else{
								if($.inArray(val.values[0].value, dupCheck) >= 0){
									return null;
								}
								dupCheck.push(val.values[0].value);
								return {value: val.values[0].value};
							}
						});
					}else if(json.status == 'error'){
						return [{value: json.errors[0].message}];
					}
//					return $.map(data.result.rows, function(val){
//						return {value: val.values[0].value};
//					});
				},
				rateLimitWait: 1000,
			},
			limit: suggestLimit
		});

		engine.initialize();

		$(element).typeahead({
			hint: false,
			highlight: true,
			minLength: 1
		},
		{
			name: datasetName,
			displayKey: 'value',
			source: engine.ttAdapter()
		})
		.on("typeahead:selected", function(){
			$(element).change();
		})
		.on("typeahead:autocompleted", function(){
			$(element).change();
		});

		ko.utils.domNodeDisposal.addDisposeCallback(element, function () {
			$(element).typeahead("destroy");
			//$(element) = null;//
		});
	},
	update: function(element, valueAccessor, viewModel, bindingContext){
		if(bindingContext.parent.readOnly) return;
//		var value = ko.utils.unwrapObservable(valueAccessor());
//		console.log(value);
		$(element).typeahead('val', ko.utils.unwrapObservable(valueAccessor()));
	}
};

// http://stackoverflow.com/questions/8961770/similar-to-jquery-closest-but-traversing-descendants
(function($) {
    $.fn.closest_descendent = function(filter) {
        var $found = $(),
            $currentSet = this; // Current place
        while ($currentSet.length) {
            $found = $currentSet.filter(filter);
            if ($found.length) break;  // At least one match: break loop
            // Get all children of the current set
            $currentSet = $currentSet.children();
        }
        return $found.first(); // Return first match of the collection
    }
})(jQuery);

// https://github.com/twbs/bootstrap/issues/6996
$.fn.modal.Constructor.prototype.enforceFocus = function() {
	modal_this = this
	$(document).on('focusin.modal', function (e) {
		if (modal_this.$element[0] !== e.target && !modal_this.$element.has(e.target).length
			&& !$(e.target).hasClass('cke_panel_frame')
			&& !$(e.target.parentNode).hasClass('cke_dialog_ui_input_select') && !$(e.target.parentNode).hasClass('cke_dialog_ui_input_text')) {
			modal_this.$element.focus()
		}
	})
};

// https://github.com/twbs/bootstrap/issues/9023
$.fn.iOsModalFix = function() {
	if( navigator.userAgent.match(/iPhone|iPad|iPod/i) ) {

		$(this).on('show.bs.modal', function() {

		    // Position modal absolute and bump it down to the scrollPosition
//		    $(this)
//		        .css({
//		            position: 'absolute',
//		            marginTop: $(window).scrollTop() + 'px',
//		            bottom: 'auto'
//		        });

		    // Position backdrop absolute and make it span the entire page
		    //
		    // Also dirty, but we need to tap into the backdrop after Boostrap
		    // positions it but before transitions finish.
		    //
		    setTimeout( function() {
		        $('.modal-backdrop').css({
		            position: 'absolute',
		            top: 0,
		            left: 0,
		            width: '100%',
		            height: Math.max(
		                document.body.scrollHeight, document.documentElement.scrollHeight,
		                document.body.offsetHeight, document.documentElement.offsetHeight,
		                document.body.clientHeight, document.documentElement.clientHeight
		            ) + 'px'
		        });
		    }, 0);
		});
	}
	return this;
};

var txdb = {
	refreshNotificationsCount : function(){
		$.getJSON('/my/count_notification.json', function(json){
			var wrapper = $('.global-notifications-count-wrapper')
			var element = $('.global-notifications-count')
			if (json.status == 'success') {
				if (json.result == 0) {
					wrapper.hide();
				} else {
					wrapper.show();
				}
				element.text(json.result);
			} else {
				wrapper.hide();
			}
		});
	},
	on: function(eventType, handler){
		$(this).on(eventType, function (event, context) {
			return handler(context);
		});
	},
	trigger: function(eventType, context){
		context.$ = $;
		$(this).trigger(eventType, [context]);
	},
	maxColsRows: function(fields, skipChild) {
		var maxCols = 1, maxRows = 1, offsets = {}, offset = 0, col = 0;
		// Sort by x/y position.
		fields.sort(function(a, b) {
			if (a.position.y < b.position.y) return -1;
			if (a.position.y > b.position.y) return 1;
			if (a.position.x < b.position.x) return -1;
			if (a.position.x > b.position.x) return 1;
		});
		$.each(fields, function(i, field) {
			if (field.showsField) {
				if (skipChild && field.layoutParentId) return;
				offset = offsets[field.position.y] ? offsets[field.position.y] : 0 ;
				if (field.sizeWidth) {
					col = offset + field.sizeWidth;
					offsets[field.position.y] = col
				} else { // line, space, label
					col = field.position.x;
				}
				maxCols = Math.max(maxCols, col);
				maxRows = Math.max(maxRows, field.position.y);
			}
		});
		return {
			fields: fields,
			maxCols: maxCols,
			maxRows: maxRows
		};
	}
	/* Interfaces

	RowReadInterface = Class.extend({
		getTableId : function() {
			return tableId;
		},
		getNo : function() {
			return rowno;
		},
		getHeaderMenuSpaceElement: function(){
			return DOMelement;
		},
		getField: function(code){
			return FieldModel;
		},
		getSpaceFieldElement: function(code){
			return DOMelement;
		},
		getLabelFieldElement: function(code){
			return DOMelement;
		},
	})

	RowWriteInterface = Class.extend({
		calculate(){
		},
		isCopy(){
			return boolean;
		}
	})

	FieldReadInterface = Class.extend({
		getType: function(){
			return string;
		},
		getValue: function(){
			return mixed;
		}
	})

	FieldWriteInterface = Class.extend({
		setValue: function(value){
		},
		lookup: function(){
			return boolean;
		},
		clear: function(){
		}
	})


	*/
};
$(function() {

	$.ajaxSetup({
		error: function(xhr, status, err) {
			// Ignore abort
		    if (xhr.status === 0 || xhr.readyState === 0) {
		        return;
		    }

			var message = 'システムエラーが発生しました。';
//			var message = xhr.responseText;
			if (/application\/json/.test(xhr.getResponseHeader('Content-Type'))) {
                try {
                	var json = JSON.parse(xhr.responseText);
                	message = json.errors[0].message;
                } catch(e) {}
            }
			alert(message);
		}
	});

	$.blockUI.defaults = $.extend(true, {}, $.blockUI.defaults, {
		message:
			'<div class="modal">'
			  + '<div class="modal-dialog">'
			    + '<div class="modal-content">'
			      + '<div class="modal-body">'
			        + '<img src="/img/ajax-loader.gif" />'
			      + '</div>'
			    + '</div>'
			  + '</div>'
			+ '</div>',
		css: { backgroundColor: 'transparent', border: 'none', cursor: 'default' },
		overlayCSS: { backgroundColor: '#fff', opacity: 0.2, cursor: 'default' },
		baseZ: 5000,
		fadeIn: 0,
		fadeOut: 0,
		onUnblock : function(element, options) { $('body').css('cursor', 'default'); }
	});

	if (window.setupBlockUI) {
		window.setupBlockUI();
	} else {
		$(document).ajaxStart($.blockUI).ajaxStop($.unblockUI);
	}

	$('.modal').iOsModalFix();
});


// http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript
$.parseQuery = function(queryString) {
	var a, b, c, e;
	a = {};
	b = queryString || window.location.search.substring(1);
	c = function(d) {
		return d && decodeURIComponent(d.replace(/\+/g, ' '));
	};
	e = function(f, g, h) {
		var i, j, k, l;
		i = g.indexOf('[');
		if (i !== -1) {
			j = g.slice(0, i);
			k = g.slice(1 + i).slice(0, g.slice(1 + i).indexOf(']'));
			l = g.slice(1 + i).slice(1 + g.slice(1 + i).indexOf(']'));
			if (k) {
				if (typeof f[j] !== 'object') {
					f[j] = {};
				}
				f[j][k] = l ? e(f[j], k + l, h) : h;
			} else {
				if (typeof f[j] !== 'object') {
					f[j] = [];
				}
				f[j].push(h);
			}
			return f[j];
		} else {
			if (f.hasOwnProperty(g)) {
				if (typeof f[g] === 'object') {
					f[g].push(h);
				} else {
					f[g] = [].concat.apply([f[g]], [h]);
				}
			} else {
				f[g] = h;
			}
			return f[g];
		}
	};
	b.split('&').forEach(function(m) {
		e(a, c(m.split('=')[0]), c(m.split('=')[1]));
	});
	return a;
};
