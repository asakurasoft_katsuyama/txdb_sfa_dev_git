var Permission = function(data, parent) {
	var self = this;
	self.parent = parent;
	self.id = data.id;
	self.role = data.role;
	self.screenName = ko.observable(data.screenName);
	self.profile = ko.observable(data.profile);
	self.isReadable = ko.observable(data.isReadable == 1);
	self.isWritable = ko.observable(data.isWritable == 1);
	self.display = ko.observable(true);
	
	if(data.isWritable == 1){
		self.disableIsReadable = ko.observable(true);
	}else{
		self.disableIsReadable = ko.observable(false);
	}
	self.isWritable.subscribe(function(newVal) {
		if (newVal == true) {
			self.isReadable(true);
			self.disableIsReadable(true);
		} else {
			self.disableIsReadable(false);
		}
	});
	
	self.serializeValue = function() {
		return { id: self.id, isReadable: self.isReadable(), isWritable: self.isWritable() };
	};
};

var ViewModel = function(option) {
	var self = this;

	// Fields -----------
	self.tableId = option.tableId;
	self.offset = ko.observable(option.offset);
	self.rowLimit = ko.observable(option.limit);
	self.rowCount = ko.observable(0);
	self.totalRowCount = ko.observable(0);
	self.rows = ko.observableArray();
	self.errors = ko.observableArray();
	self.isAllReadable = ko.observable(false);
	self.isAllWritable = ko.observable(false);

	self.searchQuery = ko.observable(option.searchQuery);
	
	// アクセス制限の状態
	self.permissionControlState = ko.observable();

	// 表示行の全チェック処理（閲覧）
	self.isAllReadable.subscribe(function(newVal) {
		if (newVal == true) {
			for(var i = 0; i < self.rows().length; i++){
				if(!self.rows()[i].disableIsReadable() && self.rows()[i].display()){
					self.rows()[i].isReadable(true);
				}
			}
		} else {
			for(var i = 0; i < self.rows().length; i++){
				if(!self.rows()[i].disableIsReadable() && self.rows()[i].display()){
					self.rows()[i].isReadable(false);
				}
			}
		}
	});

	// 表示行の全チェック処理（編集）
	self.isAllWritable.subscribe(function(newVal) {
		if (newVal == true) {
			for(var i = 0; i < self.rows().length; i++){
				if(self.rows()[i].display()){
					self.rows()[i].isWritable(true);
				}
			}
		}else{
			for(var i = 0; i < self.rows().length; i++){
				if(self.rows()[i].display()){
					self.rows()[i].isWritable(false);
				}
			}
		}
	});

	self.hasPrev = ko.computed(function() {
		return self.offset() > 0; 
	});
	
	self.hasNext = ko.computed(function() {
		return self.offset() + self.rowCount() < self.totalRowCount();
	});
	
	self.offsetStart = ko.computed(function() {
		return self.rowCount() > 0 ? self.offset() + 1 : 0; 
	});
	
	self.offsetEnd = ko.computed(function() {
		return self.offset() + self.rowCount(); 
	});

	// 初期読込み処理
	self.loadInfo = function () {
		// テーブルのアクセス制限情報　取得
		$.getJSON('/my/table/loadInfo.json?'+$.param({t:self.tableId}), function(json) {
			self.permissionControlState(json.result.permissionControl ? 'limited' : 'unlimited');
		});
		$('body,html').stop().scrollTop(0);
	};

	// ユーザの権限情報取得
	self.loadPermission = function () {
		var params = {t: self.tableId, q: self.searchQuery(), limit:self.rowLimit(), offset:self.offset()};
		$.getJSON('/my/table/permission.json', params, function(json) {
			self.rowCount(json.result.rowCount);
			self.totalRowCount(json.result.totalRowCount);
			
			var rows = [];
			rows = $.map(json.result.rows, function(data) {
				if(data){
					return new Permission(data, self);
				}
			});
			
			for (var i=0; i < rows.length; i++) {
				var isPush = true;
				
				for(var j = 0; j < self.rows().length; j++){
					if(self.rows()[j].id == rows[i].id){
						isPush = false;
						
						// ユーザ情報更新
						self.rows()[j].display(true);
						self.rows()[j].screenName(rows[i].screenName());
						self.rows()[j].profile(rows[i].profile());
						break;
					}
				}
				if(isPush){
					self.rows.push(rows[i]);
				}
			}
			
		});
	};
	
	// Initialize -----------
	self.loadInfo();
	self.loadPermission();

	// 保存処理
	self.save = function () {
		$.post('/my/table/savePermission.json', {table_id: self.tableId, data: self.serialize(), state: self.permissionControlState() == 'limited'}, function(json) {
				if (json.status == 'success') {
					document.location.href='/my/table/modify?'+$.param({t:json.result});
				}
		}, 'json');
	};

	self.serialize = function() {
		return ko.toJSON($.map(self.rows(), function(row) {
			return row.serializeValue();
		}));
	};

	// 検索処理
	self.search = function(){
		// 表示初期化
		for(var i= 0; i < self.rows().length; i++){
			self.rows()[i].display(false);
		}
		
		// 全チェックを初期化
		self.isAllReadable(false);
		self.isAllWritable(false);
		
		self.offset(0);
		self.loadPermission();
	};

	// 検索クリア
	self.clearSearch = function(){
		// 表示初期化
		for(var i= 0; i < self.rows().length; i++){
			self.rows()[i].display(false);
		}
		
		// 全チェックのチェックボックスを初期化
		self.isAllReadable(false);
		self.isAllWritable(false);
		
		self.offset(0);
		self.searchQuery('');
		self.loadPermission();
	};

	// ページング処理(next)
	self.nextList = function() {
		if (!self.hasNext())
			return;
		
		// 非表示処理
		for(var i= 0; i < self.rows().length; i++){
			self.rows()[i].display(false);
		}
		
		// オフセット更新
		self.offset(self.offset() + parseInt(self.rowLimit(), 10));

		// 全チェックを初期化
		self.isAllReadable(false);
		self.isAllWritable(false);

		// 読込み
		self.loadPermission();
	};

	// ページング処理(prev)
	self.prevList = function() {
		if (!self.hasPrev())
			return;
		
		// 非表示処理
		for(var i= 0; i < self.rows().length; i++){
			self.rows()[i].display(false);
		}
		
		// オフセット更新
		self.offset(Math.max(0, self.offset() - parseInt(self.rowLimit(), 10)));

		// 全チェックを初期化
		self.isAllReadable(false);
		self.isAllWritable(false);

		// 読込み
		self.loadPermission();
	};
};
