
var Field = function(data, parent, selected) {
	var self = this;
	self.parent = parent;
	self.load(data, selected);
};


Field.prototype.load = function (data, selected) {
	var self = this;
	var parent = this.parent;
	self.id = data.id;
	self.label = ko.observable(data.label);
	self.type = data.type;
	self.selected = ko.observable(selected);
	self.baseItemDisabled = ko.computed(function () { return !(self.selected()); });
};

ko.bindingHandlers.viewColumn = {
	init: function(_element, valueAccessor, allBindings, viewModel, bindingContext) {
//		var value = ko.utils.unwrapObservable(valueAccessor());
		var element = $(_element).find('.tx-field-container').get(0);
		var cover = $('<div class="tx-field-cover"></div>');
		var html =
			'<div class="tx-field-controls">\
				<div class="tx-field-config-menu">\
					<div class="btn-group">\
						<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">\
							設定 <span class="caret"></span>\
						</button>\
						<ul class="dropdown-menu pull-right" role="menu">\
							<li class="tx-field-open"><a href="#">開く</a></li>\
							<li class="tx-field-delete"><a href="#">削除</a></li>\
						</ul>\
					</div>\
				</div>\
			</div>';

		cover.click(function () {
			$('.tx-field-controls').remove();
			$('.tx-field-cover').removeClass('tx-field-cover-active');
			var controls = $(html);
			controls.find('.tx-field-open a').click(function() {
				viewModel.parent.settingModal.open(viewModel, viewModel.parent.viewColumnsSettings[viewModel.id] || {});
				return false;
			});
			controls.find('.tx-field-delete a').click(function() {
				bindingContext.$parent.viewColumns.remove(valueAccessor());
				valueAccessor().selected(false);
				if (bindingContext.$parent.viewColumns().length == 0) { // insert dummy column
					bindingContext.$parent.viewColumns.push(new Field({id: null, type: null, label: null}, self, false));
				}
				return false;
			});

			$(element).append(controls);
			$(element).find('.tx-field-cover').addClass('tx-field-cover-active');
		});

		$(element).append(cover);
	}
};


var SettingModal  = function(callback) {
	var self = this;
	self.errorMessage = ko.observable(null);
	self.columnWidth = ko.observable(null);
	self.callback = callback;
	self.field = null;
};

SettingModal.prototype.open = function(field, settings) {
	var self = this;
	self.field = field;
	self.columnWidth((settings['width'] || '').toString());
	self.errorMessage(null);
	$('#setting-modal').modal();
};
SettingModal.prototype.close = function() {
	$('#setting-modal').modal('hide');
};
SettingModal.prototype.update = function() {
	var self = this, columnWidth = self.columnWidth();
	self.errorMessage(null);
	if (columnWidth != null && columnWidth != '' && !columnWidth.match(/^[0-9]+$/)) {
		self.errorMessage("数値で入力してください");
		return;
	}
	self.callback(self.field, {width: parseInt(columnWidth, 10)});
	self.close();
};



var ViewModel  = function(tableId) {
	var self = this;
	self.tableId = tableId;
	self.viewColumns = ko.observableArray();
	self.viewColumnsSettings = {};	// { <Column Id> : { width: <width> }}
	self.fields = [];
	self.baseItems = ko.observableArray();
	self.error = ko.observable(null);
	self.settingModal = new SettingModal(function(field, settings) {
		self.viewColumnsSettings[field.id] = settings;
	});
	self.load();
	
	self.sortableOptions = {
		start: function(event, ui) {
			$('.tx-field-cover').removeClass('tx-field-cover-active');
			$('.tx-field-controls').remove();
			ui.item.find('.tx-field-cover').addClass('tx-field-cover-active');
			ui.placeholder.height(ui.item.height());
			ui.helper.find('.tx-field-holder').width(ui.item.width()); // thの幅
		},
		stop : function(event, ui) {
			ui.item.find('.tx-field-cover').removeClass('tx-field-cover-active');
		},
		over : function(event, ui) {
			ui.helper.css('z-index', 1000000);
		},
		items: ">*:not(.tx-sort-disabled)",
		// http://stackoverflow.com/questions/2150002/jquery-ui-sortable-how-can-i-change-the-appearance-of-the-placeholder-object
//			placeholder: 'tx-sortable-placeholder',
		placeholder: {
			element: function(currentItem) {
				return $('<tr><td class="tx-sortable-placeholder"></td></tr>');
			},
			update: function(container, p) {
				return;
			}
		},
		tolerance: 'pointer'
	};

	self.draggableOptions = {
		start : function(event, ui) {
			ui.helper.css('z-index', 1000000);
		},
		appendTo: 'body'
	};
};

ViewModel.prototype.load = function() {
	var self = this;
	$.when(
		$.ajax({url: '/my/table/columns.json', data: {t:self.tableId}, dataType: 'json'}),
		$.ajax({url: '/my/table/view.json', data: {t:self.tableId}, dataType: 'json'})
	).done(function (result1, result2) {self.render(result1, result2);});
};

ViewModel.prototype.render = function(result1, result2) {
	var self = this;
	var json = result1[0];
	
	var fields = result1[0].result;
	var viewColumns = result2[0].result;

	self.fields = $.map(fields, function(data) {
		if (data.isListable) {
			for (var i=0; i<viewColumns.length; i++) {
				if (viewColumns[i].column.id == data.id) {
					return new Field(data, self, true);
				}
			}
			return new Field(data, self, false);
		}
		return null;
	});
	


	// フォームに設置するアイテム
	for (var i=0; i<viewColumns.length; i++) {
		self.viewColumns.push(self.fieldFor(viewColumns[i].column.id));
		self.viewColumnsSettings[viewColumns[i].column.id] = { width: viewColumns[i].width };
	}
	
	// ダミーの行
	if (self.viewColumns().length == 0) { // insert dummy column
		self.viewColumns.push(new Field({id: null, type: null, label: null}, self, false));
	}

	// アイテムメニュー
	for (var i=0; i<self.fields.length; i++) {
		self.baseItems.push(self.fields[i]);
	}
	
	$('#tx-side-affix').css({position:'relative'});
	var moveSidebar = function() {
		if ($(window).scrollTop() < $('#tx-side').offset().top) {
			$('#tx-side-affix').css({top:0});
		} else if ( ($(window).scrollTop() + $(window).height()) >
				($('#tx-main .panel').offset().top + $('#tx-main .panel').height()) ) {
			$('#tx-side-affix').css({top:$('#tx-main .panel').height() - $('#tx-side-affix').height()});
		} else {
			$('#tx-side-affix').css({top:$(window).scrollTop() - $('#tx-side').offset().top});
		}
	};
	var resizeSidebar = function() {
		$('#tx-side-affix').height( $(window).height() - $('#tx-side').offset().top );
	};
	resizeSidebar();
	moveSidebar()
	$(window).resize(resizeSidebar);
	$(window).scroll(moveSidebar);

	$( ".tx-row" ).disableSelection();
};

ViewModel.prototype.fieldFor = function(id) {
	var fields = this.fields;
	for (var i = 0; i < fields.length; i++) {
		if (fields[i].id == id) {
			return fields[i];
		}
	}
	return null;
}

ViewModel.prototype.serialize = function() {
	var self = this;

	var fieldsData = [];
	for (var i = 0; i < self.viewColumns().length; i++) {
		if (self.viewColumns()[i].type != null) { // ignore dummy column
			//fieldsData.push(self.viewColumns()[i].id);
			fieldsData.push({
				id: self.viewColumns()[i].id,
				width: (self.viewColumnsSettings[self.viewColumns()[i].id] || {})['width']
			});
		}
	}
	
	var data = {
		id: self.tableId,
		viewColumnIds: fieldsData
	};

	return ko.toJSON(data);
};

ViewModel.prototype.save = function() {
	var self = this;
	var data = self.serialize();
	$.post('/my/table/saveView.json', {data: data}, function(json) {
		if (json.status == 'success') {
			document.location.href='/my/table/modify?'+$.param({t:json.result});
		} 
	}, 'json');
};

ViewModel.prototype.afterMove = function (sender) {
	var vm = sender.item.parent;
	var item = sender.item;
	if (sender.sourceParent == null) { // メニューからドラッグされた場合
			sender.item.selected(true);
			var dummy = -1;
			for (var i=0; i<vm.viewColumns().length; i++) {
				if (vm.viewColumns()[i].type == null) {
					dummy = i;
					break;
				}
			}
			if (dummy > -1) {
				vm.viewColumns.splice(dummy, 1); // remove dummy column
			}
	}
};
