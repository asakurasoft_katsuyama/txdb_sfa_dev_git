var ViewModel  = function(tableId) {
	var self = this;
	self._tableId = tableId;
	
	self.drop = function() {
		$.ajax({
			type: 'POST',
			url: '/my/table/drop.json',
			data: {t:self._tableId}, 
			success: function(json) 
			{
				if (json.status == 'success') {
					document.location.href="/my/table/";
				} else if (json.status == 'error') {
					alert($.map(json.errors, function (error){return error.message;}).join("\n"));
				}
			}, 
			dataType: 'json'
		});
	};
	
	self.release = function() {
		$.ajax({
			type: 'POST',
			url: '/my/table/release.json', 
			data: {t:self._tableId}, 
			success: function(json) 
			{
				if (json.status == 'success') {
					document.location.href="/my/table/modify?"+$.param({t:json.result});
				} else if (json.status == 'error') {
					alert($.map(json.errors, function (error){return error.message;}).join("\n"));
				}
			}, 
			dataType: 'json'
		});
	};
	
	self.rollback = function() {
		$.ajax({
			type: 'POST',
			url: '/my/table/rollback.json',
			data: {t:self._tableId}, 
			success: function(json) 
			{
				if (json.status == 'success') {
					document.location.href="/my/table/modify?"+$.param({t:json.result});
				} else if (json.status == 'error') {
					alert($.map(json.errors, function (error){return error.message;}).join("\n"));
				}
			},
			dataType: 'json'
		});
	};
	
	self._load = function() {
	};
	
	self._load();

};

