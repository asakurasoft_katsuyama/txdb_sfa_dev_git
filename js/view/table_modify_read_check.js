var OptionGroup = Class.extend({
	init: function (label, children) {
		this.label = label;
		this.children = children;
	}
});

var ViewModel = Class.extend({
	init: function (tableId) {
		var self = this;
		self._tableId = tableId;

		self.isReadCheckEnabled = ko.observable(false);
		self.isOnlistEnabled = ko.observable(false);
		self.isAutoreadEnabled = ko.observable(false);
		self.targetType = ko.observable('all');
		self.userGroupOptions = ko.observableArray([]);
		self.checkTargets = ko.observableArray([]);
		self.errors = ko.observableArray([]);


		self.load();
	},

	load: function () {
		var self = this;
		$.when(
		  $.getJSON('/my/api/v1/get_table.json', {id: self._tableId, revision: 'trunk'}),
		  $.getJSON('/my/api/v1/get_users.json'),
		  $.getJSON('/my/api/v1/get_groups.json')
		).done(function (res1, res2, res3) {
			  var options = self.createCompositeOptions(res2[0].data.rows, res3[0].data.rows);
			  self.userGroupOptions(options);

			  var tableJson = res1[0].data;
			  var readCheckSettings = tableJson['readCheckSettings'];
			  if (readCheckSettings) {
				  self.isReadCheckEnabled(readCheckSettings.isReadCheckEnabled);
				  self.isOnlistEnabled(readCheckSettings.isOnlistEnabled);
				  self.isAutoreadEnabled(readCheckSettings.isAutoreadEnabled);
				  self.targetType(readCheckSettings.checkTargets.length == 0 ? 'all' : 'user-specified');
				  self.checkTargets($.map(readCheckSettings.checkTargets, function(val) {
					  return (val ? (val.type+":"+(val.id===null ? '':val.id)) : null);
				  }));
			  }
			  self.isReadCheckEnabled(tableJson.isReadCheckEnabled);
		  });
	},
	save: function() {
		var self = this;
		self.errors([]);

		var data = {
			id: self._tableId,
			isReadCheckEnabled: self.isReadCheckEnabled(),
			isOnlistEnabled: self.isOnlistEnabled(),
			isAutoreadEnabled: self.isAutoreadEnabled(),
			targetType: self.targetType(),
			checkTargets: ko.toJSON(self.convertUserGroup(self.checkTargets()))
		};
		$.post('/my/table/save_read_check_settings.json', data,
			function(json) {
				if (json.status == 'success') {
					document.location.href="/my/table/modify?"+$.param({t:json.result});
				} else if (json.status == 'error') {
					self.errors(json.errors);
				} else {
					throw json.status;
				}
			}
		);
	},
	convertUserGroup: function(userGroups) {
		return $.map(userGroups, function(t) {
			var parts = t.split(':');
			return { type:parts[0], id:parseInt(parts[1], 10) };
		});
	},
	createCompositeOptions: function (users, groups) {
		var wrapId = function (options, type) {
			var newOptions = [];
			for (var i = 0; i < options.length; i++) {
				newOptions.push({
					id: type + ":" + options[i].id,
					name: options[i]['screenName'] || options[i].name
				});
			}
			return newOptions;
		};
		return [
			new OptionGroup('グループ', wrapId(groups, "group")),
			new OptionGroup('ユーザー', wrapId(users, "user"))
		];
	}
});
