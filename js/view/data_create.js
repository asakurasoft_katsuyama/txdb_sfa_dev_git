
var Field = Class.extend({
	init: function(data, parent){
		var self = this,
			subscribes = [ 'text', 'number', 'date', 'time', 'datetime', 'link' ];
		//self._data = data;
		self.parent = parent;
		self.id = data.column.id;
		self.code = data.column.code;
		self.isInputable = data.column.isInputable;
		self.isAutofill = data.column.isAutofill;
		self.type = data.column.type;
		self.label = data.column.label;
		self.labelHtml = data.column.labelHtml || null;
		self.minLength = data.column.minLength || null;
		self.maxLength = data.column.maxLength || null;
		self.minValue = data.column.minValue || null;
		self.maxValue = data.column.maxValue || null;
		self.expression = data.column.expression || null;
		self.sizeWidth = data.column.sizeWidth || null;
		self.sizeHeight = data.column.sizeHeight || null;
		self.showsField = data.column.showsField || false;
		self.hasOptions = data.column.hasOptions;
		self.showsLabel = data.column.showsLabel || false;
		self.isRequired = data.column.isRequired || false;
		self.optionsAlignment = data.column.optionsAlignment || null;
		self.referenceCopyMapIds = data.column.referenceCopyMapIds || [];
		self.referenceTable = data.column.referenceTable || null;
		self.position = { x:data.column.positionX || 1, y:data.column.positionY || 1 };
		self.hintText = this.getHintText();
		self.disabled = ko.observable(false);

		if (self.type == 'user_selector') {
			self.value = ko.observableArray($.map(data.value, function(user) { return user.id; }));
		} else if (self.type == 'checkbox') {
			self.value = ko.observableArray($.map(data.value, function(option) { return option.id.toString(); }));
		} else if (self.type == 'dropdown' ||
				self.type == 'radio') {
			self.value = ko.observable(data.value ? data.value.id : null);
		} else if (self.type == 'file') {
		} else if (self.type == 'lookup') {
			self.value = ko.observable(data.value ? data.value.value : null);
			self.key = ko.observable(data.value ? data.value.key : null);
		} else if (self.type == 'relational_input') {
			self.value = ko.observableArray([]);
		} else {
			self.value = ko.observable(data.value);
		}
		if (self.expression == null &&
				subscribes.indexOf(self.type) !== -1) {
			self.value.subscribe(function(newValue) {
				parent.calculate(self);
			});
		}
		self.text = ko.observable(data.text);
		self.html = ko.observable(data.html);
		self.error = ko.observable(null);
		//self.referalError = ko.observable(false);	// Autholity error.
		self.message = ko.observable(null);

		if (self.type == 'dropdown' && !self.isRequired) {
			data.column.options.unshift({id:'', name:''});
		}
		self.options = data.column.options ? ko.observableArray(data.column.options) : null;
		self.layoutParentId = data.column.layoutParentId || null;
		self.matrix = ko.observableArray([]); // for tab
		self.tabs = ko.observableArray([]); // for tab_group
		self.activeTab = ko.observable(null); // for tab_group
		self.tabs.subscribe(function(tabs){
			if (tabs.length > 0) {
				self.activeTab(tabs[0]);
			}
		});
		self.isRendered = ko.observable(false);
	},
	idName: function() {
		return this.parent.formName + '_col_' + this.id;
	},
	modalName: function(suffix) {
		return this.parent.formName + 'modal_' + this.id + (suffix || '');
	},
	getHintText: function() {
		var hintText = "";
		if (this.minLength != null || this.maxLength != null) {
			if (this.minLength != null)
				hintText += this.minLength + "文字以上";
			if (this.maxLength != null)
				hintText += this.maxLength + "文字以内";
		}
		if (this.minValue != null || this.maxValue != null) {
			if (this.minValue != null)
				hintText += this.minValue + "以上";
			if (this.maxValue != null)
				hintText += this.maxValue + "以下";
		}
		return hintText;
	},
	typeName: function() {
		return 'col_' + this.type;
	},
	serializeValue: function() {
		return { id: this.id, value: this.value() };
	},
	afterRender: function() {
		// Override.
		this.isRendered(true);
	},
	layout: function(allFields) {
		// Override.
	},
	initAfterBinding: function() {
		// Override.
	},
	validate: function() {
		// Override.
		return true;
	},
	propagateError: function() {
		// Override.
	},
	getNestedRows: function() {
		// Override.
		return [];
	}
});

var RelationalField = Field.extend({
	init: function(data, parent){
		this._super(data, parent);
		this.list = new ListModel();
		this.aggregateList = new ListModel();
		this.list.load = this.loadList.bind(this);
	},
	loadList: function() {
		var params = {
			t:this.parent.parent.tableId,
			c:this.id,
			row_id: this.parent.rowId,
			data: ko.toJSON(this.parent.serializeFields()),
			offset: this.list.offset()
		};
		if (this.parent.parent.readOnly) {
			$.post('/my/table/search_relational.json', params, this.renderList.bind(this), 'json');
		} else {
			$.post('/my/data/search_relational.json', params, this.renderList.bind(this), 'json');
		}
	},
	renderList: function(json) {
		if (json.status === 'success') {
			this.list.rows(json.result.rows);
			this.list.columns(json.result.columns);
			this.list.offset(json.result.offset);
			this.list.rowCount(json.result.rowCount);
			this.list.rowLimit(json.result.rowLimit);
			this.list.totalRowCount(json.result.totalRowCount);

			this.aggregateList.rows(json.aggregate_result.rows);
			this.aggregateList.columns(json.aggregate_result.columns);
			this.aggregateList.offset(json.aggregate_result.offset);
			this.aggregateList.rowCount(json.aggregate_result.rowCount);
			this.aggregateList.rowLimit(json.aggregate_result.rowLimit);
			this.aggregateList.totalRowCount(json.aggregate_result.totalRowCount);
		} else if (json.status === 'error') {
			this.error(json.errors[0].message);
		}
		this.isRendered(true);
	},
	initAfterBinding: function() {
		this.loadList();
	},
	afterRender: function(elements) {
		// not rendered yet
	}
});

var RelationalInputField = Field.extend({
	init: function(data, parent){
		var self = this;
		this._super(data, parent);
		this.referenceColumns = ko.observableArray(data.column.referenceColumns);
		this.isWritable = ko.observable(
			(
				!self.parent.parent.readOnly
				&& self.referenceTable
				&& self.referenceTable.allows
				&& self.referenceTable.allows.indexOf('insert') >= 0
				&& self.referenceTable.allows.indexOf('delete') >= 0
				&& self.referenceTable.allows.indexOf('update') >= 0
			)
		);
		this.isReadable = ko.observable(
			(
				self.referenceTable
				&& self.referenceTable.allows
				&& self.referenceTable.allows.indexOf('select') >= 0
			)
		);
		this.pendingRows = [];
		this.initRows(data.value);
		this.hasRows = ko.computed(function() {
			for (var i=0; i<self.value().length; i++) {
				var row = self.value()[i];
				if (row.action() != 'delete') {
					return true;
				}
			}
			return false;
		});
	},
	initRows: function(value) {
		this.renderRows(value);
	},
	getRows: function() {
		var params = {
			t:this.parent.getTableId(),
			c:this.id,
			row_id: this.parent.rowId,
			as_value: true
		};
		$.post('/my/data/search_relational_input.json', params,
			this.onGetRows.bind(this), 'json');
	},
	getRowValues: function(row) {
		var values = [];
		var columns = this.referenceColumns();
		for (var i=0; i<columns.length; i++) {
			values.push(row.fieldFor(columns[i].id));
		}
		return values;
	},
	getTemplateName: function(type) {
		if (type == 'multitext') {
			return 'index-multitext-2-tpl';
		} else if (type == 'richtext') {
			return 'index-richtext-2-tpl';
		} else if (type == 'file') {
			return 'index-file-inputting-tpl';
		} else {
			return 'index-' + type + '-tpl';
		}
	},
	onGetRows: function(json) {
		if (json.status === 'success') {
			this.renderRows(json.result);
		} else if (json.status === 'error') {
			this.error(json.errors[0].message);
		}
	},
	renderRows: function(rows) {
		var self = this;
		if (rows === null) {
			this.value([]);
		} else {
			this.value($.map(rows, function(row) {
				return new NestedRowModel(row, self, self.parent.parent);
			}));
		}
		this.isRendered(true);
	},
	initAfterBinding: function() {
		//this.loadInitialList();
	},
	afterRender: function(elements) {
		// not rendered yet
	},
	addRow: function() {
		var row = new NestedRowModel(null, this, this.parent.parent);
		this.pendingRows.push(row);
		var url='/my/data/create_child_row?'
			+$.param({
				t: row.parentField.referenceTable.id,
				row_key: row.internalKey
			});
		this.openChildRowWindow(url, row);
	},
	editRow: function(row) {
		var url='/my/data/edit_child_row?'
			+$.param({
				t: row.parentField.referenceTable.id,
				r: row.rowId,
				row_key: row.internalKey
			});
		this.openChildRowWindow(url, row);
	},
	openChildRowWindow: function(url, row) {
		//var name='tx_data_edit_';// + row.internalKey;
		var name='tx_data_edit_' + row.internalKey;
		var features='menubar=no,location=yes,resizable=yes,scrollbars=yes,status=yes';
		features+=',top=0,left=0';
		var win = window.open(url, name, features);
		row.win = win;
	},
	removeRow: function(row) {
		if (!confirm('データからこの明細を削除しますか?')) {
			return;
		}
		row.action('delete');
		if (!row.rowId) {
			var i = this.value.indexOf(row);
			this.value.splice(i,1);
		}
	},
	onCancelChangeRow: function(row) {
		if (row.action() == 'pending insert') {
			var index = this.pendingRows.indexOf(row);
			this.pendingRows.splice(index, 1);
		}
	},
	onChangeRow: function(row) {
		if (row.action() == 'pending insert') {
			var index = this.pendingRows.indexOf(row);
			this.pendingRows.splice(index, 1);
			row.action('insert');
			this.value.push(row);
		}

		var value = $.map(this.value(), function(row){
			return row.serialize();
		});
		var params = {
			t:this.parent.getTableId(),
			c:this.id,
			data: ko.toJSON(value)
		};
		$.post('/my/data/sort_child_rows.json', params, this.sortRows.bind(this), 'json');
	},
	getRowByKey: function(key) {
		var row = null;
		$.each(this.value(), function(i, _row) {
			if (_row.internalKey == key) {
				row = _row;
				return false;
			}
		});
		return row;
	},
	sortRows: function(json) {
		var self = this;
		if (json.status == 'success') {
			var rows = this.value();
			$.each(json.result, function(i, key) {
				var row = self.getRowByKey(key);
				var index = rows.indexOf(row);
				rows.splice(index, 1);
				rows.push(row);
			});
			this.value(rows);
		}
	},
	serializeValue: function() {	// override
		var value = $.map(this.value(), function(row){
			return row.serialize();
		});
		return { id: this.id, value: value};
	},
	getNestedRows: function() {
		// Override.
		return this.value().concat(this.pendingRows);
	}
});

var CalcField = Field.extend({
	init: function(data, parent){
		this._super(data, parent);
		this.textWithError = ko.computed(function() {
			 var v=this.text();
			 return (v == '' || v == null) ? '#N/A!' : v;
		}, this);
	}
});

var DateCalcField = Field.extend({
	init: function(data, parent){
		this._super(data, parent);
		this.textWithError = ko.computed(function() {
			 var v=this.text();
			 return (v == '' || v == null) ? '#N/A!' : v;
		}, this);
	}
});

var FileField = Field.extend({
	init: function(data, parent){
		this._super(data, parent);
		//if(this.parent.replication){
		//	this.text(null);
		//	this.value = ko.observableArray([]);
		//}else{
			this.value = ko.observableArray(ko.utils.arrayMap(data.value, function(file) {
				file.isSaved = true;
				return file;
			}));
		//}
	},
	removeFile: function(sender) {
		this.value.remove(sender);
	},
	initAfterBinding: function() {
		if (this.parent.parent.readOnly)
			return;
		// https://github.com/blueimp/jQuery-File-Upload/issues/2627
		if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {
			$('#'+this.idName()).removeAttr('multiple');
		}
		$('#'+this.idName()).fileupload({
			dataType: 'json',
			start: function () { $.blockUI(); },
			done: this.renderFileUploaded.bind(this)
		});
	},
	renderFileUploaded: function (e, data) {
    	var field = this;
    	field.error(null);
        $.each(data.result.files, function(index) {
        	if (this.error) {
        		field.error(this.error);
        	} else {
            	field.value.push({
            		fileName : this.originalName,
            		filekey : this.name,
            		contentType: this.type,
            		isSaved : false
            	});
        	}
        });
    }
});
var FilterModalModel = FilterModel.extend({
	init: function(options, parent){
		this._super({ tableId: options.referenceTableId }, parent);
		this.parent = parent;
		this.referenceTableId = options.referenceTableId;
	},
	apply: function() {
		var params = {table_id: this.referenceTableId, criteria: ko.toJSON(this.serialize()), only_validation: true };
		$.post('/my/data/save_criteria.json', params, function(json) {
			if (json.status == 'success') {
				this.parent.criteria = this.serialize();
				this.parent.list.offset(0);
				this.parent.searchList();
				this.close();
			} else if (json.status == 'error') {
				this.errors(json.errors);
			}
		}.bind(this), 'json');
	},
	open: function() {
		$('#'+this.parent.modalName('_filter')).modal('show');
	},
	close: function() {
		$('#'+this.parent.modalName('_filter')).modal('hide');
	}
});
var LookupField = Field.extend({
	init: function(data, parent){
		this._super(data, parent);
		this.parent = parent;
		this.list = new ListModel();
		this.list.load = this.searchList.bind(this);
		this.filterModal = new FilterModalModel({referenceTableId: this.referenceTable.id}, this);
		this.criteria = null;
//		this.backup = data.value ? data.value : { key:null, value:null };
		this.backup = data.value ? data.value.value === '' ? { key:data.value.key, value:null } : data.value : { key:null, value:null };
	},
	renderList: function(json) {
		if (json.status === 'success') {
			this.list.rows(json.result.rows);
			this.list.columns(json.result.columns);
			this.list.offset(json.result.offset);
			this.list.rowCount(json.result.rowCount);
			this.list.rowLimit(json.result.rowLimit);
			this.list.totalRowCount(json.result.totalRowCount);
		} else if (json.status === 'error') {
			this.error(json.errors[0].message);
		}
	},
	// 取得ボタンクリック
	openLookup: function(sender) {
		if (this.parent.parent.readOnly)
			return;
		this.message(null);
		this.error(null);
		if (this.value() == null ||
				this.value() == '') {
			this.searchList();
		} else {
			$.post('/my/data/search_lookup.json',
				{ t:this.parent.parent.tableId, c:this.id, data: ko.toJSON(this.parent.serializeFields()),
					r:this.parent.rowId },
				this.renderLookup.bind(this), 'json');
		}
	},
	renderLookup: function(json) {
		if (json.status == 'success') {
			if (json.result.totalRowCount <= 0) {
				this.error('データがありません。');
			} else if (json.result.totalRowCount === 1) {
				this.selectRow(json.result.rows[0]);
			} else {
				this.renderList(json);
				$('#'+this.modalName()).modal('show');
			}
		} else if (json.status == 'error') {
			this.error(json.errors[0].message);
		}
	},
	searchList: function() {
		var params = { t:this.parent.parent.tableId, c:this.id, offset: this.list.offset(), criteria: ko.toJSON(this.criteria) };
		$.post('/my/data/search_lookup_by.json', params,
			function(json) {
				this.renderList(json);
				if (json.status == 'success') {
					$('#'+this.modalName()).modal('show');
				}
			}.bind(this), 'json');
	},
	// 一覧から「選択」をクリック
	selectRow: function(sender, _options) {//sender=row
		var options = _options || {};
		var self = this;
		self.message(null);
		$.post('/my/data/get_lookup_row.json', {t:this.parent.parent.tableId, c:this.id, r: sender.id }, function(json) {
			$('#'+this.modalName()).modal('hide');
			if ($.isPlainObject(json.result.value)) {
				this.value(json.result.value.value ? json.result.value.value.toString() : null);
			} else {
				this.value(json.result.value);
			}
			this.key(json.result.id);
			this.backup = { key:this.key(), value:this.value() };
			$.each(json.result.copyValues, function(i, data) {
				var field = self.parent.fieldFor(data.id);
				if (field.hasOptions) {
					field.value(data.value ? data.value.id : null);
				} else if ($.isPlainObject(data.value)) {
					field.value(data.value.value ? data.value.value.toString() : null);
				} else {
					field.value(data.value ? data.value.toString() : null);
				}
			});
			this.message("参照先からデータをコピーしました。");
			if ('success' in options) {
				options.success();
			}
		}.bind(this));
	},
	// 「抽出条件」ボタンクリック
	openFilter: function() {
		this.filterModal.open();
	},
	// 「クリア」ボタンクリック
	clearLookup: function(sender) {
		if (this.parent.parent.readOnly)
			return;
		this.message(null);
		this.error(null);
		this.value(null);
		this.key(null);
		this.backup = { key:null, value:null };
		ko.utils.arrayForEach(this.referenceCopyMapIds, function(columns) {
			this.parent.fieldFor(columns[1]).value("");
		}.bind(this));
	},
	serializeValue: function() {	// override
		return { id: this.id, value: this.value(), key: this.key() };
	},
	initAfterBinding: function() {
		// Disable copyee fields.
		$.each(this.referenceCopyMapIds, function(i, pair) {
			this.parent.fieldFor(pair[1]).disabled(true);
		}.bind(this));
	},
	validate: function () {
		var value = this.value() === '' ? null : this.value();
		if ( this.backup.key !== this.key() || this.backup.value !== value ) {
			this.error("データが取得されていません。");
			return false;
		}
		return true;
	},
	_lookup: function(_options) { // for javascript interface
		var options = _options || {};
		var self = this;
		if (this.parent.parent.readOnly)
			return;
		this.message(null);
		this.error(null);
		if (this.value() == null ||
				this.value() == '') {
			return;
		} else {
			$.post('/my/data/search_lookup.json',
				{ t:this.parent.parent.tableId, c:this.id, data: ko.toJSON(this.parent.serializeFields()),
					r:this.parent.rowId },
				function(json) {self._renderLookup(json, options);},
				'json');
		}
	},
	_renderLookup: function(json, options) { // for javascript interface
		if (json.status == 'success') {
			if (json.result.totalRowCount <= 0) {
				this.error('データがありません。');
			} else if (json.result.totalRowCount === 1) {
				this.selectRow(json.result.rows[0], options);
			} else {
				this.error('確定されていません。');
			}
		} else if (json.status == 'error') {
			this.error(json.errors[0].message);
		}
	}
});

var RichtextField = Field.extend({
	validate: function () {
		//var colId = 'col_' + this.id;
		if (!CKEDITOR.env.isCompatible) return true;
		var colId = this.idName();
		if(CKEDITOR.instances[colId].mode !== 'wysiwyg'){
			this.error("ソース表示を解除してください。");
			return false;
		}
		this.value(CKEDITOR.instances[colId].getData());
		return true;
	},
	afterRender: function(elements) {
		var self = this;
		if (!CKEDITOR.env.isCompatible) {
			self.isRendered(true);
			return;
		}
		$.each(elements, function(i, element){
			if ($(element).hasClass('field-richtext')) {
				$(element).on('instanceReady.ckeditor', function() {
					self.isRendered(true);
				});
				return false;
			}
		});
	}
});

var TabGroupField = Field.extend({
	layout: function (allFields) {
		var self = this;
		var fields = [], maxTabs = 1, tabs = [];
		$.each(allFields, function(i, field) {
			if (field.layoutParentId == self.id) {
				fields.push(field);
			}
		});
		$.each(fields, function(i, field) {
			if (field.showsField) {
				col = field.position.x;
				maxTabs = Math.max(maxTabs, col);
			}
		});
		// Sort by position.
		fields.sort(function(a, b) {
			if (a.position.x < b.position.x) return -1;
			if (a.position.x > b.position.x) return 1;
		});
		$.each(fields, function(i, field) {
			tabs.push(field);
		});
		self.tabs(tabs);
	}
});

var TabField = Field.extend({
	propagateError: function() {
		var errorColumnId = this.parent.errorColumnId();
		if (!errorColumnId) return;
		for (var i=0; i<this.matrix().length; i++) {
			for (var j=0; j<this.matrix()[i].length; j++) {
				if (this.matrix()[i][j].field.id == errorColumnId) {
					//console.log('error in tab!');
					this.error('タブ ' + this.label + ' 内のフィールドにエラーがあります。');
				}
			}
		}
	},
	layout: function (allFields) {
		var self = this;
		var fields = [], maxCols = 1, maxRows = 1, matrix = [], unit, result;
		$.each(allFields, function(i, field) {
			if (field.layoutParentId == self.id) {
				fields.push(field);
			}
		});
		result = txdb.maxColsRows(fields, false);
		fields = result.fields;
		maxCols = result.maxCols;
		maxRows = result.maxRows;
		for (var i = 0; i < maxRows; i++)
			matrix[i] = [];
		unit = Math.floor(12/maxCols);
		$.each(fields, function(i, field) {
			var span = 12;
			if (field.sizeWidth) {
				span = field.sizeWidth * unit;
			}
			matrix[field.position.y - 1].push({ span: span ,field: field });
		});
		self.matrix(matrix);
	}
});

var OptionGroup = Field.extend({
	init: function(label, children) {
		this.label = ko.observable(label);
		this.children = ko.observableArray(children);
	}
});
var UserSelectorField = Field.extend({
	init: function(data, parent){
		this._super(data, parent);
		this.userOptions = ko.observableArray([]);
		this.allUserOptions = [];
		this.allGroupOptions = [];
		this.initialValue = this.value();

	},
	initAfterBinding: function() {
		var self = this;
		if (this.parent.parent.readOnly) {
			return;
		}

		$.when(
			$.ajax('/my/data/user_options.json'),
			$.ajax('/my/data/group_options.json')
		).done(function(res1, res2) {
			self.allUserOptions = res1.shift().result;
			self.allGroupOptions = res2.shift().result;
			self._updateUserOptions();
			self.value(self.initialValue);	// Delay set.
			self.isRendered(true);
		});
	},
	_mergeOptions: function(optionsToAdd) {
		var self = this;

		var currentIds = this.value();
		for (var i = 0; i < optionsToAdd.length; i++) {
			var found = false;
			for (var j=0; j < currentIds.length; j++) {
				if (optionsToAdd[i].id == currentIds[j]) {
					found = true;
				}
			}
			if (!found) {
				self.value.push(optionsToAdd[i].id);
			}
		}
	},
	_updateUserOptions: function() {
		var options = [];

		options.push(new OptionGroup('ユーザー', this.allUserOptions));

		// Groups
		var filtered = [];
		for (var k = 0; k < this.allGroupOptions.length; k++) {
			if (this.allGroupOptions[k].id.toString().indexOf('group') === -1)
				this.allGroupOptions[k].id = 'group:'+this.allGroupOptions[k].id;
			filtered.push(this.allGroupOptions[k]);
		}
		options.push(new OptionGroup('簡易ユーザー選択', filtered));

		this.userOptions(options);
	},
	selected: function(x, ev) {
		var self = this;
		var parts = ev.val.split(':');
		if (parts[0] == 'group') {
			$.get('/my/data/group_users.json', { group_id: parts[1] })
				.done(function(json) {
					self._mergeOptions(json.result);
				});

			$(ev.target).select2('close');
			return false;
		} else {
			return true;
		}
	},
	afterRender: function(elements) {
		// not rendered yet
	}
});

// コピーして作成
// プレビュー
var ViewModel = Class.extend({
	init: function(options){
		var self = this;
		self.ajaxLoading = ko.observable(false);
		self.formSequence =0;
		//self.preview = (options['preview'] || false);
		self.tableId = options.tableId;
		self.readOnly = options.readOnly || false;	// テーブル編集のプレビューから呼ばれるとtrue
		//self.version = null;
		//self.rowId = options['rowId'] || null;	// Sets when edit,
		self.mode = options['preview'] == true ? 'preview' : (!options['replication'] && options['rowId'] != null ? 'edit' : 'new');
		self.offset = options['offset'] || 0;	// Sets in edit,
		self.replication = options['replication'] || false;	// Sets when replicate,
		self.isChild = options['nested'] || false;
		//self.fields = ko.observableArray([]);
		//self.matrix = ko.observableArray([]);
		//self.error = ko.observable(null);	// Global error.
		self.options = options;
		//var firstModel = new FormModel(null, self.options, self.formSequence++, this);
		self.forms = ko.observableArray([ ]);

		self.load();
	},
	load: function() {
		var self = this;
		if (self.readOnly) {
			$.getJSON('/my/table/fields.json?', $.param({t:self.tableId, r:self.rowId}))
			  .done(self.renderFirstForm.bind(this));
		} else if (self.options['replication']) {
			$.getJSON('/my/data/fields_from.json?',
			  $.param({table_id:self.tableId, row_id:self.options['rowId'] }))
				.done(self.renderFirstForm.bind(this));
		} else if (self.isChild) {
			var target = self.getRowByInternalKey(
				self.options['rowKey'], window);
			$.blockUI();
			var params = {
				table_id: self.tableId,
				data:ko.toJSON(target.serializeFields()),
				row_id: target.rowId
			};
			$.post('/my/data/child_fields_with.json',
				params).done(self.renderFirstForm.bind(this));
		} else {
			var queries = $.parseQuery();
			$.getJSON('/my/data/fields.json?',
				$.param({t:self.tableId, r:self.options['rowId'],
					recursive: true, intent:(queries['intent'] || []) })
			).done(self.renderFirstForm.bind(this));
		}
	},
	getRowByInternalKey: function(rowKey, subWin) { // Called from a sub window.
		return subWin.opener.viewModel.getNestedRow(rowKey);
	},
	getNestedRow: function(rowKey) {
		for (var i=0; i<this.forms().length; i++) {
			var form = this.forms()[i];
			for (var j=0; j<form.fields().length; j++) {
				var field = form.fields()[j];
				var rows = field.getNestedRows();
				for (var k=0; k<rows.length; k++) {
					var row = rows[k];
					if (row.internalKey && row.internalKey == rowKey) {
						return row;
					}
				}
			}
		}
		return null;
	},
	renderFirstForm: function(data) {
		var self = this;
		var firstModel = new FormModel(data, { }, self.formSequence++, this, self.replication);
		self.forms.push(firstModel);
	},
	save: function() {
		var self = this;
		var hasError = false;
		$.each(self.forms(), function(formIndex, form) {
			// Clear errors and messages.
			form.error(null);
			form.errorColumnId(null);
			$.each(form.fields(), function(i, field) {
				field.error(null);
				field.message(null);
			});
			$.each(form.fields(), function(i, field) {
				if(!field.validate()) {
					hasError = true;
					form.error(field.label + "に入力エラーがあります。");
					form.errorColumnId(field.id);
					self.scroll(formIndex);
				}
			});
			form.propagateError();
		});

		if (hasError) return;

		$.blockUI();
		var data = $.map(self.forms(), function(form) {
			return form.serialize();
		});

		if (self.isChild) {
			var row = null;
			try {
				row = self.getRowByInternalKey(
					self.options['rowKey'], window);
			} catch (e) {
				alert('保存できません。\n※親データが閉じられたか、開きなおされた可能性があります。このページを閉じてください。保存する場合は、親データの入力ページから再度入力してください。');
				return;
			}
			$.post('/my/data/updateChild.json', {
				table_id: self.tableId, data:  ko.toJSON(data) },
				function(json) {
					if (json.status == 'success') {
						row.saveWork(json.result);
					} else if (json.status == 'error') {
						$.each(json.errors, function(i, error) {
							if (error.context.index == null) // 通常ありえない
								error.context.index = 0;
							self.forms()[error.context.index].error(error.message);
							self.forms()[error.context.index].errorColumnId(error.context.columnId);
						});
						$.each(self.forms(), function(i, form) {
							form.propagateError();
						});
						if (json.errors.length > 0) {
							self.scroll(json.errors[0].context.index);
						}

				}
			}, 'json');
			return;
		}

		$.post('/my/data/update.json', {
			table_id: self.tableId, data:  ko.toJSON(data) },
			function(json) {
				if (json.status == 'success') {
					document.location.href='/my/data/view?'+$.param({r:json.result[0], t:self.tableId, offset: self.offset});
				} else if (json.status == 'error') {
					$.each(json.errors, function(i, error) {
						if (error.context.index == null) // 通常ありえない
							error.context.index = 0;
						self.forms()[error.context.index].error(error.message);
						self.forms()[error.context.index].errorColumnId(error.context.columnId);
					});
					$.each(self.forms(), function(i, form) {
						form.propagateError();
					});
					if (json.errors.length > 0) {
						self.scroll(json.errors[0].context.index);
					}

			}
		}, 'json');
	},
	scroll: function(formIndex) {
		window.setTimeout(function() {
			var p = Math.max($('[data-target="form"]').eq(formIndex).offset().top
			- 100/*ナビバーの高さ*/, 0);
			$('html,body').animate({ scrollTop: p });
		}, 100);
	},
	cancel: function() {
		if (this.isChild) {
			var target = null;
			try {
				target = this.getRowByInternalKey(
					this.options['rowKey'], window);
			} catch (e) {
				alert('親データが閉じられたか、開きなおされた可能性があります。このページを閉じてください。');
				return;
			}
			target.cancelWork();
			return;
		}
		history.back();
	},
	downloadFile: function(file, ev) {
		document.location.href = file.url + '/download/' + encodeURIComponent(file.fileName);
	},
	namedFileUrl: function(file) {
		return file.url + '/raw/' + encodeURIComponent(file.fileName);
	},
	addRow: function(target) {
		var self = this,
		  queries = $.parseQuery();
		$.getJSON('/my/data/fields.json?',
			$.param({t:self.tableId, intent:(queries['intent'] || []) })).done(function(json) {
				var index = self.forms().indexOf(target);
				self.forms.splice(index + 1, 0, new FormModel(json, [],
					self.formSequence++, self));
				self.scroll(index + 1);
			});
	},
	removeRow: function(target) {
		var self = this;
		if (this.forms().length > 1) {
			var index = self.forms().indexOf(target);
			this.forms.remove(target);
			self.scroll(Math.max(index - 1, 0));
		}
	},
	addCopyRow: function(target) {
		var self = this;
		$.blockUI();
		var params = {table_id: self.tableId, data:ko.toJSON(target.serializeFields()) };
		$.post('/my/data/fields_with.json',
			params)
			.done(function(data) {
				var index = self.forms().indexOf(target);
				self.forms.splice(index + 1, 0, new FormModel(data, self.options,
					self.formSequence++, self, true));
				self.scroll(index + 1);
			});
	}
});
var FormModel = Class.extend({
	init: function(data, options, sequence, parent, isCopy){
		var self = this;
		self.parent = parent;
		self.version = null;
		self.rowId = null;
		self.rowNo = null;
		self.replication = options['replication'] || false;	// Sets when replicate,
		self.isCopy = isCopy || false;	// Sets when copy from a row/form.
		self.fields = ko.observableArray([]);
		self.matrix = ko.observableArray([]);
		self.formName = 'form_'+sequence;
		self.error = ko.observable(null);	// Global error.
		self.errorColumnId = ko.observable(null);	// Origin of global error.
		self.propagateError = function () {
			$.each(self.fields(), function(i, field){
				field.propagateError();
			});
		};
		self.load = function() {
			if (data !== null) {
				window.setTimeout(self.render.bind(self, data));
				return;
			}
			$.blockUI();
			//if (parent.readOnly) {
			//	$.getJSON('/my/table/fields.json?', $.param({t:self.tableId, r:self.rowId}), self.render, 'json');
			//} else {
			//	$.getJSON('/my/data/fields.json?', $.param({t:self.tableId, r:self.rowId}), self.render, 'json');
			//}
			$.getJSON('/my/data/fields.json?', $.param({t:self.tableId, r:self.rowId}), self.render, 'json');
		};

		self.render = function(json) {
			self.rowId = json.result.id;
			self.version = json.result.version;
			self.rowNo = json.result.no;

			var fields = [], maxCols = 1, maxRows = 1, matrix = [], unit, result;
			$.each(json.result.values, function(i, data) {
				var field = self.createField(data, self);
				if (field.showsField) {
					fields.push(field);
				}
			});
			result = txdb.maxColsRows(fields, true);
			fields = result.fields;
			maxCols = result.maxCols;
			maxRows = result.maxRows;
			for (var i = 0; i < maxRows; i++)
				matrix[i] = [];
			unit = Math.floor(12/maxCols);
			$.each(fields, function(i, field) {
				if (field.layoutParentId) {
					return;
				}
				var span = 12;
				if (field.sizeWidth) {
					span = field.sizeWidth * unit;
				}
				matrix[field.position.y - 1].push({ span: span ,field: field });
			});

			$.each(fields, function(i, field) {
				field.layout(fields);
			});

			self.fields(fields);
			self.matrix(matrix);

			// Render relational table.
			$.each(fields, function(i, field) {
				field.initAfterBinding();
			});

			var interval = window.setInterval(
				function() {
					var isRendered = true;
					$.each(self.fields(), function(i, field) {
						if (field.isRendered() == false) {
							isRendered = false;
							return false;
						}
					});
					if (isRendered) {
						window.clearInterval(interval);
						var eventType = null;
						if (self.parent.readOnly) {
							eventType = null;
						} else if (self.parent.replication) {
							eventType = 'load.create.row.table';
						} else if (self.rowId == null) {
							eventType = 'load.create.row.table';
						} else if (self.rowId) {
							eventType = 'load.update.row.table';

						}

						if (eventType) {
							txdb.trigger(eventType, {
								row: new RowInterface(self)
							});
						}
					}
				}, 500
			);
		};
		self.load();
		if(self.replication){
			self.rowId = null;
			self.rowNo = null;
		}
	},
	getTableId: function() {
		return this.parent.tableId;
	},
	calculate: function(sender, _options) {
		var handleSuccess = null;
		var options = _options || {};
		if ('success' in options) {
			handleSuccess = options.success;
		}
		var self = this;
		if (this.parent.readOnly)
			return;
		$.post('/my/data/calculate.json', {
			table_id: self.parent.tableId,
			row_id: self.rowId, data: ko.toJSON(self.serializeFields()) }, function(json) {
			$.each(json.result, function(i, obj) {
				var field = self.fieldFor(obj.id);
				field.value(obj.value);
				field.text(obj.text);
			});
			$.each(self.fields(), function(i, field) {
				if (field.type == 'relational') {
					field.list.offset(0);
					field.list.load();
				}
			});
			if (handleSuccess !== null) {
				handleSuccess();
			}
		}, 'json');
	},
	downloadFile: function(file, ev) {
		document.location.href = file.url + '/download/' + encodeURIComponent(file.fileName);
	},
	namedFileUrl: function(file) {
		return file.url + '/raw/' + encodeURIComponent(file.fileName);
	},
	serialize: function() {
		var self = this;
		return {
			rowId: self.rowId,
			version: self.version,
			values: self.serializeFields()
		};
	},
	serializeFields: function() {
		return $.map(this.fields(), function(field) {
			return field.serializeValue();
		});
	},
	fieldFor: function(id) {
		var fields = this.fields();
		for (var i = 0; i < fields.length; i++) {
			if (fields[i].id == id) {
				return fields[i];
			}
		}
		return null;
	},
	createField: function(data, parent) {
		return new ({
			'file': FileField,
			'lookup': LookupField,
			'calc': CalcField,
			'datecalc': DateCalcField,
			'relational': RelationalField,
			'relational_input': RelationalInputField,
			'lookup': LookupField,
			'richtext': RichtextField,
			'user_selector': UserSelectorField,
			'tab_group': TabGroupField,
			'tab': TabField
		}[data.column.type] || Field)(data, parent);
	},
	tplName: function(data) {
		if (this.type == 'text' && this.expression != null) {
			return this.type + '-expression-tpl';
		} else {
			return this.type + '-tpl';
		}
	}
});
var NestedRowModel = Class.extend({
	init: function(data, field, vm){
		var self = this;
		self.win = null;
		self.parent = vm;
		self.parentField = field;
		self.version = null;
		self.rowId = null;
		self.internalKey = self.tempKey();
		// action options : null | insert | update | create | pending insert
		// 'pending insert' is not sent to server.
		self.action = ko.observable(null);
		//self.replication = options['replication'] || false;	// Sets when replicate,
		self.fields = ko.observableArray([]);
		self.error = ko.observable(null);	// Global error.
		self.load = function() {
			if (data !== null) {
				//window.setTimeout(self.render.bind(self, data));
				self.renderRow(data);
				return;
			}
			$.getJSON('/my/data/fields.json?',
				$.param({t:self.getTableId(), r:self.rowId}),
				self.render, 'json');
		};

		self.render = function(json) {
			json.result.action = 'pending insert';
			self.renderRow(json.result);
		};
		self.renderRow = function (row) {
			self.rowId = row.id;
			self.version = row.version;
			self.action(row.action || null);

			var fields = [];
			$.each(row.values, function(i, data) {
				var field = self.createField(data, self);
				fields.push(field); // includes autofilled field
			});
			self.fields(fields);
		}
		self.load();
		//if(self.replication){
		//	self.rowId = null;
		//}
	},
	tempKey: function() {
		function s4() {
			return Math.floor((1 + Math.random()) * 0x10000)
				.toString(16)
				.substring(1);
		}
		return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
			s4() + '-' + s4() + s4() + s4();
	},
	getTableId: function () {
		return this.parentField.referenceTable.id;
	},
	updateFields: function(row) {
		var self = this;
		var fields = [];
		$.each(self.fields(), function(i, field) {
			if (field.isAutofill) {
				fields.push(field);
			}
		});
		$.each(row.values, function(i, data) {
			var field = self.createField(data, self);
			if (!field.isAutofill) {
				fields.push(field);
			}
		});
		self.fields(fields);
	},
	saveWork: function (rowdata) {
		//console.log('saveWork');
		if (this.rowId) {
			this.action('update');
		}
		this.updateFields(rowdata);
		this.win.close();
		this.win = null;
		this.parentField.onChangeRow(this);
	},
	cancelWork: function() {
		this.win.close();
		this.win = null;
		this.parentField.onCancelChangeRow(this);
	},
	serialize: function() {
		var self = this;
		return {
			action: self.action(),
			rowId: self.rowId,
			version: self.version,
			values: self.serializeFields(),
			internalKey: self.internalKey
		};
	},
	serializeFields: function() {
		return $.map(this.fields(), function(field) {
			if (field.showsField) {
				return field.serializeValue();
			} else {
				return null;
			}
		});
	},
	createField: function(data, parent) {
		return new ({
			'file': NestedFileField,
			'lookup': NestedLookupField,
			'relational_input': NestedRelationalInputField
		}[data.column.type] || NestedField)(data, parent);
	},
	fieldFor: function(id) {
		var fields = this.fields();
		for (var i = 0; i < fields.length; i++) {
			if (fields[i].id == id) {
				return fields[i];
			}
		}
		return null;
	}
});

var NestedField = Class.extend({
	init: function(data, parent){
		var self = this;
		self.parent = parent;
		self.id = data.column.id;
		self.isAutofill = data.column.isAutofill;
		self.type = data.column.type;
		self.label = data.column.label;
		self.showsField = data.column.showsField || false;
		self.hasOptions = data.column.hasOptions;
		self.referenceTable = data.column.referenceTable || null;

		if (self.type == 'user_selector') {
			self.value = ko.observableArray($.map(data.value, function(user) { return user.id; }));
		} else if (self.type == 'checkbox') {
			self.value = ko.observableArray($.map(data.value, function(option) { return option.id.toString(); }));
		} else if (self.type == 'dropdown' ||
				self.type == 'radio') {
			self.value = ko.observable(data.value ? data.value.id : null);
		} else if (self.type == 'file') {
		} else if (self.type == 'lookup') {
			self.value = ko.observable(data.value ? data.value.value : null);
			self.key = ko.observable(data.value ? data.value.key : null);
		} else if (self.type == 'relational_input') {
			self.value = ko.observableArray([]);
		} else {
			self.value = ko.observable(data.value);
		}
		self.text = ko.observable(data.text);
		self.html = ko.observable(data.html);
		//self.error = ko.observable(null);
		//self.message = ko.observable(null);
	},
	//idName: function() {
	//	return this.parent.formName + '_col_' + this.id;
	//},
	typeName: function() {
		return 'col_' + this.type;
	},
	serializeValue: function() {
		return { id: this.id, value: this.value() };
	},
	//initAfterBinding: function() {
	//	// Override.
	//},
	//propagateError: function() {
	//	// Override.
	//},
	getNestedRows: function() {
		// Override.
		return [];
	}
});

var NestedRelationalInputField = RelationalInputField.extend({
});

var NestedFileField = NestedField.extend({
	init: function(data, parent){
		this._super(data, parent);
		//if(this.parent.replication){
		//	this.text(null);
		//	this.value = ko.observableArray([]);
		//}else{
			this.value = ko.observableArray(ko.utils.arrayMap(data.value, function(file) {
				if (file.isTemp) {
					file.isSaved = false;
				} else {
					file.isSaved = true;
				}
				return file;
			}));
		//}
	}
});

var NestedLookupField = NestedField.extend({
	serializeValue: function() {	// override
		return { id: this.id, value: this.value(), key: this.key() };
	}
});

// see base.js txdb.RowReadInterface, txdb.RowWriteInterface
var RowInterface = Class.extend({
	init: function(form) {
		this.form = form;
		this.fields = {};
		var fields = form.fields();
		for (var i = 0; i < fields.length; i++) {
			if (this._isSupportedFieldType(fields[i].type)) {
				this.fields[fields[i].code] = this._createFieldInterface(fields[i], this);
			}
		}
	},
	getTableId : function() { // interface
		return this.form.parent.tableId;
	},
	getNo: function() { // interface
		return this.form.rowNo;
	},
	getHeaderMenuSpaceElement: function(){ // interface
		return $('#row-header-menu-space').get(0);
	},
	getField: function(code){ // interface
		return field = this.fields[code];;
	},
	getSpaceFieldElement: function(code){ // interface
		return this.getField(code)._getExtensionPointElement();
	},
	getLabelFieldElement: function(code){ // interface
		return this.getField(code)._getExtensionPointElement();
	},
	calculate: function(options){ // interface
		this.form.calculate(null, options);
	},
	isCopy: function() { // interface
		return this.form.isCopy;
	},
	_get$Element: function(){
		return $('[data-formname='+this.form.formName+']').eq(0);
	},
	_isSupportedFieldType: function(type){
		switch(type){
			case 'rownum':
			case 'text':
			case 'number':
			case 'calc':
			case 'radio':
			case 'dropdown':
			case 'date':
			case 'lookup':
			case 'label':
			case 'space':
				return true;
				break;
			default:
				return false;
		}
	},
	_createFieldInterface: function(field, parent) {
		return new ({
			'lookup': LookupFieldInterface,
			'radio': RadioFieldInterface,
			'dropdown': DropdownFieldInterface
		}[field.type] || FieldInterface)(field, parent);
	}
});

// see base.js txdb.FieldReadInterface, txdb.FieldWriteInterface
var FieldInterface = Class.extend({
	init: function(field, parentModel){
		this.field = field;
		this.row = parentModel;
	},
	getType: function(){
		return this.field.type;
	},
	setValue: function(value){ // interface
		if (this.field.disabled()) {
			throw "setValue() called on a disabled field";
		}
		if (this.field.isInputable == false) {
			throw "setValue() called on a non-inputable field";
		}
		this.field.value(value);
	},
	getValue: function(){ // interface
		return this.field.value();
	},
	lookup: function(){ // interface
		throw "not supported";
	},
	clear: function(){ // interface
		throw "not supported";
	},
	_get$Element: function(){
		return this.row._get$Element()
			.find('[data-fieldcode='+this.field.code+']')
			.eq(0);
	},
	_getExtensionPointElement: function(){
		return this._get$Element().find('.extension-point').get(0) ;
	}
});

var DropdownFieldInterface = FieldInterface.extend({
	setValue: function(value){
		var id = '';
		var text = '';
		if (value != null && value != '') {
			var found = false;
			$.each(this.field.options(), function(i, option){
				if (option.name == value.name) {
					id = option.id;
					text = option.name;
					found = true;
					return false;
				}
			});
			if (!found) {
				throw "option not found";
			}
		}
		this.field.value(id);
		this.field.text(text);
	},
	getValue: function(){
		var value = {};
		if (this.field.value() == null || this.field.value() == '') {
			return null;
		}
		var optionId = this.field.value();
		$.each(this.field.options(), function(i, option){
			if (option.id == optionId) {
				value.name = option.name;
				return false;
			}
		});
		return value;
	}
});

var RadioFieldInterface = DropdownFieldInterface.extend({
});

var LookupFieldInterface = FieldInterface.extend({
	setValue: function(value){
		this.field.value(value.value);
		this.field.text(value);
	},
	getValue: function(){
		return {
			//key: this.field.key(),
			value: this.field.value()
		};
	},
	lookup: function(options){
		this.field._lookup(options);
	},
	clear: function(){
		this.field.clearLookup();
	}
});

window.setupBlockUI = function () {
	$(document).ajaxStop($.unblockUI);
};
