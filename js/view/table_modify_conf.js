var ViewModel  = function(tableId) {
	var self = this;
	self._tableId = tableId;
	self.roundScale = ko.observable();
	self.roundType = ko.observable();
	self.apiState = ko.observable();
	self.apiMode = ko.observable();
	self.apiKey = ko.observable();
	self.writeApiKey = ko.observable();
	self.isCommentEnabled = ko.observable(false);
	self.isCalendarEnabled= ko.observable(false);
	self.isChild = ko.observable(false);
	self.isSixColumnsEnabled= ko.observable(false);
	self.errors = ko.observableArray();
	
	self.generateApiKey = function() {
		$.getJSON('/my/table/generateApiKey.json', 
			function(json) {
				if (json.status == 'success') {
					self.apiKey(json.result);
				}
			}
		);
	}
	
	self.clearApiKey = function() {
		self.apiKey(null);
	}
	
	self.generateWriteApiKey = function() {
		$.getJSON('/my/table/generateApiKey.json', 
			function(json) {
				if (json.status == 'success') {
					self.writeApiKey(json.result);
				}
			}
		);
	}
	
	self.save = function() {
		var data = {
			id: self._tableId,
			roundScale: self.roundScale(),
			roundType: self.roundType(),
			isApiEnabled: self.apiState() == 'public',
			apiKey: self.apiKey(),
			isWriteApiEnabled: self.apiMode() == 'writable',
			writeApiKey: self.writeApiKey(),
			//isCalendarEnabled: self.isCalendarEnabled(),
			isCommentEnabled: self.isCommentEnabled(),
			isSixColumnsEnabled: self.isSixColumnsEnabled()
		};
		$.post('/my/table/saveConf.json', {data: ko.toJSON(data)},
			function(json) {
				if (json.status == 'success') {
					document.location.href="/my/table/modify?"+$.param({t:json.result});
				} else if (json.status == 'error') {
					self.errors($.map(json.errors, function(error) {return error.message;}));
					$(window).scrollTop(0);
				} else {
					throw json.status;
				}
			}
		);
	}
	
	
	self._load = function() {
		$.getJSON('/my/table/loadInfo.json?'+$.param({t:self._tableId}), function(json) {
			self.roundScale(json.result.roundScale);
			self.roundType(json.result.roundType);
			self.apiState(json.result.isApiEnabled ? 'public' : 'private');
			self.isChild(json.result.isChild);
			if (!self.isChild() && json.result.isWriteApiEnabled) {
				self.apiMode('writable');
			} else {
				self.apiMode('readonly');
			}
			self.apiKey(json.result.apiKey);
			self.writeApiKey(json.result.writeApiKey);
			self.isCommentEnabled(json.result.isCommentEnabled);
			self.isCalendarEnabled(json.result.isCalendarEnabled);
			self.isSixColumnsEnabled(json.result.isSixColumnsEnabled);

		});
	};
	
	self._load();

};
