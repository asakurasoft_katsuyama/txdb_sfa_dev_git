ko.observableArrayWithUndo = function(initialArray) {
	var _tempValue = ko.observableArray($.extend(true, [], initialArray.slice(0))),
			result = ko.observableArray(initialArray),
			_errorMessage = ko.observable(null);

	//expose temp value for binding
	result.temp = _tempValue;

	//expose error value for binding
	result.error = _errorMessage;

	//commit temp value
	result.commit = function() {
		for (var i=0; i<_tempValue().length; i++) {
			if (_tempValue()[i] !== null &&
					(typeof _tempValue()[i] == 'object' || typeof _tempValue()[i] == 'function')) {
				if ( 'commit' in _tempValue()[i] && typeof _tempValue()[i].commit == 'function') {
					_tempValue()[i].commit();
				}
			}
		}
		result($.extend(true, [], _tempValue.slice(0)));
	};

	//reset temp value
	result.reset = function() {
		for (var i=0; i<_tempValue().length; i++) {
			if (_tempValue()[i] !== null &&
					(typeof _tempValue()[i] == 'object' || typeof _tempValue()[i] == 'function')) {
				if ( 'reset' in _tempValue()[i] && typeof _tempValue()[i].reset == 'function') {
					_tempValue()[i].reset();
				}
			}
		}
		_tempValue($.extend(true, [], result.slice(0)));
		result.error(null);
	};

	return result;
};

ko.observableWithUndo = function(initial) {
	var _tempValue = ko.observable(initial),
			result = ko.observable(initial),
			_errorMessage = ko.observable(null);

	//expose temp value for binding
	result.temp = _tempValue;

	//expose error value for binding
	result.error = _errorMessage;

	//commit temp value
	result.commit = function() {
		result(_tempValue());
	};

	//reset temp value
	result.reset = function() {
		_tempValue(result());
		result.error(null);
	};

	return result;
};

var CopyMap = function (data, field) {
	var self = this;
	self.field = field;
	self.acceptantId = ko.observableWithUndo(data[1]);
	self.accepteeId = ko.observableWithUndo(data[0]);

	self.acceptantOptions = ko.computed(function () {
//		console.log('[acceptantOptions]');
		var accepteeId = self.accepteeId.temp();
		var acceptee = null;
		if (accepteeId) {
			var acceptees = self.field.referenceCopyAccepteeOptions();
			for (var i=0; i<acceptees.length; i++) {
				if (accepteeId == acceptees[i].id) {
					acceptee = acceptees[i];
					break;
				}
			}
//			if (!acceptee) self.accepteeId.temp(null);
		}

		var filteredOptions = [{ id:null, label:"コピー先を選択"}];
		var options = self.field.referenceCopyAcceptantOptions();
//		console.log('accepteeId:'+accepteeId);
//		console.log('options.length'+options.length);
		if (options.length == 0) {
			return [{id: self.acceptantId.temp(), label: ''}];
		}
		if (acceptee) {
			return filteredOptions.concat($.map(options, self.getAcceptantsFilter(acceptee)));
		} else {
			return filteredOptions.concat(options);
		}
	});

	self.accepteeOptions = ko.computed(function () {
//		console.log('[accepteeOptions]');
		var acceptantId = self.acceptantId.temp();
		var acceptant = null;
		if (acceptantId) {
			var acceptants = self.field.referenceCopyAcceptantOptions();
			for (var i=0; i<acceptants.length; i++) {
				if (acceptantId == acceptants[i].id) {
					acceptant = acceptants[i];
					break;
				}
			}
//			if (!acceptant) self.acceptantId.temp(null);
		}
		var filteredOptions = [{ id:null, label:"コピー元を選択"}];
		var options = self.field.referenceCopyAccepteeOptions();
//		console.log('acceptantId:'+acceptantId);
//		console.log('options.length'+options.length);
		if (options.length == 0) {
			return [{id: self.accepteeId.temp(), label: ''}];
		}
		if (acceptant) {
			return filteredOptions.concat($.map(options, self.getAccepteesFilter(acceptant)));
		} else {
			return filteredOptions.concat(options);
		}
	});
};

// コピー先一覧をフィルタリングする
CopyMap.prototype.getAcceptantsFilter = function (acceptee) {
	return function (acceptant) {// acceptant
		var acceptees = acceptant.lookupConstraints.copyAcceptees;
		for (var i=0; i<acceptees.length; i++) {
			// コピー先の有効コピー元type一覧（acceptees）に、コピー元（acceptee）typeが一致するか
			if (acceptees[i] == acceptee.type) {
				if (acceptee.lookupConstraints
						&& acceptee.lookupConstraints.lookupOrigin) {
					var lookupOrigin = acceptee.lookupConstraints.lookupOrigin
					if (acceptees.indexOf(lookupOrigin.type) == -1) {
						continue;
					}
					if (acceptant.linkType && acceptant.linkType() != lookupOrigin.linkType) {
						continue;
					}
					return acceptant;
				} else if (acceptant.linkType && acceptee.linkType && acceptant.linkType() != acceptee.linkType) {
					// リンクタイプが一致するか
					continue;
				}
				return acceptant;
			}
		}
	};
};

// コピー元一覧をフィルタリングする
CopyMap.prototype.getAccepteesFilter = function (acceptant) {
	return function (acceptee) {
		var acceptees = acceptant.lookupConstraints.copyAcceptees;
		for (var i=0; i<acceptees.length; i++) {
			// コピー先の有効コピー元type一覧（acceptees）に、コピー元（acceptee）typeが一致するか
			if (acceptees[i] == acceptee.type) {
				if (acceptee.lookupConstraints
						&& acceptee.lookupConstraints.lookupOrigin) {
					var lookupOrigin = acceptee.lookupConstraints.lookupOrigin
					if (acceptees.indexOf(lookupOrigin.type) == -1) {
						continue;
					}
					if (acceptant.linkType && acceptant.linkType() != lookupOrigin.linkType) {
						continue;
					}
					return acceptee;
				} else if (acceptant.linkType && acceptee.linkType && acceptant.linkType() != acceptee.linkType) {
					// リンクタイプが一致するか
					continue;
				}
				return acceptee;
			}
		}
	};
};

CopyMap.prototype.reset = function () {
	this.accepteeId.reset();
	this.acceptantId.reset();
};

CopyMap.prototype.commit = function () {
	this.accepteeId.commit();
	this.acceptantId.commit();
};

CopyMap.prototype.toData = function (isTemp) {
	if (isTemp) {
		return [this.accepteeId.temp(), this.acceptantId.temp()];
	} else {
		return [this.accepteeId(), this.acceptantId()];
	}
};

var Option = function(data, field) {
	this.id = data.id;
	this.name = ko.observableWithUndo(data.name);
	this.isDefault = ko.observableWithUndo(data.isDefault); // checkbox only
};

Option.prototype.reset = function () {
	this.name.reset();
	this.isDefault.reset();
};

Option.prototype.commit = function () {
	this.name.commit();
	this.isDefault.commit();
};

Option.prototype.toData = function (isTemp, defaultValueIds) {
	if (defaultValueIds != null) {
		if (isTemp) {
			if (this.isDefault.temp() == true) {
				defaultValueIds.push(this.id);
			}
		} else {
			if (this.isDefault() == true) {
				defaultValueIds.push(this.id);
			}
		}
	}

	if (isTemp) {
		return {id: this.id, name: this.name.temp()};
	} else {
		return {id: this.id, name: this.name()};
	}
};

//Option.prototype.toData = function (isTemp) {
//	if (isTemp) {
//		return [this.accepteeId.temp(), this.acceptantId.temp()];
//	} else {
//		return [this.accepteeId(), this.acceptantId()];
//	}
//};


/**
 * @param {ViewModel|Field} parent
 */
var Row = function(parent) {
	var self = this;
	self.parent = parent;
	self.fields = ko.observableArray();
	self.allowDrop = ko.observable(true);
	self.isEmpty = ko.computed(function(){
		return self.fields().length == 0;
	});
};

/**
 * @param {object} data Data to be loaded.
 * @param {ViewModel} parent
 * @param {boolean} setup Whether create initial data or not.
 */
var Field = function(data, parent, setup) {
	var self = this;
	self.parent = parent;
	self.descriptor = parent.descriptor(data.type);
	self.load(data, setup);

	self.sortableOptions = {
		start: function(event, ui) {
			ui.placeholder.height(ui.item.height());
			ui.helper.css('z-index', 10000);
		},
		placeholder: 'tx-modal-sortable-placeholder',
		// http://www.vijayjoshi.org/2012/02/28/jqueryui-sortable-tr-width-reduces-when-using-with-table/
		helper: function(e, tr)
		{
			var $helper = tr.clone();

			var selects = tr.find('select');
			var selects2 = $helper.find('select');
			for (var i=0; i<selects.length; i++) {
				selects2.eq(i).val(selects.eq(i).val());
			}

			var form = $(
					'<form class="tx-modal-sortable-helper">\
						<div class="form-group">\
							<table class="table">\
								<tbody></tbody>\
							</table>\
						</div>\
					</form>');
			var tbody = form.find('tbody');
			tbody.append($helper);

			return form;
		},
		appendTo: 'body',
		handle: '.tx-list-sort-handle'
	};
};

Field.prototype.load = function (data, setup) {
	var self = this;
	var parent = this.parent;
	var descriptor = this.descriptor;
	self.id = data.id;
	self.type = data.type;
	self.label = ko.observableWithUndo(data.label || parent.getDefaultFieldLabel(self.type));
	self.code = ko.observableWithUndo(data.code || parent.generateFieldCode(self.type) );

	self.error = ko.observable(null);

	if ('lookupConstraints' in descriptor) {
		self.lookupConstraints = data.lookupConstraints;
	}

	if ('relationalConstraints' in descriptor) {
		self.relationalConstraints = data.relationalConstraints;
	}

	// options
	if ('options' in descriptor && descriptor.options != null) {
		if (data.options != null) {
			data.options = $.map(
					data.options,
					function(option) {
						if (self.type == 'checkbox') {
							option.isDefault = false;
							for (var i=0; i<data.defaultValueIds.length; i++) {
								if (option.id == data.defaultValueIds[i]) {
									option.isDefault = true;
								}
							}
						}
						return new Option(option, self);
					}
			);
		}
		self.options = ko.observableArrayWithUndo(data.options || []);
		self.addedOptionIds = [];
		self.removedOptionIds = [];
		if (setup) {
			self._addOption(0, '選択肢1');
			self._addOption(1, '選択肢2');
			self.options.commit();
		}
		self.optionsList = ko.computed(function(){
			return [new Option({id:'', name:''}, self)].concat(self.options());
		});
		self.optionsTempList = ko.computed(function(){
			return [new Option({id:'', name:''}, self)].concat(self.options.temp());
		});
	}

	// properties
	if ('showsField' in descriptor ) {
		self.showsField = ko.observable(!!data.showsField);
		self.baseItemDisabled = ko.computed(function () { return !(self.showsField()); });
	}
	if ('showsLabel' in descriptor) {
		self.showsLabel = ko.observableWithUndo(!!data.showsLabel);
	}
	if ('positionY' in descriptor)
		self.positionY = data.positionY || 1;
	if ('positionX' in descriptor)
		self.positionX = data.positionX || 1;
	if ('sizeWidth' in descriptor) {
		self.sizeWidth = ko.observable(data.sizeWidth || 1);
	} else {
		self.sizeWidth = ko.observable(self.parent.maxWidth());
	}
	self.styleWidth = ko.computed(function(){
		return 'col-xs-' + self.sizeWidth() * self.parent.widthUnit();
	});
	if ('sizeHeight' in descriptor) {
		self.sizeHeight = ko.observable(data.sizeHeight || 1);
	}
	if ('isUnique' in descriptor) {
		self.isUnique = ko.observableWithUndo(!!data.isUnique);
		self.isUniqueRestricted = ko.observable(true);
	}
	if ('isRequired' in descriptor) {
		if (self.type == 'radio') {
			self.isRequired = ko.observableWithUndo(true);
		} else {
			self.isRequired = ko.observableWithUndo(!!data.isRequired);
		}
	}
	if ('minLength' in descriptor)
		self.minLength = ko.observableWithUndo($.isNumeric(data.minLength) ? data.minLength : null);
	if ('maxLength' in descriptor)
		self.maxLength = ko.observableWithUndo($.isNumeric(data.maxLength) ? data.maxLength : null);
	if ('minValue' in descriptor)
		self.minValue = ko.observableWithUndo($.isNumeric(data.minValue) ? data.minValue : null);
	if ('maxValue' in descriptor)
		self.maxValue = ko.observableWithUndo($.isNumeric(data.maxValue) ? data.maxValue : null);

	self.hintText = ko.computed(function () {
		var hintText = "";
		if (self.minLength || self.maxLength) {
			if ($.isNumeric(self.minLength()))
				hintText += self.minLength() + "文字以上";
			if ($.isNumeric(self.maxLength()))
				hintText += self.maxLength() + "文字以内";
		}
		if (self.minValue || self.maxValue) {
			if ($.isNumeric(self.minValue()))
				hintText += self.minValue() + "以上";
			if ($.isNumeric(self.maxValue()))
				hintText += self.maxValue() + "以下";
		}
		return hintText;
	});

	if ('isUnique' in self && 'maxLength' in self) {
		self.isUnique.temp.subscribe(function (value) {
			if (value) {
				self.maxLength.temp(64);
			}
		});
	}

	if ('defaultValue' in descriptor) {
		if (self.type == 'date' || self.type == 'datetime' || self.type == 'time') {
			if (data.defaultValue == 'now') {
				self.defaultValue = ko.observableWithUndo(null);
				self.isDefaultNow = ko.observableWithUndo(true);
			} else {
				self.defaultValue = ko.observableWithUndo(data.defaultValue || null);
				self.isDefaultNow = ko.observableWithUndo(false);
			}
			self.isDefaultNow.temp.subscribe(function(value){
				if (value) {
					self.defaultValue.temp(null);
				}
			});
			self.defaultDate = ko.computed(function () {
				if (self.isDefaultNow()) {
					return moment().format('YYYY-MM-DD');
				} else {
					return self.defaultValue();
				}
			});
			self.defaultTime = ko.computed(function () {
				if (self.isDefaultNow()) {
					return moment().format('HH:mm');
				} else {
					return self.defaultValue();
				}
			});
			self.defaultDatetime = ko.computed(function () {
				if (self.isDefaultNow()) {
					return moment().format('YYYY-MM-DD HH:mm');
				} else {
					return self.defaultValue();
				}
			});
			if (setup) {
				self.isDefaultNow.temp(true);
				self.isDefaultNow.commit();
			}
		} else {
			self.defaultValue = ko.observableWithUndo(data.defaultValue || null);
		}
	}
	if ('defaultValueId' in descriptor) {
		self.defaultValue = ko.observableWithUndo(data.defaultValueId || null);
	}
	if ('expression' in descriptor) {
		self.expression = ko.observableWithUndo(data.expression || null);
		if (self.type == 'text') {
			self.isCalculative = ko.observableWithUndo(!!data.expression);
			self.isCalculative.temp.subscribe(function(value){
				if (value) {
					self.isRequired.temp(false);
					self.isUnique.temp(false);
					self.minLength.temp(null);
					self.maxLength.temp(null);
					self.defaultValue.temp(null);
				} else {
					self.expression.temp(null);
				}
			});
		}
	}
	if ('format' in descriptor)
		self.format = ko.observableWithUndo(data.format || 'number');
	if ('dateFormat' in descriptor)
		self.dateFormat = ko.observableWithUndo(data.dateFormat || 'datetime');
	if ('showsDigit' in descriptor)
		self.showsDigit = ko.observableWithUndo(!!data.showsDigit);
	if ('labelHtml' in descriptor) {
		self.labelHtml = ko.observableWithUndo(data.labelHtml || null);
		if (setup) {
			self.labelHtml.temp('<p>注意書きを編集してください</p>');
			self.labelHtml.commit();
		}
	}
	if ('thumbnailMaxSize' in descriptor)
		self.thumbnailMaxSize = ko.observableWithUndo(data.thumbnailMaxSize || 150);
	if ('imageMaxSize' in descriptor)
		self.imageMaxSize = ko.observableWithUndo(data.imageMaxSize || null);
	if ('linkType' in descriptor) {
		self.linkType = ko.observableWithUndo(data.linkType || 'url');
		self.linkTypeFixed = false;
		if (data.linkType) {
			self.linkTypeFixed = true;
		}
	}
	if ('referenceTableId' in descriptor) {
		self.referenceTableOptions = new ko.observableArray();
		self.referenceTableId = ko.observableWithUndo(data.referenceTable && data.referenceTable.id
				? data.referenceTable.id : null);
		self.referalError = ko.observable(false);
		if (data.referenceTable && data.referenceTable.allows
				&& data.referenceTable.allows.indexOf('select') < 0) {
			self.referalError(true);
		}
		if (self.type == 'relational') {
			self.referenceColumnLabels = new ko.observableArray();
		}
		if (self.type == 'lookup') {
			self.referenceFixed = (data.referenceTable && data.referenceTable.id) ? true : false;
			self.referenceTableName = null;
			self.referenceTargetColumnName = null;
		}
		if (self.type == 'relational_input') {
			if (data.referenceTable && data.referenceTable.allows
					&& (
						data.referenceTable.allows.indexOf('insert') < 0
						|| data.referenceTable.allows.indexOf('delete') < 0
						|| data.referenceTable.allows.indexOf('update') < 0
					)
				) {
				self.referalError(true);
			}
			self.referenceColumnLabels = new ko.observableArray();
			self.referenceFixed = (data.referenceTable && data.referenceTable.id) ? true : false;
			self.referenceTableName = null;
		}
	}
	if ('referenceTargetColumnId' in descriptor) {
		self.referenceTargetColumnOptions = new ko.observableArray();
		self.referenceTargetColumnId = ko.observableWithUndo(data.referenceTargetColumnId || null);
	}
	if ('referenceSourceColumnId' in descriptor) {
		self.referenceSourceColumnOptions = new ko.observableArray();
		self.referenceSourceColumnId = ko.observableWithUndo(data.referenceSourceColumnId || null);
	}
	if ('referenceColumnIds' in descriptor) {
		self.referenceListableOptions = new ko.observableArray();
		self.referenceColumnIds = ko.observableArrayWithUndo(
			data.referenceColumnIds ?
				$.map(data.referenceColumnIds, function (id) { return {id: id}; }) :  []
		);
	}
	if ('referenceSortColumnId' in descriptor) {
		self.referenceSortableOptions = new ko.observableArray();
		self.referenceSortColumnId = ko.observableWithUndo(data.referenceSortColumnId || null);
	}
	if ('referenceSortsReverse' in descriptor)
		self.referenceSortsReverse = ko.observableWithUndo(data.referenceSortsReverse || false);
	if ('referenceMaxRow' in descriptor)
		self.referenceMaxRow = ko.observableWithUndo(data.referenceMaxRow || 5);
	if ('referenceCopyMapIds' in descriptor) {
		self.referenceCopyAcceptantOptions = new ko.observableArray();
		self.referenceCopyAccepteeOptions = new ko.observableArray();
		self.referenceCopyMaps = ko.observableArrayWithUndo(
			data.referenceCopyMapIds ?
			$.map(data.referenceCopyMapIds, function (data) { return new CopyMap(data, self); }) : []
		);
	}
	if ('referenceAggregations' in descriptor) {
		self.referenceAggregatableOptions = new ko.observableArray();
		self.referenceAggregations = ko.observableArrayWithUndo(
			data.referenceAggregations ?
				$.map(data.referenceAggregations, function (value) { return {id: value[0], fn: value[1]}; }) :  []
		);
	}
	if ('optionsAlignment' in descriptor)
		self.optionsAlignment = ko.observableWithUndo(data.optionsAlignment || 'vertical');
	if ('layoutParentId' in descriptor) {
		self.layoutParentId = data.layoutParentId || null;
	}

	if (self.type == 'lookup') {
		if (self.referenceTableId()) {
//			self.loadLookupTargetColumnOptions(self.referenceTableId());
		}
		self.referenceTableId.temp.subscribe(function (value) {
			self.referenceTargetColumnId.temp(null);
			self.referenceCopyMaps.temp.removeAll();
			self.referenceColumnIds.temp.removeAll();
			self.loadLookupTargetColumnOptions(self.referenceTableId.temp());
		});

	}

	if (self.type == 'datecalc') {
		self.dateSourceColumnOptions = new ko.observableArray();
		self.dateSourceColumnId = ko.observableWithUndo(data.dateSourceColumnId || null);
		self.datecalcUnitOptions = new ko.observableArray();
		self.datecalcUnit = ko.observableWithUndo(data.datecalcUnit || null);
		self.datecalcBeforeAfterOptions = new ko.observableArray();
		self.datecalcBeforeAfter = ko.observableWithUndo(data.datecalcBeforeAfter || null);
	}

	if (self.type == 'relational') {
		if (self.referenceTableId()) {
			self.loadReferenceColumnLabels(self.referenceTableId(), self.referenceColumnIds());
//			self.loadRelationalTargetColumnOptions(self.referenceTableId());
		}
		self.referenceTableId.temp.subscribe(function (value) {
			self.referenceTargetColumnId.temp(null);
			self.referenceSourceColumnId.temp(null);
			self.referenceSortColumnId.temp(null);
			self.referenceColumnIds.temp.removeAll();
			self.referenceAggregations.temp.removeAll();
			self.loadRelationalTargetColumnOptions(self.referenceTableId.temp());
		});

		self.filteredReferenceSourceColumnOptions = ko.computed(function () {
			var targetColumnId = self.referenceTargetColumnId.temp();
			var targetColumn = null;
			if (targetColumnId) {
				var targetColumns = self.referenceTargetColumnOptions();
				for (var i=0; i<targetColumns.length; i++) {
					if (targetColumnId == targetColumns[i].id) {
						targetColumn = targetColumns[i];
						break;
					}
				}
			}
			var filteredOptions = [{ id:null, label:"参照元フィールドを選択"}];
			var options = self.referenceSourceColumnOptions();

			if (options.length == 0) {
				return [{id: self.referenceSourceColumnId.temp(), label: ''}];
			}
			if (targetColumn) {
				return filteredOptions.concat($.map(options, self.getRelationalSourceColumnFilter(targetColumn)));
			} else {
				return filteredOptions.concat(options);
			}

		});

		self.filteredReferenceTargetColumnOptions = ko.computed(function () {
			var sourceColumnId = self.referenceSourceColumnId.temp();
			var sourceColumn = null;
			if (sourceColumnId) {
				var sourceColumns = self.referenceSourceColumnOptions();
				for (var i=0; i<sourceColumns.length; i++) {
					if (sourceColumnId == sourceColumns[i].id) {
						sourceColumn = sourceColumns[i];
						break;
					}
				}
			}

			var filteredOptions = [{ id:null, label:"参照先フィールドを選択"}];
			var options = self.referenceTargetColumnOptions();
			if (options.length == 0) {
				return [{id: self.referenceTargetColumnId.temp(), label: ''}];
			}
			if (sourceColumn) {
				return filteredOptions.concat($.map(options, self.getRelationalTargetColumnFilter(sourceColumn)));
			} else {
				return filteredOptions.concat(options);
			}
		});
	}

	if (self.type == 'relational_input') {
		if (self.referenceTableId()) {
			self.loadReferenceColumnLabels(self.referenceTableId(), self.referenceColumnIds());
		}
		self.referenceTableId.temp.subscribe(function (value) {
			self.referenceSortColumnId.temp(null);
			self.referenceColumnIds.temp.removeAll();
			self.loadRelationalInputTargetColumnOptions(self.referenceTableId.temp());
		});
	}

	if (self.type == 'tab_group') {
		self.tabs = ko.observableArrayWithUndo([]);
		self.tabs.subscribe(function (tabs) {
			if (tabs.length > 0) {
				self.activeTab(tabs[0]);
			}
		});
		self.activeTab = ko.observable(null);
		self.addedTabs = [];
		self.removedTabs = [];
		if (setup) {
			self._addTab(0);
			self.tabs.commit();
		}
	}

	if (self.type == 'tab') {
		self.rows = ko.observableArray([]);
		if (setup) {
			self.rows.push(new Row(self));
		}
	}

	self.idName = ko.computed(function() {
		return 'col_' + self.id;
	});
	self.typeName = function() { return 'col_' + self.type; }
	self.modalName = ko.computed(function() { return 'modal_' + self.id; });
	self.uiItemIdName = ko.computed(function() {
		if (self.id == null) {
			return self.type;
		} else {
			return self.id;
		}
	});
	self.initUniqueRestricted();
};

Field.prototype.addOption = function(sender, event) {
	var self = ko.contextFor(event.target).$root;
	var index = self.options.temp.indexOf(sender);
	var optionId = self._addOption(index+1, '', self)
};

Field.prototype._addOption = function(index, name, _self) {
	var self = _self || this;
	var optionId = null;
	$.ajax({
		type: 'POST',
		url: '/my/table/addOption.json',
		data: {t: self.parent.tableId, c: self.id},
		async: false,
		success: function(data) {
			var option = {'id': data.result.id, 'name': name};
			if (self.type == 'checkbox') {
				option.isDefault = false;
			}
			self.options.temp.splice(index, 0, new Option(option, self));
			optionId = data.result.id;
		},
		dataType: 'json'
	});
	return optionId;
};

Field.prototype.removeOption = function(sender, event) {

	var self = ko.contextFor(event.target).$root;
	var index = self.options.temp.indexOf(sender);
	var optionId = self.options.temp()[index].id;
	$.ajax({
		type: 'POST',
		url: '/my/table/removeOption.json',
		data: {t: self.parent.tableId, c: self.id, o: optionId},
		async: false,
		success: function(data) {
			self.options.temp.splice(index, 1);
			self.removedOptionIds.push(optionId);
		},
		dataType: 'json'
	});

};

Field.prototype.restoreOptions = function() {
	var self = this;

	// 削除された選択肢
	var removedOptionIds = [];
	for (var i=0; i<self.removedOptionIds.length; i++ ) {
		for (var j=0; j<self.options().length; j++ ) {
			if(self.removedOptionIds[i] == self.options()[j].id) {
				removedOptionIds.push(self.removedOptionIds[i]);
				break;
			}
		}
	}
	// 追加された選択肢
	var addedOptionIds = [];
	for (var i=0; i<self.options.temp().length; i++ ) {
		var remains = false;
		for (var j=0; j<self.options().length; j++ ) {
			if(self.options.temp()[i].id == self.options()[j].id) {
				remains = true;
				break;
			}
		}
		if (!remains) {
			addedOptionIds.push(self.options.temp()[i].id);
		}
	}

	var data = self.parent.toData();
	data.columnId = self.id;
	data.addedOptionIds = addedOptionIds;
	data.removedOptionIds = removedOptionIds;

	$.ajax({
		type: 'POST',
		url: '/my/table/restoreOptions.json',
		data: {data: ko.toJSON(data)},
		success: function(data) {},
		dataType: 'json',
		async: false
	});
};

Field.prototype.addCopyMap = function(sender, event) {
	var self = ko.contextFor(event.target).$root;
	var index = self.referenceCopyMaps.temp.indexOf(sender);
	if (index == -1) {
		index == 0;
	}
	self.referenceCopyMaps.temp.splice(index + 1, 0, new CopyMap([null, null], self));
};

Field.prototype.removeCopyMap = function(sender, event) {
	var self = ko.contextFor(event.target).$root;
	var index = self.referenceCopyMaps.temp.indexOf(sender);
	if (index == -1) {
		index == 0;
	}
	self.referenceCopyMaps.temp.splice(index, 1);
};

Field.prototype.addReferenceColumn = function(sender, event) {
	var self = ko.contextFor(event.target).$root;
	var index = self.referenceColumnIds.temp.indexOf(sender);
	if (index == -1) {
		index == 0;
	}
	self.referenceColumnIds.temp.splice(index + 1, 0, {id: null});
};

Field.prototype.removeReferenceColumn = function(sender, event) {
	var self = ko.contextFor(event.target).$root;
	var index = self.referenceColumnIds.temp.indexOf(sender);
	if (index == -1) {
		index == 0;
	}
	self.referenceColumnIds.temp.splice(index, 1);
};

Field.prototype.addReferenceAggregationColumn = function(sender, event) {
	var self = ko.contextFor(event.target).$root;
	var index = self.referenceAggregations.temp.indexOf(sender);
	if (index == -1) {
		index == 0;
	}
	self.referenceAggregations.temp.splice(index + 1, 0, {id: null, fn:[]});
};

Field.prototype.removeReferenceAggregationColumn = function(sender, event) {
	var self = ko.contextFor(event.target).$root;
	var index = self.referenceAggregations.temp.indexOf(sender);
	if (index == -1) {
		index == 0;
	}
	self.referenceAggregations.temp.splice(index, 1);
};

Field.prototype.addTab = function(sender, event) {
	var self = ko.contextFor(event.target).$root;
	var index = self.tabs.temp.indexOf(sender);
	var tab = self._addTab(index+1, self);
	tab.label.temp('');
};

Field.prototype._addTab = function(index, _self) {
	var self = _self || this;
	var tab = null;
	$.ajax({
		type: 'POST',
		url: '/my/table/addColumn.json',
		data: {t: self.parent.tableId, type: 'tab'},
		async: false,
		success: function(data) {
			data.result.layoutParentId = self.id;
			tab =  new Field(data.result, self.parent, true);
			self.tabs.temp.splice(index, 0, tab);
			self.addedTabs.push(tab);
			self.parent.fields.push(tab);
		},
		dataType: 'json'
	});
	return tab;
};

Field.prototype.removeTab = function(sender, event) {
	var self = ko.contextFor(event.target).$root;
	var index = self.tabs.temp.indexOf(sender);
	var tab = self.tabs.temp()[index];
	var messages = tab.validateRemove();
	if (messages.length > 0) {
		alert(messages[0]);
		return;
	}

	$.ajax({
		type: 'POST',
		url: '/my/table/removeColumn.json',
		data: {t: self.parent.tableId, c: sender.id},
		async: false,
		success: function(data) {
			self.tabs.temp.splice(index, 1);
			self.removedTabs.push(sender);
			var index2 = self.parent.fields.indexOf(sender);
			self.parent.fields.splice(index2, 1);
		},
		dataType: 'json'
	});
};

Field.prototype.restoreTabs = function() {
	var self = this;

	// 削除された選択肢
	var removedTabIds = [];
	var removedTabs = [];
	for (var i=0; i<self.removedTabs.length; i++ ) {
		for (var j=0; j<self.tabs().length; j++ ) {
			if(self.removedTabs[i].id == self.tabs()[j].id) {
				removedTabIds.push(self.removedTabs[i].id);
				removedTabs.push(self.removedTabs[i]);
				break;
			}
		}
	}
	// 追加された選択肢
	var addedTabIds = [];
	var addedTabs = [];
	for (var i=0; i<self.tabs.temp().length; i++ ) {
		var remains = false;
		for (var j=0; j<self.tabs().length; j++ ) {
			if(self.tabs.temp()[i].id == self.tabs()[j].id) {
				remains = true;
				break;
			}
		}
		if (!remains) {
			addedTabIds.push(self.tabs.temp()[i].id);
			addedTabs.push(self.tabs.temp()[i]);
		}
	}

	// 削除分を ViewModel へ復元
	for (var i=0; i<removedTabs.length; i++) {
		self.parent.fields.push(removedTabs[i]);
	}
	// 追加分を ViewModel から削除
	for (var i=0; i<addedTabs.length; i++) {
		var index = self.parent.fields.indexOf(addedTabs[i]);
		self.parent.fields.splice(index, 1);
	}


	var data = self.parent.toData();
	data.addedTabIds = addedTabIds;
	data.removedTabIds = removedTabIds;

	$.ajax({
		type: 'POST',
		url: '/my/table/restoreTabs.json',
		data: {data: ko.toJSON(data)},
		success: function(data) {},
		dataType: 'json',
		async: false
	});
};

Field.prototype.modalTplName = function () {
	return this.parent.modalTplName(this);
};

Field.prototype.loadLookupTableOptions = function () {
	var self = this;
	return $.ajax({
		type: 'GET',
		url: '/my/table/referableTables.json',
		data: {t: self.parent.tableId},
		success: function(json) {
			var referenceTableOptions = [];
			$.each(json.result, function(i, table) {
				referenceTableOptions.push(table);
				if (table.id == self.referenceTableId()) {
					self.referenceTableName = table.name;
				}
			});
			self.referenceTableOptions(referenceTableOptions);
		},
		async: false,
		dataType: 'json'
	});
};

Field.prototype.loadLookupSourceColumnOptions = function () {
	var self = this;
	var parent = self.parent;

	var referenceCopyAcceptantOptions = [];
	$.each(parent.fields, function(i, field) {
		if (field.lookupConstraints && field.lookupConstraints.copyAcceptable == true) {
			if ('linkType' in field && String.isEmpty(field.linkType()))
				return true;
			referenceCopyAcceptantOptions.push(field);
		}
	});
	self.referenceCopyAcceptantOptions(referenceCopyAcceptantOptions);
};

Field.prototype.loadReferenceColumnLabels = function (tableId, columnIds) {
	var self = this;
	return $.ajax({
		url: '/my/table/referenceColumns.json',
		data: {t: tableId},
		success: function(json) {
			var referenceColumnLabels = [];
			$.each(columnIds, function(i, obj) {
				var label = '';
				for(var j=0; j<json.result.length; j++) {
					if (json.result[j].id == obj.id) {
						label = json.result[j].label;
					}
				}
				referenceColumnLabels.push(label);
			});
			self.referenceColumnLabels(referenceColumnLabels);
		},
		async: false,
		dataType: 'json'
	});
};

Field.prototype.loadLookupTargetColumnOptions = function (tableId) {
	var self = this;

	self.referenceTargetColumnOptions.removeAll();
	self.referenceCopyAccepteeOptions.removeAll();
	self.referenceListableOptions.removeAll();

	if (!tableId) return;

	return $.ajax({
		url: '/my/table/referenceColumns.json',
		data: {t: tableId},
		success: function(json) {
			var referenceTargetColumnOptions = [ { id:null, label:"選択してください"} ],
			referenceCopyAccepteeOptions = [],
			referenceListableOptions = [];

			var accepteeTypes = {};
			var fields = self.parent.fields;
			for (var i=0; i<fields.length; i++) {
				if (fields[i].lookupConstraints && fields[i].lookupConstraints.copyAcceptable) {
					var acceptees = fields[i].lookupConstraints.copyAcceptees;
					for (var j=0; j<acceptees.length; j++) {
						accepteeTypes[acceptees[j]] = true;
					}
				}
			}

			$.each(json.result, function(i, column) {
				if (column.lookupConstraints && column.lookupConstraints.referable == true)
					referenceTargetColumnOptions.push(column);
				if (column.lookupConstraints && column.type in accepteeTypes)
					referenceCopyAccepteeOptions.push(column);
				if (column.isListable == true)
					referenceListableOptions.push(column);
				if (column.id == self.referenceTargetColumnId()) {
					self.referenceTargetColumnName = column.label;
				}
			});
			self.referenceTargetColumnOptions(referenceTargetColumnOptions);
			self.referenceCopyAccepteeOptions(referenceCopyAccepteeOptions);
			self.referenceListableOptions(referenceListableOptions);
		},
		async: false,
		dataType: 'json'
	});
};

Field.prototype.loadRelationalTableOptions = function () {
	var self = this;
	return $.ajax({
		type: 'GET',
		url: '/my/table/referableTables.json',
		data: {t: self.parent.tableId},
		success: function(json) {
			var referenceTableOptions = [];
			$.each(json.result, function(i, table) {
				referenceTableOptions.push(table);
			});
			self.referenceTableOptions(referenceTableOptions);
		},
		async: false,
		dataType: 'json'
	});
};

Field.prototype.loadRelationalInputTableOptions = function () {
	var self = this;
	return $.ajax({
		type: 'GET',
		url: '/my/table/child_table_candidates.json',
		data: {t: self.parent.tableId, c:self.id},
		success: function(json) {
			var referenceTableOptions = [];
			$.each(json.result, function(i, table) {
				referenceTableOptions.push(table);
				if (table.id == self.referenceTableId()) {
					self.referenceTableName = table.name;
				}
			});
			self.referenceTableOptions(referenceTableOptions);
		},
		async: false,
		dataType: 'json'
	});
};

Field.prototype.loadDateSourceColumnOptions = function (setFirstEntry) {
	var self = this;
	var parent = self.parent;
	var dateSourceColumnOptions = !setFirstEntry ? [] : [{ id:null, label:"参照元フィールドを選択"}] ;
	var referrerKeys = ['date', 'datetime', 'time', 'created_on', 'updated_on'];
	$.each(parent.fields, function(i, field) {
			for (var i=0; i<referrerKeys.length; i++) {
				if (referrerKeys[i] == field.type) {
					dateSourceColumnOptions.push(field);
					break;
				}
			}
	});
	self.dateSourceColumnOptions(dateSourceColumnOptions);
};

Field.prototype.loadDatecalcUnitColumnOptions = function (setFirstEntry) {
	var self = this;
	var datecalcUnitOptions = !setFirstEntry ? [] : [{ id:null, label:"期間を選択"}] ;
	datecalcUnitOptions = datecalcUnitOptions.concat([
		{id: 'minute', label: '分'}
		, {id: 'hour', label: '時間'}
		, {id: 'day', label: '日'}
		, {id: 'week', label: '週間'}
		, {id: 'month', label: 'ヶ月'}
		, {id: 'year', label: '年'}
	]);
	self.datecalcUnitOptions(datecalcUnitOptions);
};

Field.prototype.loadDatecalcBeforeAfterColumnOptions = function (setFirstEntry) {
	var self = this;
	var datecalcBeforeAfterOptions = !setFirstEntry ? [] : [{ id:null, label:"前後を選択"}] ;
	datecalcBeforeAfterOptions = datecalcBeforeAfterOptions.concat([
		{id: 'after', label: '後'}
		, {id: 'before', label: '前'}
	]);
	self.datecalcBeforeAfterOptions(datecalcBeforeAfterOptions);
};

Field.prototype.loadRelationalSourceColumnOptions = function () {
	var self = this;
	var parent = self.parent;
	var referenceSourceColumnOptions = [];
	$.each(parent.fields, function(i, field) {
		if (field.relationalConstraints && field.relationalConstraints.referable == true) {
			var referrerKeys = field.relationalConstraints.referrerKeys;
			for (var i=0; i<referrerKeys.length; i++) {
				if (referrerKeys[i] == field.type) {
					referenceSourceColumnOptions.push(field);
					break;
				}
			}
		}
	});
	self.referenceSourceColumnOptions(referenceSourceColumnOptions);
};

/**
 * 参照先一覧のフィルタリング
 */
Field.prototype.getRelationalTargetColumnFilter = function (sourceColumn) {
	var vm = this.parent;
	return function (targetColumn) {
		var referrerKeys = targetColumn.relationalConstraints.referrerKeys;
		for (var i=0; i<referrerKeys.length; i++) {
			// 参照先の許可type（一覧）に、参照元typeが一致するか（含まれるか）
			if (referrerKeys[i] == sourceColumn.type) {
				if (targetColumn.lookupConstraints
						&& targetColumn.lookupConstraints.lookupOrigin) {
					var lookupOrigin = targetColumn.lookupConstraints.lookupOrigin
					// 参照先がルックアップ先を持っている場合、参照先の本来の型はルックアップ先の型。
					// 参照元が、本来の参照先型の参照許可type一覧に含まれるか？
					var referrerKeys = vm.referrerKeys(lookupOrigin.type);
					if (referrerKeys == null || referrerKeys.indexOf(sourceColumn.type) == -1) {
						continue;
					}
					if (sourceColumn.linkType && sourceColumn.linkType() != lookupOrigin.linkType) {
						continue;
					}
					return targetColumn;
				} else if (sourceColumn.linkType && targetColumn.linkType && sourceColumn.linkType() != targetColumn.linkType) {
					// リンクタイプが一致するか
					continue;
				}
				return targetColumn;
			}
		}
	};
};

/**
 * 参照元一覧のフィルタリング
 */
Field.prototype.getRelationalSourceColumnFilter = function (targetColumn) {
	var vm = this.parent;
	return function (sourceColumn) {
		var referrerKeys = targetColumn.relationalConstraints.referrerKeys;
		for (var i=0; i<referrerKeys.length; i++) {
			if (referrerKeys[i] == sourceColumn.type) {
				// 参照先の許可type（一覧）に、参照元typeが一致するか（含まれるか）
				if (targetColumn.relationalConstraints
						&& targetColumn.relationalConstraints.lookupOrigin) {
					var lookupOrigin = targetColumn.relationalConstraints.lookupOrigin;
					// 参照先がルックアップ先を持っている場合、参照先の本来の型はルックアップ先の型。
					// 参照元が、本来の参照先型の参照許可type一覧に含まれるか？
					var referrerKeys = vm.referrerKeys(lookupOrigin.type);
					if (referrerKeys == null || referrerKeys.indexOf(sourceColumn.type) == -1) {
						continue;
					}
					if (sourceColumn.linkType && sourceColumn.linkType() != lookupOrigin.linkType) {
						continue;
					}
					return sourceColumn;
				} else if (sourceColumn.linkType && targetColumn.linkType && sourceColumn.linkType() != targetColumn.linkType) {
					// リンクタイプが一致するか
					continue;
				}
				return sourceColumn;
			}
		}
	};
};

Field.prototype.loadRelationalTargetColumnOptions = function (tableId) {
	var self = this;

	self.referenceTargetColumnOptions.removeAll();
	self.referenceSortableOptions.removeAll();
	self.referenceListableOptions.removeAll();
	self.referenceAggregatableOptions.removeAll();

	if (!tableId) return;

	return $.ajax({
		url: '/my/table/referenceColumns.json',
		data: {t: tableId},
		success: function(json) {
			var referenceTargetColumnOptions = [],
			referenceSortableOptions = [],
			referenceListableOptions = [];
			referenceAggregatableOptions = [];
			$.each(json.result, function(i, column) {
				if (column.relationalConstraints && column.relationalConstraints.referable == true)
					referenceTargetColumnOptions.push(column);
				if (column.isSortable == true)
					referenceSortableOptions.push(column);
				if (column.isListable == true)
					referenceListableOptions.push(column);
				if (column.isListable == true && ($.inArray(column.type, ['rownum','number','calc', 'datecalc']) >= 0))
					referenceAggregatableOptions.push(column);
			});
			self.referenceTargetColumnOptions(referenceTargetColumnOptions);
			self.referenceSortableOptions(referenceSortableOptions);
			self.referenceListableOptions(referenceListableOptions);
			self.referenceAggregatableOptions(referenceAggregatableOptions);
		},
		async: false,
		dataType: 'json'
	});
};

Field.prototype.loadRelationalInputTargetColumnOptions = function (tableId) {
	var self = this;

	self.referenceSortableOptions.removeAll();
	self.referenceListableOptions.removeAll();

	if (!tableId) return;

	return $.ajax({
		url: '/my/table/referenceColumns.json',
		data: {t: tableId},
		success: function(json) {
			var referenceSortableOptions = [],
			referenceListableOptions = [];
			$.each(json.result, function(i, column) {
				if (column.isSortable == true)
					referenceSortableOptions.push(column);
				if (column.isListable == true)
					referenceListableOptions.push(column);
			});
			self.referenceSortableOptions(referenceSortableOptions);
			self.referenceListableOptions(referenceListableOptions);
		},
		async: false,
		dataType: 'json'
	});
};



Field.prototype.beforeShowModal = function () {
	var self = this;

	if ('options' in self) {
		self.addedOptionIds = [];
		self.removedOptionIds = [];
	}

	if ('tabs' in self) {
		self.addedTabs = [];
		self.removedTabs = [];
	}

	if ('dateSourceColumnId' in self) {
		self.loadDateSourceColumnOptions(true);
		self.loadDatecalcUnitColumnOptions(true);
		self.loadDatecalcBeforeAfterColumnOptions(true);
	}

	if ('referenceTableId' in self) {
		if (self.type == 'lookup') {
			self.loadLookupTableOptions();
			self.loadLookupSourceColumnOptions();
			self.loadLookupTargetColumnOptions(self.referenceTableId());
		}
		if (self.type == 'relational') {
			self.loadRelationalTableOptions();
			self.loadRelationalSourceColumnOptions();
			self.loadRelationalTargetColumnOptions(self.referenceTableId());
		}
		if (self.type == 'relational_input') {
			self.loadRelationalInputTableOptions();
			self.loadRelationalInputTargetColumnOptions(self.referenceTableId());
		}
		self.select2OptionsForTable = {
			placeholder: 'DBを選択してください',
			data: { results: self.referenceTableOptions(), text: 'name' },
			formatResult: self.formatSelect2OptionsForTable,
			formatSelection: self.formatSelect2OptionsForTable,
			escapeMarkup: function(m) { return m; }
		}
	}
};

Field.prototype.formatSelect2OptionsForTable = function(table) {
    if (!table.id) return table.name;
    return "<img class='select-icon' src='"+table.icon.url+"'/>" + table.name;
};

Field.prototype.props = {
	'id' : {  },
	'code' : {  },
	//'type' : {  },
	'label' : {  },
	'options' : {  },
	'showsField' : {  },
	'showsLabel' : {  },
	'positionY' : {  },
	'positionX' : {  },
	'sizeWidth' : {  },
	'sizeHeight' : {  },
	'isUnique' : {  },
	'isRequired' : {  },
	'minLength' : {  },
	'maxLength' : {  },
	'minValue' : {  },
	'maxValue' : {  },
	'defaultValue' : {  },
	//'defaultValueId' : {  }, // not property
	//'defaultValueIds' : {  }, // not property
	'isDefaultNow' : {  }, // not property
	'expression' : {  },
	'isCalculative' : {  },
	'format' : {  },
	'showsDigit' : {  },
	'labelHtml' : {  },
	'thumbnailMaxSize' : {  },
	'imageMaxSize' : {  },
	'linkType' : {  },
	//'referenceTable' : {  }, // not property
	'referenceTableId' : {  },
	'referenceTargetColumnId' : {  },
	//'referenceTargetColumnType' : {  }, // not property
	'referenceSourceColumnId' : {  },
	'referenceColumnIds' : {  },
	'referenceSortColumnId' : {  },
	'referenceSortsReverse' : {  },
	'referenceMaxRow' : {  },
	'referenceCopyMaps' : {  },
	'referenceAggregations' : {  },
	// datecalc
	'dateFormat' : {  },
	'dateSourceColumnId' : { },
	'datecalcUnit' : { },
	'datecalcBeforeAfter' : { },
//	'referenceCopyMapIds' : {  },
	'optionsAlignment' : {  },
	'tabs' : {  } // not property, but validation needed.
};

// validate from modal
Field.prototype.validateTemp = function () {
	var self = this;
	var values = {};

	for (var i in self.props ) {
		if (i in self) {
			if ( ko.isObservable(self[i])
					&& ('temp' in self[i]) ) {
				values[i] = self[i].temp();
			} else {
				values[i] = ko.utils.unwrapObservable(self[i]);
			}
		}
	}
	return self._validate(values);
}

// validate from out of modal
Field.prototype.validate = function () {
	var self = this;
	var result = self.validateTemp();

	for (var i in self.props ) {
		if (i in self) {
			if ( ko.isObservable(self[i])
					&& ('error' in self[i]) ) {
				self[i].error(null);
			}
		}
	}

	return result;
}

Field.prototype.validateRemove = function () {
	var self = this;
	var messages = [];
	if (self.type == 'tab') {
		var tab = self;
		for (var i=0; i<tab.rows().length; i++) {
			if (tab.rows()[i].fields().length > 0) {
				messages.push('タブの中にフィールドがあるため削除できません。');
				return messages;
			}
		}
	}
	if (self.type == 'tab_group') {
		for (var t=0; t<self.tabs().length; t++) {
			var tab = self.tabs()[t];
			for (var i=0; i<tab.rows().length; i++) {
				if (tab.rows()[i].fields().length > 0) {
					messages.push('タブの中にフィールドがあるため削除できません。');
					return messages;
				}
			}
		}
	}
	return messages;
}

Field.prototype.handleAfterRemove = function () {
	var self = this;
	if (self.type == 'tab_group') {
		for (var t=0; t<self.tabs().length; t++) {
			var tab = self.tabs()[t];
			var index = self.parent.fields.indexOf(tab);
			self.parent.fields.splice(index, 1);
		}
	}
}

Field.prototype.styleMenu = function(sender) {
	if (!sender.isMenuEnabled()) {
		return 'restricted';
	}
	return null;
}

Field.prototype.isMenuEnabled = function (sender) {
	var self = sender || this;
	if (self.type == 'relational_input') {
		if (self.parent.isChild) {
			return false;
		}
	}
	return true;
}

Field.prototype.initUniqueRestricted = function() {
	var self = this;
	if ('isUniqueRestricted' in self) {
		self.isUniqueRestricted = self.initialUniqueDisabled();
		if (self.isUniqueRestricted()) {
			self.isUnique.temp(false);
			self.isUnique.commit();
		}
	}
}

Field.prototype.initialUniqueDisabled = function() {
	var self = this;
	if (self.parent.isChild) {
		return ko.observable(true);
	}
	if (self.type == 'text') {
		return self.isCalculative.temp;
	}
	return ko.observable(false);
}

Field.prototype._validate = function (values) {
	var self = this;

	self.error(null);

	for (var i in self.props ) {
		if (i in self) {
			if ( ko.isObservable(self[i])
					&& ('error' in self[i]) ) {
				self[i].error(null);
			}
		}
	}

	if ('minLength' in values) {
		if (values.minLength != null && values.minLength !== '') {
			if (! /^[0-9]+$/.test(values.minLength)) {
				self.minLength.error('数字を入力してください。');
			}
		}
	}

	if ('maxLength' in values) {
		if (values.maxLength != null && values.maxLength !== '') {
			if (! /^[0-9]+$/.test(values.maxLength)) {
				self.maxLength.error('数字を入力してください。');
			}
		}
	}

	if ('options' in values) {
		var value = values.options;
		if (value.length == 0) {
			self.options.error('選択肢を作成してください。');
		} else {
			var blank = false;
			for (var i=0; i<value.length; i++) {
				if (value[i].name.temp() == '') {
					blank = true;
					break;
				}
			}
			if (blank) {
				self.options.error('選択肢名を入力してください。');
			} else {
				var duplicate = (function () {
					for (var i=0; i<value.length-1; i++) {
						for (var j=i+1; j<value.length; j++) {
							if (value[i].name.temp() == value[j].name.temp())
								return true;
						}
					}
					return false;
				})();
				if (duplicate) {
					self.options.error('選択肢が重複しています。');
				}
			}
		}
	}

	if ('expression' in values) {
		var value = values.expression;
		var isCalculative = true;
		if ('isCalculative' in values) {
			isCalculative = values.isCalculative;
		}
		if (isCalculative) {
			if (value == null || value == '') {
				self.expression.error('入力してください。');
			}
		}
	}

	if ('referenceTableId' in values) {
		var value = values.referenceTableId;
		if (String.isEmpty(value)) {
			self.referenceTableId.error('入力してください。');
		}
	}

	if ('referenceTargetColumnId' in values) {
		var value = values.referenceTargetColumnId;
		if (String.isEmpty(value)) {
			self.referenceTargetColumnId.error('入力してください。');
		}
	}

	if ('referenceSourceColumnId' in values) {
		var value = values.referenceSourceColumnId;
		if (String.isEmpty(value)) {
			self.referenceSourceColumnId.error('入力してください。');
		}
	}

	if ('referenceCopyMaps' in values) {
		var maps = values.referenceCopyMaps;
		for (var i=0; i<maps.length; i++) {
			if (String.isEmpty(maps[i].acceptantId.temp()) || String.isEmpty(maps[i].accepteeId.temp())) {
				self.referenceCopyMaps.error('入力してください。');
				break;
			}
		}
		if (!self.referenceCopyMaps.error()) {
			if (maps.length > 0) {
				for (var i=0; i<maps.length-1; i++) {
					for (var j=i+1; j<maps.length; j++) {
						if (maps[i].acceptantId.temp() == maps[j].acceptantId.temp()) {
							self.referenceCopyMaps.error('コピー先が重複しています。');
							break;
						}
					}
				}
			}
		}
		if (!self.referenceCopyMaps.error()) {
			var fields = self.parent.fields;
			for (var i=0; i<fields.length; i++) {
				if (self != fields[i]) {
					if (fields[i].type == 'lookup') {
						var otherMaps = fields[i].referenceCopyMaps();
						for (var j=0; j<maps.length; j++) {
							for (var k=0; k<otherMaps.length; k++) {
								if (maps[j].acceptantId.temp() == otherMaps[k].acceptantId()) {
									self.referenceCopyMaps.error('コピー先が他の参照コピーフィールドと重複しています。');
									break;
								}
							}
						}
					}
				}
			}
		}
	}

	if ('referenceAggregations' in values) {
		var value = values.referenceAggregations;
		for (var i=0; i < value.length; i++){
			if(value[i].fn.length == 0){
				self.referenceAggregations.error('集計方法を選択してください。');
			}
		}
		var duplicate = (function () {	
			for (var i=0; i<value.length-1; i++) {
				for (var j=i+1; j<value.length; j++) {
					if (value[i].id == value[j].id) return true;
				}
			}
			return false;
		})();
		if (duplicate) {
			self.referenceAggregations.error('選択肢が重複しています。');	
		}
	}

	if (self.type == 'relational') {
		var value = values.referenceColumnIds;
		if (value.length == 0) {
			self.referenceColumnIds.error('入力してください。');
		}
	}

	if (self.type == 'relational_input') {
		var value = values.referenceColumnIds;
		if (value.length == 0) {
			self.referenceColumnIds.error('入力してください。');
		}
	}

	if ('dateSourceColumnId' in values) {
		var value = values.dateSourceColumnId;
		if (String.isEmpty(value)) {
			self.dateSourceColumnId.error('入力してください。');
		}
	}

	if ('datecalcUnit' in values) {
		var value = values.datecalcUnit;
		if (String.isEmpty(value)) {
			self.datecalcUnit.error('入力してください。');
		}
	}

	if ('datecalcBeforeAfter' in values) {
		var value = values.datecalcBeforeAfter;
		if (String.isEmpty(value)) {
			self.datecalcBeforeAfter.error('入力してください。');
		}
	}

	if ('tabs' in values) {
		var value = values.tabs;
		if (value.length == 0) {
			self.tabs.error('タブを作成してください。');
		} else {
			var blank = false;
			for (var i=0; i<value.length; i++) {
				if (value[i].label.temp() == '') {
					blank = true;
					break;
				}
			}
			if (blank) {
				self.tabs.error('タブ名を入力してください。');
			}
		}
	}

	for (var i in self.props ) {
		if (i in self) {
			if ( ko.isObservable(self[i])
					&& ('error' in self[i]) ) {
				if (self[i].error() != null) {
					return false;
					break;
				}
			}
		}
	}

	return true;

};

Field.prototype.propertyFor = function (name) {
	var self = this;
	var property = null;
	if (name in self) {
		property = self[name];
	}
	return property;
};

Field.prototype.sendUpdate = function (sender) {
	var self = this;
	var success = true;
	var data = self.parent.toDataForColumnUpdate(self);
	$.ajax({
		type: 'POST',
		url: '/my/table/updateColumn.json',
		data: {data: ko.toJSON(data)},
		dataType: 'json',
		async: false,
		success: function (json) {
			if (json.status == 'success') {
			} else if (json.status == 'error') {
				success = false;
				var messages = [];
				$.each(json.errors, function(i, error) {
					if (error.context != null && error.context.property != null && self.propertyFor(error.context.property)) {
						self.propertyFor(error.context.property).error(error.message);
					} else {
						messages.push(error.message);
					}
				});
				if (messages.length > 0) {
					alert(messages.join("\n"));
				}
			}
		}
	});
	return success;
}

Field.prototype.save = function (sender) {

	var self = this; // or sender

	// validation

	if (!self.validateTemp()) {
		return;
	};

	if (!self.sendUpdate()) {
		return;
	}

	// commit values
	for (var i in self.props ) {
		if (i in self) {
			if ( ko.isObservable(self[i])
					&& ('commit' in self[i]) ) {
				self[i].commit();
			}
		}
	}

	// update relational header
	if ('referenceColumnLabels' in self) {
		self.loadReferenceColumnLabels(self.referenceTableId(), self.referenceColumnIds());
	}

	self.parent.reflectFieldChanges(self);

	this.modal.close('commited');
};

Field.prototype.getValues = function (isTemp) {
	var self = this;
	var values = {};
	for (var i in self.props ) {
		if (i in self) {
			if ( ko.isObservable(self[i])
					&& ('temp' in self[i]) ) {
				values[i] = isTemp ? self[i].temp() : self[i]();
			} else {
				values[i] = ko.utils.unwrapObservable(self[i]);
			}
		}
	}
	return values;
}

Field.prototype.toData = function (isTemp) {

	var self = this;
	var parent = this.parent;
	var descriptor = this.descriptor;
	var data = {}
	var values = self.getValues(isTemp);

	data.id = self.id;
	data.label = values.label;
	data.code = values.code;

	// options
	if ('options' in descriptor && descriptor.options != null) {
		if (self.type == 'checkbox') {
			data.defaultValue = [];
			data.options = $.map(
				values.options,
				function(option) {
					return option.toData(isTemp, data.defaultValue);
				}
			);
		} else {
			data.options = $.map(
				values.options,
				function(option) {
					return option.toData(isTemp);
				}
			);
		}
	}

	if ('showsField' in descriptor) data.showsField = values.showsField;
	if ('showsLabel' in descriptor) data.showsLabel = values.showsLabel;
	if ('layoutParentId' in descriptor) data.layoutParentId = self.layoutParentId;
	if ('positionY' in descriptor) data.positionY = self.positionY;
	if ('positionX' in descriptor) data.positionX = self.positionX;
	if ('sizeWidth' in descriptor) data.sizeWidth = values.sizeWidth;
	if ('sizeHeight' in descriptor) data.sizeHeight = values.sizeHeight;
	if ('isUnique' in descriptor) data.isUnique = values.isUnique;
	if ('isRequired' in descriptor) data.isRequired = values.isRequired;
	if ('minLength' in descriptor) data.minLength = values.minLength;
	if ('maxLength' in descriptor) data.maxLength = values.maxLength;
	if ('minValue' in descriptor)
		data.minValue = (values.minValue == null || values.minValue == '') ? null : String(values.minValue);
	if ('maxValue' in descriptor)
		data.maxValue = (values.maxValue == null || values.maxValue == '') ? null : String(values.maxValue);
	if ('defaultValue' in descriptor) {
		if (self.type == 'date' || self.type == 'datetime' || self.type == 'time') {
			if (self.isDefaultNow()) {
				data.defaultValue = 'now';
			} else {
				data.defaultValue = values.defaultValue;
			}
		} else {
			data.defaultValue = values.defaultValue;
		}
	}
	if ('defaultValueId' in descriptor) data.defaultValue = values.defaultValue;
	if ('expression' in descriptor) data.expression = values.expression;
	if ('format' in descriptor) data.format = values.format;
	if ('dateFormat' in descriptor) data.dateFormat = values.dateFormat;
	if ('showsDigit' in descriptor) data.showsDigit = values.showsDigit;
	if ('labelHtml' in descriptor) data.labelHtml = values.labelHtml;
	if ('thumbnailMaxSize' in descriptor) data.thumbnailMaxSize = values.thumbnailMaxSize;
	if ('imageMaxSize' in descriptor) data.imageMaxSize = values.imageMaxSize;
	if ('linkType' in descriptor) data.linkType = values.linkType;
	if ('referenceTableId' in descriptor) data.referenceTableId = values.referenceTableId;
	if ('referenceTargetColumnId' in descriptor) data.referenceTargetColumnId = values.referenceTargetColumnId;
	if ('referenceSourceColumnId' in descriptor) data.referenceSourceColumnId = values.referenceSourceColumnId;
	if ('referenceColumnIds' in descriptor) {
		data.referenceColumnIds = (
			values.referenceColumnIds ?
				$.map(values.referenceColumnIds, function (obj) { return obj.id; }) :  []
		);
	}
	if ('referenceSortColumnId' in descriptor) data.referenceSortColumnId = values.referenceSortColumnId;
	if ('referenceSortsReverse' in descriptor) data.referenceSortsReverse = values.referenceSortsReverse;
	if ('referenceMaxRow' in descriptor) data.referenceMaxRow = values.referenceMaxRow;
	if ('referenceCopyMapIds' in descriptor) {
		data.referenceCopyMapIds = ko.utils.arrayMap(values.referenceCopyMaps,
			function(copyMap){ return copyMap.toData(isTemp); }
		);
	}
	if ('referenceAggregations' in descriptor) {
		data.referenceAggregations = (
			values.referenceAggregations ?
				ko.utils.arrayMap(values.referenceAggregations, function (obj) { return [obj.id, obj.fn]; }) :  []
		);
	}
	if ('optionsAlignment' in descriptor) data.optionsAlignment = values.optionsAlignment;

	if ('dateSourceColumnId' in descriptor) data.dateSourceColumnId = values.dateSourceColumnId;
	if ('datecalcUnit' in descriptor) data.datecalcUnit = values.datecalcUnit;
	if ('datecalcBeforeAfter' in descriptor) data.datecalcBeforeAfter = values.datecalcBeforeAfter;


	return data;
};

Field.prototype.cancel = function (sender) {
	var self = this;

	// 選択肢の変更のキャンセル
	if ('options' in self) {
		self.restoreOptions();
	}

	// タブの変更のキャンセル
	if ('tabs' in self) {
		self.restoreTabs();
	}

	// tempデータのリセット
	this.reset();

	sender.modal.close();
};

Field.prototype.reset = function () {
	var self = this;
	for (var i in self.props ) {
		if (i in self) {
			if (ko.isObservable(self[i])) {
				if ('reset' in self[i] && typeof self[i].reset == 'function') {
					self[i].reset();
				}
			}
		}
	}
};

ko.bindingHandlers.field = {
	init: function(element, valueAccessor, allBindings, viewModel, bindingContext) {

		var cover = $('<div class="tx-field-cover"></div>');
		var html =
			'<div class="tx-field-controls">\
				<div class="tx-field-size-buttons">\
					<span class="tx-field-width-buttons">\
						<button type="button" class="tx-width-minus btn btn-default btn-xs"><span>幅-</span></button>\
						<button type="button" class="tx-width-plus btn btn-default btn-xs"><span>幅+</span></button>\
					</span>\
					<span class="tx-field-height-buttons">\
						<button type="button" class="tx-height-minus btn btn-default btn-xs"><span>高さ-</span></button>\
						<button type="button" class="tx-height-plus btn btn-default btn-xs"><span>高さ+</span></button>\
					</span>\
				</div>\
				<div class="tx-field-config-menu">\
					<div class="btn-group">\
						<button type="button" class="btn btn-default btn-xs dropdown-toggle" data-toggle="dropdown">\
							設定 <span class="caret"></span>\
						</button>\
						<ul class="dropdown-menu pull-right" role="menu">\
							<li class="tx-field-config"><a href="#">開く</a></li>\
							<li class="divider"></li>\
							<li class="tx-field-delete"><a href="#">削除</a></li>\
						</ul>\
					</div>\
				</div>\
			</div>';

		var vm = bindingContext.$root;
		var row = bindingContext.$parent;
		var beforeDragStart = function () {
			vm.allowDropFor(valueAccessor());
		};
		var afterDragStop = function () {
			vm.allowDropAll();
		};
		cover.mousedown(beforeDragStart);
		cover.mouseup(afterDragStop);
		cover.click(function () {
			$('.tx-field-controls').remove();
			$('.tx-field-cover').removeClass('tx-field-cover-active');
			var controls = $(html);
			controls.click(function(event){$(this).remove();});
			controls.mousedown(beforeDragStart);
			controls.mouseup(afterDragStop);
			controls.find('.tx-field-config-menu .dropdown-toggle').click(function(event){
				event.stopPropagation();
				controls.find('.tx-field-config-menu .dropdown-menu').toggle();
			})
			controls.find('.tx-width-plus').click(function(event) {
				event.stopPropagation();
				var fields = row.fields();
				var sum = 0;
				for (var i=0; i<fields.length; i++) {
					sum += fields[i].sizeWidth();
				}
				if (sum+1 > vm.maxWidth()) {
					return false;
				}
				valueAccessor().sizeWidth(valueAccessor().sizeWidth()+1);
			});
			controls.find('.tx-width-minus').click(function(event) {
				event.stopPropagation();
				if (valueAccessor().sizeWidth()-1 < 1) {
					return false;
				}
				valueAccessor().sizeWidth(valueAccessor().sizeWidth()-1);
			});
			controls.find('.tx-height-plus').click(function(event) {
				event.stopPropagation();
				valueAccessor().sizeHeight(valueAccessor().sizeHeight()+1);
			});
			controls.find('.tx-height-minus').click(function(event) {
				event.stopPropagation();
				if (valueAccessor().sizeHeight()-1 < 1) {
					return false;
				}
				valueAccessor().sizeHeight(valueAccessor().sizeHeight()-1);
			});
			controls.find('.tx-field-delete a').click(function() {
				controls.find('.tx-field-config-menu .dropdown-menu').toggle();
				var rowIndex = null;
				var layoutParent = row.parent;
				for (var i=0; i<layoutParent.rows().length; i++) {
					if (row === layoutParent.rows()[i]) {
						rowIndex = i;
						break;
					}
				}
				var messages = valueAccessor().validateRemove();
				if (messages.length > 0) {
					alert(messages[0]);
					return false;
				}
				if (vm.prop(valueAccessor().type, 'isSingle') == true) {
					row.fields.remove(valueAccessor());
					valueAccessor().showsField(false);
				} else {
					var referencedMessage = function () {
						alert('他のフィールドから参照されています。');
						return false;
					};
					for (var i=0; i<vm.fields.length; i++) {
						if ('referenceCopyMaps' in vm.fields[i]) {
							var maps = vm.fields[i].referenceCopyMaps();
							for (var j=0; j<maps.length; j++) {
								if (maps[j].acceptantId() == valueAccessor().id) {
									return referencedMessage();
								}
							}
						}
						if ('referenceSourceColumnId' in vm.fields[i]) {
							if (vm.fields[i].referenceSourceColumnId() == valueAccessor().id) {
								return referencedMessage();
							}
						}
					}

					var confirmMessage = null;
					$.ajax({
						type: 'GET',
						url: '/my/table/confirm_removed.json',
						data: {t: vm.tableId, c: valueAccessor().id},
						dataType: 'json',
						async: false,
						success: function(json) {
							if (json.status == 'success') {
								confirmMessage = json.result;
							}
						}
					})
					if(confirmMessage && !window.confirm(confirmMessage)) {
						return false;
					}

					var removed = false;
					$.ajax({
						type: 'GET',
						url: '/my/table/check_removed.json',
						data: {t: vm.tableId, c: valueAccessor().id},
						dataType: 'json',
						async: false,
						success: function(json) {
							if (json['errors'].length > 0) {
								alert(json['errors'][0].message);
							} else {
								vm.removeColumn(valueAccessor());
								row.fields.remove(valueAccessor());
								removed = true;
							}
						}
					})
					if (!removed) return false;
				}
				// 削除後、行が空行になったら、上下のダミー行を削除する
				if (row.fields().length < 1) {
					if (rowIndex != null) {
						layoutParent.rows.splice(rowIndex + 1, 1);
						layoutParent.rows.splice(rowIndex - 1, 1);
					}
				}
				return false;
			});
			controls.find('.tx-field-config a').click(function() {
				controls.find('.tx-field-config-menu .dropdown-menu').toggle();
				viewModel.beforeShowModal();
				showModal({ viewModel: bindingContext.$data })
					.done(function(result) {

					})
					.fail(function() {

					});
				return false;
			});
			if (!('sizeWidth' in valueAccessor().descriptor)) {
				controls.find('.tx-field-width-buttons').hide();
			}
			if (!('sizeHeight' in valueAccessor().descriptor)) {
				controls.find('.tx-field-height-buttons').hide();
			}
			if (vm.prop(valueAccessor().type, 'hasModal') == false
					|| ('referalError' in valueAccessor() && valueAccessor().referalError() == true) ) {
				controls.find('.tx-field-config').hide();
				controls.find('.tx-field-config-menu li.divider:eq(0)').hide();
			}

			$(element).append(controls);
			$(element).closest_descendent('.tx-field-cover').addClass('tx-field-cover-active');
		});

		$(element).append(cover);
	}
};

//http://aboutcode.net/2012/11/15/twitter-bootstrap-modals-and-knockoutjs.html
var createModalElement = function(templateName, viewModel) {

	var temporaryDiv = addHiddenDivToBody();
	var deferredElement = $.Deferred();
	ko.renderTemplate(
			templateName,
			viewModel,
			// We need to know when the template has been rendered,
			// so we can get the resulting DOM element.
			// The resolve function receives the element.
			{
					afterRender: function (nodes) {
							// Ignore any #text nodes before and after the modal element.
							var elements = nodes.filter(function(node) {
									return node.nodeType === 1; // Element
							});
							deferredElement.resolve(elements[0]);
							$(elements[0]).iOsModalFix();
					}
			},
			// The temporary div will get replaced by the rendered template output.
			temporaryDiv,
			"replaceNode"
	);

	// Return the deferred DOM element so callers can wait until it's ready for use.
	return deferredElement;
};

var addHiddenDivToBody = function() {
	var div = document.createElement("div");
	div.style.display = "none";
	document.body.appendChild(div);
	return div;
};

var addModalHelperToViewModel = function (viewModel, deferredModalResult, context) {
	// Provide a way for the viewModel to close the modal and pass back a result.
	viewModel.modal = {
			close: function (result) {
					if (typeof result !== "undefined") {
							deferredModalResult.resolveWith(context, [result]);
					} else {
							// When result is undefined, we don't want any `done` callbacks of
							// the deferred being called. So reject instead of resolve.
							deferredModalResult.rejectWith(context, []);
					}
			}
	};
};

var showTwitterBootstrapModal = function($ui) {

	// Display the modal UI using Twitter Bootstrap's modal plug-in.
	$ui.modal({
			// Clicking the backdrop, or pressing Escape, shouldn't automatically close the modal by default.
			// The view model should remain in control of when to close.
			backdrop: "static",
			keyboard: false
	});

};

var whenModalResultCompleteThenHideUI = function (deferredModalResult, $ui) {
	// When modal is closed (with or without a result)
	// Then always hide the UI.
	deferredModalResult.always(function () {
			$ui.modal("hide");
	});
};

var whenUIHiddenThenRemoveUI = function($ui) {
	// Hiding the modal can result in an animation.
	// The `hidden` event is raised after the animation finishes,
	// so this is the right time to remove the UI element.
	$ui.on("hidden", function() {
			// Call ko.cleanNode before removal to prevent memory leaks.
			$ui.each(function (index, element) { ko.cleanNode(element); });
			$ui.remove();
	});
};

var showModal = function(options) {

	if (typeof options === "undefined") throw new Error("An options argument is required.");
	if (typeof options.viewModel !== "object") throw new Error("options.viewModel is required.");

	var viewModel = options.viewModel;
	var template = options.tplName || viewModel.modalTplName();
	var context = options.context;

	if (!template) throw new Error("options.template or options.viewModel.template is required.");

	return createModalElement(template, viewModel)
			.pipe($) // jQueryify the DOM element
			.pipe(function($ui) {
					var deferredModalResult = $.Deferred();
					addModalHelperToViewModel(viewModel, deferredModalResult, context);
					showTwitterBootstrapModal($ui);
					whenModalResultCompleteThenHideUI(deferredModalResult, $ui);
					whenUIHiddenThenRemoveUI($ui);
					return deferredModalResult;
			});
};

var ViewModel  = function(tableId, isChild, isSixColumns) {
	var self = this;
	self.tableId = tableId;
	self.rows = ko.observableArray();
	self.fields = [];
	self.items = ko.observableArray();
	self.baseItems = ko.observableArray();
	self.fieldDesciptors = null;
	self.error = ko.observable(null);
	self.previewWindow = null;
	self.isChild = isChild;
	self.isSixColumns = isSixColumns;
	self.maxw = 3;
	self.wunit = 4;

	if (isSixColumns) {
		self.maxw = 6;
		self.wunit = 2;
	}

	self.sortableOptions = {
		start: function(event, ui) {
			// remove selection
			$('.tx-field-cover').removeClass('tx-field-cover-active');
			$('.tx-field-controls').remove();
			ui.item.find('.tx-field-cover').addClass('tx-field-cover-active');
			// resize placeholder
			ui.placeholder.height(ui.item.height());
			var uiItemId = ui.item.data('uiitemid');
			var uiItemIdRe = new RegExp('^[0-9]+$');
			var field = uiItemIdRe.test(uiItemId) ? self.fieldFor(uiItemId) : self.itemFor(uiItemId);
			var width = field.sizeWidth()
			ui.placeholder.toggleClass(field.styleWidth(), true);
		},
		sort : function(event, ui) {
			$('.tx-row').removeClass('tx-row-hover');
			$(ui.placeholder).closest('.tx-row').toggleClass('tx-row-hover',true);
		},
		out : function(event, ui) {
			$('.tx-row').removeClass('tx-row-hover');
		},
		stop : function(event, ui) {
			$('.tx-row').removeClass('tx-row-hover');
			ui.item.find('.tx-field-cover').removeClass('tx-field-cover-active');
			self.allowDropAll();
		},
		over : function(event, ui) {
			ui.helper.css('z-index', 1000000);
		},
		placeholder: 'tx-sortable-placeholder',
		tolerance: 'pointer',
		handle: '.tx-field-cover,.tx-field-controls',
		cursorAt : { top: 25, left: 80 } // this works well around floating
	};

	self.draggableOptions = {
		start : function(event, ui) {
			ui.helper.css('z-index', 1000000);
		},
		stop : function(event, ui) {
			self.allowDropAll();
		},
		appendTo: 'body'
	};


	self.load();
};

ViewModel.prototype.fieldProperties = {
		'text' : {
			'name' : '文字列（一行）'
		},
		'multitext' : {
			'name' : '文字列（複数行）'
		},
		'richtext' : {
			'name' : '装飾テキスト'
		},
		'number' : {
			'name' : '数値'
		},
		'calc' : {
			'name' : '計算'
		},
		'radio' : {
			'name' : '選択肢'
		},
		'checkbox' : {
			'name' : '選択肢（複数）'
		},
		'dropdown' : {
			'name' : 'プルダウン選択肢'
		},
		'user_selector' : {
			'name' : 'ユーザー選択'
		},
		'date' : {
			'name' : '日付'
		},
		'time' : {
			'name' : '時刻'
		},
		'datetime' : {
			'name' : '日時'
		},
		'datecalc' : {
			'name' : '日時計算'
		},
		'file' : {
			'name' : '添付ファイル'
		},
		'link' : {
			'name' : 'リンク'
		},
		'relational' : {
			'name' : '関係データ一覧'
		},
		'relational_input' : {
			'name' : '明細入力'
		},
		'lookup' : {
			'name' : '参照コピー'
		},
		'rownum' : {
			'name' : 'データ番号'
				,'isSingle' : true
		},
		'created_by' : {
			'name' : '作成者'
				,'isSingle' : true
		},
		'created_on' : {
			'name' : '作成日時'
				,'isSingle' : true
		},
		'updated_by' : {
			'name' : '更新者'
				,'isSingle' : true
		},
		'updated_on' : {
			'name' : '更新日時'
				,'isSingle' : true
		},
		'label' : {
			'name' : '注意書き'
		},
		'space' : {
			'name' : '空白'
				//,'hasModal' : false
		},
		'line' : {
			'name' : '罫線'
				,'hasModal' : false
		},
		'tab_group' : {
			'name' : 'タブグループ'
				,'hasModal' : true
		},
		'tab' : {
			'name' : 'タブ'
				,'hasModal' : false
				,'draggable' : false
		}

};

ViewModel.prototype.descriptor = function(type) {
	return this.fieldDescriptors[type];
};

ViewModel.prototype.referrerKeys = function(type) {
	if ('relationalConstraints' in this.fieldDescriptors[type]
		&& 'referrerKeys' in this.fieldDescriptors[type].relationalConstraints) {
		return this.fieldDescriptors[type].relationalConstraints.referrerKeys;
	}
	return null;
};

ViewModel.prototype.maxWidth = function() {
	return this.maxw;
};

ViewModel.prototype.widthUnit = function() {
	return this.wunit;
};

ViewModel.prototype.modalTplName = function(data) {
	return data.type + '-modal-tpl';
};

ViewModel.prototype.itemClassName = function(data) {
	return 'fielditem-' + data.type;
};

ViewModel.prototype.load = function() {
	var self = this;
	var types = $.map(self.fieldProperties, function(value, key){
		return key;
	});
	$.when(
		$.ajax({url: '/my/table/formColumns.json', data: {t:self.tableId}, dataType: 'json'}),
		$.ajax({url: '/my/table/columnPropertyDescriptors.json', data: {types:types}, dataType: 'json'})
	).done(function (result1, result2) {self.render(result1, result2);});
};

ViewModel.prototype.render = function(result1, result2) {
	var self = this;
	var json = result1[0];
	self.fieldDescriptors = result2[0].result;

	// アイテムメニュー
	for (var type in self.fieldProperties) {
		if (self.prop(type, 'isSingle') == true
				|| self.prop(type, 'draggable') == false) {
			continue;
		}
		self.items.push(new Field({id: null, type: type, label: self.prop(type, 'name')}, self));
	}

	self.fields = $.map(json.result, function(data) {
		return new Field(data, self);
	});

	var tabGroups = [];
	for (var i=0; i<self.fields.length; i++) {
		if (self.fields[i].type == 'tab_group') {
			tabGroups.push(self.fields[i]);
		}
	}
	for (var i=0; i<self.fields.length; i++) {
		if (self.fields[i].type == 'tab') {
			var tab = self.fields[i];
			var layoutParent = self.fieldFor(tab.layoutParentId);
			layoutParent.tabs.temp.push(tab);
			layoutParent.tabs.commit();
		}
	}

	// フォームに設置するアイテム
	var rows = [];
	for (var i=0; i<self.fields.length; i++) {
		if (self.fields[i].type == 'tab') {
			continue;
		}
		var y = self.fields[i].positionY;
		if (self.fields[i].showsField()) {
			var field = self.fields[i];
			if (field.layoutParentId) { // fields in tabs
				for (var j=0; j<tabGroups.length; j++) {
					var tabGroup = tabGroups[j];
					for (var k=0; k<tabGroup.tabs().length; k++) {
						var tab = tabGroup.tabs()[k];
						if (tab.id == field.layoutParentId) {
							var tabNo = self.fields[i].tabNo;
							if(tab.rows()[y] == null) {
								var tabRows = tab.rows();
								tabRows[y] = new Row(tab);
								tab.rows(tabRows);
							}
							tab.rows()[y].fields.push(field);
							break;
						}
					}
				}
			} else {
				if (rows[y] == null) {
					rows[y] = new Row(self);
				}
				rows[y].fields.push(self.fields[i]);
			}

		}
		if (self.prop(self.fields[i].type, 'isSingle') == true) {
			self.baseItems.push(self.fields[i]);
		}
	}

	var _noLayoutParentRows = rows;

	for (var g=0; g<tabGroups.length; g++) {
		var tabGroup = tabGroups[g];
		var tabs = tabGroup.tabs();
		// sorting tabs.
		tabs.sort(function (a, b) {
			if (a.positionX < b.positionX) {
				return -1
			}
			if (a.positionX > b.positionX) {
				return 1;
			}
			return 0;
		});
		tabGroup.tabs.temp(tabs);
		tabGroup.tabs.commit();
		// sorting fields in tabs.
		for(var t=0; t<tabGroup.tabs().length; t++) {
			var tab = tabGroup.tabs()[t];
			var rows = tab.rows();
			var sortedRows = [];
			for (var i=0; i<rows.length; i++) {
				if (rows[i] == null) continue; // 空の行は削除
				rows[i].fields.sort(function (a, b) {
					if (a.positionX < b.positionX) {
						return -1
					}
					if (a.positionX > b.positionX) {
						return 1;
					}
					return 0;
				});
				sortedRows.push(rows[i]);
			}

			var sparseRows = [];
			for (var i=0; i<sortedRows.length; i++) {
				sparseRows.push(new Row(tab));
				sparseRows.push(sortedRows[i]);
			}
			sparseRows.push(new Row(tab));
			tab.rows(sparseRows);
		}
	}

	rows = _noLayoutParentRows;

	var sortedRows = [];
	for (var i=0; i<rows.length; i++) {
		if (rows[i] == null) continue; // 空の行は削除
		rows[i].fields.sort(function (a, b) {
			if (a.positionX < b.positionX) {
				return -1
			}
			if (a.positionX > b.positionX) {
				return 1;
			}
			return 0;
		});
		sortedRows.push(rows[i]);
	}

	var sparseRows = [];
	for (var i=0; i<sortedRows.length; i++) {
		sparseRows.push(new Row(self));
		sparseRows.push(sortedRows[i]);
	}
	sparseRows.push(new Row(self));
	self.rows(sparseRows);

	$('#tx-side-affix').css({position:'relative'});
	var moveSidebar = function() {
		if ($(window).scrollTop() < $('#tx-side').offset().top) {
			$('#tx-side-affix').css({top:0});
		} else if ( ($(window).scrollTop() + $(window).height()) >
				($('#tx-main .panel').offset().top + $('#tx-main .panel').height()) ) {
			$('#tx-side-affix').css({top:$('#tx-main .panel').height() - $('#tx-side-affix').height()});
		} else {
			$('#tx-side-affix').css({top:$(window).scrollTop() - $('#tx-side').offset().top});
		}
	};
	var resizeSidebar = function() {
		$('#tx-side-affix').height( $(window).height() - $('#tx-side').offset().top );
	};
	resizeSidebar();
	moveSidebar()
	$(window).resize(resizeSidebar);
	$(window).scroll(moveSidebar);

	$( ".tx-row" ).disableSelection();

	window.setTimeout(function(){
		$('.tx-item').mousedown(function () {
			var uiItemId = $(this).data('uiitemid');
			var uiItemIdRe = new RegExp('^[0-9]+$');
			var field = uiItemIdRe.test(uiItemId) ? self.fieldFor(uiItemId) : self.itemFor(uiItemId);
			self.allowDropFor(field);
		});
		$('.tx-item').mouseup(function () {
			self.allowDropAll();
		});
	}, 0);
};

ViewModel.prototype.fieldFor = function(id) {
	var fields = this.fields;
	for (var i = 0; i < fields.length; i++) {
		if (fields[i].id == id) {
			return fields[i];
		}
	}
	return null;
}

ViewModel.prototype.itemFor = function(type) {
	var items = this.items();
	for (var i = 0; i < items.length; i++) {
		if (items[i].type == type) {
			return items[i];
		}
	}
	return null;
}


ViewModel.prototype.validateFields = function () {
	var self = this;
	var hasError = false;

	for (var i=0; i<self.fields.length; i++) {
		if(!self.fields[i].validate()) {
			hasError = true;
			self.fields[i].error('設定してください。');
		}
	}

	return !hasError;
};

ViewModel.prototype.toData = function() {
	var self = this;

	var fieldsData = [];
	for (var i = 0; i < self.fields.length; i++) {
		fieldsData.push(self.fields[i].toData());
	}

	var data = {
		id: self.tableId,
		defs: fieldsData
	};

	return data;
};

ViewModel.prototype.toDataForColumnUpdate = function(field) {
	var self = this;

	var fieldsData = [];
	for (var i = 0; i < self.fields.length; i++) {
		if (field.id != self.fields[i].id) {
			fieldsData.push(self.fields[i].toData());
		}
	}

	var data = {
			id: self.tableId,
			defs: fieldsData,
			temp: field.toData(true)
	};

	return data;
};


ViewModel.prototype.serialize = function() {
	var self = this;
	var data = self.toData();
	return ko.toJSON(data);
};

ViewModel.prototype.prop = function(type, name) {
	var props = this.fieldProperties[type];
	return (name in props) ? props[name] : null;
};

ViewModel.prototype.getDefaultFieldLabel = function (type) {
	return this.prop(type, 'name');
};

ViewModel.prototype.generateFieldCode = function (type) {
	var self = this;

	var name = self.prop(type, 'name');
	var newCode = name;

	var isCodeDuplicated = false;
	var maxSeq = 0;

	for (var i = 0; i < self.fields.length; i++) {
		var code = self.fields[i].code();
		if (newCode == code) {
			isCodeDuplicated = true;
		}
		var result = new RegExp('^'+name+'_([0-9]+)$').exec(code);
		if (result && result.hasOwnProperty(1)) {
			var seq = parseInt(result[1]);
			if (seq > maxSeq) {
				maxSeq = seq;
			}
		}
	}

	if (isCodeDuplicated) {
		newCode = name + '_' + (maxSeq + 1);
	}

	return newCode;
}

ViewModel.prototype.update = function () {
	var self = this;

	self.updatePosition();

	for (var i = 0; i < self.fields.length; i++) {
		self.reflectFieldChanges(self.fields[i]);
	}
};

ViewModel.prototype.updatePosition = function() {
	var self = this;
	var rows = self.rows();

	for (var i = 0; i < self.fields.length; i++) {
		self.fields[i].layoutParentId = null;
		self.fields[i].positionY = null;
		self.fields[i].positionX = null;
	}

	for (var i = 0; i < rows.length; i++) {
		var fields = rows[i].fields();
		for (var j = 0; j < fields.length; j++) {
			fields[j].layoutParentId = null;
			fields[j].positionY = i + 1;
			fields[j].positionX = j + 1;
		}
	}

	// nested fields
	for (var g = 0; g < self.fields.length; g++) {
		if (self.fields[g].type == 'tab_group') {
			var tabGroup = self.fields[g];
			for (var t = 0; t < tabGroup.tabs().length; t++) {
				var tab = tabGroup.tabs()[t];
				tab.positionX = t + 1;
				tab.layoutParentId = tabGroup.id;
				var rows = tab.rows();
				for (var i = 0; i < rows.length; i++) {
					var fields = rows[i].fields();
					for (var j = 0; j < fields.length; j++) {
						fields[j].layoutParentId = tab.id;
						fields[j].positionY = i + 1;
						fields[j].positionX = j + 1;
					}
				}
			}
		}
	}
};

ViewModel.prototype.reflectFieldChanges = function (field) {
	var self = this;

	if ('referenceCopyMaps' in field) {
		// Clear several settings of fields in copymaps.
		var maps = field.referenceCopyMaps();
		for (var i=0; i<maps.length; i++) {
			var acceptant = self.fieldFor(maps[i].acceptantId());
			if ('defaultValue' in acceptant) {
				if (acceptant.type == 'dropdown' || acceptant.type == 'radio') {
					acceptant.defaultValue.temp('');
				} else {
					acceptant.defaultValue.temp(null);
				}
				acceptant.defaultValue.commit();
			}
			if ('isDefaultNow' in acceptant) {
				acceptant.isDefaultNow.temp(false);
				acceptant.isDefaultNow.commit();
			}
			if ('expression' in acceptant) {
				acceptant.expression.temp(null);
				acceptant.expression.commit();
				acceptant.isCalculative.temp(false);
				acceptant.isCalculative.commit();
			}
		}
	}

	if ('tabs' in field) {
		for (var i=0; i<field.tabs().length; i++) {
			field.tabs()[i].label.commit();
		}
	}

};

ViewModel.prototype.preview = function() {
	var self = this;
	self.update();

	if (!self.validateFields()) {
		alert('設定されていない項目があります。');
		return false;
	}

	var data = self.serialize();

	if (self.previewWindow) {
		self.previewWindow.close();
	}
	var url='/my/table/preview?'+$.param({intermediary:true});
	var name='tx_table_form_preview_t' + self.tableId;
	var features='menubar=no,location=yes,resizable=yes,scrollbars=yes,status=yes';
	features+=',top=0,left=0';
	var w = window.open(url, name, features);
	self.previewWindow = w;
	$.ajax({
		type: "POST",
		url: '/my/table/updateForm.json',
		data: {data: data},
		success: function(json) {
			if (json.status == 'success') {
				w.location.href = '/my/table/preview?'+$.param({t:json.result});
				w.focus();
			} else if (json.status == 'error') {
				w.close();
				self.previewWindow = null;
			}
		},
		dataType: 'json',
		async: false
	});
};


ViewModel.prototype.save = function() {
	var self = this;
	self.update();

	if (!self.validateFields()) {
		alert('設定されていない項目があります。');
		return false;
	}

	var data = self.serialize();

	// Clear errors.
	$.each(self.fields, function(i, field) {
		field.error(null);
	});

	$.post('/my/table/saveForm.json', {data: data}, function(json) {
		if (json.status == 'success') {
			document.location.href='/my/table/modify?'+$.param({t:json.result});
		} else if (json.status == 'error') {
			var messages = [];
			$.each(json.errors, function(i, error) {
				if (error.context != null && error.context.id != null) {
					self.fieldFor(error.context.id).error('設定してください。');
				} else {
					messages.push(error.message);
				}
			});
			if (messages.length > 0) {
				alert(messages.join("\n"));
			}
		}
	}, 'json');
};


ViewModel.prototype.dragged = function (item) {
	if (item.id == null) {
		var newItem;
		$.ajax({
			type: 'POST',
			url: '/my/table/addColumn.json',
			data: {t: item.parent.tableId, type: item.type},
			async: false,
			success: function(data) {
				newItem =  new Field(data.result, item.parent, true);
			},
			dataType: 'json'
		});
		return newItem;
	}
}

ViewModel.prototype.removeColumn = function (item) {
	var self = this;
	$.ajax({
		type: 'POST',
		url: '/my/table/removeColumn.json',
		data: {t: self.tableId, c: item.id},
		async: false,
		success: function(data) {
			item.handleAfterRemove();
			var index = self.fields.indexOf(item);
			self.fields.splice(index, 1);
		},
		dataType: 'json'
	});
}

ViewModel.prototype.beforeMove = function (sender) {
	var vm = sender.item.parent;
	var item = sender.item;
	var targetRow = vm._rowForFields(sender.targetParent);
	var layoutParent = targetRow.parent;
	if (layoutParent != vm) {
		if (item.type == 'tab_group' && layoutParent.type == 'tab') {
			alert('タブ内部にタブグループを持つことはできません。');
			sender.cancelDrop = true;
			return;
		}

	}
	var targetArray = sender.targetParent;
	if (sender.sourceParent != sender.targetParent) {
		var width = item.sizeWidth();
		for (var i=0; i<targetArray().length; i++) {
			width += targetArray()[i].sizeWidth();
		}
		if (width > vm.maxWidth()) {
			alert('１行の幅を超えるため、移動できません。');
			sender.cancelDrop = true;
			return;
		}
	}
}

ViewModel.prototype.afterMove = function (sender) {
	var vm = sender.item.parent;
	var item = sender.item;
	var targetRow = vm._rowForFields(sender.targetParent);
	var layoutParent = targetRow.parent;
	if (layoutParent == vm) {
		item.layoutParentId = null;
	} else {
		item.layoutParentId = layoutParent.parent.id;
	}

	if (sender.sourceParent == null) { // メニューからドラッグされた場合
		if (sender.targetParent().length == 1) {
			var rowIndex = null;
			for (var i=0; i<layoutParent.rows().length; i++) {
				if (sender.targetParent == layoutParent.rows()[i].fields) {
					rowIndex = i;
					break;
				}
			}
			if (rowIndex != null) {
				layoutParent.rows.splice(rowIndex + 1, 0, new Row(layoutParent));
				layoutParent.rows.splice(rowIndex, 0, new Row(layoutParent));
			}

		}
		if (vm.prop(sender.item.type, 'isSingle') == true) {
			sender.item.showsField(true);
		} else {
			vm.fields.push(sender.item);
		}
		if (!item.validate() ) {
			item.error('設定してください。');
		}
	} else if (sender.sourceParent != sender.targetParent) {

		if (sender.targetParent().length == 1) {
			var rowIndex = null;
			for (var i=0; i<layoutParent.rows().length; i++) {
				if (sender.targetParent == layoutParent.rows()[i].fields) {
					rowIndex = i;
					break;
				}
			}
			if (rowIndex != null) {
				layoutParent.rows.splice(rowIndex + 1, 0, new Row(layoutParent));
				layoutParent.rows.splice(rowIndex, 0, new Row(layoutParent));
			}
		}
		if (sender.sourceParent().length == 0) {
			var rowIndex = null;
			var sourceRow = vm._rowForFields(sender.sourceParent);
			var sourceLayoutParent = sourceRow.parent;
			for (var i=0; i<sourceLayoutParent.rows().length; i++) {
				if (sender.sourceParent == sourceLayoutParent.rows()[i].fields) {
					rowIndex = i;
					break;
				}
			}
			if (rowIndex != null) {
				sourceLayoutParent.rows.splice(rowIndex + 1, 1);
				sourceLayoutParent.rows.splice(rowIndex - 1, 1);
			}
		}
	}
};

ViewModel.prototype._getFlattenRows = function() {
	var rows = [];
	var self = this;
	$.each(self.rows(), function(i, row){
		rows.push(row);
	});
	$.each(self.fields, function(i, field){
		if ('rows' in field) {
			$.each(field.rows(), function(i, row){
				rows.push(row);
			});
		}
	});
	return rows;
};

ViewModel.prototype.allowDropFor = function(item) {
	var self = this;
	var width = item.sizeWidth();
	var type = item.type;
	var rows = self._getFlattenRows();
	$.each(rows, function(i, row){
		var sum = width;
		var fields = row.fields();
		for (var j=0; j<fields.length; j++) {
			sum += fields[j].sizeWidth();
		}
		if (sum > self.maxWidth()) {
			row.allowDrop(false);
		}
		if (row.parent.type) {
			if (row.parent.type == 'tab' && type == 'tab_group') {
				row.allowDrop(false);
			}
		}
	});
};


ViewModel.prototype.allowDropAll = function() {
	var self = this;
	var rows = self._getFlattenRows();
	$.each(rows, function(i, row){
		row.allowDrop(true);
	});
};

ViewModel.prototype._rowForFields = function(fields) {
	var rows = this._getFlattenRows();
	var row = null;
	$.each(rows, function(i, _row){
		if (fields == _row.fields) {
			row = _row;
			return false;
		}
	});
	return row;
};
