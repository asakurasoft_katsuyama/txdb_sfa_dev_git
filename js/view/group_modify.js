var ViewModel  = function(userId) {
	self = this;
	self.id = userId;
	self.name = ko.observable('');
	self.errors = ko.observableArray([]);

	self.save = function() {
		self.errors.removeAll();
//		var json = ko.toJSON(viewModel);
		var data = ko.toJSON({
			id: self.id,
			name: self.name()
		});
		$.post('/admin/group/modify.json', {data:data}, function(json) {
			if (json.status == 'success') {
				document.location.href="/admin/group/";
			} else if (json.status == 'error') {
				self.errors(json.errors);
				$(window).scrollTop(0);
			} else {
				throw json.status;
			}
		});
	};
	
	self.cancel = function() {
		document.location.href="/admin/group/";
	};
	
	self._load = function() {
		if (self.id) {
			$.getJSON('/admin/group/load.json?'+$.param({g:self.id}), function(json) {
				self.name(json.result.name);
			});
		}
	};
	self._load();

};

