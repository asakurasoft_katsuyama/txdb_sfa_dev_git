var ViewModel  = function(data) {
	var self = this;
	self.tableId = data.tableId;
	self.error = ko.observable(null);
	self.message = ko.observable(null);
	self.downloadCharset = ko.observable('Shift_JIS');
	self.importCharset = ko.observable('Shift_JIS');
	self.file = ko.observable(null);
	self.doDownload = function(sender, ev) {
		$.getJSON('/my/data/export_format.json', {table_id: self.tableId, charset: self.downloadCharset() }, function(json) {
			if (json.status === 'success') {
				document.location.href = '/my/data/download_format.csv/'+encodeURIComponent(json.result.fileName)+'?'+ $.param({table_id: self.tableId, file_key:json.result.fileKey})
			}
		});
	};
	self.doImport = function(sender, ev) {
		self.error(null);
		self.message(null);
		$.post('/my/data/import.json', { table_id:self.tableId, file: self.file() ? self.file().filekey : null, charset: self.importCharset(),
				keyColumnId: self.keyColumnId()}, function(json) {
			if (json.status == 'error') {
				self.error(json.errors[0].message);
			} else if (json.status == 'success') {
				self.message(json.message.message);
				self.file(null);
			}
			$('body,html').stop().scrollTop(0);
		}, 'json');		
	};
	
	self.removeFile = function(sender) {
		self.file(null);
	};
	
    $('.fileupload').fileupload({
        dataType: 'json',
        start: function (e, data) {
        	self.error(null);
        },
        done: function (e, data) {
        	var field = ko.dataFor(e.target);
            $.each(data.result.files, function(index, file) {
            	if (file.error) {
            		self.error(file.error);
            		$('body,html').stop().scrollTop(0);
            	} else {
	            	self.file({
	            		fileName : file.originalName,
	            		filekey : file.name,
	            		contentType: file.type
	            	});
            	}
            });
        }
    });

	self.rownumColumn = null;
	self.keyColumnId = ko.observable();
	self.keyOptions = ko.observableArray();
	self.useRownumKey = ko.computed(function(){
		var columnId = self.keyColumnId();
		var columnType = null;
		var options = self.keyOptions();
		$(options).each(function(){
			if (this.id == columnId) {
				columnType = this.type;
				return false;
			}
		});
		return columnType == 'rownum';
	});

	self.load = function () {
		$.ajax({
			type: 'GET',
			url: '/my/data/importKeys.json',
			dataType: 'json',
			data: {t: self.tableId},
			async: false,
			success: function(data) {
				if (data.status == 'success') {
					self.keyOptions(data.result);
				}
			}
		});
		$(self.keyOptions()).each(function(){
			if (this.type == 'rownum') {
				self.rownumColumn = this;
				self.keyColumnId(this.id);
				return false;
			}
		});
	};

	self.load();

};

