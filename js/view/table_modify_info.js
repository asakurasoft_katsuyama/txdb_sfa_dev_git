
var IconModal = function(modalName, parent) {
	var self = this;

	self.icons = ko.observableArray([]);
	self.currentIcon = ko.observable();
	self.currentIconUrl = ko.computed(function() {
		var icon=self.currentIcon();
		return icon ? icon.url : '';
	});
	self.show = function(icon) {
		self.currentIcon(icon);
		if (self.icons().length > 0) {
			$('#'+modalName).modal('show');
		} else {
			$.get('/my/table/icons.json', null,
					function(json) {
						if (json.status == 'success') {
							self.icons(json.result);
							$('#'+modalName).modal('show');
						}
			}, 'json');
		}
	}
	self.close = function() {
		$('#'+modalName).modal('hide');
	}
	self.action = function(icon) {
		parent.onIconModalAction(icon);
		self.close();
	}
}


var ViewModel  = function(tableId, isCreate, baseType) {
	var self = this;
	self._tableId = tableId;
	self._isCreate = isCreate;
	self._baseType = baseType || null;

	self.errors = ko.observableArray([]);
	self.name = ko.observable(null);
	self.iconType = ko.observable('preset');
	self.isPublic = ko.observable(false);
	self.description = ko.observable(null);
	self.icon = ko.observable(null);
	self.iconUrl = ko.computed(function() {
		return self.icon() ? self.icon().url : '';
	});
	self.iconFile = ko.observable(null);
	self.iconModal = new IconModal('iconModal', self);

	self.openIconModal = function() {
		self.iconModal.show(self.icon());
	}

	self.onIconModalAction = function(icon) {
		self.icon(icon);
		self.iconType('preset');
	}
	
	self.save = function() {
		var data = {
			id: self._tableId,
			create: self._isCreate,
			base: self._baseType,
			name: self.name(),
			iconkey: self.icon().filekey,
			file: self.iconFile(),
			iconType: self.iconType(),
			isPublic: !!self.isPublic(),
			description: self.description()
		};
		$.post('/my/table/saveInfo.json', {data: ko.toJSON(data)},
			function(json) {
				if (json.status == 'success') {
					document.location.href="/my/table/modify?"+$.param({t:json.result});
				} else if (json.status == 'error') {
					self.errors(json.errors);
					$(window).scrollTop(0);
				} else {
					throw json.status;
				}
			}
		);
	};
	
	self.removeIconFile = function(sender) {
		self.iconFile(null);
	};

	self._load = function() {
		var data = {
			t: self._tableId,
			create: self._isCreate,
			base: self._baseType
		};
		$.getJSON('/my/table/loadInfo.json?'+$.param(data), function(json) {
			if (self._isCreate == true) {
				self.name(json.result.isDefault == true ? '' : json.result.name + 'のコピー');
			} else {
				self.name(json.result.name);
			}
			self.icon(json.result.icon);
			self.isPublic(json.result.isPublic);
			self.description(json.result.description);
		});

		$('.icon-fileupload').fileupload({
			autoUpload : true,
			url: '/my/table/uploadIcon.json',
			dataType: 'json',
			start: function (e, data) {
				self.errors.removeAll();
			},
			done: function (e, data) {
				$.each(data.result.files, function(index, file) {
					if (file.error) {
						self.errors.push({ message: file.error });
						$('body,html').stop().scrollTop(0);
					} else {
						self.iconFile({
							fileName : file.originalName,
							filekey : file.name,
							contentType: file.type
						});
						self.iconType('upload');
					}
				});
			}
		});
	};
	self._load();
};

