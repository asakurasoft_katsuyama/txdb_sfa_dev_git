var ViewModel  = function(data) {
	var self = this;
	self.error = ko.observable(null);
	self.message = ko.observable(null);
	self.downloadCharset = ko.observable('Shift_JIS');
	self.importCharset = ko.observable('Shift_JIS');
	self.file = ko.observable(null);
	self.doDownload = function(sender, ev) {
//		$('<iframe>')
//			.attr('src', '/admin/user/download_format.csv?'+$.param({charset: self.downloadCharset() }))
//			.css({width:1, height: 1, position: 'absolute', top: -99999})
//			.appendTo('body');
		$.getJSON('/admin/user/export_format.json', {charset: self.downloadCharset() }, function(json) {
			if (json.status === 'success') {
				document.location.href = '/admin/user/download_format.csv/'+encodeURIComponent(json.result.fileName)+'?'+ $.param({file_key:json.result.fileKey})
			}
		});
	};
	self.doImport = function(sender, ev) {
		self.error(null);
		self.message(null);
		$.post('/admin/user/import.json', { file: self.file() ? self.file().filekey : null, charset: self.importCharset() }, function(json) {
			if (json.status == 'error') {
				self.error(json.errors[0].message);
			} else if (json.status == 'success') {
				self.message(json.message.message);
				self.file(null);
			}
			$('body,html').stop().scrollTop(0);
		}, 'json');
	};
	
	self.removeFile = function(sender) {
		self.file(null);
	};
	
    $('.fileupload').fileupload({
        dataType: 'json',
        start: function (e, data) {
        	self.error(null);
        },
        done: function (e, data) {
        	var field = ko.dataFor(e.target);
            $.each(data.result.files, function(index, file) {
            	if (file.error) {
            		self.error(file.error);
            		$('body,html').stop().scrollTop(0);
            	} else {
	            	self.file({
	            		fileName : file.originalName,
	            		filekey : file.name,
	            		contentType: file.type
	            	});
            	}
            });
        }
    });
};

