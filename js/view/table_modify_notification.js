// Note:
// OptionGroup is copied from filter_model.js

var OptionGroup = Class.extend({
	init: function(label, children){
		this.label = label;
		this.children = children;
	}
});

// Note:
// ConditionTerm is copied from filter_model.js and modified.
var ConditionTerm = Class.extend({
	init: function(data, parent) {
		var self = this,
			dateFuncOptions = [
				{ id: null, name: '任意の日付', valueType: 'date' },
				{ id: 'now', name: '今日', valueType: false },
				{ id: 'last_month', name: '先月', valueType: 'days' },
				{ id: 'current_month', name: '今月', valueType: 'days' },
				{ id: 'current_year', name: '今年', valueType: false }
			],
			datetimeFuncOptions = [
				{ id: null, name: '任意の日時', valueType: 'datetime' },
				{ id: 'now', name: '当時刻', valueType: false },
				{ id: 'today', name: '今日', valueType: false },
				{ id: 'last_month', name: '先月', valueType: 'days' },
				{ id: 'current_month', name: '今月', valueType: 'days' },
				{ id: 'current_year', name: '今年', valueType: false }
			],
			datePresetColumns = ['datetime', 'date', 'created_on', 'updated_on', 'datecalc'],
			unselected = '';
		this.parent = parent;
		this.id = ko.observable(data['column'] ? data['column'].id : null);
		this.id.subscribe(function(id) {
			// Clear following conditions.
			self.operator(null);
			self.value(null);
			self.values([]);
			self.datePreset(null);
		});
		this.operator = ko.observable(data['operator'] ? data['operator'] : null);	// 演算子
		this.operatorOptions = ko.computed(function() {
			var id = this.id(),
				column = this.parent.searchableOptionFor(id),
				self = this;
			if (column == null) return false;
			var constraints = column['notificationConstraints'];
			return $.map(constraints['conditional']['operators'], function(operator) {
					return { id: operator, name: self.getOperatorLabel(operator) };
				});
		}, this);
		this.datePreset = ko.observable(null);
		this.datePreset.subscribe(function(id) {
			self.value(null);
		});
		this.valueOptionsDays = this.getDaysOptions();
		this.datePresetOptions = ko.computed(function()	{
			var id = self.id(),
			column = this.parent.searchableOptionFor(id);
			if (column == null) return false;
			if (column.type == 'date') {
				return dateFuncOptions;
			} else if (column.type == 'datetime' ||
					column.type == 'created_on' ||
					column.type == 'updated_on') {
				return datetimeFuncOptions;
			} else if (column.type == 'datecalc') {
				if (column.dateFormat == 'datetime') {
					return datetimeFuncOptions;
				} else if (column.dateFormat == 'date') {
					return dateFuncOptions;
				} else {
					return false;
				}
			} else {
				return false;
			}
		}, this);
		this.value = ko.observable(null);
		if (data['column'] && data['value'] && datePresetColumns.indexOf(data.column.type) != -1) {
			// Expand expression.
			var p;
			if ((p = data.value.indexOf('/')) != -1) {
				self.datePreset(data.value.substr(p + 1));	// 設定する順番があるので注意!
				self.value(data.value.substr(0, p));
			} else {
				self.value(data.value);
			}
		} else {
			self.value(data['value'] ? data.value : null);
		}
		self.values = ko.observableArray(null);	// 検索値（複数）
		if ('value' in data && data.value instanceof Array) {
			//var column = self.parent.searchableOptionFor(self);
			if (data['column'].type == 'user_selector') {
				self.values($.map(data['value'], function(val) {
					return (val ? (val.type+":"+(val.id===null ? '':val.id)) : null);
				}));
				//console.log(self.values());
			} else {
				self.values($.map(data['value'], function(val) {
					return (val == null ? unselected : val.id);
				}));
			}
		}

		self.valueType = ko.computed(function() {
			var id = self.id(),
				column = this.parent.searchableOptionFor(id),
				datePreset = self.datePreset();
			if (column == null) return null;
			switch (column.type) {
			case 'checkbox':
			case 'radio':
			case 'dropdown':
			case 'created_by':
			case 'updated_by':
				return 'options';
			case 'user_selector':
				return 'userGroupOptions';
			case 'time':
				return 'time';
			case 'datecalc':
				if (column.dateFormat == 'time') return 'time';
			case 'date':
			case 'datetime':
			case 'datecalc':
			case 'created_on':
			case 'updated_on':
			//case 'datecalc':
				return self.datePresetOptionFor(datePreset).valueType;
			default:
				return 'text';
			}
		}, this);

		self.valueOptionsOptions = ko.computed(function() {	// オプション（checkbox, radio, dropdown）
			var id = self.id(),
				column = this.parent.searchableOptionFor(id);
			if (column == null) return [];
			if (column.type == 'checkbox' ||
				column.type == 'radio' ||
				column.type == 'dropdown') {
				return [ { id: unselected, name: '未選択' }].concat(column.options);
			} else if (column.type == 'created_by' ||
					column.type == 'updated_by') {
				return parent._userOptionsCache;
			} else if (column.type == 'user_selector') {
				return self.createCompositeOptions(parent._userOptionsCache, parent._groupOptionsCache);
			}
		}, this);

		self.userGroupOptions = ko.computed(function() {
			var id = self.id(),
				column = this.parent.searchableOptionFor(id);
			if (column == null) return [];
			if (column.type == 'user_selector') {
				return self.createCompositeOptions(parent._userOptionsCache, parent._groupOptionsCache);
			}
		}, this);
	},
	createCompositeOptions: function(users, groups) {
		var wrapId = function(options, type) {
			var newOptions = [];
			for (var i = 0; i < options.length; i++) {
				newOptions.push({
					id: type+":"+options[i].id,
					name: options[i].name
				});
			}
			return newOptions;
		};
		return [
				new OptionGroup('ユーザー', [{ id: 'user:', name: '未選択'}].concat(wrapId(users, "user"))),
				new OptionGroup('グループ', wrapId(groups, "group"))
			];
	},
	getOperatorLabel: function(operator) {
		return {
			'=': '＝ (等しい)',
			'!=':'≠ (等しくない)',
			'like':'キーワードを含む',
			'not like':'キーワードを含まない',
			'>=':'≧ (以上)',
			'<=':'≦ (以下)',
			'<':'＜ (未満)',
			'>':'＞ (超える)',
			'contains': 'いずれかを含む',
			'not contains': 'いずれも含まない'
		}[operator];
	},
	datePresetOptionFor: function(id)	{
		var found = null;
		$.each(this.datePresetOptions(), function(i, option) {
			if (option.id == id)
				found = option;
		});
		return found;
	},
	serialize: function() {
		var unselected = '';
		if (this.id() == null)
			return null;
		var column = this.parent.searchableOptionFor(this.id());
		var value = null;
		if (column.hasOptions) {
			value = $.map(this.values(), function(value) {
				return value == unselected ? unselected : { id: value };
			});
		} else if (column.type == 'user_selector') {
			value = $.map(this.values(), function(value) {
				value.match(/^(user|group):(.*)$/);
				return { id: RegExp.$2, type: RegExp.$1 };
			});
		} else if (column.type == 'created_by' ||
				column.type == 'updated_by') {
			value = $.map(this.values(), function(value) {
				return { id: value };
			});
		} else if (!String.isEmpty(this.datePreset())) {
			value = (this.value()||'')+'/'+this.datePreset();
		} else {
			value = this.value();
		}
		return {
			column: { id: this.id(), type: column.type },
			operator: this.operator(),
			value: value
		};
	},
	getDaysOptions: function() {
		var options = [];
		options.push({ id:null ,name: 'すべて' });
		for(var i=1;i <= 31;i++)
			options.push({id:i.toString() ,name: i+'日'});
		options.push({ id: 'last_day' ,name:'末日'});
		return options;
	}
});

var ViewColumn = Class.extend({
	init: function(data, parent){
		this.parent = parent;
		this.id = ko.observable(data.id ? data.id : null);
	}
});

var BasicRule = Class.extend({
	init: function(data, parent) {
		var self = this;
		self.errors = ko.observableArray([]);
		self.parent = parent;
		self.targetingType = ko.observable(data['targetingType'] || 'user_group');
		self.targetIds = ko.observableArray([]);
		self.targetColumnId = ko.observable(null);
		self.targetingType.subscribe(function() {
			self.targetIds([]);
			self.targetColumnId(null);
		});
		if ('targets' in data) {
			if (self.targetingType() == 'user_group') {
				self.targetIds($.map(data['targets'] || [], function(val) {
					return (val ?
						(val.type+":"+(val.id===null ? '':val.id)) : null);
				}));
			} else if (self.targetingType() == 'column') {
				if (data['targets'].length > 0) {
					self.targetColumnId(data['targets'][0].id);
				}
			}
		}
		self.isOnRowInsert = ko.observable(data['isOnRowInsert'] || false);
		self.isOnRowUpdate = ko.observable(data['isOnRowUpdate'] || false);
		self.isOnCommentInsert = ko.observable(data['isOnCommentInsert'] || false);
	},
	serialize: function() {
		var rule = {
			targetingType: this.targetingType(),
			targets: [],
			isOnRowInsert: this.isOnRowInsert(),
			isOnRowUpdate: this.isOnRowUpdate(),
			isOnCommentInsert: this.isOnCommentInsert()
		};

		if (this.targetingType() == 'user_group') {
			rule.targets = $.map(this.targetIds(), function(value) {
				value.match(/^(user|group):(.*)$/);
				return { id: RegExp.$2, type: RegExp.$1 };
			});
		} else if (this.targetingType() == 'column') {
			if (this.targetColumnId() != null) {
				rule.targets.push(
					{ id: this.targetColumnId(), type: 'column'});
			}
		}

		return rule;
	}
});


var ConditionalRule = Class.extend({
	init: function(data, parent) {
		var self = this,
			boolTypeOptions = [ { id: 'and', name: '下記条件のすべてに一致する' }, { id: 'or', name: '下記条件のいずれかに一致する'} ];

		self.errors = ko.observableArray([]);
		self.parent = parent;
		self.targetingType = ko.observable(data['targetingType'] || 'user_group');
		self.targetIds = ko.observableArray([]);
		self.targetColumnId = ko.observable(null);
		this.targetingType.subscribe(function() {
			self.targetIds([]);
			self.targetColumnId(null);
		});
		if ('targets' in data) {
			if (self.targetingType() == 'user_group') {
				self.targetIds($.map(data['targets'] || [], function(val) {
					return (val ?
						(val.type+":"+(val.id===null ? '':val.id)) : null);
				}));
			} else if (self.targetingType() == 'column') {
				if (data['targets'].length > 0) {
					self.targetColumnId(data['targets'][0].id);
				}
			}
		}
		self.boolOperator = ko.observable(
				data.criteria && data.criteria.boolOperator ?
				data.criteria.boolOperator : 'and');
		self.boolTypeOptions = boolTypeOptions;
		self.conditionTerms = ko.observableArray([]);
		self.message = ko.observable(data.message || null);

		self._userOptionsCache = this.parent._userOptionsCache;
		self._groupOptionsCache = this.parent._groupOptionsCache;
		self.searchableOptions = this.parent.searchableOptions;
		//console.log(this.parent.searchableOptions())

		self.reset();
		if (data.criteria && data.criteria.conditions
				&& data.criteria.conditions.length > 0) {
			this.conditionTerms($.map(data.criteria.conditions, function(obj) {
				return new ConditionTerm(obj, self);
			}));
		}
	},
	searchableOptionFor : function(id) {
		return this.parent.searchableOptionFor(id);
	},
	serialize: function() {
		var rule = {
			targetingType: this.targetingType(),
			targets: [],
			criteria: {
				conditions: [],
				boolOperator: this.boolOperator()
			},
			message: this.message()
		};

		if (this.targetingType() == 'user_group') {
			rule.targets = $.map(this.targetIds(), function(value) {
				value.match(/^(user|group):(.*)$/);
				return { id: RegExp.$2, type: RegExp.$1 };
			});
		} else if (this.targetingType() == 'column') {
			if (this.targetColumnId() != null) {
				rule.targets.push(
					{ id: this.targetColumnId(), type: 'column'});
			}
		}

		$.each(this.conditionTerms(), function() {
			var obj = this.serialize();
			if (obj != null) {
				rule.criteria.conditions.push(obj);
			}
		});

		return rule;
	},
	reset: function() {
		this.conditionTerms([ new ConditionTerm({}, this) ]);
	},
	addConditionTerm: function(data) {
		var p = this.conditionTerms.indexOf(data);
		this.conditionTerms.splice(p + 1, 0, new ConditionTerm({}, this));
	},
	removeConditionTerm: function(data) {
		this.conditionTerms.remove(data);
		if (this.conditionTerms().length == 0) {
			this.conditionTerms.push(new ConditionTerm({}, this));
		}
	}
});

var ViewModel  = function(tableId) {
	var self = this,
		targetingTypeOptions = [
			{ id: 'user_group', label: '通知先ユーザー/グループ'},
			{ id: 'column', label: '通知先ユーザーフィールド'}
		];
	self.defaultErrorMessage = '通知設定にエラーがあります。';
	self._tableId = tableId;

	self.basicRules = ko.observableArray([]);
	self.conditionalRules = ko.observableArray([]);
	self.viewColumns = ko.observableArray([]);
	self.isCommentFollowingEnabled = ko.observable(false);
	self.errors = ko.observableArray([]);
	self.basicRuleErrors = ko.observableArray([]);
	self.conditionalRuleErrors = ko.observableArray([]);

	self.targetingTypeOptions = targetingTypeOptions;
	self.viewColumnOptions = [ { id:null, label:""} ];
	self.targettableColumnOptions = [ { id:null, label:""} ];
	self.searchableOptions = ko.observableArray([ { id:null, label:""} ]);
	self.userGroupOptions = [];
	self._userOptionsCache = null;
	self._groupOptionsCache = null;
	self._columnOptionsCache = null;

	// Preload
	$.ajax({ async: false, type: 'GET', dataType: 'json',
		url: '/my/table/user_options.json',
		success: function(json) {
			self._userOptionsCache = json.result;
		}});
	$.ajax({ async: false, type: 'GET', dataType: 'json',
		url: '/my/table/group_options.json',
		success: function(json) {
			self._groupOptionsCache = json.result;
		}});
	$.ajax({ async: false, type: 'GET', dataType: 'json',
		url: '/my/table/columns.json?t=' + self._tableId,
		success: function(json) {
			self._columnOptionsCache = json.result;
		}});

	self._load = function() {
		self.userGroupOptions = self.createCompositeOptions(
				self._userOptionsCache, self._groupOptionsCache);

		$.each(self._columnOptionsCache, function(i, column) {
			var constraints = column['notificationConstraints'];
			if (constraints['listable'] == true) {
				self.viewColumnOptions.push(column);
			}
			if (constraints['targettable'] == true) {
				self.targettableColumnOptions.push(column);
			}
			if (constraints['conditional']['operators'].length > 0) {
				self.searchableOptions.push(column);
			}
		});
		$.getJSON(
			'/my/table/load_notification_settings.json',
			{t:self._tableId},
			function (json) {
				$.each(json.result.basicRules, function(i, obj) {
					self.basicRules.push(
						new BasicRule(obj, self));
				});
				$.each(json.result.conditionalRules, function(i, obj) {
					self.conditionalRules.push(
						new ConditionalRule(obj, self));
				});
				$.each(json.result.viewColumns, function(i, obj) {
					self.viewColumns.push(
						new ViewColumn(obj, self));
				});
				self.isCommentFollowingEnabled(
					json.result.isCommentFollowingEnabled);
			}
		);
	};

	self.save = function() {
		self.errors([]);
		$.each(self.basicRules(), function(){ this.errors([]); });
		$.each(self.conditionalRules(), function(){ this.errors([]); });
		var data = {
			id: self._tableId,
			basicRules: [],
			conditionalRules: [],
			viewColumns: [],
			isCommentFollowingEnabled: self.isCommentFollowingEnabled()
		};
		data.basicRules = $.map(self.basicRules(),
				function(obj) { return ko.toJSON(obj.serialize()); });
		data.conditionalRules = $.map(self.conditionalRules(),
				function(obj) { return ko.toJSON(obj.serialize()); });
		data.viewColumns = $.map(self.viewColumns(),
				function(obj) { return obj.id(); });
		$.post('/my/table/save_notification_settings.json', data,
			function(json) {
				if (json.status == 'success') {
					document.location.href="/my/table/modify?"+$.param({t:json.result});
				} else if (json.status == 'error') {
					if (json.errors) {
						self.errors(json.errors);
					} else {
						self.errors([{message: self.defaultErrorMessage}]);
					}
					$.each(json.basicRuleErrors || [], function(){
						self.basicRules()[this.index].errors(this.messages);
					});
					$.each(json.conditionalRuleErrors || [], function(){
						self.conditionalRules()[this.index].errors(this.messages);
					});
					$(window).scrollTop(0);
				} else {
					throw json.status;
				}
			}
		);
	};

	self.addBasicRule = function(data, event) {
		var index = 0;
		if (this != data) {
			var p = this.basicRules.indexOf(data);
			if (index >= 0) index = p + 1;
		}
		this.basicRules.splice(index, 0, new BasicRule({}, this));
	};

	self.removeBasicRule = function(data, event) {
		var index = this.basicRules.indexOf(data);
		if (index < 0) { return; }
		this.basicRules.splice(index, 1);
	};

	self.addConditionalRule = function(data, event) {
		var index = 0;
		if (this != data) {
			var p = this.conditionalRules.indexOf(data);
			if (index >= 0) index = p + 1;
		}
		this.conditionalRules.splice(index, 0, new ConditionalRule({}, this));
	};

	self.removeConditionalRule = function(data, event) {
		var index = this.conditionalRules.indexOf(data);
		if (index < 0) { return; }
		this.conditionalRules.splice(index, 1);
	};

	self.addViewColumn = function(data, event) {
		if (this.viewColumns().length >= 3) {
			alert("付加情報は3つまでです。");
			return;
		}
		var index = 0;
		if (this != data) {
			var p = this.viewColumns.indexOf(data);
			if (index >= 0) index = p + 1;
		}
		this.viewColumns.splice(index, 0, new ViewColumn({}, this));
	};

	self.removeViewColumn = function(data, event) {
		var index = this.viewColumns.indexOf(data);
		if (index < 0) { return; }
		this.viewColumns.splice(index, 1);
	};

	self.createCompositeOptions = function(users, groups) {
		var wrapId = function(options, type) {
			var newOptions = [];
			for (var i = 0; i < options.length; i++) {
				newOptions.push({
					id: type+":"+options[i].id,
					name: options[i].name
				});
			}
			return newOptions;
		};
		return [
				new OptionGroup('ユーザー', wrapId(users, "user")),
				new OptionGroup('グループ', wrapId(groups, "group"))
			];
	};

	self.searchableOptionFor = function(id) {
		var found = null;
		$.each(this.searchableOptions(), function(i, column) {
			if (column.id == id && column.id != null)
				found = column;
		});
		return found;
	};

	self._load();

};
