/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.language = 'ja';
	config.removePlugins = "elementspath";
	config.enterMode = CKEDITOR.ENTER_BR;
	config.shiftEnterMode =  CKEDITOR.ENTER_P;
	config.linkShowAdvancedTab = false;
	config.linkShowTargetTab = false;
	
	config.dialog_noConfirmCancel = true;
	config.toolbar = [
		['Undo', 'Redo' ] ,
		[ 'Styles' ],
		['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', /*'-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight' */ ],
		['Bold', 'Underline', 'Strike','-', 'RemoveFormat' ],
		['TextColor', 'BGColor' ],
		['Link', 'Unlink' ]
	];
	
	var agent = navigator.userAgent.toLowerCase();
	if (/(android|ipad|iphone)/.test(agent) ) {
		config.resize_enabled = false;
	}
};

CKEDITOR.on( 'dialogDefinition', function( ev )
{
	// Take the dialog name and its definition from the event data.
	var dialogName = ev.data.name;
	var dialogDefinition = ev.data.definition;
	
	// Check if the definition is from the dialog we're
	// interested in (the 'link' dialog).
	if ( dialogName == 'link' )
	{
		var infoTab = dialogDefinition.getContents( 'info' );
		var protocolField = infoTab.get( 'protocol' );
		// remove protocol: 'ftp://', 'news://', ''
		protocolField['items'].splice(2, 3); 
		var linktypeField = infoTab.get( 'linkType' );
		// remove linktype: 'anchor'
		linktypeField['items'].splice(1, 1); 
		// remove email option
		infoTab.remove( 'emailSubject' );
		infoTab.remove( 'emailBody' );
		// Set the default value for the LinkTargetType field.
		var targetTab = dialogDefinition.getContents( 'target' );
		var linkTargetTypeField = targetTab.get( 'linkTargetType' );
		linkTargetTypeField[ 'default' ] = '_blank';
	}
});

CKEDITOR.on('instanceReady', function( ev )
{
	ev.editor.getCommand('indent').fire('refresh', {
		editor:ev.editor,
		//path:ev.editor.elementPath() // This doesn't work on IE, Firefox
		path:new CKEDITOR.dom.elementPath(new CKEDITOR.dom.element('body'))
	});
});

