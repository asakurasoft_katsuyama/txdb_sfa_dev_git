<?php

include_once dirname(__FILE__).'/.app/loader.php';
$c = new \Barge\Web\FrontController();
$c->setRouter(function($path) {
    $matches = array();
    if (preg_match('#^/my/api/v1/(.*)#', $path, $matches)) {
        return '/my/api/'.$matches[1];
    } else if (preg_match('#^/asset/files/.*#', $path) && preg_match('/^t\d+$/', @$_SERVER['QUERY_STRING'])) {
		return '/file/thumb';
	} else if (preg_match('#^/asset/files/.*#', $path)) {
		return '/file/download';
	} else if (preg_match('#^/my/data/download.csv/.*#', $path)) {
		return '/my/data/download.csv';
	} else if (preg_match('#^/my/data/download_format.csv/.*#', $path)) {
		return '/my/data/download_format.csv';
	} else if (preg_match('#^/admin/user/download_format.csv/.*#', $path)) {
		return '/admin/user/download_format.csv';
	} else {
		return $path;
	}
});
$c->setErrorHandler(array(new \Action\ErrorHandler(), 'handle'));
$c->dispatch();
