<?php 

// -----------------------------------
// Setup PHP.
// -----------------------------------
error_reporting(E_ALL);
ini_set('mbstring.func_overload', 4);
ini_set('track_errors', 1);	// for $php_errormsg
ini_set('display_errors', 0);
date_default_timezone_set('UTC');	// 日付加算時の時差を無効に

// -----------------------------------
// Setup incldue dirs.
// -----------------------------------
set_include_path(get_include_path() . PATH_SEPARATOR . join(PATH_SEPARATOR, array(
	'.app/lib',
	'.app/src',
	)));

// -----------------------------------
// Automatical class loading.
// -----------------------------------
function barge_autoload($className)
{
	// Inner class loading.
	if (($p =strpos($className, '__')) !== false) {
		$className = substr($className, 0, $p);
	}
	$classpath = str_replace('\\', '/', $className).'.php';
	@include_once($classpath);
}
spl_autoload_register('barge_autoload', true, true);
ini_set('unserialize_callback_func', 'barge_autoload');

// -----------------------------------
// Delegate error and exception to framework.
// -----------------------------------
function barge_error_handler($errno, $errstr, $errfile = null, $errline = null, array $contxt = null)
{
	// Skip calling silent mode. e.g. @file_get_contents
	if (error_reporting() !== 0) {
		static $names = array(
			E_ERROR => 'ERROR',
			E_WARNING => 'WARNING',
			E_PARSE => 'PARSE',
			E_NOTICE => 'NOTICE',
			E_CORE_ERROR => 'CORE_ERROR',
			E_CORE_WARNING => 'CORE_WARNING',
			E_COMPILE_ERROR => 'COMPILE_ERROR',
			E_COMPILE_WARNING => 'COMPILE_WARNING',
			E_USER_ERROR => 'USER_ERROR',
			E_USER_WARNING => 'USER_WARNING',
			E_USER_NOTICE => 'USER_NOTICE',
			E_STRICT => 'STRICT',
			E_RECOVERABLE_ERROR => 'RECOVERABLE_ERROR',
			E_DEPRECATED => 'DEPRECATED',
			E_USER_DEPRECATED => 'USER_DEPRECATED',
			);
		$message = sprintf("[%s] %s in %s (%d)",
			(isset($names[$errno]) ? $names[$errno] : 'UNKNOWN_ERROR'),
			$errstr, $errfile, $errline);

		$e = new \ErrorException($message, 0, $errno, $errfile, $errline);
		call_user_func_array($GLOBALS['app_error_handler'], array($e));
		die();	// For calling shoutdown handler.
	}
	return false;	// For @phperrmsg
}
function barge_exception_handler(\Exception $e)
{
	call_user_func_array($GLOBALS['app_error_handler'], array($e));
}
function barge_shutdown_handler()
{
	static $names = array(
		E_ERROR => 'ERROR',
		E_PARSE => 'PARSE',
		E_NOTICE => 'NOTICE',
		E_CORE_ERROR => 'CORE_ERROR',
		E_CORE_WARNING => 'CORE_WARNING',
		E_COMPILE_ERROR => 'COMPILE_ERROR',
		E_COMPILE_WARNING => 'COMPILE_WARNING',
		);

	if ($error = error_get_last()) {
		switch($error['type']){
    	case E_ERROR:
        case E_PARSE:
        case E_CORE_ERROR:
        case E_CORE_WARNING:
        case E_COMPILE_ERROR:
        case E_COMPILE_WARNING:
			$message = sprintf("[%s] %s in %s (%d)",
				(isset($names[$error['type']]) ? $names[$error['type']] : 'UNKNOWN_ERROR'),
				$error['message'], $error['file'], $error['line']);
			$e = new ErrorException($message, 0, $error['type'], $error['file'], $error['line'], null);
			call_user_func_array($GLOBALS['app_error_handler'], array($e));
		}
	}
}
function set_app_error_handler($callable)
{
	$GLOBALS['app_error_handler'] = $callable;
}
function default_app_error_handler($e) 
{
	//while (@ob_end_clean());
	echo format_exception($e);
}
set_exception_handler('barge_exception_handler');
set_error_handler('barge_error_handler');
set_app_error_handler('default_app_error_handler');
register_shutdown_function('barge_shutdown_handler');

// -----------------------------------
// Incuding standard functions.
// -----------------------------------
require_once('functions.php');
