<?php
namespace Form\Admin;

use Barge\Web\Form;
use TxCore\Message;

class UserImportForm extends Form
{
	public $charset = null;
	public $file = null;
	
	public function validate() 
	{
		if (is_empty($this->file)) {
			return array(new Message(':data_import.file.required'));
		}
		return array();
	}
	
}

