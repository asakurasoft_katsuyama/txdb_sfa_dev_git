<?php
namespace Form\Table;

use TxCore\Message;

use Barge\Web\Form;


class SaveCalendarForm extends Form
{
    public $id;
    public $dateColumnId;
    public $userColumnId;
    public $startTimeColumnId;
    public $endTimeColumnId;
    public $titleColumnId;
    public $isCalendarEnabled = false;

    /**
     * @return Message[]
     */
    public function validate()
    {
        $messages = array();
        if (is_empty($this->dateColumnId)) {
            $messages[] = new Message('日付に対応するフィールドを選択してください。', null, 'dateColumnId');
        }
        if (is_empty($this->userColumnId)) {
            $messages[] = new Message('ユーザーに対応するフィールドを選択してください。', null, 'userColumnId');
        }
        if (is_empty($this->titleColumnId)) {
            $messages[] = new Message('タイトルにするフィールドを選択してください。', null, 'titleColumnId');
        }

        if ((is_empty($this->startTimeColumnId) && is_not_empty($this->endTimeColumnId)) ||
            is_not_empty($this->startTimeColumnId) && is_empty($this->endTimeColumnId)) {
            $messages[] = new Message('開始時刻と終了時刻を設定する場合は、セットで選択してください。', null, 'startTimeColumnId');
        }

        if (is_not_empty($this->startTimeColumnId) &&
            $this->startTimeColumnId === $this->endTimeColumnId) {
            $messages[] = new Message('開始時刻と終了時刻に同じフィールドが選択されています。', null, 'startTimeColumnId');
        }

        return $messages;
    }
}

