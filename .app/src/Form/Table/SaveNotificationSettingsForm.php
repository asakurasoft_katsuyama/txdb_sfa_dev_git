<?php
namespace Form\Table;

use TxCore\Message;

use Barge\Web\Form;


class SaveNotificationSettingsForm extends Form
{
    public $id = 0;
    public $isCommentFollowingEnabled = false;
    public $basicRules = array();
    public $conditionalRules = array();
    public $viewColumns = array();

    /**
     * @return Message[]
     */
    public function validate()
    {
        $messages = array();

        $ids = array();
        foreach ($this->viewColumns as $id) {
            if (is_not_empty($id) && in_array($id, $ids)) {
                $messages[] = new Message(':table_modify.notification.view_column.duplicated', array(), 'viewColumn');
                break;
            }
            $ids[] = $id;
        }

        return $messages;
    }
}

