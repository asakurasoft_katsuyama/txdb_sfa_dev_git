<?php
namespace Form\Table;

use TxCore\Message;

use Barge\Web\Form;


class SaveReadCheckForm extends Form
{
    public $id;
    public $isAutoreadEnabled = false;
    public $isOnlistEnabled = false;

    /**
     * @var string
     */
    public $targetType;

    /**
     * JSON array
     * @var string
     */
    public $checkTargets;

//    public $userColumnId;
//    public $startTimeColumnId;
//    public $endTimeColumnId;
//    public $titleColumnId;
    public $isReadCheckEnabled = false;

    /**
     * @return Message[]
     */
    public function validate()
    {
        $messages = array();
        if ($this->targetType !== 'all' && $this->checkTargets === '[]') {
            $messages[] = new Message('グループまたはユーザーを選択してください。', null, null);
        }
        if ($this->targetType === 'all') {
            $this->checkTargets = '[]';
        }
        return $messages;
    }
}

