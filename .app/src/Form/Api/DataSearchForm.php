<?php
namespace Form\Api;

use TxCore\TxCoreException;

use TxCore\Column;
use TxCore\Criteria;
use TxCore\Engine;
use TxCore\Message;

use Barge\Web\Form;
use TxCore\User;

/**
 * /api/data/search.json?
 *      api_key= <api_key> 
 *      table= <table_id>
 *      columns= <comma separerated column codes>
 *      conditions[<index>][column]= <code>
 *      conditions[<index>][operator]= <=|!=|like...>
 *      conditions[<index>][value]= <value> 
 *      sorts[<index>][column]= <code>
 *      sorts[<index>][reverse]= <true|false>
 *      limit= <integer>
 *      offset= <integer>
 *      parent_table= <parent table id>
 *      parent_row= <parent row number>
 *      parent_column= <parent column code>
 * @author kenji
 *
 */
class DataSearchForm extends Form
{
	/**
	 * Table id.
	 * @var string
	 */
	public $table;
	
	/**
	 * @var string
	 */
	public $callback;
	
	/**
	 * Api key.
	 * @var string
	 */
	public $apiKey;
	
	/**
	 * Select columns.
	 * @var string
	 */
	public $columns;
	
	/**
	 * @var array
	 */
	public $conditions = array();
	
	/**
	 * @var array
	 */
	public $sorts = array();
	
	/**
	 * @var string
	 */
	public $boolOperator;
	
	/**
	 * @var int
	 */
	public $limit = 20;

	/**
	 * @var int
	 */
	public $offset;

	/**
	 * Parent table id.
	 * @var string
	 */
	public $parentTable;

	/**
	 * Parent row number.
	 * @var string
	 */
	public $parentRow;

	/**
	 * Parent column code.
	 * @var string
	 */
	public $parentColumn;
	
	/**
	 * @var Criteria
	 */
	public $_criteria;
	
	/**
	 * @var Table
	 */
	public $_table;

	/**
	 * @var Table
	 */
	public $_parentTable;

	/**
	 * @var Row
	 */
	public $_parentRow;

	/**
	 * @var Column
	 */
	public $_parentColumn;

	/**
	 * @return Message[]
	 * @throws TxCoreException
	 */
	public function validate() 
	{
		$this->_criteria = $criteria = new Criteria();
		$erros = array();
		
		if (is_empty($this->table)) {
			return array(new Message(':api.search.table.required', array(), 'table'));
		}
		
		if (is_empty($this->apiKey)) {
			return array(new Message(':api.search.api_key.required', array(), 'api_key'));
		}
		
		$table = Engine::factory()->getTable($this->table, Engine::RELEASE, null, true);
		if ($table === null || !$table->isApiEnabled()) {
			return array(new Message(':api.search.table.not_exist', array($this->table), 'table'));
		}
		
		if ($table->getApikey() !== $this->apiKey) {
			return array(new Message(':api.search.api_key.missing', array(), 'api_key'));
		}
		
		$this->_table = $table;
		if (is_empty($this->columns)) {
			return array(new Message(':api.search.columns.required', array(), 'columns'));
		}
		
		$columns = array();
		foreach (explode(',', $this->columns) as $code) {
			$matchedColumns = $table->getColumns(array('code'=>trim($code)));
			if (is_empty($matchedColumns)) {
				return array(new Message(':api.search.columns.not_exist', array($code), 'columns'));
			}
			if (!$matchedColumns[0]->isListable()) {
				return array(new Message(':api.search.columns.not_listable', array($matchedColumns[0]->getTypeName()), 'columns'));
			}
			$columns[] = $matchedColumns[0];
		}
		$criteria->setListColumns($columns);

		if (is_not_empty($this->boolOperator)) {
			if (!in_array($this->boolOperator, array(Criteria::AND_, Criteria::OR_), true)) {
				return array(new Message(':api.search.boolOperator.invalid', array(), 'boolOperator'));
			}
			$criteria->setBoolOperator($this->boolOperator);
		}
		
		foreach ($this->conditions as $i => $condition) {
			if (!is_array($condition) || 
				!array_key_exists('column', $condition) ||
				!array_key_exists('operator', $condition) ||
				!array_key_exists('value', $condition)) {
				return array(new Message(':api.search.conditions.invalid', array(), "conditions[$i]"));
			}
			$matchedColumns = $table->getColumns(array('code'=>$condition['column']));
			if (is_empty($matchedColumns)) {
				return array(new Message(':api.search.conditions.columns.not_exist', array($condition['column']), "conditions[$i][column]"));
			}
			if (!$matchedColumns[0]->isSearchable()) {
				return array(new Message(':api.search.conditions.columns.not_searchable', array($matchedColumns[0]->getTypeName()), "conditions[$i][column]"));
			}
			if (!in_array($condition['operator'], array(
				Criteria::EQUALS, Criteria::NOT_EQUALS, Criteria::LIKE, 
				Criteria::NOT_LIKE, Criteria::GREATER_EQUALS, Criteria::LESS_EQUALS, 
				Criteria::GREATER, Criteria::LESS, Criteria::CONTAINS, Criteria::NOT_CONTAINS), true)) {
				return array(new Message(':api.search.conditions.operator.invalid', array(), "conditions[$i][operator]"));
			}
			
			$value = null;
			if ($matchedColumns[0]->hasOptions()) {
				if (!is_array($condition['value'])) {
					return array(new Message(':api.search.conditions.contains.must_array', array(), "conditions[$i][value]"));
				}
				$options = array();
				foreach ($condition['value'] as $optionName) {
					if (is_empty($optionName)) {
						$options[] = null;	// 未選択
						continue;
					}
					$matchedOptions = $matchedColumns[0]->getOptions(array('name'=>$optionName));
					if (is_empty($matchedOptions)) {
						return array(new Message(':api.search.conditions.option.not_exist', array($optionName), "conditions[$i][value]"));
					}
					$options[] = $matchedOptions[0];
				} 
				$value =$options;
			} else if ($matchedColumns[0]->getType() === Column::CREATED_BY || 
					$matchedColumns[0]->getType() === Column::UPDATED_BY) {
				if (!is_array($condition['value'])) {
					return array(new Message(':api.search.conditions.contains.must_array', array(), "conditions[$i][value]"));
				}
				$options = array();
				foreach ($condition['value'] as $userName) {
					$user = Engine::factory()->getUser($userName, true, true);
					if ($user === null) {
						return array(new Message(':api.search.conditions.user.not_exist', array($userName), "conditions[$i][value]"));
					}
					$options[] = $user;
				} 
				$value =$options;
			} else if ($matchedColumns[0]->getType() === Column::USER_SELECTOR) {
				if (!is_array($condition['value'])) {
					return array(new Message(':api.search.conditions.contains.must_array', array(), "conditions[$i][value]"));
				}

				$options = array();
				foreach ($condition['value'] as $j => $obj) {
					$type = null;
					if (is_array($obj)) {
						$type = isset($obj['type']) ? $obj['type'] : null;
						if (is_empty($type) || !in_array($type, array('user', 'group'), true)) {
							return array(new Message(':api.search.conditions.user_selector.type.invalid', array(),
								"conditions[$i][value]"));
						}
					} else {
						$obj = array('type' => 'user', 'code' => $obj);
						$type = 'user';
					}

					if ($type == 'user') {
						$code_ = isset($obj['code']) ? $obj['code'] : null;
						if (is_not_empty($code_)) {
							$user = Engine::factory()->getUser($code_, true, true);
							if ($user === null) {
								return array(new Message(':api.search.conditions.user.not_exist', array($code_), "conditions[$i][value]"));
							}
							$options[] = $user;
						} else {
							$user = new User();
							$user->_setId(null);
							$options[] = $user;// 未選択
						}
					} else if ($type == 'group') {
						$name_ = isset($obj['name']) ? $obj['name'] : null;
						$group = Engine::factory()->findGroupByName($name_);
						if ($group === null) {
							return array(new Message(':api.search.conditions.group.not_exist', array($name_), "conditions[$i][value]"));
						}
						$options[] = $group;
					}
				}
				log_debug(print_r($options, true));
				$value =$options;

			} else {
				$value = $condition['value'];
			}
			
			try {
				$criteria->addCondition($matchedColumns[0], $condition['operator'], $value);
			} catch (TxCoreException $e) {
				return array(new Message(':api.search.conditions.invalid', array(), "conditions[$i]"));
			}
 		}
		
		foreach ($this->sorts as $i => $sort) {
			if (!is_array($sort) ||
				!array_key_exists('column', $sort)) {
				return array(new Message(':api.search.sorts.invalid', array($code), "sorts[$i]"));
			}
			$matchedColumns = $table->getColumns(array('code'=>$sort['column']));
			if (is_empty($matchedColumns)) {
				return array(new Message(':api.search.sorts.columns.not_exist', array($sort['column']), "sorts[$i][column]"));
			}
			if (!$matchedColumns[0]->isSortable()) {
				return array(new Message(':api.search.sorts.columns.not_sortable', array($sort['column']), "sorts[$i][column]"));
			}
			$reverse = false;
			if (isset($sort['reverse']) && $sort['reverse'] == 'true') {
				$reverse = true;
			}
			
			$criteria->addSort($matchedColumns[0], $reverse);
		}

		if (is_not_empty($this->offset)) {
			if (!preg_match('/^\d+$/', $this->offset)) {
				return array(new Message(':api.search.offset.invalid', array(), 'offset'));
			}
			if (is_empty($this->limit)) {
				return array(new Message(':api.search.limit.required', array(), 'limit'));
			}
		}
		
		if (is_not_empty($this->limit)) {
			if (!preg_match('/^\d+$/', $this->limit)) {
				return array(new Message(':api.search.limit.invalid', array(), 'limit'));
			} else if ($this->limit > 100) {
				return array(new Message(':api.search.limit.exceeded', array(), 'limit'));
			}
		}

		if ($this->_table->isChild()) {
			if (is_empty($this->parentTable)) {
				return array(new Message(':api.search.parentTable.required',
					array(), 'parentTable'));
			}
			if (is_empty($this->parentRow)) {
				return array(new Message(':api.search.parentRow.required',
					array(), 'parentRow'));
			}
			if (is_empty($this->parentColumn)) {
				return array(new Message(':api.search.parentColumn.required',
					array(), 'parentColumn'));
			}
			$this->_parentTable = Engine::factory()->getTable(
				$this->parentTable, Engine::RELEASE, null, true);
			if ($this->_parentTable === null) {
				return array(new Message(':api.search.parentTable.not_exist',
					array($this->parentTable), 'parentTable'));
			}
			$this->_parentRow = $this->_parentTable->getRowFor(
				$this->parentRow, null, true, true);
			if ($this->_parentRow === null) {
				return array(new Message(':api.search.parentRow.not_exist',
					array($this->parentRow), 'parentRow'));
			}
			$matchedColumns = $this->_parentTable->getColumns(
				array('code' => $this->parentColumn));
			if (is_empty($matchedColumns)) {
				return array(new Message(':api.search.parentColumn.not_exist',
					array($this->parentColumn), 'parentColumn'));
			}
			if ($matchedColumns[0]->getType() != Column::RELATIONAL_INPUT) {
				return array(new Message(':api.search.parentColumn.invalid_type',
					array($this->parentColumn), 'parentColumn'));
			}
			if ($matchedColumns[0]->getReferenceTable()->getId()
				!= $this->table) {
				return array(new Message(':api.search.parentColumn.not_link',
					array($this->parentColumn, $this->table), 'parentColumn'));
			}
			$this->_parentColumn = $matchedColumns[0];
		}
		
		$this->_criteria = $criteria;
		return $erros;
	}
}

