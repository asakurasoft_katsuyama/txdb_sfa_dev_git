<?php
namespace Form\Api;

use Barge\Web\Form;
use TxCore\Engine;
use TxCore\Message;
use TxCore\User;

/**
 * /api/data/delete.json
 *
 * @author nakatsuka
 *
 */
class DataDeleteForm extends Form
{
	/**
	 * Table id.
	 * @var string
	 */
	public $table;
	
	/**
	 * Api key.
	 * @var string
	 */
	public $apiKey;
	
	/**
	 * rows.
	 * @var array
	 */
	public $rows;
	
	/**
	 * @var Table
	 */
	public $_table;

	
	public function validate(User $user) 
	{
		$errors = array();
		
		if (is_empty($this->table)) {
			return array(new Message(':api.data.table.required', array(), 'table'));
		}
		
		if (is_empty($this->apiKey)) {
			return array(new Message(':api.data.api_key.required', array(), 'api_key'));
		}
		
		if (is_empty($this->rows) || !is_array($this->rows)) {
			return array(new Message(':api.data.rows.required', array(), 'rows'));
		}
		
		if (count($this->rows) > 100) {
			return array(new Message(':api.data.rows.exceeded', array(), 'rows'));
		}
		
		$table = Engine::factory()->getTable($this->table, Engine::RELEASE, null, true);
		if ($table === null || !$table->isApiEnabled() || !$table->isWriteApiEnabled()) {
			return array(new Message(':api.data.table.not_exist', array($this->table), 'table'));
		}
		
		if ($table->getWriteApikey() !== $this->apiKey) {
			return array(new Message(':api.data.api_key.missing', array(), 'api_key'));
		}
		
		if (is_empty($user) || !$table->allows('delete', $user)) {
			return array(new Message(':api.data.permission_denied', array(), 'table'));
		}

		if ($table->isChild()) {
			return array(new Message(':api.data.direct_writing_denied', array($this->table), 'table'));
		}
		
		$this->_table = $table;
		
		foreach ($this->rows as $i => $row) {
			if (!isset($row['no']) || is_empty($row['no'])) {
				$message = new Message(':api.data.row_no.required', array(), "rows[{$i}][no]");
				return array($message);
			}
		}
		
		return $errors;
	}
	
}

