<?php
namespace Form\Api;

use Barge\Web\Form;
use TxCore\Engine;
use TxCore\Message;
use TxCore\User;

/**
 * /api/file/upload.json
 *
 * @author nakatsuka
 *
 */
class FileUploadForm extends Form
{
	/**
	 * Table id.
	 * @var string
	 */
	public $table;
	
	/**
	 * Api key.
	 * @var string
	 */
	public $apiKey;
	
	/**
	 * @var Table
	 */
	public $_table;

	
	public function validate(User $user) 
	{
		$errors = array();
		
		if (is_empty($this->table)) {
			return array(new Message(':api.file.table.required', array(), 'table'));
		}
		
		if (is_empty($this->apiKey)) {
			return array(new Message(':api.file.api_key.required', array(), 'api_key'));
		}
		
		$table = Engine::factory()->getTable($this->table, Engine::RELEASE, null, true);
		if ($table === null || !$table->isApiEnabled() || !$table->isWriteApiEnabled()) {
			return array(new Message(':api.file.table.not_exist', array($this->table), 'table'));
		}
		
		if ($table->getWriteApikey() !== $this->apiKey) {
			return array(new Message(':api.file.api_key.missing', array(), 'api_key'));
		}
		
		if (is_empty($user)) {
			return array(new Message(':api.file.permission_denied', array()));
		}
		
		$this->_table = $table;
		
		return $errors;
	}
	
}

