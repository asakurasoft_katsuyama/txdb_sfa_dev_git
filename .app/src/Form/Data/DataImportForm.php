<?php
namespace Form\Data;

use TxCore\Message;

use Barge\Web\Form;

class DataImportForm extends Form
{
	public $tableId = 0;
	public $charset = null;
	public $file = null;
	public $keyColumnId = 0;
	
	public function validate() 
	{
		if (is_empty($this->file)) {
			return array(new Message(':data_import.file.required'));
		}
		return array();
	}
	
}

