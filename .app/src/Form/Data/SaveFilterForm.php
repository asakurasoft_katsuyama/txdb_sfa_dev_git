<?php
namespace Form\Data;

use TxCore\Message;

use Barge\Web\Form;


class SaveFilterForm extends Form
{
	public $filter = 0;
	public $tableId = 0;
	public $name;
	public $criteria;
// 	public $onlyValidation = false;
	public function validate() 
	{
		if ($this->filter == 0 && is_empty($this->name) ) {
			return array(new Message(':data_index.filter_name.required'));
		}
		return array();
	}
}

