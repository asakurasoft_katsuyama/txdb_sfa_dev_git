<?php
namespace Form\Data;

use Barge\Web\Form;


class DataExportForm extends Form
{
	public $tableId = 0;
	public $heading = false;
	public $charset = null;
	public $importing = false;
}

