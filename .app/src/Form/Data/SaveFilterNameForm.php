<?php
namespace Form\Data;

use TxCore\Message;

use Barge\Web\Form;


class SaveFilterNameForm extends Form
{
	public $filterId = 0;
	public $tableId = 0;
	public $name;

	public function validate() 
	{
		if (is_empty($this->name) ) {
			return array(new Message(':data_index.filter_name.required'));
		}
		return array();
	}
}

