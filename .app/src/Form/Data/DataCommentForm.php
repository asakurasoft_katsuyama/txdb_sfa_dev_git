<?php
namespace Form\Data;

use Barge\Web\Form;
use TxCore\Message;


class DataCommentForm extends Form
{
	public $rowId;

	public $tableId;

	public $text;

	/**
	 * JSON
	 * @var
	 */
	public $notificationTarget;

	/**
	 * @return array
	 */
	public function validate()
	{
		$messages = array();
		if (is_empty($this->text)) {
			$messages[] = new Message(':data_view.comment.text.required', array());
		}
		return $messages;
	}
}

