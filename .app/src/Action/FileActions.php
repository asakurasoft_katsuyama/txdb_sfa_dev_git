<?php 
namespace Action;

use TxCore\Util;

use Image\Resizer;

use Barge\Web\Actions;
use Barge\Web\ActionContext;

use TxCore\Engine;
use TxCore\User;
// use TxCore\TableDef;
// use TxCore\ColumnDef;

use Form\IndexForm;


class FileActions extends BaseActions 
{
	
	/**
	 * @param ActionContext $ctx
	 * @param string $action
	 */
	public function checkLogin(ActionContext $ctx, $action) 
	{
		// ログインチェックなし
		return true;
	}
	
	/**
	 * サムネイル表示（.htaccess 参照)
	 * 
	 * @param ActionContext $ctx
	 */
	public function thumb(ActionContext $ctx)
	{
		$matches = array();
		if (!preg_match('#/(\d+)_(\d+)_(.+)_t(\d+)(\..+)$#', $ctx->getInternalRequestURI(), $matches)) {
			log_error(sprintf('Invalid request path : %s', $ctx->getInternalRequestURI()));
			return $ctx->respondDummyGif();
		}

		$size = intval($matches[4]);
		if (intval($size) > 500) {
			log_error(sprintf('Invalid size was specified : %s', $ctx->getInternalRequestURI()));
			return $ctx->respondDummyGif();	
		}
		
		$srcPath = substr($ctx->getRequestURI(), 1);
		if (strpos($srcPath, '?') !== false) 
			$srcPath = substr($srcPath, 0, strpos($srcPath, '?'));
		$destPath = substr($ctx->getInternalRequestURI(), 1);

		$resizer = new Resizer(Resizer::IMAGICK);
		$resizer->setAllowScaleUp(false);
		$resizer->setStyle(Resizer::CONTAIN);
		list ($type) = $resizer->resize($srcPath, $destPath, $size, $size);
		
		return $ctx->respondFile($destPath, image_type_to_mime_type($type));
	}

	/**
	 * ファイルのダウンロード
	 *
	 * @param ActionContext $ctx
	 */
	public function download(ActionContext $ctx)
	{
		$p1 = strrpos($ctx->getRequestURI(), '/');
		$p2 = strrpos(substr($ctx->getRequestURI(), 0, $p1), '/');
		$url = substr($ctx->getRequestURI(), 0, $p2);
		$fileName = substr($ctx->getRequestURI(), $p1 + 1);

		$baseUrl = get_config('app', 'user_file_dir_url');
		if (!str_starts_with($baseUrl, $url)) {
			log_error(sprintf('File dose not exists : %s', $url));
			return;
		}
		
		$filePath = concat_path(get_config('app', 'user_file_dir'), substr($url, strlen($baseUrl)));
		if (!file_exists($filePath)) {
			log_error(sprintf('File dose not exists : %s', $url));
			return;
		}

		$ctx->setHeader('Content-Disposition', 'attachment');	// IEはこれがないとダウンロードにならない
		return $ctx->respondFile($filePath, 'application/octet-stream');
	}
	
}
