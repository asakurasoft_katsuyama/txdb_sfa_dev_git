<?php
namespace Action\My;

use Action\BaseActions;

use Barge\Web\Actions;
use Barge\Web\ActionContext;

use TxCore\Message;

use TxCore\User;
use TxCore\Notification;
use TxCore\TxCoreException;
use TxCore\Engine;

class NotificationActions extends BaseActions
{

    /**
     * @param ActionContext $ctx
     */
    public function index(ActionContext $ctx)
    {
        return $ctx->render('my/notification/notification_index.tpl',
            array(
                'limit' => $this->getStoredLimit($ctx), 'read' => $this->getStoredRead($ctx) ? 'read' : 'unread'
            )
        );
    }

    /**
     * @param ActionContext $ctx
     */
    public function listUnread(ActionContext $ctx)
    {
        $this->storeLimit($ctx, array(0, null));
        $this->storeRead($ctx, 'unread');
        return $ctx->redirect('/my/notification/');
    }

    /**
     * @param ActionContext $ctx
     * @return array
     */
    private function getStoredLimit(ActionContext $ctx)
    {
        $searchStatus = $ctx->getSession('user_notification_limit');
        if ($searchStatus)
            return $searchStatus;
        return array(0, 20);
    }


    /**
     * @param ActionContext $ctx
     * @param array $limit
     */
    private function storeLimit(ActionContext $ctx, array $limit = null)
    {
        $ctx->setSession('user_notification_limit', $limit);
    }

    /**
     * @param ActionContext $ctx
     * @param boolean $read
     */
    private function storeRead(ActionContext $ctx, $read)
    {
        $ctx->setSession('user_notification_read', $read == 'read');
    }

    /**
     * @param ActionContext $ctx
     * @return string
     */
    private function getStoredRead(ActionContext $ctx)
    {
        $searchStatus = $ctx->getSession('user_notification_read');
        if ($searchStatus)
            return $searchStatus;
        return null;
    }

    /**
     * @param ActionContext $ctx
     */
    public function indexJson(ActionContext $ctx,
        $read = 'unread', $offset = 0, $limit = 20)
    {
        $list = $this->engine->searchNotificationReceipts(
            $ctx->getUser(), $read == 'read', $offset, $limit);
        $this->storeRead($ctx, $read);
        $this->storeLimit($ctx, array($offset, $limit));

        return $ctx->respondJSON(array('status'=>'success', 'result'=>$list));
    }

    public function toggleReadJson(ActionContext $ctx, $id, $is_read = false)
    {
        $ids = is_array($id) ? $id : array($id);
        $user = $ctx->getUser();
        foreach ($ids as $_id) {
            $receipt = $this->engine->getNotificationReceipt($_id, $user);
            $receipt->setRead($is_read);
            $receipt->save();
        }
        return $ctx->respondJSON(array('status'=>'success'));
    }

}
