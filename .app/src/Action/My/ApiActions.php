<?php
namespace Action\My;

use Action\BaseActions;
use Barge\Web\ActionContext;
use TxCore\Engine;
use TxCore\User;

/**
 * General internal APIs.
 * TODO : テスト作ってない
 *
 * ## Rules
 * - get, create, update, delete
 * — 特定画面用の加工はしない（する場合は個別のアクションへ）
 * - レスポンスのネスト構造はオプションパラメータ<code>with</code>で制御する（デフォルトは最小構成を返す）
 * - リスト系は、List_を返す
 * - レスポンス形式は、<code>array('status' => 'success', 'data' => xxx )</code>
 * - ハンドリングしたい異常系は status = failed
 */
class ApiActions extends BaseActions
{
    /**
     * Bulk call interface.
     * TODO : テスト
     * /my/api/v1/call.json?apis[0]=get_table.json?id=1&apis[1]=get_users.json?limit=10
     * @param ActionContext $ctx
     * @param array $apis
     * @return bool
     * @throws \Exception
     */
    public function callJson(ActionContext $ctx, $apis = array())
    {
        $proxy = ActionContextProxy::create($ctx);

        foreach ($apis as $api) {
            $p = strpos($api, '?');
            $params = array();
            if ($p !== null) {
                parse_str(substr($api, $p + 1), $params);
                $api = substr($api, 0, $p);
            }

            $proxy->forward($api, $params);
        }

        $ctx->setHeader('Content-Type', "application/json; charset=utf-8");
        return $ctx->respondJSON($proxy->getResponses());
    }

    /**
     * /my/api/v1/get_table.json?id=1
     * @param ActionContext $ctx
     * @param int $id
     * @param string $revision 'trunk', 'release', default release
     * @param array $with
     * @return bool
     * @throws \TxCore\TxCoreException
     */
    public function getTableJson(ActionContext $ctx, $id = -1, $revision = Engine::RELEASE, $with = array())
    {
        assert(in_array($revision, array(Engine::RELEASE, Engine::TRUNK), true));
        $table = $this->engine->getTable($id, $revision, $ctx->getUser());
        return $ctx->respondJSON(array('status' => 'success', 'data' => $table ));
    }

    /**
     * /api/v1/user.json?id=1
     * @param ActionContext $ctx
     * @param int $id
     * @return bool
     * @throws \TxCore\TxCoreException
     */
    public function getUserJson(ActionContext $ctx, $id = -1)
    {
        $user = $this->engine->getUser($id);
        return $ctx->respondJSON(array('status' => 'success', 'data' => $user ));
    }

    /**
     * /api/v1/group.json?id=1
     * @param ActionContext $ctx
     * @param int $id
     * @return bool
     * @throws \TxCore\TxCoreException
     */
    public function getGroupJson(ActionContext $ctx, $id = -1)
    {
        $group = $this->engine->getGroup($id);
        return $ctx->respondJSON(array('status' => 'success', 'data' => $group ));
    }

    /**
     * /api/v1/users.json
     * @param ActionContext $ctx
     * @return bool
     */
    public function getUsersJson(ActionContext $ctx)
    {
        $users = $this->engine->searchUsers(null, 0, PHP_INT_MAX);
        return $ctx->respondJSON(array('status'=>'success', 'data'=> $users));
    }

    /**
     * /api/v1/groups.json
     * @param ActionContext $ctx
     * @return bool
     */
    public function getGroupsJson(ActionContext $ctx)
    {
        $groups = $this->engine->searchGroups();
        return $ctx->respondJSON(array('status'=>'success', 'data'=>$groups));
    }





}

/**
 * @package Action\My
 */
class ActionContextProxy
{
    /**
     * @var ActionContext
     */
    private $context;

    /**
     * @var array
     */
    private $responses = array();

    /**
     * @param $context
     * @return ActionContext|ActionContextProxy
     */
    public static function create(ActionContext $context)
    {
        return new self($context);
    }

    private function __construct($context)
    {
        $this->context = $context;
    }

    public function __call($name, $args)
    {
        // Hook JSON response.
        if ($name === 'respondJSON') {
            $this->responses[] = $args[0];
        } else
            return call_user_func_array($name, $args);
    }

    /**
     * @return array
     */
    public function getResponses()
    {
        return $this->responses;
    }

}
