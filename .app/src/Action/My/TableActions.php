<?php
namespace Action\My;

use Form\Table\SaveReadCheckForm;
use Form\Table\SaveCalendarForm;
use Form\Table\SaveNotificationSettingsForm;
use TxCore\CalendarSettings;
use TxCore\Option;
use TxCore\ReadCheckSettings;
use TxCore\Reference;
use TxCore\Column;
use TxCore\Table;
use TxCore\Row;
use TxCore\Icon;
use TxCore\Util;
use TxCore\LookupValue;
use TxCore\TxCoreException;
use TxCore\Engine;
use TxCore\User;
use TxCore\Permission;
use TxCore\NotificationBasicRule;
use TxCore\NotificationConditionalRule;
use TxCore\NotificationCriteria;

use Action\BaseActions;
use TxCore\Criteria;
use Barge\Web\Actions;
use Barge\Web\ActionContext;
use Barge\Web\Uploader;

use Barge\DB\DBClient;

use Form\LoginForm;
use TxCore\Message;
use TxCore\ViewColumn;
use Util\TableUnderConstructionConverter;

class TableActions extends BaseActions
{
	/**
	 * @param ActionContext $ctx
	 * @param $id table id.
	 */
	public function index(ActionContext $ctx)
	{
		$_tables = $this->engine->getUserTables($ctx->getUser());
		$tables = array();
		foreach ($_tables as $table) {
			if ($table->allows('alter', $ctx->getUser())) {
				$tables[] = $table;
			}
		}
		$publicTables = array();
		$privateTables = array();
		foreach ($tables as $table) {
			if ($table->isPublic()) {
				$publicTables[] = $table;
			} else {
				$privateTables[] = $table;
			}
		}

		$tableLimit = get_config('app', 'table_num_limit');
		if ($tableLimit <= 0) $tableLimit = "無制限";
		$tableCount = $this->engine->countTables();

		return $ctx->render('my/table/table_index.tpl',
				array('publicTables' => $publicTables, 'privateTables' => $privateTables,
					'tableCount' => $tableCount, 'tableLimit' => $tableLimit));
	}

	/**
	 * @param ActionContext $ctx
	 * @param number $t
	 */
	public function referableTablesJson(ActionContext $ctx, $t=0)
	{
		$_tables = $this->engine->getUserTables($ctx->getUser());
		$tables = array();
		foreach ($_tables as $table) {
			// 参照権限の確認
			if(!$ctx->getUser()->isAdmin() && ($table->getOwner() === null || $table->getOwner()->getId() != $ctx->getUser()->getId())){
				$permissionControl = $table->getPermissionControl();
				if($permissionControl){
					$permission = $table->getPermissionForUserId($ctx->getUser()->getId(), true);
					if(is_null($permission) || !$permission->getIsReadable()){
						continue;
					}
				}
			}
			if ($table->getId() != $t) {
				$tables[] = $table;
			}
		}
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$tables));
	}

	/**
	 * @param ActionContext $ctx
	 * @param number $t
	 */
	public function childTableCandidatesJson(ActionContext $ctx, $t=0, $c=0)
	{
		$workTable = $this->getTableForModifyForm($ctx, $t);
		$workColumn = $workTable->getColumnFor($c);
		$_tables = $this->engine->getUserTables($ctx->getUser());
		$tables = array();
		foreach ($_tables as $table) {
			// 権限の確認
			if (
				!$table->allows('select', $ctx->getUser())
				|| !$table->allows('insert', $ctx->getUser())
				|| !$table->allows('update', $ctx->getUser())
				|| !$table->allows('delete', $ctx->getUser())
			) {
				continue;
			}

			// 編集中テーブルの他のカラムから参照されているテーブルを除く
			$filter = array('type' => Column::RELATIONAL_INPUT);
			$inOtherColumn = false;
			foreach ($workTable->getColumns($filter) as $column) {
				if ($workColumn->getId() == $column->getId()) {
					continue;
				}
				if ($column->getReferenceTable()
					&& $column->getReferenceTable()->getId()
						== $table->getId()) {
					$inOtherColumn = true;
					break;
				}
			}
			if ($inOtherColumn) continue;

			// 他のテーブルから参照されているテーブルを除く
			if ($table->isChild(true)) {
				$inWorkTable = false;
				foreach ($workTable->getColumns($filter) as $column) {
					if ($column->getReferenceTable()
						&& $column->getReferenceTable()->getId()
							== $table->getId()) {
						$inWorkTable = true;
						break;
					}
				}
				foreach ($workTable->_getRemovedColumns() as $column) {
					if ($column->getType() == Column::RELATIONAL_INPUT
						&& $column->getReferenceTable()
						&& $column->getReferenceTable()->getId()
							== $table->getId()) {
						$inWorkTable = true;
						break;
					}
				}
				if (!$inWorkTable) continue;
			}

			// すでに親テーブルとなっているテーブルを除く
			if ($table->isParent(true)) {
				continue;
			}

			// 重複チェックのあるフィールドを持つテーブルを除く
			$referenceTrunk = $this->engine->getTable($table->getId(),
				Engine::TRUNK);
			if ($table->hasUniqueConstraint()
				|| $referenceTrunk->hasUniqueConstraint()) {
				continue;
			}

			if ($table->getId() != $t) {
				$tables[] = $table;
			}
		}
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$tables));
	}

	/**
	 * @param ActionContext $ctx
	 */
	public function create(ActionContext $ctx)
	{
		$_tables = $this->engine->getUserTables($ctx->getUser());
		$tables = array();
		foreach ($_tables as $table) {
			if ($table->allows('alter', $ctx->getUser())) {
				$tables[] = $table;
			}
		}
		$templates = $this->engine->getTemplates();
		return $ctx->render('my/table/table_create.tpl', array('userTables' => $tables, 'templates' => $templates));
	}


	/**
	 * @param ActionContext $ctx
	 * @param $t table id.
	 */
	public function createInfo(ActionContext $ctx, $t = 0)
	{
		$table = $this->engine->getTable($t);
		return $ctx->render('my/table/table_create_info.tpl', array('table' => $table) );
	}

	/**
	 * @param ActionContext $ctx
	 * @param $t table id.
	 */
	public function dropJson(ActionContext $ctx, $t = 0)
	{
		$table = $this->engine->getTable($t);
		$referrers = $table->getReferrerTables();
		if (sizeof($referrers) > 0) {
			$message = "他のデータベースから参照されています。\n";
			$message .= "参照元:\n";
			foreach ($referrers as $table) {
				$message .= "・{$table->getName()}\n";
			}
			return $ctx->respondJSON(array('status'=>'error',
					'errors'=>array(new Message($message))));
		}
		$columns = $table->getColumns();
		foreach ($table->getColumns() as $column) {
			if ($column->getType() == Column::RELATIONAL_INPUT) {
				$message = $column->getTypeName()
					. "フィールドがあります。\n"
					. "データベースを削除する前に、"
					. $column->getTypeName()
					. "フィールドを削除し、適用してください。";
				return $ctx->respondJSON(array('status'=>'error',
						'errors'=>array(new Message($message))));
			}
		}
		$table->drop($ctx->getUser());

		$this->engine->scheduleOptimization();

		return $ctx->respondJSON(array('status'=>'success', 'result'=>$table->getId()));
	}

	/**
	 * @param ActionContext $ctx
	 * @param $t table id.
	 */
	public function releaseJson(ActionContext $ctx, $t = 0)
	{
		$table = $this->engine->getTable($t, Engine::TRUNK, $ctx->getUser());

		$errors = $table->validateRelease();
		if (sizeof($errors) > 0) {
			return $ctx->respondJSON(array('status'=>'error', 'errors'=>$errors));
		}
		$table->release($ctx->getUser());
		$this->resetOnRelease($ctx, $table);
		$ctx->setSession('flash_message', new Message('変更が適用されました。'));
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$table->getId()));
	}

	/**
	 * @param ActionContext $ctx
	 * @param Table $table
	 */
	private function resetOnRelease(ActionContext $ctx, Table $table)
	{
		$searchStatus = $ctx->getSession('search');
		if (!$searchStatus)
			$searchStatus = array();
		$searchStatus[$table->getId()] = null;
		$ctx->setSession('search', $searchStatus);
	}

	/**
	 * @param ActionContext $ctx
	 * @param $t table id.
	 */
	public function rollbackJson(ActionContext $ctx, $t = 0)
	{
		$table = $this->engine->getTable($t, Engine::TRUNK, $ctx->getUser());
		$table->discardChanges($ctx->getUser());
		$ctx->setSession('flash_message', new Message('変更が破棄されました。'));
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$table->getId()));
	}

	/**
	 * @param ActionContext $ctx
	 * @param $t table id.
	 */
	public function modify(ActionContext $ctx, $t = 0)
	{
		$table = $this->engine->getTable($t, Engine::TRUNK, $ctx->getUser());
		$finishedMessage = null;
		if ($flashMessage = $ctx->getSession('flash_message')) {
			$finishedMessage = $flashMessage->getMessage();
			$ctx->setSession('flash_message', null);
		}
		return $ctx->render('my/table/table_modify.tpl', array('table' => $table,
			'finishedMessage' => $finishedMessage));
	}

	/**
	 * @param ActionContext $ctx
	 * @param $t table id.
	 */
	public function modifyForm(ActionContext $ctx, $t = 0)
	{
		$this->unsetTableForModifyForm($ctx, $t);
		$table = $this->engine->getTable($t, Engine::TRUNK, $ctx->getUser());
		$this->setTableForModifyForm($ctx, $table);
		return $ctx->render('my/table/table_modify_form.tpl', array('table' => $table));
	}

	protected function setTableForModifyForm(ActionContext $ctx, Table $table)
	{
		$tableStatus = $ctx->getSession('table_form');
		if (!$tableStatus)
			$tableStatus = array();
		$tableStatus[$table->getId()] = $table;
		$ctx->setSession('table_form', $tableStatus);
	}

	/**
	 * @param ActionContext $ctx
	 * @param $tableId
	 * @return Table
	 */
	protected function getTableForModifyForm(ActionContext $ctx, $tableId)
	{
		$tableStatus = $ctx->getSession('table_form');
		if (!$tableStatus || !isset($tableStatus[$tableId]))
			return null;
		return $tableStatus[$tableId];
	}

	protected function unsetTableForModifyForm(ActionContext $ctx, $tableId)
	{
		$tableStatus = $ctx->getSession('table_form');
		if (!$tableStatus)
			return;
		$tableStatus[$tableId] = null;
		$ctx->setSession('table_form', $tableStatus);
	}

	/**
	 * @param ActionContext $ctx
	 */
	public function updateColumnJson(ActionContext $ctx)
	{
		$json = $ctx->getParam('data');
		$data = json_decode($json, true);
		$t = $data['id'];
		$temp = $data['temp'];
		$defs = $data['defs'];

		$table = $this->getTableForModifyForm($ctx, $t);

		$this->updateForm($table, $defs);
		$column = $this->updateColumn($table, $temp);

		$messages = $column->handleValidateProperties();
		if (sizeof($messages)) {
			return $ctx->respondJSON(array('status'=>'error', 'errors'=>$messages));
		} else {
			return $ctx->respondJSON(array('status'=>'success', 'result'=>$column->getId()));
		}

	}
	/**
	 * @param ActionContext $ctx
	 */
	public function restoreOptionsJson(ActionContext $ctx)
	{
		$json = $ctx->getParam('data');
		$data = json_decode($json, true);
		$t = $data['id'];
		$defs = $data['defs'];
		$c = $data['columnId'];
		$addedOptionIds = $data['addedOptionIds'];
		$removedOptionIds = $data['removedOptionIds'];

		$table = $this->getTableForModifyForm($ctx, $t);

		$column = $table->getColumnFor($c);
		foreach ($addedOptionIds as $o) {
			$option = $column->getOptionFor($o, false, false);
			$column->removeOption($option);
		}
		foreach ($removedOptionIds as $o) {
			$option = $column->getRemovedOptionFor($o);
			$column->restoreRemovedOption($option);
		}

		$this->updateForm($table, $defs);
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$column->getId()));
	}

	/**
	 * @param ActionContext $ctx
	 */
	public function restoreTabsJson(ActionContext $ctx)
	{
		$json = $ctx->getParam('data');
		$data = json_decode($json, true);
		$t = $data['id'];
		$defs = $data['defs'];
		$addedTabIds = $data['addedTabIds'];
		$removedTabIds = $data['removedTabIds'];

		$table = $this->getTableForModifyForm($ctx, $t);

		foreach ($addedTabIds as $c) {
			$column = $table->getColumnFor($c, false);
			$table->removeColumn($column);
		}
		foreach ($removedTabIds as $o) {
			$column = $table->getRemovedColumnFor($o);
			$table->restoreRemovedColumn($column);
		}

		$this->updateForm($table, $defs);
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$table->getId()));
	}

	/**
	 *
	 * @param Table $table
	 * @param array $def
	 * @return Column
	 */
	private function updateColumn(Table $table, $def)
	{
		$c = $def['id'];

		$column = $table->getColumnFor($c);
		if (array_key_exists('options', $def)) {
			$optionSeq = 1;
			foreach ($def['options'] as $optionDef) {
				$o = $optionDef['id'];
				$option = $column->getOptionFor($o, false, false);
				$option->setName($optionDef['name']);
				$option->setSeq($optionSeq);
				$optionSeq++;
			}
		}
		$this->setColumnDef($column, $def);

		return $column;
	}

	/**
	 * @param ActionContext $ctx
	 */
	public function saveFormJson(ActionContext $ctx)
	{
		$json = $ctx->getParam('data');
		$data = json_decode($json, true);
		$t = $data['id'];
		$defs = $data['defs'];

		$table = $this->getTableForModifyForm($ctx, $t);
		$this->updateForm($table, $defs);

		$messages = $table->validate();
		if (sizeof($messages)) {
			return $ctx->respondJSON(array('status'=>'error', 'errors'=>$messages));
		}

		try {
			$table->saveChanges($ctx->getUser());
			$this->unsetTableForModifyForm($ctx, $t);
			return $ctx->respondJSON(array('status'=>'success', 'result'=>$table->getId()));
		} catch (TxCoreException $e) {
			if ($e->getCode() === TxCoreException::TABLE_HAS_UPADTED) {
				$this->rollback();
				return $ctx->respondJSON(array('status'=>'error',
						'errors'=>array(new Message(':table_modify.table_has_updated'))));
			} else {
				throw $e;
			}
		}
	}

	/**
	 * update table object for preview.
	 * @param ActionContext $ctx
	 */
	public function updateFormJson(ActionContext $ctx)
	{
		$json = $ctx->getParam('data');
		$data = json_decode($json, true);
		$t = $data['id'];
		$defs = $data['defs'];

		$table = $this->getTableForModifyForm($ctx, $t);
		$this->updateForm($table, $defs);
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$table->getId()));
	}

	/**
	 * @param ActionContext $ctx
	 * @return Table
	 */
	private function updateForm(Table $table, $defs)
	{
		foreach ($defs as $def) {
			$this->updateColumn($table, $def);
		}

		return $table;
	}

	protected function setColumnDef($column, $def) {

		$table = $column->getParent();
		$referenceTable = null;
		if (isset($def['referenceTableId'])) {
			$referenceTable = $this->engine->getTable($def['referenceTableId']);
		}
		if (isset($def['layoutParentId'])) {
			$layoutParent = $table->getColumnFor($def['layoutParentId']);
		}

		foreach ($def as $property => $value) {
			switch($property){
				//case 'id':
				case 'code':			$column->setCode($value); break;
				//case 'type':
				case 'label':			$column->setLabel($value); break;
				//case 'options':
				case 'showsField':		$column->setShowsField($value); break;
				case 'showsLabel':		$column->setShowsLabel($value); break;
				case 'positionY':		$column->setPositionY($value); break;
				case 'positionX':		$column->setPositionX($value); break;
				case 'sizeWidth':		$column->setSizeWidth($value); break;
				case 'sizeHeight':		$column->setSizeHeight($value); break;
				case 'isUnique':		$column->setUnique($value); break;
				case 'isRequired':		$column->setRequired($value); break;
				case 'minLength':		$column->setMinLength($value); break;
				case 'maxLength':		$column->setMaxLength($value); break;
				case 'minValue':		$column->setMinValue($value); break;
				case 'maxValue':		$column->setMaxValue($value); break;
				case 'expression':		$column->setExpression($value); break;
				case 'format':			$column->setFormat($value); break;
				case 'dateFormat':		$column->setDateFormat($value); break;
				case 'showsDigit':		$column->setShowsDigit($value); break;
				case 'labelHtml':		$column->setLabelHtml($value); break;
				case 'thumbnailMaxSize':$column->setThumbnailMaxSize($value); break;
				case 'imageMaxSize':		$column->setImageMaxSize($value); break;
				case 'linkType':		$column->setLinkType($value); break;
				case 'referenceSortsReverse':$column->setReferenceSortsReverse($value); break;
				case 'referenceMaxRow':	$column->setReferenceMaxRow($value); break;
				case 'optionsAlignment':$column->setOptionsAlignment($value); break;

				case 'datecalcUnit':	$column->setDatecalcUnit($value); break;
				case 'datecalcBeforeAfter':	$column->setDatecalcBeforeAfter($value); break;

				case 'defaultValue':
					$defaultValue = null;
					if ($column->getType() == Column::RADIO
						|| $column->getType() == Column::DROPDOWN) {
						if ($value != null) {
							$option = $column->getOptionFor($value, false, false);
							$defaultValue = $option;
						}
					} else if ($column->getType() == Column::CHECKBOX) {
						$defaultValue = array();
						foreach ($value as $optionId) {
							$option = $column->getOptionFor($optionId, false, false);
							$defaultValue[] = $option;
						}
					} else {
						$defaultValue = $value;
					}
					$column->setDefaultValue($defaultValue);
					break;
				//case 'referenceTable': // not property
				case 'referenceTableId':
					$column->setReferenceTable($referenceTable);
					break;
				case 'referenceTargetColumnId':
				//case 'referenceTargetColumnType':
					$referenceTargetColumn = null;
					if ($referenceTable && $value !== null) {
						$referenceTargetColumn = $referenceTable->getColumnFor($value);
					}
					$column->setReferenceTargetColumn($referenceTargetColumn);
					break;
				case 'referenceSourceColumnId':
					$referenceSourceColumn = null;
					if ($value !== null) {
						$referenceSourceColumn = $table->getColumnFor($value);
					}
					$column->setReferenceSourceColumn($referenceSourceColumn);
					break;
				case 'referenceColumnIds':
					$referenceColumns = array();
					if ($referenceTable) {
						foreach ($value as $c) {
							$referenceColumn = $referenceTable->getColumnFor($c);
							if (!$referenceColumn) continue;
							$referenceColumns[] = $referenceColumn;
						}
					}
					$column->setReferenceColumns($referenceColumns);
					break;
				case 'referenceSortColumnId':
					$referenceSortColumn = null;
					if ($referenceTable && $value !== null) {
						$referenceSortColumn = $referenceTable->getColumnFor($value);
					}
					$column->setReferenceSortColumn($referenceSortColumn);
					break;
				case 'referenceCopyMapIds':
					$referenceCopyMap = array();
					if ($referenceTable) {
						foreach ($value as $map) {
							$referenceColumn = $referenceTable->getColumnFor($map[0]);
							$selfColumn = $table->getColumnFor($map[1]);
							if (!$referenceColumn) continue;
							if (!$selfColumn) continue;
							$referenceCopyMap[] = array(
								$referenceColumn,
								$selfColumn
							);
						}
					}
					$column->setReferenceCopyMap($referenceCopyMap);
					break;
				case 'referenceAggregations':
					$referenceAggregation = array();
					if($referenceTable){
						foreach ($value as $pair) {
							$referenceColumn = $referenceTable->getColumnFor($pair[0]);
							if (!$referenceColumn) continue;
							if (!in_array($referenceColumn->getType(),
								array(Column::ROWNUM, Column::NUMBER, Column::CALC, Column::DATECALC))) continue;
							if (!array_key_exists(1, $pair) || !$pair[1]) continue;
							$referenceAggregation[] = array($referenceColumn, $pair[1]);
						}
					}
					$column->setReferenceAggregations($referenceAggregation);
					break;
				case 'dateSourceColumnId':
					$dateSourceColumn = null;
					if ($value !== null) {
						$dateSourceColumn = $table->getColumnFor($value);
					}
					$column->setDateSourceColumn($dateSourceColumn);
					break;
				case 'layoutParentId':
					$layoutParent = null;
					if ($value !== null) {
						$layoutParent = $table->getColumnFor($value);
					}
					$column->setLayoutParent($layoutParent);
					break;

			}
		}
	}

	/**
	 * @param ActionContext $ctx
	 */
	public function formColumnsJson(ActionContext $ctx, $t=0)
	{
		$table = $this->getTableForModifyForm($ctx, $t);
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$table->getColumns()));
	}

	/**
	 * @param ActionContext $ctx
	 */
	public function formReferrersJson(ActionContext $ctx, $t=0, $c=0)
	{
		$table = $this->getTableForModifyForm($ctx, $t);
		$column = $table->getColumnFor($c);
		$tables = $column->getReferrerTables();
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$tables));
	}

	/**
	 * @param ActionContext $ctx
	 * @param int $t
	 * @param int $c
	 * @return bool
	 * @throws \Exception
	 */
	public function checkRemovedJson(ActionContext $ctx, $t=0, $c=0)
	{
		$table = $this->getTableForModifyForm($ctx, $t);
		$column = $table->getColumnFor($c);
		$tables = $column->getReferrerTables();

		$messages = array();
		if (sizeof($tables) > 0) {
			$messages[] = new Message("他のデータベースから参照されています。\n参照元:\n".
				join("\n", array_map(function(Table $table) {
					return "・".$table->getName();
				}, $tables)));
		}

		$settings = $table->getCalendarSettings();

		// Trunkをチェック.
		if ($table->isCalendarEnabled() &&
		     $settings &&
			($settings->getDateColumnId() === $c ||
			 $settings->getUserColumnId() === $c ||
			 $settings->getEndTimeColumnId() === $c ||
			 $settings->getStartTimeColumnId() === $c ||
			 $settings->getTitleColumnId() === $c)) {
			$messages[] = new Message("カレンダー表示設定で使用されているため、削除できません。");
		}

		return $ctx->respondJSON(array('status' => 'success', 'errors' => $messages));
	}

	/**
	 * @param ActionContext $ctx
	 * @param $t table id.
	 */
	public function columnsJson(ActionContext $ctx, $t = 0)
	{
		$table = $this->engine->getTable($t, Engine::TRUNK, $ctx->getUser());
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$table->getColumns()));
	}

	/**
	 * @param ActionContext $ctx
	 * @param $t table id.
	 */
	public function referenceColumnsJson(ActionContext $ctx, $t = 0)
	{
		$release = $this->engine->getTable($t);
		$trunk = $this->engine->getTable($t, ENGINE::TRUNK);

		// trunkにないカラムは削除されると見なし、返却しない。
		$columns = array();

		// 参照権限の確認
		if(!$ctx->getUser()->isAdmin() && ($release->getOwner() === null || $release->getOwner()->getId() != $ctx->getUser()->getId())){
			$permissionControl = $release->getPermissionControl();
			if($permissionControl){
				$permission = $release->getPermissionForUserId($ctx->getUser()->getId(), true);
				if(is_null($permission) || !$permission->getIsReadable()){
					return $ctx->respondJSON(array('status'=>'success', 'result'=>$columns));
				}
			}
		}

		foreach ($release->getColumns() as $column) {
			foreach ($trunk->getColumns() as $column2) {
				if ($column->getId() == $column2->getId()) {
					$columns[] = $column;
					break;
				}
			}
		}

		return $ctx->respondJSON(array('status'=>'success', 'result'=>$columns));
	}

	/**
	 * @param ActionContext $ctx
	 * @param $t table id.
	 */
	public function columnPropertyDescriptorsJson(ActionContext $ctx, $types)
	{
		$descriptors = array();
		foreach ($types as $type) {
			$column = Column::factory($type);
			$descriptors[$type] = $column;
		}
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$descriptors),
				new TableUnderConstructionConverter($ctx->getUser()));
	}

	/**
	 * @param ActionContext $ctx
	 * @param $t table id.
	 * @param $type column type.
	 */
	public function addColumnJson(ActionContext $ctx, $t = 0, $type)
	{
		$table = $this->getTableForModifyForm($ctx, $t);
		$column = $table->addColumn($type);
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$column),
				new TableUnderConstructionConverter($ctx->getUser()));
	}

	/**
	 * @param ActionContext $ctx
	 * @param $t table id.
	 * @param $c column id.
	 */
	public function removeColumnJson(ActionContext $ctx, $t = 0, $c = 0)
	{
		$table = $this->getTableForModifyForm($ctx, $t);
		$column = $table->getColumnFor($c);

		$viewColumns = array();
		foreach ($table->getViewColumns() as $viewColumn) {
			if ($column->getId() != $viewColumn->getColumn()->getId()) {
				$viewColumns[] = $viewColumn;
			}
		}
		$table->setViewColumns($viewColumns);

		if ($column->getType() == Column::TAB_GROUP) {
			$tabs = array();
			foreach ($table->getColumns() as $column2) {
				if ($column2->getType() == Column::TAB) {
					if ($column2->getProperty('layoutParentId')
						== $column->getId()) {
						$tabs[] = $column2;
					}
				}
			}
			foreach ($tabs as $tab) {
				$table->removeColumn($tab);
			}
		}

		$column = $table->removeColumn($column);

		return $ctx->respondJSON(array('status'=>'success', 'result'=>$column->getId()));
	}

	/**
	 * @param ActionContext $ctx
	 * @param $t table id.
	 * @param $c column id.
	 */
	public function addOptionJson(ActionContext $ctx, $t = 0, $c = 0)
	{
		$table = $this->getTableForModifyForm($ctx, $t);
		$option = null;
		$column = $table->getColumnFor($c);
		$option = $column->addOption();
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$option));
	}

	/**
	 * @param ActionContext $ctx
	 * @param $t table id.
	 * @param $c column id.
	 * @param $o option id.
	 */
	public function removeOptionJson(ActionContext $ctx, $t=0, $c=0, $o=0)
	{
		$table = $this->getTableForModifyForm($ctx, $t);
		$column = $table->getColumnFor($c);
		$option = $column->getOptionFor($o, false, false);
		$option = $column->removeOption($option);
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$option));
	}

	/**
	 * @param ActionContext $ctx
	 * @param $t table id.
	 * @param $create
	 * @param $base 'template': create from template. 'table': create from table.
	 */
	public function modifyInfo(ActionContext $ctx, $t = 0, $create = false, $base = '')
	{
		$table = null;
		if ($create) {
			$table = $this->getTemplate($t, $base);
		} else {
			$table = $this->engine->getTable($t, Engine::TRUNK, $ctx->getUser());
		}

		return $ctx->render('my/table/table_modify_info.tpl', array('table' => $table, 'create' => $create, 'base' => $base));
	}


	private function getTemplate($t, $base)
	{
		if ($base == 'template') {
			$table = $this->engine->getTemplate($t);
		} else if ($base == 'table') {
			$table = $this->engine->getTable($t);
			$table = $this->engine->getTemplateFromTable($table);
		}
		return $table;
	}

	/**
	 * @param ActionContext $ctx
	 */
	public function iconsJson(ActionContext $ctx)
	{
		$icons = $this->engine->getIcons();
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$icons));
	}

	public function uploadIconJson(ActionContext $ctx) {
		$this->uploadFile($ctx->getUser(), array(
				'max_file_size' => 1 * 1024 * 1024,
				'accept_file_types' => '/\.(gif|jpe?g|png)$/i'
		));
	}

	/**
	 * @param ActionContext $ctx
	 * @param $t table id.
	 */
	public function loadInfoJson(ActionContext $ctx, $t = 0, $create = false, $base = '')
	{
		$table = null;
		if ($create) {
			$table = $this->getTemplate($t, $base);
		} else {
			$table = $this->engine->getTable($t, Engine::TRUNK, $ctx->getUser());
		}
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$table));
	}

	private function validateInfo($data) {
		$messages = array();

		$name = $data['name'];
		$iconType = $data['iconType'];
		$uploadkey = null;
		if ($data['file']) {
			$uploadkey = $data['file']['filekey'];
		}

		if (is_empty($name)) {
			$messages[] = new Message('データベース名を入力してください。');
		} else if (mb_strlen($name) > 64) {
			$messages[] = new Message('データベース名は64文字以内で入力してください。');
		}

		if ($iconType == 'upload' && is_empty($uploadkey) ) {
			$messages[] = new Message('アップロードファイルを選択してください。');
		}

		return $messages;
	}


	/**
	 * @param ActionContext $ctx
	 */
	public function saveInfoJson(ActionContext $ctx)
	{
		$json = $ctx->getParam('data');
		$data = json_decode($json, true);
		$errors = $this->validateInfo($data);
		if (is_not_empty($errors)) {
			return $ctx->respondJSON(array('status'=>'error', 'errors'=>$errors));
		}

		$t = $data['id'];
		$create = $data['create'];
		$base = isset($data['base']) ? $data['base'] : '';
		$table = null;
		if ($create) {
			$tableLimit = get_config('app', 'table_num_limit');
			$tableCount = $this->engine->countTables();
			if ($tableLimit > 0 && $tableCount >= $tableLimit) {
				$errors[] = new Message('データベース数上限により登録できません。');
				return $ctx->respondJSON(array('status'=>'error', 'errors'=>$errors));
			}
			// テンプレートへセット
			$template = $this->getTemplate($t, $base);
			$newIcon = $this->loadNewIcon($template->getIcon(), $data, $ctx->getUser());
			$template->setName($data['name']);
			if ($newIcon !== null) {
				$template->setIcon($newIcon);
			}
			$template->setPublic($data['isPublic']);
			$template->setDescription($data['description']);
			$table = $template->createTable($ctx->getUser());
			$ctx->setSession('flash_message', new Message('データベースが作成されました。'));
		} else {
			// ワークテーブルへセット
			$table = $this->engine->getTable($t, Engine::TRUNK, $ctx->getUser());
			$newIcon = $this->loadNewIcon($table->getIcon(), $data, $ctx->getUser());
			$table->setName($data['name']);
			if ($newIcon !== null) {
				$table->setIcon($newIcon);
			}
			$table->setPublic($data['isPublic']);
			$table->setDescription($data['description']);
			$table->saveChanges($ctx->getUser());
		}

		if (sizeof($errors) == 0) {
			return $ctx->respondJSON(array('status'=>'success', 'result'=>$table->getId()));
		} else {
			return $ctx->respondJSON(array('status'=>'error', 'errors'=>$errors));
		}
	}

	private function loadNewIcon($currentIcon, $data, $user)
	{
		$newIcon = null;
		$uploadkey = null;
		if ($data['file']) {
			$uploadkey = $data['file']['filekey'];
		}
		// プリセット選択
		if ($data['iconType'] === 'preset') {
			$selectedkey = $data['iconkey'];
			// ファイルキーが変わっている場合のみ適用
			if ($currentIcon->getFilekey() !== $selectedkey) {
				$newIcon = new Icon($selectedkey, true);
			}
			// アップロードファイルの場合
		} else if ($data['iconType'] === 'upload') {
			list($uploadIconFilePath, ) = $this->getTempFilePath('upload', $uploadkey);
			$newIcon = Icon::upload($uploadIconFilePath);
		}
		return $newIcon;
	}

	/**
	 * @param ActionContext $ctx
	 * @param $t table id.
	 */
	public function modifyConf(ActionContext $ctx, $t = 0)
	{
		$table = $this->engine->getTable($t);
		return $ctx->render('my/table/table_modify_conf.tpl', array('table' => $table));
	}

	/**
	 * @param ActionContext $ctx
	 * @param int $t table id.
	 * @return bool
	 * @throws TxCoreException
	 */
	public function modifyCalendar(ActionContext $ctx, $t = 0)
	{
		$table = $this->engine->getTable($t);
		return $ctx->render('my/table/table_modify_calendar.tpl', array('table' => $table));
	}

    /**
     * @param ActionContext $ctx
     * @param int $t table id.
     * @return bool
     * @throws TxCoreException
     */
	public function modifyNotification(ActionContext $ctx, $t = 0)
	{
		$table = $this->engine->getTable($t);
		return $ctx->render('my/table/table_modify_notification.tpl', array('table' => $table));
	}

    /**
     * @param ActionContext $ctx
     * @param int $t table id.
     * @return bool
     * @throws TxCoreException
     */
    public function modifyReadCheck(ActionContext $ctx, $t = 0)
    {
        $table = $this->engine->getTable($t);
        return $ctx->render('my/table/table_modify_read_check.tpl', array('table' => $table));
    }

    /**
     * @param ActionContext $ctx
     * @return bool
     */
	public function generateApiKeyJson(ActionContext $ctx)
	{
		return $ctx->respondJSON(array('status'=>'success', 'result'=>Util::generateApiKey()));
	}

    /**
     * @param ActionContext $ctx
     * @return bool
     */
	public function saveConfJson(ActionContext $ctx)
	{
		$json = $ctx->getParam('data');
		$data = json_decode($json, true);
		$errors = $this->validateConf($data);
		if (is_not_empty($errors)) {
			return $ctx->respondJSON(array('status'=>'error', 'errors'=>$errors));
		}

		$t = $data['id'];
		$workTable = $this->engine->getTable($t, Engine::TRUNK, $ctx->getUser());

		// ワークテーブルへセット
		$workTable->setRoundScale($data['roundScale']);
		$workTable->setRoundType($data['roundType']);
		$workTable->setApiEnabled($data['isApiEnabled']);
		$workTable->setApiKey($data['apiKey']);
		$workTable->setWriteApiEnabled($data['isWriteApiEnabled']);
		$workTable->setWriteApiKey($data['writeApiKey']);
		$workTable->setCommentEnabled($data['isCommentEnabled']);
		//$workTable->setCalendarEnabled($data['isCalendarEnabled']);
		$workTable->setSixColumnsEnabled($data['isSixColumnsEnabled']);

		$workTable->saveChanges($ctx->getUser());

		return $ctx->respondJSON(array('status'=>'success', 'result'=>$workTable->getId()));
	}

	/**
	 * @param ActionContext $ctx
	 * @param SaveCalendarForm $from
	 * @return bool
	 * @throws TxCoreException
	 */
	public function saveCalendarSettingsJson(ActionContext $ctx, SaveCalendarForm $from)
	{
		//log_debug(print_r($from, true));

		$errors = $from->validate();
		if (is_not_empty($errors)) {
			return $ctx->respondJSON(array('status'=>'error', 'errors'=>$errors));
		}

		$trunk = $this->engine->getTable($from->id, Engine::TRUNK, $ctx->getUser());

		// ワークテーブルへセット
		$settings = new CalendarSettings($trunk);
		$settings->setTitleColumnId($from->titleColumnId);
		$settings->setDateColumnId($from->dateColumnId);
		$settings->setEndTimeColumnId($from->endTimeColumnId);
		$settings->setStartTimeColumnId($from->startTimeColumnId);
		$settings->setUserColumnId($from->userColumnId);
		$trunk->setCalendarSettings($settings);
		$trunk->setCalendarEnabled($from->isCalendarEnabled);

		$trunk->saveChanges($ctx->getUser());

		return $ctx->respondJSON(array('status'=>'success', 'result'=>$trunk->getId()));
	}

    /**
     * @param ActionContext $ctx
     * @param SaveReadCheckForm $form
     * @return bool
     * @throws TxCoreException
     */
    public function saveReadCheckSettingsJson(ActionContext $ctx, SaveReadCheckForm $form)
    {
        $errors = $form->validate();
        if (is_not_empty($errors)) {
            return $ctx->respondJSON(array('status'=>'error', 'errors'=>$errors));
        }

        $trunk = $this->engine->getTable($form->id, Engine::TRUNK, $ctx->getUser());

        // ワークテーブルへセット
        $settings = new ReadCheckSettings($trunk);
        $settings->setAutoreadEnabled($form->isAutoreadEnabled);
        $settings->setOnlistEnabled($form->isOnlistEnabled);

        //var_dump($form->checkTargets);
        $settings->setCheckTargetsArray(json_decode($form->checkTargets));
        $trunk->setReadCheckSettings($settings);
        $trunk->setReadCheckEnabled($form->isReadCheckEnabled);

        $trunk->saveChanges($ctx->getUser());

        return $ctx->respondJSON(array('status'=>'success', 'result'=>$trunk->getId()));
    }

	public function saveNotificationSettingsJson(ActionContext $ctx, SaveNotificationSettingsForm $form)
	{
		$errors = $form->validate();
		if (is_not_empty($errors)) {
			return $ctx->respondJSON(array('status'=>'error', 'errors'=>$errors));
		}

		$trunk = $this->engine->getTable($form->id, Engine::TRUNK, $ctx->getUser());

		// オブジェクト生成
		$basicRules = array();
		$basicRuleErrors = array();
		$line = 0;
		foreach ($form->basicRules as $json) {
			$line++;
			$obj = Util::decodeJSON($json);
			$rule = new NotificationBasicRule();
			$rule->_setParent($trunk);
			$rule->setTargetingType($obj->targetingType);
			$rule->setTargetsArray($obj->targets);
			$rule->setOnRowInsert($obj->isOnRowInsert);
			$rule->setOnRowUpdate($obj->isOnRowUpdate);
			$rule->setOnCommentInsert($obj->isOnCommentInsert);
			$messages = $rule->validate();
			if (count($messages)) {
				$basicRuleErrors[] = array(
					'index' => $line-1, 'messages' => $messages);
				return $ctx->respondJSON(
					array('status'=>'error',
					'basicRuleErrors'=>$basicRuleErrors));
			}
			$basicRules[] = $rule;
		}

		$conditionalRules = array();
		$conditionalRuleErrors = array();
		$line = 0;
		foreach ($form->conditionalRules as $json) {
			$line++;
			$obj = Util::decodeJSON($json);
			$rule = new NotificationConditionalRule();
			$rule->_setParent($trunk);
			$rule->setTargetingType($obj->targetingType);
			$rule->setTargetsArray($obj->targets);
			$rule->setCriteria(
				NotificationCriteria::fromArray($obj->criteria, $trunk));
			$rule->setMessage($obj->message);
			$conditionalRules[] = $rule;
			$messages = $rule->validate();
			if (count($messages)) {
				$conditionalRuleErrors[] = array(
					'index' => $line-1, 'messages' => $messages);
				return $ctx->respondJSON(
					array('status'=>'error',
					'conditionalRuleErrors'=>$conditionalRuleErrors));
			}
		}

		$viewColumns = array();
		foreach ($form->viewColumns as $c) {
			$viewColumn = $trunk->getColumnFor($c, true);
			if ($viewColumn) {
				$viewColumns[] = $viewColumn;
			}
		}

		// ワークテーブルへセット
		$trunk->setNotificationBasicRules($basicRules);
		$trunk->setNotificationConditionalRules($conditionalRules);
		$trunk->setNotificationViewColumns($viewColumns);
		$trunk->setCommentFollowingEnabled($form->isCommentFollowingEnabled);
		$trunk->saveChanges($ctx->getUser());

		return $ctx->respondJSON(array('status'=>'success', 'result'=>$trunk->getId()));
	}

	/**
	 * @param ActionContext $ctx
	 * @return bool
	 * @throws TxCoreException
	 */
	public function loadNotificationSettingsJson(ActionContext $ctx, $t = 0)
	{
		$table = $this->engine->getTable($t, Engine::TRUNK, $ctx->getUser());
		$result = array(
			'isCommentFollowingEnabled' => $table->isCommentFollowingEnabled(),
			'basicRules' => $table->getNotificationBasicRules(),
			'conditionalRules' => $table->getNotificationConditionalRules(),
			'viewColumns' => $table->getNotificationViewColumns(),
		);
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$result));
	}


	private function validateConf($data) {
		$messages = array();

		$roundScale = $data['roundScale'];

		if ($roundScale === null || $roundScale === '') {
			$messages[] = new Message('小数部桁数を入力してください。');
		} else if (preg_match('/^[0-9]+$/', $roundScale) < 1) {
			$messages[] = new Message('小数部桁数は数字で入力してください。');
		} else if (intval($roundScale) < 0 || intval($roundScale) > 10) {
			$messages[] = new Message('小数部桁数は0～10で入力してください。');
		}

		$isApiEnabled = $data['isApiEnabled'];
		$isWriteApiEnabled = $data['isWriteApiEnabled'];
		$writeApiKey = $data['writeApiKey'];
		if ($isApiEnabled && $isWriteApiEnabled) {
			if ($writeApiKey === null || $writeApiKey === '') {
				$messages[] = new Message('書き込み用APIキーを入力してください。');
			}
		}


		return $messages;
	}

	/**
	 * @param ActionContext $ctx
	 * @param $t table id.
	 */
	public function modifyView(ActionContext $ctx, $t = 0)
	{
		$table = $this->engine->getTable($t, Engine::TRUNK, $ctx->getUser());
		return $ctx->render('my/table/table_modify_view.tpl', array('table' => $table));
	}

	/**
	 * @param ActionContext $ctx
	 * @param $t table id.
	 */
	public function viewJson(ActionContext $ctx, $t = 0)
	{
		$table = $this->engine->getTable($t, Engine::TRUNK, $ctx->getUser());
		$viewColumns = $table->getViewColumns();
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$viewColumns));
	}

	/**
	 * @param ActionContext $ctx
	 */
	public function saveViewJson(ActionContext $ctx)
	{
		$json = $ctx->getParam('data');
		$data = json_decode($json, true);
		$t = $data['id'];
		$columnIds = $data['viewColumnIds'];
		$table = $this->engine->getTable($t, Engine::TRUNK, $ctx->getUser());

		// ワークテーブルへセット
		$viewColumns = array();
		foreach ($columnIds as $c) {

			//$viewColumn = $table->getColumnFor($c, true);
            $column = $table->getColumnFor($c['id'], true);
			if ($column) {
                $viewColumn = new ViewColumn($table);
                $viewColumn->setColumn($column);
                $viewColumn->setWidth(array_key_exists('width', $c) ? $c['width'] : null);
				$viewColumns[] = $viewColumn;
			}
		}
		$table->setViewColumns($viewColumns);

		$table->saveChanges($ctx->getUser());

		return $ctx->respondJSON(array('status'=>'success', 'result'=>$table->getId()));
	}

	/**
	 * 作成ページ
	 * @param ActionContext $ctx
	 * @param $id table id.
	 */
	public function preview(ActionContext $ctx, $t = 0, $intermediary = false)
	{
		if ($intermediary === true) {
			return $ctx->render('my/table/table_modify_form_preview_intermediary.tpl');
		} else {
			$table = $this->getTableForModifyForm($ctx, $t);
			return $ctx->render('my/table/table_modify_form_preview.tpl', array('table' => $table));
		}
	}

	/**
	 * @param ActionContext $ctx
	 * @param $id table id.
	 */
	public function fieldsJson(ActionContext $ctx, $t = 0)
	{
		$table = $this->getTableForModifyForm($ctx, $t);
		$row = $table->addRow();
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$row));
	}

	/**
	 * 関連レコード一覧の検索（JSON)
	 * @param ActionContext $ctx
	 * @param $id table id.
	 * @see DataActions
	 */
	public function searchRelationalJson(ActionContext $ctx, $t = 0, $c = 0, $rowId = 0, $offset = 0)
	{
		$table = $this->getTableForModifyForm($ctx, $t);
		$column = $table->getColumnFor($c);
		$row = $table->addRow();

		try {
			$criteria = new Criteria();
			$criteria->setListColumns($column->getReferenceColumns());
			$criteria->equals($table->getRownum(), -1);	// 0件になる
			$list = $row->serachReferal($column, $ctx->getUser(), $criteria, $offset);
			return $ctx->respondJSON(
					array('status'=>'success', 'result'=> $list));
		} catch (TxCoreException $e) {
			// 権限エラー
			if ($e->getCode() === TxCoreException::PERMISSION_DENIED) {
				return $ctx->respondJSON(array('status'=>'error',
						'errors' => array(new Message(':data_create.reference.permission'))));
			} else {
				throw $e;
			}
		}
	}

	/**
	 * @param ActionContext $ctx
	 * @param $t table id.
	 */
	public function modifyPermission(ActionContext $ctx, $t = 0)
	{
		$table = $this->engine->getTable($t, Engine::TRUNK, $ctx->getUser());
		return $ctx->render('my/table/table_modify_permission.tpl', array('table' => $table));
	}

	/**
	 * テーブル内レコードに対するアクセス権限情報の取得（JSON)
	 * @param ActionContext $ctx
	 * @param $id table id.
	 * @param $q search keyword.
	 */
	public function permissionJson(ActionContext $ctx, $t = 0, $q = null, $offset = 0, $limit = 20)
	{
		// 権限取得
		$table = $this->engine->getTable($t, Engine::TRUNK, $ctx->getUser());
		$permissions = $table->getPermissions();

		// ユーザ検索
		$userList = $this->engine->searchUsersExceptFor(array(1, $table->getOwner()->getId()), $q, $offset, $limit);

		$rows = array_map(function(User $user) use ($permissions) {
			$isWritable = false;
			$isReadable = false;
			foreach($permissions as $permission){
				if($permission->getUserId() == $user->getId()){
					$isReadable = $permission->getIsReadable();
					$isWritable = $permission->getIsWritable();
					break;
				}
			}
			return array(
				'id' => $user->getId(),
				'role' => $user->getRole(),
				'screenName' => $user->getScreenName(),
				'profile' => $user->getProfile(),
				'isReadable' => $isReadable,
				'isWritable' => $isWritable,
			);
		}, $userList->getRows());

		return $ctx->respondJSON(array('status'=>'success', 'result'=> array(
			'totalRowCount' => $userList->getTotalRowCount(),
			'rowCount' => $userList->getRowCount(),
			'rows' => $rows)));
	}

	/**
	 * @param ActionContext $ctx
	 */
	public function savePermissionJson(ActionContext $ctx, $tableId = 0)
	{
		$json = $ctx->getParam('data');
		$data = json_decode($json, true);
		$state = json_decode($ctx->getParam('state'));

		$workTable = $this->engine->getTable($tableId, Engine::TRUNK, $ctx->getUser());
		$revision = $workTable->getRevision();

		// 管理者とテーブルオーナーを除くユーザリストを取得
		$userList = $this->engine->searchUsersExceptFor(array(1, $workTable->getOwner()->getId()), null, 0, PHP_INT_MAX);

		// ワークテーブルへセット
		$workTable->setPermissionControl($state);

		$permissions = array();
		foreach($userList->getRows() as $user){
			$isExist = false;

			foreach($data as $value){
				// リクエストデータに当該ユーザIDがある場合
				if($value['id'] == $user->getId()){
					$permission = new Permission();
					$permission->setTableId($tableId);
					$permission->setRevision($revision);
					$permission->setUserId($user->getId());
					$permission->setIsReadable($value['isReadable']);
					$permission->setIsWritable($value['isWritable']);
					$isExist = true;
					break;
				}
			}

			// リクエストデータに当該ユーザIDがない場合
			if(!$isExist){
				// ワークテーブルから取得
				$permission = $workTable->getPermissionForUserId($user->getId(), true);
				if(!$permission){
					// ワークテーブルに登録されていない場合
					$permission = new Permission();
					$permission->setTableId($tableId);
					$permission->setRevision($revision);
					$permission->setUserId($user->getId());
				}
			}

			$permissions[] = $permission;
		}

		$workTable->setPermissions($permissions);
		$workTable->saveChanges($ctx->getUser());

		return $ctx->respondJSON(array('status'=>'success', 'result'=>$workTable->getId()));
	}

	public function userOptionsJson(ActionContext $ctx)
	{
		$users = $this->engine->searchUsers(null, 0, PHP_INT_MAX);
		$result = array_map(function(User $user) {
			return array('id' => $user->getId(), 'name' => $user->getScreenName());
		}, $users->getRows());
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$result));
	}

	/**
	 * @param ActionContext $ctx
	 * @return bool
	 */
	public function groupOptionsJson(ActionContext $ctx)
	{
		$groups = $this->engine->searchGroups();
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$groups->getRows()));
	}

	/**
	 * @param ActionContext $ctx
	 * @param int $t
	 * @param int $c
	 * @return bool
	 * @throws \Exception
	 */
	public function confirmRemovedJson(ActionContext $ctx, $t=0, $c=0)
	{
		$table = $this->getTableForModifyForm($ctx, $t);
		$column = $table->getColumnFor($c);
		$release = $this->engine->getTable(
			$t, Engine::RELEASE, $ctx->getUser());
		$message = null;

		if ($column->getType() == Column::RELATIONAL_INPUT) {
			$releasedColumn = $release->getColumnFor($c, true);
			if ($releasedColumn) {
				$referenceTable = $releasedColumn->getReferenceTable();
				$message = "この" . $releasedColumn->getTypeName()
					. "フィールドを削除すると、"
					. "関連づくデータベースの全てのデータが削除されます。\n\n"
					. "削除対象のデータベース:\n"
					. "・" . $referenceTable->getName() . "\n\n"
					. "本当に削除しますか?";
			}
		}

		return $ctx->respondJSON(array('status' => 'success', 'result' => $message));
	}
}
