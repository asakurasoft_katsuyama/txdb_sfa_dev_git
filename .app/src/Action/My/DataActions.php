<?php
namespace Action\My;


use Barge\Web\Actions;
use Barge\Web\ActionContext;

use TxCore\CalendarView;
use TxCore\Comment;
use TxCore\Intent;
use TxCore\TxCoreException;
use TxCore\Filter;
use TxCore\Engine;
use TxCore\User;
use TxCore\File;
use TxCore\LookupValue;
use TxCore\Message;
use TxCore\Reference;
use TxCore\Option;
use TxCore\Column;
use TxCore\Table;
use TxCore\Row;
use TxCore\Criteria;
use TxCore\Index;

use Action\BaseActions;

//use Form\LoginForm;
use Form\Data\SubtotalForm;
use Form\Data\SaveFilterForm;
use Form\Data\DataImportForm;
use Form\Data\DataExportForm;
use Form\Data\DataCommentForm;
use Form\Data\RemoveFilterForm;
use Form\Data\SaveFilterNameForm;

use TxCore\Math\Math;

use TxCore\Util;
use Util\CSV;
use TxCore\Math\MathContext;
use TxCore\List_;

class DataActions extends BaseActions
{
    const DATA_VIEW_SETTINGS_FORMAT = 'data_view_settings__%d_%d';

    /**
	 * @param ActionContext $ctx
	 * @param int $t
	 * @param string $view
	 * @return bool
	 * @throws TxCoreException
	 */
	public function index(ActionContext $ctx, $t = 0, $view = null)
	{
		$rawCriteria = $ctx->getParam('criteria', true);
		$table = $this->engine->getTable($t);
		$limit = $this->getStoredLimit($ctx, $table);

		// テーブルのアクセス制限の状態取得
		$permissionControl = $table->getPermissionControl();

        $viewSettings = $this->getStoredCalendarCriteria($ctx, $table);

        if ($view !== null) {
            $viewSettings['viewType'] = $view;
            $this->storeCalendarCriteria($ctx, $table, $viewSettings);
        }

//        // View
//        $viewSettings = $this->getViewSettings($ctx, $t);
//        if ($view !== null) {
//            $viewSettings['type'] = $view;
//            $this->storeViewSettings($ctx, $table->getId(), $view, null);
//        }

        if ($viewSettings['viewType'] === 'calendar') {
            if (!$table->isCalendarEnabled()) {
                $viewSettings['viewType'] = 'list';
            }
        }

		$isReadable = true;
		$isWritable = true;


		// ログインユーザの権限取得
		if(!$ctx->getUser()->isAdmin() && ($table->getOwner() === null || $table->getOwner()->getId() != $ctx->getUser()->getId())
		&& $permissionControl){
			$permission = $table->getPermissionForUserId($ctx->getUser()->getId(), true);
			if(is_null($permission) || !$permission->getIsReadable()){
				$isReadable = false;
			}
			if(is_null($permission) || !$permission->getIsWritable()){
				$isWritable = false;
			}
		}

		return $ctx->render('my/data/data_index.tpl', array(
			'table' => $table,
			'limit'=>$limit,
			'isReadable'=>$isReadable,
			'isWritable'=>$isWritable,
			'view'=> $viewSettings['viewType']
            ));
	}

    /**
     * @param ActionContext $ctx
     * @param int $t
     * @param int $offset
     * @param int $limit
     * @param null $filter
     * @return bool
     * @throws TxCoreException
     */
	public function indexJson(ActionContext $ctx, $t = 0, $offset = 0, $limit = 20, $filter = null)
	{
		$table = $this->engine->getTable($t);
		$criteria = $this->getPreferredCriteria($ctx, $table, $filter);

		if (is_empty($table->getViewColumns())) {
			$criteria->setListColumns(array_values($this->getDefaultListableColumnsMap($table)));
		}

		$list = $table->search($criteria, $ctx->getUser(), $offset, $limit);

// 		if (is_empty($filter))
			$this->storeCriteria($ctx, $table, $criteria);
		$this->storeLimit($ctx, $table, array($offset, $limit, $filter));

		return $ctx->respondJSON(array('status'=>'success', 'result'=>$list, 'criteria'=>$criteria));
	}


    /**
     * @param ActionContext $ctx
     * @param int $t table id.
     * @return bool
     */
    public function viewColumnsJson(ActionContext $ctx, $t = 0)
    {
        // ここでしか使っていないのでColumnに持っていない
        $defaultWidth = array(
            Column::MULTITEXT => 300,
            Column::RICHTEXT => 300,
            Column::FILE => 150,
			'...' => 100
        );

        $table = $this->engine->getTable($t);
        $viewColumns = $table->getViewColumns();

        $options = array();
//        $widthSpecified = false;
        if (is_not_empty($viewColumns)) {
            foreach ($viewColumns  as $viewColumn) {
                $column = $viewColumn->getColumn();
                if ($column) {
//                    if ($viewColumn->getWidth() !== null) {
//                        $widthSpecified = true;
//                    }
                    $options[] = array(
                        'width' => $viewColumn->getWidth(),
                        'defaultWidth' => isset($defaultWidth[$viewColumn->getColumn()->getType()]) ?
                            $defaultWidth[$viewColumn->getColumn()->getType()] : $defaultWidth['...'],
                        'column' => array(
                            'id' => $viewColumn->getColumn()->getId()
                        )
                    );
                }
            }
        }

        return $ctx->respondJSON(array('status'=>'success', 'result'=>$options));
    }

    /**
     * @param ActionContext $ctx
     * @param int $t
     * @param int $offset
     * @param int $limit
     * @param string $startDate
     * @return bool
     * @throws TxCoreException
     */
    public function indexCalendarJson(ActionContext $ctx, $t = 0, $offset = 0, $limit = 10, $startDate = null)
    {
        $table = $this->engine->getTable($t);

        $userGroupsObj = $ctx->getParam('userGroups', true);
        $userGroups = $this->convertUserGroups($userGroupsObj);
        $calendar = CalendarView::search($table, $userGroups,
            $ctx->getUser(), $offset, $limit, $startDate);

        // Update
        $criteria = $this->getStoredCalendarCriteria($ctx, $table);
        $criteria['startDate'] = $startDate;
        $criteria['offset'] = $calendar->getUserOffset();
        $criteria['userGroups'] = $userGroupsObj;
        $this->storeCalendarCriteria($ctx, $table, $criteria);
        //$this->storeLimit($ctx, $table, array($offset, $limit, $filter));

        return $ctx->respondJSON(array('status'=>'success', 'result'=>$calendar));
    }

    /**
     * @param $obj
     * @return array
     */
    private function convertUserGroups($obj)
    {
        if ($obj === null || !is_array($obj)) {
            return array();
        }
        $entities = array();
        foreach ($obj as $entity) {
            if ($entity->type === 'user') {
                $user = Engine::factory()->getUser($entity->id, false, true);
                if ($user !== null) {
                    $entities[] = $user;
                }
            } else if ($entity->type === 'group') {
                $group = Engine::factory()->getGroup($entity->id, true);
                if ($group !== null) {
                    $entities[] = $group;
                }
            } else {
                assert(false);
            }
        }
        return $entities;
    }

	private function &getDefaultListableColumnsMap(Table $table)
	{
		$map = array();
		foreach ($table->getColumns() as $column) {	/*@var $column Column */
			if ($column->isListable() && $column->getLayoutParent() == null)
				$map[$column->getId()] = $column;
		}


		uasort($map,  function($a, $b) {    /** @var $a Column *//** @var $b Column */

			$aX = $a->hasProperty('positionX') ? $a->getPositionX() : null;
			$aY = $a->hasProperty('positionY') ? $a->getPositionY() : null;
			$bX = $b->hasProperty('positionX') ? $b->getPositionX() : null;
			$bY = $b->hasProperty('positionY') ? $b->getPositionY() : null;

			if ($aX === null) $aX = PHP_INT_MAX;
			if ($aY === null) $aY = PHP_INT_MAX;
			if ($bX === null) $bX = PHP_INT_MAX;
			if ($bY === null) $bY = PHP_INT_MAX;

			if ($aY == $bY) {
				if ($aX == $bX) {
					return $a->getId() - $b->getId();
				}
				return $aX - $bX;
			} else {
				return $aY - $bY;
			}
		});

		$map = array_slice($map, 0, 5);

		return $map;
	}


	public function subtotal(ActionContext $ctx, SubtotalForm $form)
	{
		$table = $this->engine->getTable($form->t);
		return $ctx->render('my/data/data_subtotal.tpl', array('table' => $table ));
	}

	public function subtotalJson(ActionContext $ctx, SubtotalForm $form)
	{
		$table = $this->engine->getTable($form->t);
		$criteria = $this->getPreferredCriteria($ctx, $table, null);
		$criteria->setSubtotal($table->getColumnFor($form->gf), $form->fn, $form->af ? $table->getColumnFor($form->af) : null);
		$list = $table->search($criteria, $ctx->getUser());

		// 選択肢が20より多い場合は、値の少さいものをまとめる
		$list2 = $this->editSubtotal($list);

// 		return $ctx->respondJSON(array('status'=>'success', 'result' => $list));
		return $ctx->respondJSON(array('status'=>'success',
				'result'=>array('list' => $list, 'editedList' => $list2) ));
	}

	private function editSubtotal(List_ $subtotal) {

		$resolution = 20; // 円グラフでの最大分割数

		$mc = new MathContext(2, MathContext::ROUNDHALF_UP);
		$columns = $subtotal->getColumns();
		$textColumn = $columns[0];
		$valueColumn = $columns[1];

		// 選択肢数が20以下の場合
		$this_ = $this;
		if ($subtotal->getTotalRowCount() <= $resolution) {
			$items = array_map(function ($row) use ($mc, $textColumn, $valueColumn) {   /** @var $row Row */
				return array('text'=>$row->getText($textColumn), 'value'=>Math::bcround($row->getValue($valueColumn), $mc));
			}, $subtotal->getRows());
			$items = array_filter($items, function ($item) use ($this_) {
				return $this_->_filterValue__PieChart($item['value']);
			});
			return $items;
		}

		// 選択肢が20を超える場合、小さい数字をまとめる
		$key = 0;
		$list = array_map(function ($row) use (&$key, $textColumn, $valueColumn) {   /** @var $row Row */
			return array('key' => $key++, 'text'=>$row->getText($textColumn), 'value'=>$row->getValue($valueColumn));
		}, $subtotal->getRows());

		// ワークで大きい順に並べる
		$workList = $list;
		usort($workList, function ($a, $b) {
			if ($a['value'] == null) $a['value'] = 0;
			if ($b['value'] == null) $b['value'] = 0;
			if (Math::bccompare($b['value'], $a['value']) == 0) {
				return $a['key'] - $b['key'];
			} else {
				return Math::bccompare($b['value'], $a['value']);
			}
		});

		$first = true;
		$lastValue = null;
		$sum = 0;
		$count = 0;
		for ($i=count($workList)-1; $i>=0; $i--) {
			if ($first || Math::bccompare($lastValue,$workList[$i]['value']) == 0
				|| count($list) > $resolution - 1) {
				// 20より小さくなるように、小さい数値、同じ数値をまとめる
				$sum = bcadd(Math::bcround($sum, $mc), Math::bcround($workList[$i]['value'], $mc));
				$count++;
				// まとめたものは最終的なリストから削除する
				$index = array_search($workList[$i], $list);
				array_splice($list, $index, 1);
			} else if (!$first && Math::bccompare($lastValue,$workList[$i]['value']) != 0
				&& count($list) <= $resolution) {
				break;
			}
			$first = false;
			$lastValue =$workList[$i]['value'];
		}

		$editedList = array_map(function($obj) use ($mc) {
			return array('text'=>$obj['text'], 'value'=>Math::bcround($obj['value'], $mc));
		}, $list);

		$editedList[] = array('text'=>"他($count)", 'value'=>Math::bcround($sum, $mc));

		$editedList = array_filter($editedList, function ($item) use ($this_) {
			return $this_->_filterValue__PieChart($item['value']);
		});

		return $editedList;

	}

	public function _filterValue__PieChart($value) {
		if (Math::bccompare($value, 0) < 0) {
			return false;
		}
		if (!Util::isValidNumericRange($value)) {
			return false;
		}
		return true;
	}

    /**
     * @param ActionContext $ctx
     * @param int $t
     * @return bool
     * @throws TxCoreException
     */
	public function criteriaJson(ActionContext $ctx, $t = 0)
	{
		$table = $this->engine->getTable($t);
		$criteria = $this->getStoredCriteria($ctx, $table);
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$criteria));
	}

    /**
     * @param ActionContext $ctx
     * @param int $t
     * @return bool
     * @throws \Exception
     */
    public function calendarCriteriaJson(ActionContext $ctx, $t = 0)
    {
        $table = $this->engine->getTable($t);

        $criteria = $this->getStoredCalendarCriteria($ctx, $table);

        //$settings = $this->getViewSettings($ctx, $table->getId());
//        // Merge (issetは念のため）.
//        $criteria['userGroups'] = isset($settings['userGroups']) ?
//            $settings['userGroups'] : array();

        return $ctx->respondJSON(array('status'=>'success', 'result'=>$criteria));
    }

    /**
	 * @param ActionContext $ctx
	 * @param int $tableId
	 * @param null $criteria
	 * @param bool $onlyValidation
	 * @return bool
	 * @throws TxCoreException
	 */
	public function saveCriteriaJson(ActionContext $ctx, $tableId = 0, $criteria = null, $onlyValidation = false)
	{
		$table = $this->engine->getTable($tableId);
		$rawCriteria = $ctx->getParam('criteria', true);
		$criteria = Criteria::fromArray($rawCriteria, $table);

		$messages = $criteria->validate();
		if (is_not_empty($messages)) {
			return $ctx->respondJSON(array('status'=>'error', 'errors'=>$messages));
		}

		if ($onlyValidation) {
			return $ctx->respondJSON(array('status'=>'success'));
		}

		$this->storeCriteria($ctx, $table, $criteria);
		return $ctx->respondJSON(array('status'=>'success'));
	}

    /**
     * @param ActionContext $ctx
     * @param int $tableId
     * @return bool
     * @throws TxCoreException
     */
	public function clearCriteriaJson(ActionContext $ctx, $tableId = 0)
	{
		$table = $this->engine->getTable($tableId);
		$criteria = $this->getDefaultCriteria($ctx, $table);
		$this->storeCriteria($ctx, $table, $criteria);
		return $ctx->respondJSON(array('status'=>'success'));
	}

    /**
     * @param ActionContext $ctx
     * @param int $tableId
     * @return bool
     * @throws TxCoreException
     */
	public function filtersJson(ActionContext $ctx, $tableId = 0)
	{
		$table = $this->engine->getTable($tableId);
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$table->getFilters()));
	}

	/**
	 * @param ActionContext $ctx
	 * @param SaveFilterNameForm $form
	 */
	public function saveFilterNameJson(ActionContext $ctx, SaveFilterNameForm $form)
	{
		$table = $this->engine->getTable($form->tableId);

		$messages = $form->validate();
		if (is_not_empty($messages)) {
			return $ctx->respondJSON(array('status'=>'error', 'errors'=>$messages));
		}

		$filter = $table->getFilterFor($form->filterId);
		$filter->setName($form->name);
		$filter->save($ctx->getUser());

		return $ctx->respondJSON(array('status'=>'success', 'result'=>$filter));
	}

	/**
	 * @param ActionContext $ctx
	 * @param SaveFilterNameForm $form
	 */
	public function removeFilterJson(ActionContext $ctx, RemoveFilterForm $form)
	{
		$table = $this->engine->getTable($form->tableId);

		$messages = $form->validate();
		if (is_not_empty($messages)) {
			return $ctx->respondJSON(array('status'=>'error', 'errors'=>$messages));
		}

		$filter = $table->getFilterFor($form->filterId);
		$filter->remove($ctx->getUser());

		return $ctx->respondJSON(array('status'=>'success'));
	}

    /**
     * @param ActionContext $ctx
     * @param SaveFilterForm $form
     * @return bool
     * @throws TxCoreException
     */
	public function saveFilterJson(ActionContext $ctx, SaveFilterForm $form)
	{
		$table = $this->engine->getTable($form->tableId);

		$messages = $form->validate();
		if (is_not_empty($messages)) {
			return $ctx->respondJSON(array('status'=>'error', 'errors'=>$messages));
		}

		$rawCriteria = $ctx->getParam('criteria', true);
		$criteria = Criteria::fromArray($rawCriteria, $table);
		$messages = $criteria->validate();
		if (is_not_empty($messages)) {
			return $ctx->respondJSON(array('status'=>'error', 'errors'=>$messages));
		}

		$filter = null;
		if ($form->filter) {
			$filter = $table->getFilterFor($form->filter);
			$filter->setCriteria($criteria);
			$filter->save($ctx->getUser());
		} else {
			$filter = new Filter();
			$filter->setName($form->name);
			$filter->setCriteria($criteria);
			$table->saveFilter($filter, $ctx->getUser());
		}

		return $ctx->respondJSON(array('status'=>'success', 'result'=>$filter));
	}

	/**
	 * @param ActionContext $ctx
	 * @param int $t
	 * @param int $r
	 * @param int $offset
	 * @param int $n Notification id
	 * @param int $isChild Whether render a child row or not.
	 * @return bool
	 */
	public function view(ActionContext $ctx, $t = 0, $r = 0, $offset = 0,
		$n = 0, $isChild = false)
	{
		$table = $this->engine->getTable($t);
		$row = $table->getRowFor($r, $ctx->getUser());

		// テーブルのアクセス制限の状態取得
		$permissionControl = $table->getPermissionControl();

		$isReadable = true;
		$isWritable = true;

		// ログインユーザの権限取得
		if(!$ctx->getUser()->isAdmin() && ($table->getOwner() === null || $table->getOwner()->getId() != $ctx->getUser()->getId())
		&& $permissionControl){
			$permission = $table->getPermissionForUserId($ctx->getUser()->getId(), true);
			if(is_null($permission) || !$permission->getIsReadable()){
				$isReadable = false;
			}
			if(is_null($permission) || !$permission->getIsWritable()){
				$isWritable = false;
			}
		}

        // 自動的に既読にする
        if ($table->isReadCheckEnabled() && !$table->isChild()) {
            $settings = $table->getReadCheckSettings();
            if ($settings->isAutoreadEnabled() &&
                $table->isReadCheckTarget($ctx->getUser()) &&
                !$row->hasRead($ctx->getUser())) {
                $row->makeRead($ctx->getUser(), null, true);
            }
        }

		if ($isChild) {
			return $ctx->render('my/data/data_view_child.tpl', array('table' => $table, 'row'=>$row, 'offset'=>$offset, 'isReadable'=>$isReadable, 'isWritable'=>$isWritable, 'notificationId'=>$n));
		} else {
			return $ctx->render('my/data/data_view.tpl', array('table' => $table, 'row'=>$row, 'offset'=>$offset, 'isReadable'=>$isReadable, 'isWritable'=>$isWritable, 'notificationId'=>$n));
		}
	}

	public function userOptionsJson(ActionContext $ctx)
	{
		$users = $this->engine->searchUsers(null, 0, PHP_INT_MAX);
		$result = array_map(function(User $user) {
			return array('id' => $user->getId(), 'name' => $user->getScreenName());
		}, $users->getRows());
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$result));
	}

	public function columnsJson(ActionContext $ctx, $tableId = 0, $filter = null)
	{
		$filters = $filter ? explode(',', $filter) : array();
		$table = $this->engine->getTable($tableId);
		$columns = array_filter($table->getColumns(), function(Column $column) use ($filters) {
			if (in_array('showsFields', $filters) && !$column->showsFields())
				return false;
			if (in_array('sortable', $filters) && !$column->isSortable())
				return false;
			if (in_array('inputable', $filters) && !$column->isInputable())
				return false;
			if (in_array('sumable', $filters) && !$column->isSumable())
				return false;
			return true;
		});
		$columns = array_values($columns);
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$columns));
	}

    /**
     * @param ActionContext $ctx
     * @param int $t
     * @param int $r
     * @param array $intent
     * @return bool
     * @throws TxCoreException
     */
	public function fieldsJson(ActionContext $ctx, $t = 0, $r = 0, $intent = array(), $recursive = false)
	{
        if (is_array($intent)) {
            $intent_ = new Intent();
            $intent_->setDate(isset($intent['date']) ? $intent['date'] : null);
            $intent_->setUser(isset($intent['user']) ? $intent['user'] : null);
            $this->engine->setIntent($intent_);
        }

		$table = $this->engine->getTable($t);
		$row = ($r === 0 ? $table->addRow() : $table->getRowFor($r,
			null, false, false, $recursive));
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$row));
	}

	/**
	 * コピー時（POSTデータの複製を作成する）
	 * @param ActionContext $ctx
	 * @param int $tableId Table id
	 * @return bool
	 */
	public function fieldsWithJson(ActionContext $ctx, $tableId)
	{
		$table = $this->engine->getTable($tableId);
		$row = $table->addRow();
		$data = $ctx->getParam('data', true);
		$this->setRowValues($ctx, $table, $row, $data);

		// ファイルは値をコピーできないのでクリア
		$this->clearFileColumnValue($table, $row);
		// 明細入力のデータを新規扱いにする
		$this->renewRelationalInput($table, $row);

		return $ctx->respondJSON(array('status'=>'success', 'result'=>$row));
	}

	/**
	 * 子データ編集時
	 * @param ActionContext $ctx
	 * @param int $tableId Table id
	 * @return bool
	 */
	public function childFieldsWithJson(ActionContext $ctx, $tableId, $rowId = 0)
	{
		$table = $this->engine->getTable($tableId);
		$row = ($rowId === 0 ? $table->addRow() : $table->getRowFor($rowId));
		$data = $ctx->getParam('data', true);
		$this->setRowValues($ctx, $table, $row, $data);

		if (!$rowId) {
			// ファイルは値をコピーできないのでクリア
			$this->clearFileColumnValue($table, $row);
		}

		return $ctx->respondJSON(array('status'=>'success', 'result'=>$row));
	}

	/**
	 * 行指定のコピー時（指定行の複製を作成する）
	 * @param ActionContext $ctx
	 * @param int $tableId Table id
	 * @param int $rowId Row id
	 * @return bool
	 */
	public function fieldsFromJson(ActionContext $ctx, $tableId = 0, $rowId = 0)
	{
		$table = $this->engine->getTable($tableId);
		$row = ($rowId === 0 ? $table->addRow() : $table->getRowFor($rowId,
			null, false, false, true));

		// ファイルは値をコピーできないのでクリア
		$this->clearFileColumnValue($table, $row);
		// 明細入力のデータを新規扱いにする
		$this->renewRelationalInput($table, $row);
		$row->_setId(null);
		$row->setRowNo(null);

		return $ctx->respondJSON(array('status'=>'success', 'result'=>$row));
	}

	/**
	 * @param ActionContext $ctx
	 * @param $tableId
	 * @param $rowId
	 * @return bool
	 */
	public function commentsJson(ActionContext $ctx, $tableId, $rowId)
	{
		$table = $this->engine->getTable($tableId);
		$row = $table->getRowFor($rowId);
		$commentList = $row->getComments();
		//$row = ($r === 0 ? $table->addRow() : $table->getRowFor($r));
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$commentList));
	}

	/**
	 * @param ActionContext $ctx
	 * @param DataCommentForm $form
	 * @return bool
	 * @throws TxCoreException
	 */
	public function createCommentJson(ActionContext $ctx, DataCommentForm $form)
	{
		$errors = $form->validate();
		if (is_not_empty($errors)) {
			return $ctx->respondJSON(array('status'=>'error', 'errors'=>$errors));
		}

		$table = $this->engine->getTable($form->tableId);
		$row = $table->getRowFor($form->rowId);
		$comment = new Comment();
		$comment->setText($form->text);
		$targets = $ctx->getParam('notificationTargets', true);
		foreach ($targets as $targetObject) {
			if ($targetObject->type == 'user') {
				$user = Engine::factory()->getUser($targetObject->id);
				$comment->addNotificationTarget($user);
			} else if ($targetObject->type == 'group') {
				$group = Engine::factory()->getGroup($targetObject->id);
				$comment->addNotificationTarget($group);
			}
		}

		$row->createComment($comment, $ctx->getUser());

		return $ctx->respondJSON(array('status'=>'success'));
	}

	/**
	 * @param ActionContext $ctx
	 * @param int $tableId
	 * @param int $rowId
	 * @param int $commentId
	 * @return bool
	 * @throws TxCoreException
	 */
	public function deleteCommentJson(ActionContext $ctx, $tableId = 0, $rowId = 0, $commentId = 0)
	{
		$table = $this->engine->getTable($tableId);
		$row = $table->getRowFor($rowId);
		$comment = $row->getCommentFor($commentId, true);
		if ($comment) {
			$row->deleteComment($comment, $ctx->getUser());
		}

		return $ctx->respondJSON(array('status'=>'success'));
	}


	/**
	 * @param ActionContext $ctx
	 * @param $t Table id to export.
	 */
	public function export(ActionContext $ctx, $t = 0)
	{
		$table = $this->engine->getTable($t);
		return $ctx->render('my/data/data_export.tpl', array('table' => $table));
	}

	public function import(ActionContext $ctx, $t = 0)
	{
		$table = $this->engine->getTable($t);
		return $ctx->render('my/data/data_import.tpl', array('table' => $table, 'fields' => CSV::getImportRequiredFields($table)));
	}

	public function importKeysJson(ActionContext $ctx, $t = 0)
	{
		$table = $this->engine->getTable($t);
		$keys = CSV::getKeyCandidates($table);
		return $ctx->respondJSON(array('status'=>'success', 'result' => $keys));
	}

	public function importJson(ActionContext $ctx, DataImportForm $form)
	{
		set_time_limit(0);
		$table = $this->engine->getTable($form->tableId);
		$keyColumn = $table->getColumnFor($form->keyColumnId);
		$getkeys = function($keystr) {
			return explode('_', substr($keystr, strpos($keystr, '=') + 1).'_');
		};

		$errors = $form->validate($ctx);
		if (is_not_empty($errors)) {
			$this->rollback();
			return $ctx->respondJSON(array('status'=>'error', 'errors' => $errors));
		}

		list($filePath, ) = $this->getTempFilePath('upload', $form->file);
		$csv = CSV::open($table, $filePath, false, $form->charset);
		$recnum = $csv->import($ctx->getUser(), $keyColumn);
		$csv->close();

		if (is_not_empty($csv->lastErrors())) {
			$this->rollback();
			return $ctx->respondJSON(array('status'=>'error', 'errors' => $csv->lastErrors()));
		}

		return $ctx->respondJSON(array('status'=>'success',
			'message' => new Message(':data_import.success', array($recnum))));
	}

	public function validateImportFields(CSV $csv)
	{
		$csv->readHeading();
	}

	public function offsetRowJson(ActionContext $ctx, $t = 0, $offset = 0)
	{
		$table = $this->engine->getTable($t);
		$criteria = $this->getStoredCriteria($ctx, $table);
		list($offset_, $limit) = $this->getStoredLimit($ctx, $table);
		$absOffset = $offset_ + $offset;
		if ($absOffset < 0) {
			return $ctx->respondJSON(array('status'=>'success', 'result' => null));
		}
		$list = $table->search($criteria, $ctx->getUser(), $absOffset, 1);
		return $ctx->respondJSON(array('status'=>'success', 'result' => ($list->getRowCount() == 0 ? null : $list[0]->getId())));
	}

	public function exportFormatJson(ActionContext $ctx, $tableId = 0, $charset = null)
	{
		$table = $this->engine->getTable($tableId);
		list ($filePath) = $this->createTempFile('upload', 'csv');

		$csv = CSV::open($table, $filePath, true, $charset);
		$csv->writeHeadingForImport();
		$csv->close();

		$fileName = $table->getName() . '_import_'.date('Ymd').'.csv';

		return $ctx->respondJSON(array('status'=>'success',
				'result' => array ('fileKey' => basename($filePath), 'fileName' => $fileName)
				));
	}

	public function downloadFormatCsv(ActionContext $ctx, $tableId, $fileKey)
	{
		list($filePath, ) = $this->getTempFilePath('upload', $fileKey);
		$ctx->setHeader('Content-Disposition', 'attachment');
		return $ctx->respondFile($filePath, 'text/csv');

	}




	public function exportJson(ActionContext $ctx, DataExportForm $form)
	{
		set_time_limit(0);
		$this_ = $this;
		$table = $this->engine->getTable($form->tableId);
		$criteria = $this->getStoredCriteria($ctx, $table);

		list ($filePath, ) = $this->createTempFile('upload', 'csv');

		$csv = CSV::open($table, $filePath, true, $form->charset);
		$writer = $csv->export($form->heading, $form->importing);
		$table->fetch($criteria, $writer);
		$csv->close();

		$fileName = $table->getName() . '_'.date('Ymd').'.csv';

		return $ctx->respondJSON(array('status'=>'success',
				'result' => array ('fileKey' => basename($filePath), 'fileName' => $fileName),
				'message' => new Message(':data_export.success', array())));
	}

	public function downloadCsv(ActionContext $ctx, $tableId, $fileKey)
	{
		list($filePath, ) = $this->getTempFilePath('upload', $fileKey);
		$ctx->setHeader('Content-Disposition', 'attachment');
		return $ctx->respondFile($filePath, 'text/csv');
	}

	/**
	 * 作成ページ
	 * @param ActionContext $ctx
	 * @param int $t
	 * @return bool
	 * @throws \Exception
	 */
	public function create(ActionContext $ctx, $t = 0)
	{
		$table = $this->engine->getTable($t);

		// 権限チェック
		if(!$table->allows('insert', $ctx->getUser())){
			throw new TxCoreException(
				sprintf('User %d has no authentication for %s table %d (owner:%s).',
					$ctx->getUser()->getId(),
					'insert',
					$table->getId(),
					($table->getOwner() ? $table->getOwner()->getId() : null)),
				TxCoreException::PERMISSION_DENIED);
		}

		return $ctx->render('my/data/data_create.tpl', array('table' => $table));
	}

	/**
	 * 編集ページ
	 * @param ActionContext $ctx
	 * @param $id table id.
	 */
	public function edit(ActionContext $ctx, $t = 0, $r = 0, $offset = 0)
	{
		$table = $this->engine->getTable($t);
		// 権限チェック
		if(!$table->allows('update', $ctx->getUser())){
			throw new TxCoreException(
				sprintf('User %d has no authentication for %s table %d (owner:%s).',
					$ctx->getUser()->getId(),
					'update',
					$table->getId(),
					($table->getOwner() ? $table->getOwner()->getId() : null)),
				TxCoreException::PERMISSION_DENIED);
		}

		$row = $table->getRowFor($r);
		return $ctx->render('my/data/data_create.tpl', array('table' => $table, 'row'=>$row, 'offset'=>$offset));	// テンプレートは同じ
	}

	/**
	 * 複製ページ
	 * @param ActionContext $ctx
	 * @param $id table id.
	 */
	public function replicate(ActionContext $ctx, $t = 0, $r = 0)
	{
		$table = $this->engine->getTable($t);

		// 権限チェック
		if(!$table->allows('insert', $ctx->getUser())){
			throw new TxCoreException(
				sprintf('User %d has no authentication for %s table %d (owner:%s).',
					$ctx->getUser()->getId(),
					'insert',
					$table->getId(),
					($table->getOwner() ? $table->getOwner()->getId() : null)),
				TxCoreException::PERMISSION_DENIED);
		}

		$row = $table->getRowFor($r);
		return $ctx->render('my/data/data_create.tpl',
			array('table' => $table, 'row'=>$row, 'replication'=>true));	// テンプレートは同じ
	}

	/**
	 * ファイルのアップロード (JSON)
	 * @param ActionContext $ctx
	 */
	public function uploadFileJson(ActionContext $ctx)
	{
		$this->uploadFile($ctx->getUser(), array(
			'max_file_size' => get_config('app','data_file_size_limit')
		));
	}

	/**
	 * インポートファイルのアップロード
	 * @param ActionContext $ctx
	 */
	public function uploadImportFileJson(ActionContext $ctx)
	{
		$this->uploadFile($ctx->getUser(), array(
			'max_file_size' => 100 * 1024 * 1024,
			'accept_file_types' => '/\.(csv)$/i'
		));
	}

	/**
	 * 関連レコード一覧の検索（JSON)
	 * @param ActionContext $ctx
	 * @param $id table id.
	 */
	public function searchRelationalJson(ActionContext $ctx, $t = 0, $c = 0, $rowId = 0, $offset = 0)
	{
		$table = $this->engine->getTable($t);
		$column = $table->getColumnFor($c);
		if ($rowId !== 0) {	// 表示のみ
			$row = $table->getRowFor($rowId);
			$this->setRowValues($ctx, $table, $row, $ctx->getParam('data', true));
		} else {
			$row = $table->addRow();
			$this->setRowValues($ctx, $table, $row, $ctx->getParam('data', true));
		}

		// 参照先テーブル　取得
		$targetTable = $column->getReferenceTable();

		try {
			$value = $row->getValue($column->getReferenceSourceColumn());
			if (is_empty($value)) {
				// 0件データを返す
				$criteria = new Criteria();
				$criteria->setListColumns($column->getReferenceColumns());
				$criteria->equals($table->getRownum(), -1);
				$list = $row->serachReferal($column, $ctx->getUser(), $criteria, $offset);

				// 集計処理 0件データを返す
				$targetCriteria = new Criteria();
				$targetCriteria->setListColumns($column->getReferenceColumns());
				$targetCriteria->setAggregate(null);
				$aggregateList = $targetTable->search($targetCriteria, $ctx->getUser());

				return $ctx->respondJSON(
						array('status'=>'success', 'result'=> $list, 'aggregate_result'=> $aggregateList));
			}
			$list = $row->serachReferal($column, $ctx->getUser(), null, $offset);

			// 集計処理用
			$targetCriteria = new Criteria();
			$targetCriteria->setListColumns($column->getReferenceColumns());
			$targetCriteria->equals($column->getReferenceTargetColumn(), $row->getValue($column->getReferenceSourceColumn()));

			if($list->getTotalRowCount() > 0){
				$targetCriteria->setAggregate($column->getReferenceAggregations());
			}else{
				$targetCriteria->setAggregate(null);
			}
			$aggregateList = $targetTable->search($targetCriteria, $ctx->getUser());

			return $ctx->respondJSON(
				array('status'=>'success', 'result'=>$list, 'aggregate_result'=>$aggregateList));
		} catch (TxCoreException $e) {
			// 権限エラー
			if ($e->getCode() === TxCoreException::PERMISSION_DENIED) {
				return $ctx->respondJSON(array('status'=>'error',
					'errors' => array(new Message(':data_create.reference.permission'))));
			} else if ($e->getCode() === TxCoreException::VALIDATION_FAILED) {
				// 入力エラー時は「レコードがありません。」を出す（数値のカラム）
				$criteria = new Criteria();
				$criteria->setListColumns($column->getReferenceColumns());
				$criteria->equals($table->getRownum(), -1);	// 0件になる
				$list = $row->serachReferal($column, $ctx->getUser(), $criteria, $offset);
				
				// 集計処理 0件データを返す
				$targetCriteria = new Criteria();
				$targetCriteria->setListColumns($column->getReferenceColumns());
				$targetCriteria->setAggregate(null);
				$aggregateList = $targetTable->search($targetCriteria, $ctx->getUser());
				return $ctx->respondJSON(
					array('status'=>'success', 'result'=> $list, 'aggregate_result'=>$aggregateList));
			} else {
				throw $e;
			}
		}
	}

	/**
	 * Lookupの検索（JSON)
	 * @param ActionContext $ctx
	 * @param $id table id.
	 */
	public function searchLookupJson(ActionContext $ctx, $t = 0, $c = 0, $r = 0)
	{
		$table = $this->engine->getTable($t);
		$column = $table->getColumnFor($c);
		if ($column->getType() != Column::LOOKUP) {
			throw new \Exception('Invalid column.');
		}

		$row = ($r === 0 ? $table->addRow() : $table->getRowFor($r));
		$this->setRowValues($ctx, $table, $row, $ctx->getParam('data', true));

		try {
			$list = $row->serachReferal($column, $ctx->getUser());
			return $ctx->respondJSON(array('status'=>'success', 'result' => $list));
		} catch (TxCoreException $e) {
			// 権限エラー
			if ($e->getCode() === TxCoreException::PERMISSION_DENIED) {
				return $ctx->respondJSON(array('status'=>'error',
					'errors' => array(new Message(':data_create.reference.permission'))));
			} else if ($e->getCode() === TxCoreException::VALIDATION_FAILED) {
				return $ctx->respondJSON(array('status'=>'error',
					'errors' => $e->getContext()));
			} else {
				throw $e;
			}
		}
	}

	/**
	 * @param ActionContext $ctx
	 * @param int $t
	 * @param int $c
	 * @param int $offset
	 */
	public function searchLookupByJson(ActionContext $ctx, $t = 0, $c = 0, $offset = 0)
	{
		$table = $this->engine->getTable($t);
		$column = $table->getColumnFor($c);
		if ($column->getType() != Column::LOOKUP) {
			throw new \Exception('Invalid column.');
		}

		$row = $table->addRow();
		$criteria = Criteria::fromArray($ctx->getParam('criteria', true), $column->getReferenceTable());
		try {
			$list = $row->serachReferal($column, $ctx->getUser(), $criteria, $offset);
			return $ctx->respondJSON(array('status'=>'success', 'result' => $list));
		} catch (TxCoreException $e) {
			// 権限エラー
			if ($e->getCode() === TxCoreException::PERMISSION_DENIED) {
				return $ctx->respondJSON(array('status'=>'error',
					'errors' => array(new Message(':data_create.reference.permission'))));
			} else {
				throw $e;
			}
		}
	}

    /**
     * レコードの削除（JSON)
     * @param ActionContext $ctx
     * @param int $tableId
     * @param string $token
     * @return bool
     */
	public function truncateJson(ActionContext $ctx, $tableId = 0, $token = null)
	{
		set_time_limit(0);
		$this_ = $this;
		$table = $this->engine->getTable($tableId);
		// ロック
		$table->lock();
		$criteria = $this->getStoredCriteria($ctx, $table);

		$table->fetch($criteria, function($row, $index) use ($ctx, $token) {    /** @var $row Row */
			if ($index == get_config('app','data_truncate_num_limit'))
				return false;
			$row->delete($ctx->getUser());
		});

		$this->engine->scheduleOptimization();

		return $ctx->respondJSON(array('status' => 'success', 'message'=>'削除処理が完了しました。'));
	}

	/**
	 * Lookupのコピーデータを返す
	 * @param ActionContext $ctx
	 * @param int $t
	 * @param int $c
	 * @param int $r 選択されたrow
	 */
	public function getLookupRowJson(ActionContext $ctx, $t = 0, $c = 0, $r = 0)
	{
		$table = $this->engine->getTable($t);
		$column = $table->getColumnFor($c);
		$row = $column->getReferenceTable()->getRowFor($r);

		$result = array(
			'id' => $row->getId(),
			'value' => $row->getValue($column->getReferenceTargetColumn()),
			'copyValues' => array()
			);
		$result['copyValues'] = $this->getLookupCopyValues($column, $row);
		return $ctx->respondJSON(array('status'=>'success', 'result'=> $result));
	}

	private function getLookupCopyValues(Column $column, Row $row)
	{
		$copyValues = array();
		foreach ($column->getReferenceCopyMap() as $pair) {
			list ($form, $to) = $pair;  /** @var $form Column *//** @var $to Column */
			$convertedValue = null;
			$fromValue = $row->getValue($form);
			if ($form->hasOptions() &&
				$to->hasOptions()) {
				if ($fromValue !== null) {
					// 名前が一致するものをセット
					$matched = $to->getOptions(array('name'=>$fromValue->getName()));
					if (is_not_empty($matched)) {
						$convertedValue = $matched[0];
					}
				}
			} else if ($form->hasOptions() &&
				!$to->hasOptions()) {
				if ($fromValue !== null) {
					$convertedValue = $fromValue->getName();
				}
			} else {
				$convertedValue = $fromValue;
			}
			$copyValues[] = array('id'=>$to->getId(), 'value' => $convertedValue);
		}
		return $copyValues;
	}

    /**
     * 行の削除（JSON)
     * @param ActionContext $ctx
     * @param int $t table id.
     * @param int $r table id.
     * @return bool
     */
	public function removeJson(ActionContext $ctx, $t = 0, $r = 0)
	{
		$table = $this->engine->getTable($t);
		// ロック
		$table->lock();
		$filter = array('type' => Column::RELATIONAL_INPUT);
		foreach ($table->getColumns($filter) as $column) {
			$referenceTable = $column->getReferenceTable();
			$referenceTable->lock();
		}
		$row = $table->getRowFor($r);
		$row->delete($ctx->getUser());

// 		// 抽出条件をクリア
// 		$this->storeCriteria($ctx, $table, null);
// 		$this->storeLimit($ctx, $table, null);

		return $ctx->respondJSON(array('status'=>'success'));
	}

	/**
	 * 再計算（JSON）
	 * @param ActionContext $ctx
	 * @param int $tableId
	 * @param int $rowId
	 * @return bool
	 * @throws \Exception
	 */
	public function calculateJson(ActionContext $ctx, $tableId = 0, $rowId = 0)
	{
		$table = $this->engine->getTable($tableId);
		$row = $rowId === 0 ? $table->addRow() : $table->getRowFor($rowId);
		$this->setRowValues($ctx, $table, $row, $ctx->getParam('data', true));

		try {
			$row->calculate($table);
		} catch (TxCoreException $e) {
			log_debug($e->getMessage());
		}

		$result = array();
		foreach ($table->getColumns() as $column) {	/* @var $column \TxCore\Column */
			if ($column->isCalculable()) {
				$result[] = array(
					'id'=>$column->getId(),
					'value'=>$row->getValue($column),
					'text'=>$row->getText($column)
				);
			}
		}
		return $ctx->respondJSON(
			array('status'=>'success', 'result'=>$result));
	}

	/**
	 * @param ActionContext $ctx
	 * @param $tableId
	 * @param int $rowId
	 * @return bool
	 */
	public function copyFromJson(ActionContext $ctx, $tableId, $rowId)
	{
//		$table = $this->engine->getTable($tableId);
//		$row = $rowId === 0 ? $table->addRow() : $table->getRowFor($rowId);
//		$this->setRowValues($ctx, $table, $row, $ctx->getParam('data', true));
//
//		try {
//			$row->calculate($table);
//		} catch (TxCoreException $e) {
//			log_debug($e->getMessage());
//		}
//
//		$result = array();
//		foreach ($table->getColumns() as $column) {	/* @var $column \TxCore\Column */
//			if ($column->isCalculable()) {
//				$result[] = array(
//					'id'=>$column->getId(),
//					'value'=>$row->getValue($column),
//					'text'=>$row->getText($column)
//				);
//			}
//		}
//		return $ctx->respondJSON(
//			array('status'=>'success', 'result'=>$result));
	}

    /**
     * データの登録・更新（JSON）
     * @param ActionContext $ctx
     * @param int $tableId
     * @return bool
     * @throws TxCoreException
     */
	public function updateJson(ActionContext $ctx, $tableId = 0)
	{
		$table = $this->engine->getTable($tableId);
		$data = $ctx->getParam('data', true);
		/**@var $data array */

		// ロック
		$table->lock();
		$filter = array('type' => Column::RELATIONAL_INPUT);
		foreach ($table->getColumns($filter) as $column) {
			$referenceTable = $column->getReferenceTable();
			$referenceTable->lock();
		}

		$rows = array();
		$allErrors = array();
		$hasError = false;
		$uniqueColumns = array();   /** @var Column[] $uniqueColumns  */
		$uniqueColumnValues = array();
		foreach ($table->getColumns() as $c) {
			if ($c->hasProperty('isUnique') && $c->getProperty('isUnique')) {
				$uniqueColumns[$c->getId()] = $c;
				$uniqueColumnValues[$c->getId()] = array();
			}
		}
		foreach ($data as $index => $entryData) {
			$row = isset($entryData->rowId) ? $table->getRowFor($entryData->rowId) : $table->addRow();	/** @var Row $row  */
			$this->setRowValues($ctx, $table, $row, $entryData->values);
			$errors = $row->validate();
			if (is_empty($errors)) {
				foreach ($uniqueColumns as $c) {
					$value = $row->getValue($c);
					if (is_empty($value)) continue;
					if (in_array($value, $uniqueColumnValues[$c->getId()])) {
						$errors[] = new Message(":data_create.duplicated",
							array($c->getLabel()), $c);
					}
					$uniqueColumnValues[$c->getId()][] = $value;
				}
			}
			$allErrors = array_merge($allErrors, $errors);
			foreach ($errors as $error) {
				$originalContext = $error->getContext();
				$error->setContext(array('index' => $index,
					'columnId' => $originalContext instanceof Column
						? $originalContext->getId() : null));
			}

			if (is_not_empty($errors)) {
				$hasError = true;
			}
			$row->setVersion($entryData->version);
			$rows[] = $row;
		}

		if ($hasError) {
			$this->rollback();
			return $ctx->respondJSON(array('status' => 'error', 'errors' => $allErrors));
		}

		foreach ($rows as $index => $row) {
			if ($row->getId() === null) {
				$table->insertRows($ctx->getUser(), true);
				// 抽出条件をクリア
				$this->storeCriteria($ctx, $table, null);
				$this->storeLimit($ctx, $table, null);
				break;
			} else {
				try {
					$row->update($ctx->getUser(), true);
				} catch (TxCoreException $e) {
					if ($e->getCode() === TxCoreException::PERMISSION_DENIED) {
						return $ctx->respondJSON(array('status' => 'error', 'errors' =>
							array(new Message(':global.permission_error'), array(), array('index' => $index))
						));
					}
					$row = $e->getContext();
					$this->rollback();
					return $ctx->respondJSON(array(
						'status' => 'error',
						'errors' => array(
							new Message(':data_create.row_has_updated',
								array($row->getUpdatedOn(), $row->getCreatedBy() ? $row->getCreatedBy()->getScreenName() : ''),
								array('index' => $index))
						)
					));
				}
			}
		}

		return $ctx->respondJSON(array('status' => 'success', 'result' =>
			array_map(function(Row $row) { return $row->getId(); }, $rows)));
	}


    /**
     * @param ActionContext $ctx
     * @param Table $table
     * @param Row $row
     * @param object $postData
     * @throws TxCoreException
     */
	private function setRowValues(ActionContext $ctx, Table $table, Row $row, $postData)
	{
		if ($postData === null)
			return;

		foreach ($postData as $data) {
			$column = $table->getColumnFor($data->id);	/* @var $column \TxCore\Column */
			if ($column->isAutofill())
				continue;
			switch ($column->getType()) {
				case Column::CHECKBOX:
					$options = array();
					foreach (is_empty($data->value) ? array() : $data->value as $optionId) {
						$option = $column->getOptionFor($optionId, true);
						if ($option !== null) {	// Note : 削除された場合は存在しない
							$options[] = $option;
						}
					}
					$row->setValue($column, $options);
					break;
				case Column::RADIO:
				case Column::DROPDOWN:
					if (is_empty($data->value)) {
						$row->setValue($column, null);
					} else {
						$row->setValue($column, $column->getOptionFor($data->value, true));
					}
					break;
				case Column::LOOKUP:
					$lookup = new LookupValue();
					$lookup->setKey($data->key);
					$lookup->setValue($data->value);
					$row->setValue($column, $lookup);
					break;
				case Column::FILE:
					$files = array();
					foreach ($data->value as $value) {
						if ($value->isSaved) {
							$hits = array_grep($row->getValue($column), function($file) use ($value) {  /** @var $file File */
								return $file->getFilekey() == $value->filekey;
							});
							$files[] = $hits[0];
						} else {
							list($filePath, ) = $this->getTempFilePath('upload', $value->filekey);
							$files[] = File::create($filePath, $value->fileName, $value->contentType);
						}
					}
					$row->setValue($column, $files);
					break;
				case Column::USER_SELECTOR:
					$users = array();
					foreach (is_empty($data->value) ? array() : $data->value as $userId) {
						$user = $this->engine->getUser($userId, false, true);
						if ($user !== null) {
							$users[] = $user;
						}
					}
					$row->setValue($column, $users);
					break;
				case Column::RELATIONAL_INPUT:
					$_rows = array();
					$_referenceTable = $column->getReferenceTable();
					foreach ($data->value as $_data) {
						$action = $_data->action; // insert, update, delete
						$_row = isset($_data->rowId) ?
							$_referenceTable->getRowFor($_data->rowId)
							: $_referenceTable->addRow();	/** @var Row $_row  */
						$_row->setAction($action);
						if ($action !== 'delete') {
							$this->setRowValues($ctx, $_referenceTable,
							$_row, $_data->values);
						}
						$_rows[] = $_row;
					}
					$row->setValue($column, $_rows);
					break;
				default:
					$row->setValue($column, trim_to_null($data->value));
					break;
			}
		}
	}

    /**
     * @param ActionContext $ctx
     * @param Table $table
     * @param array $limit
     */
	private function storeLimit(ActionContext $ctx, Table $table, array $limit = null)
	{
		$searchStatus = $ctx->getSession('search_limit');
		if (!$searchStatus)
			$searchStatus = array();
		$searchStatus[$table->getId()] = $limit;
		$ctx->setSession('search_limit', $searchStatus);
	}

    /**
     * @param ActionContext $ctx
     * @param Table $table
     * @param Criteria $criteria
     */
	private function storeCriteria(ActionContext $ctx, Table $table, Criteria $criteria = null)
	{
		$searchStatus = $ctx->getSession('search');
		if (!$searchStatus)
			$searchStatus = array();
		$searchStatus[$table->getId()] = $criteria;
		$ctx->setSession('search', $searchStatus);
	}

    /**
     * @param ActionContext $ctx
     * @param Table $table
     * @return Criteria
     */
	private function getStoredLimit(ActionContext $ctx, Table $table)
	{
		$searchStatus = $ctx->getSession('search_limit');
		if (!$searchStatus)
			$searchStatus = array();
		if (isset($searchStatus[$table->getId()]))
			return $searchStatus[$table->getId()];
		$limit = get_config('app', 'data_index_limit_default', 20);
		return array(0, $limit, -1); // -1: all
	}

    /**
     * @param ActionContext $ctx
     * @param Table $table
     * @return Criteria
     */
	private function getStoredCriteria(ActionContext $ctx, Table $table)
	{
		$searchStatus = $ctx->getSession('search');
		if (!$searchStatus)
			$searchStatus = array();
		if (isset($searchStatus[$table->getId()])) {
			return clone $searchStatus[$table->getId()];
		}

		// なければデフォルトの抽出条件
		return $this->getDefaultCriteria($ctx, $table);	// Returns dummy.
	}

    /**
     * @param ActionContext $ctx
     * @param Table $table
     * @return array
     */
    private function getStoredCalendarCriteria(ActionContext $ctx, Table $table)
    {
        $searchStatus = $ctx->getSession('calendar');
        if (!$searchStatus)
            $searchStatus = array();
        if (isset($searchStatus[$table->getId()])) {
            return $searchStatus[$table->getId()];
        }

        $criteria = array(
            'viewType' => 'list',
            'userGroups' => array(
                array(
                    'type' => 'user',
                    'id' => $ctx->getUser()->getId()
                )
            ),
            'startDate' => null,
            'offset' => 0);


        // なければクッキーから復元
        $cookieSettings = $this->getViewSettings($ctx, $table->getId());
        if ($cookieSettings !== false) {
            $criteria['viewType'] = $cookieSettings['viewType'];
            if ($cookieSettings['userGroups'] !== null) {
                $criteria['userGroups'] = $cookieSettings['userGroups'];
            }
        }

        // 一度セッションに格納
        $this->storeCalendarCriteria($ctx, $table, $criteria);

        return $criteria;

//        $userObj = array(
//            'type' => 'user',
//            'id' => $ctx->getUser()->getId()
//        );
//        return array('userGroups' => array($userObj),
//            'startDate' => null,
//            'offset' => 0);
    }


    /**
     * @param ActionContext $ctx
     * @param Table $table
     * @param array $criteria
     */
    private function storeCalendarCriteria(ActionContext $ctx, Table $table, array $criteria)
    {
        $userId = $ctx->getUser()->getId();
        $searchStatus = $ctx->getSession('calendar');
        if (!$searchStatus)
            $searchStatus = array();

        $searchStatus[$table->getId()] = $criteria;
        $ctx->setSession('calendar', $searchStatus);

        // Cookie.
        $key = sprintf(self::DATA_VIEW_SETTINGS_FORMAT,
            intval($userId), intval($table->getId()));

        $cookieData = array();
        $cookieData['viewType'] = $criteria['viewType'];
        if (sizeof($criteria['userGroups']) > 20) {
            $cookieData['userGroups'] = null;
        } else {
            $cookieData['userGroups'] = $criteria['userGroups'];
        }

        $ctx->setCookie($key, json_encode($cookieData),
            strtotime('+30 days'));

    }


    /**
     * @param ActionContext $ctx
     * @param Table $table
     * @return Criteria
     */
	private function getDefaultCriteria(ActionContext $ctx, Table $table)
	{
		$criteria = new Criteria();
		$criteria->addSort($table->getRownum(), true);
		return $criteria;
	}

    /**
     * @param ActionContext $ctx
     * @param Table $table
     * @return Criteria
     */
    private function getDefaultCalendarCriteria(ActionContext $ctx, Table $table)
    {
        $criteria = new Criteria();
        $criteria->addSort($table->getRownum(), true);
        return $criteria;
    }

	/**
	 * @param ActionContext $ctx
	 * @param Table $table
	 * @param string $filter
	 * @return Criteria
	 */
	private function getPreferredCriteria(ActionContext $ctx, Table $table, $filter)
	{
		$criteria = null;
		if ($filter) {
			$filter = $table->getFilterFor($filter, true);
			if ($filter != null) {
				$criteria = $filter->getCriteria();
			}
		}
		if ($criteria == null) {
			$criteria = $this->getStoredCriteria($ctx, $table);
		}
		return $criteria;
	}

	/**
	 * Twitter/Typeaheadによる検索処理（JSON)
	 * @param ActionContext $ctx
	 * @param $id table id.
	 */
	public function searchTypeaheadJson(ActionContext $ctx, $t = 0, $c = 0, $offset = 0)
	{
		$table = $this->engine->getTable($t);
		$column = $table->getColumnFor($c);
		if ($column->getType() != Column::LOOKUP) {
			throw new \Exception('Invalid column.');
		}

		$row = $table->addRow();

		$criteria = new Criteria();
		if ($column->getIndexType() === Index::STRING) {
			$criteria->like($column->getReferenceTargetColumn(), $ctx->getParam('searchWord', false));
		}else if ($column->getIndexType() === Index::NUMBER || $column->getIndexType() === Index::INTEGER){
			$criteria->equals($table->getRownum(), -1); // 0件にする
		}

		try {
			$list = $row->serachReferal($column, $ctx->getUser(), $criteria, $offset);
			return $ctx->respondJSON(array('status'=>'success', 'result' => $list));
		} catch (TxCoreException $e) {
			// 権限エラー
			if ($e->getCode() === TxCoreException::PERMISSION_DENIED) {
				return $ctx->respondJSON(array('status'=>'error',
					'errors' => array(new Message(':data_create.reference.permission'))));
			} else {
				throw $e;
			}
		}
	}

	/**
	 * @param ActionContext $ctx
	 * @return bool
	 */
	public function groupOptionsJson(ActionContext $ctx)
	{
		$groups = $this->engine->searchGroups();
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$groups->getRows()));
	}

	/**
	 * @param ActionContext $ctx
	 * @param $groupId
	 * @return bool
	 * @throws TxCoreException
	 */
	public function groupUsersJson(ActionContext $ctx, $groupId)
	{
		$group = $this->engine->getGroup($groupId);
		$groupUsers = $group->getUsers();
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$groupUsers));
	}

	/**
	 * @param Table $table
	 * @param Row $row
	 */
	private function clearFileColumnValue(Table $table, Row $row)
	{
		foreach ($table->getColumns() as $column) {
			if ($column->getType() == Column::FILE) {
				$row->setValue($column, array());
			}
		}
	}

	/**
	 * @param Table $table
	 * @param Row $row
	 */
	private function clearAutofillValues(Table $table, Row $row)
	{
		foreach ($table->getColumns() as $column) {
			if ($column->isAutofill()) {
				$row->setValue($column, null);
			}
		}
	}

	/**
	 * @param Table $table
	 * @param Row $row
	 */
	private function renewRelationalInput(Table $table, Row $row)
	{
		foreach ($table->getColumns() as $column) {
			if ($column->getType() == Column::RELATIONAL_INPUT) {
				$rows = $row->getValue($column);
				$referenceTable = $column->getReferenceTable();
				foreach ($rows as $child) {
					$child->_setId(null);
					$child->setAction(Row::ACTION_INSERT);
					$this->clearFileColumnValue($referenceTable, $child);
					$this->clearAutofillValues($referenceTable, $child);
					$this->renewRelationalInput($referenceTable, $child);
				}
			}
		}
	}

	public function notificationJson(ActionContext $ctx, $notificationId)
	{
		$receipt = $this->engine->getNotificationReceipt(
			$notificationId, $ctx->getUser());

		return $ctx->respondJSON(
			array('status'=>'success', 'result'=>$receipt));
	}

	public function toggleNotificationReadJson(ActionContext $ctx, $id = 0, $is_read = false)
	{
		$user = $ctx->getUser();
		$receipt = $this->engine->getNotificationReceipt($id, $user);
		$receipt->setRead($is_read);
		$receipt->save();
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$receipt));
	}

    /**
     * @param ActionContext $ctx
     * @param int $tableId
     * @return array
     */
    private function getViewSettings(ActionContext $ctx, $tableId)
    {
        $userId = $ctx->getUser()->getId();
        $key = sprintf(self::DATA_VIEW_SETTINGS_FORMAT,
            intval($userId), intval($tableId));
        $rawStr = $ctx->getCookie($key);

        $obj = false;
        if (is_not_empty($rawStr)) {
            $obj = @json_decode($rawStr, JSON_OBJECT_AS_ARRAY);
        }

//        if ($obj === false) {
//            $obj = array('viewType' => 'list', 'userGroups' => array());
//        }

        return $obj;
    }

//    /**
//     * @param ActionContext $ctx
//     * @param int|null $tableId
//     * @param string|null $viewType
//     * @param array $userGroupsObj
//     */
//    private function storeViewSettings(
//        ActionContext $ctx, $tableId, $viewType = null, $userGroupsObj = null)
//    {
//        $key = 'data_view_settings_'.intval($tableId);
//        $viewSettings = $this->getViewSettings($ctx, $tableId);
//        $viewSettings['viewType'] = $viewType;
//
//        if ($userGroupsObj !== null) {
//            if (sizeof($userGroupsObj) > 20) {
//                $viewSettings['userGroups'] = null;
//            } else {
//                $viewSettings['userGroups'] = $userGroupsObj;
//            }
//        }
//
//        $ctx->setCookie($key, json_encode($viewSettings),
//            strtotime('+30 days'));
//    }

	/**
	 * 作成ページ
	 * @param ActionContext $ctx
	 * @param int $t
	 * @return bool
	 * @throws \Exception
	 */
	public function createChildRow(ActionContext $ctx, $t = 0, $rowKey = null)
	{
		$table = $this->engine->getTable($t);

		// 権限チェック
		if(!$table->allows('insert', $ctx->getUser())){
			throw new TxCoreException(
				sprintf('User %d has no authentication for %s table %d (owner:%s).', 
					$ctx->getUser()->getId(), 
					'insert', 
					$table->getId(), 
					($table->getOwner() ? $table->getOwner()->getId() : null)),
				TxCoreException::PERMISSION_DENIED);
		}
		
		return $ctx->render('my/data/data_create_child.tpl', array('table' => $table, 'rowKey' => $rowKey));
	}

	/**
	 * 編集ページ
	 * @param ActionContext $ctx
	 * @param $t table id.
	 * @param $c source column id.
	 * @param $f form internal key.
	 * @param $k row internal key.
	 */
	public function editChildRow(ActionContext $ctx, $t = 0, $r = 0, $rowKey = null)
	{
		$table = $this->engine->getTable($t);
		// 権限チェック
		if(!$table->allows('update', $ctx->getUser())){
			throw new TxCoreException(
				sprintf('User %d has no authentication for %s table %d (owner:%s).', 
					$ctx->getUser()->getId(), 
					'update', 
					$table->getId(), 
					($table->getOwner() ? $table->getOwner()->getId() : null)),
				TxCoreException::PERMISSION_DENIED);
		}
		
		$row = null;
		if ($r) {
			$row = $table->getRowFor($r);
		}
		//return $ctx->render('my/data/data_create_child.tpl', array('table' => $table, 'row' => $row));
		return $ctx->render('my/data/data_create_child.tpl', array('table' => $table, 'row' => $row, 'rowKey' => $rowKey));
	}

	/**
	 * 子データの登録・更新（JSON）
	 * @param ActionContext $ctx
	 * @param int $tableId
	 * @return bool
	 * @throws TxCoreException
	 */
	public function updateChildJson(ActionContext $ctx, $tableId = 0)
	{
		$table = $this->engine->getTable($tableId);
		$data = $ctx->getParam('data', true);
		/**@var $data array */

		// ロック
		$table->lock();

		$rows = array();
		$allErrors = array();
		$hasError = false;
		$uniqueColumns = array();
		$uniqueColumnValues = array();
		foreach ($table->getColumns() as $c) {
			if ($c->hasProperty('isUnique') && $c->getProperty('isUnique')) {
				$uniqueColumns[$c->getId()] = $c;
				$uniqueColumnValues[$c->getId()] = array();
			}
		}
		foreach ($data as $index => $entryData) {
			$row = isset($entryData->rowId) ? $table->getRowFor($entryData->rowId) : $table->addRow();	/** @var Row $row  */
			$this->setRowValues($ctx, $table, $row, $entryData->values);
			$errors = $row->validate();
			if (is_empty($errors)) {
				foreach ($uniqueColumns as $c) {
					$value = $row->getValue($c);
					if (is_empty($value)) continue;
					if (in_array($value, $uniqueColumnValues[$c->getId()])) {
						$errors[] = new Message(":data_create.duplicated",
							array($c->getLabel()), $c);
					}
					$uniqueColumnValues[$c->getId()][] = $value;
				}
			}
			$allErrors = array_merge($allErrors, $errors);
			foreach ($errors as $error) {
				$originalContext = $error->getContext();
				$error->setContext(array('index' => $index,
					'columnId' => $originalContext instanceof Column
						? $originalContext->getId() : null));
			}

			if (is_not_empty($errors)) {
				$hasError = true;
			}
			$row->setVersion($entryData->version);
			$rows[] = $row;
		}

		if ($hasError) {
			$this->rollback();
			return $ctx->respondJSON(array('status' => 'error', 'errors' => $allErrors));
		}

		return $ctx->respondJSON(array('status' => 'success', 'result' => $rows[0]));
			//array_map(function(Row $row) { return $row->getId(); }, $rows)));
	}


	public function sortChildRowsJson(ActionContext $ctx, $t = 0, $c = 0)
	{
		$parentTable = $this->engine->getTable($t);
		$column = $parentTable->getColumnFor($c);
		$table = $column->getReferenceTable();
		$data = $ctx->getParam('data', true);
		$rows = array();
		foreach ($data as $index => $entryData) {
			$row = isset($entryData->rowId) ? $table->getRowFor($entryData->rowId) : $table->addRow();	/** @var Row $row  */
			$this->setRowValues($ctx, $table, $row, $entryData->values);
			$rows[$entryData->internalKey] = $row;
		}

		$criteria = new Criteria();
		if ($column->getReferenceSortColumn() !== null) {
			$criteria->addSort($column->getReferenceSortColumn(), $column->getReferenceSortsReverse());
		}
		$rows = $this->engine->sortRows($rows, $criteria);
		$order = array_keys($rows);

		return $ctx->respondJSON(array('status'=>'success', 'result'=>$order));
	}

	/**
	 * 明細入力レコードの検索（JSON)
	 * @param ActionContext $ctx
	 * @param $id table id.
	 */
	public function searchRelationalInputJson(ActionContext $ctx, $t = 0, $c = 0, $rowId = 0, $asValue = false)
	{
		$table = $this->engine->getTable($t);
		$column = $table->getColumnFor($c);
		if ($rowId !== 0) {	// 表示のみ
			$row = $table->getRowFor($rowId);
		} else {
			$row = $table->addRow();
		}

		try {
			if (is_empty($row->getId())) {
				// 0件データを返す
				$criteria = new Criteria();
				$criteria->setListColumns($column->getReferenceColumns());
				$criteria->equals($table->getRownum(), -1);
				$list = $row->serachReferal($column, $ctx->getUser(), $criteria, 0);
				return $ctx->respondJSON(
						array('status'=>'success', 'result'=> $asValue ? $list->getRows() : $list));
			}
			$list = $row->serachReferal($column, $ctx->getUser(), null, 0);
			return $ctx->respondJSON(
				array('status'=>'success', 'result'=> $asValue ? $list->getRows() : $list));
		} catch (TxCoreException $e) {
			// 権限エラー
			if ($e->getCode() === TxCoreException::PERMISSION_DENIED) {
				return $ctx->respondJSON(array('status'=>'error',
					'errors' => array(new Message(':data_create.reference.permission'))));
			} else {
				throw $e;
			}
		}
	}





    /**
     * /my/api/v1/get_read_state.json?table_id=1&row_id=1
     * @param ActionContext $ctx
     * @param int $tableId
     * @param int $rowId
     * @return bool
     * @throws \TxCore\TxCoreException
     */
    public function readStateJson(ActionContext $ctx, $tableId = 0, $rowId = 0)
    {
        $table = $this->engine->getTable($tableId);

        if (!$table->isReadCheckEnabled() ||
            !$table->isReadCheckTarget($ctx->getUser())) {
            return $ctx->respondJSON(
                array('status'=>'failed', 'data' => array('isTarget' => false)));
        }

        $row = $table->getRowFor($rowId, $ctx->getUser());
        $state = $row->getReadState($ctx->getUser());

        return $ctx->respondJSON(
            array('status'=>'success', 'data' =>
                array('isTarget' => true, 'state'=>$state)));
    }

    /**
     * @param ActionContext $ctx
     * @param int $tableId
     * @param int $rowId
     * @param int $offset
     * @param int $limit
     * @return bool
     * @throws \TxCore\TxCoreException
     */
    public function readListJson(ActionContext $ctx, $tableId = 0, $rowId = 0, $offset = 0, $limit = 20)
    {
        $table = $this->engine->getTable($tableId, Engine::RELEASE, $ctx->getUser());
        $row = $table->getRowFor($rowId, $ctx->getUser());
        $readList = $row->getReadList($offset, $limit);
        return $ctx->respondJSON(array('status' => 'success', 'data' => $readList ));
    }

    /**
     * @param ActionContext $ctx
     * @param int $tableId
     * @param int $rowId
     * @param string $comment
     * @param bool $value
     * @param $notificationId
     * @return bool
     * @throws TxCoreException
     */
    public function updateReadJson(ActionContext $ctx, $tableId,
        $rowId, $comment = null, $value = false, $notificationId = 0)
    {
        $table = $this->engine->getTable($tableId);
        $row = $table->getRowFor($rowId, $ctx->getUser());
        $row->makeRead($ctx->getUser(), $comment);
//        if ($value) {
//            $row->makeRead($ctx->getUser(), $comment);
//        } else {
//            $row->makeUnread($ctx->getUser());
//        }

        if ($notificationId !== 0) {
            $user = $ctx->getUser();
            $receipt = $this->engine->getNotificationReceipt($notificationId, $user);
            $receipt->setRead($value);
            $receipt->save();
        }

        return $ctx->respondJSON(array('status' => 'success', 'data' => $row ));
    }


}
