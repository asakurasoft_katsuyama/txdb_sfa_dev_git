<?php
namespace Action;

use Action\BaseActions;
use Barge\DB\DBClient;
use Barge\Web\ActionContext;
use Barge\Web\ActionFilterChain;
use Barge\Web\DefaultUploadHandler;
use TxCore\User;
use Util\ApiConverter;
use JSON\JSON;

class ApiActions extends BaseActions
{
	/**
	 * @var TxCore\User
	 */
	private $user = null;

	/**
	 * @param ActionContext $ctx
	 * @param string $action
	 */
	public function filterAction(ActionFilterChain $chain, ActionContext $ctx)
	{
		if ($this->checkAction($ctx, $chain->getTargetMethod()) === false) {
			$errors = array(array('message'=>get_message('api.permission_denied')));
			$this->response($ctx, 400, array('errors' => $errors), null);
		} else {
			$this->removeTempFiles("upload", 60 * 60 * 24);	// 24時間経過したものを消す
			$this->removeTempFiles("thumb", 60 * 60 * 24);	// 24時間経過したものを消す
			$ctx->setJSONConverter(new ApiConverter());
			$tx = DBClient::connect();
			$tx->beginTrans();
			$chain->doChain();
			if (!$this->rollback)
				$tx->commit();
		}
	}

	/**
	 * @param ActionContext $ctx
	 * @param string $action
	 */
	protected function checkAction(ActionContext $ctx, $action)
	{
		if ($this->getUser() == null) {
			return false;
		}
		return true;
	}

	protected function getUser()
	{
		if ($this->user === null) {
			$loginId = isset($_SERVER['HTTP_X_TSUKAERUCLOUDDB_USER'])
					? $_SERVER['HTTP_X_TSUKAERUCLOUDDB_USER'] : null;
			if ($loginId) {
				$this->user = $this->engine->findUserByLoginId(base64_decode($loginId));
			}
		}
		return $this->user;
	}

	/**
	 * @param User $user
	 */
	protected function uploadFile(User $user, $options = array())
	{
		list($tempDir, $tempUrl) = self::getTempFilePath('upload');

		$options['upload_dir'] = $tempDir . DIRECTORY_SEPARATOR;
		$options['upload_url'] = $tempUrl . '/';

		$handler = new DefaultUploadHandler($user, $options, false);
		return $handler->post(false);
	}

	protected function response(ActionContext $ctx, $status, $data, $callback)
	{
		http_response_code($status);

		if (is_empty($callback)) {
			return $ctx->respondJSON($data);
		} else {
			$converter = new ApiConverter();
			return $ctx->respond($callback.'('.JSON::serialize($data, $converter).')', 'text/javascript; charset=utf-8');
		}
	}

}
