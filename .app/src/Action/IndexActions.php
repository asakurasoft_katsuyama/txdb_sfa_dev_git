<?php 
namespace Action;

use Barge\Web\Actions;
use Barge\Web\ActionContext;

use TxCore\Engine;
use TxCore\User;
// use TxCore\TableDef;
// use TxCore\ColumnDef;

use Form\IndexForm;


class IndexActions extends BaseActions 
{
	/**
	 * @param ActionContext $ctx
	 * @param string $action
	 */
	public function checkLogin(ActionContext $ctx, $action)
	{
		return true;
	}
	
	/**
	 * @param ActionContext $ctx
	 * @param IndexForm $form
	 * @param string $sampleText
	 */
	public function index(ActionContext $ctx, IndexForm $form) 
	{
		if ($ctx->getUser() !== null) {
			return $ctx->redirect('/my/');
		} else {
			return $ctx->redirect('/login/');
		}
	}

}
