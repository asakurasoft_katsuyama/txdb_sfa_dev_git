<?php 
namespace Action;

use Barge\Web\Actions;
use Barge\Web\ActionContext;

use Form\LoginForm;
use Util\Message;
use TxCore\Engine;

/**
 * ホーム
 */
class MyActions extends BaseActions
{
	/**
	 * @param ActionContext $ctx
	 */
	public function index(ActionContext $ctx) 
	{
		$user = $ctx->getUser();
		$tables = $this->engine->getUserTables($user);
		$publicTables = array();
		$privateTables = array();
		foreach ($tables as $table) {
			if(!$user->isAdmin() && ($table->getOwner() === null || $table->getOwner()->getId() != $user->getId())){
				// テーブルのアクセス制限状態を取得
				$permissionControl = $table->getPermissionControl();
				if($permissionControl){
					// ログインユーザの権限情報を取得
					$permission = $table->getPermissionForUserId($user->getId(), true);
					if(is_null($permission) || !$permission->getIsReadable()){
						continue;
					}
				}
			}
			
			if ($table->isPublic()) {
				$publicTables[] = $table;
			} else {
				$privateTables[] = $table;
			}
		}
		return $ctx->render('my/my_index.tpl', 
				array('publicTables' => $publicTables, 'privateTables' => $privateTables));
	}

	public function logout(ActionContext $ctx)
	{
		$ctx->destroySession();
		return $ctx->redirect('/login/');
	}

	public function countNotificationJson(ActionContext $ctx)
	{
		$user = $ctx->getUser();
		$count = Engine::factory()->countNotificationReceipts($user, false);
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$count));
	}

}
