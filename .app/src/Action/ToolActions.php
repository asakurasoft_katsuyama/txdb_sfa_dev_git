<?php 
namespace Action;

use TxCore\RelationalColumn;

use Util\Util;
use TxCore\Reference;
use TxCore\Option;
use TxCore\Column;
use TxCore\Table;
use TxCore\Row;

use Action\BaseActions;
use TxCore\Criteria;
use Barge\Web\Actions;
use Barge\Web\ActionContext;

use Form\LoginForm;

class ToolActions extends BaseActions
{
	/**
	 * 空のテンプレート作成 
	 */
/*
	public function init(ActionContext $ctx) 
	{
		$user = $ctx->getUser();
		if (!$user->isAdmin()) {
			echo '<h1>This action requires the administrator login.</h1>';
			exit;
		}
		
		if ($this->engine->countTemplateTables() > 0) {
			echo '<h1>The first template was already created.</h1>';
			exit;
		}
		
		$icons = $this->engine->getIcons();
		$table = new Table();
		$table->setName('空のDB');
		$table->setIcon($icons[0]);
		$table->create($ctx->getUser());
		$this->engine->changeTableToTemplate($table, true);
		
		return $ctx->redirect('/my/');
		
	}
*/
		
	/**
	 * テストテーブル作成
	 * @param ActionContext $ctx
	 * @param $id table id.
	 */
/*
	public function index(ActionContext $ctx, $t = 0) 
	{
		$tables = $this->engine->getUserTables($ctx->getUser());
		return $ctx->render('tool/tool_index.tpl', array(@tables=>$tables));
	}
	
	public function create(ActionContext $ctx) 
	{
		$icons = $this->engine->getIcons();
		
		$table = new Table();
		$table->setName($ctx->getParam('name'));
		$table->setIcon($icons[0]);
		
		$params = $ctx->getParams();
		$listColumns = array();
		$columns = array();
		for ($i = 0; $i < 30; $i++) {
			if (isset($params['columns'][$i]['checked'])) {
				$count = $params['columns'][$i]['count'] ?: 1;
				foreach (range(1, $count) as $t) {
					$column = $table->addColumn($params['columns'][$i]['type']);
					$column->setLabel($params['columns'][$i]['code'] . ($count > 1 ? "_$t" : ""));
					$column->setCode($params['columns'][$i]['code'] . ($count > 1 ? "_$t" : ""));
					$column->setShowsField(true);
					if (isset($params['columns'][$i]['default']) && is_not_empty($params['columns'][$i]['default'])) 
						$column->setDefaultValue($params['columns'][$i]['default']);
					if (isset($params['columns'][$i]['height']) && is_not_empty($params['columns'][$i]['height']))
						$column->setSizeHeight($params['columns'][$i]['height']);
					if (isset($params['columns'][$i]['expression']) && is_not_empty($params['columns'][$i]['expression'])) 
						$column->setExpression($params['columns'][$i]['expression']);
					if (isset($params['columns'][$i]['x']) && is_not_empty($params['columns'][$i]['x']))
						$column->setPositionX(intval($params['columns'][$i]['x']));
					if (isset($params['columns'][$i]['y']) && is_not_empty($params['columns'][$i]['y']))
						$column->setPositionY(intval($params['columns'][$i]['y']));
					if (isset($params['columns'][$i]['span']) && is_not_empty($params['columns'][$i]['span']))
						$column->setSizeWidth(intval($params['columns'][$i]['span']));
					if (isset($params['columns'][$i]['required']))
						$column->setRequired(true);
					if (isset($params['columns'][$i]['minLength']) && is_not_empty($params['columns'][$i]['minLength'])) 
						$column->setMinLength(intval($params['columns'][$i]['minLength']));
					if (isset($params['columns'][$i]['maxLength']) && is_not_empty($params['columns'][$i]['maxLength'])) 
						$column->setMaxLength(intval($params['columns'][$i]['maxLength']));
					if (isset($params['columns'][$i]['minValue']) && is_not_empty($params['columns'][$i]['minValue'])) 
						$column->setMinValue(intval($params['columns'][$i]['minValue']));
					if (isset($params['columns'][$i]['maxValue']) && is_not_empty($params['columns'][$i]['maxValue'])) 
						$column->setMaxValue(intval($params['columns'][$i]['maxValue']));
					if (isset($params['columns'][$i]['link']) && is_not_empty($params['columns'][$i]['link']))
						$column->setLinkType($params['columns'][$i]['link']);
					if (isset($params['columns'][$i]['labelHtml']) && is_not_empty($params['columns'][$i]['labelHtml']))
						$column->setLabelHtml($params['columns'][$i]['labelHtml']);
					if (isset($params['columns'][$i]['tumb']) && is_not_empty($params['columns'][$i]['tumb']))
						$column->setThumbnailMaxSize(intval($params['columns'][$i]['tumb']));
					$ref = null;
					if (isset($params['columns'][$i]['refTable']) && is_not_empty($params['columns'][$i]['refTable'])) {
						$ref = $this->engine->getTable($params['columns'][$i]['refTable']);
						$column->setReferenceTable($ref);
						$column->setReferenceColumns(array_filter($ref->getColumns(), function($c) {
							return !in_array($c->getType(), array('label', 'space', 'line', 'relational'), true);
						}));
					}
					if (isset($params['columns'][$i]['refTarget']) && is_not_empty($params['columns'][$i]['refTarget']))
						$column->setReferenceTargetColumn($ref->getColumn(intval($params['columns'][$i]['refTarget'])));
					if (isset($params['columns'][$i]['refSource']) && is_not_empty($params['columns'][$i]['refSource']))
						$column->setReferenceSourceColumn($columns[intval($params['columns'][$i]['refSource'])]);
					if (isset($params['columns'][$i]['refRows']) && is_not_empty($params['columns'][$i]['refRows']))
						$column->setReferenceMaxRow(intval($params['columns'][$i]['refRows']));
					if (isset($params['columns'][$i]['refCopy']) && is_not_empty($params['columns'][$i]['refCopy'])) {
						$copyMap = array();
						foreach (explode(',', $params['columns'][$i]['refCopy']) as $x) {
							if (is_empty($x)) continue;
							$offsets = explode('->', $x);
							$copyMap[] = array($ref->getColumn($offsets[0]), $columns[$offsets[1]]);
						}
						$column->setReferenceCopyMap($copyMap);
					}
					if (isset($params['columns'][$i]['option'])) {
						for ($j = 0; $j < intval($params['columns'][$i]['option']); $j++) {
							$option = $column->addOption();
							$option->setName("Option $j");
						}
					}
					if (isset($params['columns'][$i]['list'])) {
						$listColumns[] = $column; 
					}
				}
				$columns[$i] = $column;
			}
		}
		
		$table->setViewColumns($listColumns);
		$table->create($ctx->getUser());
		
		return $ctx->redirect('/my/data/?t='.$table->getId());
	}

	private function getParamValue($ctx, Column $column)
	{
		return $ctx->getParam('col_'.$column->getId());
	}
	
	private function getValues($ctx) 
	{
		$data = array();
		foreach ($ctx->getParams() as $key => $value) {
			if (!str_starts_with('col_', $key))
				continue;
			$data[substr($key, 4)] = $value;
		}
		return $data;
	}
*/
}


