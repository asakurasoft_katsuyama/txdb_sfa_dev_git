<?php
namespace Action;


use Barge\Log\DC;

use \DirectoryIterator;

use Barge\Web\ActionFilterChain;
use Barge\DB\DBClient;

use TxCore\Criteria;
use TxCore\Table;
use TxCore\Util;

use Barge\Web\Actions;
use Barge\Web\ActionContext;
use Barge\Web\DefaultUploadHandler;

use TxCore\User;
use TxCore\Engine;

use Util\DefaultConverter;

class BaseActions extends Actions
{
	/**
	 * @var \TxCore\Engine
	 */
	protected $engine;

	/**
	 * @var boolean
	 */
	protected $rollback;

	/**
	 * A constructor.
	 */
	public function __construct()
	{
		$this->engine = Engine::factory();
	}

	protected function rollback()
	{
		$this->rollback = true;
	}

	/**
	 * @param ActionContext $ctx
	 * @param string $action
	 */
	public function filterAction(ActionFilterChain $chain, ActionContext $ctx)
	{
		if ($this->checkLogin($ctx, $chain->getTargetMethod()) === false) {
			if (Util::isJSONRequest($ctx->getRequestURI())) {
				http_response_code(400);
				$errors = array(array('message'=>get_message('global.session_timeout')));
				$ctx->respondJSON(array('errors'=>$errors));
			} else {
				$requestUri = $ctx->getRequestURI();
				if ($requestUri != '/login/' ) {
					$ctx->setSession('bookmarked_url', $requestUri);
				}
				$ctx->forward('/login/');
			}
		} else {
			$ctx->setJSONConverter(new DefaultConverter($ctx->getUser()));
			$tx = DBClient::connect();
			$tx->beginTrans();
			$chain->doChain();
			if (!$this->rollback)
				$tx->commit();
		}
	}

	/**
	 * @param ActionContext $ctx
	 * @param string $action
	 */
	protected function checkLogin(ActionContext $ctx, $action)
	{
		if ($ctx->getUser() == null) {
			return false;
		}
	}


	/**
	 * @param User $user
	 */
	protected function uploadFile(User $user, $options = array())
	{
		list($tempDir, $tempUrl) = self::getTempFilePath('upload');

		$options['upload_dir'] = $tempDir . DIRECTORY_SEPARATOR;
		$options['upload_url'] = $tempUrl . '/';

		new DefaultUploadHandler($user, $options);
	}


	/**
	 * 一時ディレクトリに保存されたファイルを削除する
	 * @param $dir
	 */
	public function removeTempFiles($subdir, $pastSecs = 0)
	{
		list($tempDir, ) = self::getTempFilePath($subdir);
		if (!file_exists($tempDir)) {
			return;
		}
		$currentTime = time();
		foreach (new DirectoryIterator($tempDir) as $f) {
			if ($f->isDot()) continue;
			if ($currentTime - $f->getMTime() > $pastSecs) {
				@unlink($f->getPathname());
			}
		}
	}

	/**
	 * ユーザーの一次ファイルを作成する（パス名を作成する）
	 * @param User $user
	 * @param string $extention 拡張子
	 * @return array ファイルパスとURL
	 */
	public function createTempFile($dir, $extention)
	{
		$tempDir = $this->getTempFilePath($dir);
		$fileName = Util::generateFileHash().'.'.$extention;
		return array(
			concat_path($tempDir[0], $fileName),
			concat_path($tempDir[1], $fileName));
	}

	/**
	 * ファイル名から、ユーザ一時ファイルのパスを作成する
	 * @param User $user
	 * @param $filename ファイル名
	 * @return array ファイルパスとURL
	 */
	public function getTempFilePath($subdir, $fileName = '')
	{
		$tempDir = concat_path(get_config('app', 'user_temp_dir'), $subdir);
		$tempUrl = concat_path(get_config('app', 'user_temp_dir_url'), $subdir);
		return array(concat_path($tempDir, $fileName), concat_path($tempUrl, $fileName));
	}


}
