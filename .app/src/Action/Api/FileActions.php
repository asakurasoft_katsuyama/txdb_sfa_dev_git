<?php 
namespace Action\Api;


use Form\Api\FileUploadForm;

use JSON\JSON;

use Util\ApiConverter;

use Action\ApiActions;
use Barge\Web\ActionContext;

use TxCore\TxCoreException;
use TxCore\Filter;
use TxCore\Engine;
use TxCore\User;
use TxCore\Message;
use TxCore\Column;
use TxCore\Table;
use TxCore\UploadedFile;

use Util\Util;


class FileActions extends ApiActions
{
	
	/**
	 * ファイルのアップロード (JSON)
	 * @param ActionContext $ctx
	 */
	public function uploadJson(ActionContext $ctx, FileUploadForm $form)
	{
		$user = $this->getUser();
		$errors = $form->validate($user);
		if (is_not_empty($errors)) {
			return $this->response($ctx, 400, array('errors' => $errors), null);
		}
		
		/* @var $table Table */
		$table = $form->_table;
		
		$fileParamName = 'file';
		// Number of file must be 1.
		$upload = isset($_FILES[$fileParamName]) ? $_FILES[$fileParamName] : null;
		if (!$upload || !isset($upload['tmp_name'])) {
			$errors = array(new Message(':api.file.invalid', array(), $fileParamName));
			return $this->response($ctx, 400, array('errors' => $errors), null);
		}
		
		if (is_array($upload['tmp_name'])) {
			$errors = array(new Message(':api.file.invalid', array(), $fileParamName));
			return $this->response($ctx, 400, array('errors' => $errors), null);
		}
		
		$content = $this->uploadFile($user, array(
			'param_name' => $fileParamName,
			'max_file_size' => get_config('app','data_file_size_limit')
		));
		
 		$file_ = $content[$fileParamName][0];
 		if (isset($file_->error)) {
			$errors = array(new Message($file_->error, array(), $fileParamName));
			return $this->response($ctx, 400, array('errors' => $errors), null);
 		}
 		
 		$file = UploadedFile::create($file_->name, $file_->originalName, $file_->type);
 		
		list($filePath, ) = self::getTempFilePath('upload', $file->getFilekey());
		$metainfo = array(
			'apiFileUpload' => array(
				'tableId' => $table->getId(),
				'fileName' => $file->getFileName(),
				'contentType' => $file->getContentType()
			)
		);
		file_put_contents("{$filePath}.meta", JSON::serialize($metainfo));
		
		return $this->response($ctx, 200, $file, null);
	}
	
}
