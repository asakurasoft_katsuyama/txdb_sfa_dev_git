<?php 
namespace Action\Api;


use Form\Api\DataSearchForm;
use Form\Api\DataDeleteForm;
use Form\Api\DataInsertForm;
use Form\Api\DataUpdateForm;

use JSON\JSON;

use Action\ApiActions;
use Barge\Web\ActionContext;

use TxCore\TxCoreException;
use TxCore\Filter;
use TxCore\Engine;
use TxCore\User;
use TxCore\Message;
use TxCore\Column;
use TxCore\Table;
use TxCore\Row;
use TxCore\Criteria;
use TxCore\LookupValue;
use TxCore\File;

use Util\Util;


class DataActions extends ApiActions
{
	
	/**
	 * @param ActionContext $ctx
	 * @param string $action
	 */
	protected function checkAction(ActionContext $ctx, $action)
	{
		switch ($action) {
			case 'searchJson':
				return true;
				break;
			case 'insertJson':
			case 'updateJson':
			case 'deleteJson':
			default:
				return parent::checkAction($ctx, $action);
		}
	}
	
	/**
	 * @param ActionContext $ctx
	 */
	public function searchJson(ActionContext $ctx, DataSearchForm $form)
	{
		$errors = $form->validate();
		if (is_not_empty($errors)) {
			return $this->response($ctx, 400, array('errors' => $errors), $form->callback);
		}
		
		$errors = $form->_criteria->validate();
		if (is_not_empty($errors)) {
			$errors = array_map(function($error) {
				return (object)array(@message=>$error->getMessage(), @context=>'conditions');
			}, $errors);
			return $this->response($ctx, 400, array('errors' => $errors), $form->callback);
		}
		
		$table = $form->_table;
		$list = null;
		if ($table->isChild()) {
			$list = Engine::factory()->searchRelationshipsForRowInternal($table, $form->_parentRow, $form->_parentColumn, $form->_criteria, $form->offset, $form->limit);
		} else {
			$list = Engine::factory()->searchTableInternal($table, $form->_criteria, $form->offset, $form->limit);
		}

		return $this->response($ctx, 200, $list, $form->callback);
	}
	
	public function insertJson(ActionContext $ctx, DataInsertForm $form)
	{
		return $this->_upsertJson($ctx, $form);
	}
	
	public function updateJson(ActionContext $ctx, DataUpdateForm $form)
	{
		return $this->_upsertJson($ctx, $form);
	}
	
	private function _upsertJson(ActionContext $ctx, $form)
	{
		$user = $this->getUser();
		$errors = $form->validate($user);
		if (is_not_empty($errors)) {
			return $this->response($ctx, 400, array('errors' => $errors), null);
		}
	
		$table = $form->_table;
		$rows = $form->rows;
	
		$resultRows = $this->upsert($rows, $errors, $table, $user);
		if (is_not_empty($errors)) {
			$this->rollback();
			return $this->response($ctx, 400, array('errors' => $errors), null);
		}
		
		$result = array(
			'rows' => $resultRows
		);
	
		return $this->response($ctx, 200, $result, null);
	}
	
	private function upsert($rows, &$errors_, Table $table, User $user, $context = 'rows')
	{
		// ロック
		$table->lock();
		
		$importableColumns = $this->getImportRequiredFields($table);
		
		$results = array();
		
		foreach ($rows as $i => $row_) {
			$no_ = isset($row_['no']) ? $row_['no'] : null;
			$version_ = isset($row_['version']) ? $row_['version'] : null;
			$row = null;
			try {
				$row = $no_ == null ? $table->addRow() : $table->getRowFor($no_, $user, true);
			} catch (TxCoreException $e) {
				if ($e->getCode() == TxCoreException::ROW_NOT_FOUND) {
					$errors_[] = new Message(':global.row_not_found', array(), "rows[{$i}][no]");
					return false;
				}
				throw $e;
			}
				
			$valueMap = array();
			$columnMap = array();
			foreach (isset($row_['values']) ? $row_['values'] : array() as $code_ => $value_) {
				$matches = $table->getColumns(array('code'=>$code_));
				if (is_empty($matches)) {
					$errors = array(new Message(':api.data.columns.not_exist', array($code_), 
							 "{$context}[{$i}][values][{$code_}]"));
					array_push_all($errors_, $errors);
					return false;
				}
				$column = $matches[0];
				$columnMap[$column->getId()] = $column;
				if (in_array($column, $importableColumns)) {
					$valueMap[$column->getId()] = $value_;
				} else {
					// 非対応カラムはエラー
					$errors = array(new Message(':api.data.column_not_supported', array($column->getCode()),
							"{$context}[{$i}][values][{$code_}]"));
					array_push_all($errors_, $errors);
					return false;
				}
			}
				
			$this->setRowValues($row, $valueMap, $errors, $table, $user, "{$context}[{$i}][values]");
			if (is_not_empty($errors)) {
				array_push_all($errors_, $errors);
				return false;
			}
				
			$errors = $row->validate();
			if (is_not_empty($errors)) {
				array_push_all($errors_, $this->convertRowMessages($errors, "{$context}[{$i}][values]"));
				return false;
			}
				
			if ($no_ == null) {
				$table->insertRows($user, true);
			} else {
				try {
					if ($version_) {
						$row->setVersion($version_);
					}
					$row->update($user, true);
				} catch (TxCoreException $e) {
					$row = $e->getContext();
					$errors = array(
						new Message(':api.data.row_has_updated',
							array(
								$row->getUpdatedOn(), 
								$row->getUpdatedBy() ? $row->getUpdatedBy()->getScreenName() : ''
							),
							"{$context}[{$i}][version]"
						)
					);
					array_push_all($errors_, $errors);
					return false;
				}
			}
				
			$result = array(
				'no'=>$row->getRowNo(),
				'version'=>$row->getVersion()
			);
				
			$results[] = $result;
		
		}
		
		return $results;
	}
	
	/**
	 * @param Table $table
	 * @param Row $row
	 * @param stdClass $postData
	 */
	private function setRowValues(Row $row, $values, &$errors_, Table $table, User $by, $context)
	{
		if ($values === null)
			return;
	
		foreach ($values as $id => $value_) {
			$column = $table->getColumnFor($id);	/* @var $column \TxCore\Column */
			if ($column->getType() == Column::CHECKBOX) {
				$options = array();
				foreach (is_empty($value_) ? array() : $value_ as $i => $obj) {
					$value = isset($obj['name']) ? $obj['name'] : null;
					$hits = array_grep($options, function ($option) use ($value) {
						return $option->getName() == $value;
					});
					if (is_not_empty($hits)) {
						continue;
					}
					$matchedOptions = $column->getOptions(array('name' => $value));
					if (is_empty($matchedOptions)) {
						$errors_[] = new Message(':api.data.invalid_option', array($column->getCode()),
							"{$context}[{$column->getCode()}][{$i}][name]");
						return false;
					} else {
						$options[] = $matchedOptions[0];
					}
				}
				$row->setValue($column, $options);
			} else if ($column->getType() == Column::USER_SELECTOR) {
				$options = array();
				foreach (is_empty($value_) ? array() : $value_ as $i => $obj) {
					// Get type.
					//$entities = array();
					if (is_string($obj)) {
						$obj = array('type'=>'user', 'code' => $obj);
					}
				//	log_debug(print_r($obj, true));
					$type = isset($obj['type']) ? $obj['type'] : null;
					if (is_empty($type) || !in_array($type, array('user', 'group'))) {
						$errors_[] = new Message(':api.data.user_selector.type.invalid', array($column->getCode()),
							"{$context}[{$column->getCode()}][{$i}][type]");
						return false;
					}

					if ($type == 'user') {
						$code_ = isset($obj['code']) ? $obj['code'] : null;
						$user = $this->engine->getUser($code_, true, true);
						if ($user === null) {
							$errors_[] = new Message(':api.data.user.not_exist', array($code_),
								"{$context}[{$column->getCode()}][{$i}][code]");
							return false;
						}
						$options[$user->getId()] = $user;
					} else if ($type == 'group') {
						$name = isset($obj['name']) ? $obj['name'] : null;
						$group = $this->engine->findGroupByName($name);
						if ($group === null) {
							$errors_[] = new Message(':api.data.group.not_exist', array($name),
								"{$context}[{$column->getCode()}][{$i}][name]");
							return false;
						}
						foreach ($group->getUsers() as $user) {
							$options[$user->getId()] = $user;
						}
					} else {
						assert(false);
					}
//					$value = isset($option['type']) ? $option['id'] : null;
//					$value = isset($option['name']) ? $option['id'] : null;
//					$hits = array_grep($options, function($option) use ($value) {
//						return $option->getName() == $value;
//					});
//					if (is_not_empty($hits)) {
//						continue;
//					}
//					$matchedOptions = $column->getOptions(array('name'=>$value));
//					if (is_empty($matchedOptions)) {
//						$errors_[] = new Message(':api.data.invalid_option', array($column->getCode()),
//							"{$context}[{$column->getCode()}][{$i}][name]");
//						return false;
//					} else {
//						$options[] = $matchedOptions[0];
//					}
				}
				$row->setValue($column, array_values($options));
			} else if ($column->getType() == Column::LOOKUP) {
				if (is_not_empty($value_)) {
					$value = isset($value_['value']) ? $value_['value'] : null;
					$lookup = new LookupValue();
					$lookup->setValue($value);
					$row->setValue($column, $lookup);

					try {
						$hits = $row->serachReferal($column, $by, null, 0);
						if ($hits->getTotalRowCount() != 1) {
							$errors_[] = new Message(':api.data.lookup_failed', array($column->getCode()),
									"{$context}[{$column->getCode()}]");
							return false;
						}
							
						// Set key
						$lookup->setKey($hits[0]->getId());
							
						// Copy reference-copy field.
						foreach ($column->getReferenceCopyMap() as $pair) {
							$row->setValue($pair[1], $this->getLookupCopyValue($pair, $hits[0]));
						}
		
					} catch (TxCoreException $e) {
						// 権限エラー
						if ($e->getCode() === TxCoreException::PERMISSION_DENIED) {
							$errors_[] = new Message(':api.data.permission_denied', array($column->getCode()),
									"{$context}[{$column->getCode()}]");
							return false;
						} else {
							throw $e;
						}
					}
				} else {
					// clear lookup
					$value = null;
					$lookup = new LookupValue();
					$lookup->setValue($value);
					$row->setValue($column, $lookup);
					$lookup->setKey(null);
					foreach ($column->getReferenceCopyMap() as $pair) {
						$row->setValue($pair[1], null);
					}
				}
			} else if ($column->getType() == Column::FILE) {
				$files = array();
				$filekeys = array();
				foreach (is_empty($value_) ? array() : $value_ as $i => $file) {
					$filekey = isset($file['filekey']) ? $file['filekey'] : null;
					if (is_empty($filekey) || in_array($filekey, $filekeys)) {
						continue;
					}
					$storage = Engine::factory()->getStorage();
					$savedFilepath = $storage->getFilePath($filekey, true);
					if ($savedFilepath) {
						$hits = array_grep($row->getValue($column), function($file) use ($filekey) {
							return $file->getFilekey() == $filekey;
						});
						$files[] = $hits[0];
					} else {
						list($filePath, ) = $this->getTempFilePath('upload', $filekey);
						// see Api\FileActions::uploadJson
						$metafilePath = $filePath.'.meta';
						if (!file_exists($filePath) || !file_exists($metafilePath)) {
							$errors_[] = new Message(':api.data.file.not_exist', array($column->getCode(), $filekey),
									"{$context}[{$column->getCode()}][{$i}][filekey]");
							return false;
						}
						$metainfo = json_decode(file_get_contents($metafilePath));
						if (!isset($metainfo->apiFileUpload)) {
							$errors_[] = new Message(':api.data.file.invalid', array($column->getCode(), $filekey),
									"{$context}[{$column->getCode()}][{$i}][filekey]");
							return false;
						}
						$files[] = File::create($filePath,
								$metainfo->apiFileUpload->fileName, 
								$metainfo->apiFileUpload->contentType);
					}
					$filekeys[] = $filekey;
				}
				$row->setValue($column, $files);
			} else if ($column->hasOptions()) {
				$obj = $value_;
				$value = null;
				if (is_not_empty($obj)) {
					$value = isset($obj['name']) ? $obj['name'] : null;
				}
				if (is_not_empty($value)) {
					$matchedOptions = $column->getOptions(array('name'=>$value));
					if (is_empty($matchedOptions)) {
						$errors_[] = new Message(':api.data.invalid_option', array($column->getCode()),
								"{$context}[{$column->getCode()}][name]");
						return false;
					}
					$row->setValue($column, $matchedOptions[0]);
				} else {
					$row->setValue($column, null);
				}
			} else {
				$value = $value_;
				$row->setValue($column, $value);
			}
		}

	}
	
	private function getLookupCopyValue($pair, Row $row)
	{
		list ($from, $to) = $pair;
		$convertedValue = null;
		$fromValue = $row->getValue($from);
	
		if ($fromValue instanceof LookupValue) {
			$convertedValue = $fromValue->getValue();
		} else if ($from->hasOptions() && $to->hasOptions()) {
			if ($fromValue !== null) {
				// 名前が一致するものをセット
				$matched = $to->getOptions(array('name'=>$fromValue->getName()));
				if (is_not_empty($matched)) {
					$convertedValue = $matched[0];
				}
			}
		} else if ($from->hasOptions() && !$to->hasOptions()) {
			if ($fromValue !== null) {
				$convertedValue = $fromValue->getName();
			}
		} else {
			$convertedValue = $fromValue;
		}
	
		return $convertedValue;
	}
	
	public static function getImportRequiredFields(Table $table)
	{
		$fields = array();
		foreach ($table->getColumns() as $column) {
			if (!$column->isInputable())
				continue;
			if ($column->getType() === Column::RELATIONAL_INPUT)
				continue;
			if ($column->getType() === Column::LOOKUP) {
				$target = $column->getReferenceTargetColumn();
				if ((!$target->hasProperty('isUnique') || !$target->isUnique())
				&& $target->getType() !== Column::ROWNUM) {
					continue;
				}
			}
			// Lookupのコピー先は入力不可
			if (self::isReferenceCopyee($column))
				continue;
			$fields[] = $column;
		}
		return $fields;
	}
	
	/**
	 * @param Column $column
	 * @return boolean
	 */
	private static function isReferenceCopyee($column)
	{
		$lookupColumns = $column->getParent()->getColumns(array('type'=>Column::LOOKUP));
		foreach ($lookupColumns as $lookupColumn) {
			foreach ($lookupColumn->getReferenceCopyMap() as $pairOfColumn) {
				if ($column->getId() == $pairOfColumn[1]->getId()) {
					return true;
				}
			}
		}
		return false;
	}
	
	public function deleteJson(ActionContext $ctx, DataDeleteForm $form)
	{
		$user = $this->getUser();
		$errors = $form->validate($user);
		if (is_not_empty($errors)) {
			return $this->response($ctx, 400, array('errors' => $errors), null);
		}
	
		$table = $form->_table;
		$rows = $form->rows;
		
		$results = $this->delete($rows, $errors, $table, $user);
		if (is_not_empty($errors)) {
			return $this->response($ctx, 400, array('errors' => $errors), null);
		}
	
		return $this->response($ctx, 200, (object)array(), null);
	}
	
	private function delete($rows, &$errors_, Table $table, User $user, $context = 'rows')
	{
		// ロック
		$table->lock();
		
		foreach ($rows as $i => $row_) {
			$row = null;
			try {
				$row = $table->getRowFor($row_['no'], $user, true);
			} catch (TxCoreException $e) {
				if ($e->getCode() == TxCoreException::ROW_NOT_FOUND) {
					$errors_[] = new Message(':global.row_not_found', array(), "{$context}[{$i}][no]");
					return false;
				}
				throw $e;
			}
			
			$row->delete($user);
		}
		return array();
	}
		
	private function convertRowMessage($message, $context)
	{
		if ($message->getContext() instanceof Column) {
			$column = $message->getContext();
			$message->setContext("{$context}[{$column->getCode()}]");
		}
		
		return $message;
	}
	
	private function convertRowMessages($messages, $context)
	{
		foreach ($messages as $message) 	/* @var $message Message */
			$this->convertRowMessage($message, $context);
		return $messages;
	}
	
}
