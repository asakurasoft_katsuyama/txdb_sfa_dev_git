<?php
namespace Action\Admin;

use Action\BaseActions;

use TxCore\Criteria;

use Barge\Web\Actions;
use Barge\Web\ActionContext;

use TxCore\Message;

use TxCore\User;
use TxCore\TxCoreException;
use Util\UserCSV;
use Form\Admin\UserImportForm;
use TxCore\Engine;

/**
 * ユーザー
 */
class UserActions extends BaseActions
{
	/**
	 * @param ActionContext $ctx
	 */
	public function index(ActionContext $ctx)
	{
		if ($ctx->getUser()->isAdmin()) {
			$userLimit = get_config('app', 'user_num_limit');
			if ($userLimit <= 0) $userLimit = "無制限";
			$userCount = $this->engine->countUsers();
			return $ctx->render('admin/user_index.tpl',
					array(
						'userLimit' => $userLimit, 'userCount' => $userCount,
						'limit' => $this->getStoredLimit($ctx), 'searchQuery' => $this->getStoredSearchQuery($ctx)
					)
			);
		} else {
			return $ctx->redirect('/admin/user/modify?u='.$ctx->getUser()->getId());
		}

	}

	/**
	 * @param ActionContext $ctx
	 * @return array
	 */
	private function getStoredLimit(ActionContext $ctx)
	{
		$searchStatus = $ctx->getSession('user_search_limit');
		if ($searchStatus)
			return $searchStatus;
		return array(0, 20);
	}


	/**
	 * @param ActionContext $ctx
	 * @param array $limit
	 */
	private function storeLimit(ActionContext $ctx, array $limit = null)
	{
		$ctx->setSession('user_search_limit', $limit);
	}

	/**
	 * @param ActionContext $ctx
	 * @param array $searchQuery
	 */
	private function storeSearchQuery(ActionContext $ctx, $searchQuery = null)
	{
		$ctx->setSession('user_search_query', $searchQuery);
	}

	/**
	 * @param ActionContext $ctx
	 * @return string
	 */
	private function getStoredSearchQuery(ActionContext $ctx)
	{
		$searchStatus = $ctx->getSession('user_search_query');
		if ($searchStatus)
			return $searchStatus;
		return null;
	}

	/**
	 * @param ActionContext $ctx
	 * @param $q keyword.
	 */
	public function indexJson(ActionContext $ctx, $q = null, $offset = 0, $limit = 20)
	{
		if (!$ctx->getUser()->isAdmin()) {
			return $ctx->respondJSON(array('status'=>'error'));
		}


		$list = $this->engine->searchUsers($q, $offset, $limit);

		$this->storeSearchQuery($ctx, $q);
		$this->storeLimit($ctx, array($offset, $limit));

		return $ctx->respondJSON(array('status'=>'success', 'result'=>$list));
	}

	/**
	 * @param ActionContext $ctx
	 */
	public function create(ActionContext $ctx)
	{
		return $ctx->render('admin/user_modify.tpl', array('userId' => null));
	}

	/**
	 * @param ActionContext $ctx
	 * @param $u user id.
	 */
	public function modify(ActionContext $ctx, $u = 0)
	{
		return $ctx->render('admin/user_modify.tpl', array('userId' => $u));
	}
	/**
	 * @param ActionContext $ctx
	 * @param $u user id.
	 */
	public function loadJson(ActionContext $ctx, $u = 0)
	{
		if (!$ctx->getUser()->isAdmin()) {
			if ($ctx->getUser()->getId() != $u) {
				return $ctx->respondJSON(array('status'=>'error'));
			}
		}

		$user = $this->engine->getUser($u);
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$user));
	}

	/**
	 * @param ActionContext $ctx
	 */
	public function modifyJson(ActionContext $ctx)
	{
		$json = $ctx->getParam('data');
		$data = json_decode($json, true);

		$u = $data['id'];
		$user = null;
		if ($u == null) { // create
			$userLimit = get_config('app', 'user_num_limit');
			$userCount = $this->engine->countUsers();
			if ($userLimit > 0 && $userCount >= $userLimit) {
				$errors[] = new Message('ユーザー数上限により登録できません。');
				return $ctx->respondJSON(array('status'=>'error', 'errors'=>$errors));
			}
			$user = new User();
			$user->setScreenName($data['screenName']);
			$user->setLoginId($data['loginId']);
			$user->setPassword($data['password']);
			$user->setProfile($data['profile']);
			$user->setSeq($data['seq']);
			$user->setRole(User::USER);
		} else {
			$user = $this->engine->getUser($u);
			$user->setScreenName($data['screenName']);
			$user->setLoginId($data['loginId']);
			$user->setPassword($data['password']);
			$user->setProfile($data['profile']);
			if ($ctx->getUser()->isAdmin()) {
				$user->setSeq($data['seq']);
			}
		}

		$errors = $user->validate();
		if (is_not_empty($errors)) {
			return $ctx->respondJSON(array('status'=>'error', 'errors'=>$errors));
		}

		try {
			if ($u == null) { // create
				$user->create($ctx->getUser());
				$this->storeSearchQuery($ctx, null);
				$this->storeLimit($ctx, null);
			} else {
				$user->update($ctx->getUser());
				if ($user->getId() == $ctx->getUser()->getId()) {
					$ctx->setUser(Engine::factory()->getUser($ctx->getUser()->getId()));
				}
			}
		} catch (TxCoreException $e) {
			if ($e->getCode() == TxCoreException::LOGIN_ID_WAS_DUPLICATED) {
				$this->rollback();
				$errors[] = new Message('入力されたログインIDは他のユーザーが登録済みです。');
				return $ctx->respondJSON(array('status'=>'error', 'errors'=>$errors));
			} else {
				throw $e;
			}
		}

		return $ctx->respondJSON(array('status'=>'success', 'result'=>$user->getId()));
	}
	/**
	 * @param ActionContext $ctx
	 * @param $u user id.
	 */
	public function delete(ActionContext $ctx, $u = 0)
	{
		$user = $this->engine->getUser($u);
		return $ctx->render('admin/user_delete.tpl', array('user' => $user));
	}

	/**
	 * @param ActionContext $ctx
	 */
	public function deleteJson(ActionContext $ctx)
	{
		$json = $ctx->getParam('data');
		$data = json_decode($json, true);
		$u = $data['id'];
		$user = $this->engine->getUser($u);

		$user->delete($ctx->getUser());
		$this->storeSearchQuery($ctx, null);
		$this->storeLimit($ctx, null);

		return $ctx->respondJSON(array('status'=>'success', 'result'=>$user->getId()));
	}

	public function import(ActionContext $ctx)
	{
		return $ctx->render('admin/user_import.tpl');
	}

	/**
	 * インポートファイルのアップロード
	 * @param ActionContext $ctx
	 */
	public function uploadImportFileJson(ActionContext $ctx)
	{
		$this->uploadFile($ctx->getUser(), array(
				'max_file_size' => 100 * 1024 * 1024,
				'accept_file_types' => '/\.(csv)$/i'
		));
	}

	public function importJson(ActionContext $ctx, UserImportForm $form)
	{
		if (!$ctx->getUser()->isAdmin()) {
			return $ctx->respondJSON(array('status'=>'error'));
		}

		$errors = $form->validate($ctx);
		if (is_not_empty($errors)) {
			return $ctx->respondJSON(array('status'=>'error', 'errors' => $errors));
		}
		list ($filePath, ) = $this->getTempFilePath('upload', $form->file);
		$csv = UserCSV::open($filePath, false, $form->charset);
		$recnum = $csv->import($ctx->getUser());
		$csv->close();

		if (is_not_empty($csv->lastErrors())) {
			$this->rollback();
			return $ctx->respondJSON(array('status'=>'error', 'errors' => $csv->lastErrors()));
		}

		$ctx->setUser(Engine::factory()->getUser($ctx->getUser()->getId()));

		return $ctx->respondJSON(array('status'=>'success',
				'message' => new Message(':user_import.success', array($recnum))));
	}

	public function exportFormatJson(ActionContext $ctx, $charset = null)
	{
		list ($filePath) = $this->createTempFile('upload', 'csv');

		$csv = UserCSV::open($filePath, true, $charset);
		$csv->writeHeadingForImport();
		$csv->close();

		$fileName = $fileName = 'ユーザー情報' . '_import_'.date('Ymd').'.csv';

		return $ctx->respondJSON(array('status'=>'success',
				'result' => array ('fileKey' => basename($filePath), 'fileName' => $fileName)
		));
	}

	public function downloadFormatCsv(ActionContext $ctx, $fileKey)
	{
		list($filePath, ) = $this->getTempFilePath('upload', $fileKey);
		$ctx->setHeader('Content-Disposition', 'attachment');
		return $ctx->respondFile($filePath, 'text/csv');
	}

}
