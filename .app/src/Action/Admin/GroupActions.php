<?php
namespace Action\Admin;

use Action\BaseActions;
use Barge\Web\ActionContext;
use Form\Admin\GroupImportForm;
use TxCore\Group;
use TxCore\Message;
use TxCore\TxCoreException;
use TxCore\User;
use Util\GroupCSV;


/**
 * グループ
 */
class GroupActions extends BaseActions
{

	/**
	 * @param ActionContext $ctx
	 * @return bool
	 */
	public function index(ActionContext $ctx)
	{
		if (!$ctx->getUser()->isAdmin()) {
			return $ctx->respondJSON(array('status' => 'error'));
		}
		return $ctx->render('admin/group_index.tpl', array(
			'searchCriteria' => $this->getStoredSearchQuery($ctx)
		));

	}

//
//	/**
//	 * @param ActionContext $ctx
//	 * @return array
//	 */
//	private function getStoredLimit(ActionContext $ctx)
//	{
//		$searchStatus = $ctx->getSession('user_search_limit');
//		if ($searchStatus)
//			return $searchStatus;
//		return array(0, 20);
//	}
//
//
//	/**
//	 * @param ActionContext $ctx
//	 * @param array $limit
//	 */
//	private function storeLimit(ActionContext $ctx, array $limit = null)
//	{
//		$ctx->setSession('user_search_limit', $limit);
//	}
//
	/**
	 * @param ActionContext $ctx
	 * @param array $searchQuery
	 */
	private function storeSearchQuery(ActionContext $ctx, $searchQuery = null)
	{
		$ctx->setSession('group_search_criteria', $searchQuery);
	}

	/**
	 * @param ActionContext $ctx
	 * @return string
	 */
	private function getStoredSearchQuery(ActionContext $ctx)
	{
		$criteria = $ctx->getSession('group_search_criteria');
		if ($criteria)
			return $criteria;
		return array();
	}

	/**
	 * @param ActionContext $ctx
	 * @param string $q keyword.
	 * @param int $criterialUserId
	 * @return bool
	 */
	public function indexJson(ActionContext $ctx, $q = null, $criterialUserId = null)
	{
		if (!$ctx->getUser()->isAdmin()) {
			return $ctx->respondJSON(array('status'=>'error'));
		}

		$list = $this->engine->searchGroups($q, $criterialUserId);
		$this->storeSearchQuery($ctx, array('query' => $q, 'criterialUserId' => $criterialUserId));
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$list));
	}

	/**
	 * @param ActionContext $ctx
	 * @return bool
	 */
	public function create(ActionContext $ctx)
	{
		return $ctx->render('admin/group_modify.tpl', array('groupId' => null));
	}

	/**
	 * @param ActionContext $ctx
	 * @param int $g user id.
	 * @return bool
	 */
	public function modify(ActionContext $ctx, $g = 0)
	{
		return $ctx->render('admin/group_modify.tpl', array('groupId' => $g));
	}

	/**
	 * @param ActionContext $ctx
	 * @param int $g Group id.
	 * @return bool
	 */
	public function loadJson(ActionContext $ctx, $g = 0)
	{
		$group = $this->engine->getGroup($g);
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$group));
	}

	/**
	 * @param ActionContext $ctx
	 * @return bool
	 * @throws \Exception
	 */
	public function modifyJson(ActionContext $ctx)
	{
		$data = $ctx->getParam('data', true);

		$id = $data->id;
		$group = null;
		if ($id == null) { // create
			$group = new Group();
			$group->setName($data->name);
		} else {
			$group = $this->engine->getGroup($id);
			$group->setName($data->name);
		}

		$errors = $group->validate();
		if (is_not_empty($errors)) {
			return $ctx->respondJSON(array('status'=>'error', 'errors'=>$errors));
		}

		try {
			if ($id == null) { // create
				$group->create($ctx->getUser());
				$this->storeSearchQuery($ctx, array());
			} else {
				$group->update($ctx->getUser());
			}
		} catch (TxCoreException $e) {
			if ($e->getCode() == TxCoreException::GROUP_NAME_WAS_DUPLICATED) {
				$this->rollback();
				$errors[] = new Message('入力されたグループ名は既に登録されています。');
				return $ctx->respondJSON(array('status' => 'error', 'errors' => $errors));
			} else {
				throw $e;
			}
		}

		return $ctx->respondJSON(array('status'=>'success',
			'result'=>$group->getId()));
	}

	/**
	 * @param ActionContext $ctx
	 * @param int $g Group id to delete.
	 * @return bool
	 */
	public function delete(ActionContext $ctx, $g = 0)
	{
		$group = $this->engine->getGroup($g);
		return $ctx->render('admin/group_delete.tpl', array('group' => $group));
	}

	/**
	 * @param ActionContext $ctx
	 * @param int $g Group id to delete.
	 * @return bool
	 */
	public function users(ActionContext $ctx, $g = 0)
	{
		$group = $this->engine->getGroup($g);
		return $ctx->render('admin/group_users.tpl', array('group' => $group));
	}

	/**
	 * @param ActionContext $ctx
	 * @return bool
	 */
	public function deleteJson(ActionContext $ctx)
	{
		$data = $ctx->getParam('data', true);
		$group = $this->engine->getGroup($data->id);

		$group->delete($ctx->getUser());
		$this->storeSearchQuery($ctx, null);

		return $ctx->respondJSON(array('status'=>'success', 'result'=>$group->getId()));
	}

	/**
	 * @param ActionContext $ctx
	 * @return bool
	 */
	public function userOptionsJson(ActionContext $ctx)
	{
		$list_ = $this->engine->searchUsers(null, 0, PHP_INT_MAX);
		$result = array_map(function(User $user) {
			return array('id' => $user->getId(), 'name' => $user->getScreenName());
		}, $list_->getRows());
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$result));
	}

	/**
	 * @param ActionContext $ctx
	 * @param $g
	 * @return bool
	 * @throws \Exception
	 */
	public function usersJson(ActionContext $ctx, $g)
	{
		$group = $this->engine->getGroup($g);
		$groupUsers = $group->getUsers();
		return $ctx->respondJSON(array('status'=>'success', 'result'=>$groupUsers));
	}

	/**
	 * @param ActionContext $ctx
	 * @param $groupId
	 * @param array $userIds
	 * @return bool
	 * @throws TxCoreException
	 */
	public function updateUsersJson(ActionContext $ctx, $groupId, $userIds = array())
	{

		//$users = array_maps
		$group = $this->engine->getGroup($groupId);
		$group->replaceUsers_($userIds);

		return $ctx->respondJSON(array('status'=>'success'));
	}


	public function import(ActionContext $ctx)
	{
		return $ctx->render('admin/group_import.tpl');
	}
//
//	/**
//	 * インポートファイルのアップロード
//	 * @param ActionContext $ctx
//	 */
//	public function uploadImportFileJson(ActionContext $ctx)
//	{
//		$this->uploadFile($ctx->getUser(), array(
//				'max_file_size' => 100 * 1024 * 1024,
//				'accept_file_types' => '/\.(csv)$/i'
//		));
//	}
//
	public function importJson(ActionContext $ctx, GroupImportForm $form)
	{
		if (!$ctx->getUser()->isAdmin()) {
			return $ctx->respondJSON(array('status' => 'error'));
		}

		$errors = $form->validate($ctx);
		if (is_not_empty($errors)) {
			return $ctx->respondJSON(array('status' => 'error',
				'errors' => $errors));
		}

		list ($filePath,) = $this->getTempFilePath('upload', $form->file);
		$csv = GroupCSV::open($filePath, false, $form->charset);
		list ($createdCount, $skippedCount) = $csv->import($ctx->getUser());
		$csv->close();

		if (is_not_empty($csv->lastErrors())) {
			$this->rollback();
			return $ctx->respondJSON(array('status' => 'error', 'errors' => $csv->lastErrors()));
		}

		return $ctx->respondJSON(array(
			'status' => 'success',
			'message' => new Message(':group_import.success', array($createdCount,
				$skippedCount))
		));
	}

	public function exportFormatJson(ActionContext $ctx, $charset = null)
	{
		list ($filePath) = $this->createTempFile('upload', 'csv');

		$csv = GroupCSV::open($filePath, true, $charset);
		$csv->writeHeadingForImport();
		$csv->close();

		$fileName = $fileName = 'グループ情報' . '_import_'.date('Ymd').'.csv';

		return $ctx->respondJSON(array('status'=>'success',
				'result' => array ('fileKey' => basename($filePath), 'fileName' => $fileName)
		));
	}
//
//	public function downloadFormatCsv(ActionContext $ctx, $fileKey)
//	{
//		list($filePath, ) = $this->getTempFilePath('upload', $fileKey);
//		$ctx->setHeader('Content-Disposition', 'attachment');
//		return $ctx->respondFile($filePath, 'text/csv');
//	}

}
