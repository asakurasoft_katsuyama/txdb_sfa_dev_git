<?php
namespace Action;


use Util\TempDir;

use Form\Login\LoginForm;

use Barge\Web\User;

use TxCore\TxCoreException;

use Util\Message;
use Util\UserAdaptor;

use Barge\Web\ActionContext;

use Barge\Web\Actions;
use Barge\Web\ActonContext;

/**
 * ログイン
 */
class LoginActions extends BaseActions
{
	/**
	 * @param ActionContext $ctx
	 * @param string $action
	 */
	public function checkLogin(ActionContext $ctx, $action)
	{
		return true;
	}

	/**
	 * @param ActionContext $ctx
	 * @param LoginForm $form
	 */
	public function index(ActionContext $ctx, LoginForm $form)
	{
		$form->saveLogin = ($ctx->getCookie('login_saveLogin') != null);
		$form->login = trim($ctx->getCookie('login_saveLogin'));
		return $ctx->render('login/login.tpl');
	}

	/**
	 * @param ActionContext $ctx
	 * @param LoginForm $form
	 */
	public function authenticate(ActionContext $ctx, LoginForm $form)
	{
		$messages = $this->validateLogin($form);
		if (is_not_empty($messages)) {
			$ctx->saveMessages($messages);
			return $ctx->render('login/login.tpl');
		}

		$user =null;
		try {
			$user = $this->engine->authenticateUser($form->login, $form->password);
		} catch (TxCoreException $e) {
			$ctx->saveMessages(array(new Message(null, 'ログインに失敗しました。')));
			return $ctx->render('login/login.tpl');
		}

		$ctx->setCookie('login_saveLogin', $user->getLoginId(), $form->saveLogin ? strtotime('+30 days') : strtotime('-100 days'));
		$ctx->setUser($user);
		$this->removeTempFiles("upload", 60 * 60 * 24);	// 24時間経過したものを消す
		$this->removeTempFiles("thumb", 60 * 60 * 24);	// 24時間経過したものを消す

		$bookmarked = $ctx->getSession('bookmarked_url');
		if ($bookmarked) return $ctx->redirect($bookmarked);
		return $ctx->redirect('/my/');
	}

	/**
	 * Validation for login form.
	 * @param LoginForm $form
	 */
	private function validateLogin(LoginForm $form)
	{
		$messages = array();
		if (is_empty($form->login)) {
			$messages[] = new Message('login', 'ログインIDを入力してください。');
		}
		if (is_empty($form->password)) {
			$messages[] = new Message('password', 'パスワードを入力してください。');
		}
		return $messages;
	}
}
