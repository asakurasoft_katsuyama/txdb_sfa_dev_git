<?php 
namespace Action;

use TxCore\TxCoreException;
use TxCore\Util;

use \Exception;
use \PDOException;
use Barge\Web\ActionContext;

class ErrorHandler 
{
	public function handle(ActionContext $ctx, Exception $e) 
	{
		$trace = format_exception($e);
		log_error($trace);
		
		$errors = array(array('message'=>$this->convertMessage($e)));
		
		if (Util::isJSONRequest($ctx->getRequestURI())) {
			http_response_code(500);
			return $ctx->respondJSON(array('errors'=>$errors));
		} else {
			http_response_code(500);
			return $ctx->render('error.tpl', array('errors'=>$errors));
// 			virtual('/error/error.html');
		}
	}
	
	private function convertMessage($e) 
	{
		if ($e instanceof TxCoreException) {
			if ($e->getCode() == TxCoreException::PERMISSION_DENIED) {
				return get_message('global.permission_error');
			} else if ($e->getCode() == TxCoreException::ROW_NOT_FOUND) {
				return get_message('global.row_not_found');
			} else if ($e->getCode() == TxCoreException::LOCK_FAILED) {
				return get_message('global.lock_failed');
			} else {
				return get_message('global.unknown_error');
			}
		} 
		
		
		return get_message('global.unknown_error');
	}
}

