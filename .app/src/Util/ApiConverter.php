<?php
namespace Util;

use TxCore\Filter;

use TxCore\Condition;
use TxCore\Sort;
use TxCore\Criteria;
use TxCore\LookupValue;
use TxCore\Table;
use TxCore\Template;
use TxCore\Message;
use TxCore\File;
use TxCore\UploadedFile;
use TxCore\Row;
use TxCore\Option;
use TxCore\Column;
use TxCore\User;
use TxCore\Icon;
use TxCore\List_;
use TxCore\Engine;
use TxCore\Column__Calc;

use JSON\TypeConverter;

/**
 * @see TypeConverter
 */
class ApiConverter implements TypeConverter
{

	public function convert($value, $parent)
	{
		if ($value === null) {
			return $value;
		} else if ($value instanceof User) {
			return array(
				'id' => $value->getId(),
				'code' => $value->getLoginId(),
				'screenName' => $value->getScreenName(),
			);
		} else if ($value instanceof Icon) {
			return array(
				'filekey' => $value->getFilekey(),
				'url' => $value->getUrl(),
			);
		} else if ($value instanceof List_ && $value->getType() === 'table') {
			$columns = array();
			foreach ($value->getColumns() as $column) {
				$columns[] = array(
					'id' => $column->getId(),
					'type' => $column->getType(),
					'label' => $column->getLabel(),
					'code' => $column->getCode(),
				);
			}
			$rows = array();
			foreach ($value->getRows() as $row) {
				$rowValues = array();
				foreach ($value->getColumns() as $column) {
					$_value;
					if ($column->getType() == Column::CALC) {
						if ($column->getFormat() == Column__Calc::FORMAT_NUMBER ||
							$column->getFormat() == Column__Calc::FORMAT_NUMBER_WITH_COMMA) {
							$_value = $row->getValue($column);
						} else {
							$_value = $row->getText($column);
						}
					} else {
						$_value = $row->getValue($column);
					}
					$rowValues[$column->getCode()] = $_value;
				}
				$rows[] = array(
						'id'=>$row->getId(),
						'no'=>$row->getRowNo(),
						'values'=>$rowValues,
						'version'=>$row->getVersion()
				);
			}

			return array(
				'totalRowCount' => $value->getTotalRowCount(),
				'offset' => $value->getOffset(),
				'rowLimit' => $value->getRowLimit(),
				'rowCount' => count($value->getRows()),
				'columns' =>$columns,
				'rows' => $rows
			);
		} else if ($value instanceof Option) {
			return $this->convertOption($value);
		} else if ($value instanceof UploadedFile) {
			return $this->convertUploadedFile($value);
		} else if ($value instanceof File) {
			return $this->convertFile($value);
		} else if ($value instanceof LookupValue) {
			return $this->convertLookup($value);
		} else if ($value instanceof Message) {
			return $this->convertMessage($value);
		} else {
			return $value;
		}
	}

	private function convertFile(File $value)
	{
		return array(
			'fileName' => $value->getFileName(),
			'url' => concat_path(get_config('app', 'myhost'), $value->getUrl()),
			'thumbnailUrl' => $this->getThumbnailUrl($value),
			'filekey' => $value->getFilekey(),
		);
	}

	private function convertUploadedFile(UploadedFile $value)
	{
		return array(
			'filekey' => $value->getFilekey(),
		);
	}

	private function getThumbnailUrl(File $file)
	{
		if (preg_match(get_config('app', 'file_inline'), $file->getContentType()) === 1) {
			if ($file->getSize() <= get_config('app', 'thumbnail_original_size_limit') ) {
				$storage = Engine::factory()->getStorage();
				if (($filePath = $storage->getFilePath($file->getFilekey(), true)) === null) return null;
				list($width, $height) = getimagesize($filePath);
				$limit = get_config('app', 'thumbnail_original_imagesize_limit');
				if ($width <= $limit && $height <= $limit) {
					return concat_path(get_config('app', 'myhost'), $file->getUrl().'?t'.$file->getParent()->getThumbnailMaxSize());
				}
			}
		}
		return null;
	}

	private function convertTable(Table $value)
	{
		return array(
			'id' => $value->getId(),
			'name' => $value->getName(),
			'description' => $value->getDescription(),
			'icon' => $value->getIcon(),
			'isPublic' => $value->isPublic(),
			'hasPendingRelease' => $value->hasPendingRelease(),
			'isDefault' => null,
			'isApiEnabled' => $value->isApiEnabled(),
			'apiKey' => $value->getApiKey(),
			'owner' => $value->getOwner(),
			'roundType' => $value->getRoundType(),
			'roundScale' => $value->getRoundScale(),
		);
	}

// 	http://localhost:8003/api/search.json?table=85511&api_key=a36f777f64b56d1343716e402ebf6caae3550e7d&columns=%E6%95%B0%E5%80%A4,%E6%99%82%E5%88%BB_1
// 	&sorts[0][column]=%E6%95%B0%E5%80%A4&sorts[0][reverse]=true
// 	&conditions[0][column]=%E3%83%AC%E3%82%B3%E3%83%BC%E3%83%89%E7%95%AA%E5%8F%B7
// 	&conditions[0][operator]=like&conditions[0][value]=1&callback=f

	private function convertMessage(Message $value)
	{
		$data = array();
		$data['context'] = $value->getContext();
		$data['message'] = $value->getMessage();
		return $data;
	}

	private function convertLookup(LookupValue $value)
	{
		return array(
			'key' => $value->getKey(),
			'value' => $value->getValue(),
		);
	}

	private function convertOption(Option $value)
	{
		if ($value->_isDeleted()) {
			return null;
		}

		return array(
			'id' => $value->getId(),
			'name' => $value->getName(),
		);
	}

}
