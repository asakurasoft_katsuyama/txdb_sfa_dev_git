<?php
namespace Util;


use TxCore\Column;
use TxCore\Group;
use TxCore\Table;
use TxCore\User;

use TxCore\Message;

use TxCore\Engine;

class GroupColumn
{
	private $id = null;
	private $label = null;

	public function _setId($value)
	{
		$this->id = $value;
	}

	public function getId()
	{
		return $this->id;
	}

	public function _setLabel($value)
	{
		$this->label = $value;
	}

	public function getLabel()
	{
		return $this->label;
	}
}

/**
 * Import/export helper.
 */
class GroupCSV
{
	/**
	 * @var resource
	 */
	private $fp;

	private $fieldPos = 0;

	/**
	 * @var Table
	 */
	private $charset = 'utf-8';
	private $lineFeed = null;
	private $errors = array();

	/**
	 * @param string $filepath
	 * @param bool|string $create
	 * @param string $charset
	 * @return CSV
	 */
	public static function open($filepath, $create = false, $charset = 'utf-8')
	{
		return new self($filepath, $create, $charset);
	}


	/**
	 * @param $filepath
	 * @param $create
	 * @param $charset
	 */
	private function __construct($filepath, $create, $charset)
	{
		$this->charset = $charset ?: 'utf-8';
		if (strtolower($charset) == 'shift_jis') {
			$this->charset = 'SJIS-win';
		}
		$this->lineFeed = $this->getLfCode($this->charset);

		if (!$create) {
			$locale = get_config('app', 'csv.locale');
			if ($locale) {
				setlocale(LC_ALL, $locale);
			}

			$sjis = strtolower($charset) == 'shift_jis';
			$fromExt = $sjis ? 'sjis' : 'utf8';
			$toExt = 'utf8';
			$outFilepath = $filepath.'.'.$fromExt.'.'.$toExt;
			$workFilepath = $outFilepath . '.work';
			if (!file_exists($outFilepath)) {
				$fp_in = fopen($filepath, "r");
				$fp_out = fopen($workFilepath, "w");
				while ($line = fgets($fp_in)) {
					if ($sjis) $line = mb_convert_encoding($line, 'utf8', 'SJIS-win');
					fputs($fp_out, preg_replace("/(\r\n|\r|\n)/", "\r\n", $line));
				}
				fclose($fp_out);
				fclose($fp_in);
				rename($workFilepath, $outFilepath);
			}
			$filepath = $outFilepath;
		}

		$this->fp = fopen($filepath, $create ? 'w' : 'r');
	}

	public function writeHeadingForImport()
	{
		$fields = $this->getImportRequiredFields();
		$this->writeHeading($fields, true);
	}

	public function lastErrors()
	{
		return $this->errors;
	}

	public function import(User $by)
	{
		$createdCount = 0;

		$heading = $this->readHeading();
		if ($heading === false) {
			$this->errors[] = new Message(':group_import.file.invalid');
			return false;	// Invalid heading.
		}
		$columns = $this->getImportRequiredFields();

		// 必須カラムのチェック
		$matches = $this->matchImportFields($heading, $columns);
		if ($matches === false) {
			$this->errors[] = new Message(':group_import.file.invalid');
			return false;	// Invalid heading.
		}

		$line = 2;
		$skippedCount = 0;
		while (($values = $this->readFileds()) !== false) {
			$line++;
			$name = $values[$matches[0]];	// 0列はログインid

			if (is_empty($name)) {
				// カラムが１つしかなので、必須チェックはせず読み飛ばす
				continue;
			}

			$group = Engine::factory()->findGroupByName($name);
			if ($group !== null) {
				$skippedCount++;
				continue;
			}

			$group = new Group();
			$group->setName($name);

			// Validate.
			$errors = $group->validate();
			if (is_not_empty($errors)) {
				$this->errors = $this->wrapMessages($errors, $line);
				break;
			}

			$group->create($by);
			$createdCount++;
		}
		return array($createdCount, $skippedCount);
	}

	private function wrapMessages($messages, $line)
	{
		foreach ($messages as $message) 	/* @var $message Message */
			$message->setMessage($line.'行目 : ' . $message->getMessage());
		return $messages;
	}

	/**
	 * @param $keys
	 * @param GroupColumn[] $fields
	 * @return array|bool
	 */
	private function matchImportFields($keys, $fields)
	{
		$matches = array();
		for ($i = 0; $i < count($fields); $i++) {
			$column = $fields[$i];
			$found = -1;
			for ($j = 0; $j < count($keys); $j++) {
				if ($column->getId() == $keys[$j][0]) {
					$found = $j;
					break;
				}
			}
			if ($found == -1) {
				return false;
			}
			$matches[$i] = $found;
		}
		return $matches;
	}

	/**
	 * @param GroupColumn[] $fields
	 * @param bool $keyLines
	 */
	private function writeHeading($fields, $keyLines = false)
	{
		foreach ($fields as $column) {
			$this->_writeField($column->getLabel());
		}
		$this->_writeLf();
		if ($keyLines) {
			foreach ($fields as $column) {	/* @var $column Column */
				$this->_writeField("key=".$column->getId());
			}
			$this->_writeLf();
		}
	}

	private function readHeading()
	{
		$fields = $this->readFileds();
		if ($fields === false) {
			return false;
		}
		$fields = $this->readFileds();
		if ($fields === false) {
			return false;
		}

		if (!str_starts_with('key=', $fields[0])) {
			return false;
		}

		return array_map(function($keystr) {
			return explode('_', substr($keystr, strpos($keystr, '=') + 1).'_');
		}, $fields);
	}


	public function _writeField($field)
	{
		$buff = null;
		if ($this->fieldPos > 0) {
			$buff .= ',';
		}
		if (is_int($field) || is_float($field)|| is_double($field)) {
			$buff .= $field;
		} else {
			$buff .= '"' .str_replace('"', '""', $this->encode($field)) . '"';
		}
		fwrite($this->fp, $buff);
		$this->fieldPos++;
	}

	private function readFileds()
	{
// 		setlocale(LC_ALL,'ja_JP.UTF-8');
		$fields = fgetcsv($this->fp);
		if ($fields === false || $fields == array(null))
			return false;
		$encoded = array();
		foreach ($fields as $field) {
			$encoded[] = $this->decode($field);
		}
		return $encoded;
	}

	public function _writeLf()
	{
		fwrite($this->fp, $this->lineFeed);
		$this->fieldPos = 0;
	}

	public function close()
	{
		if ($this->fp != null) {
			fclose($this->fp);
			$this->fp = null;
		}
	}

	private function decode($value)
	{
		return $value;
	}

	/**
	 * @param string $value
	 * @return string
	 */
	private function encode($value)
	{
		if (strtolower($this->charset) == 'utf-8')
			return $value;
		return mb_convert_encoding($value, $this->charset, 'utf-8');
	}

	/**
	 * @param string $charset
	 * @return string
	 */
	private function getLfCode($charset)
	{
		if (strtolower($charset) == 'shift_jis') {
			return "\r\n";
		} else if (strtolower($charset) == 'sjis-win') {
			return "\r\n";
		} else if (strtolower($charset) == 'utf-8') {
			return "\n";
		} else {
			die($charset);
		}
	}

	/**
	 * @return GroupColumn[]
	 * @internal param Table $table
	 */
	public static function getImportRequiredFields()
	{
		$fields = array(
			array('id' => 'name', 'label' => 'グループ名'),
		);

		$columns = array();
		foreach ($fields as $field) {
			$column = new GroupColumn();
			$column->_setId($field['id']);
			$column->_setLabel($field['label']);
			$columns[] = $column;
		}

		return $columns;

	}

}

