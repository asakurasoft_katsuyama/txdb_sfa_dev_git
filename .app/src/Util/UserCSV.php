<?php
namespace Util;

use TxCore\TxCoreException;

use TxCore\User;

use TxCore\Message;

use TxCore\Engine;

class UserColumn
{
	private $id = null;
	private $label = null;

	public function _setId($value)
	{
		$this->id = $value;
	}

	public function getId()
	{
		return $this->id;
	}

	public function _setLabel($value)
	{
		$this->label = $value;
	}

	public function getLabel()
	{
		return $this->label;
	}
}

/**
 * Import/export helper.
 */
class UserCSV
{
	/**
	 * @var resource
	 */
	private $fp;

	private $fieldPos = 0;

	/**
	 * @var Table
	 */
	private $charset = 'utf-8';
	private $lineFeed = null;
	private $fields = array();
	private $headers = null;
	private $errors = array();

	/**
	 * @param string $filepath
	 * @param string $create
	 * @return \Util\CSV
	 */
	public static function open($filepath, $create = false, $charset = 'utf-8')
	{
		return new self($filepath, $create, $charset);
	}


	private function __construct($filepath, $create, $charset)
	{
		$this->charset = $charset ?: 'utf-8';
		if (strtolower($charset) == 'shift_jis') {
			$this->charset = 'SJIS-win';
		}
		$this->lineFeed = $this->getLfCode($this->charset);

		if (!$create) {
			$locale = get_config('app', 'csv.locale');
			if ($locale) {
				setlocale(LC_ALL, $locale);
			}

			$sjis = strtolower($charset) == 'shift_jis';
			$fromExt = $sjis ? 'sjis' : 'utf8';
			$toExt = 'utf8';
			$outfilepath = $filepath.'.'.$fromExt.'.'.$toExt;
			$workfilepath = $outfilepath . '.work';
			if (!file_exists($outfilepath)) {
				$fp_in = fopen($filepath, "r");
				$fp_out = fopen($workfilepath, "w");
				while ($line = fgets($fp_in)) {
					if ($sjis) $line = mb_convert_encoding($line, 'utf8', 'SJIS-win');
					fputs($fp_out, preg_replace("/(\r\n|\r|\n)/", "\r\n", $line));
				}
				fclose($fp_out);
				fclose($fp_in);
				rename($workfilepath, $outfilepath);
			}
			$filepath = $outfilepath;
		}

		$this->fp = fopen($filepath, $create ? 'w' : 'r');
	}

	public function writeHeadingForImport()
	{
		$fields = $this->getImportRequiredFields();
		$this->writeHeading($fields, true);
	}

// 	/**
// 	 * @param unknown_type $heading
// 	 * @param unknown_type $useImport
// 	 * @return callable Row writer
// 	 */
// 	public function export($heading, $useImport)
// 	{
// 		$fields = $this->getExportFields($this->table);
// 		if ($heading || $useImport) {
// 			$this->writeHeading($fields, $useImport);
// 		}
// 		$this_ = $this;
// 		return function(Row $row) use ($this_, $fields) {
// 			foreach ($fields as $column) {
// 				if ($column->getType() == Column::CHECKBOX) {
// 					foreach ($column->getOptions() as $option) {
// 						$found = array_grep($row->getValue($column), function($value) use ($this_, $option){
// 							return $value->getId() ==  $option->getId();
// 						});
// 						$this_->_writeField(is_empty($found) ? "0" : "1");
// 					}
// 				} else if ($column->getType() == Column::DROPDOWN ||
// 					$column->getType() == Column::RADIO) {
// 					$this_->_writeField($row->getText($column));
// 				} else if ($column->getType() == Column::LOOKUP) {
// 					$value = $row->getValue($column);
// 					$this_->_writeField($value ? $value->getValue() : "");
// 				} else if ($column->getType() == Column::CREATED_BY ||
// 					$column->getType() == Column::UPDATED_BY) {
// 					$value = $row->getValue($column);
// 					$this_->_writeField($value ? $value->getLoginId() : null);
// 				} else {
// 					$this_->_writeField($row->getValue($column));
// 				}
// 			}
// 			$this_->_writeLf();
// 		};
// 	}

	public function lastErrors()
	{
		return $this->errors;
	}

	public function import(User $_user)
	{
		$userLimit = get_config('app', 'user_num_limit');
		$userCount = Engine::factory()->countUsers();
		$createdCount = 0;

		$heading = $this->readHeading();
		if ($heading === false) {
			$this->errors[] = new Message(':user_import.file.invalid');
			return false;	// Invalid heading.
		}
		$columns = $this->getImportRequiredFields();

		// 必須カラムのチェック
		$matches = $this->matchImportFields($heading, $columns);
		if ($matches === false) {
			$this->errors[] = new Message(':user_import.file.invalid');
			return false;	// Invalid heading.
		}

		$line = 2;
		while (($values = $this->readFileds()) !== false) {
			$line++;
			$loginId = $values[$matches[0]];	// 0列はログインid

			if (is_empty($loginId)) {
				$this->errors[] = new Message(':user_import.invalid_loginId', array($line, $column->getLabel()));
				break;
			}
			$user = Engine::factory()->findUserByLoginId($loginId);
			if ($user === null) {
				if ($userLimit > 0 && $userCount + $createdCount >= $userLimit) {
					$this->errors[] = new Message(':user_import.exceed_max_records', array($line));
					return false;
				}
				$user = new User();
				$user->setRole(User::USER);
			}

			$user->setLoginId($values[$matches[0]]);
			$user->setPassword($values[$matches[1]]);
			$user->setScreenName($values[$matches[2]]);
			$user->setProfile($values[$matches[3]]);

			if (is_not_empty($this->errors)) {
				break;
			}

			// Validate.
			log_debug(var_export($user,true));
			$errors = $user->validate();
			if (is_not_empty($errors)) {
				$this->errors = $this->wrapMessages($errors, $line);
				break;
			}


			if ($user->getId()) {
				$user->update($_user);
			} else {
				$user->create($_user);
				$createdCount++;
			}
		}
		return $line - 2;
	}

	private function wrapMessages($messages, $line)
	{
		foreach ($messages as $message) 	/* @var $message Message */
			$message->setMessage($line.'行目 : ' . $message->getMessage());
		return $messages;
	}

	private function matchImportFields($keys, $fields)
	{
		$matches = array();
		for ($i = 0; $i < count($fields); $i++) {
			$column = $fields[$i];
			$found = -1;
			for ($j = 0; $j < count($keys); $j++) {
				if ($column->getId() == $keys[$j][0]) {
					$found = $j;
					break;
				}
			}
			if ($found == -1) {
				return false;
			}
			$matches[$i] = $found;
		}
		return $matches;
	}

	private function writeHeading($fields, $keyLines = false)
	{
		foreach ($fields as $column) {
			$this->_writeField($column->getLabel());
		}
		$this->_writeLf();
		if ($keyLines) {
			foreach ($fields as $column) {	/* @var $column Column */
				$this->_writeField("key=".$column->getId());
			}
			$this->_writeLf();
		}
	}

	private function readHeading()
	{
		$fields = $this->readFileds();
		if ($fields === false) {
			return false;
		}
		$fields = $this->readFileds();
		if ($fields === false) {
			return false;
		}

		if (!str_starts_with('key=', $fields[0])) {
			return false;
		}

		return array_map(function($keystr) {
			return explode('_', substr($keystr, strpos($keystr, '=') + 1).'_');;
		}, $fields);
	}


	public function _writeField($field)
	{
		$buff = null;
		if ($this->fieldPos > 0) {
			$buff .= ',';
		}
		if (is_int($field) || is_float($field)|| is_double($field)) {
			$buff .= $field;
		} else {
			$buff .= '"' .str_replace('"', '""', $this->encode($field)) . '"';
		}
		fwrite($this->fp, $buff);
		$this->fieldPos++;
	}

	private function readFileds()
	{
// 		setlocale(LC_ALL,'ja_JP.UTF-8');
		$fields = fgetcsv($this->fp);
		if ($fields === false || $fields == array(null))
			return false;
		$encoded = array();
		foreach ($fields as $field) {
			$encoded[] = $this->decode($field);
		}
		return $encoded;
	}

	public function _writeLf()
	{
		fwrite($this->fp, $this->lineFeed);
		$this->fieldPos = 0;
	}

	public function close()
	{
		if ($this->fp != null) {
			fclose($this->fp);
			$this->fp = null;
		}
	}

	private function decode($value)
	{
		return $value;
	}

	/**
	 * @param string $value
	 */
	private function encode($value)
	{
		if (strtolower($this->charset) == 'utf-8')
			return $value;
		return mb_convert_encoding($value, $this->charset, 'utf-8');
	}

	/**
	 * @param string $charset
	 * @return string
	 */
	private function getLfCode($charset)
	{
		if (strtolower($charset) == 'shift_jis') {
			return "\r\n";
		} else if (strtolower($charset) == 'sjis-win') {
			return "\r\n";
		} else if (strtolower($charset) == 'utf-8') {
			return "\n";
		} else {
			die($charset);
		}
	}

// 	/**
// 	 * エクスポート可能なカラムの一覧を返す
// 	 * @param Table $table
// 	 * @return Column[]
// 	 */
// 	public static function getExportFields(Table $table)
// 	{
// 		$heading = $table->getColumns(array('type' => Column::ROWNUM));
// 		$succedent = array_filter($table->getColumns(), function(Column $column) {
// 			return $column->isListable() && $column->getType() !== Column::FILE && $column->getType() !== Column::ROWNUM;
// 		});
// 		return array_merge($heading, $succedent);
// 	}

	/**
	 * @param Table $table
	 * @return Column[]
	 */
	public static function getImportRequiredFields()
	{
		$fields = array(
			array('id' => 'loginId', 'label' => 'ログインid'),
			array('id' => 'password', 'label' => 'パスワード'),
			array('id' => 'screenName', 'label' => '表示名'),
			array('id' => 'profile', 'label' => '所属等'),
		);

		$columns = array();
		foreach ($fields as $field) {
			$column = new UserColumn();
			$column->_setId($field['id']);
			$column->_setLabel($field['label']);
			$columns[] = $column;
		}

		return $columns;

	}

}

