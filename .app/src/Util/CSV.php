<?php
namespace Util;

use TxCore\Engine;
use TxCore\TxCoreException;

use TxCore\LookupValue;

use TxCore\User;

use TxCore\Message;

use TxCore\Table;

use TxCore\Column;
use TxCore\Criteria;
use TxCore\Row;
use TxCore\Option;
use TxCore\Column__Calc;

/**
 * Import/export helper.
 */
class CSV
{
	/**
	 * @var resource
	 */
	private $fp;

	private $fieldPos = 0;

	private $recordNum = null;

	/**
	 * @var Table
	 */
	private $table;

	private $charset = 'utf-8';
	private $lineFeed = null;
	private $fields = array();
	private $headers = null;
	private $errors = array();

	/**
	 * @param string $filepath
	 * @param string $create
	 * @return \Util\CSV
	 */
	public static function open(Table $table, $filepath, $create = false, $charset = 'utf-8')
	{
		return new self($table, $filepath, $create, $charset);
	}


	private function __construct(Table $table, $filepath, $create, $charset)
	{
		$this->table = $table;
		$this->charset = $charset ?: 'utf-8';
		if (strtolower($charset) == 'shift_jis') {
			$this->charset = 'SJIS-win';
		}
		$this->lineFeed = $this->getLfCode($this->charset);

		if (!$create) {
			$locale = get_config('app', 'csv.locale');
			if ($locale) {
				setlocale(LC_ALL, $locale);
			}

			$sjis = strtolower($charset) == 'shift_jis';
			$fromExt = $sjis ? 'sjis' : 'utf8';
			$toExt = 'utf8';
			$outfilepath = $filepath.'.'.$fromExt.'.'.$toExt;
			$workfilepath = $outfilepath . '.work';
			if (!file_exists($outfilepath)) {
				$fp_in = fopen($filepath, "r");
				$fp_out = fopen($workfilepath, "w");
				while ($line = fgets($fp_in)) {
					if ($sjis) $line = mb_convert_encoding($line, 'utf8', 'SJIS-win');
					fputs($fp_out, preg_replace("/(\r\n|\r|\n)/", "\r\n", $line));
				}
				fclose($fp_out);
				fclose($fp_in);
				rename($workfilepath, $outfilepath);
			}
			$filepath = $outfilepath;
		}

		$this->fp = fopen($filepath, $create ? 'w' : 'r');
		if (!$create) {
			$this->recordNum = $this->countRecords();
		}
	}

	private function countRecords()
	{
		rewind($this->fp);
		$count = 0;
		$heading = $this->readHeading();
		if ($heading === false) {
			return $count;	// Invalid heading.
		}

		while (($values = $this->readFileds()) !== false) {
			$count++;
		}
		rewind($this->fp);

		return $count;
	}

	public function getRecordNum()
	{
		return $this->recordNum;
	}

	public function writeHeadingForImport()
	{
		$fields = $this->getImportRequiredFields($this->table);
		$this->writeHeading($fields, true);
	}

	/**
	 * @param bool $heading
	 * @param bool $useImport
	 * @return callable Row writer
	 */
	public function export($heading, $useImport)
	{
		$fields = $this->getExportFields($this->table);
		if ($heading || $useImport) {
			$this->writeHeading($fields, $useImport);
		}
		$this_ = $this;
		return function(Row $row) use ($this_, $fields) {
			foreach ($fields as $column) {
				if ($column->getType() == Column::CHECKBOX) {
					foreach ($column->getOptions() as $option) {
						$found = array_grep($row->getValue($column), function($value) use ($this_, $option){
							return $value->getId() ==  $option->getId();
						});
						$this_->_writeField(is_empty($found) ? "0" : "1");
					}
				} else if ($column->getType() == Column::DROPDOWN ||
					$column->getType() == Column::RADIO) {
					$option = $row->getValue($column);
					$this_->_writeField(!$option || $option->_isDeleted() ? "" : $row->getText($column));
				} else if ($column->getType() == Column::USER_SELECTOR) {
					$usersNames = array();
					foreach ($row->getValue($column) as $user) /** @var $user User */
						if (!$user->_isDeleted())
							$usersNames[] = $user->getLoginId();
					$this_->_writeField(join("\n", $usersNames));	// LF固定
				} else if ($column->getType() == Column::LOOKUP) {
					$value = $row->getValue($column);
					$this_->_writeField($value ? $value->getValue() : "");
				} else if ($column->getType() == Column::CALC) {
					if ($column->getFormat() == Column__Calc::FORMAT_NUMBER ||
						$column->getFormat() == Column__Calc::FORMAT_NUMBER_WITH_COMMA) {
						$this_->_writeField($row->getValue($column));
					} else {
						$this_->_writeField($row->getText($column));
					}
				} else if ($column->getType() == Column::DATECALC) {
					$this_->_writeField($row->getText($column));
				} else if ($column->getType() == Column::LOOKUP) {
					$value = $row->getValue($column);
					$this_->_writeField($value ? $value->getValue() : "");
				} else if ($column->getType() == Column::CREATED_BY ||
					$column->getType() == Column::UPDATED_BY) {
					$value = $row->getValue($column);
					$this_->_writeField($value ? $value->getLoginId() : null);
				} else {
					$this_->_writeField($row->getValue($column));
				}
			}
			$this_->_writeLf();
		};
	}

	public function lastErrors()
	{
		return $this->errors;
	}

	public function import(User $by, Column $keyColumn)
	{
		$heading = $this->readHeading();
		if ($heading === false) {
			$this->errors[] = new Message(':data_import.file.invalid');
			return false;	// Invalid heading.
		}
		$columns = $this->getImportRequiredFields($this->table);

		// 必須カラムのチェック
		$matches = $this->matchImportFields($heading, $columns);
		if ($matches === false) {
			$this->errors[] = new Message(':data_import.file.invalid');
			return false;	// Invalid heading.
		}

		// レコード数チェック
		if ($this->getRecordNum() > get_config('app','data_import_num_limit')) {
			$this->errors[] = new Message(':data_import.file.exceed_max_records');
			return false;	// Invalid heading.
		}

		// 更新キーフィールドが$columnsの何番目であるか
		$keyidx = null;
		if ($keyColumn->getType() != Column::ROWNUM) {
			foreach($columns as $idx => $column){
				if ($column->getId() == $keyColumn->getId()) {
					$keyidx = $idx;
				}
			}
		}

		// ロック
		$this->table->lock();

		$line = 2;
		while (($values = $this->readFileds()) !== false) {
			$line++;
			$rownum = null;
			$row = null;
			if ($keyColumn->getType() == Column::ROWNUM) {
				$rownum = $values[$matches[0]];	// 0列はrownum
				$row = is_empty($rownum) ? $this->table->addRow() : $this->table->getRowFor($rownum, null, true, true);
			} else { // 更新キーがデータ番号フィールド以外の場合
				$keyval = $values[$matches[$keyidx]];
				$values[$matches[0]] = null; //CSVのrownumを上書き
				if (is_not_empty($keyval)) {
					$criteria = new Criteria();
					//$criteria->setListColumns($this->table->getColumns());
					$criteria->addCondition($keyColumn, Criteria::EQUALS, $keyval);
					$result = $this->table->search($criteria, $by, 0, 1);
					$rows = $result->getRows();
					if (count($rows) == 1) {
						$row = $rows[0];
						$rownum = $row->getRownum();
						$values[$matches[0]] = $rownum; //CSVのrownumを上書き
					}
				}
				if ($row === null) {
					$row = $this->table->addRow();
				}
			}
			if ($row === null) {
				$this->errors[] = new Message(':data_import.invalid_rownum', array($line, $columns[0]->getLabel()));
				break;
			}

			for ($i = 0; $i < count($columns); $i++) {
				$column = $columns[$i];	/* @var $column Column */
				if ($column->getType() == Column::CHECKBOX) {
					$options = array();
					for ($k = 0; $k < count($column->getOptions()); $k++) {
						$value = $values[$matches[$i][$k]];
						if (!in_array($value, array('1', '0', ''))) {
							$this->errors[] = new Message(':data_import.invalid_checkbox', array(
								$line, $column->getLabel()
							));
						}
						if ($value == '1') {
							$options[] = $column->getOption($k);
						}
					}
					$row->setValue($column, $options);
				} else if ($column->getType() == Column::USER_SELECTOR) {
					$users = array();
					$value = $values[$matches[$i]];
					foreach (preg_split("/\r?\n/", $value) as $loginId) {
						if (strlen($loginId) === 0)
							continue;
						$user = Engine::factory()->findUserByLoginId($loginId);
						if ($user === null) {
							$this->errors[] = new Message(':data_import.invalid_user_selector', array(
								$line, $column->getLabel()
							));
						}
						$users[] = $user;
					}
					$row->setValue($column, $users);
				} else if ($column->getType() == Column::LOOKUP) {
					$value = $values[$matches[$i]];

					if (is_not_empty($value)) {
						$lookup = new LookupValue();
						$lookup->setValue($value);
						$row->setValue($column, $lookup);

						try {
							$hits = $row->serachReferal($column, $by, null, 0);
							if ($hits->getTotalRowCount() != 1) {
								$this->errors[] = new Message(':data_import.lookup_failed', array($line, $column->getLabel()));
								break;
							}

							// Set key
							$lookup->setKey($hits[0]->getId());

							// Copy reference-copy field.
							foreach ($column->getReferenceCopyMap() as $pair) {
								$row->setValue($pair[1], $this->getLookupCopyValue($pair, $hits[0]));
							}

						} catch (TxCoreException $e) {
							// 権限エラー
							if ($e->getCode() === TxCoreException::PERMISSION_DENIED) {
								$this->errors[] = new Message(':data_import.lookup_failed', array($line, $column->getLabel()));
								break;
							} else {
								throw $e;
							}
						}
					} else {
						// clear lookup
						$lookup = new LookupValue();
						$lookup->setValue($value);
						$row->setValue($column, $lookup);
						$lookup->setKey(null);
						foreach ($column->getReferenceCopyMap() as $pair) {
							$row->setValue($pair[1], null);
						}
					}

				} else if ($column->hasOptions()) {
					$value = $values[$matches[$i]];
					if (is_not_empty($value)) {
						$matchedOptions = $column->getOptions(array('name'=>$value));
						if (is_empty($matchedOptions)) {
							$this->errors[] = new Message(':data_import.invalid_option', array($line, $column->getLabel()));
							break;
						}
						$row->setValue($column, $matchedOptions[0]);
					} else {
						$row->setValue($column, null);
					}
				} else {
					$value = $values[$matches[$i]];
					$row->setValue($column, $value);
				}
			}

			if (is_not_empty($this->errors)) {
				break;
			}

			// Validate.
			$errors = $row->validate($by);
			if (is_not_empty($errors)) {
				$this->errors = $this->wrapMessages($errors, $line);
				break;
			}

			$rownum ? $row->update($by) :
				$this->table->insertRows($by);
		}
		return $line - 2;
	}

	private function getLookupCopyValue($pair, Row $row)
	{
		list ($from, $to) = $pair;
		$convertedValue = null;
		$fromValue = $row->getValue($from);

		if ($fromValue instanceof LookupValue) {
			$convertedValue = $fromValue->getValue();
		} else if ($from->hasOptions() && $to->hasOptions()) {
			if ($fromValue !== null) {
				// 名前が一致するものをセット
				$matched = $to->getOptions(array('name'=>$fromValue->getName()));
				if (is_not_empty($matched)) {
					$convertedValue = $matched[0];
				}
			}
		} else if ($from->hasOptions() && !$to->hasOptions()) {
			if ($fromValue !== null) {
				$convertedValue = $fromValue->getName();
			}
		} else {
			$convertedValue = $fromValue;
		}

		return $convertedValue;
	}

	private function wrapMessages($messages, $line)
	{
		foreach ($messages as $message) 	/* @var $message Message */
			$message->setMessage($line.'行目 : ' . $message->getMessage());
		return $messages;
	}

	private function matchImportFields($keys, $fields)
	{
		$matches = array();
		for ($i = 0; $i < count($fields); $i++) {
			$column = $fields[$i];
			if ($column->getType() == Column::CHECKBOX) {
				$matches[$i] = array();
				for ($k = 0; $k < count($column->getOptions()); $k++) {
					$found = -1;
					for ($j = 0; $j < count($keys); $j++) {
						if ($column->getId() == $keys[$j][0] &&
							$column->getOption($k)->getId() == $keys[$j][1]) {
							$found = $j;
							break;
						}
					}
					if ($found == -1) {
						return false;
					}
					$matches[$i][$k] = $found;
				}
			} else {
				$found = -1;
				for ($j = 0; $j < count($keys); $j++) {
					if ($column->getId() == $keys[$j][0]) {
						$found = $j;
						break;
					}
				}
				if ($found == -1) {
					return false;
				}
				$matches[$i] = $found;
			}
		}
		return $matches;
	}


// 	public function writeRow(Row $row)
// 	{
// 		foreach ($this->fields as $column) {
// 			if ($column->getType() == Column::CHECKBOX) {
// 				foreach ($column->getOptions() as $option) {
// 					$found = array_grep($row->getValue($column), function($value) use ($option){
// 						return $value->getId() ==  $option->getId();
// 					});
// 					$this->_writeField(is_empty($found) ? "0" : "1");
// 				}
// 			} else if ($column->getType() == Column::LOOKUP) {
// 				$value = $row->getValue($column);
// 				$this->_writeField($value ? $value->getValue() : "");
// 			} else {
// 				$this->_writeField($row->getText($column));
// 			}
// 		}
// 		$this->_writeLf();
// 	}


// 	public function readRow(Table $table)
// 	{
// 		foreach ($this->fields as $column) {
// 			if ($column->getType() == Column::CHECKBOX) {
// 				foreach ($column->getOptions() as $option) {
// 					$found = array_grep($row->getValue($column), function($value) use ($option){
// 						return $value->getId() ==  $option->getId();
// 					});
// 					$this->_writeField(is_empty($found) ? "0" : "1");
// 				}
// 			} else if ($column->getType() == Column::LOOKUP) {
// 				$value = $row->getValue($column);
// 				$this->_writeField($value ? $value->getValue() : "");
// 			} else {
// 				$this->_writeField($row->getText($column));
// 			}
// 		}
// 		$this->_writeLf();
// 	}

	private function writeHeading($fields, $keyLines = false)
	{
		foreach ($fields as $column) {
			if ($column->getType() == Column::CHECKBOX) {
				foreach ($column->getOptions() as $option)
					$this->_writeField($column->getLabel()."[".$option->getName()."]");
			} else {
				$this->_writeField($column->getLabel());
			}
		}
		$this->_writeLf();
		if ($keyLines) {
			foreach ($fields as $column) {	/* @var $column Column */
				if ($column->getType() == Column::CHECKBOX) {
					foreach ($column->getOptions() as $option)	/* @var $option Option */
						$this->_writeField("key=".$column->getId()."_".$option->getId());
				} else {
					$this->_writeField("key=".$column->getId());
				}
			}
			$this->_writeLf();
		}
	}

	private function readHeading()
	{
		$fields = $this->readFileds();
		if ($fields === false) {
			return false;
		}
		$fields = $this->readFileds();
		if ($fields === false) {
			return false;
		}

		if (!str_starts_with('key=', $fields[0])) {
			return false;
		}

		return array_map(function($keystr) {
			return explode('_', substr($keystr, strpos($keystr, '=') + 1).'_');;
		}, $fields);
	}


	public function _writeField($field)
	{
		$buff = null;
		if ($this->fieldPos > 0) {
			$buff .= ',';
		}
		if (is_int($field) || is_float($field)|| is_double($field)) {
			$buff .= $field;
		} else {
			$buff .= '"' .str_replace('"', '""', $this->encode($field)) . '"';
		}
		fwrite($this->fp, $buff);
		$this->fieldPos++;
	}

	private function readFileds()
	{
// 		setlocale(LC_ALL,'ja_JP.UTF-8');
		$fields = fgetcsv($this->fp);
		if ($fields === false || $fields == array(null))
			return false;
		$encoded = array();
		foreach ($fields as $field) {
			$encoded[] = $this->decode($field);
		}
		return $encoded;
	}

	public function _writeLf()
	{
		fwrite($this->fp, $this->lineFeed);
		$this->fieldPos = 0;
	}

	public function close()
	{
		if ($this->fp != null) {
			fclose($this->fp);
			$this->fp = null;
		}
	}

	private function decode($value)
	{
		return $value;
	}

	/**
	 * @param string $value
	 */
	private function encode($value)
	{
		if (strtolower($this->charset) == 'utf-8')
			return $value;
		return mb_convert_encoding($value, $this->charset, 'utf-8');
	}

	/**
	 * @param string $charset
	 * @return string
	 */
	private function getLfCode($charset)
	{
		if (strtolower($charset) == 'shift_jis') {
			return "\r\n";
		} else if (strtolower($charset) == 'sjis-win') {
			return "\r\n";
		} else if (strtolower($charset) == 'utf-8') {
			return "\n";
		} else {
			die($charset);
		}
	}

	/**
	 * エクスポート可能なカラムの一覧を返す
	 * @param Table $table
	 * @return Column[]
	 */
	public static function getExportFields(Table $table)
	{
		$heading = $table->getColumns(array('type' => Column::ROWNUM));
		$succedent = array_filter($table->getColumns(), function(Column $column) {
			return $column->isListable() && $column->getType() !== Column::FILE && $column->getType() !== Column::ROWNUM;
		});
		return array_merge($heading, $succedent);
	}

	/**
	 * @param Table $table
	 * @return Column[]
	 */
	public static function getImportRequiredFields(Table $table)
	{
		// 先頭はレコード番号
		$heading = $table->getColumns(array('type' => Column::ROWNUM));
		$succedents = array();
		foreach ($table->getColumns() as $column) {
			if (!$column->isInputable())
				continue;
			if ($column->getType() === Column::FILE)
				continue;
			if ($column->getType() === Column::RELATIONAL_INPUT)
				continue;
			if ($column->getType() === Column::LOOKUP) {
				$target = $column->getReferenceTargetColumn();
				if ((!$target->hasProperty('isUnique') || !$target->isUnique())
					&& $target->getType() !== Column::ROWNUM) {
					continue;
				}
			}
			// Lookupのコピー先は入力不可
			if (self::isReferenceCopyee($column))
				continue;
			$succedents[] = $column;
		}
		return array_merge($heading, $succedents);
	}

	/**
	 * 更新キーの候補を返す。
	 * 更新キーの候補の条件は以下のいずれかに一致すること:
	 *  * データ番号フィールド
	 *  * 重複禁止かつCSV入力可能な文字列1行フィールド
	 */
	public static function getKeyCandidates(Table $table)
	{
		$keys = array();
		$fields = self::getImportRequiredFields($table);
		foreach ($fields as $field) {
			if ($field->getType() == Column::ROWNUM
				|| ($field->getType() == Column::TEXT && $field->isUnique())) {
				$keys[] = $field;
			}
		}
		return $keys;
	}

	/**
	 * @param Column $column
	 * @return boolean
	 */
	private static function isReferenceCopyee($column)
	{
		$lookupColumns = $column->getParent()->getColumns(array('type'=>Column::LOOKUP));
		foreach ($lookupColumns as $lookupColumn) {
			foreach ($lookupColumn->getReferenceCopyMap() as $pairOfColumn) {
				if ($column->getId() == $pairOfColumn[1]->getId()) {
					return true;
				}
			}
		}
		return false;
	}
}
