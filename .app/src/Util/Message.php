<?php
namespace Util;


class Message
{
	public $field = "";
	public $message = "";
	public $category = null;
	
	/**
	 * 
	 * @param string $field
	 * @param string $message
	 * @param string $category 'success'|'warning'|'info'
	 */
	public function __construct($field, $message, $category=null) 
	{
		$this->field = $field;
		$this->message = $message;
	}
}

