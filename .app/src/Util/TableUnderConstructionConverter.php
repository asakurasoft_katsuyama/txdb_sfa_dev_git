<?php
namespace Util;

use Util\DefaultConverter;

use TxCore\Column;

use TxCore\User;

/**
 * JSON concerter for internal JSON API.
 * @see TypeConverter
 */
class TableUnderConstructionConverter extends DefaultConverter
{
	/**
	 * @var User
	 */
	private $user;

	public function __construct(User $user = null)
	{
		parent::__construct($user);
	}

	protected function convertColumn(Column $value)
	{
		$properties = $value->getProperties();
		if (isset($properties['referenceTableId'])) {
			$table = $value->getReferenceTable();
			$properties['referenceTable'] = array(
				'id' => $table->getId(),
				'allows' => $table->getAllowedActions($this->user),
			);
			unset($properties['referenceTableId']);
		}
		
		return array_merge($properties, array(
			'id' => $value->getId(),
			'code' => $value->getCode(),
			'type' => $value->getType(),
			'label' => $value->getLabel(),
			'options' => $value->hasOptions() ? $value->getOptions() : null,
			'lookupConstraints' => $value->getLookupConstraints(),
			'relationalConstraints' => $value->getRelationalConstraints(),
		));
	}
	
}

