<?php
namespace Util;

use TxCore\CalendarSettings;
use TxCore\CalendarView;
use TxCore\CalendarView__Date;
use TxCore\CalendarView__Item;
use TxCore\CalendarView__UserEntry;
use TxCore\Comment;
use TxCore\Filter;

use TxCore\Condition;
use TxCore\Group;
use TxCore\ReadCheckSettings;
use TxCore\ReadLog;
use TxCore\Sort;
use TxCore\Criteria;
use TxCore\LookupValue;
use TxCore\Table;
use TxCore\Template;
use TxCore\Message;
use TxCore\File;
use TxCore\Row;
use TxCore\Option;
use TxCore\Column;
use TxCore\User;
use TxCore\Icon;
use TxCore\List_;
use TxCore\Engine;
use TxCore\Permission;
use TxCore\NotificationBasicRule;
use TxCore\NotificationConditionalRule;
use TxCore\NotificationCriteria;
use TxCore\Notification;
use TxCore\NotificationReceipt;
use TxCore\RowColumnChange;

use JSON\TypeConverter;
use TxCore\ViewColumn;

/**
 * JSON concerter for internal JSON API.
 * @see TypeConverter
 */
class DefaultConverter implements TypeConverter
{
	/**
	 * @var User
	 */
	private $user;

	public function __construct(User $user = null)
	{
		$this->user = $user;
	}

	public function convert($value, $parent)
	{
		if ($value === null) {
			return $value;
		} else if ($value instanceof User) {
            return $this->convertUser($value);
        } else if ($value instanceof Group) {
			return $this->convertGroup($value);
		} else if ($value instanceof Icon) {
			return array(
				'filekey' => $value->getFilekey(),
				'url' => $value->getUrl(),
			);
		} else if ($value instanceof List_ && $value->getType() === 'table') {
			$columns = array();
			foreach ($value->getColumns() as $column) {
				$columns[] = array(
					'id' => $column->getId(),
					'type' => $column->getType(),
					'label' => $column->getLabel(),
					'code' => $column->getCode(),
					'hasOptions' => $column->hasOptions(),
					'isSortable' => $column->isSortable(),
					'properties' => $column->getProperties()
				);
			}
			$rows = array();
			foreach ($value->getRows() as $row) {
				$rowValues = array();
				foreach ($value->getColumns() as $column) {
					$rowValues[] = array(
						'value' => $row->getValue($column),
						'text' =>$row->getText($column),
						'html' =>$row->getHtml($column),
						'type' =>$column->getType(),
					);
				}
				$rows[] = array(
					'id' => $row->getId(),
					'values' => $rowValues,
					'commentsCount' => $row->getCommentsCount(),
					'readUsersCount' =>
						$value->getTable()->isReadCheckEnabled()
						? $row->getReadUsersCount() : null,
					'hasRead' =>
						$value->getTable()->isReadCheckEnabled()
						? $row->hasRead($this->user) : null
				);

			}
				
			return array(
				'totalRowCount' => $value->getTotalRowCount(),
				'offset' => $value->getOffset(),
				'rowLimit' => $value->getRowLimit(),
				'rowCount' => count($value->getRows()),
				'columns' =>$columns,
				'rows' => $rows,
				'table' => array('id'=>$value->getTable()->getId()),
			);
		} else if ($value instanceof List_ && $value->getType() === 'user') {
			return array(
				'totalRowCount' => $value->getTotalRowCount(),
				'offset' => $value->getOffset(),
				'rowCount' => count($value->getRows()),
				'rows' => $value->getRows()
			);
		} else if ($value instanceof List_ && $value->getType() === 'group') {
			return array(
				'rows' => $value->getRows()
			);
		} else if ($value instanceof List_ && $value->getType() === 'comment') {
			return array(
				'rows' => $value->getRows()
			);
		} else if ($value instanceof List_ && $value->getType() === 'notification_receipt') {
            return array(
                'totalRowCount' => $value->getTotalRowCount(),
                'offset' => $value->getOffset(),
                'rowCount' => count($value->getRows()),
                'rows' => $value->getRows()
            );
        } else if ($value instanceof List_ && $value->getType() === null) { // Default
            return array(
                'totalRowCount' => $value->getTotalRowCount(),
                'offset' => $value->getOffset(),
                'rowLimit' => $value->getRowLimit(),
                'rowCount' => count($value->getRows()),
                'rows' => $value->getRows(),
            );
		} else if ($value instanceof Table) {
			return $this->convertTable($value);
		} else if ($value instanceof Template) {
			return $this->convertTemplate($value);
		} else if ($value instanceof Column) {
			return $this->convertColumn($value);
		} else if ($value instanceof Option) {
			return $this->convertOption($value);
		} else if ($value instanceof File) {
			return $this->convertFile($value);
		} else if ($value instanceof LookupValue) {
			return $this->convertLookup($value);
		} else if ($value instanceof Row) {
			return $this->convertRow($value);
		} else if ($value instanceof Message) {
			return $this->convertMessage($value);
		} else if ($value instanceof NotificationCriteria) {
			return $value->toArray();
		} else if ($value instanceof Criteria) {
			return $value->toArray();
		} else if ($value instanceof Comment) {
			return $this->convertComment($value, $this->user);
		} else if ($value instanceof CalendarSettings) {
			return $this->convertCalendarSettings($value);
        } else if ($value instanceof ReadCheckSettings) {
            return $this->convertReadCheckSettings($value);
		} else if ($value instanceof Filter) {
			return array('id'=>$value->getId(), 'name'=>$value->getName(), 'criteria'=>$value->getCriteria());
		} else if ($value instanceof Permission) {
			return $this->convertPermission($value);
        } else if ($value instanceof ViewColumn) {
            return $this->convertViewColumn($value);
// 		} else if ($value instanceof Sort) {
// 			return $this->convertSort($value);
// 		} else if ($value instanceof Condition) {
// 			return $this->convertCondition($value);

        } else if ($value instanceof CalendarView) {
            return $this->convertCalendarView($value);
        } else if ($value instanceof CalendarView__UserEntry) {
            return $this->convertCalendarViewUserEntry($value);
        } else if ($value instanceof CalendarView__Date) {
            return $this->convertCalendarViewDate($value);
        } else if ($value instanceof CalendarView__Item) {
            return $this->convertCalendarViewItem($value);
        } else if ($value instanceof NotificationBasicRule) {
            return $this->convertNotificationBasicRule($value);
        } else if ($value instanceof NotificationConditionalRule) {
            return $this->convertNotificationConditionalRule($value);
        } else if ($value instanceof Notification) {
            return $this->convertNotification($value);
        } else if ($value instanceof NotificationReceipt) {
            return $this->convertNotificationReceipt($value);
        } else if ($value instanceof RowColumnChange) {
            return $value->toArray();
        } else if ($value instanceof ReadLog) {
            return $this->convertReadLog($value);
		} else {
			return $value;
		}
	}

	private function convertFile(File $value)
	{
		if ($value->isTemp()) {
			return array(
				'isTemp' => true,
				'fileName' => $value->getFileName(),
				'url' => null,
				'thumbnailUrl' => null,
				'filekey' => basename($value->getFilekey()),
				'size' => null,
				'contentType' => $value->getContentType(),
			);
		}
		return array(
			'fileName' => $value->getFileName(),
			'url' => $value->getUrl(),
			'thumbnailUrl' => $this->getThumbnailUrl($value),
			'filekey' => $value->getFilekey(),
			'size' => $value->getSize(),
		);
	}
	
	private function getThumbnailUrl(File $file)
	{
		if (preg_match(get_config('app', 'file_inline'), $file->getContentType()) === 1) {
			if ($file->getSize() <= get_config('app', 'thumbnail_original_size_limit') ) {
				$storage = Engine::factory()->getStorage();
				if (($filePath = $storage->getFilePath($file->getFilekey(), true)) === null) return null;
				list($width, $height) = getimagesize($filePath);
				$limit = get_config('app', 'thumbnail_original_imagesize_limit');
				if ($width <= $limit && $height <= $limit) {
					return $file->getUrl().'?t'.$file->getParent()->getThumbnailMaxSize();
				}
			}
		}
		return null;
	}

	private function convertTable(Table $value)
	{
		return array(
			'id' => $value->getId(),
			'name' => $value->getName(),
			'description' => $value->getDescription(),
			'icon' => $value->getIcon(),
			'isPublic' => $value->isPublic(),
			'hasPendingRelease' => $value->hasPendingRelease(),
			'isDefault' => null,
			'isApiEnabled' => $value->isApiEnabled(),
			'apiKey' => $value->allows('alter', $this->user)
				? $value->getApiKey() : null,
			'isWriteApiEnabled' => $value->isWriteApiEnabled(),
			'writeApiKey' => $value->allows('alter', $this->user)
				? $value->getWriteApiKey() : null,
			'owner' => $value->getOwner(),
			'roundType' => $value->getRoundType(),
			'roundScale' => $value->getRoundScale(),
			'permissionControl' => $value->getPermissionControl(),
			'isCommentEnabled' => $value->isCommentEnabled(),
			'isCalendarEnabled' => $value->isCalendarEnabled(),
            'isReadCheckEnabled' => $value->isReadCheckEnabled(),
			'calendarSettings' => $value->getCalendarSettings(),
            'readCheckSettings' => $value->getReadCheckSettings(),
            'isReadCheckTarget' =>
                $value->isReadCheckEnabled() && !$value->getReadCheckSettings()
                ? null : $value->isReadCheckTarget($this->user), // Null means setting not loaded.
			'isCommentFollowingEnabled' => $value->isCommentFollowingEnabled(),
			'isSixColumnsEnabled' => $value->isSixColumnsEnabled(),
			'isChild' => $value->isChild(),
		);
	}
	
	private function convertTemplate(Template $value)
	{
		return array(
			'id' => $value->getId(),
			'name' => $value->getName(),
			'description' => $value->getDescription(),
			'icon' => $value->getIcon(),
			'isPublic' => $value->isPublic(),
			'isDefault' => $value->isDefault(),
			'roundType' => $value->getRoundType(),
			'roundScale' => $value->getRoundScale(),
		);
	}
	
	protected function convertColumn(Column $value)
	{
		$properties = $value->getProperties();
		if (isset($properties['referenceTableId'])) {
			$table = $value->getReferenceTable();
			$properties['referenceTable'] = array(
				'id' => $table->getId(),
				'allows' => $table->getAllowedActions($this->user),
			);
			unset($properties['referenceTableId']);
		}
		if ($value->hasProperty('referenceColumnIds')) {
			$properties['referenceColumns']
				= $value->getReferenceColumns();
		}
		
		return array_merge($properties, array(
			'id' => $value->getId(),
			'code' => $value->getCode(),
			'type' => $value->getType(),
			'label' => $value->getLabel(),
			'options' => $value->hasOptions() ? $value->getOptions() : null,
			'hasOptions' => $value->hasOptions(),
			'isSortable' => $value->isSortable(),
			'isListable' => $value->isListable(),
			'isInputable' => $value->isInputable(),
			'isAutofill' => $value->isAutofill(),
			'rolesInCalendar' => $value->getRolesInCalendar(),
			'searchableOperators' => $value->getSearchableOperators(),
			'lookupConstraints' => $value->getLookupConstraints(),
			'relationalConstraints' => $value->getRelationalConstraints(),
			'notificationConstraints' => $value->getNotificationConstraints(),
		));
	}
	
	private function convertMessage(Message $value)
	{
		$data = array();
		$context = null;
		if ($value->getContext() instanceof Column) {
			$context = array( 'id' => $value->getContext()->getId());
		} else if (is_array($value->getContext())) {
			$context = array();
			foreach ($value->getContext() as $k => $v) {
				if ($v instanceof Column) {
					$context['id'] = $v->getId();
				} else {
					$context[$k] = $v;
				}
			}
		}
		$data['context'] = $context;
		$data['message'] = $value->getMessage();
		return $data;
	}

	private function convertLookup(LookupValue $value)
	{
		return array(
			'key' => $value->getKey(),
			'value' => $value->getValue(),
		);
	}

	private function convertOption(Option $value)
	{
		return array(
			'id' => $value->getId(),
			'name' => $value->getName(),
		);
	}

	private function convertRow(Row $value)
	{
		$rowValues = array();
		foreach ($value->getParent()->getColumns() as $column) {
			$rowValues[] = array(
				'value' => $value->getValue($column),
				'text' => $value->getText($column),
				'html' =>$value->getHtml($column),
				'column' => $column,
				'rownum' => $value->getRownum()
			);
		}
		$row = array(
            'id' => $value->getId(),
            'no' => $value->getRownum(),
            'values'=>$rowValues,
            'version' => $value->getVersion(),
            'commentsCount' => $value->getCommentsCount(),
            'readUsersCount' => $value->getParent()->isReadCheckEnabled() ?
                $value->getReadUsersCount() : null,
            'hasRead' => $value->getParent()->isReadCheckEnabled() ?
                $value->hasRead($this->user) : null
        );
		if ($value->getAction()) {
			$row['action'] = $value->getAction();
		}
		return $row;
	}
	
// 	private function convertCriteria(Criteria $value)
// 	{
// 		return array(
// // 			'offset' => $value->getOffset(),
// // 			'limit' => $value->getLimit(),
// 			'boolOperator' => $value->getBoolOperator(),
// 			'conditions' => $value->getConditions(),
// 			'sort' => $value->getSort(),
// 			);
// 	}
	
// 	private function convertSort(Sort $value)
// 	{
// 		return array(
// 			'column' => array('id'=>$value->getColumn()->getId()),
// 			'isReverse' => $value->isReverse(),
// 		);
// 	}
	
// 	private function convertCondition(Condition $value)
// 	{
// 		return array(
// 			'column' => array(
// 				'id'=>$value->getColumn()->getId(), 
// 				'type'=>$value->getColumn()->getType()
// 			),
// 			'operator' => $value->getOperator(),
// 			'value' => $value->getValue(),
// 		);
// 	}

	private function convertPermission(Permission $value)
	{
		return array(
			'tableId' => $value->getTableId(),
			'userId' => $value->getUserId(),
			'isReadable' => $value->getIsReadable(),
			'isWritable' => $value->getIsWritable(),
		);
	}

	/**
	 * @param Group $value
	 * @return array
	 */
	private function convertGroup(Group $value)
	{
		return array(
			'id' => $value->getId(),
			'name' => $value->getName(),
		);
	}

	/**
	 * @param Comment $value
	 * @param User $user
	 * @return array
	 */
	private function convertComment(Comment $value, User $user)
	{
		$targets = array();
		$targets = array_map(function($entity) {
			if ($entity instanceof User) {
				return array('type'=>'user', 'id'=>$entity->getId(),
					'name' => $entity->getScreenName());
			} else if ($entity instanceof Group) {
				return array('type'=>'group', 'id'=>$entity->getId(),
					'name' => $entity->getName());
			}
		}, $value->getNotificationTargets());
		$postedTime = $value->getPostedTime(true);
		return array(
			'id' => $value->getId(),
			'no' => $value->getNo(),
			'text' => $value->getText(),
			'postedTime' => $postedTime ? date('Y-m-d H:i', $postedTime) : null,
			'postedUser' => $value->getPostedUser(),
			'isDeletable' => $user->isAdmin() || ($value->getPostedUser() && $value->getPostedUser()->getId() == $user->getId()),
			'notificationTargets' => $targets,
		);
	}

	/**
	 * @param CalendarSettings $value
	 * @return array
	 */
	private function convertCalendarSettings(CalendarSettings $value)
	{
		return array(
			'dateColumn' => $value->getDateColumnId() ? array('id' => $value->getDateColumnId()) : null,
			'userColumn' => $value->getUserColumnId() ? array('id' => $value->getUserColumnId()) : null,
			'startTimeColumn' =>  $value->getStartTimeColumnId() ? array('id' => $value->getStartTimeColumnId()) : null,
			'endTimeColumn' =>  $value->getEndTimeColumnId() ? array('id' => $value->getEndTimeColumnId()) : null,
			'titleColumn' => $value->getTitleColumnId() ? array('id' => $value->getTitleColumnId()) : null,
		);
	}

    /**
     * @param ReadCheckSettings $value
     * @return array
     */
    private function convertReadCheckSettings(ReadCheckSettings $value)
    {
        return array(
            'isAutoreadEnabled' => $value->isAutoreadEnabled(),
            'isOnlistEnabled' => $value->isOnlistEnabled(),
            'checkTargets' => $value->getCheckTargetsArray(),
        );
    }

    /**
     * @param CalendarView $value
     * @return array
     */
    private function convertCalendarView($value)
    {
        return array(
//             'endTime' => $value->getEndTime(),
           'dateEntries' => $value->getDateEntries(),
            'userEntries' => $value->getUserEntries(),
            'nextStartDate' => $value->getNextStartDate(),
            'prevStartDate' => $value->getPrevStartDate(),
            'startDate' => $value->getStartDate(),
            'userOffset' => $value->getUserOffset(),
            'userCount' => $value->getUserCount(),
            'userTotalCount' => $value->getUserTotalCount(),
        );
    }

    /**
     * @param CalendarView__UserEntry $value
     * @return array
     */
    private function convertCalendarViewUserEntry($value)
    {
        return array(
            'dateEntries' => $value->getDateEntries(),
            'user' => $value->getUser(),
        );
    }

    /**
     * @param CalendarView__Date $value
     * @return array
     */
    private function convertCalendarViewDate($value)
    {
        return array(
            'date' => $value->getDate(),
            'isToday' => $value->isToday(),
            'dateFormatted' => $value->getDateFormatted('m/d'),
            'dayOfWeek' => $value->getDateFormatted('w'),
            'items' => $value->getItems()
        );
    }

    /**
     * @param CalendarView__Item $value
     * @return array
     */
    private function convertCalendarViewItem($value)
    {
        return array(
            'rowId' => $value->getRowId(),
            'title' => $value->getTitle(),
            'startTime' => $value->getStartTime(),
            'endTime' => $value->getEndTime(),
        );
    }

	/**
	 * @param NotificationBasicRule $value
	 * @return array
	 */
	private function convertNotificationBasicRule(NotificationBasicRule $value)
	{
		$targetingType = $value->getTargetingType();
		$targets = array_map(function($t) use($targetingType) {
			/** @var $t Group|User|Column */
			if ($targetingType == NotificationBasicRule::TARGETING_COLUMN)
				return array('id'=>$t->getId(), 'type'=>'column');
			else
				return array('id'=>$t->getId(), 'type'=>($t instanceof User ? 'user' : 'group'));
		}, $value->getTargets());

		return array(
			'targetingType' => $value->getTargetingType(),
			'targets' => $targets,
			'isOnRowInsert' => $value->isOnRowInsert(),
			'isOnRowUpdate' => $value->isOnRowUpdate(),
			'isOnCommentInsert' => $value->isOnCommentInsert(),
		);
	}

	/**
	 * @param NotificationConditionalRule $value
	 * @return array
	 */
	private function convertNotificationConditionalRule(
		NotificationConditionalRule $value)
	{
		$targetingType = $value->getTargetingType();
		$targets = array_map(function($t) use ($targetingType) {
			/** @var $t Group|User|Column */
			if ($targetingType == NotificationConditionalRule::TARGETING_COLUMN)
				return array('id'=>$t->getId(), 'type'=>'column');
			else
				return array('id'=>$t->getId(), 'type'=>($t instanceof User ? 'user' : 'group'));
		}, $value->getTargets());

		return array(
			'targetingType' => $value->getTargetingType(),
			'targets' => $targets,
			'criteria' => $value->getCriteria(),
			'message' => $value->getMessage(),
		);
	}

	/**
	 * @param Notification $value
	 * @return array
	 */
	private function convertNotification(Notification $value)
	{
		$table = $value->getTable();
		$tableArray = array(
			'id'=>$table->getId(),
			'name'=>$table->getName(),
		);

		$columns = $table->getNotificationViewColumns();
		$columnsArray = array();
		foreach ($columns as $column) {
			$columnsArray[] = array(
				'id' => $column->getId(),
				'type' => $column->getType(),
				'label' => $column->getLabel(),
				'code' => $column->getCode(),
			);
		}

		$row = $value->getRow();
		$rowValues = array();
		foreach ($columns as $column) {
			$rowValues[] = array(
				'value' => $row->getValue($column),
				'text' =>$row->getText($column),
				'html' =>$row->getHtml($column),
				'type' =>$column->getType(),
			);
		}
		$rowArray = array(
			'id' => $row->getId(),
			'values' => $rowValues,
			'columns' => $columnsArray,
		);

		//$comment = $value->getComment();
		//$commentArray = null;
		//if ($comment) {
		//	$commentArray = array(
		//		'text'
		//	);
		//}

		$time = $value->getNotifiedOn(true);
		return array(
			'id' => $value->getId(),
			'notifiedOn' => $time ? date('Y-m-d H:i', $time) : null,
			'notifier' => $value->getNotifier(),
			'message' => $value->getMessage(),
			'changes' => $value->getChanges(),
			'table' => $tableArray,
			'row' => $rowArray,
			'comment' => $value->getComment(),
		);
	}

    /**
     * @param NotificationReceipt $value
     * @return array
     */
	private function convertNotificationReceipt(NotificationReceipt $value)
	{
		return array(
			'notification' => $value->getNotification(),
			'recipient' => $value->getRecipient(),
			'isRead' => $value->isRead(),
		);
	}


    /**
     * @param ViewColumn $value
     * @return array
     */
    private function convertViewColumn($value)
    {
        return array(
            'width' => $value->getWidth(),
            'column' => $value->getColumn(),
        );
    }

    /**
     * @param ReadLog $value
     * @return array
     */
    private function convertReadLog(ReadLog $value)
    {
        return array(
            'readOn' => $value->getReadOn(),
            'readUser' => $this->convertUser($value->getReadUser()),
            'comment' => $value->getComment(),
            'canEdit' => $value->canEdit($this->user),
        );
    }

    /**
     * @param User $value
     * @return array
     */
    private function convertUser($value)
    {
        return array(
            'id' => $value->getId(),
            'role' => $value->getRole(),
            'screenName' => $value->getScreenName(),
            'loginId' => $value->getLoginId(),
            'profile' => $value->getProfile(),
            'seq' => $value->getSeq(),
            'password' => ($this->user->isAdmin() || $value->getId() == $this->user->getId())
                ? $value->getPassword() : '***'
        );
    }
}

