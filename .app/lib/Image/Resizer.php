<?php
namespace Image;

use \Exception;

/**
 * Image Resizer.
 */
class Resizer
{
	const GD2 = 1;
	const IMAGICK = 2;
	
	/**
	 * Default resize interpolation (GD native).
	 * @var int
	 */
	const AVARAGE = 1;
	
	/**
	 * Bicubic resize interpolation (Coded originally).
	 * @var int
	 */
	const BICUBIC = 2;

	
	const CROP_TOP = 1;
	const CROP_BOTTOM = 3;
	
	const CROP_LEFT = 1;
	const CROP_CENTER = 2;
	const CROP_RIGHT = 3;
	

	/**
	 * Resize to specified dimension. 
	 * It style not keep original aspect ratio.
	 * 
	 */
	const FIT = 1;

	/**
	 * Resizing to be a size less than the specified size, and keeping origianl aspect ratio.
	 * Some case, might be one of dimension (vertical or horizontal) is smaller than specified size.
	 */
	const CONTAIN = 2;
	
	/**
	 * オリジナル比率を保持し、指定されたサイズ以下になるようにリサイズする。足りない部分は、背景色で塗る。
	 * オリジナル比率と指定サイズを保障する。
	 */
	const CONTAIN_FILL = 4;

	/**
	 * keeping original ratio. 
	 * オリジナル比率を保持し、指定されたサイズを覆うようにリサイズする（必要であれば拡大もする）。
	 * はみ出した部分は、cropOrigin に従い切り取る。
	 */
	const COVER = 3;

	private $style = self::FIT;

	private $cropOriginY = self::CROP_CENTER;
	private $cropOriginX = self::CROP_CENTER;
	
	/**
	 * Background color
	 * @var int
	 */
	private $backgroundColor;

	/**
	 * @var int
	 */
	private $interpolation = self::AVARAGE;
	
	/**
	 * @var int
	 */
	private $allowScaleUp = false;

	/**
	 * Image processing engine. 
	 * @var int
	 */
	private $engine = self::GD2;
	
	/**
	 * @param bool $value
	 */
	public function setStyle($value)
	{
		$this->style = $value;
	}

	/**
	 * @param bool $value
	 */
	public function setInterpolation($value)
	{
		$this->interpolation = $value;
	}
	
	/**
	 * Sets crop orign.
	 * @param bool $value
	 */
	public function setCropOriginY($vertical)
	{
		$this->cropOriginY = $vertical;
	}
	
	/**
	 * Sets crop orign.
	 * @param bool $value
	 */
	public function setCropOriginX($horizontal)
	{
		$this->cropOriginX = $horizontal;
	}
	
	/**
	 * @param bool $value
	 */
	public function setAllowScaleUp($falg)
	{
		$this->allowScaleUp = $falg;
	}

	/**
	 * 
	 * @param int $engine
	 */
	public function __construct($engine = self::GD2)
	{
		$this->engine = $engine;
	}
	
	/**
	 * @param string $imagePath
	 * @param string $destPath
	 * @param int $width
	 * @param int $height
	 */
	public function resize($imagePath, $destPath, $width = null, $height = null)
	{
		return $this->engine == self::GD2 ? 
			$this->resizeGD2($imagePath, $destPath, $width, $height) : 
			$this->resizeImagick($imagePath, $destPath, $width, $height);
	}
	
	/**
	 * @param string $imagePath
	 * @param string $destPath
	 * @param int $width
	 * @param int $height
	 */
	private function resizeGD2($imagePath, $destPath, $width, $height)
	{
		$width = $width !== null ? intval($width) : null;
		$height = $height !== null ? intval($height) : null;
		
		if (!file_exists($imagePath)) {
			throw new Exception('Source image dose not found.');
		}

		list ($orgWidth, $orgHeight, $orgType, $orgAttr) = getimagesize($imagePath);
		if ($orgWidth == 0 || $orgHeight == 0) {
			throw new Exception('Image size is zero : ' . $imagePath);
		}
		$resizeInfo = array();
		if ($this->style == self::FIT) {
			$resizeInfo = self::getFitSize($orgWidth, $orgHeight, $width, $height);
		} else if ($this->style == self::CONTAIN) {
			$resizeInfo = self::getContainingSize($orgWidth, $orgHeight, $width, $height);
		} else if ($this->style == self::CONTAIN_FILL) {
			$resizeInfo = self::getContainingSize($orgWidth, $orgHeight, $width, $height, true);
		} else if ($this->style == self::COVER) {
			$resizeInfo = $this->getCoveredSize($orgWidth, $orgHeight, $width, $height);
		} else {
			throw new Exception('Unsupported resize style.');
		}

		if ($resizeInfo[0] && (($resizeInfo[5] <= $orgWidth && $resizeInfo[6] <= $orgHeight) || $this->allowScaleUp)) {
			$imagecreatefromxxx = array(
				IMAGETYPE_GIF  => 'imagecreatefromgif',
				IMAGETYPE_JPEG  => 'imagecreatefromjpeg',
				IMAGETYPE_JPEG2000 => 'imagecreatefromjpeg',
				IMAGETYPE_PNG  => 'imagecreatefrompng',
			);
			$imagexxx = array(
				IMAGETYPE_GIF  => function($image, $path) { return imagegif($image, $path); },
				IMAGETYPE_JPEG  => function($image, $path) { return imagejpeg($image, $path, 100); },
				IMAGETYPE_JPEG2000 => function($image, $path) { return imagejpeg($image, $path, 100); },
				IMAGETYPE_PNG  => function($image, $path) { return imagepng($image, $path, 0); },
			);

			if (!isset($imagecreatefromxxx[$orgType])) {
				throw new Exception('Unsupported image type : '. $imagePath);
			}

			$srcImage = $imagecreatefromxxx[$orgType]($imagePath);

			list (, $destX, $destY, $srcX, $srcY, $destWidth, $destHeight, $srcWidth, $srcHeight) = $resizeInfo;
//			list ($distLeft, $distTop, $distWidth, $destHeight) = $resize_dist;
			$destImage = imagecreatetruecolor($destWidth, $destHeight);//先画像
// 			$destImage = imagecreatetruecolor($width, $height);//先画像

			$color_while = imagecolorallocate($destImage, 255, 255, 255);
			imagefill($destImage, 0, 0, $color_while);

			
			$resampledMethod = ($this->interpolation == self::AVARAGE ? 
					'imagecopyresampled' : 
					array('Image\Resizer', 'imagecopyresamplebicubic'));
			 
			$resampledMethod(
				$destImage,
				$srcImage,
				$destX,
				$destY,
				$srcX,
				$srcY,
				$destWidth,
				$destHeight,
				$srcWidth,
				$srcHeight);

			$imagexxx[$orgType]($destImage, $destPath);

			imagedestroy($srcImage);
			imagedestroy($destImage);

		} else {
			// Copy simpliy
			copy($imagePath, $destPath);
		}
		
		return array($orgType);
	}

	/**
	 * Resizeing.
	 *
	 * @param string $imagePath
	 * @param string $destPath
	 * @param int $width
	 * @param int $height
	 */
	private function resizeImagick($imagePath, $destPath, $width, $height)
	{
		list ($orgWidth, $orgHeight, $orgType, $orgAttr) = getimagesize($imagePath);
		$im = new \Imagick($imagePath);
		//list($orgWidth, $orgHeight) = $im->getsize();
		
		$resizeInfo = array();
		if ($this->style == self::FIT) {
			$resizeInfo = self::getFitSize($orgWidth, $orgHeight, $width, $height);
		} else if ($this->style == self::CONTAIN) {
			$resizeInfo = self::getContainingSize($orgWidth, $orgHeight, $width, $height);
		} else if ($this->style == self::CONTAIN_FILL) {
			$resizeInfo = self::getContainingSize($orgWidth, $orgHeight, $width, $height, true);
		} else if ($this->style == self::COVER) {
			$resizeInfo = $this->getCoveredSize($orgWidth, $orgHeight, $width, $height);
		} else {
			throw new Exception('Unsupported resize style.');
		}
		
		list (, $destX, $destY, $srcX, $srcY, $destWidth, $destHeight, $srcWidth, $srcHeight) = $resizeInfo;
		if ($srcX != 0 || 
			$srcY != 0 || 
			$srcWidth !== $orgWidth || 
			$srcHeight !== $orgHeight) {
			$im->cropimage($srcWidth, $srcHeight, $srcX, $srcY);
		}

		if (($destWidth <= $orgWidth && $destHeight <= $orgHeight) || $this->allowScaleUp) {
			$im->resizeImage($destWidth, $destHeight, \Imagick::FILTER_MITCHELL, 1, false);
		}
		$im->writeImage($destPath);
		$im->destroy();
		return array($orgType);
		
	}
	
	/**
	 * 指定された長さに強制的にリサイズする
	 * 
	 * @param int $orgWidth
	 * @param int $orgHeight
	 * @param int $width
	 * @param int $height
	 * @return array
	 */
	private static function getFitSize($orgWidth, $orgHeight, $width, $height)
	{
		if (is_empty($width) || is_empty($height)) {
			throw new Exception('Invalid demension.');
		}
			
		$destWidth = $width;
		$destHeight = $height;
		$destX = 0;
		$destY = 0;
		$srcY = 0;
		$srcX = 0;
		$srcWidth = $orgWidth;
		$srcHeight = $orgHeight;

		$shouldResize = ($orgWidth !== $width) || ($orgHeight !== $height);
		return array($shouldResize, $destX, $destY, $srcX, $srcY, 
				$destWidth, $destHeight, $srcWidth, $srcHeight);
	}


	/**
	 * @param int $orgWidth
	 * @param int $orgHeight
	 * @param int $width
	 * @param int $height
	 * @param int $min_w
	 * @param int $min_h
	 * @param int $resize_dist
	 * @return boolean
	 */
	private static function getContainingSize($orgWidth, $orgHeight, $width, $height, $fillfLess = false)
	{
		$destWidth = 0;
		$destHeight = 0;
		$distY = 0;
		$distX = 0;

		$resizeRatesV = 0;
		$resizeRatesH = 0;
		if ($width !== null)
			$resizeRatesH = ($width / $orgWidth);
		if ($height !== null)
			$resizeRatesV = ($height / $orgHeight);

		// Determine resize rate.
		$resizeRate = 0;
		if ($width == null) {
			$resizeRate = $resizeRatesV;
		} else if ($height == null) {
			$resizeRate = $resizeRatesH;
		} else {
			$resizeRate = min($resizeRatesH, $resizeRatesV);
		}
		
		$destWidth = $orgWidth * $resizeRate;
		$destHeight = $orgHeight * $resizeRate;
		
		// Fills whitespace if image smaller than specified size.
		if ($fillfLess) {
			if ($width !== null && $width > $destWidth) {
				$distX = ($width - $destWidth) / 2;
				$destWidth = $width;
			}
			if ($height !== null && $height > $destHeight) {
				$distY = ($height - $destHeight) / 2;
				$destHeight = $height;
			}
		}
		
		return array(true, $distX, $distY, 0, 0, $destWidth, $destHeight, $orgWidth, $orgHeight);
	}
	
	/**
	 * 
	 * @param int $orgWidth
	 * @param int $orgHeight
	 * @param int $width
	 * @param int $height
	 * @throws Exception
	 * @return array()
	 */
	private function getCoveredSize($orgWidth, $orgHeight, $width, $height)
	{
		if (is_empty($width) || is_empty($height)) {
			throw new Exception('Invalid demension.');
		}
			
		$destWidth = 0;
		$destHeight= 0;
		$distY = 0;
		$distX = 0;
		$srcX = 0;
		$srcY = 0;


		$resizeRate = max(($width / $orgWidth), ($height / $orgHeight));
		$destWidth = $orgWidth * $resizeRate;
		$destHeight = $orgHeight * $resizeRate;

		// Crop overflowing region.
		if ($width < $destWidth) {
			if ($this->cropOriginX == self::CROP_CENTER) {
				$srcX = ($orgWidth - ($width / $resizeRate)) / 2;
			} else if ($this->cropOriginX == self::CROP_LEFT) {
				$srcX = 0;
			} else if ($this->cropOriginX == self::CROP_RIGHT) {
				$srcX = $orgWidth;
			} else {
				throw new Exception('Unknown crop orign');
			}
			$orgWidth =  ($width / $resizeRate);
			$destWidth = $width;
		}
		
		if ($height < $destHeight) {
			if ($this->cropOriginY == self::CROP_CENTER) {
				$srcY = ($orgHeight - ($height / $resizeRate)) / 2;
			} else if ($this->cropOriginY == self::CROP_TOP) {
				$srcY = 0;
			} else if ($this->cropOriginY == self::CROP_BOTTOM) {
				$srcY = $orgHeight;
			} else {
				throw new Exception('Unknown crop orign');
			}
			
			$orgHeight =  ($height / $resizeRate);
			$destHeight = $height;
		}
		
		return array(true, $distX, $distY, $srcX, $srcY, $destWidth, $destHeight, $orgWidth, $orgHeight);
	}
		

	// http://www.php.net/manual/en/function.imagecopyresampled.php
	public static function imagecopyresamplebicubic($dst_img, $src_img, $dst_x, $dst_y, $src_x, $src_y, $dst_w, $dst_h, $src_w, $src_h)
	{
		$scaleX = ($src_w - 1) / $dst_w;
		$scaleY = ($src_h - 1) / $dst_h;

		$scaleX2 = $scaleX / 2.0;
		$scaleY2 = $scaleY / 2.0;

		$tc = imageistruecolor($src_img);

		for ($y = $src_y; $y < $src_y + $dst_h; $y++) {
			$sY   = $y * $scaleY;
			$siY  = (int) $sY;
			$siY2 = (int) $sY + $scaleY2;

			for ($x = $src_x; $x < $src_x + $dst_w; $x++) {
				$sX   = $x * $scaleX;
				$siX  = (int) $sX;
				$siX2 = (int) $sX + $scaleX2;

				if ($tc) {
					$c1 = imagecolorat($src_img, $siX, $siY2);
					$c2 = imagecolorat($src_img, $siX, $siY);
					$c3 = imagecolorat($src_img, $siX2, $siY2);
					$c4 = imagecolorat($src_img, $siX2, $siY);

					$r = (($c1 + $c2 + $c3 + $c4) >> 2) & 0xFF0000;
					$g = ((($c1 & 0xFF00) + ($c2 & 0xFF00) + ($c3 & 0xFF00) + ($c4 & 0xFF00)) >> 2) & 0xFF00;
					$b = ((($c1 & 0xFF)   + ($c2 & 0xFF)   + ($c3 & 0xFF)   + ($c4 & 0xFF))   >> 2);

					imagesetpixel($dst_img, $dst_x + $x - $src_x, $dst_y + $y - $src_y, $r+$g+$b);
				} else {
					$c1 = imagecolorsforindex($src_img, imagecolorat($src_img, $siX, $siY2));
					$c2 = imagecolorsforindex($src_img, imagecolorat($src_img, $siX, $siY));
					$c3 = imagecolorsforindex($src_img, imagecolorat($src_img, $siX2, $siY2));
					$c4 = imagecolorsforindex($src_img, imagecolorat($src_img, $siX2, $siY));

					$r = ($c1['red']   + $c2['red']   + $c3['red']   + $c4['red']  ) << 14;
					$g = ($c1['green'] + $c2['green'] + $c3['green'] + $c4['green']) << 6;
					$b = ($c1['blue']  + $c2['blue']  + $c3['blue']  + $c4['blue'] ) >> 2;

					imagesetpixel($dst_img, $dst_x + $x - $src_x, $dst_y + $y - $src_y, $r+$g+$b);
				}
			}
		}
	}

}

