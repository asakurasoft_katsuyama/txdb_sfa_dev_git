<?php 

namespace Barge;

/**
 * <pre>
 * preg_match_callback('/{\d+}/', 
 * 		Barge_Proxy:create($this, 'callback', array($args)));
 * </pre>
 */
class Proxy
{
	private $args = array();
	private $callable;
	
	public static function create($obj, $method, $args) 
	{
// 		$mehtod = new ReflectionMethod($obj, $method);
// 		if ($mehtod->isPrivate()) 
// 			$mehtod->setAccessible(true);

		$obj = new self(array($obj, $method), $args);
		return array($obj, 'handle');
	}
	
	public function __construct($callable, $args)
	{	
		$this->callable = $callable;
		$this->args = $args;
	}
	
	public function handle(/*...*/) 
	{
		$args = func_get_args();
		foreach ($this->args as &$value) 
			$args[] = &$value;

		return call_user_func_array($this->callable, $args);
	}
}
