<?php 
namespace Barge\Web;

use Barge\Log\DC;

use TxCore\Util;

use \Exception;
use \ReflectionMethod;

use JSON\JSON;
use JSON\TypeConverter;

use Barge\Web\Renderer;
use Barge\Web\Form;
use Barge\Web\User;

/**
 * 
 *
 */
class ActionContext 
{
	static $initSession = false;
	
	/**
	 * @var Renderer
	 */
	private $renderer;
	
	/**
	 * @var Form
	 */
	private $form;
	
	/**
	 * @var array
	 */
	private $messages = array();
	
	/**
	 * @var JSON\TypeConverter
	 */
	private $jsonConverter;
	
	/**
	 * @var Object
	 */
	private $user;
	
	private $globalFilters = array();

	public function __construct()
	{
		$this->renderer = new Renderer();
	}

	/**
	 * @param string $url
	 */
	public function redirect($url)
	{
		$this->setHeader("Location", $url);
	}

	/**
	 * @param string $name
	 * @return mixed
	 */
	public function getSession($name)
	{
		$this->initSession();
		if (isset($_SESSION[$name]))
			return $_SESSION[$name];
		return null;
	}
	
	/**
	 * @param string $name
	 * @param mixed $value
	 */
	public function setSession($name, $value)
	{
		$this->initSession();
		$_SESSION[$name] = $value;
	}
	
	
	/**
	 * @param string $name
	 * @param boolean $json
	 * @return mixed
	 */
	public function getParam($name, $json = false)
	{
		$params = $this->getParams();
		$value = isset($params[$name]) ? $params[$name] : null;
		return $json ? Util::decodeJSON($value) : $value; 	 
	}
	
	/**
	 * @return array
	 */
	public function getParams()
	{
		static $params = null;
		if ($params == null) {
			$method = isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : null;
			if ($method == 'POST') {
				$params = $_POST;
			} else if ($method == 'PUT') {
				parse_str(file_get_contents("php://input"), $params);
			} else {
				$params = $_GET;
			}
		}
		return $params;
	}

	public function getForm($formClass) 
	{
		$form = new $formClass();
		$form->populate($this->getParams());
		$this->form = $form;
		return $form;
	}
	
	/**
	 * Sets HTTP header.
	 * 
	 * @param string $name
	 * @param string $content
	 * @param bool $encode
	 */
	public function setHeader($name, $content = false, $encode = false)
	{
		if ($content === false) {
			header($name);
		} else {
			header($name.': '. ($encode ? urlencode($content) : $content));
		}
	}
	
	/**
	 * Render HTML as HTTP response, using template.
	 * 
	 * @param string $path Template path to render.
	 * @param array $vars Template variables.
	 * @param string $contentType
	 * @return bool
	 */
	public function render($path, $vars = array(), $contentType = "text/html; charset=utf-8")
	{
		log_info('Template : ' .$path);
		header('Content-Type: '.$contentType);
		$vars = $vars + array(@form => $this->form, @messages => $this->messages, @user => $this->getUser());
		$this->renderer->render($path, $vars);
		return true;
	}
	
	/**
	 * Respond JSON.
	 * @param mixed $data
	 * @param string $contentType
	 * @return bool
	 */
	public function respondJSON($data, $converter = null, $contentType = "application/json; charset=utf-8")
	{
		header('Content-Type: '.$contentType);
		echo JSON::serialize($data, $converter ? $converter : $this->jsonConverter);
		return true;
	}

    /**
     * Respond JSON.
     * @param mixed $data
     * @param string $contentType
     * @return bool
     */
	public function respond($data, $contentType = "text/html; charset=utf-8")
	{
		header('Content-Type: '.$contentType);
		echo $data;
        return true;
	}

	/**
	 * @return \TxCore\User
	 */
	public function getUser()
	{
		$this->initSession();
		return (isset($_SESSION['USER']))
		? $_SESSION['USER']  : null; 
	}

	/**
	 * @param $user
	 */
	public function setUser($user)
	{
		$this->initSession();
		$_SESSION['USER']  = $user;
	}
	
	/**
	 * @param array
	 */
	public function saveMessages($messages)
	{
		$this->messages = $messages;
	}
	
	public function getCookie($name) 
	{
		if (isset($_COOKIE[$name])) {
			return $_COOKIE[$name];
		} else {
			return null;
		}
	}
	
	public function setCookie($name, $value, $expires)
	{
		setcookie($name, $value, $expires, '/');
	}
	
	/**
	 * Sets default JSON type converter using #respondJSON.
	 * 
	 * @param TypeConverter $typeConverter
	 */
	public function setJSONConverter(TypeConverter $converter)
	{
		$this->jsonConverter = $converter;
	}
	
	public function getInternalRequestURI()
	{
		if (isset($_SERVER['REDIRECT_URL']))
			return $_SERVER['REDIRECT_URL'];
		else
			return $this->getRequestURI();
	}
	
	public function getRequestURI()
	{
		return @$_SERVER['REQUEST_URI'];
	}
	
	public function respondDummyGif()
	{
		$this->setHeader('Content-Type', 'image/gif');
		echo base64_decode('R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw==');
	}
	
	public function respondFile($filepath, $type = 'application/octet-stream', $filename = null)
	{
		$this->setHeader('Content-Type', $type);
		if ($filename !== null) {
// 			$this->setHeader('Content-Disposition', 'attachment; filename*=UTF-8\'\''.urlencode($filename));
			$this->setHeader('Content-Disposition', 'attachment; filename*=UTF-8\'\''.rawurlencode($filename));
		}
		readfile($filepath);
	}

    /**
     *
     * @param string $action e.g.) "/index/show"
     * @param array $params
     * @throws Exception
     */
	public function forward($action, array $params = null)
	{
		$params = $params !== null ? $params : $this->getParams();
		
		// Remove first slash.
		$action = ltrim($action, '/');

		if (empty($action))
			$action .= 'index';
		if (str_ends_with('/', $action))
			$action .= 'index';
		if (strpos($action, '/') === false)
			$action = 'index/' . $action;
	
		$pathes = explode("/", $action);
		log_info(sprintf('Forwading : %s', $action));
		$count = count($pathes);

		$className = 'Action\\'.join('\\', array_map(function($p) { return str_camelize($p, UPPER_CASE); }, array_slice($pathes, 0, -1))).'Actions';
		$method = $pathes[$count - 1];
		$method = str_camelize(str_replace('.', ' ', $method), LOWER_CASE);	// xxx.json -> xxxJson
		$pathes = join('/', array_slice($pathes, 0, $count - 2));
		$filenName = str_replace('\\', '/', $className).'.php';
		include_once $filenName;
		$action_obj = new $className();
		if (!method_exists($action_obj, $method)) {
			throw new Exception(sprintf("class '%s' has no method : '%s'", $className, $method));
		}
		
		$actionInvokeArgs = array();
		$methodRef = new ReflectionMethod($action_obj, $method);
		$methodArgs = $methodRef->getParameters();
		for ($i = 1; $i < count($methodArgs); $i++) {
			if ($methodArgs[$i]->getClass() !== null) {
				if (is_subclass_of($methodArgs[$i]->getClass()->getName(), 'Barge\Web\Form')) {
					$actionInvokeArgs[] = $this->getForm($methodArgs[$i]->getClass()->getName());
				} else {
					// WARN :
				}
			} else {
				$underscoredName = str_underscore($methodArgs[$i]->getName());
				$value = isset($params[$underscoredName]) ? $params[$underscoredName] : null;
				if ($methodArgs[$i]->isDefaultValueAvailable()) {
					if ($value === null) {
						$value = $methodArgs[$i]->getDefaultValue();
					} else {
						$value = Form::convertType($value, gettype($methodArgs[$i]->getDefaultValue()));
					}
				}
				$actionInvokeArgs[] = $value;
			}
		}
		
		$chain = new ActionFilterChain(array_merge($this->globalFilters, 
			array(array(new ActionMethodFilter(), 'filter'))), $this, $action_obj, $method, $actionInvokeArgs);
		$chain->doChain();
	}
	
	
	private function initSession()
	{
		if (!self::$initSession) {
			session_start();
			self::$initSession = true;
		}
	}
	
	public function destroySession()
	{
		// Initialize the session.
		// If you are using session_name("something"), don't forget it now!
		$this->initSession();
		
		// Unset all of the session variables.
		$_SESSION = array();
		
		// Finally, destroy the session.
		session_destroy();
	}
	
	
	/**
	 * @param array $filters
	 */
	public function setGlobalFilters(array $filters)
	{
		$this->globalFilters = 	$filters;
	}
	
	
}
