<?php 
namespace Barge\Web;


use \Exception;
use \ReflectionMethod;
use \ReflectionClass;

class ActionMethodFilter 
{
	public function filter(ActionFilterChain $chain, ActionContext $ctx)
	{
		$refClass = new ReflectionClass($chain->getTargetAction());
		if ($refClass->hasMethod('filterAction')) {
			$refMethod = $refClass->getMethod('filterAction');
			$refMethod->invokeArgs($chain->getTargetAction(), array($chain, $ctx));
		} else {
			$chain->doChain();
		}
	}
		
}
