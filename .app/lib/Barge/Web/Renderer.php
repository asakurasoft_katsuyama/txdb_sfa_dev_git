<?php
namespace Barge\Web;

class Renderer
{
	private $smarty;

	public function __construct()
	{
		include_once 'Smarty/Smarty.class.php';
		$this->smarty = new \Smarty;

		$templateDirs = array();
		$customBaseDir = $this->getCustomBase();
		if ($customBaseDir) {
			$customTemplateDir = "{$customBaseDir}templates";
			$templateDirs[] = realpath($customTemplateDir);
		}
		$templateDirs[] = realpath('.app/templates');

		$this->smarty->setTemplateDir($templateDirs);
		$this->smarty->addPluginsDir(realpath('.app/smarty_plugins'));
		$this->smarty->setCompileDir(realpath('.app/smarty'));
//		$this->smarty->setCompileDir(get_temp_dir('smarty'));
		$this->smarty->registerDefaultTemplateHandler(
			array($this, '_handleDefaultTemplate'));
	}

	public function fetch($template, $vars = array())
	{
		$customBaseUrl = $this->getCustomBase(true);
		if ($customBaseUrl) {
			$this->smarty->assign('customBaseUrl', $customBaseUrl);
		}
		foreach ($vars as $key => $value)
			$this->smarty->assign($key, $value);
		return $this->smarty->fetch($template);
	}

	public function render($template, $vars = array())
	{
		$customBaseUrl = $this->getCustomBase(true);
		if ($customBaseUrl) {
			$this->smarty->assign('customBaseUrl', $customBaseUrl);
		}
		foreach ($vars as $key => $value)
			$this->smarty->assign($key, $value);
		$this->smarty->display($template);
	}

	public function _handleDefaultTemplate(
		$type, $name, &$content, &$modified, \Smarty $smarty)
	{
		if (str_ends_with('__extension.tpl', $name)) {
			return realpath('.app/templates/include/empty_extension.tpl');
		}
		return false;
	}

	private function getCustomBase($url = false) {
		$customCode = get_config('app', 'custom', null);
		if ($customCode == null) {
			return null;
		}
		if ($url) {
			$customUrl = get_config('app', 'custom_dir_url', null);
			return "{$customUrl}{$customCode}/";
		} else {
			$customDir = get_config('app', 'custom_dir', null);
			return "{$customDir}{$customCode}/";
		}
	}

}

