<?php 
namespace Barge\Web;

use Barge\Log\DC;

use \Exception;
use Barge\Web\ActionContext;

class FrontController 
{
	/**
	 * @var ActionContext
	 */
	private $context;
	
	/**
	 * @var callable
	 */
	private $router;
	
	/**
	 * @var callable
	 */
	private $errorHandler;
	
	/**
	 * @var array
	 */
	private $filters = array();
	
	public function __construct()
	{
		set_app_error_handler(array($this, 'handleException'));
		$this->context = new ActionContext();
		DC::push('uid', @$_COOKIE[session_name()]);
	}
	
	public function dispatch() 
	{
		try {
			log_info('>> Start dispatching for : '.$this->getRequestPath());
			$this->context->setGlobalFilters($this->filters);
			$this->context->forward($this->routing());
		} catch (Exception $e) {
			$this->handleException($e);
		}
	}
	
	public function setRouter($router) 
	{
		$this->router = $router;
	}
	
	public function routing()
	{
		$path = $this->getRequestPath();
		if ($this->router !== null) {
			return call_user_func($this->router, $path);
		} else {
			return $path;
		}
	}
	
	public function handleException($e)
	{
		if ($this->errorHandler === null) {
			$message = format_exception($e);
			log_error($message);
			
			http_response_code(500);
			echo "<pre>";
			echo htmlspecialchars($message);
			echo "</pre>";
		} else {
			call_user_func_array($this->errorHandler, array($this->context, $e));
		}
	}
	
	/**
	 * @return string e.g.) /user/auth
	 */
	private function getRequestPath()
	{
// 		$uri = getenv('REQUEST_URI');
		$uri = $_SERVER['REQUEST_URI'];
		if (($pos = strpos($uri, '?')) !== false) {
			$uri = substr($uri, 0, $pos);
		}
		return $uri;
	}
	
	/**
	 * @param callable $filter
	 */
	public function addFilter($filter) 
	{
		$this->filters[] = $filter;
	}
	
	/**
	 * @param callable $handler
	 */
	public function setErrorHandler($handler)
	{
		$this->errorHandler = $handler;
	}
}

