<?php 
namespace Barge\Web;

use TxCore\Util;

use \Exception;
use \ReflectionMethod;

class ActionFilterChain
{
	/**
	 * Appling action filters.
	 * @var array
	 */
	private $filters = array();

	/**
	 * @var ActionContext
	 */
	private $context;

	/**
	 * @var object
	 */
	private $targetAction;

	/**
	 * @var string
	 */
	private $targetMethod;

	/**
	 * @var array
	 */
	private $params = array();

	/**
	 * @var int
	 */
	private $index = 0;

	/**
	 *
	 * @param $filters Appling filters.
	 * @param $context
	 * @param $targetAction Invoking action.
	 * @param $targetMethod Invoking action method.
	 * @param array $params The additional parameters.
	 */
	public function __construct(array $filters, ActionContext $context,
		$action, $method, array $params)
	{
// 		$this->log = LogFactory::getLogger(get_called_class());
		$this->context = $context;
		$this->filters = $filters;
		$this->targetAction = $action;
		$this->targetMethod = $method;
		$this->params = $params;
	}

	/**
	 * Do chain.
	 */
	public function doChain()
	{
		if ($this->index < count($this->filters)) {
			$filter = $this->filters[$this->index];
			$this->index++;
			log_debug(sprintf('Invoke action filter : %s', to_string($filter)));
			call_user_func_array($filter, array($this, $this->context));
		} else {
			// Invoke action.
			$this->executeAction();
		}
	}

	/**
	 * Invoke action.
	 */
	public function executeAction()
	{
		// Note : No-checking
		call_user_func_array(array($this->targetAction, $this->targetMethod), array_merge(array($this->context), $this->params));
	}

	/**
	 * Returns invoking action name.
	 * @return string
	 */
	public function getTargetAction()
	{
		return $this->targetAction;
	}

	/**
	 * Returns invoking action method.
	 * @return string
	 */
	public function getTargetMethod()
	{
		return $this->targetMethod;
	}

}

