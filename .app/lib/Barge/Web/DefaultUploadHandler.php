<?php 
namespace Barge\Web;

use TxCore\User;
use TxCore\Util;

require_once 'Upload/UploadHandler.php';

class DefaultUploadHandler extends \UploadHandler 
{
	
	protected $user = null;
	
	function __construct($user, $options, $initialize = true) 
	{
		$this->user = $user;
		$options['image_versions'] = array();
		$options['script_url'] = $this->get_full_url();
		$locale = get_config('app', 'locale');
		if ($locale) {
			setlocale(LC_ALL, $locale);
		}
		parent::__construct($options, $initialize);
		
	}
	
// 	protected function get_user_id() {
// 		return $this->user_id;
// 	}
	
	public function createTempFile($ext)
	{
		$fileName = Util::getUniqeFileName($ext);
		return $fileName;
	}
	
	protected function get_unique_filename($file_path, $name, $size, $type, $error,
			$index, $content_range) {
		
		$extension = pathinfo($name, PATHINFO_EXTENSION);
		if (!$extension) { $extension = 'dat';}
		
		return $this->createTempFile($extension);
	}
	
	protected function handle_file_upload($uploaded_file, $name, $size, $type, $error,
			$index = null, $content_range = null) {
		$file = parent::handle_file_upload($uploaded_file, $name, $size, $type, $error);
		$file->originalName = $name;
		return $file;
	}
	
	protected function get_full_url() {
		$https = !empty($_SERVER['HTTPS']) && strcasecmp($_SERVER['HTTPS'], 'on') === 0;
		return
		($https ? 'https://' : 'http://').
		(!empty($_SERVER['REMOTE_USER']) ? $_SERVER['REMOTE_USER'].'@' : '').
		(isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : ($_SERVER['SERVER_NAME'].
				($https && $_SERVER['SERVER_PORT'] === 443 ||
						$_SERVER['SERVER_PORT'] === 80 ? '' : ':'.$_SERVER['SERVER_PORT']))).
						//substr($_SERVER['SCRIPT_NAME'],0, strrpos($_SERVER['SCRIPT_NAME'], '/'));
						//substr($_SERVER['REQUEST_URI'],0, strrpos($_SERVER['REQUEST_URI'], '/'));
						$_SERVER['REQUEST_URI'];
	}
	
	protected function set_additional_file_properties($file) {
		$file->deleteUrl = $this->options['script_url']
		.$this->get_query_separator($this->options['script_url'])
		.$this->get_singular_param_name()
		.'='.rawurlencode($file->name);
		$file->deleteType = $this->options['delete_type'];
		if ($file->deleteType !== 'DELETE') {
			$file->deleteUrl .= '&_method=DELETE';
		}
		if ($this->options['access_control_allow_credentials']) {
			$file->deleteWithCredentials = true;
		}
	}
	
	
	
	public function getUploadDirectoryPath() {
		return $this->get_upload_path();
	}
	
}


