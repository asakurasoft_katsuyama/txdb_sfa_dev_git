<?php 
namespace Barge\Web;

use Barge\Web\FormFile;

/**
 * Form.
 */
class Form
{
	/**
	 * @param array $params The parameters to populate.
	 */
	public function populate($params)
	{
		$this->populateBean($this, $params, 3);
	}

	/**
	 * @param mixed $form
	 * @param array $map
	 */
	private function populateBean($bean, $params, $depth)
	{
		if ($depth <= 0) {
			return;
		}
		
		foreach ($params as $name => $value) {
			$cameledName = str_camelize($name, LOWER_CASE);
			if (property_exists(get_class($this), $cameledName)) {
				$initial = $this->{$cameledName};
				$propType = gettype($initial);
				$value = self::convertType($value, $propType);
				$this->{$cameledName} = $value;
			}
		}
	}

	/**
	 * @param mixed  $value
	 * @param mixed $type
	 * @return mixed Converted value.
	 */
	public static function convertType($value, $type)
	{
		if ($type === null)
			return $value;
		switch (strtolower($type)) {
			case 'string':
				break;
			case 'integer':
			case 'int':
				$value = intval($value);
				break;
			case 'double':
				$value = doubleval($value);
				break;
			case 'float':
				$value = floatval($value);
				break;
			case 'boolean':
			case 'bool':
				$value = in_array($value, array('yes', 'on', 'true', '1', true), true);
				break;
			case 'file':
			case 'formfile':
				$value = new FormFile();
				break;
			case 'array':
				if (! is_array($value)) {
					$value = array($value);
				}
				break;
			case 'datetime':
				// TODO :
				break;
		}

		return $value;
	}
}

