<?php
namespace Barge\DB;

use \Exception;
use \PDO;

use Barge\Proxy;

/**
 * PDOラッパー
 */
class DBClient
{
	private static $cache = array();
	private $pdo;
	private $commitHook;
	private $hasTx =  false;

	/**
	 * @param unknown_type $ds
	 * @return Barge\DBClient;
	 */
	public static function connect($ds = 'default')
	{ 
		if (!isset(self::$cache[$ds]))
			self::$cache[$ds] = new self($ds);
		return self::$cache[$ds];
	}
	
	public function __construct($ds) 
	{
		$this->pdo = new PDO(
			get_config('/db/'.$ds, 'uri'), 
			get_config('/db/'.$ds, 'user_name'), 
			get_config('/db/'.$ds, 'password'));
		$this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$this->pdo->setAttribute(PDO::ATTR_CASE, PDO::CASE_LOWER);
		$this->pdo->setAttribute(PDO::ATTR_AUTOCOMMIT, true);
		if ( defined('PDO::MYSQL_ATTR_MAX_BUFFER_SIZE') ) {
			$this->pdo->setAttribute(PDO::MYSQL_ATTR_MAX_BUFFER_SIZE, 100 * 1024 * 1024);
		}
		
		$collation = get_config('/db/'.$ds, 'collation');
		$charset = get_config('/db/'.$ds, 'charset');
		$names = "set names '$charset'".
			( ! is_null($collation) ? " collate '$collation'" : '');
		$this->pdo->prepare($names)->execute();
	}

	public function beginTrans()
	{
		if (!$this->hasTx) {
			log_debug('Begin transaction.');
			$this->pdo->beginTransaction();
			$this->hasTx = true;
		}
	}
	
	public function commit()
	{
		log_debug('Commit transaction.');
		$this->pdo->commit();
		$this->hasTx = false;
		
		if ($this->commitHook !== null) {
			call_user_func_array($this->commitHook, array());
		}
	}
	
	public function close()
	{
		// Do nothing.
	}
	
	public function rollback()
	{
		$this->pdo->rollBack();
		$this->hasTx = false;
	}
	
	public function fetch($query, $params, $callable)
	{
// 		$callable =  ? 
// 			array($callable, 'handle') : $callable;
		 
		// Convert to array, If parameter is object.
		// 		$params = is_object($params) ? BeanUtil::toArray($params) : $params;
		log_debug(sprintf('Query : [%s] params : %s', $query, to_string($params)));
	
		$args = array();
		foreach ($params as $key => $value) {
			if (is_array($value) || is_object($value)) 
				throw new \Exception(sprintf('Invalid parameter type : %s', gettype($value)));
			$args[is_int($key) ? $key : ':'.$key] = $value;
		}
	
		$start = microtime(true);
	
		$sth = $this->pdo->prepare($query);
		$sth->execute($args);
		log_debug(sprintf('(%5f[ms])', (microtime(true) - $start)));
	
		$list = array();
		$index = 0;
		while ($data = $sth->fetch(PDO::FETCH_OBJ)) {
			$ret = call_user_func_array($callable, array($data, $index));
			if ($ret === false)
				break;
			$index++;
		}
	
		return $list;
	}
	
	public function query($query, $params = array(), $scalar = false, $file = false)
	{
		if ($file) {
			return $this->fetchFile($query, $params);
		}
		
		$result = array();
		$this->fetch($query, $params, function($data) use (&$result) {
			$result[] = $data;
		});
		
		if ($scalar) {
			if (count($result) > 0) {
				$props = get_object_vars($result[0]);
				return current($props);
			} else {
				return null;				
			}
		} else {
			return $result;
		}
	}
	
	/**
	 * @param string $query 'select single_lob_column from ... '
	 * @param array $params
	 * @throws \Exception
	 * @return fp
	 */
	public function fetchFile($query, $params)
	{
		log_debug(sprintf('Query : [%s] params : %s', $query, to_string($params)));
	
		$args = array();
		foreach ($params as $key => $value) {
			if (is_array($value) || is_object($value)) 
				throw new \Exception(sprintf('Invalid parameter type : %s', gettype($value)));
			$args[is_int($key) ? $key : ':'.$key] = $value;
		}
	
		$start = microtime(true);
	
		$sth = $this->pdo->prepare($query);
		
		$sth->execute($args);
		$sth->bindColumn(1, $fp, PDO::PARAM_LOB);
		log_debug(sprintf('(%5f[ms])', (microtime(true) - $start)));
	
		$sth->fetch(PDO::FETCH_BOUND);
	
		return $fp;
	}
	
	public function update($query, $params = array())
	{
		// Convert to array, If parameter is object.
// 		$params = is_object($params) ? BeanUtil::toArray($params) : $params;
	
		log_debug(sprintf('Query : [%s] params : %s', $query, to_string($params)));
	
		$sth = $this->pdo->prepare($query);
		foreach ($params as $key => &$value) {
			if (!is_resource($value)) {
				$sth->bindParam(is_int($key) ? $key+1 : ':'.$key, $value);
			} else {
				$sth->bindParam(is_int($key) ? $key+1 : ':'.$key, $value, PDO::PARAM_LOB);
			}
		}
		
		$start = microtime(true);
		
		$sth->execute();
		log_debug(sprintf('(%5f[ms])', (microtime(true) - $start)));
		return $sth->rowCount();
	}
	
	public function lastInsertId($name = null)
	{
		return $this->pdo->lastInsertId($name);
	}
	
	public function registerCommitHook($callable)
	{
		return $this->commitHook = $callable;
	}
}

