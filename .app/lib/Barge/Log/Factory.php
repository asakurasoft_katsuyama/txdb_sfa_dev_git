<?PHP
namespace Barge\Log;

use Barge\Log\Logger;

class Factory
{
	const DEFAULT_FORMAT = "{timestamp} {level} - {message}\n";
	const DEFAULT_LEVEL = 'debug';
	const ROOT_LOGGER = "root";

	/**
	 * @var array
	 */
	private static $loggers = array();

	/**
	 * @var array
	 */
	private static $appenders = array();

	/**
	 * @var array
	 */
	private static $appenderPresets = array(
		'console' => 'Barge\Log\ConsoleAppender',
		'file' => 'Barge\Log\FileAppender',
		'daily' => 'Barge\Log\DailyFileAppender',
		);

	/**
	 * @param string $name The logger name.
	 * @return Logger
	 */
	public static function getLogger($name = self::ROOT_LOGGER)
	{
// 		// Initialize appenders.
// 		if (self::$appenders === null) {
// 			$appenders = array();
// 			$configs = get_config('/log/appender/', array());
// 			foreach ($configs as $key => $temp) {
// 				$appenders[$key] = self::instanceAppender($temp);
// 			}
// 			self::$appenders = $appenders;
// 		}

		// Initialize specified logger.
		if (! isset(self::$loggers[$name])) {
			$logger = null;
			$appendar = null;
			$config = get_config('/log/logger/'.$name);
			if ($config !== null) {
				$appenders = array();
				foreach (explode(',', array_get($config, 'appenders')) as $key) {
					if (isset(self::$appenders[$key])) {
						$appenders[] = self::$appenders[$key];
					} else {
						$appenders[] = self::$appenders[$key] 
							= self::instanceAppender(get_config('/log/appender/'.$key));
					}
				}
				$logger = new Logger(
					isset($config['priority']) ? $config['priority'] : self::DEFAULT_LEVEL,
					isset($config['format']) ? $config['format'] : self::DEFAULT_FORMAT,
					$appenders);
			}
			if ($logger === null) {
				$logger = new Logger();
			}
			self::$loggers[$name] = $logger;
		}

		return self::$loggers[$name];
	}

	private static function instanceAppender(array $config)
	{
		$type = @$config['type'];
		if (!empty($type) && isset(self::$appenderPresets[$type])) {
			$class = self::$appenderPresets[$type];
		} else {
			$class = $type;
		}
		$appendar = null;
		if ($class !== null) {
			$appendar = new $class;
			foreach ($config as $key => $value) 
				$appendar->{$key} = $value;
		}
		return $appendar;
	}
	
}

