<?PHP

namespace Barge\Log;

/**
 * Diagnostic context
 */
class DC
{

	/**
	 * @var array
	 */
	protected static $values = array(); 
	

	public static function push($name, $value) 
	{
		self::$values[$name] = $value;
	}
	
	public static function pop($name)
	{
		unset(self::$values[$name]);
	}
	
	public static function get($name)
	{
		return isset(self::$values[$name]) ? self::$values[$name] : null;
	}
}

