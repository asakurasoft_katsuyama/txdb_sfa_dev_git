<?PHP
namespace Barge\Log;

use Barge\Log\FileAppender;


class DailyFileAppender extends FileAppender
{
	const DEFAULT_DATE_PATTERN = 'Y-m-d';

	/**
	 * @var string
	 */
	public $datePattern;

	/**
	 * @var bool
	 */
	private $inited = false;

	public function __construct()
	{
		parent::__construct();
	}

	public function append($message)
	{
		if (!$this->inited) {
			$datePattern = $this->datePattern ? $this->datePattern: self::DEFAULT_DATE_PATTERN;
			$this->file .= '_' . date($datePattern);
			$this->inited = true;
		}
		parent::append($message);
	}
}
