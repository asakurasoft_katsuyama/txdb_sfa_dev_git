<?PHP
namespace Barge\Log;

interface Appender
{
	public function append($message);
}
