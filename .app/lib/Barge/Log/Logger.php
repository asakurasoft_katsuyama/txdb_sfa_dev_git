<?PHP

namespace Barge\Log;


class Logger
{


	/**
	 * @var bool
	 * @since 1.0 2006/10/18
	 */
	protected $infoEnabled = false;

	/**
	 * @var bool
	 * @since 1.0 2006/10/18
	 */
	protected $fatalEnabled = false;

	/**
	 * @var bool
	 * @since 1.0 2006/10/18
	 */
	protected $errorEnabled = false;

	/**
	 * @var bool
	 * @since 1.0 2006/10/18
	 */
	protected $debugEnabled = false;

	/**
	 * @var bool
	 * @since 1.0 2006/10/18
	 */
	protected $warnEnabled = false;

	/**
	 * @var array
	 */
	protected $appenders;

	/**
	 * @var string
	 */
	protected $encoding;
	
	/**
	 * @var Formatter
	 */
	protected $formatter;
	
	/**
	 * Constructor.
	 *
	 * @param string $priority The log level.
	 * @param string $format Log format.
	 * @param $appenders
	 */
	public function __construct($priority = null, $format = null, array $appenders = array())
	{
		$this->formatter = new Formatter($format);
		$priority = $priority ?  $priority: 'debug';
		if (count($appenders) > 0) {
			$this->appenders = $appenders;
			switch ($priority) {
				case 'debug' :
					$this->debugEnabled = true;
				case 'debug' :
				case 'info' :
					$this->infoEnabled = true;
				case 'debug' :
				case 'info' :
				case 'warn' :
					$this->warnEnabled = true;
				case 'debug' :
				case 'info' :
				case 'warn' :
				case 'error' :
					$this->errorEnabled = true;
				case 'debug' :
				case 'info' :
				case 'warn' :
				case 'error' :
				case 'fatal' :
					$this->fatalEnabled = true;
			}
		}
	}

	/**
	 * @return bool
	 */
	public function isInfoEnabled()
	{
		return $this->infoEnabled;
	}

	/**
	 * @return bool
	 */
	public function isDebugEnabled()
	{
		return $this->debugEnabled;
	}
	
	/**
	 * @return bool
	 */
	public function _setDebugEnabled($value)
	{
		$this->debugEnabled = $value;
	}

	/**
	 * @return bool
	 */
	public function isErrorEnabled()
	{
		return $this->errorEnabled;
	}

	/**
	 * @return bool
	 */
	public function isFatalEnabled()
	{
		return $this->fatalEnabled;
	}

	/**
	 * @return bool
	 */
	public function isWarnEnabled()
	{
		return $this->warnEnabled;
	}

	/**
	 * @param mixed $message
	 */
	public function info($message)
	{
		if ($this->isInfoEnabled())
			$this->append($message, 'info');
	}

	/**
	 * @param mixed $message
	 */
	public function warn($message)
	{
		if ($this->isWarnEnabled())
			$this->append($message, 'warn');
	}

	/**
	 * @param mixed $message
	 */
	public function debug($message)
	{
		if ($this->isDebugEnabled())
			$this->append($message, 'debug');
	}

	/**
	 * @param mixed $var
	 */
	public function error($message)
	{
		if ($this->isErrorEnabled())
			$this->append($message, 'error');
	}

	/**
	 * @param mixed $message
	 */
	public function fatal($message)
	{
		if ($this->isFatalEnabled())
			$this->append($message, 'fatal');
	}

	/**
	 * @param mixed $message
	 * @param string $level
	 */
	protected function append($message, $level)
	{
		$rendered = $this->formatter->format($message, $level);
		foreach ($this->appenders as $appender) {
			$appender->append($rendered);
		}
	}
}

class Formatter
{
	protected $level;
	protected $message;
	
	/**
	 * @var string
	 */
	protected $format;
	
	
	public function __construct($format) 
	{
		$this->format = $format;
	}
	
	public function format($message, $level) 
	{
		$this->message = $message;
		$this->level = $level;
		return preg_replace_callback('/{([^}]+)}/',
			array($this, '_callback'), $this->format);
	}
	
	public function _callback($matches)  
	{
		$matches2 = array();
		if ($matches[1] === 'level') {
			return sprintf("%-5s", strtoupper($this->level));
		} else if (preg_match('/^timestamp(:\'?(.*?)\'?)?$/', $matches[1], $matches2)) {
			return @date($matches2[2] ? $matches2[2] : "Y/m/d H:i:s");
		} else if ($matches[1] === 'message') {
			return $this->message;
		} else if ($matches[1] === 'pid') {
			return getmypid();
		} else if ($matches[1] === 'lf') {
			return "\n";
		} else if (str_starts_with('dc:', $matches[1])) {
			return DC::get(substr($matches[1], 3));
		} else {
			return "";
		}
	}
}

