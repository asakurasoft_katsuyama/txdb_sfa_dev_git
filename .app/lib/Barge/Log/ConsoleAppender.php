<?PHP
namespace Barge\Log;

use Barge\Log\Appender;

class ConsoleAppender implements Appender
{
	/**
	 * @var string
	 */
	public $encoding;

	/**
	 * @param mixed $message
	 */
	public function append($message)
	{
		// Since 1.1
		if (!is_empty($this->encoding)) {
			$message = mb_convert_encoding($message, $this->encoding, 'utf-8');
		}

		echo $message;
	}
}
