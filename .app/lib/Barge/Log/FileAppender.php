<?PHP
namespace Barge\Log;

use Barge\Log\Appender;

class FileAppender implements Appender
{
	const DEFAULT_MAXSIZE = 2485760;	// = 2M

	/**
	 * @var string
	 */
	public $file;

	/**
	 * @var int
	 */
	public $maxSize = self::DEFAULT_MAXSIZE;

	/**
	 * @var bool
	 */
	public $append;

	/**
	 * @var stream
	 */
	protected $fp = false;

	/**
	 * @var long
	 */
	protected $fsize = 0;


	public function append($message)
	{
		$path = to_absolute($this->file);
		if ($this->fp === false) {
			$append = !in_array($this->append, array('0', 'false', 'FALSE'));
			if ($append) {
				$this->fp = @fopen($path, 'a');
			} else {
				$this->fp = @fopen($path, 'w');
			}
			if ($this->fp !== false) {
				$this->fsize = filesize($path);
			}
		}

		if ($this->maxSize && $this->maxSize < $this->fsize) {
			fclose($this->fp);
			$this->fp = @fopen($path, 'w');
			$this->fsize = 0;
		}

		if ($this->fp === false) {
			return;
		}

		$this->fsize += fwrite($this->fp, $message);
	}

	public function __destruct()
	{
		if ($this->fp !== false) {
			fflush($this->fp);
			fclose($this->fp);
			$this->fp = false;
		}
	}
}

