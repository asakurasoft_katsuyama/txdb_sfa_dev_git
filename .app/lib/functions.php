<?php 

// -----------------------------
// Misc functions
// -----------------------------
// function is_absolute($path)
// {
// 	return ($path[0] === '/' || @$path[1] === ':');
// }
function is_empty($value)
{
// 	return empty($path);
	return $value === null || $value === '' || (is_array($value) && count($value) === 0);
}
function is_not_empty($path)
{
	return !is_empty($path);
}
function is_blank($path)
{
	return $path === null || trim($path) === '';
}
function is_cjk_blank($path)
{
	return $path === null || trim_cjk($path) === '';
}
function get_message($key, $args = array()) 
{
	$format = get_config('messages', $key, $key);
// var_dump(array_slice(func_get_args(), 1));
	return call_user_func_array('sprintf', 
		array_merge(array($format), $args));
}
function intval_to_null($var)
{
	return $var === null ? $var : intval($var);
}
function is_not_blank($path)
{
	return !is_blank($path);
}
function to_string($obj, $format = false)
{
	return __to_string($obj, $format, 0);
}
function __to_string($obj, $format, $level)
{
	$buf = array();
	if ($level > 10)  {
		$buf[] = '...';
		return;
	}
	if ($obj === null) {
		$buf[] = 'null';
	} else if (is_bool($obj)) {
		$buf[] = ($obj ? 'true' : 'false');
	} else if (is_string($obj)) {
		$buf[] = ('"'.$obj.'"');
	} else if (is_numeric($obj)) {
		$buf[] = ($obj);
	} else if (is_array($obj) || is_object($obj)) {
		if ($obj instanceof stdClass) {
			$obj = (array)$obj;
		} else if (is_object($obj)) {
			$refClass = new ReflectionClass($obj);
			$structured = array();
			foreach ($refClass->getProperties() as $refProp) {
				$refProp->setAccessible(true);
				$structured[$refProp->getName()] = $refProp->getValue($obj);
			}
			$obj = $structured;
			
		}
		$firstdone = false;
		$buf[] = ('[');
		if ($format)
			$buf[] = ("\n");
		foreach ($obj as $key => $value) {
			if ($firstdone) {
				$buf[] = (',');
				if ($format)
					$buf[] = ("\n");
			}
			if (is_numeric($key)) {
				$buf[] = __to_string($value, $format, $level + 1);
			} else {
				$buf[] = $key.'=>'.__to_string($value, $format, $level + 1);
			}
			$firstdone = true;
		}
		$buf[] = (']');
	}
	return join('', $buf);
}

// -----------------------------
// System functions
// -----------------------------
function concat_path(/*...*/)
{
	$args = func_get_args();
	$path = _concat_path($args[0], $args[1]);
	for ($i = 2; $i < count($args); $i++) {
		$path = _concat_path($path, $args[$i]);
	}
	return $path;
}
function _concat_path($basedir, $path)
{
	if (str_ends_with('/', $basedir)) {
		$basedir = substr($basedir, 0, -1);
	}
	if (str_starts_with('/', $path)) {
		$path = substr($path, 1);
	}
	return $basedir . '/' . $path;
}
function get_config($section, $key = null, $default = null) 
{
	$config = get_config_all();
	
	if (!str_starts_with('/', $section))
		$section = '/'.$section;
	if (!str_ends_with('/', $section))
		$section = $section.'/';
// 	if (!str_starts_with('/', $key))
// 		$key = '/'.$key;
// 	$p = strrpos($key, '/');
// 	list ($section, $name) = array(substr($key, 0, $p), substr($key, $p + 1));
	if ($key) {
		return isset($config[$section][$key]) ? $config[$section][$key] : $default;
	} else {
		return isset($config[$section]) ? $config[$section] : $default;
	}
}


function get_config_all()
{
	static $config = null;
	if ($config === null) {
		$config_path = defined('CONFIG_PATH') ?  CONFIG_PATH : dirname(__FILE__).'/../config.ini';
		$work = parse_ini_file($config_path, true);
		$normalized = array();
		foreach ($work as $key => $value) {
			if (!str_starts_with('/', $key))
				$key = '/'.$key;
			if (!str_ends_with('/', $key))
				$key = $key.'/';
			$normalized[$key] = $value;
		}
		$config = $normalized;
		if (file_exists(dirname(__FILE__).'/../messages.ini')) {
			$config['/messages/'] = parse_ini_file(dirname(__FILE__).'/../messages.ini', false);
		}
	}
	return $config;
}

function get_temp_dir($category = null)
{
	$dir = sys_get_temp_dir();
	if ($category !== null) {
		$subdir = $dir.'/'.$category;
		if (!file_exists($subdir)) {
			mkdir($subdir);
		}
		return realpath($subdir);
	} else {
		return realpath($dir);
	}
}

function is_absolute($path)
{
	return ($path[0] === '/' || @$path[1] === ':');
}

function to_absolute($path = '.')
{
	$basedir = realpath('.');
	if (is_absolute($path)) {
		return $path;
	} else if ($path === '.') {
		return $basedir;
	} else {
		return (substr($basedir, -1, 1) === '/' ? '' : $basedir).'/'.($path === '/' ? '' : $path);
	}
}
// -----------------------------
// Logging alias.
// -----------------------------
function log_debug($message) 
{
	$log = Barge\Log\Factory::getLogger();
	$log->debug($message);
}

function log_info($message)
{
	$log = Barge\Log\Factory::getLogger();
	$log->info($message);
}

function log_error($message)
{
	$log = Barge\Log\Factory::getLogger();
	$log->error($message);
}

function log_warn($message)
{
	$log = Barge\Log\Factory::getLogger();
	$log->warn($message);
}

function format_exception($e)
{
	$e->getCode();
	$bt = $e->getTrace();
	$ret = get_class($e) . ': ' . $e->getMessage() . "\n";

	$caller = array('file'=>basename($e->getFile()), 'line'=>$e->getLine());
	$stack = null;
	foreach ($bt as $stack) {
		$filename = basename(isset($caller['file']) ? $caller['file'] : '');
		$ret .= sprintf("    at %s%s%s(%s:%s)\n",
			isset($stack['class']) ? $stack['class'] : '',
			isset($stack['type']) ? $stack['type'] : '',
			$stack['function'],
			$filename,
			isset($caller['line']) ? $caller['line'] : '');
		$caller = $stack;
	}
	$filename = basename(isset($stack['file']) ? $stack['file'] : '');
	$ret .= sprintf("    at <main>(%s:%s)\n",
		$filename,
		isset($stack['line']) ? $stack['line'] : '');

	$e = $e->getPrevious();
	if ($e !== null) {
		$ret .= '  Caused by: ';
		$ret .= format_exception($e);
	}
	return $ret;
}

// -----------------------------
// String functions
// -----------------------------
define('UPPER_CASE', 1);
define('LOWER_CASE', 2);

function str_camelize($string, $fistCase = UPPER_CASE)
{
	if (empty($string))
		return $string;

	$cameled = '';
	$words = preg_split('/[_\-\s]+/', $string);
	if (count($words) > 1) {
		foreach ($words as $word) {
			$cameled .= strtoupper(substr($word, 0, 1)) . strtolower(substr($word, 1));
		}
	} else {
		$cameled = $string;
	}
	return ($fistCase == UPPER_CASE ? strtoupper($cameled[0]) :
		strtolower(substr($cameled, 0, 1))) . substr($cameled, 1);
}

function str_underscore($value)
{
	return preg_replace(
		'/([a-z0-9])([A-Z])/e', '\'$1\' . \'_\' .strtolower(\'$2\')', $value);
}

function str_ends_with($ends, $value)
{
	return (mb_substr($value, -1 * mb_strlen($ends)) === $ends);
}

function str_starts_with($starts, $value)
{
	return (mb_substr($value, 0, mb_strlen($starts)) === (string)$starts);
}

function empty_to_null($value)
{
	if (is_empty($value)) {
		return null;
	} else {
		return $value;
	}
}
function trim_to_null($value)
{
	if ($value === null)
		return null;
	$value = trim($value);
	if ($value === '') {
		return null;
	} else {
		return $value;
	}
}

function mb_truncate($string, $length = 80, $etc = '...') 
{
	if ($length == 0) {
		return '';
	}

	if (mb_strlen($string) > $length) {
		return mb_substr($string, 0, $length).$etc;
	} else {
		return $string;
	}
}
// -----------------------------
// Array functions
// -----------------------------
function array_get($array, $key, $default = null)
{
	$ret = null;
	if (isset($array[$key])) {
		$ret = $array[$key];
	}

	if ($ret === null) {
		return $default;
	} else {
		return $ret;
	}
}
function var_dump_json($data)
{
	if (php_sapi_name() != 'cli') {
		echo '<pre>';
	}
	echo json_encode($data, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE)."\n";
	if (php_sapi_name() != 'cli') {
		echo '</pre>';
	}
}
function array_peek($arr)
{
	if (count($arr) == 0)
		return false;
	return $arr[count($arr) - 1];
}
function array_push_all(&$arr, $arr2)
{
	$arr = array_merge($arr, $arr2);
}

function array_each(array $array, $callable)
{
	foreach ($array as $key => $value) {
		if (call_user_func_array($callable, array($value, $key)) === false) {
			break;
		}
	}
}

function array_grep(array $array, $callable)
{
	$found = array();
	foreach ($array as $key => $value) {
		if (call_user_func_array($callable, array($value, $key)) === true) {
			if (is_int($key)) {
				$found[] = $value;
			} else {
				$found[$key] = $value;
			}
		}
	}
	return $found;
}

function compare_int($a, $b) 
{
	if ($a == $b) {
		return 0;
	}
	return ($a < $b) ? -1 : 1;
}

// see http://www.php.net/manual/en/function.http-response-code.php
if (!function_exists('http_response_code')) {
	function http_response_code($code = NULL) {

		if ($code !== NULL) {

			switch ($code) {
				case 100: $text = 'Continue'; break;
				case 101: $text = 'Switching Protocols'; break;
				case 200: $text = 'OK'; break;
				case 201: $text = 'Created'; break;
				case 202: $text = 'Accepted'; break;
				case 203: $text = 'Non-Authoritative Information'; break;
				case 204: $text = 'No Content'; break;
				case 205: $text = 'Reset Content'; break;
				case 206: $text = 'Partial Content'; break;
				case 300: $text = 'Multiple Choices'; break;
				case 301: $text = 'Moved Permanently'; break;
				case 302: $text = 'Moved Temporarily'; break;
				case 303: $text = 'See Other'; break;
				case 304: $text = 'Not Modified'; break;
				case 305: $text = 'Use Proxy'; break;
				case 400: $text = 'Bad Request'; break;
				case 401: $text = 'Unauthorized'; break;
				case 402: $text = 'Payment Required'; break;
				case 403: $text = 'Forbidden'; break;
				case 404: $text = 'Not Found'; break;
				case 405: $text = 'Method Not Allowed'; break;
				case 406: $text = 'Not Acceptable'; break;
				case 407: $text = 'Proxy Authentication Required'; break;
				case 408: $text = 'Request Time-out'; break;
				case 409: $text = 'Conflict'; break;
				case 410: $text = 'Gone'; break;
				case 411: $text = 'Length Required'; break;
				case 412: $text = 'Precondition Failed'; break;
				case 413: $text = 'Request Entity Too Large'; break;
				case 414: $text = 'Request-URI Too Large'; break;
				case 415: $text = 'Unsupported Media Type'; break;
				case 500: $text = 'Internal Server Error'; break;
				case 501: $text = 'Not Implemented'; break;
				case 502: $text = 'Bad Gateway'; break;
				case 503: $text = 'Service Unavailable'; break;
				case 504: $text = 'Gateway Time-out'; break;
				case 505: $text = 'HTTP Version not supported'; break;
				default:
					exit('Unknown http status code "' . htmlentities($code) . '"');
					break;
			}

			$protocol = (isset($_SERVER['SERVER_PROTOCOL']) ? $_SERVER['SERVER_PROTOCOL'] : 'HTTP/1.0');

			header($protocol . ' ' . $code . ' ' . $text);

			$GLOBALS['http_response_code'] = $code;

		} else {

			$code = (isset($GLOBALS['http_response_code']) ? $GLOBALS['http_response_code'] : 200);

		}

		return $code;

	}
}

// see http://www.php.net/manual/ja/function.filesize.php
function file_size_convert($bytes)
{
	$bytes = floatval($bytes);
	$arBytes = array(
			0 => array(
					"UNIT" => "TB",
					"VALUE" => pow(1024, 4)
			),
			1 => array(
					"UNIT" => "GB",
					"VALUE" => pow(1024, 3)
			),
			2 => array(
					"UNIT" => "MB",
					"VALUE" => pow(1024, 2)
			),
			3 => array(
					"UNIT" => "KB",
					"VALUE" => 1024
			),
			4 => array(
					"UNIT" => "B",
					"VALUE" => 1
			),
	);

	foreach($arBytes as $arItem)
	{
		if($bytes >= $arItem["VALUE"])
		{
			$result = $bytes / $arItem["VALUE"];
			$result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
			break;
		}
	}
	return $result;
}

function get_ver()
{
	return get_config('updator','version');
}

function get_ver_query()
{
	return urlencode(get_ver());
}
