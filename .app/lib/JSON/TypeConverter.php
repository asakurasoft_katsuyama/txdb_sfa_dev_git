<?php 
namespace JSON;

/**
 * JSON type converter interface.
 */
interface TypeConverter
{
	/**
	 * @param mixed $value
	 * @return mixed
	 */
	public function convert($value, $parent); 
}
