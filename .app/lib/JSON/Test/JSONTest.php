<?php
namespace JSON\Test;

use \UnitTestCase;

use JSON\TypeConverter;
use JSON\JSON;

class JSONTest extends UnitTestCase
{
	public function testSerialize() 
	{
		$this->assertIdentical('null', JSON::serialize(null));
		$this->assertIdentical('{"a":1}', JSON::serialize(array('a'=>1)));
		$this->assertIdentical('{"a":1}', JSON::serialize((object)array('a'=>1)));
		$this->assertIdentical('[1,2,3]', JSON::serialize(array(1, 2, 3)));
		$this->assertIdentical('{}', JSON::serialize(new TestObject()));
		$this->assertIdentical('{"c":false}', JSON::serialize((object)array('c'=>new TestObject()), new TypeConverterImpl()));
	}
	
}

class TestObject {
	
}
class TypeConverterImpl implements TypeConverter
{
	public function convert($value, $parent) 
	{
		if ($value instanceof TestObject) {
			return false;
		}
		return $value;
	}
}
