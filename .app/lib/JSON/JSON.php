<?php 

namespace JSON;

/**
 * Interface based JSON encoder.
 */
class JSON
{
	/**
	 * @param object $obj
	 * @return string
	 * @throws JSONException
	 */
	public static function serialize($obj, TypeConverter $typeConverter = null)
	{
		if ($obj === null)
			return 'null';
		$buf = array();
		self::_serialize($buf, $obj, null, $typeConverter);
		return join('', $buf);
	}

	/**
	 * @param array $sb
	 * @param mixed $obj
	 */
	private static function _serialize(array &$sb, $obj, $parent, TypeConverter $typeConverter = null)
	{
		if ($typeConverter !== null) {
			$obj = $typeConverter->convert($obj, $parent);
		}
		
		if ($obj === null) {
			$sb[] = 'null';
		} else if (is_array($obj) && (array_key_exists(0, $obj) || count($obj) === 0)) {
			$sb[] = '[';
			$firstDone = false;
			foreach ($obj as $colObj) {
				if ($firstDone) {
					$sb[] = ',';
				} else {
					$firstDone = true;
				}
				self::_serialize($sb, $colObj, $obj, $typeConverter);
			}
			$sb[] = (']');
		} else if (is_array($obj)) {
			$sb[] = ('{');
			$firstDone = false;
			foreach ($obj as $key=>$value) {
				if ($firstDone) {
					$sb[] = ',';
				} else {
					$firstDone = true;
				}
				$sb[] = '"';
				$sb[] = ($key);
				$sb[] = '"';
				$sb[] = ':';
				self::_serialize($sb, $value, $obj, $typeConverter);
			}
			$sb[] = '}';
		} else if (is_string($obj)) {
			$sb[] = '"';
			$sb[] = (self::replaceSpecial($obj));
			$sb[] = '"';
		} else if (is_int($obj) || is_float($obj) || is_double($obj)) {
			$sb[] = (strval($obj));
		} else if (is_bool($obj)) {
			$sb[] = ($obj ? 'true' : 'false');
		} else if ($obj instanceof DateTime) {
			$sb[] = '"';
			$sb[] = (self::replaceSpecial($obj->format($this->dateFormat)));
			$sb[] = '"';
		} else if ($obj instanceof \stdClass) {
			$sb[] = ('{');
			$firstDone = false;
			foreach ($obj as $key => $value) {
				if ($firstDone) {
					$sb[] = ',';
				} else {
					$firstDone = true;
				}
				$sb[] = '"';
				$sb[] = $key;
				$sb[] = '":';
				self::_serialize($sb, $value, $obj, $typeConverter);
			}
			$sb[] = ('}');
		} else if (is_object($obj)) {
			$sb[] = ('{}');	// Hide property.
		} else {
			// ?
		}
	}

	/**
	 * Escape special charactors.
	 * @param source
	 * @return string
	 */
	private static function replaceSpecial($source)
	{
		static $tranChars = array(
			"\n" => "\\n",
			"\r" => "\\r",
			"\\" => "\\\\",
			"\t" => "\\t",
			'"' => "\\\"",
//			"'" => "\\\'",
			);
		if ($source === null) {
			return 'null';
		}
		$ret = '';
		$len = strlen($source);
		for ($i = 0; $i < $len; $i++) {
			$c = $source[$i];
			if (isset($tranChars[$c])) {
				$ret.= $tranChars[$c];
			} else {
				$ret.=$c;
			}
		}
		return $ret;
	}

	
}
