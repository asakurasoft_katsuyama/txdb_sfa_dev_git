<?php
namespace TxCore;

class RowColumnChange
{
	private $columnLabel;
	private $columnType;
	private $oldText;
	private $newText;

    public function getColumnLabel()
    {
        return $this->columnLabel;
    }
    public function setColumnLabel($value)
    {
        $this->columnLabel = $value;
    }
    public function getColumnType()
    {
        return $this->columnType;
    }
    public function setColumnType($value)
    {
        $this->columnType = $value;
    }
    public function getOldText()
    {
        return $this->oldText;
    }
    public function setOldText($value)
    {
        $this->oldText = $value;
    }
    public function getNewText()
    {
        return $this->newText;
    }
    public function setNewText($value)
    {
        $this->newText = $value;
    }

	public static function fromArray($obj)
	{
		$obj = is_string($obj) ? Util::decodeJSON($obj) : $obj;
		$change = new RowColumnChange();
		$change->setColumnLabel($obj->column->label);
		$change->setColumnType($obj->column->type);
		$change->setOldText($obj->text->old);
		$change->setNewText($obj->text->new);
		return $change;
	}

	public function toArray()
	{
		$change = array(
			'column' => array(
				'label' => $this->getColumnLabel(),
				'type' => $this->getColumnType(),
			),
			'text' => array(
				'old' => $this->getOldText(),
				'new' => $this->getNewText(),
			),
		);
		return $change;
	}

}
