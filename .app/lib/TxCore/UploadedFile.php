<?php 
namespace TxCore;

class UploadedFile
{
	private $fileName;
	private $filekey;
	private $contentType;
	
	public static function create($path, $fileName, $contentType)
	{
		$file = new self();
		$file->_setFileName($fileName);
		$file->_setFilekey($path);
		$file->_setContentType($contentType);
		return $file;
	}
	
	public function getFileName() 
	{
		return $this->fileName;
	}
	public function _setFileName($value)
	{
		$this->fileName = $value;
	}
	
	public function getFilekey()
	{
		return $this->filekey;
	}
	public function _setFilekey($value)
	{
		$this->filekey = $value;
	}
	
	public function getContentType()
	{
		return $this->contentType;
	}
	public function _setContentType($value)
	{
		$this->contentType = $value;
	}

}
