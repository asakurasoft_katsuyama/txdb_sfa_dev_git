<?php
namespace TxCore\Math;

class MathContext
{
	const ROUND_UP = 'roundup';
	const ROUND_DOWN = 'rounddown';
	const ROUNDHALF_UP = 'roundhalfup';
	const ROUNDHALF_TOEVEN = 'roundhalftoeven';
	const ROUND_DEFAULT = 'default';
		
	private $precision = 0;
	private $roundType;
	
	public function __construct($precision = 0, $roundType = self::ROUND_DEFAULT)
	{
		$this->precision = $precision;
		$this->roundType = $roundType;
	}
	
	public function setPrecision($value) 
	{
		$this->precision = $value;
	}
	
	public function setRoundType($value)
	{
		$this->roundType = $value;
	}
	
	public function getPrecision()
	{
		return $this->precision;
	}
	
	public function getRoundType()
	{
		return $this->roundType;
	}
}