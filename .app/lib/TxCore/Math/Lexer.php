<?php
namespace TxCore\Math;

use TxCore\TxCoreException;

/**
 * Lexical parser using caluc column.
 */
class Lexer 
{
	private $patterns;
	
	/**
	 * @param stirng $str
	 * @return array
	 */
	public function tokenize($str) 
	{
		$tokens = array();
		$offset = 0;
		$buffer = $str;
		while (strlen($buffer) > 0) {
			$successful = false;
			foreach ($this->patterns as $type => $pattern) {
				$matches = array();
				if (!preg_match($pattern, $buffer, $matches, PREG_OFFSET_CAPTURE)) {
					continue;
				} 
				list ($value, ) = $matches[0];
				$tokens[] = (object)array('value'=>$value, 'type'=>$type);
				
				$buffer = mb_substr($buffer, mb_strlen($value));
				$successful = true;
				break; 
			}
			if (!$successful) {
				throw new TxCoreException(sprintf("Invalid format '%s'", $buffer),
					TxCoreException::INVALID_FORMULA);
			}
		}
		return $tokens;
	}
	
	public function setPatterns($value) 
	{
		$this->patterns = $value;
	}
}

