<?php
namespace TxCore\Math;

use TxCore\Math\MathContext;

class Math
{
	
 	public static function bcround ($val, MathContext $context)
 	{
 		switch($context->getRoundType()) {
 			case 'roundup':
 				return self::bcroundup($val, $context->getPrecision());
 			case 'rounddown':
 				return self::bcrounddown($val, $context->getPrecision());
 			case 'roundhalfup':
 				return self::bcroundhalfup($val, $context->getPrecision());
 			case 'default':
 			case 'roundhalftoeven':
 			default:
 				return self::bcroundhalftoeven($val, $context->getPrecision());
 		}
 	}

 	/**
 	 * Normalize number format, e.g.) .1 -> 0.1  / 000000 -> 0
 	 * @param string $val
 	 * @return string Normalized number.
 	 */
	public static function bcnormalize ($val, $precision = 10)
	{
		return bcadd($val, '0', $precision);
	}
	
	
	public static function bccompare($a, $b)
	{
		return bccomp($a, $b, 10);
	}
	
	
	/**
	 * round up toward +-infinity
	 * @param string $val
	 * @param number $precision
	 * @return string
	 */
	public static function bcroundup ($val, $precision = 0)
	{
		$val = rtrim($val, '.');
		if ($val[0] == '.') {
			$val = '0' . $val;
		} else if ($val[0] == '-' && $val[1] == '.') {
			$val = '-0'.substr($val, 1);
		}
	
		if (($pos = strpos($val, '.')) !== false) {
			if ( bccomp($val, '0', strlen(substr($val, $pos+1))) === 0 ) {
				return '0';
			}
			if ($precision > 0) {
				$int = substr($val, 0, $pos);
				$pos2 = ++$pos+$precision;
				if ($pos2 < strlen($val)) {
					$val2 = sprintf('%s.%s', substr($val, $pos, $pos2-$pos), substr($val, $pos2));
	
// 					$val2 = ($val[0] == '-') ? self::bcfloor($val2) : self::bcceil($val2);
					$val2 = self::bcceil($val2);
	
					if (strlen($val2) > $precision)
						return bcadd($int, $val[0] == '-' ? -1 : 1, 0);
					else {
						$val2 = str_pad($val2, $precision, '0', STR_PAD_LEFT);
						$val2 = rtrim(sprintf('%s.%s', $int, rtrim($val2, '0')), '.');
						return (bccomp($val2, '0', $precision) === 0) ? '0' : $val2;
					}
				}
				return $val;
			} else if ($precision < 0){
				$denomi = '1' . str_pad('', abs($precision), '0', STR_PAD_RIGHT);
				if ($val[0] == '-') {
					return bcmul( self::bcfloor(
							bcdiv($val, $denomi, abs($precision) + strlen(substr($val, $pos+1))) ), $denomi, 0 );
				} else {
					return bcmul( self::bcceil(
							bcdiv($val, $denomi, abs($precision) + strlen(substr($val, $pos+1))) ), $denomi, 0 );
				}
			} else {
				$val2 = ($val[0] == '-') ? self::bcfloor($val) : self::bcceil($val);
				return (bccomp($val2, '0', $precision) === 0) ? '0' : $val2;
			}
		} else {
			if (bccomp($val, '0') === 0) {
				return '0';
			}
			if ($precision > 0) {
				return $val;
			} else if ($precision < 0) {
				$denomi = '1' . str_pad('', abs($precision), '0', STR_PAD_RIGHT);
				
				if ($val[0] == '-') {
					return bcmul( self::bcfloor( bcdiv($val, $denomi, abs($precision)) ), $denomi, 0 );
				} else {
					return bcmul( self::bcceil( bcdiv($val, $denomi, abs($precision)) ), $denomi, 0 );
				}
			} else {
				return $val;
			}
		}
	
	}

//// round up toward +infinity
// 	public static function bcroundup ($val, $precision = 0)
// 	{
//		$val = rtrim($val, '.');
// 		if ($val[0] == '.') {
// 			$val = '0' . $val;
// 		} else if ($val[0] == '-' && $val[1] == '.') {
// 			$val = '-0'.substr($val, 1);
// 		}

// 		if (($pos = strpos($val, '.')) !== false) {
// 			if ( bccomp($val, '0', strlen(substr($val, $pos+1))) === 0 ) {
// 				return '0';
// 			}
// 			if ($precision > 0) {
// 				$int = substr($val, 0, $pos);
// 				$pos2 = ++$pos+$precision;
// 				if ($pos2 < strlen($val)) {
// 					$val2 = sprintf('%s.%s', substr($val, $pos, $pos2-$pos), substr($val, $pos2));

// 					$val2 = ($val[0] == '-') ? self::bcfloor($val2) : self::bcceil($val2);

// 					if (strlen($val2) > $precision)
// 						return bcadd($int, $val[0] == '-' ? -1 : 1, 0);
// 					else {
// 						$val2 = str_pad($val2, $precision, '0', STR_PAD_LEFT);
// 						$val2 = rtrim(sprintf('%s.%s', $int, rtrim($val2, '0')), '.');
// 						return (bccomp($val2, '0', $precision) === 0) ? '0' : $val2;
// 					}
// 				}
// 				return $val;
// 			} else if ($precision < 0){
// 				$denomi = '1' . str_pad('', abs($precision), '0', STR_PAD_RIGHT);

// 				return bcmul( self::bcceil(
// 						bcdiv($val, $denomi, abs($precision) + strlen(substr($val, $pos+1))) ), $denomi, 0 );
// 			} else {
// 				$val2 = self::bcceil($val);
// 				return (bccomp($val2, '0', $precision) === 0) ? '0' : $val2;
// 			}
// 		} else {
// 			if (bccomp($val, '0') === 0) {
// 				return '0';
// 			}
// 			if ($precision > 0) {
// 				return $val;
// 			} else if ($precision < 0) {
// 				$denomi = '1' . str_pad('', abs($precision), '0', STR_PAD_RIGHT);
	
// 				return bcmul( self::bcceil( bcdiv($val, $denomi, abs($precision)) ), $denomi, 0 );
// 			} else {
// 				return $val;
// 			}
// 		}

// 	}
	
	/**
	 * round down toward 0
	 * @param string $val
	 * @param number $precision
	 * @return string
	 */
	public static function bcrounddown ($val, $precision = 0)
	{
		$val = rtrim($val, '.');
		if ($val[0] == '.') {
			$val = '0' . $val;
		} else if ($val[0] == '-' && $val[1] == '.') {
			$val = '-0'.substr($val, 1);
		}
	
		if (($pos = strpos($val, '.')) !== false) {
			if ( bccomp($val, '0', strlen(substr($val, $pos+1))) === 0 ) {
				return '0';
			}
			if ($precision > 0) {
				$int = substr($val, 0, $pos);
				$pos2 = ++$pos+$precision;
				if ($pos2 < strlen($val)) {
					$val2 = sprintf('%s.%s', substr($val, $pos, $pos2-$pos), substr($val, $pos2));
	
// 					$val2 = ($val[0] == '-') ? self::bcceil($val2) : self::bcfloor($val2);
					$val2 = self::bcfloor($val2);
	
					if (strlen($val2) > $precision)
						return bcadd($int, $val[0] == '-' ? -1 : 1, 0);
					else {
						$val2 = str_pad($val2, $precision, '0', STR_PAD_LEFT);
						$val2 = rtrim(sprintf('%s.%s', $int, rtrim($val2, '0')), '.');
						return (bccomp($val2, '0', $precision) === 0) ? '0' : $val2;
					}
				}
				return $val;
			} else if ($precision < 0){
				$denomi = '1' . str_pad('', abs($precision), '0', STR_PAD_RIGHT);
				
				if ($val[0] == '-') {
					return bcmul( self::bcceil(
							bcdiv($val, $denomi, abs($precision) + strlen(substr($val, $pos+1))) ), $denomi, 0 );
				} else {
					return bcmul( self::bcfloor(
							bcdiv($val, $denomi, abs($precision) + strlen(substr($val, $pos+1))) ), $denomi, 0 );
				}
			} else {
				$val2 = ($val[0] == '-') ? self::bcceil($val) : self::bcfloor($val);
				return (bccomp($val2, '0', $precision) === 0) ? '0' : $val2;
			}
		} else {
			if (bccomp($val, '0') === 0) {
				return '0';
			}
			if ($precision > 0) {
				return $val;
			} else if ($precision < 0) {
				$denomi = '1' . str_pad('', abs($precision), '0', STR_PAD_RIGHT);
				if ($val[0] == '-') {
					return bcmul( self::bcceil( bcdiv($val, $denomi, abs($precision)) ), $denomi, 0 );
				} else {
					return bcmul( self::bcfloor( bcdiv($val, $denomi, abs($precision)) ), $denomi, 0 );
				}
			} else {
				return $val;
			}
		}
	}

// // round down toward -infinity
// 	public static function bcrounddown ($val, $precision = 0)
// 	{
//		$val = rtrim($val, '.');
// 		if ($val[0] == '.') {
// 			$val = '0' . $val;
// 		} else if ($val[0] == '-' && $val[1] == '.') {
// 			$val = '-0'.substr($val, 1);
// 		}
	
// 		if (($pos = strpos($val, '.')) !== false) {
// 			if ( bccomp($val, '0', strlen(substr($val, $pos+1))) === 0 ) {
// 				return '0';
// 			}
// 			if ($precision > 0) {
// 				$int = substr($val, 0, $pos);
// 				$pos2 = ++$pos+$precision;
// 				if ($pos2 < strlen($val)) {
// 					$val2 = sprintf('%s.%s', substr($val, $pos, $pos2-$pos), substr($val, $pos2));
	
// 					$val2 = ($val[0] == '-') ? self::bcceil($val2) : self::bcfloor($val2);
	
// 					if (strlen($val2) > $precision)
// 						return bcadd($int, $val[0] == '-' ? -1 : 1, 0);
// 					else {
// 						$val2 = str_pad($val2, $precision, '0', STR_PAD_LEFT);
// 						$val2 = rtrim(sprintf('%s.%s', $int, rtrim($val2, '0')), '.');
// 						return (bccomp($val2, '0', $precision) === 0) ? '0' : $val2;
// 					}
// 				}
// 				return $val;
// 			} else if ($precision < 0){
// 				$denomi = '1' . str_pad('', abs($precision), '0', STR_PAD_RIGHT);
					
// 				return bcmul( self::bcfloor(
// 						bcdiv($val, $denomi, abs($precision) + strlen(substr($val, $pos+1))) ), $denomi, 0 );
// 			} else {
// 				return self::bcfloor($val);
// 			}
// 		} else {
// 			if (bccomp($val, '0') === 0) {
// 				return '0';
// 			}
// 			if ($precision > 0) {
// 				return $val;
// 			} else if ($precision < 0) {
// 				$denomi = '1' . str_pad('', abs($precision), '0', STR_PAD_RIGHT);
					
// 				return bcmul( self::bcfloor( bcdiv($val, $denomi, abs($precision)) ), $denomi, 0 );
// 			} else {
// 				return $val;
// 			}
// 		}
// 	}
	
	/**
	 * round half up toward +-infinity
	 * @param unknown $val
	 * @param number $precision
	 * @return string|Ambigous <string, unknown>
	 */
	public static function bcroundhalfup ($val, $precision = 0)
	{
		$val = rtrim($val, '.');
		if ($val[0] == '.') {
			$val = '0' . $val;
		} else if ($val[0] == '-' && $val[1] == '.') {
			$val = '-0'.substr($val, 1);
		}
	
		if (($pos = strpos($val, '.')) !== false) {
			if ( bccomp($val, '0', strlen(substr($val, $pos+1))) === 0 ) {
				return '0';
			}
			if ($precision > 0) {
				$int = substr($val, 0, $pos);
				$pos2 = ++$pos+$precision;
				if ($pos2 < strlen($val)) {
					$val2 = sprintf('%s.%s', substr($val, $pos, $pos2-$pos), substr($val, $pos2));
	
					$precisionNumber = substr($val, $pos2);
					$c = bccomp('0.'.$precisionNumber, "0.5", strlen($precisionNumber));
					if ($val[0] == '-') {
						$val2 = ($c >= 0) ? self::bcceil($val2) : self::bcfloor($val2);
					} else {
						$val2 = ($c >= 0) ? self::bcceil($val2) : self::bcfloor($val2);
					}
	
					if (strlen($val2) > $precision)
						return bcadd($int, $val[0] == '-' ? -1 : 1, 0);
					else {
						$val2 = str_pad($val2, $precision, '0', STR_PAD_LEFT);
						$val2 = rtrim(sprintf('%s.%s', $int, rtrim($val2, '0')), '.');
						return (bccomp($val2, '0', $precision) === 0) ? '0' : $val2;
					}
				}
				return $val;
			} else if ($precision < 0){
				$denomi = '1' . str_pad('', abs($precision), '0', STR_PAD_RIGHT);
					
				$val2 = bcdiv($val, $denomi, abs($precision) + strlen(substr($val, $pos+1)));
				$pos2 = strpos($val2, '.');
				$precisionNumber = substr($val2, $pos2+1 );
				$c = bccomp('0.'.$precisionNumber, "0.5", strlen($precisionNumber));
				if ($val[0] == '-') {
					$val2 = ($c >= 0) ? self::bcfloor($val2) : self::bcceil($val2);
				} else {
					$val2 = ($c >= 0) ? self::bcceil($val2) : self::bcfloor($val2);
				}
				return bcmul( $val2, $denomi, 0 );
			} else {
				$pos2 = ++$pos;
				$val2 = '0.'.substr($val, $pos);
				$precisionNumber = substr($val, $pos2);
				$c = bccomp('0.'.$precisionNumber, "0.5", strlen($precisionNumber));
				if ($val[0] == '-') {
					return ($c >= 0) ? self::bcfloor($val) : self::bcceil($val);
				} else {
					return ($c >= 0) ? self::bcceil($val) : self::bcfloor($val);
				}
			}
		} else {
			if (bccomp($val, '0') === 0) {
				return '0';
			}
			if ($precision > 0) {
				return $val;
			} else if ($precision < 0) {
				$denomi = '1' . str_pad('', abs($precision), '0', STR_PAD_RIGHT);
					
				$val2 = bcdiv($val, $denomi, abs($precision));
				$pos2 = strpos($val2, '.');
				$precisionNumber = substr($val2, $pos2+1 );
				$c = bccomp('0.'.$precisionNumber, "0.5", strlen($precisionNumber));
				if ($val[0] == '-') {
					$val2 = ($c >= 0) ? self::bcfloor($val2) : self::bcceil($val2);
				} else {
					$val2 = ($c >= 0) ? self::bcceil($val2) : self::bcfloor($val2);
				}
				return bcmul( $val2, $denomi, 0 );
			} else {
				return $val;
			}
		}
	}

// // round half up toward +infinity
// 	public static function bcroundhalfup ($val, $precision = 0)
// 	{
//		$val = rtrim($val, '.');
// 		if ($val[0] == '.') {
// 			$val = '0' . $val;
// 		} else if ($val[0] == '-' && $val[1] == '.') {
// 			$val = '-0'.substr($val, 1);
// 		}
	
// 		if (($pos = strpos($val, '.')) !== false) {
// 			if ( bccomp($val, '0', strlen(substr($val, $pos+1))) === 0 ) {
// 				return '0';
// 			}
// 			if ($precision > 0) {
// 				$int = substr($val, 0, $pos);
// 				$pos2 = ++$pos+$precision;
// 				if ($pos2 < strlen($val)) {
// 					$val2 = sprintf('%s.%s', substr($val, $pos, $pos2-$pos), substr($val, $pos2));
	
// 					$precisionNumber = substr($val, $pos2);
// 					$c = bccomp('0.'.$precisionNumber, "0.5", strlen($precisionNumber));
// 					if ($val[0] == '-') {
// 						$val2 = ($c > 0) ? self::bcceil($val2) : self::bcfloor($val2);
// 					} else {
// 						$val2 = ($c >= 0) ? self::bcceil($val2) : self::bcfloor($val2);
// 					}
	
// 					if (strlen($val2) > $precision)
// 						return bcadd($int, $val[0] == '-' ? -1 : 1, 0);
// 					else {
// 						$val2 = str_pad($val2, $precision, '0', STR_PAD_LEFT);
// 						$val2 = rtrim(sprintf('%s.%s', $int, rtrim($val2, '0')), '.');
// 						return (bccomp($val2, '0', $precision) === 0) ? '0' : $val2;
// 					}
// 				}
// 				return $val;
// 			} else if ($precision < 0){
// 				$denomi = '1' . str_pad('', abs($precision), '0', STR_PAD_RIGHT);
					
// 				$val2 = bcdiv($val, $denomi, abs($precision) + strlen(substr($val, $pos+1)));
// 				$pos2 = strpos($val2, '.');
// 				$precisionNumber = substr($val2, $pos2+1 );
// 				$c = bccomp('0.'.$precisionNumber, "0.5", strlen($precisionNumber));
// 				if ($val[0] == '-') {
// 					$val2 = ($c > 0) ? self::bcfloor($val2) : self::bcceil($val2);
// 				} else {
// 					$val2 = ($c >= 0) ? self::bcceil($val2) : self::bcfloor($val2);
// 				}
// 				return bcmul( $val2, $denomi, 0 );
// 			} else {
// 				$pos2 = ++$pos;
// 				$val2 = '0.'.substr($val, $pos);
// 				$precisionNumber = substr($val, $pos2);
// 				$c = bccomp('0.'.$precisionNumber, "0.5", strlen($precisionNumber));
// 				if ($val[0] == '-') {
// 					return ($c > 0) ? self::bcfloor($val) : self::bcceil($val);
// 				} else {
// 					return ($c >= 0) ? self::bcceil($val) : self::bcfloor($val);
// 				}
// 			}
// 		} else {
// 			if (bccomp($val, '0') === 0) {
// 				return '0';
// 			}
// 			if ($precision > 0) {
// 				return $val;
// 			} else if ($precision < 0) {
// 				$denomi = '1' . str_pad('', abs($precision), '0', STR_PAD_RIGHT);
					
// 				$val2 = bcdiv($val, $denomi, abs($precision));
// 				$pos2 = strpos($val2, '.');
// 				$precisionNumber = substr($val2, $pos2+1 );
// 				$c = bccomp('0.'.$precisionNumber, "0.5", strlen($precisionNumber));
// 				if ($val[0] == '-') {
// 					$val2 = ($c > 0) ? self::bcfloor($val2) : self::bcceil($val2);
// 				} else {
// 					$val2 = ($c >= 0) ? self::bcceil($val2) : self::bcfloor($val2);
// 				}
// 				return bcmul( $val2, $denomi, 0 );
// 			} else {
// 				return $val;
// 			}
// 		}
// 	}
	
	public static function bcroundhalftoeven ($val, $precision = 0)
	{
		$val = rtrim($val, '.');
		if ($val[0] == '.') {
			$val = '0' . $val;
		} else if ($val[0] == '-' && $val[1] == '.') {
			$val = '-0'.substr($val, 1);
		}
	
		if (($pos = strpos($val, '.')) !== false) {
			if ( bccomp($val, '0', strlen(substr($val, $pos+1))) === 0 ) {
				return '0';
			}
			if ($precision > 0) {
				$int = substr($val, 0, $pos);
				$pos2 = ++$pos+$precision;
				if ($pos2 < strlen($val)) {
					$val2 = sprintf('%s.%s', substr($val, $pos, $pos2-$pos), substr($val, $pos2));
	
					$precisionNumber = substr($val, $pos2);
					$c = bccomp('0.'.$precisionNumber, "0.5", strlen($precisionNumber));
					if ($c > 0) {
						$val2 = self::bcceil($val2);
					} else if ($c < 0) {
						$val2 = self::bcfloor($val2);
					} else {
						$val2 = intval($val2[$pos2-$pos-1]) % 2 == 1 ? self::bcceil($val2) : self::bcfloor($val2);
					}
	
					if (strlen($val2) > $precision)
						return bcadd($int, $val[0] == '-' ? -1 : 1, 0);
					else {
						$val2 = str_pad($val2, $precision, '0', STR_PAD_LEFT);
						$val2 = rtrim(sprintf('%s.%s', $int, rtrim($val2, '0')), '.');
						return (bccomp($val2, '0', $precision) === 0) ? '0' : $val2;
					}
				}
				return $val;
			} else if ($precision < 0){
				$denomi = '1' . str_pad('', abs($precision), '0', STR_PAD_RIGHT);
					
				$val2 = bcdiv($val, $denomi, abs($precision) + strlen(substr($val, $pos+1)));
				$pos2 = strpos($val2, '.');
				$onesPlace = '0';
				if (isset($val2[$pos2-1])) {
					if ($val2[$pos2-1] != '-') {
						$onesPlace = $val2[$pos2-1];
					}
				}
				$precisionNumber = substr($val2, $pos2+1 );
				$c = bccomp('0.'.$precisionNumber, "0.5", strlen($precisionNumber));
				if ($c > 0) {
					$val2 = ($val[0] == '-') ? self::bcfloor($val2) : self::bcceil($val2);
				} else if ($c < 0) {
					$val2 = ($val[0] == '-') ? self::bcceil($val2) : self::bcfloor($val2);
				} else {
					if (intval($onesPlace) % 2 == 1) {
						$val2 = ($val[0] == '-') ? self::bcfloor($val2) : self::bcceil($val2);
					} else {
						$val2 = ($val[0] == '-') ? self::bcceil($val2) : self::bcfloor($val2);
					}
				}
				return bcmul( $val2, $denomi, 0 );
			} else {
				$int = substr($val, 0, $pos);
				$onesPlace = '0';
				if (isset($val[$pos-1])) {
					if ($val[$pos-1] != '-') {
						$onesPlace = $val[$pos-1];
					}
				}
				$pos2 = ++$pos;
				$val2 = '0.'.substr($val, $pos);
				$precisionNumber = substr($val, $pos2);
				$c = bccomp('0.'.$precisionNumber, "0.5", strlen($precisionNumber));
				if ($c > 0) {
					$val2 = self::bcceil($val2);
				} else if ($c < 0) {
					$val2 = self::bcfloor($val2);
				} else {
					$val2 = intval($onesPlace) % 2 == 1 ? self::bcceil($val2) : self::bcfloor($val2);
				}
				return $val[0] == '-' ? bcsub($int, $val2, 0) : bcadd($int, $val2, 0);
			}
		} else {
			if (bccomp($val, '0') === 0) {
				return '0';
			}
			if ($precision > 0) {
				return $val;
			} else if ($precision < 0) {
				$denomi = '1' . str_pad('', abs($precision), '0', STR_PAD_RIGHT);
	
				$int = $val;
				$val2 = bcdiv($val, $denomi, abs($precision));
				$pos2 = strpos($val2, '.');
				$int2 = substr($val, 0, $pos2);
				$onesPlace2 = '0';
				if (isset($val2[$pos2-1])) {
					if ($val2[$pos2-1] != '-') {
						$onesPlace2 = $val2[$pos2-1];
					}
				}
				$precisionNumber = substr($val2, $pos2+1 );
				$c = bccomp('0.'.$precisionNumber, "0.5", strlen($precisionNumber));
				if ($c > 0) {
					$val2 = ($val[0] == '-') ? self::bcfloor($val2) : self::bcceil($val2);
				} else if ($c < 0) {
					$val2 = ($val[0] == '-') ? self::bcceil($val2) : self::bcfloor($val2);
				} else {
					if (intval($onesPlace2) % 2 == 1) {
						$val2 = ($val[0] == '-') ? self::bcfloor($val2) : self::bcceil($val2);
					} else {
						$val2 = ($val[0] == '-') ? self::bcceil($val2) : self::bcfloor($val2);
					}
				}
				return bcmul( $val2, $denomi, 0 );
			} else {
				return $val;
			}
		}
	}
	
	//http://stackoverflow.com/questions/1642614/how-to-ceil-floor-and-round-bcmath-numbers
	private static function bcceil($number)
	{
		if ( ($pos = strpos($number, '.')) !== false) {
// 			if (preg_match("~\.[0]+$~", $number)) return self::bcround($number, 0);
			if (preg_match("~\.[0]+$~", $number)) return substr($number, 0, $pos);
			if ($number[0] != '-') return bcadd($number, 1, 0);
			return bcsub($number, 0, 0);
		}
		return $number;
	}
	
	private static function bcfloor($number)
	{
		if ( ($pos = strpos($number, '.')) !== false) {
// 			if (preg_match("~\.[0]+$~", $number)) return self::bcround($number, 0);
			if (preg_match("~\.[0]+$~", $number)) return substr($number, 0, $pos);
			if ($number[0] != '-') return bcadd($number, 0, 0);
			return bcsub($number, 1, 0);
		}
		return $number;
	}
	
}