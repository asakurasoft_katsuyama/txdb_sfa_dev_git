<?php
namespace TxCore\Math;

use TxCore\TxCoreException;

/**
 * Expression evaluator
 * @see http://code.ohloh.net/file?fid=Ydz4dXkw8JjgDUwv8YL_WLjMRZA&cid=0ZcIk8pk2z8
 * @see http://www.meta-calculator.com/learning-lab/how-to-build-scientific-calculator/infix-to-postifix-convertor.php
 */
class RPN
{
// 	const CALC = 0;
	const MATHMATIC = 0;
	const INTERPOLATE = 1;
// 	const MATH = 0;
	
	const NUMBER = 'number';
	const PAREN_LEFT = 'paren left';
	const PAREN_RIGHT = 'paren right';
	const SPACE = 'space';
	const VARIABLE = 'variable';
	const OPERATOR = 'operator';
	const FUNCTION_ = 'function';
	const LITERAL = "literal";
	
	const ROUND = 'default';
	const ROUND_UP = 'roundup';
	const ROUND_DOWN = 'rounddown';
	
	/**
	 * @var MathContext
	 */
	private $mathContext;
	
	/**
	 * @var int
	 */
	private $type = self::MATHMATIC;
	
	private static $operators = array(
		'+' => array( 'name' => 'add', 'precedence' => 1, 'arity' => 2 ),
		'-' => array( 'name' => 'sub', 'precedence' => 1, 'arity' => 2 ),
		'/' => array( 'name' => 'div', 'precedence' => 2, 'arity' => 2 ),
		'*' => array( 'name' => 'mul', 'precedence' => 2, 'arity' => 2 ),
		// 		'%' => array( 'name' => 'mod', 'precedence' => 2, 'arity' => 2 ),
		'^' => array( 'name' => 'pow', 'precedence' => 3, 'associativity' => 'right', 'arity' => 2 ),
		'&' => array( 'name' => 'concat', 'precedence' => 1, 'arity' => 2 ),
	);
	
	private static $calcPatters = array(
		self::NUMBER => '/^(?:\d*\.\d+)|^(?:\d+)/u',
		self::PAREN_LEFT => '/^\(/u',
		self::PAREN_RIGHT => '/^\)/u',
		self::SPACE => '/^\s+/u',
		self::OPERATOR => '/^[+\-\/\*\^]/u',
		self::VARIABLE => '/^[^+\-\/\*\^\s\(\)&0-9][^+\-\/\*\^\s\(\)&]*/u',	// 上記以外かつ先頭が数値でない
	);
	
	static $textPatters = array(
		self::SPACE => '/^\s+/u',
		self::OPERATOR => '/^[&]/u',
// 		self::LITERAL => '/"(?:[^\\"]|\\.)*"/',
		self::LITERAL => '/^"[^"]*"/u',
		self::VARIABLE => '/^[^+\-\/\*\^\s\(\)&0-9][^+\-\/\*\^\s\(\)&]*/u',	// 上記以外かつ先頭が数値でない
	);
	
	public function __construct() 
	{
		$this->mathContext = new MathContext();	
	}
	
	/**
	 * @param string $str
	 * @return string 
	 * @throws TxCoreException
	 */
	public function evaluate($str, $variables = array())
	{
		$tree = $this->parse($str);
		$output = array();
		foreach ($tree as $node) {
			$this->evaluate_($node, $output, $variables);
		}
	
		if (count($output) === 1) {
			return $output[0];
		} else {
			throw new TxCoreException('Too many values : .' . $str);
		}
	}
	
	/**
	 * @param int $value
	 */
	public function setType($value)
	{
		$this->type = $value;
	}
	
	/**
	 * Prase to RPN
	 * @param string $str
	 * @return array
	 * @throws TxCoreException
	 */
	public function parse($str)
	{
		$patterns = $this->type == self::MATHMATIC ? self::$calcPatters : self::$textPatters;
		$output = array();
		return $this->_parse($str, $patterns, $this->type);
	}
	
	/**
	 * Prase to RPN
	 * @param string $str
	 * @return array
	 * @throws TxCoreException
	 */
	private function _parse($str, $patterns)
	{
		$lex = new Lexer();
		$lex->setPatterns($patterns);
		$tokens  = $lex->tokenize($str);
		$were = array();
		$output = array();
		$stack = array();
		$v1 = null;
		$v2 = null;

		for ($i = 0; $i < count($tokens); $i++)  {
			$token = $tokens[$i];
			
			switch ($token->type) {
			case self::NUMBER:
			case self::VARIABLE:
				$output[] = $token;
				break;
			case self::LITERAL:
				$token->value = mb_substr($token->value, 1, -1);
				$output[] = $token;
				break;
			case self::OPERATOR:
				$propery = self::$operators[$token->value];
				// for unary minus sign (e.g. =-1*(-1*-10)
				if ($propery['name'] == 'sub' &&
					($i == 0 ||
					 ($i > 0 && 
					  in_array($tokens[$i - 1]->type, array(self::OPERATOR, self::PAREN_LEFT)) &&
					  isset($tokens[$i + 1]) && 
					  $tokens[$i + 1]->type === self::NUMBER))
					) {
					$tokens[$i + 1]->value = '-' .$tokens[$i + 1]->value;
					break; 
				}
				
				while (is_not_empty($stack) && array_peek($stack)->type === self::OPERATOR && (
					(!isset($propery['associativity']) && $propery['precedence'] <= 
						self::$operators[array_peek($stack)->value]['precedence']) ||
					(isset($propery['associativity']) && $propery['precedence'] < 
						self::$operators[array_peek($stack)->value]['precedence'])
				)) {
					$v1 = array_pop($output);
					$v2 = array_pop($output);
					$output[] = $v2;
					$output[] = $v1;
					$output[] = array_pop($stack);
				}
				$stack[] = $token;
				break;
			case self::PAREN_LEFT:
				$stack[] = $token;
				break;
			case self::PAREN_RIGHT:
				while (is_not_empty($stack) && array_peek($stack)->type !== self::PAREN_LEFT) {
					$v1 = array_pop($output);
					$v2 = array_pop($output);

					$output[] = $v2;
					$output[] = $v1;
					$output[] = array_pop($stack);

					if (is_empty($stack)) {
						throw new TxCoreException('Mismatched parentheses.', TxCoreException::INVALID_FORMULA);
					}
				}
				array_pop($stack); // remove parentheses
				break;
			}
		}
		if (is_not_empty($stack)) {
			while (is_not_empty($stack)) {
				if (array_peek($stack)->type === self::PAREN_LEFT || array_peek($stack)->type === self::PAREN_RIGHT) {
					throw new TxCoreException('Mismatched parentheses.', TxCoreException::INVALID_FORMULA);
				} else {
					$v1 = array_pop($output);
					if ($v1 === null) {
						throw new TxCoreException('Unary operator "' . array_peek($stack)->value . '" requires at least one argument.', 
							TxCoreException::INVALID_FORMULA);
					}
					$output[] = $v1;
					$output[] = array_pop($stack);
				}
			}
		}
		return $output;
	}
	
	/**
	 * Enaluate for each token.
	 * @param token$token
	 * @param array $output
	 * @throws TxCoreException
	 */
	private function evaluate_($token, &$output, $variables = array()) 
	{
		$operands = array();
		if ($token->type === self::NUMBER) {
			$output[] = $token->value;
		} else if ($token->type === self::VARIABLE) {
			$output[] = @$variables[$token->value];
		} else if ($token->type === self::SPACE) {
			// Slip
		} else if ($token->type === self::OPERATOR) {
			$poperty = self::$operators[$token->value];
			if (count($output) < $poperty['arity']) {
				throw new TxCoreException(
					'Operator ' . $poperty['name'] . ' requires at least ' . $poperty['arity'] . ' arguments.'
					, TxCoreException::INVALID_FORMULA);
			}
	
			$operands = array_slice($output, count($output) - $poperty['arity']);
			$output = array_slice($output, 0, count($output) - $poperty['arity']);
			
			$fname = 'op_'.$poperty['name'];
			$output[] = call_user_func_array(array($this, $fname), $operands);
		} else if ($token->type === self::LITERAL) {
			$output[] = $token->value;
		}
	}
	
	/**
	 * @param MathContext $value
	 */
	public function setMathContext(MathContext $value)
	{
		$this->mathContext = $value;
	}
	
	/**
	 * @param string $str
	 */
	private function isValid($str)  
	{
		return preg_match('/^-?\d*\.\d+$|^-?\d+$/u', $str);
	}
	
	// -- Operations ----------------------------------
	 
	private function op_add($a, $b) 
	{
		if (!$this->isValid($a) || !$this->isValid($b)) 
			return null;
		return self::round(bcadd(self::round($a), self::round($b), 10));
	}
	private function op_sub($a, $b)
	{
		if (!$this->isValid($a) || !$this->isValid($b))
			return null;
		return self::round(bcsub(self::round($a), self::round($b), 10));
	}
	private function op_div($a, $b)
	{
		if (!$this->isValid($a) || !$this->isValid($b) || $b == 0)
			return null;
		return self::round(bcdiv(self::round($a), self::round($b), 10));
	}
	private function op_mul($a, $b)
	{
		if (!$this->isValid($a) || !$this->isValid($b))
			return null;
		return self::round(bcmul(self::round($a), self::round($b), 10));
	}
	private function op_pow($a, $b)
	{
		if (!$this->isValid($a) || !$this->isValid($b))
			return null;
		return self::round(bcpow(self::round($a), self::round($b), 10));
	}
	private function op_concat($a, $b)
	{
		if ($a === null || $b === null)
			return null;
		return $a. $b;
	}

	private function round($val) 
	{
		return Math::bcround($val, $this->mathContext);
	}
	
// 	private function _math($f, $a, $b)
// 	{
// 		if ($a === null || $b== null)
// 			return null;
// 		$margin = 0;
// 		if ($this->roundType === 'ceil' || 
// 			$this->roundType === 'floor') {
// 			$margin = 1;
// 		}
// 		$ret = call_user_func($f, $a, $b, $this->scale + $margin);
// 		if ($this->roundType === 'ceil') {
// 			$ret = bcroundup($ret, $this->scale); 
// 		} else if ($this->roundType === 'floor') {
// 			$ret = bcrounddown($ret, $this->scale);
// 		}
// 		return $ret;
// 	}
}

