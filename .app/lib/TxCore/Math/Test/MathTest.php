<?php
namespace TxCore\Math\Test;


use TxCore\Test\BaseTestCase;

use TxCore\Math\Math;
use TxCore\Math\MathContext;

class MathTest extends BaseTestCase
{
	public function testRound_後ろのゼロは削除される()
	{
		$this->assertSame('0', Math::bcround('0.00000', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('0.00000', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('0', Math::bcround('0.00000', new MathContext(0, MathContext::ROUND_DOWN)));
	}
	
	public function testRound_最近接偶数へのまるめ_基本演算()
	{
		//- default -
		//positive, p0
		$this->assertSame('0', Math::bcround('0.5', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('10', Math::bcround('10.5', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('12', Math::bcround('11.5', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('12', Math::bcround('11.6', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('11', Math::bcround('11.4', new MathContext(0, MathContext::ROUND_DEFAULT)));
		//negative, p0
		$this->assertSame('0', Math::bcround('-0.5', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-10', Math::bcround('-10.5', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-12', Math::bcround('-11.5', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-12', Math::bcround('-11.6', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-11', Math::bcround('-11.4', new MathContext(0, MathContext::ROUND_DEFAULT)));
		//positive, p1
		$this->assertSame('10', Math::bcround('10.05', new MathContext(1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('10.2', Math::bcround('10.15', new MathContext(1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('10.2', Math::bcround('10.16', new MathContext(1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('10.1', Math::bcround('10.14', new MathContext(1, MathContext::ROUND_DEFAULT)));
		//negative, p1
		$this->assertSame('-10', Math::bcround('-10.05', new MathContext(1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-10.2', Math::bcround('-10.15', new MathContext(1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-10.2', Math::bcround('-10.16', new MathContext(1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-10.1', Math::bcround('-10.14', new MathContext(1, MathContext::ROUND_DEFAULT)));
		//positive, p2
		$this->assertSame('10', Math::bcround('10.005', new MathContext(2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('10.02', Math::bcround('10.015', new MathContext(2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('10.02', Math::bcround('10.016', new MathContext(2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('10.01', Math::bcround('10.014', new MathContext(2, MathContext::ROUND_DEFAULT)));
		//negative, p2
		$this->assertSame('-10', Math::bcround('-10.005', new MathContext(2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-10.02', Math::bcround('-10.015', new MathContext(2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-10.02', Math::bcround('-10.016', new MathContext(2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-10.01', Math::bcround('-10.014', new MathContext(2, MathContext::ROUND_DEFAULT)));
		//positive, p2, overflow
		$this->assertSame('11', Math::bcround('10.995', new MathContext(2, MathContext::ROUND_DEFAULT)));
		//negative, p2, overflow
		$this->assertSame('-11', Math::bcround('-10.995', new MathContext(2, MathContext::ROUND_DEFAULT)));
		//positive, p0, integer
		$this->assertSame('1005', Math::bcround('1005', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('1015', Math::bcround('1015', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('1016', Math::bcround('1016', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('1014', Math::bcround('1014', new MathContext(0, MathContext::ROUND_DEFAULT)));
		//negative, p0, integer
		$this->assertSame('-1005', Math::bcround('-1005', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-1015', Math::bcround('-1015', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-1016', Math::bcround('-1016', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-1014', Math::bcround('-1014', new MathContext(0, MathContext::ROUND_DEFAULT)));
		//positive, p-1, integer
		$this->assertSame('1000', Math::bcround('1005', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('1020', Math::bcround('1015', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('1020', Math::bcround('1016', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('1010', Math::bcround('1014', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		//negative, p-1, integer
		$this->assertSame('-1000', Math::bcround('-1005', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-1020', Math::bcround('-1015', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-1020', Math::bcround('-1016', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-1010', Math::bcround('-1014', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		//positive, p-2, integer
		$this->assertSame('1000', Math::bcround('1050', new MathContext(-2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('1200', Math::bcround('1150', new MathContext(-2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('1200', Math::bcround('1161', new MathContext(-2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('1100', Math::bcround('1141', new MathContext(-2, MathContext::ROUND_DEFAULT)));
		//positive, p-2, integer
		$this->assertSame('0', Math::bcround('50', new MathContext(-2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('200', Math::bcround('150', new MathContext(-2, MathContext::ROUND_DEFAULT)));
		//negative, p-2, integer
		$this->assertSame('-1000', Math::bcround('-1050', new MathContext(-2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-1200', Math::bcround('-1150', new MathContext(-2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-1200', Math::bcround('-1161', new MathContext(-2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-1100', Math::bcround('-1141', new MathContext(-2, MathContext::ROUND_DEFAULT)));
		//positive, p-4, integer
		$this->assertSame('0', Math::bcround('5000', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('10000', Math::bcround('6000', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('4000', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		//negative, p-4, integer
		$this->assertSame('0', Math::bcround('-5000', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-10000', Math::bcround('-6000', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('-4000', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		//positive, p-5, integer
		$this->assertSame('0', Math::bcround('5000', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('6000', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('4000', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		//negative, p-5, integer
		$this->assertSame('0', Math::bcround('-5000', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('-6000', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('-5000', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		//positive, p-1
		$this->assertSame('1010', Math::bcround('1005.5', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('1020', Math::bcround('1015.5', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('1020', Math::bcround('1016.4', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('1010', Math::bcround('1014.6', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		//negative, p-1
		$this->assertSame('-1010', Math::bcround('-1005.5', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-1020', Math::bcround('-1015.1', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-1020', Math::bcround('-1016.4', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-1010', Math::bcround('-1014.6', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		//positive, p-2
		$this->assertSame('1100', Math::bcround('1050.5', new MathContext(-2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('1200', Math::bcround('1150.5', new MathContext(-2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('1200', Math::bcround('1161.4', new MathContext(-2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('1100', Math::bcround('1141.6', new MathContext(-2, MathContext::ROUND_DEFAULT)));
		//negative, p-2
		$this->assertSame('-1100', Math::bcround('-1050.5', new MathContext(-2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-1200', Math::bcround('-1150.1', new MathContext(-2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-1200', Math::bcround('-1161.4', new MathContext(-2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-1100', Math::bcround('-1141.6', new MathContext(-2, MathContext::ROUND_DEFAULT)));
		//positive, p-4
		$this->assertSame('10000', Math::bcround('5000.1', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('10000', Math::bcround('6000.4', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('4000.6', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		//negative, p-4
		$this->assertSame('-10000', Math::bcround('-5000.1', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-10000', Math::bcround('-6000.4', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('-4000.6', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		//positive, p-5
		$this->assertSame('0', Math::bcround('5000.1', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('6000.4', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('4000.6', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		//negative, p-5
		$this->assertSame('0', Math::bcround('-5000.1', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('-6000.4', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('-4000.6', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		//- 0. -
		//positive, p0
		$this->assertSame('0.1', Math::bcround('0.105', new MathContext(2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0.12', Math::bcround('0.115', new MathContext(2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0.12', Math::bcround('0.116', new MathContext(2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0.11', Math::bcround('0.114', new MathContext(2, MathContext::ROUND_DEFAULT)));
		//negative, p0
		$this->assertSame('-0.1', Math::bcround('-0.105', new MathContext(2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-0.12', Math::bcround('-0.115', new MathContext(2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-0.12', Math::bcround('-0.116', new MathContext(2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-0.11', Math::bcround('-0.114', new MathContext(2, MathContext::ROUND_DEFAULT)));
		//positive, p1
		$this->assertSame('0.01', Math::bcround('0.01005', new MathContext(4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0.0102', Math::bcround('0.01015', new MathContext(4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0.0102', Math::bcround('0.01016', new MathContext(4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0.0101', Math::bcround('0.01014', new MathContext(4, MathContext::ROUND_DEFAULT)));
		//negative, p1
		$this->assertSame('-0.01', Math::bcround('-0.01005', new MathContext(4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-0.0102', Math::bcround('-0.01015', new MathContext(4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-0.0102', Math::bcround('-0.01016', new MathContext(4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-0.0101', Math::bcround('-0.01014', new MathContext(4, MathContext::ROUND_DEFAULT)));
		//positive, p2
		$this->assertSame('0.1', Math::bcround('.10005', new MathContext(4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0.1002', Math::bcround('.10015', new MathContext(4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0.1002', Math::bcround('.10016', new MathContext(4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0.1001', Math::bcround('.10014', new MathContext(4, MathContext::ROUND_DEFAULT)));
		//negative, p2
		$this->assertSame('-0.1', Math::bcround('-.10005', new MathContext(4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-0.1002', Math::bcround('-.10015', new MathContext(4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-0.1002', Math::bcround('-.10016', new MathContext(4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-0.1001', Math::bcround('-.10014', new MathContext(4, MathContext::ROUND_DEFAULT)));
		//positive, p2, overflow
		$this->assertSame('0.011', Math::bcround('0.010995', new MathContext(3, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0.011', Math::bcround('.010995', new MathContext(3, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0.11', Math::bcround('.10995', new MathContext(2, MathContext::ROUND_DEFAULT)));
		//negative, p2, overflow
		$this->assertSame('-0.011', Math::bcround('-0.010995', new MathContext(3, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-0.011', Math::bcround('-.010995', new MathContext(3, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-0.11', Math::bcround('-.10995', new MathContext(2, MathContext::ROUND_DEFAULT)));
		//positive, p0, integer
		$this->assertSame('0', Math::bcround('0.105', new MathContext(0, MathContext::ROUND_DEFAULT)));
		//negative, p0, integer
		$this->assertSame('0', Math::bcround('-0.105', new MathContext(0, MathContext::ROUND_DEFAULT)));
		//positive, p-1, integer
		$this->assertSame('0', Math::bcround('0.105', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		//negative, p-1, integer
		$this->assertSame('0', Math::bcround('-0.105', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		//positive, p-2, integer
		$this->assertSame('0', Math::bcround('0.150', new MathContext(-2, MathContext::ROUND_DEFAULT)));
		//negative, p-2, integer
		$this->assertSame('0', Math::bcround('-0.150', new MathContext(-2, MathContext::ROUND_DEFAULT)));
		//
		// //positive, p-4, integer
		$this->assertSame('0', Math::bcround('5000', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('10000', Math::bcround('6000', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('4000', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		// //negative, p-4, integer
		$this->assertSame('0', Math::bcround('-5000', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-10000', Math::bcround('-6000', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('-4000', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		// //positive, p-5, integer
		$this->assertSame('0', Math::bcround('5000', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('6000', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('4000', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		// //negative, p-5, integer
		$this->assertSame('0', Math::bcround('-5000', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('-6000', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('-5000', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		//positive, p-1
		$this->assertSame('0', Math::bcround('.1055', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('0.01155', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		//negative, p-1
		$this->assertSame('0', Math::bcround('-.1055', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('-0.01151', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		// //positive, p-4
		$this->assertSame('10000', Math::bcround('5000.1', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('10000', Math::bcround('6000.4', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('4000.6', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		// //negative, p-4
		$this->assertSame('-10000', Math::bcround('-5000.1', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-10000', Math::bcround('-6000.4', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('-4000.6', new MathContext(-4, MathContext::ROUND_DEFAULT)));
		// //positive, p-5
		$this->assertSame('0', Math::bcround('5000.1', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('6000.4', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('4000.6', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		// //negative, p-5
		$this->assertSame('0', Math::bcround('-5000.1', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('-6000.4', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('-4000.6', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		
		//
	}
	
	public function testRound_切り上げ_基本演算()
	{
		//- round up -
		//positive, p0 
		$this->assertSame('11', Math::bcround('10.5', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('12', Math::bcround('11.5', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('12', Math::bcround('11.6', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('12', Math::bcround('11.4', new MathContext(0, MathContext::ROUND_UP)));
		//negative, p0 
		$this->assertSame('-11', Math::bcround('-10.5', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('-12', Math::bcround('-11.5', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('-12', Math::bcround('-11.6', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('-12', Math::bcround('-11.4', new MathContext(0, MathContext::ROUND_UP)));
		//positive, p1 
		$this->assertSame('10.1', Math::bcround('10.05', new MathContext(1, MathContext::ROUND_UP)));
		$this->assertSame('10.2', Math::bcround('10.15', new MathContext(1, MathContext::ROUND_UP)));
		$this->assertSame('10.2', Math::bcround('10.16', new MathContext(1, MathContext::ROUND_UP)));
		$this->assertSame('10.2', Math::bcround('10.14', new MathContext(1, MathContext::ROUND_UP)));
		//negative, p1 
		$this->assertSame('-10.1', Math::bcround('-10.05', new MathContext(1, MathContext::ROUND_UP)));
		$this->assertSame('-10.2', Math::bcround('-10.15', new MathContext(1, MathContext::ROUND_UP)));
		$this->assertSame('-10.2', Math::bcround('-10.16', new MathContext(1, MathContext::ROUND_UP)));
		$this->assertSame('-10.2', Math::bcround('-10.14', new MathContext(1, MathContext::ROUND_UP)));
		//positive, p2 
		$this->assertSame('10.01', Math::bcround('10.005', new MathContext(2, MathContext::ROUND_UP)));
		$this->assertSame('10.02', Math::bcround('10.015', new MathContext(2, MathContext::ROUND_UP)));
		$this->assertSame('10.02', Math::bcround('10.016', new MathContext(2, MathContext::ROUND_UP)));
		$this->assertSame('10.02', Math::bcround('10.014', new MathContext(2, MathContext::ROUND_UP)));
		//negative, p2 
		$this->assertSame('-10.01', Math::bcround('-10.005', new MathContext(2, MathContext::ROUND_UP)));
		$this->assertSame('-10.02', Math::bcround('-10.015', new MathContext(2, MathContext::ROUND_UP)));
		$this->assertSame('-10.02', Math::bcround('-10.016', new MathContext(2, MathContext::ROUND_UP)));
		$this->assertSame('-10.02', Math::bcround('-10.014', new MathContext(2, MathContext::ROUND_UP)));
		//positive, p2, overflow 
		$this->assertSame('11', Math::bcround('10.995', new MathContext(2, MathContext::ROUND_UP)));
		//negative, p2, overflow 
		$this->assertSame('-11', Math::bcround('-10.995', new MathContext(2, MathContext::ROUND_UP)));
		//positive, p0, integer 
		$this->assertSame('1005', Math::bcround('1005', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('1015', Math::bcround('1015', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('1016', Math::bcround('1016', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('1014', Math::bcround('1014', new MathContext(0, MathContext::ROUND_UP)));
		//negative, p0, integer 
		$this->assertSame('-1005', Math::bcround('-1005', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('-1015', Math::bcround('-1015', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('-1016', Math::bcround('-1016', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('-1014', Math::bcround('-1014', new MathContext(0, MathContext::ROUND_UP)));
		//positive, p-1, integer 
		$this->assertSame('1010', Math::bcround('1005', new MathContext(-1, MathContext::ROUND_UP)));
		$this->assertSame('1020', Math::bcround('1015', new MathContext(-1, MathContext::ROUND_UP)));
		$this->assertSame('1020', Math::bcround('1016', new MathContext(-1, MathContext::ROUND_UP)));
		$this->assertSame('1020', Math::bcround('1014', new MathContext(-1, MathContext::ROUND_UP)));
		//negative, p-1, integer 
		$this->assertSame('-1010', Math::bcround('-1005', new MathContext(-1, MathContext::ROUND_UP)));
		$this->assertSame('-1020', Math::bcround('-1015', new MathContext(-1, MathContext::ROUND_UP)));
		$this->assertSame('-1020', Math::bcround('-1016', new MathContext(-1, MathContext::ROUND_UP)));
		$this->assertSame('-1020', Math::bcround('-1014', new MathContext(-1, MathContext::ROUND_UP)));
		//positive, p-2, integer 
		$this->assertSame('1100', Math::bcround('1050', new MathContext(-2, MathContext::ROUND_UP)));
		$this->assertSame('1200', Math::bcround('1150', new MathContext(-2, MathContext::ROUND_UP)));
		$this->assertSame('1200', Math::bcround('1161', new MathContext(-2, MathContext::ROUND_UP)));
		$this->assertSame('1200', Math::bcround('1141', new MathContext(-2, MathContext::ROUND_UP)));
		//negative, p-2, integer 
		$this->assertSame('-1100', Math::bcround('-1050', new MathContext(-2, MathContext::ROUND_UP)));
		$this->assertSame('-1200', Math::bcround('-1150', new MathContext(-2, MathContext::ROUND_UP)));
		$this->assertSame('-1200', Math::bcround('-1161', new MathContext(-2, MathContext::ROUND_UP)));
		$this->assertSame('-1200', Math::bcround('-1141', new MathContext(-2, MathContext::ROUND_UP)));
		//positive, p-4, integer 
		$this->assertSame('10000', Math::bcround('5000', new MathContext(-4, MathContext::ROUND_UP)));
		$this->assertSame('10000', Math::bcround('6000', new MathContext(-4, MathContext::ROUND_UP)));
		$this->assertSame('10000', Math::bcround('4000', new MathContext(-4, MathContext::ROUND_UP)));
		//negative, p-4, integer 
		$this->assertSame('-10000', Math::bcround('-5000', new MathContext(-4, MathContext::ROUND_UP)));
		$this->assertSame('-10000', Math::bcround('-6000', new MathContext(-4, MathContext::ROUND_UP)));
		$this->assertSame('-10000', Math::bcround('-4000', new MathContext(-4, MathContext::ROUND_UP)));
		//positive, p-5, integer 
		$this->assertSame('100000', Math::bcround('5000', new MathContext(-5, MathContext::ROUND_UP)));
		$this->assertSame('100000', Math::bcround('6000', new MathContext(-5, MathContext::ROUND_UP)));
		$this->assertSame('100000', Math::bcround('4000', new MathContext(-5, MathContext::ROUND_UP)));
		//negative, p-5, integer 
		$this->assertSame('-100000', Math::bcround('-5000', new MathContext(-5, MathContext::ROUND_UP)));
		$this->assertSame('-100000', Math::bcround('-6000', new MathContext(-5, MathContext::ROUND_UP)));
		$this->assertSame('-100000', Math::bcround('-5000', new MathContext(-5, MathContext::ROUND_UP)));
		//positive, p-1 
		$this->assertSame('1010', Math::bcround('1005.5', new MathContext(-1, MathContext::ROUND_UP)));
		$this->assertSame('1020', Math::bcround('1015.5', new MathContext(-1, MathContext::ROUND_UP)));
		$this->assertSame('1020', Math::bcround('1016.4', new MathContext(-1, MathContext::ROUND_UP)));
		$this->assertSame('1020', Math::bcround('1014.6', new MathContext(-1, MathContext::ROUND_UP)));
		//negative, p-1 
		$this->assertSame('-1010', Math::bcround('-1005.5', new MathContext(-1, MathContext::ROUND_UP)));
		$this->assertSame('-1020', Math::bcround('-1015.1', new MathContext(-1, MathContext::ROUND_UP)));
		$this->assertSame('-1020', Math::bcround('-1016.4', new MathContext(-1, MathContext::ROUND_UP)));
		$this->assertSame('-1020', Math::bcround('-1014.6', new MathContext(-1, MathContext::ROUND_UP)));
		//positive, p-2 
		$this->assertSame('1100', Math::bcround('1050.5', new MathContext(-2, MathContext::ROUND_UP)));
		$this->assertSame('1200', Math::bcround('1150.5', new MathContext(-2, MathContext::ROUND_UP)));
		$this->assertSame('1200', Math::bcround('1161.4', new MathContext(-2, MathContext::ROUND_UP)));
		$this->assertSame('1200', Math::bcround('1141.6', new MathContext(-2, MathContext::ROUND_UP)));
		//negative, p-2 
		$this->assertSame('-1100', Math::bcround('-1050.5', new MathContext(-2, MathContext::ROUND_UP)));
		$this->assertSame('-1200', Math::bcround('-1150.1', new MathContext(-2, MathContext::ROUND_UP)));
		$this->assertSame('-1200', Math::bcround('-1161.4', new MathContext(-2, MathContext::ROUND_UP)));
		$this->assertSame('-1200', Math::bcround('-1141.6', new MathContext(-2, MathContext::ROUND_UP)));
		//positive, p-4 
		$this->assertSame('10000', Math::bcround('5000.1', new MathContext(-4, MathContext::ROUND_UP)));
		$this->assertSame('10000', Math::bcround('6000.4', new MathContext(-4, MathContext::ROUND_UP)));
		$this->assertSame('10000', Math::bcround('4000.6', new MathContext(-4, MathContext::ROUND_UP)));
		//negative, p-4 
		$this->assertSame('-10000', Math::bcround('-5000.1', new MathContext(-4, MathContext::ROUND_UP)));
		$this->assertSame('-10000', Math::bcround('-6000.4', new MathContext(-4, MathContext::ROUND_UP)));
		$this->assertSame('-10000', Math::bcround('-4000.6', new MathContext(-4, MathContext::ROUND_UP)));
		//negative, p-5 
		$this->assertSame('100000', Math::bcround('5000.1', new MathContext(-5, MathContext::ROUND_UP)));
		$this->assertSame('100000', Math::bcround('6000.4', new MathContext(-5, MathContext::ROUND_UP)));
		$this->assertSame('100000', Math::bcround('4000.6', new MathContext(-5, MathContext::ROUND_UP)));
		//negative, p-5 
		$this->assertSame('-100000', Math::bcround('-5000.1', new MathContext(-5, MathContext::ROUND_UP)));
		$this->assertSame('-100000', Math::bcround('-6000.4', new MathContext(-5, MathContext::ROUND_UP)));
		$this->assertSame('-100000', Math::bcround('-5000.6', new MathContext(-5, MathContext::ROUND_UP)));
		
	}
	
// 	public function testRound＿切り上げ＿基本演算()
// 	{
// 		//- round up -
// 		//positive, p0 
// 		$this->assertSame('11', Math::bcround('10.5', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('12', Math::bcround('11.5', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('12', Math::bcround('11.6', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('12', Math::bcround('11.4', new MathContext(0, MathContext::ROUND_UP)));
// 		//negative, p0 
// 		$this->assertSame('-10', Math::bcround('-10.5', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('-11', Math::bcround('-11.5', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('-11', Math::bcround('-11.6', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('-11', Math::bcround('-11.4', new MathContext(0, MathContext::ROUND_UP)));
// 		//positive, p1 
// 		$this->assertSame('10.1', Math::bcround('10.05', new MathContext(1, MathContext::ROUND_UP)));
// 		$this->assertSame('10.2', Math::bcround('10.15', new MathContext(1, MathContext::ROUND_UP)));
// 		$this->assertSame('10.2', Math::bcround('10.16', new MathContext(1, MathContext::ROUND_UP)));
// 		$this->assertSame('10.2', Math::bcround('10.14', new MathContext(1, MathContext::ROUND_UP)));
// 		//negative, p1 
// 		$this->assertSame('-10', Math::bcround('-10.05', new MathContext(1, MathContext::ROUND_UP)));
// 		$this->assertSame('-10.1', Math::bcround('-10.15', new MathContext(1, MathContext::ROUND_UP)));
// 		$this->assertSame('-10.1', Math::bcround('-10.16', new MathContext(1, MathContext::ROUND_UP)));
// 		$this->assertSame('-10.1', Math::bcround('-10.14', new MathContext(1, MathContext::ROUND_UP)));
// 		//positive, p2 
// 		$this->assertSame('10.01', Math::bcround('10.005', new MathContext(2, MathContext::ROUND_UP)));
// 		$this->assertSame('10.02', Math::bcround('10.015', new MathContext(2, MathContext::ROUND_UP)));
// 		$this->assertSame('10.02', Math::bcround('10.016', new MathContext(2, MathContext::ROUND_UP)));
// 		$this->assertSame('10.02', Math::bcround('10.014', new MathContext(2, MathContext::ROUND_UP)));
// 		//negative, p2 
// 		$this->assertSame('-10', Math::bcround('-10.005', new MathContext(2, MathContext::ROUND_UP)));
// 		$this->assertSame('-10.01', Math::bcround('-10.015', new MathContext(2, MathContext::ROUND_UP)));
// 		$this->assertSame('-10.01', Math::bcround('-10.016', new MathContext(2, MathContext::ROUND_UP)));
// 		$this->assertSame('-10.01', Math::bcround('-10.014', new MathContext(2, MathContext::ROUND_UP)));
// 		//positive, p2, overflow 
// 		$this->assertSame('11', Math::bcround('10.995', new MathContext(2, MathContext::ROUND_UP)));
// 		//negative, p2, overflow 
// 		$this->assertSame('-10.99', Math::bcround('-10.995', new MathContext(2, MathContext::ROUND_UP)));
// 		//positive, p0, integer 
// 		$this->assertSame('1005', Math::bcround('1005', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('1015', Math::bcround('1015', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('1016', Math::bcround('1016', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('1014', Math::bcround('1014', new MathContext(0, MathContext::ROUND_UP)));
// 		//negative, p0, integer 
// 		$this->assertSame('-1005', Math::bcround('-1005', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('-1015', Math::bcround('-1015', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('-1016', Math::bcround('-1016', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('-1014', Math::bcround('-1014', new MathContext(0, MathContext::ROUND_UP)));
// 		//positive, p-1, integer 
// 		$this->assertSame('1010', Math::bcround('1005', new MathContext(-1, MathContext::ROUND_UP)));
// 		$this->assertSame('1020', Math::bcround('1015', new MathContext(-1, MathContext::ROUND_UP)));
// 		$this->assertSame('1020', Math::bcround('1016', new MathContext(-1, MathContext::ROUND_UP)));
// 		$this->assertSame('1020', Math::bcround('1014', new MathContext(-1, MathContext::ROUND_UP)));
// 		//negative, p-1, integer 
// 		$this->assertSame('-1000', Math::bcround('-1005', new MathContext(-1, MathContext::ROUND_UP)));
// 		$this->assertSame('-1010', Math::bcround('-1015', new MathContext(-1, MathContext::ROUND_UP)));
// 		$this->assertSame('-1010', Math::bcround('-1016', new MathContext(-1, MathContext::ROUND_UP)));
// 		$this->assertSame('-1010', Math::bcround('-1014', new MathContext(-1, MathContext::ROUND_UP)));
// 		//positive, p-2, integer 
// 		$this->assertSame('1100', Math::bcround('1050', new MathContext(-2, MathContext::ROUND_UP)));
// 		$this->assertSame('1200', Math::bcround('1150', new MathContext(-2, MathContext::ROUND_UP)));
// 		$this->assertSame('1200', Math::bcround('1161', new MathContext(-2, MathContext::ROUND_UP)));
// 		$this->assertSame('1200', Math::bcround('1141', new MathContext(-2, MathContext::ROUND_UP)));
// 		//negative, p-2, integer 
// 		$this->assertSame('-1000', Math::bcround('-1050', new MathContext(-2, MathContext::ROUND_UP)));
// 		$this->assertSame('-1100', Math::bcround('-1150', new MathContext(-2, MathContext::ROUND_UP)));
// 		$this->assertSame('-1100', Math::bcround('-1161', new MathContext(-2, MathContext::ROUND_UP)));
// 		$this->assertSame('-1100', Math::bcround('-1141', new MathContext(-2, MathContext::ROUND_UP)));
// 		//positive, p-4, integer 
// 		$this->assertSame('10000', Math::bcround('5000', new MathContext(-4, MathContext::ROUND_UP)));
// 		$this->assertSame('10000', Math::bcround('6000', new MathContext(-4, MathContext::ROUND_UP)));
// 		$this->assertSame('10000', Math::bcround('4000', new MathContext(-4, MathContext::ROUND_UP)));
// 		//negative, p-4, integer 
// 		$this->assertSame('0', Math::bcround('-5000', new MathContext(-4, MathContext::ROUND_UP)));
// 		$this->assertSame('0', Math::bcround('-6000', new MathContext(-4, MathContext::ROUND_UP)));
// 		$this->assertSame('0', Math::bcround('-4000', new MathContext(-4, MathContext::ROUND_UP)));
// 		//positive, p-5, integer 
// 		$this->assertSame('100000', Math::bcround('5000', new MathContext(-5, MathContext::ROUND_UP)));
// 		$this->assertSame('100000', Math::bcround('6000', new MathContext(-5, MathContext::ROUND_UP)));
// 		$this->assertSame('100000', Math::bcround('4000', new MathContext(-5, MathContext::ROUND_UP)));
// 		//negative, p-5, integer 
// 		$this->assertSame('0', Math::bcround('-5000', new MathContext(-5, MathContext::ROUND_UP)));
// 		$this->assertSame('0', Math::bcround('-6000', new MathContext(-5, MathContext::ROUND_UP)));
// 		$this->assertSame('0', Math::bcround('-5000', new MathContext(-5, MathContext::ROUND_UP)));
// 		//positive, p-1 
// 		$this->assertSame('1010', Math::bcround('1005.5', new MathContext(-1, MathContext::ROUND_UP)));
// 		$this->assertSame('1020', Math::bcround('1015.5', new MathContext(-1, MathContext::ROUND_UP)));
// 		$this->assertSame('1020', Math::bcround('1016.4', new MathContext(-1, MathContext::ROUND_UP)));
// 		$this->assertSame('1020', Math::bcround('1014.6', new MathContext(-1, MathContext::ROUND_UP)));
// 		//negative, p-1 
// 		$this->assertSame('-1000', Math::bcround('-1005.5', new MathContext(-1, MathContext::ROUND_UP)));
// 		$this->assertSame('-1010', Math::bcround('-1015.1', new MathContext(-1, MathContext::ROUND_UP)));
// 		$this->assertSame('-1010', Math::bcround('-1016.4', new MathContext(-1, MathContext::ROUND_UP)));
// 		$this->assertSame('-1010', Math::bcround('-1014.6', new MathContext(-1, MathContext::ROUND_UP)));
// 		//positive, p-2 
// 		$this->assertSame('1100', Math::bcround('1050.5', new MathContext(-2, MathContext::ROUND_UP)));
// 		$this->assertSame('1200', Math::bcround('1150.5', new MathContext(-2, MathContext::ROUND_UP)));
// 		$this->assertSame('1200', Math::bcround('1161.4', new MathContext(-2, MathContext::ROUND_UP)));
// 		$this->assertSame('1200', Math::bcround('1141.6', new MathContext(-2, MathContext::ROUND_UP)));
// 		//negative, p-2 
// 		$this->assertSame('-1000', Math::bcround('-1050.5', new MathContext(-2, MathContext::ROUND_UP)));
// 		$this->assertSame('-1100', Math::bcround('-1150.1', new MathContext(-2, MathContext::ROUND_UP)));
// 		$this->assertSame('-1100', Math::bcround('-1161.4', new MathContext(-2, MathContext::ROUND_UP)));
// 		$this->assertSame('-1100', Math::bcround('-1141.6', new MathContext(-2, MathContext::ROUND_UP)));
// 		//positive, p-4 
// 		$this->assertSame('10000', Math::bcround('5000.1', new MathContext(-4, MathContext::ROUND_UP)));
// 		$this->assertSame('10000', Math::bcround('6000.4', new MathContext(-4, MathContext::ROUND_UP)));
// 		$this->assertSame('10000', Math::bcround('4000.6', new MathContext(-4, MathContext::ROUND_UP)));
// 		//negative, p-4 
// 		$this->assertSame('0', Math::bcround('-5000.1', new MathContext(-4, MathContext::ROUND_UP)));
// 		$this->assertSame('0', Math::bcround('-6000.4', new MathContext(-4, MathContext::ROUND_UP)));
// 		$this->assertSame('0', Math::bcround('-4000.6', new MathContext(-4, MathContext::ROUND_UP)));
// 		//negative, p-5 
// 		$this->assertSame('100000', Math::bcround('5000.1', new MathContext(-5, MathContext::ROUND_UP)));
// 		$this->assertSame('100000', Math::bcround('6000.4', new MathContext(-5, MathContext::ROUND_UP)));
// 		$this->assertSame('100000', Math::bcround('4000.6', new MathContext(-5, MathContext::ROUND_UP)));
// 		//negative, p-5 
// 		$this->assertSame('0', Math::bcround('-5000.1', new MathContext(-5, MathContext::ROUND_UP)));
// 		$this->assertSame('0', Math::bcround('-6000.4', new MathContext(-5, MathContext::ROUND_UP)));
// 		$this->assertSame('0', Math::bcround('-5000.6', new MathContext(-5, MathContext::ROUND_UP)));
		
// 	}
	
	public function testRound_切り捨て_基本演算()
	{
		//- round down -
		//positive, p0
		$this->assertSame('10', Math::bcround('10.5', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('11', Math::bcround('11.5', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('11', Math::bcround('11.6', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('11', Math::bcround('11.4', new MathContext(0, MathContext::ROUND_DOWN)));
		//negative, p0
		$this->assertSame('-10', Math::bcround('-10.5', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('-11', Math::bcround('-11.5', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('-11', Math::bcround('-11.6', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('-11', Math::bcround('-11.4', new MathContext(0, MathContext::ROUND_DOWN)));
		//positive, p1
		$this->assertSame('10', Math::bcround('10.05', new MathContext(1, MathContext::ROUND_DOWN)));
		$this->assertSame('10.1', Math::bcround('10.15', new MathContext(1, MathContext::ROUND_DOWN)));
		$this->assertSame('10.1', Math::bcround('10.16', new MathContext(1, MathContext::ROUND_DOWN)));
		$this->assertSame('10.1', Math::bcround('10.14', new MathContext(1, MathContext::ROUND_DOWN)));
		//negative, p1
		$this->assertSame('-10', Math::bcround('-10.05', new MathContext(1, MathContext::ROUND_DOWN)));
		$this->assertSame('-10.1', Math::bcround('-10.15', new MathContext(1, MathContext::ROUND_DOWN)));
		$this->assertSame('-10.1', Math::bcround('-10.16', new MathContext(1, MathContext::ROUND_DOWN)));
		$this->assertSame('-10.1', Math::bcround('-10.14', new MathContext(1, MathContext::ROUND_DOWN)));
		//positive, p2
		$this->assertSame('10', Math::bcround('10.005', new MathContext(2, MathContext::ROUND_DOWN)));
		$this->assertSame('10.01', Math::bcround('10.015', new MathContext(2, MathContext::ROUND_DOWN)));
		$this->assertSame('10.01', Math::bcround('10.016', new MathContext(2, MathContext::ROUND_DOWN)));
		$this->assertSame('10.01', Math::bcround('10.014', new MathContext(2, MathContext::ROUND_DOWN)));
		//negative, p2
		$this->assertSame('-10', Math::bcround('-10.005', new MathContext(2, MathContext::ROUND_DOWN)));
		$this->assertSame('-10.01', Math::bcround('-10.015', new MathContext(2, MathContext::ROUND_DOWN)));
		$this->assertSame('-10.01', Math::bcround('-10.016', new MathContext(2, MathContext::ROUND_DOWN)));
		$this->assertSame('-10.01', Math::bcround('-10.014', new MathContext(2, MathContext::ROUND_DOWN)));
		//positive, p2, overflow
		$this->assertSame('10.99', Math::bcround('10.995', new MathContext(2, MathContext::ROUND_DOWN)));
		//negative, p2, overflow
		$this->assertSame('-10.99', Math::bcround('-10.995', new MathContext(2, MathContext::ROUND_DOWN)));
		//positive, p0, integer
		$this->assertSame('1005', Math::bcround('1005', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('1015', Math::bcround('1015', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('1016', Math::bcround('1016', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('1014', Math::bcround('1014', new MathContext(0, MathContext::ROUND_DOWN)));
		//negative, p0, integer
		$this->assertSame('-1005', Math::bcround('-1005', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('-1015', Math::bcround('-1015', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('-1016', Math::bcround('-1016', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('-1014', Math::bcround('-1014', new MathContext(0, MathContext::ROUND_DOWN)));
		//positive, p-1, integer
		$this->assertSame('1000', Math::bcround('1005', new MathContext(-1, MathContext::ROUND_DOWN)));
		$this->assertSame('1010', Math::bcround('1015', new MathContext(-1, MathContext::ROUND_DOWN)));
		$this->assertSame('1010', Math::bcround('1016', new MathContext(-1, MathContext::ROUND_DOWN)));
		$this->assertSame('1010', Math::bcround('1014', new MathContext(-1, MathContext::ROUND_DOWN)));
		//negative, p-1, integer
		$this->assertSame('-1000', Math::bcround('-1005', new MathContext(-1, MathContext::ROUND_DOWN)));
		$this->assertSame('-1010', Math::bcround('-1015', new MathContext(-1, MathContext::ROUND_DOWN)));
		$this->assertSame('-1010', Math::bcround('-1016', new MathContext(-1, MathContext::ROUND_DOWN)));
		$this->assertSame('-1010', Math::bcround('-1014', new MathContext(-1, MathContext::ROUND_DOWN)));
		//positive, p-2, integer
		$this->assertSame('1000', Math::bcround('1050', new MathContext(-2, MathContext::ROUND_DOWN)));
		$this->assertSame('1100', Math::bcround('1150', new MathContext(-2, MathContext::ROUND_DOWN)));
		$this->assertSame('1100', Math::bcround('1161', new MathContext(-2, MathContext::ROUND_DOWN)));
		$this->assertSame('1100', Math::bcround('1141', new MathContext(-2, MathContext::ROUND_DOWN)));
		//negative, p-2, integer
		$this->assertSame('-1000', Math::bcround('-1050', new MathContext(-2, MathContext::ROUND_DOWN)));
		$this->assertSame('-1100', Math::bcround('-1150', new MathContext(-2, MathContext::ROUND_DOWN)));
		$this->assertSame('-1100', Math::bcround('-1161', new MathContext(-2, MathContext::ROUND_DOWN)));
		$this->assertSame('-1100', Math::bcround('-1141', new MathContext(-2, MathContext::ROUND_DOWN)));
		//positive, p-4, integer
		$this->assertSame('0', Math::bcround('5000', new MathContext(-4, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('6000', new MathContext(-4, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('4000', new MathContext(-4, MathContext::ROUND_DOWN)));
		//negative, p-4, integer
		$this->assertSame('0', Math::bcround('-5000', new MathContext(-4, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('-6000', new MathContext(-4, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('-4000', new MathContext(-4, MathContext::ROUND_DOWN)));
		//positive, p-5, integer
		$this->assertSame('0', Math::bcround('5000', new MathContext(-5, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('6000', new MathContext(-5, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('4000', new MathContext(-5, MathContext::ROUND_DOWN)));
		//negative, p-5, integer
		$this->assertSame('0', Math::bcround('-5000', new MathContext(-5, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('-6000', new MathContext(-5, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('-5000', new MathContext(-5, MathContext::ROUND_DOWN)));
		//positive, p-1
		$this->assertSame('1000', Math::bcround('1005.5', new MathContext(-1, MathContext::ROUND_DOWN)));
		$this->assertSame('1010', Math::bcround('1015.5', new MathContext(-1, MathContext::ROUND_DOWN)));
		$this->assertSame('1010', Math::bcround('1016.4', new MathContext(-1, MathContext::ROUND_DOWN)));
		$this->assertSame('1010', Math::bcround('1014.6', new MathContext(-1, MathContext::ROUND_DOWN)));
		//negative, p-1
		$this->assertSame('-1000', Math::bcround('-1005.5', new MathContext(-1, MathContext::ROUND_DOWN)));
		$this->assertSame('-1010', Math::bcround('-1015.1', new MathContext(-1, MathContext::ROUND_DOWN)));
		$this->assertSame('-1010', Math::bcround('-1016.4', new MathContext(-1, MathContext::ROUND_DOWN)));
		$this->assertSame('-1010', Math::bcround('-1014.6', new MathContext(-1, MathContext::ROUND_DOWN)));
		//positive, p-2
		$this->assertSame('1000', Math::bcround('1050.5', new MathContext(-2, MathContext::ROUND_DOWN)));
		$this->assertSame('1100', Math::bcround('1150.5', new MathContext(-2, MathContext::ROUND_DOWN)));
		$this->assertSame('1100', Math::bcround('1161.4', new MathContext(-2, MathContext::ROUND_DOWN)));
		$this->assertSame('1100', Math::bcround('1141.6', new MathContext(-2, MathContext::ROUND_DOWN)));
		//negative, p-2
		$this->assertSame('-1000', Math::bcround('-1050.5', new MathContext(-2, MathContext::ROUND_DOWN)));
		$this->assertSame('-1100', Math::bcround('-1150.1', new MathContext(-2, MathContext::ROUND_DOWN)));
		$this->assertSame('-1100', Math::bcround('-1161.4', new MathContext(-2, MathContext::ROUND_DOWN)));
		$this->assertSame('-1100', Math::bcround('-1141.6', new MathContext(-2, MathContext::ROUND_DOWN)));
		//positive, p-4
		$this->assertSame('0', Math::bcround('5000.1', new MathContext(-4, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('6000.4', new MathContext(-4, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('4000.6', new MathContext(-4, MathContext::ROUND_DOWN)));
		//negative, p-4
		$this->assertSame('0', Math::bcround('-5000.1', new MathContext(-4, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('-6000.4', new MathContext(-4, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('-4000.6', new MathContext(-4, MathContext::ROUND_DOWN)));
		//negative, p-5
		$this->assertSame('0', Math::bcround('5000.1', new MathContext(-5, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('6000.4', new MathContext(-5, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('4000.6', new MathContext(-5, MathContext::ROUND_DOWN)));
		//negative, p-5
		$this->assertSame('0', Math::bcround('-5000.1', new MathContext(-5, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('-6000.4', new MathContext(-5, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('-5000.6', new MathContext(-5, MathContext::ROUND_DOWN)));
		
	}
	
// 	public function testRound＿切り捨て＿基本演算()
// 	{
// 		//- round down -
// 		//positive, p0
// 		$this->assertSame('10', Math::bcround('10.5', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('11', Math::bcround('11.5', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('11', Math::bcround('11.6', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('11', Math::bcround('11.4', new MathContext(0, MathContext::ROUND_DOWN)));
// 		//negative, p0
// 		$this->assertSame('-11', Math::bcround('-10.5', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-12', Math::bcround('-11.5', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-12', Math::bcround('-11.6', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-12', Math::bcround('-11.4', new MathContext(0, MathContext::ROUND_DOWN)));
// 		//positive, p1
// 		$this->assertSame('10', Math::bcround('10.05', new MathContext(1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('10.1', Math::bcround('10.15', new MathContext(1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('10.1', Math::bcround('10.16', new MathContext(1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('10.1', Math::bcround('10.14', new MathContext(1, MathContext::ROUND_DOWN)));
// 		//negative, p1
// 		$this->assertSame('-10.1', Math::bcround('-10.05', new MathContext(1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-10.2', Math::bcround('-10.15', new MathContext(1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-10.2', Math::bcround('-10.16', new MathContext(1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-10.2', Math::bcround('-10.14', new MathContext(1, MathContext::ROUND_DOWN)));
// 		//positive, p2
// 		$this->assertSame('10', Math::bcround('10.005', new MathContext(2, MathContext::ROUND_DOWN)));
// 		$this->assertSame('10.01', Math::bcround('10.015', new MathContext(2, MathContext::ROUND_DOWN)));
// 		$this->assertSame('10.01', Math::bcround('10.016', new MathContext(2, MathContext::ROUND_DOWN)));
// 		$this->assertSame('10.01', Math::bcround('10.014', new MathContext(2, MathContext::ROUND_DOWN)));
// 		//negative, p2
// 		$this->assertSame('-10.01', Math::bcround('-10.005', new MathContext(2, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-10.02', Math::bcround('-10.015', new MathContext(2, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-10.02', Math::bcround('-10.016', new MathContext(2, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-10.02', Math::bcround('-10.014', new MathContext(2, MathContext::ROUND_DOWN)));
// 		//positive, p2, overflow
// 		$this->assertSame('10.99', Math::bcround('10.995', new MathContext(2, MathContext::ROUND_DOWN)));
// 		//negative, p2, overflow
// 		$this->assertSame('-11', Math::bcround('-10.995', new MathContext(2, MathContext::ROUND_DOWN)));
// 		//positive, p0, integer
// 		$this->assertSame('1005', Math::bcround('1005', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1015', Math::bcround('1015', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1016', Math::bcround('1016', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1014', Math::bcround('1014', new MathContext(0, MathContext::ROUND_DOWN)));
// 		//negative, p0, integer
// 		$this->assertSame('-1005', Math::bcround('-1005', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1015', Math::bcround('-1015', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1016', Math::bcround('-1016', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1014', Math::bcround('-1014', new MathContext(0, MathContext::ROUND_DOWN)));
// 		//positive, p-1, integer
// 		$this->assertSame('1000', Math::bcround('1005', new MathContext(-1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1010', Math::bcround('1015', new MathContext(-1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1010', Math::bcround('1016', new MathContext(-1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1010', Math::bcround('1014', new MathContext(-1, MathContext::ROUND_DOWN)));
// 		//negative, p-1, integer
// 		$this->assertSame('-1010', Math::bcround('-1005', new MathContext(-1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1020', Math::bcround('-1015', new MathContext(-1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1020', Math::bcround('-1016', new MathContext(-1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1020', Math::bcround('-1014', new MathContext(-1, MathContext::ROUND_DOWN)));
// 		//positive, p-2, integer
// 		$this->assertSame('1000', Math::bcround('1050', new MathContext(-2, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1100', Math::bcround('1150', new MathContext(-2, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1100', Math::bcround('1161', new MathContext(-2, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1100', Math::bcround('1141', new MathContext(-2, MathContext::ROUND_DOWN)));
// 		//negative, p-2, integer
// 		$this->assertSame('-1100', Math::bcround('-1050', new MathContext(-2, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1200', Math::bcround('-1150', new MathContext(-2, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1200', Math::bcround('-1161', new MathContext(-2, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1200', Math::bcround('-1141', new MathContext(-2, MathContext::ROUND_DOWN)));
// 		//positive, p-4, integer
// 		$this->assertSame('0', Math::bcround('5000', new MathContext(-4, MathContext::ROUND_DOWN)));
// 		$this->assertSame('0', Math::bcround('6000', new MathContext(-4, MathContext::ROUND_DOWN)));
// 		$this->assertSame('0', Math::bcround('4000', new MathContext(-4, MathContext::ROUND_DOWN)));
// 		//negative, p-4, integer
// 		$this->assertSame('-10000', Math::bcround('-5000', new MathContext(-4, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-10000', Math::bcround('-6000', new MathContext(-4, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-10000', Math::bcround('-4000', new MathContext(-4, MathContext::ROUND_DOWN)));
// 		//positive, p-5, integer
// 		$this->assertSame('0', Math::bcround('5000', new MathContext(-5, MathContext::ROUND_DOWN)));
// 		$this->assertSame('0', Math::bcround('6000', new MathContext(-5, MathContext::ROUND_DOWN)));
// 		$this->assertSame('0', Math::bcround('4000', new MathContext(-5, MathContext::ROUND_DOWN)));
// 		//negative, p-5, integer
// 		$this->assertSame('-100000', Math::bcround('-5000', new MathContext(-5, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-100000', Math::bcround('-6000', new MathContext(-5, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-100000', Math::bcround('-5000', new MathContext(-5, MathContext::ROUND_DOWN)));
// 		//positive, p-1
// 		$this->assertSame('1000', Math::bcround('1005.5', new MathContext(-1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1010', Math::bcround('1015.5', new MathContext(-1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1010', Math::bcround('1016.4', new MathContext(-1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1010', Math::bcround('1014.6', new MathContext(-1, MathContext::ROUND_DOWN)));
// 		//negative, p-1
// 		$this->assertSame('-1010', Math::bcround('-1005.5', new MathContext(-1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1020', Math::bcround('-1015.1', new MathContext(-1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1020', Math::bcround('-1016.4', new MathContext(-1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1020', Math::bcround('-1014.6', new MathContext(-1, MathContext::ROUND_DOWN)));
// 		//positive, p-2
// 		$this->assertSame('1000', Math::bcround('1050.5', new MathContext(-2, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1100', Math::bcround('1150.5', new MathContext(-2, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1100', Math::bcround('1161.4', new MathContext(-2, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1100', Math::bcround('1141.6', new MathContext(-2, MathContext::ROUND_DOWN)));
// 		//negative, p-2
// 		$this->assertSame('-1100', Math::bcround('-1050.5', new MathContext(-2, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1200', Math::bcround('-1150.1', new MathContext(-2, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1200', Math::bcround('-1161.4', new MathContext(-2, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1200', Math::bcround('-1141.6', new MathContext(-2, MathContext::ROUND_DOWN)));
// 		//positive, p-4
// 		$this->assertSame('0', Math::bcround('5000.1', new MathContext(-4, MathContext::ROUND_DOWN)));
// 		$this->assertSame('0', Math::bcround('6000.4', new MathContext(-4, MathContext::ROUND_DOWN)));
// 		$this->assertSame('0', Math::bcround('4000.6', new MathContext(-4, MathContext::ROUND_DOWN)));
// 		//negative, p-4
// 		$this->assertSame('-10000', Math::bcround('-5000.1', new MathContext(-4, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-10000', Math::bcround('-6000.4', new MathContext(-4, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-10000', Math::bcround('-4000.6', new MathContext(-4, MathContext::ROUND_DOWN)));
// 		//negative, p-5
// 		$this->assertSame('0', Math::bcround('5000.1', new MathContext(-5, MathContext::ROUND_DOWN)));
// 		$this->assertSame('0', Math::bcround('6000.4', new MathContext(-5, MathContext::ROUND_DOWN)));
// 		$this->assertSame('0', Math::bcround('4000.6', new MathContext(-5, MathContext::ROUND_DOWN)));
// 		//negative, p-5
// 		$this->assertSame('-100000', Math::bcround('-5000.1', new MathContext(-5, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-100000', Math::bcround('-6000.4', new MathContext(-5, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-100000', Math::bcround('-5000.6', new MathContext(-5, MathContext::ROUND_DOWN)));
		
// 	}
	
	public function testRound_四捨五入_基本演算()
	{
		//- round half up -
		// example
		// p0
		$this->assertSame('24', Math::bcround('23.5', new MathContext(0, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-24', Math::bcround('-23.5', new MathContext(0, MathContext::ROUNDHALF_UP)));
		$this->assertSame('23', Math::bcround('23.4', new MathContext(0, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-24', Math::bcround('-23.6', new MathContext(0, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-24', Math::bcround('-23.50', new MathContext(0, MathContext::ROUNDHALF_UP)));
		$this->assertSame('23', Math::bcround('23.49', new MathContext(0, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-24', Math::bcround('-23.51', new MathContext(0, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-23', Math::bcround('-23.49', new MathContext(0, MathContext::ROUNDHALF_UP)));
		// p1
		$this->assertSame('23.1', Math::bcround('23.05', new MathContext(1, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-23.1', Math::bcround('-23.05', new MathContext(1, MathContext::ROUNDHALF_UP)));
		$this->assertSame('23', Math::bcround('23.04', new MathContext(1, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-23.1', Math::bcround('-23.06', new MathContext(1, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-23.1', Math::bcround('-23.050', new MathContext(1, MathContext::ROUNDHALF_UP)));
		$this->assertSame('23', Math::bcround('23.049', new MathContext(1, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-23.1', Math::bcround('-23.051', new MathContext(1, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-23', Math::bcround('-23.049', new MathContext(1, MathContext::ROUNDHALF_UP)));
		// p2
		$this->assertSame('23.05', Math::bcround('23.05', new MathContext(2, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-23.05', Math::bcround('-23.05', new MathContext(2, MathContext::ROUNDHALF_UP)));
		$this->assertSame('23.04', Math::bcround('23.04', new MathContext(2, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-23.05', Math::bcround('-23.05', new MathContext(2, MathContext::ROUNDHALF_UP)));
		$this->assertSame('23.01', Math::bcround('23.005', new MathContext(2, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-23.01', Math::bcround('-23.005', new MathContext(2, MathContext::ROUNDHALF_UP)));
		$this->assertSame('23', Math::bcround('23.0049', new MathContext(2, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-23.01', Math::bcround('-23.0051', new MathContext(2, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-23', Math::bcround('-23.0049', new MathContext(2, MathContext::ROUNDHALF_UP)));
		// p3
		$this->assertSame('23.05', Math::bcround('23.05', new MathContext(3, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-23.05', Math::bcround('-23.05', new MathContext(3, MathContext::ROUNDHALF_UP)));
		$this->assertSame('23.04', Math::bcround('23.04', new MathContext(3, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-23.05', Math::bcround('-23.05', new MathContext(3, MathContext::ROUNDHALF_UP)));
		$this->assertSame('23.005', Math::bcround('23.005', new MathContext(3, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-23.005', Math::bcround('-23.005', new MathContext(3, MathContext::ROUNDHALF_UP)));
		$this->assertSame('23.005', Math::bcround('23.0049', new MathContext(3, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-23.005', Math::bcround('-23.0050', new MathContext(3, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-23.005', Math::bcround('-23.0051', new MathContext(3, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-23.005', Math::bcround('-23.00510', new MathContext(3, MathContext::ROUNDHALF_UP)));
		// p-1, integer
		$this->assertSame('30', Math::bcround('25', new MathContext(-1, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-20', Math::bcround('-24', new MathContext(-1, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-30', Math::bcround('-25', new MathContext(-1, MathContext::ROUNDHALF_UP)));
		$this->assertSame('20', Math::bcround('24', new MathContext(-1, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-30', Math::bcround('-26', new MathContext(-1, MathContext::ROUNDHALF_UP)));
		// p-1
		$this->assertSame('30', Math::bcround('25.0', new MathContext(-1, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-20', Math::bcround('-24.0', new MathContext(-1, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-30', Math::bcround('-25.0', new MathContext(-1, MathContext::ROUNDHALF_UP)));
		$this->assertSame('20', Math::bcround('24.9', new MathContext(-1, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-30', Math::bcround('-25.1', new MathContext(-1, MathContext::ROUNDHALF_UP)));
		// p-2, integer
		$this->assertSame('100', Math::bcround('50', new MathContext(-2, MathContext::ROUNDHALF_UP)));
		$this->assertSame('0', Math::bcround('-49', new MathContext(-2, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-100', Math::bcround('-50', new MathContext(-2, MathContext::ROUNDHALF_UP)));
		$this->assertSame('0', Math::bcround('49', new MathContext(-2, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-100', Math::bcround('-51', new MathContext(-2, MathContext::ROUNDHALF_UP)));
		// p-2
		$this->assertSame('100', Math::bcround('50.0', new MathContext(-2, MathContext::ROUNDHALF_UP)));
		$this->assertSame('0', Math::bcround('-49.0', new MathContext(-2, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-100', Math::bcround('-50.0', new MathContext(-2, MathContext::ROUNDHALF_UP)));
		$this->assertSame('0', Math::bcround('49.9', new MathContext(-2, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-100', Math::bcround('-50.1', new MathContext(-2, MathContext::ROUNDHALF_UP)));
		// p-3, integer
		$this->assertSame('0', Math::bcround('25', new MathContext(-3, MathContext::ROUNDHALF_UP)));
		$this->assertSame('0', Math::bcround('-24', new MathContext(-3, MathContext::ROUNDHALF_UP)));
		$this->assertSame('0', Math::bcround('-25', new MathContext(-3, MathContext::ROUNDHALF_UP)));
		$this->assertSame('0', Math::bcround('24', new MathContext(-3, MathContext::ROUNDHALF_UP)));
		$this->assertSame('0', Math::bcround('-26', new MathContext(-3, MathContext::ROUNDHALF_UP)));
		// p-3
		$this->assertSame('0', Math::bcround('25.0', new MathContext(-3, MathContext::ROUNDHALF_UP)));
		$this->assertSame('0', Math::bcround('-24.0', new MathContext(-3, MathContext::ROUNDHALF_UP)));
		$this->assertSame('0', Math::bcround('-25.0', new MathContext(-3, MathContext::ROUNDHALF_UP)));
		$this->assertSame('0', Math::bcround('24.0', new MathContext(-3, MathContext::ROUNDHALF_UP)));
		$this->assertSame('0', Math::bcround('-26.0', new MathContext(-3, MathContext::ROUNDHALF_UP)));
	}
	
	
// 	public function testRound＿四捨五入＿基本演算()
// 	{
// 		//- round half up -
// 		// example
// 		// p0
// 		$this->assertSame('24', Math::bcround('23.5', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-23', Math::bcround('-23.5', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('23', Math::bcround('23.4', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-24', Math::bcround('-23.6', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-23', Math::bcround('-23.50', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('23', Math::bcround('23.49', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-24', Math::bcround('-23.51', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		// p1
// 		$this->assertSame('23.1', Math::bcround('23.05', new MathContext(1, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-23', Math::bcround('-23.05', new MathContext(1, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('23', Math::bcround('23.04', new MathContext(1, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-23', Math::bcround('-23.05', new MathContext(1, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-23', Math::bcround('-23.050', new MathContext(1, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('23', Math::bcround('23.049', new MathContext(1, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-23.1', Math::bcround('-23.051', new MathContext(1, MathContext::ROUNDHALF_UP)));
// 		// p2
// 		$this->assertSame('23.05', Math::bcround('23.05', new MathContext(2, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-23.05', Math::bcround('-23.05', new MathContext(2, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('23.04', Math::bcround('23.04', new MathContext(2, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-23.05', Math::bcround('-23.05', new MathContext(2, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('23.01', Math::bcround('23.005', new MathContext(2, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-23', Math::bcround('-23.005', new MathContext(2, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('23', Math::bcround('23.0049', new MathContext(2, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-23.01', Math::bcround('-23.0051', new MathContext(2, MathContext::ROUNDHALF_UP)));
// 		// p3
// 		$this->assertSame('23.05', Math::bcround('23.05', new MathContext(3, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-23.05', Math::bcround('-23.05', new MathContext(3, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('23.04', Math::bcround('23.04', new MathContext(3, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-23.05', Math::bcround('-23.05', new MathContext(3, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('23.005', Math::bcround('23.005', new MathContext(3, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-23.005', Math::bcround('-23.005', new MathContext(3, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('23.005', Math::bcround('23.0049', new MathContext(3, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-23.005', Math::bcround('-23.0051', new MathContext(3, MathContext::ROUNDHALF_UP)));
// 		// p-1, integer
// 		$this->assertSame('30', Math::bcround('25', new MathContext(-1, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-20', Math::bcround('-25', new MathContext(-1, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('20', Math::bcround('24', new MathContext(-1, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-30', Math::bcround('-26', new MathContext(-1, MathContext::ROUNDHALF_UP)));
// 		// p-1
// 		$this->assertSame('30', Math::bcround('25.0', new MathContext(-1, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-20', Math::bcround('-25.0', new MathContext(-1, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('20', Math::bcround('24.9', new MathContext(-1, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-30', Math::bcround('-25.1', new MathContext(-1, MathContext::ROUNDHALF_UP)));
// 		// p-2, integer
// 		$this->assertSame('100', Math::bcround('50', new MathContext(-2, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('0', Math::bcround('-50', new MathContext(-2, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('0', Math::bcround('49', new MathContext(-2, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-100', Math::bcround('-51', new MathContext(-2, MathContext::ROUNDHALF_UP)));
// 		// p-2
// 		$this->assertSame('100', Math::bcround('50.0', new MathContext(-2, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('0', Math::bcround('-50.0', new MathContext(-2, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('0', Math::bcround('49.9', new MathContext(-2, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-100', Math::bcround('-50.1', new MathContext(-2, MathContext::ROUNDHALF_UP)));
// 		// p-3, integer
// 		$this->assertSame('0', Math::bcround('25', new MathContext(-3, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('0', Math::bcround('-25', new MathContext(-3, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('0', Math::bcround('24', new MathContext(-3, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('0', Math::bcround('-26', new MathContext(-3, MathContext::ROUNDHALF_UP)));
// 		// p-3
// 		$this->assertSame('0', Math::bcround('25.0', new MathContext(-3, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('0', Math::bcround('-25.0', new MathContext(-3, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('0', Math::bcround('24.0', new MathContext(-3, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('0', Math::bcround('-26.0', new MathContext(-3, MathContext::ROUNDHALF_UP)));
// 	}
	
	public function testRound_ゼロで終わる()
	{
	
		$this->assertSame('-2', Math::bcround('-2.500000000', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-2', Math::bcround('-2.490000000', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-2', Math::bcround('-2.000000000', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-2', Math::bcround('-1.510000000', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-2', Math::bcround('-1.500000000', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-1', Math::bcround('-1.490000000', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-1', Math::bcround('-1.000000000', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('0.000000000', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('1', Math::bcround('1.000000000', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('1', Math::bcround('1.490000000', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('2', Math::bcround('1.500000000', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('2', Math::bcround('1.510000000', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('2', Math::bcround('2.000000000', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('2', Math::bcround('2.490000000', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('2', Math::bcround('2.500000000', new MathContext(0, MathContext::ROUND_DEFAULT)));
		
		$this->assertSame('-2', Math::bcround('-2.500000000', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('-2', Math::bcround('-2.490000000', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('-2', Math::bcround('-2.000000000', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('-1', Math::bcround('-1.510000000', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('-1', Math::bcround('-1.500000000', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('-1', Math::bcround('-1.490000000', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('-1', Math::bcround('-1.000000000', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('0.000000000', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('1', Math::bcround('1.000000000', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('1', Math::bcround('1.490000000', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('1', Math::bcround('1.500000000', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('1', Math::bcround('1.510000000', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('2', Math::bcround('2.000000000', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('2', Math::bcround('2.490000000', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('2', Math::bcround('2.500000000', new MathContext(0, MathContext::ROUND_DOWN)));
		
// 		$this->assertSame('-3', Math::bcround('-2.500000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-3', Math::bcround('-2.490000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-2', Math::bcround('-2.000000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-2', Math::bcround('-1.510000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-2', Math::bcround('-1.500000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-2', Math::bcround('-1.490000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1', Math::bcround('-1.000000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('0', Math::bcround('0.000000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1', Math::bcround('1.000000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1', Math::bcround('1.490000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1', Math::bcround('1.500000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1', Math::bcround('1.510000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('2', Math::bcround('2.000000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('2', Math::bcround('2.490000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('2', Math::bcround('2.500000000', new MathContext(0, MathContext::ROUND_DOWN)));
	
		$this->assertSame('-3', Math::bcround('-2.500000000', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('-3', Math::bcround('-2.490000000', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('-2', Math::bcround('-2.000000000', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('-2', Math::bcround('-1.510000000', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('-2', Math::bcround('-1.500000000', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('-2', Math::bcround('-1.490000000', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('-1', Math::bcround('-1.000000000', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('0', Math::bcround('0.000000000', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('1', Math::bcround('1.000000000', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('2', Math::bcround('1.490000000', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('2', Math::bcround('1.500000000', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('2', Math::bcround('1.510000000', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('2', Math::bcround('2.000000000', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('3', Math::bcround('2.490000000', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('3', Math::bcround('2.500000000', new MathContext(0, MathContext::ROUND_UP)));
		
// 		$this->assertSame('-2', Math::bcround('-2.500000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('-2', Math::bcround('-2.490000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('-2', Math::bcround('-2.000000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('-1', Math::bcround('-1.510000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('-1', Math::bcround('-1.500000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('-1', Math::bcround('-1.490000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('-1', Math::bcround('-1.000000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('0', Math::bcround('0.000000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('1', Math::bcround('1.000000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('2', Math::bcround('1.490000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('2', Math::bcround('1.500000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('2', Math::bcround('1.510000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('2', Math::bcround('2.000000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('3', Math::bcround('2.490000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('3', Math::bcround('2.500000000', new MathContext(0, MathContext::ROUND_UP)));
		
	
		$this->assertSame('-3', Math::bcround('-2.500000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-2', Math::bcround('-2.490000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-2', Math::bcround('-2.000000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-2', Math::bcround('-1.510000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-2', Math::bcround('-1.500000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-1', Math::bcround('-1.490000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
		$this->assertSame('-1', Math::bcround('-1.000000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
		$this->assertSame('0', Math::bcround('0.000000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
		$this->assertSame('1', Math::bcround('1.000000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
		$this->assertSame('1', Math::bcround('1.490000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
		$this->assertSame('2', Math::bcround('1.500000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
		$this->assertSame('2', Math::bcround('1.510000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
		$this->assertSame('2', Math::bcround('2.000000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
		$this->assertSame('2', Math::bcround('2.490000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
		$this->assertSame('3', Math::bcround('2.500000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
		
// 		$this->assertSame('-2', Math::bcround('-2.500000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-2', Math::bcround('-2.490000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-2', Math::bcround('-2.000000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-2', Math::bcround('-1.510000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-1', Math::bcround('-1.500000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-1', Math::bcround('-1.490000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-1', Math::bcround('-1.000000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('0', Math::bcround('0.000000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('1', Math::bcround('1.000000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('1', Math::bcround('1.490000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('2', Math::bcround('1.500000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('2', Math::bcround('1.510000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('2', Math::bcround('2.000000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('2', Math::bcround('2.490000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('3', Math::bcround('2.500000000', new MathContext(0, MathContext::ROUNDHALF_UP)));

		$this->assertSame('0', Math::bcround('50.00000000', new MathContext(-2, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('50.00000000', new MathContext(-2, MathContext::ROUND_DOWN)));
		$this->assertSame('100', Math::bcround('50.00000000', new MathContext(-2, MathContext::ROUND_UP)));
		$this->assertSame('100', Math::bcround('50.00000000', new MathContext(-2, MathContext::ROUNDHALF_UP)));
	}
	
	public function testRound_整数部なし()
	{
		//-over precision -
		$this->assertSame('1.5', Math::bcround('1.5', new MathContext(5, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-1.5', Math::bcround('-1.5', new MathContext(5, MathContext::ROUND_DEFAULT)));
		$this->assertSame('1.5', Math::bcround('1.5', new MathContext(5, MathContext::ROUND_UP)));
		$this->assertSame('-1.5', Math::bcround('-1.5', new MathContext(5, MathContext::ROUND_UP)));
// 		$this->assertSame('1.5', Math::bcround('1.5', new MathContext(5, MathContext::ROUND_UP)));
// 		$this->assertSame('-1.5', Math::bcround('-1.5', new MathContext(5, MathContext::ROUND_UP)));
		$this->assertSame('1.5', Math::bcround('1.5', new MathContext(5, MathContext::ROUND_DOWN)));
		$this->assertSame('-1.5', Math::bcround('-1.5', new MathContext(5, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1.5', Math::bcround('1.5', new MathContext(5, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1.5', Math::bcround('-1.5', new MathContext(5, MathContext::ROUND_DOWN)));
		//-zero -
		$this->assertSame('0', Math::bcround('0', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('0', new MathContext(-5, MathContext::ROUND_UP)));
// 		$this->assertSame('0', Math::bcround('0', new MathContext(-5, MathContext::ROUND_UP)));
		$this->assertSame('0', Math::bcround('-0', new MathContext(-5, MathContext::ROUND_DOWN)));
// 		$this->assertSame('0', Math::bcround('-0', new MathContext(-5, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('0.0', new MathContext(-5, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('0.0', new MathContext(-5, MathContext::ROUND_UP)));
// 		$this->assertSame('0', Math::bcround('0.0', new MathContext(-5, MathContext::ROUND_UP)));
		$this->assertSame('0', Math::bcround('-0.0', new MathContext(-5, MathContext::ROUND_DOWN)));
// 		$this->assertSame('0', Math::bcround('-0.0', new MathContext(-5, MathContext::ROUND_DOWN)));
		//- zero -
		$this->assertSame('0', Math::bcround('.5', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('-.5', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('0.5', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('-0.5', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('.05', new MathContext(1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('-.05', new MathContext(1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('0.05', new MathContext(1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('-0.05', new MathContext(1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('.05', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('-.01', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('0.05', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('0.01', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('10', Math::bcround('5.01', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('4.99', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('-0.05', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		
		$this->assertSame('1', Math::bcround('.1', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('-1', Math::bcround('-.1', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('1', Math::bcround('0.1', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('-1', Math::bcround('-0.1', new MathContext(0, MathContext::ROUND_UP)));
		$this->assertSame('0.1', Math::bcround('.01', new MathContext(1, MathContext::ROUND_UP)));
		$this->assertSame('-0.1', Math::bcround('-.01', new MathContext(1, MathContext::ROUND_UP)));
		$this->assertSame('0.1', Math::bcround('0.01', new MathContext(1, MathContext::ROUND_UP)));
		$this->assertSame('-0.1', Math::bcround('-0.01', new MathContext(1, MathContext::ROUND_UP)));
		$this->assertSame('10', Math::bcround('.01', new MathContext(-1, MathContext::ROUND_UP)));
		$this->assertSame('-10', Math::bcround('-.01', new MathContext(-1, MathContext::ROUND_UP)));
		$this->assertSame('10', Math::bcround('0.01', new MathContext(-1, MathContext::ROUND_UP)));
		$this->assertSame('-10', Math::bcround('-0.01', new MathContext(-1, MathContext::ROUND_UP)));
		
// 		$this->assertSame('1', Math::bcround('.1', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('0', Math::bcround('-.1', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('1', Math::bcround('0.1', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('0', Math::bcround('-0.1', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('0.1', Math::bcround('.01', new MathContext(1, MathContext::ROUND_UP)));
// 		$this->assertSame('0', Math::bcround('-.01', new MathContext(1, MathContext::ROUND_UP)));
// 		$this->assertSame('0.1', Math::bcround('0.01', new MathContext(1, MathContext::ROUND_UP)));
// 		$this->assertSame('0', Math::bcround('-0.01', new MathContext(1, MathContext::ROUND_UP)));
// 		$this->assertSame('10', Math::bcround('.01', new MathContext(-1, MathContext::ROUND_UP)));
// 		$this->assertSame('0', Math::bcround('-.01', new MathContext(-1, MathContext::ROUND_UP)));
// 		$this->assertSame('10', Math::bcround('0.01', new MathContext(-1, MathContext::ROUND_UP)));
// 		$this->assertSame('0', Math::bcround('-0.01', new MathContext(-1, MathContext::ROUND_UP)));
		
		$this->assertSame('0', Math::bcround('.1', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('-.1', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('0.1', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('-0.1', new MathContext(0, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('.01', new MathContext(1, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('-.01', new MathContext(1, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('0.01', new MathContext(1, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('-0.01', new MathContext(1, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('.01', new MathContext(-1, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('-.01', new MathContext(-1, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('0.01', new MathContext(-1, MathContext::ROUND_DOWN)));
		$this->assertSame('0', Math::bcround('-0.01', new MathContext(-1, MathContext::ROUND_DOWN)));
		
// 		$this->assertSame('0', Math::bcround('.1', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1', Math::bcround('-.1', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('0', Math::bcround('0.1', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1', Math::bcround('-0.1', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('0', Math::bcround('.01', new MathContext(1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-0.1', Math::bcround('-.01', new MathContext(1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('0', Math::bcround('0.01', new MathContext(1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-0.1', Math::bcround('-0.01', new MathContext(1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('0', Math::bcround('.01', new MathContext(-1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-10', Math::bcround('-.01', new MathContext(-1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('0', Math::bcround('0.01', new MathContext(-1, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-10', Math::bcround('-0.01', new MathContext(-1, MathContext::ROUND_DOWN)));
		
	}
	
	
	public function testRound_小数部なし()
	{
		//
		$this->assertSame('-6', Math::bcround('-6', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-5', Math::bcround('-5', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-4', Math::bcround('-4', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('0', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('4', Math::bcround('4', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('5', Math::bcround('5', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('6', Math::bcround('6', new MathContext(0, MathContext::ROUND_DEFAULT)));
		
		$this->assertSame('-6', Math::bcround('-6.', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-5', Math::bcround('-5.', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-4', Math::bcround('-4.', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('0.', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('4', Math::bcround('4.', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('5', Math::bcround('5.', new MathContext(0, MathContext::ROUND_DEFAULT)));
		$this->assertSame('6', Math::bcround('6.', new MathContext(0, MathContext::ROUND_DEFAULT)));
		
		$this->assertSame('-6', Math::bcround('-6.', new MathContext(1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-5', Math::bcround('-5.', new MathContext(1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('-4', Math::bcround('-4.', new MathContext(1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('0.', new MathContext(1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('4', Math::bcround('4.', new MathContext(1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('5', Math::bcround('5.', new MathContext(1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('6', Math::bcround('6.', new MathContext(1, MathContext::ROUND_DEFAULT)));
		
		$this->assertSame('-10', Math::bcround('-6.', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('-5.', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('-4.', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('0.', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('4.', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('0', Math::bcround('5.', new MathContext(-1, MathContext::ROUND_DEFAULT)));
		$this->assertSame('10', Math::bcround('6.', new MathContext(-1, MathContext::ROUND_DEFAULT)));
	
// 		//
// 		$this->assertSame('-2', Math::bcround('-2.500000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-2', Math::bcround('-2.490000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-2', Math::bcround('-2.000000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1', Math::bcround('-1.510000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1', Math::bcround('-1.500000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1', Math::bcround('-1.490000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('-1', Math::bcround('-1.000000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('0', Math::bcround('0.000000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1', Math::bcround('1.000000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1', Math::bcround('1.490000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1', Math::bcround('1.500000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('1', Math::bcround('1.510000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('2', Math::bcround('2.000000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('2', Math::bcround('2.490000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		$this->assertSame('2', Math::bcround('2.500000000', new MathContext(0, MathContext::ROUND_DOWN)));
	
// 		// 		$this->assertSame('-3', Math::bcround('-2.500000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		// 		$this->assertSame('-3', Math::bcround('-2.490000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		// 		$this->assertSame('-2', Math::bcround('-2.000000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		// 		$this->assertSame('-2', Math::bcround('-1.510000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		// 		$this->assertSame('-2', Math::bcround('-1.500000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		// 		$this->assertSame('-2', Math::bcround('-1.490000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		// 		$this->assertSame('-1', Math::bcround('-1.000000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		// 		$this->assertSame('0', Math::bcround('0.000000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		// 		$this->assertSame('1', Math::bcround('1.000000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		// 		$this->assertSame('1', Math::bcround('1.490000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		// 		$this->assertSame('1', Math::bcround('1.500000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		// 		$this->assertSame('1', Math::bcround('1.510000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		// 		$this->assertSame('2', Math::bcround('2.000000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		// 		$this->assertSame('2', Math::bcround('2.490000000', new MathContext(0, MathContext::ROUND_DOWN)));
// 		// 		$this->assertSame('2', Math::bcround('2.500000000', new MathContext(0, MathContext::ROUND_DOWN)));
	
// 		$this->assertSame('-3', Math::bcround('-2.500000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('-3', Math::bcround('-2.490000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('-2', Math::bcround('-2.000000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('-2', Math::bcround('-1.510000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('-2', Math::bcround('-1.500000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('-2', Math::bcround('-1.490000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('-1', Math::bcround('-1.000000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('0', Math::bcround('0.000000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('1', Math::bcround('1.000000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('2', Math::bcround('1.490000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('2', Math::bcround('1.500000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('2', Math::bcround('1.510000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('2', Math::bcround('2.000000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('3', Math::bcround('2.490000000', new MathContext(0, MathContext::ROUND_UP)));
// 		$this->assertSame('3', Math::bcround('2.500000000', new MathContext(0, MathContext::ROUND_UP)));
	
// 		// 		$this->assertSame('-2', Math::bcround('-2.500000000', new MathContext(0, MathContext::ROUND_UP)));
// 		// 		$this->assertSame('-2', Math::bcround('-2.490000000', new MathContext(0, MathContext::ROUND_UP)));
// 		// 		$this->assertSame('-2', Math::bcround('-2.000000000', new MathContext(0, MathContext::ROUND_UP)));
// 		// 		$this->assertSame('-1', Math::bcround('-1.510000000', new MathContext(0, MathContext::ROUND_UP)));
// 		// 		$this->assertSame('-1', Math::bcround('-1.500000000', new MathContext(0, MathContext::ROUND_UP)));
// 		// 		$this->assertSame('-1', Math::bcround('-1.490000000', new MathContext(0, MathContext::ROUND_UP)));
// 		// 		$this->assertSame('-1', Math::bcround('-1.000000000', new MathContext(0, MathContext::ROUND_UP)));
// 		// 		$this->assertSame('0', Math::bcround('0.000000000', new MathContext(0, MathContext::ROUND_UP)));
// 		// 		$this->assertSame('1', Math::bcround('1.000000000', new MathContext(0, MathContext::ROUND_UP)));
// 		// 		$this->assertSame('2', Math::bcround('1.490000000', new MathContext(0, MathContext::ROUND_UP)));
// 		// 		$this->assertSame('2', Math::bcround('1.500000000', new MathContext(0, MathContext::ROUND_UP)));
// 		// 		$this->assertSame('2', Math::bcround('1.510000000', new MathContext(0, MathContext::ROUND_UP)));
// 		// 		$this->assertSame('2', Math::bcround('2.000000000', new MathContext(0, MathContext::ROUND_UP)));
// 		// 		$this->assertSame('3', Math::bcround('2.490000000', new MathContext(0, MathContext::ROUND_UP)));
// 		// 		$this->assertSame('3', Math::bcround('2.500000000', new MathContext(0, MathContext::ROUND_UP)));
	
	
// 		$this->assertSame('-2', Math::bcround('-2.500000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-2', Math::bcround('-2.490000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-2', Math::bcround('-2.000000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-2', Math::bcround('-1.510000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-1', Math::bcround('-1.500000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-1', Math::bcround('-1.490000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('-1', Math::bcround('-1.000000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('0', Math::bcround('0.000000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('1', Math::bcround('1.000000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('1', Math::bcround('1.490000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('2', Math::bcround('1.500000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('2', Math::bcround('1.510000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('2', Math::bcround('2.000000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('2', Math::bcround('2.490000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
// 		$this->assertSame('3', Math::bcround('2.500000000', new MathContext(0, MathContext::ROUNDHALF_UP)));
	}
	
}
