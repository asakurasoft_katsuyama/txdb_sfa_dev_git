<?php
namespace TxCore\Math\Test;


use TxCore\Math\MathContext;

use TxCore\TxCoreException;

use \UnitTestCase;
use TxCore\Math\RPN;

class RPNTest extends UnitTestCase
{
	/**
	 * @var RPN
	 */
	protected $target;
	
	public function __construct() 
	{
		$this->target = new RPN();
	}

	public function testEvaluate＿基本演算() 
	{
		$this->target->setMathContext(new MathContext());
		$this->target->setType(RPN::MATHMATIC);
		$this->assertIdentical('3', $this->target->evaluate('1+2'));	// addition
		$this->assertIdentical('6', $this->target->evaluate('1+2+3'));	// addition
		$this->assertIdentical('-1', $this->target->evaluate('1 - 2'));	// subtraction
		$this->assertIdentical('5', $this->target->evaluate('2 + 9 - 6')); 
		$this->assertIdentical('2', $this->target->evaluate('1 *2'));	// multiplication
		$this->assertIdentical('2', $this->target->evaluate(' 4 /2'));	// Division
		$this->assertIdentical('4', $this->target->evaluate('2 ^ 2'));	// power
		$this->assertIdentical('56', $this->target->evaluate('2 + 9 * 6')); 
		$this->assertIdentical('4', $this->target->evaluate(' 1+(4-1)'));	//  parentheses
		$this->assertIdentical('1', $this->target->evaluate('-1+2'));//unary munus
		$this->assertIdentical('4', $this->target->evaluate('1+(-1+4)'));
		$this->assertIdentical('-3', $this->target->evaluate('1+(4*-1)'));
		$this->assertIdentical(null, $this->target->evaluate('a+1', array('a'=>'a')));	// addition
		//
	}
	
	public function testEvaluate＿文字列演算()
	{
		$this->target->setType(RPN::INTERPOLATE);
		$this->assertIdentical('ab', $this->target->evaluate('"a"&"b"'));	// addition
		$this->assertIdentical('acd', $this->target->evaluate('"a" & "c"&"d"'));	// addition
	}
	
	public function testEvaluate＿文字列演算と変数()
	{
		$this->target->setType(RPN::INTERPOLATE);
		$this->assertIdentical('210', $this->target->evaluate('合計&"10"', array('合計'=>2)));
		$this->assertIdentical('あ2', $this->target->evaluate('"あ" & 文字列', array('文字列'=>2)));
	}
	
	public function testEvaluate＿変数()
	{
		$this->target->setMathContext(new MathContext());
		$this->target->setType(RPN::MATHMATIC);
		$this->assertIdentical('4', $this->target->evaluate('a+2', array('a'=>2)));
	}
	
	public function testEvaluate＿ヌル演算()
	{
		$this->target->setType(RPN::MATHMATIC);
		$this->assertIdentical(null, $this->target->evaluate('a+2', array('a'=>null)));	// addition
		$this->assertIdentical(null, $this->target->evaluate('2+a', array('a'=>null)));
		$this->assertIdentical(null, $this->target->evaluate('a - 2', array('a'=>null)));	// subtraction
		$this->assertIdentical(null, $this->target->evaluate('1 - a', array('a'=>null)));
		$this->assertIdentical(null, $this->target->evaluate('1 *a', array('a'=>null)));	// multiplication
		$this->assertIdentical(null, $this->target->evaluate('a *2', array('a'=>null)));
		$this->assertIdentical(null, $this->target->evaluate(' a /2', array('a'=>null)));	// Division
		$this->assertIdentical(null, $this->target->evaluate(' 4 /a', array('a'=>null)));
		$this->assertIdentical(null, $this->target->evaluate('a ^ 2', array('a'=>null)));	// power
		$this->assertIdentical(null, $this->target->evaluate('2 ^ a', array('a'=>null)));
	}
	
	public function testEvaluate＿スケールと丸め()
	{
		$this->target->setType(RPN::MATHMATIC);
		$this->target->setMathContext(new MathContext(2, MathContext::ROUND_DOWN));
		$this->assertIdentical('4000.24', $this->target->evaluate('2000.123 *2'));
	}
	
	public function testEvaluate＿構文エラー()
	{
		try {
			$this->target->evaluate('*');
			$this->fail();
		} catch (TxCoreException $e) {
			$this->assertNotNull(TxCoreException::INVALID_FORMULA, $e->getCode());
		}
		try {
			$this->target->evaluate('10*(');
			$this->fail();
		} catch (TxCoreException $e) {
			$this->assertNotNull(TxCoreException::INVALID_FORMULA, $e->getCode());
		}
	}

	public function testEvaluate_右結合()
	{
		$this->target->setMathContext(new MathContext());
		$this->target->setType(RPN::MATHMATIC);
		$this->assertIdentical('65536', $this->target->evaluate('a ^ b ^ c', array('a'=>4, 'b'=>2, 'c'=>3)));
		$this->assertIdentical('65536', $this->target->evaluate('a^b^c', array('a'=>4, 'b'=>2, 'c'=>3)));
		$this->assertIdentical('65536', $this->target->evaluate('4^2^3'));
		$this->assertIdentical('4096', $this->target->evaluate('(4^2)^3'));
		$this->assertIdentical('65536', $this->target->evaluate('4^(2^3)'));
	}

	public function testEvaluate_左結合()
	{
		$this->target->setMathContext(new MathContext());
		$this->target->setType(RPN::MATHMATIC);
		$this->assertIdentical('-1', $this->target->evaluate('a - b + c', array('a'=>2, 'b'=>9, 'c'=>6)));
		$this->assertIdentical('-1', $this->target->evaluate('a-b+c', array('a'=>2, 'b'=>9, 'c'=>6)));
		$this->assertIdentical('-1', $this->target->evaluate('2-9+6'));
		$this->assertIdentical('-1', $this->target->evaluate('(2-9)+6'));
		$this->assertIdentical('-13', $this->target->evaluate('2-(9+6)'));
		$this->assertIdentical('36', $this->target->evaluate('24/2*3'));
		$this->assertIdentical('36', $this->target->evaluate('(24/2)*3'));
		$this->assertIdentical('4', $this->target->evaluate('24/(2*3)'));
	}

	public function testEvaluate_優先順位()
	{
		$this->target->setMathContext(new MathContext());
		$this->target->setType(RPN::MATHMATIC);
		$this->assertIdentical('15', $this->target->evaluate('4-(2-(2+6)-8)-3'));
		$this->assertIdentical('225', $this->target->evaluate('(4-(2-(2+6)-8)-3)*(4-(2-(2+6)-8)-3)')); // 15*15
		$this->assertIdentical('65536', $this->target->evaluate('4^(2*(2+6)/8)^3'));
    }
}
