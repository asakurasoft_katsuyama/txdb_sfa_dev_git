<?php
namespace TxCore;

class ViewColumn
{
    /**
     * @var
     */
    private $width;

    private $columnId;
    /**
     * @var Table
     */
    private $table;

    /**
     * @param Table $table
     */
    public function __construct(Table $table)
    {
        $this->table = $table;
    }

    /**
     * @return Column
     */
    public function getColumn()
    {
        return $this->table->getColumnFor($this->columnId);
    }

    /**
     * @return mixed
     */
    public function getColumnId()
    {
        return $this->columnId;
    }

    /**
     * @param mixed $columnId
     */
    public function _setColumnId($columnId)
    {
        $this->columnId = Util::toInt($columnId);
    }


    /**
     * @return mixed
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @param mixed $width
     */
    public function setWidth($width)
    {
        $this->width = Util::toInt($width);
    }

    /**
     * @param Column $column
     */
    public function setColumn($column)
    {
        $this->columnId = $column->getId();
    }
}
