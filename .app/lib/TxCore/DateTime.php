<?php
namespace TxCore;

use \DateTime;
use \DateInterval;


class ___File
{
	private $fileName;
	private $contentType;
// 	private $url;
	private $filekey;
	private $size;
	private $isTemp;

	/**
	 * @var Column
	 */
	private $parent;


	public static function create($path, $fileName, $contentType)
	{
		$file = new self();
		$file->_setFileName($fileName);
		$file->_setFilekey($path);
		$file->_setContentType($contentType);
		$file->_setTemp(true);
		return $file;
	}


	public function getFileName()
	{
		return $this->fileName;
	}
	public function _setFileName($value)
	{
		$this->fileName = $value;
	}
	public function _setParent(Column $column)
	{
		$this->parent = $column;
	}
	public function getParent()
	{
		return $this->parent;
	}
	public function getContentType()
	{
		return $this->contentType;
	}
	public function _setContentType($value)
	{
		$this->contentType = $value;
	}
	public function _setTemp($value)
	{
		$this->isTemp = $value;
	}

	public function getFilekey()
	{
		return $this->filekey;
	}
	public function _setFilekey($value)
	{
		$this->filekey = $value;
	}

	public function getSize()
	{
		return $this->size;
	}
	public function _setSize($value)
	{
		$this->size = $value;
	}

	/**
	 * @return bool
	 */
	public function isTemp()
	{
		return $this->isTemp;
	}


	/**
	 * @return string
	 */
	public function getUrl()
	{
		if ($this->isTemp()) {
			return null;
		}
		return Engine::factory()->getStorage()->getFileUrl($this->filekey);
	}


}
