<?php
namespace TxCore;


/**
 * カレンダーView
 */
class CalendarView
{

    private $userOffset = 0;
    private $userTotalCount = 0;
    private $userCount = 0;


    /**
     * @var string
     */
    private $startDate;


    /**
     * @var string[]
     */
    private $dateList;

    /**
     * @var CalendarView__UserEntry[]
     */
    private $series;

    /**
     * @param string $startDate
     * @param $dateList
     * @param $series
     */
    private function __construct($startDate, $dateList, $series)
    {
        $this->series = $series;
        $this->dateList = $dateList;
        $this->startDate = $startDate;
    }

    /**
     * @param Table $table
     * @param array $entities
     * @param User $by
     * @param int $userOffset
     * @param int $userLimit
     * @param string $startDate
     * @return CalendarView
     * @throws TxCoreException
     * @throws \Exception
     */
    public static function search(Table $table, array $entities, User $by,
                                  $userOffset = 0, $userLimit = 10, $startDate = null)
    {
        if (!$table->isCalendarEnabled()) {
            throw new \Exception("Calendar was disable.");
        }
        //log_debug("startDate=".$startDate);
        //var_dump($startDate);
        $dateTime = null;
        if (is_empty($startDate)) {
            $dateTime = new \DateTime();
            $dateTime->modify('next day')
                ->modify('last monday');
        } else {
            $dateTime = new \DateTime($startDate);
        }
        $endTime = clone $dateTime;
        $endTime->modify('+7 days');

        $engine = Engine::factory();
        $criteria1 = new UserSearchCriteria();
        $criteria1->setInEntities($entities);
        $userList = $engine->searchUsers_($criteria1, $userOffset, $userLimit);

        $settings = $table->getCalendarSettings();
        $userColumn = $settings->getUserColumn();
        $dateColumn = $settings->getDateColumn();
        $titleColumn = $settings->getTitleColumn();
        $startTimeColumn = $settings->getStartTimeColumn();
        $endTimeColumn = $settings->getEndTimeColumn();

        $criteria2 = new Criteria();
        if (is_not_empty($userList->getRows())) {
            $criteria2->addCondition($userColumn, Criteria::CONTAINS, $userList->getRows());
        } else {
            $criteria2->addCondition($table->getRownum(), Criteria::EQUALS, -1);    // Dummy  （グループに所属するユーザーが0人の場合など）
        }

        $criteria2->addCondition($dateColumn, Criteria::GREATER_EQUALS, $dateTime->format(Util::DATE_FORMAT));
        $criteria2->addCondition($dateColumn, Criteria::LESS, $endTime->format(Util::DATE_FORMAT));
        if ($startTimeColumn) {
            $criteria2->addSort($startTimeColumn);
        } else {
            $criteria2->addSort($table->getRownum());
        }

        $list_ = $table->search($criteria2, $by);


        $map = array();
        foreach ($list_ as $row) {
            $entry = new CalendarView__Item();
            $entry->setRowId($row->getId());
            $entry->setTitle($row->getText($titleColumn));
            if ($startTimeColumn !== null)
                $entry->setStartTime($row->getValue($startTimeColumn));
            if ($endTimeColumn !== null)
                $entry->setEndTime($row->getValue($endTimeColumn));

            $users = $row->getValue($userColumn);
            /** @var $user User */
            $date = $row->getValue($dateColumn);
            foreach ($users as $user) {
                $map[$user->getId() . '/' . $date][] = $entry;
            }
        }


        $dateList = array();
        /** @var $dateList CalendarView__Date[] */
        for ($i = 0; $i < 7; $i++) {
            $dateItem = new CalendarView__Date();
            $dateItem->setDate(self::getDateLatter($dateTime, $i)->format(Util::DATE_FORMAT));
            $dateList[] = $dateItem;
        }

        $seriesList = array();
        foreach ($userList as $user) {
            $series = new CalendarView__UserEntry();
            $series->setUser($user);

            $dateEntries = array();
            foreach ($dateList as $date) {
                $dateEntry = new CalendarView__Date();
                $dateEntry->setDate($date->getDate());
                $dateEntry->setItems(isset($map[$user->getId() . '/' . $date->getDate()]) ?
                    $map[$user->getId() . '/' . $date->getDate()] : array());
                $dateEntries[] = $dateEntry;
            }

            $series->setDateEntries($dateEntries);
            $seriesList[] = $series;
        }

        $calendar = new self($dateTime->format(Util::DATE_FORMAT), $dateList, $seriesList);
        $calendar->setUserOffset($userList->getOffset());
        $calendar->setUserCount($userList->getRowCount());
        $calendar->setUserTotalCount($userList->getTotalRowCount());
        return $calendar;
    }

    /**
     * @param \DateTime $dateTime
     * @param $n
     * @return \DateTime
     */
    private static function getDateLatter(\DateTime $dateTime, $n)
    {
        $date = clone $dateTime;
        $date->modify(($n >= 0 ? '+' : '-') . $n . ' days');
        return $date;
    }

    /**
     * @return CalendarView__UserEntry[]
     */
    public function getUserEntries()
    {
        return $this->series;
    }

    /**
     * @return string[]
     */
    public function getDateEntries()
    {
        return $this->dateList;
    }

    /**
     * @return string[]
     */
    public function getNextStartDate()
    {
        $date = new \DateTime($this->startDate);
        return $date->modify('+7 days')->format(Util::DATE_FORMAT);
    }

    /**
     * @return string[]
     */
    public function getPrevStartDate()
    {
        $date = new \DateTime($this->startDate);
        return $date->modify('-7 days')->format(Util::DATE_FORMAT);
    }

    /**
     * @return int
     */
    public function getUserOffset()
    {
        return $this->userOffset;
    }

    /**
     * @param int $userOffset
     */
    public function setUserOffset($userOffset)
    {
        $this->userOffset = $userOffset;
    }

    /**
     * @return int
     */
    public function getUserTotalCount()
    {
        return $this->userTotalCount;
    }

    /**
     * @param int $userTotalCount
     */
    public function setUserTotalCount($userTotalCount)
    {
        $this->userTotalCount = $userTotalCount;
    }

    /**
     * @return int
     */
    public function getUserCount()
    {
        return $this->userCount;
    }

    /**
     * @param int $userCount
     */
    public function setUserCount($userCount)
    {
        $this->userCount = $userCount;
    }

    /**
     * @return string
     */
    public function getStartDate()
    {
        return $this->startDate;
    }
}



class CalendarView__Date
{
    /**
     * @var string
     */
    private $date;
    private $items = array();

    /**
     * @return mixed
     */
    public function isToday()
    {
        return $this->date == date('Y-m-d');
    }

    /**
     * @return CalendarView__Item[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param array $items
     */
    public function setItems($items)
    {
        $this->items = $items;
    }

    /**
     * @return string
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @return mixed
     */
    public function getDateFormatted($format = null)
    {
        static $week = array("日", "月", "火", "水", "木", "金", "土");
        $date = new \DateTime($this->date);
        if ($format == null) {
            return $date->format('m/d') .'('.$week[intval($date->format('w'))].')';
        } else if ($format == 'w') {
            return $week[intval($date->format('w'))];
        } else {
            return $date->format($format);
        }
    }

    /**
     * @param string $date
     */
    public function setDate($date)
    {
        $this->date = $date;
    }

}

class CalendarView__Item
{
    private $rowId;
    private $title;
    private $startTime;
    private $endTime;

    /**
     * @return mixed
     */
    public function getRowId()
    {
        return $this->rowId;
    }

    /**
     * @param mixed $rowId
     */
    public function setRowId($rowId)
    {
        $this->rowId = $rowId;
    }

    /**
     * @return mixed
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param mixed $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getStartTime()
    {
        return $this->startTime;
    }

    /**
     * @param mixed $startTime
     */
    public function setStartTime($startTime)
    {
        $this->startTime = $startTime;
    }

    /**
     * @return mixed
     */
    public function getEndTime()
    {
        return $this->endTime;
    }

    /**
     * @param mixed $endTime
     */
    public function setEndTime($endTime)
    {
        $this->endTime = $endTime;
    }


}

class CalendarView__UserEntry
{
    private $user;
    private $dateEntries = array();

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }

    /**
     * @return CalendarView__Date[]
     */
    public function getDateEntries()
    {
        return $this->dateEntries;
    }

    /**
     * @param array $dateEntries
     */
    public function setDateEntries($dateEntries)
    {
        $this->dateEntries = $dateEntries;
    }

}


