<?php
namespace TxCore;

/**
 * Icon
 */
class Icon
{
	private $filekey;
	private $isPreset;

	public function __construct($filekey, $isPreset = false)  
	{
		$this->filekey = $filekey;
		$this->isPreset = $isPreset;
	}
	
	public function getFilekey() 
	{
		return $this->filekey;
	}
	
	public function getUrl()
	{
		$baseUrl = $this->isPreset ?
			get_config('app', 'preset_icon_dir_url') :
			get_config('app', 'user_icon_dir_url');
		return $this->filekey ? concat_path($baseUrl, $this->filekey) : null;
	}
	
	public function _getLocalPath()
	{
		$baseUrl = $this->isPreset ?
			get_config('app', 'preset_icon_dir') :
			get_config('app', 'user_icon_dir');
		return $this->filekey ? concat_path($baseUrl, $this->filekey) : null;
	}
	
// 	/**
// 	 * @return Icon
// 	 */
// 	public function copy($prefix = '')
// 	{
// 		$fileName = $prefix . uniqid().'.'.pathinfo($this->filekey, PATHINFO_EXTENSION);
// 		$dest = get_config('app', 'user_icon_dir');
// 		if (!file_exists($this->_getLocalPath())) {
// 			throw new TxCoreException('Icon file dose not exists : '.$this->_getLocalPath());
// 		}
		
// 		copy($this->_getLocalPath(), $dest.'/'.$fileName);
// 		return new Icon($fileName, false);
// 	}
	
	
	public function isPreset() 
	{
		return $this->isPreset;
	}
	
	/**
	 * @param string $path Image path to upload.
	 * @return \TxCore\Icon
	 */
	public static function upload($path) 
	{
		return Engine::factory()->uploadIcon($path);
	}
}

