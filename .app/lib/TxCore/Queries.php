<?php

namespace TxCore;

use Barge\DB\DBClient;
use Barge\Log\Factory;

class Queries
{
	/**
	 * @var DBClient
	 */
	private $db;
	private $fromTimeStampFormat = array(
		'time' => "%H:%i",
		'date' => "%Y-%m-%d",
		'datetime' => "%Y-%m-%d %H:%i:%s",
	);

	public function __construct($db)
	{
		$this->db =$db;
	}

	public function selectUsersCount()
	{
		$query = 'select count(*) as count from user';
		$list = $this->db->query($query,
			array());
		return $list[0]->count;
	}


	public function selectNewObjectId()
	{
		$query = 'select get_new_object_id() as id';
		$list = $this->db->query($query,
			array());
		return $list[0]->id;
	}

	public function InsertRevisionNo(Table $table)
	{
		$query = 'insert into revision_no (table_id, revision_no) values (?, 0)';
		$params = array( $table->getId() );
		return $this->db->update($query, $params);
	}

	public function selectNextRevisionNo($tableId)
	{
		$query = 'select get_new_revision_no(?) as revno';
		$list = $this->db->query($query, array($tableId));
		return $list[0]->revno;
	}

	public function InsertRownum(Table $table)
	{
		$query = 'insert into row_no (table_id, row_no) values (?, 0)';
		$params = array( $table->getId() );
		return $this->db->update($query, $params);
	}

	public function selectNextRownum($tableId)
	{
		$query = 'select get_new_row_no(?) as rownum';
		$list = $this->db->query($query, array($tableId));
		return $list[0]->rownum;
	}


	public function selectUser($id, $byLoginId)
	{
		$query ="";
		if ($byLoginId) {
			$query = 'select * from user where user.login_id = ?';
		} else {
			$query = 'select * from user where user.id = ?';
		}
		$params = array(
			$id
		);
		return $this->db->query($query, $params);
	}


	public function selectFilter($id)
	{
		$query = "select * from filter where id = ?";
		$params = array(
			$id
		);
		$filters = $this->db->query($query, $params);
		return is_empty($filters) ? null : $filters[0];
	}

	public function selectFilters($table)
	{
		$query = "select * from filter where table_id = ? order by id";
		$params = array(
			$table->getId()
		);

		return $this->db->query($query, $params);
	}

	public function updateUser(User $user)
	{
		$query = 'insert into user
			(id, login_id, password, role, screen_name, profile, seq, created_on, updated_on) values
			(null, ?, ?, ?, ?, ?, ?, current_timestamp, current_timestamp)';
		$params = array(
			trim_to_null($user->getLoginId()),
			trim_to_null($user->getPassword()),
			trim_to_null($user->getRole()),
			trim_to_null($user->getScreenName()),
			trim_to_null($user->getProfile()),
			trim_to_null($user->getSeq()),
		);
		return $this->db->update($query, $params);
	}

	public function deleteUser(User $user)
	{
		$query = 'delete from user where user.id = ?';
		$params = array(
			$user->getId(),
		);
		return $this->db->update($query, $params);
	}


	public function selectGroup($id)
	{
		$query = 'select * from `group` where `group`.id = ?';
		$params = array(
			$id
		);
		return $this->db->query($query, $params);
	}

	public function selectComment($id)
	{
		$query = 'select * from `comment` where `comment`.id = ?';
		$params = array(
			$id
		);
		return $this->db->query($query, $params);
	}

	public function insertGroup(Group $group)
	{
		$query = 'insert into `group`
			(id, name, created_on, updated_on) values
			(null, ?, current_timestamp, current_timestamp)';
		$params = array(
			trim_to_null($group->getName())
		);

		$this->db->update($query, $params);
		return $this->db->lastInsertId('id');
	}

	public function updateGroup(Group $group)
	{
		$query = 'update `group`
			set name = ?,
			updated_on = current_timestamp where `group`.id = ?';
		$params = array(
			trim_to_null($group->getName()),
			trim_to_null($group->getId())
		);
		return $this->db->update($query, $params);
	}

	public function deleteGroup(Group $group)
	{
		$query = 'delete from `group` where `group`.id = ?';
		$params = array(
			$group->getId(),
		);
		return $this->db->update($query, $params);
	}

	public function deleteComment(Comment $comment)
	{
		$query = 'delete from `comment` where `comment`.id = ?';
		$params = array(
			$comment->getId(),
		);
		return $this->db->update($query, $params);
	}

	public function selectTablesCount()
	{
		$query = 'select count(*) as count from `release_control` r inner join `table_def` t '
				.' on r.table_id = t.id and r.release_revision = t.revision and t.is_template <> 1';
		$list = $this->db->query($query,
				array());
		return $list[0]->count;
	}

	public function selectTemplateTableDefs()
	{
		$query = 'select * from table_def
	where is_template = 1
	order by name';
		$params = array(
		);
		return $this->db->query($query, $params);
	}

	public function selectTemplateTableDef($id)
	{
		$query = 'select * from table_def
			where id = ? and is_template = 1';
		$params = array(
			$id,
		);
		return $this->db->query($query, $params);
	}

	public function deleteColumnDef(Column $column)
	{
		$table = $column->getParent();
		$query = 'delete from `column_def` where id = ? and revision = ?';
		$params = array(
			$column->getId(),
			$table->getRevision(),
		);
		return $this->db->update($query, $params);
	}

	public function deleteColumnDefsByTable(Table $table, $allVers = false)
	{
		if ($allVers) {
			$query = 'delete from `column_def` where table_id = ?';
			$params = array(
				$table->getId(),
			);
			$this->db->update($query, $params);
		} else {
			$query = 'delete from `column_def` where table_id = ? and revision = ?';
			$params = array(
				$table->getId(),
				$table->getRevision(),
			);
			$this->db->update($query, $params);
		}
	}


	public function selectColumnDefByTable(Table $table, $callback)
	{
		$query = 'select * from column_def where table_id = ? and revision = ?
			order by id ';
		$params = array(
			$table->getId(),
			$table->getRevision()
		);
		$this->db->fetch($query, $params, $callback);
	}

	public function selectUserTableDefs(User $user, $callback)
	{
		if ($user->isAdmin()) {
			$query = "
			select * from table_def
				where exists (select 1 from release_control
				where table_def.id = release_control.table_id and table_def.revision = release_control.release_revision) and
				is_template = 0
				order by name
				";
			$params = array(
				$user->getId()
			);
			$this->db->fetch($query, $params, $callback);
		} else {
			$query = "
				select * from table_def
				where
				exists (select 1 from release_control
				where table_def.id = release_control.table_id and table_def.revision = release_control.release_revision) and
				(owner = ? or is_public = 1) and
				is_template = 0
				order by name
				";
			$params = array(
				$user->getId()
			);
			$this->db->fetch($query, $params, $callback);
		}
	}

	public function selectTableDefs($filter, $callback)
	{
		$where = "";
		$params = array();
		if (isset($filter['apiKey'])) {
			$where .= ' and api_key = ? ';
			$params[] = $filter['apiKey'];
		}
		$query = "
			select *
			from table_def
			where
			exists (select 1 from release_control
				where table_def.id = release_control.table_id and table_def.revision = release_control.release_revision)
			and is_template = 0
			$where
			order by name
			";
		$this->db->fetch($query, $params, $callback);
	}


	public function selectReleasedTableDef($id)
	{
		$query = "select * from table_def where id = ? and exists (select 1 from release_control
			where table_def.id = table_id and table_def.revision = release_control.release_revision)";
		$params = array(
			$id
		);
		return $this->db->query($query, $params);
	}

	public function selectTrunkTableDef($id)
	{
		$query = "select * from table_def where id = ? and exists (select 1 from release_control
			where table_def.id = table_id and table_def.revision = release_control.trunk_revision)";
		$params = array(
			$id
		);
		return $this->db->query($query, $params);
	}

	public function selectTableDef($id, $revision)
	{
		$query = "select * from table_def where id = ? and revision = ?";
		$params = array(
			$id,
			$revision
		);
		return $this->db->query($query, $params);
	}

	public function selectIcon($id)
	{
		$query = "select * from `icon` where id = ?";
		$params = array(
			$id
		);
		return $this->db->query($query, $params);
	}

	public function selectIcons()
	{
		$query = "select * from `icon` order by `seq`";
		$params = array(
		);
		return $this->db->query($query, $params);
	}

	public function deleteFilter(Filter $filter)
	{
		$query = 'delete from filter where id = ?';
		$params = array(
			$filter->getId()
		);
		$this->db->update($query, $params);
	}

	public function insertFilter(Filter $filter, User $user)
	{
		$query = 'insert into filter (id, table_id, name, expression, created_by) values
			(?, ?, ?, ?, ?)';
		$params = array(
			null,
			$filter->getParent()->getId(),
			$filter->getName(),
			Util::encodeJSON($filter->getCriteria()->toArray()),
			$user->getId()
		);
		$this->db->update($query, $params);
		return $this->db->lastInsertId('id');
	}

	public function updateFilter(Filter $filter, User $user)
	{
		$query = 'update filter set name=?, expression= ? where id = ?';
		$params = array(
			$filter->getName(),
			Util::encodeJSON($filter->getCriteria()->toArray()),
			$filter->getId()
		);
		$this->db->update($query, $params);
	}

	public function insertViewColumns(Table $table)
	{
		$query = array();
		$query[] = 'insert into `view_column`
				(table_id,revision, column_id,width,seq) values ';
		$params = array();

		$seq = 1;
		$firstDone = false;
		foreach ($table->getViewColumns() as $viewColumn) {
			if ($firstDone)
				$query[] = ',';
			$query[] = '(?, ?, ?, ?, ?)';
			$params = array_merge($params, array(
				$table->getId(),
				$table->getRevision(),
                $viewColumn->getColumn()->getId(),
                $viewColumn->getWidth(),
				$seq++
			));
			$firstDone = true;
		}
		if (!$firstDone)
			return;
		$this->db->update(join(' ', $query), $params);
	}

	public function insertPermissions(Table $table)
	{
		$query = array();
		$query[] = 'insert into `permission`
				(table_id, revision, user_id, is_readable, is_writable) values ';
		$params = array();
		
		$firstDone = false;
		foreach ($table->getPermissions() as $i => $permission) {
			if ($firstDone)
				$query[] = ',';
			$query[] = '(?, ?, ?, ?, ?)';
			$params = array_merge($params, array(
				$table->getId(),
				$table->getRevision(),
				$permission->getUserId(),
				$permission->getIsReadable(),
				$permission->getIsWritable()
			));
			$firstDone = true;
		}
		if (!$firstDone)
			return;
		$this->db->update(join(' ', $query), $params);
	}
	
	public function insertRow(Table $table, $rownum = null)
	{
		$query = "insert into `row` (id, table_id, row_no, version) values (null, ?, ?, ?)";
		$params = array(
			$table->getId(),
			$rownum,
			1
		);
		$this->db->update($query, $params);
		return $this->db->lastInsertId('id');
	}

	public function insertColumnDef(Column $column, $replace = false)
	{
		$table = $column->getParent();
		$query = ($replace ? 'replace':'insert').
			' into `column_def` (id, revision, table_id,column_type,code,label,properties)
			values (?,?,?,?,?,?,?) ';
		$params = array(
			$column->getId(),
			$table->getRevision(),
			$table->getId(),
			$column->getType(),
			$column->getCode(),
			$column->getLabel(),
			Util::encodeJSON($column->getRawProperties() ? $column->getRawProperties() : array()),
		);
		$this->db->update($query, $params);
	}

    /**
     * @param Table $table
     * @param $newRevision
     */
	public function updateTableRevision($table, $newRevision)
	{
		$query = ' update table_def set revision = ? where id = ? and revision = ?';
		$params = array(
			$newRevision,
			$table->getId(),
			$table->getRevision(),
		);
		$this->db->update($query, $params);
	}

    /**
     * @param Table $table
     * @param $newRevision
     */
	public function updateColumnRevision($table, $newRevision)
	{
		$query = ' update column_def set revision = ? where table_id = ? and revision = ?';
		$params = array(
			$newRevision,
			$table->getId(),
			$table->getRevision(),
		);
		$this->db->update($query, $params);
	}

    /**
     * @param Table $table
     * @param $newRevision
     */
	public function updateOptionRevision($table, $newRevision)
	{
		$query = ' update `option` set revision = ? where table_id = ? and revision = ?';
		$params = array(
			$newRevision,
			$table->getId(),
			$table->getRevision(),
		);
		$this->db->update($query, $params);
	}

    /**
     * @param Table $table
     * @param $newRevision
     */
	public function updateViewColumnRevision($table, $newRevision)
	{
		$query = ' update `view_column` set revision = ? where table_id = ? and revision = ?';
		$params = array(
			$newRevision,
			$table->getId(),
			$table->getRevision(),
		);
		$this->db->update($query, $params);
	}

    /**
     * @param Table $table
     * @param $newRevision
     */
	public function updatePermissionRevision($table, $newRevision)
	{
		$query = ' update `permission` set revision = ? where table_id = ? and revision = ?';
		$params = array(
			$newRevision,
			$table->getId(),
			$table->getRevision(),
		);
		$this->db->update($query, $params);
	}

    /**
     * @param Table $table
     * @param $newRevision
     */
	public function updateViewCalendarSettingRevision($table, $newRevision)
	{
		$query = ' update `view_calendar_setting` set revision = ? where table_id = ? and revision = ?';
		$params = array(
			$newRevision,
			$table->getId(),
			$table->getRevision(),
		);
		$this->db->update($query, $params);
	}

    /**
     * @param Table $table
     * @param $newRevision
     */
    public function updateReadCheckSettingsRevision($table, $newRevision)
    {
        $query = ' update `read_check_setting` set revision = ? where table_id = ? and revision = ?';
        $params = array(
            $newRevision,
            $table->getId(),
            $table->getRevision(),
        );
        $this->db->update($query, $params);
    }

    /**
     * @param Table $table
     * @param $newRevision
     */
	public function updateReferenceRevision($table, $newRevision)
	{
		$query = ' update `reference` set revision = ? where table_id = ? and revision = ?';
		$params = array(
			$newRevision,
			$table->getId(),
			$table->getRevision(),
		);
		$this->db->update($query, $params);
	}

	public function updateColumnDef(Column $column)
	{
		$table = $column->getParent();
		$query = 'update `column_def` set code = :code, label = :label,properties = :properties
			where id = :id and revision = :revision';
		$params = array(
			'code' => $column->getCode(),
			'label' => $column->getLabel(),
			'properties' => Util::encodeJSON($column->getRawProperties() ? $column->getRawProperties() : array()),
			'id' => $column->getId(),
			'revision' => $table->getRevision(),
		);
		$this->db->update($query, $params);
	}

	public function insertOptions(Column $column, $replace = false)
	{
		$table = $column->getParent();
		$query = array();
		$params = array();

		$seqBase = $this->db->query('
			select ifnull(max(seq) +1, 1) as `seq` from `option` where column_id = ? and revision = ?',
			array(
				$column->getId(),
				$table->getRevision()
			),
			true
		);

		$firstDone = false;
		$seq = intval($seqBase);
		$query[] = $replace ? 'replace' : 'insert';
		$query[] = 'into `option` (id,revision, table_id,column_id,name,seq) values';
		foreach ($column->getOptions() as $option) {
			if ($firstDone) $query[] = ',';
			$query[] = '(?,?,?,?,?,?)';
			$params = array_merge($params, array(
				$option->getId(),
				$table->getRevision(),
				$table->getId(),
				$column->getId(),
				$option->getName(),
				$option->getSeq() ?: $seq,
			));
			$firstDone = true;
			$seq++;
		}
		if (!$firstDone)
			return;
		return $this->db->update(join(' ', $query), $params);
	}

	public function updateOption(Option $option)
	{
		$query = 'update option set name = ? where id = ? and revision = ?';
		$params = array(
			$option->getName(),
			$option->getId(),
			$option->getParent()->getParent()->getRevision(),
		);
		return $this->db->update($query, $params);
	}

	public function deleteOption(Option $option)
	{
		$query = 'delete from `option` where id = ? and revision = ?';
		$params = array(
			$option->getId(),
			$option->getParent()->getParent()->getRevision(),
		);
		return $this->db->update($query, $params);
	}

	public function insertReferences(Column $column, $newRevision = null)
	{
		$table = $column->getParent();
		$query = array();
		$params = array();

		$seqBase = $this->db->query('
			select ifnull(max(no) +1, 1) as `no` from `reference` where column_id = ? and revision = ?',
				array(
						$column->getId(),
						$table->getRevision()
				),
				true
		);

		$firstDone = false;
		$seq = intval($seqBase);
		$query[] = 'insert';
		$query[] = 'into `reference` (column_id, revision, no, table_id, target_column_id, target_table_id) values';
		foreach ($column->getReferences() as $reference) {
			if ($firstDone) $query[] = ',';
			$query[] = '(?,?,?,?,?,?)';
			$params = array_merge($params, array(
					$column->getId(),
					$newRevision ? $newRevision : $table->getRevision(),
					$seq,
					$table->getId(),
					$reference->getTargetColumnId(),
					$column->getReferenceTable()->getId()
			));
			$firstDone = true;
			$seq++;
		}
		if (!$firstDone)
			return;
		return $this->db->update(join(' ', $query), $params);
	}

	public function deleteReferencesByTable(Table $table, $allVers = false)
	{
		if ($allVers) {
			$query = 'delete from `reference` where table_id = ?';
			$params = array(
					$table->getId(),
			);
			return $this->db->update($query, $params);
		} else {
			$query = 'delete from `reference` where table_id = ? and revision = ?';
			$params = array(
					$table->getId(),
					$table->getRevision(),
			);
			return $this->db->update($query, $params);
		}
	}

	public function deleteReferencesByTableOtherRevision(Table $table)
	{
		$query = 'delete from `reference` where table_id = ? and revision <> ?';
		$params = array(
				$table->getId(),
				$table->getRevision(),
		);
		return $this->db->update($query, $params);
	}

    /**
     * @param Table $table
     * @param int $newRevision
     */
	public function updateTableDef(Table $table, $newRevision = null)
	{
		$query = "update table_def set
			name= :name, description= :description, icon_filekey= :icon_filekey,
			is_public= :is_public, is_api_enabled= :is_api_enabled, api_key= :api_key,
			is_write_api_enabled= :is_write_api_enabled, write_api_key= :write_api_key,
			round_type= :round_type, round_scale= :round_scale, permission_control= :permission_control,
			is_comment_enabled = :is_comment_enabled, is_calendar_enabled = :is_calendar_enabled, is_comment_following_enabled = :is_comment_following_enabled,
			is_read_check_enabled = :is_read_check_enabled,
			is_six_columns_enabled = :is_six_columns_enabled
			where id= :id and revision = :revision";

		$params = array(
			'id' => $table->getId(),
			'name' => $table->getName(),
			'description' => $table->getDescription(),
			'icon_filekey' => $table->getIcon() ? $table->getIcon()->getFilekey() : null,
			'is_public' => Util::toBoolInt($table->isPublic(), 0),
			'is_api_enabled' => Util::toBoolInt($table->isApiEnabled(), 0),
			'api_key' => $table->getApiKey(),
			'is_write_api_enabled' => Util::toBoolInt($table->isWriteApiEnabled(), 0),
			'write_api_key' => $table->getWriteApiKey(),
			'round_type' => $table->getRoundType(),
			'round_scale' => $table->getRoundScale(),
			'revision' => $newRevision ? $newRevision : $table->getRevision(),
			'permission_control' => Util::toBoolInt($table->getPermissionControl(), 0),
			'is_comment_enabled' => Util::toBoolInt($table->isCommentEnabled()),
			'is_calendar_enabled' => Util::toBoolInt($table->isCalendarEnabled()),
			'is_comment_following_enabled' => Util::toBoolInt($table->isCommentFollowingEnabled()),
            'is_read_check_enabled' => Util::toBoolInt($table->isReadCheckEnabled()),
			'is_six_columns_enabled' => Util::toBoolInt($table->isSixColumnsEnabled()),

		);

		$this->db->update($query, $params);
	}

	public function updateTableDefToTemplate(Table $table, $default = false)
	{
		$query = "update table_def set
			is_template= 1, is_default= :is_default
			where id= :id and revision = :revision";

		$params = array(
			'id' => $table->getId(),
			'is_default' => $default ? 1 : 0,
			'revision' => $table->getRevision()
		);

		$this->db->update($query, $params);
	}

	public function deleteOptionsByColumn(Column $column)
	{
		$query = 'delete from `option` where column_id = ? and revision = ?';
		$params = array(
			$column->getId(),
			$column->getParent()->getRevision(),
		);
		return $this->db->update($query, $params);
	}

	public function deleteOptionsByTable(Table $table, $allVers = false)
	{
		if ($allVers) {
			$query = 'delete from `option` where table_id = ?';
			$params = array(
				$table->getId(),
			);
			return $this->db->update($query, $params);
		} else {
			$query = 'delete from `option` where table_id = ? and revision = ?';
			$params = array(
				$table->getId(),
				$table->getRevision(),
			);
			return $this->db->update($query, $params);
		}
	}

	public function insertData(Table $table, Row $row, User $by, $replace = false)
	{
		$q = array();
		$params = array();
		$q[] = $replace ? 'replace' : 'insert';
		$q[] = "into `data` (row_id, column_id, table_id, value) values";
		$firstDone = false;
		foreach ($table->getColumns() as $i => $column) {/*@var $column Column*/
			if (!$column->hasValue())
				continue;
			$value = $row->_createStoreValue($column);
			if (is_object($value)) {
				throw new TxCoreException(sprintf('Invalid value of %s.', $column->getType()),
					TxCoreException::ROW_NOT_FOUND);
			}
			if ($firstDone)
				$q[] = ",";
			$q[] = sprintf("/*%s=*/( ?, ?, ?, ? )", $column->getType());
			$params[] = $row->getId();
			$params[] = $column->getId();
			$params[] = $table->getId();
			$params[] = $value;
			$firstDone = true;
		}
		// 		$q[] = 'ON DUPLICATE KEY SET `json` = $json'
		$this->db->update(join(' ', $q), $params);
	}

	public function selectReleaseControl(Table $table)
	{
		$query = 'select * from release_control
			where table_id = ?';
		$params = array(
			$table->getId()
		);
		return $this->db->query($query, $params);
	}

	public function insertIndexes(Table $table, Row $row, $replace = false)
	{
		$now = Engine::factory()->getNow();

		if ($replace) {
			$this->db->update('delete from `indexes` where table_id = ? and row_id = ?',
				array($table->getId(), $row->getId()));
		}

		static $allIndexTypes =
			array(Index::STRING, Index::INTEGER, Index::NUMBER, Index::DATE, Index::TIME, Index::DATETIME);

		$q = array();
		$params = array();
		$q[] = "insert into `indexes` (id, table_id, column_id, string_value, integer_value, number_value, date_value, time_value, datetime_value, row_id)";
		$q[] = "values";
		$firstDone = false;
		foreach ($table->getColumns() as $column) { /* @var $column \TxCore\Column */
			if (!$column->hasValue())
				continue;
			$indexes = $row->_createIndexKey($column);	// checkboxなどは複数のキーを持つ
			foreach (is_array($indexes) ? $indexes : array($indexes) as $index) {
				if ($firstDone)
					$q[] = ",";
				$q[] = "(null, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
				$params[] = $table->getId();
				$params[] = $column->getId();
				$foundIndex = false;
				foreach ($allIndexTypes as $_indexType) {
					if ($index->getType() == $_indexType) {
						if (is_object($index->getValue())) {
							throw new TxCoreException(sprintf('Invalid index value of %s.', $column->getType()),
								TxCoreException::INTERNAL);
						}
						if ($_indexType == Index::DATE && strtolower($index->getValue()) == 'now') {
							$params[] = date('Y-m-d', $now);
						} else if ($_indexType == Index::DATETIME && strtolower($index->getValue()) == 'now') {
							$params[] = date('Y-m-d H:i', $now);
						} else {
							$params[] = $index->getValue();
						}

						$foundIndex = true;
					} else {
						$params[] = null;  // Do not use.
					}
				}
				if (!$foundIndex) {
					throw new TxCoreException(sprintf('Invalid index type : %s.', $index->getType()),
						TxCoreException::INTERNAL);
				}
				$params[] = $row->getId();
				$firstDone = true;
			}
		}

		$this->db->update(join(' ', $q), $params);
	}

// 	public function updateTable(Table $table, $asWork = false)
// 	{
// 		$query = "update table_def set
// 			name= :name, description= :description, icon_filekey= :icon_filekey,
// 			is_public= :is_public, is_api_enabled= :is_api_enabled,
// 			api_key= :api_key, round_type= :round_type, round_scale= :round_scale
// 			where id= :id and revision: :revision";

// 		$params = array(
// 			'id' => $table->getId(),
// 			'name' => $table->getName(),
// 			'description' => $table->getDescription(),
// 			'icon_filekey' => $table->getIcon() ? $table->getIcon()->getFilekey() : null,
// 			//'def_type' => $asWork ? Table::WORK : Table::PRODUCTION,
// 			'is_public' => Util::toBoolInt($table->isPublic(), 0),
// // 			'is_saved' => $asWork ? Util::toBoolInt($table->isSaved(), 0) : null,
// 			//'is_default' => null,
// 			//'work_table_id' => $asWork ? null : $tableWork->getId(),
// 			'is_api_enabled' => Util::toBoolInt($table->isApiEnabled(), 0),
// 			'api_key' => $table->getApiKey(),
// 			//'owner' => $by->getId(),
// 			'round_type' => $table->getRoundType(),
// 			'round_scale' => $table->getRoundScale(),
// 			'revision' => $table->getRevision()
// 			);

// 		$this->db->update($query, $params);
// // 		return $this->db->lastInsertId('id');
// 	}

// 	public function buildUpdataTableQuery(Table $table, $by)
// 	{
// 		$query = "update table_def set
// 			name = :name,
// 			description = :description,
// 			icon_filekey = :icon_filekey,
// 			is_public = :is_public,
// 			is_saved = :is_saved,
// 			is_api_enabled = :is_api_enabled,
// 			api_key = :api_key,
// 			round_type = :round_type,
// 			round_scale = :round_scale,
// 			version = version + 1
// 			where id = :id";

// 		$params = array(
// 			'name' => $table->getName(),
// 			'description' => $table->getDescription(),
// 			'icon_filekey' => $table->getIcon() ? $table->getIcon()->getFilekey() : null,
// 			'is_public' => Util::toBoolInt($table->isPublic(), 0),
// 			'is_saved' => $asWork ? null : Util::toBoolInt(false),
// 			'is_api_enabled' => Util::toBoolInt(false),
// 			'api_key' => $table->getApiKey(),
// 			'round_type' => $table->getRoundType(),
// 			'round_scale' => $table->getRoundScale(),
// 			'id' => $table->getId(),
// 		);
	//
// 		return array($query, $params);
// 	}


// 	/**
// 	 *
// 	 * @param Table $table 親テーブル
// 	 * @param Column $column 親カラム
// 	 * @param Column $columnWork
// 	 * @param array $columnMap プロダクションIDとワークIDの対応
// 	 */
// 	public function buildInsertColumnQuery(Table $table, Column $column)
// 	{
// 		$query = 'insert into `column_def` (id,table_id,column_type,code,label,shows_field,
// 			shows_label,position_y,position_x,size_height,size_width,is_unique,is_required,min_length,max_length,
// 			min_value,max_value,default_value,expression,default_expression,format,shows_digit,
// 			label_html,thumbnail_max_size,link_type,reference_table_id,reference_target_column_id,
// 			reference_source_column_id,reference_columns,reference_sort_column_id,reference_sorts_reverse,
// 			reference_max_row,reference_copy_map,options_alignment, properties)
// 			values (:id,:table_id,:column_type, :code,:label,:shows_field,:shows_label,
// 			:position_y,:position_x,:size_height,:size_width,:is_unique,:is_required,:min_length,:max_length,:min_value,
// 			:max_value,:default_value,:expression,:default_expression,:format,:shows_digit,:label_html,
// 			:thumbnail_max_size,:link_type,:reference_table_id,:reference_target_column_id,:reference_source_column_id,
// 			:reference_columns,:reference_sort_column_id,:reference_sorts_reverse,:reference_max_row,:reference_copy_map,:options_alignment, :properties) ';

// 		// Replace referal ids from production to work.
// // 		$reference = $column->getReference();
// // 		$columnIds = null;
// // 		if ($reference !== null && $reference->getColumns() !== null) {
// // 			$columnIds = array_map(function($t) { return $t->getId(); }, $reference->getColumns());
// // 		}
// // 		$sourceColumnId = null;
// // 		if ($reference !== null && $reference->getSourceColumn() !== null) {
// // 			$sourceColumnId = $reference->getSourceColumn()->getId();
// // 		}
// // 		$copyMap = null;
// // 		if ($reference !== null && $reference->_getCopyMapIds() !== null) {
// // 			$copyMap = Util::encodeJSON($reference->_getCopyMapIds());
// // 		}
// // 		$defaultValue = $column->getDefaultValue();
// // 		if ($defaultValue !== null) {
// // 			if (is_array($defaultValue)) {
// // 				$defaultValue = Util::encodeJSON($defaultValue);
// // 			}
// // 		}
// 		$params = array(
// 			'id' => $column->getId(),
// 			'table_id' => $table->getId(),
// 			'column_type' => $column->getType(),
// // 			'work_column_id' => $columnWork ? $columnWork->getId() : null,
// // 			'is_deleted' => Util::toBoolInt(false),
// 			'code' => $column->getCode(),
// 			'label' => $column->getLabel(),
// 			'shows_field' => null,
// 			'shows_label' => null,
// 			'position_y' => null,
// 			'position_x' => null,
// 			'size_height' => null,
// 			'size_width' => null,
// // 			'is_unique' => Util::toBoolInt($column->isUnique()),
// 			'is_unique' => null,
// 			'is_required' => null,
// 			'min_length' => null,
// 			'max_length' => null,
// 			'min_value' => null,
// 			'max_value' => null,
// 			//ラジオボタン	チェックボックス	ドロップダウン
// 			'default_value' => null,
// 			'expression' => null,
// 			'default_expression' => null,
// 			'format' => null,
// // 			'shows_digit' => Util::toBoolInt($column->getShowsDigit()),
// 			'shows_digit' =>null,
// 			'label_html' => null,
// 			'thumbnail_max_size' => NULL,
// 			'link_type' => NULL,
// // 			'reference_table_id' => $reference && $reference->getTable() ? $reference->getTable()->getId() : null,
// // 			'reference_target_column_id' => $reference && $reference->getTargetColumn() ? $reference->getTargetColumn()->getId() : null,
// // 			'reference_source_column_id' => $sourceColumnId,
// // 			'reference_columns' => Util::encodeJSON($columnIds),
// // 			'reference_sort_column_id' => $reference && $reference->getSortColumn() ? $reference->getSortColumn()->getId() : null,
// // 			'reference_sorts_reverse' => $reference ? Util::toBoolInt($reference->getSortsReverse()) : null,
// // 			'reference_max_row' => $reference ? $reference->getMaxRow() : null,
// // 			'reference_copy_map' => $copyMap,
// 			'reference_table_id' => null,
// 			'reference_target_column_id' => null,
// 			'reference_source_column_id' => null,
// 			'reference_columns' => null,
// 			'reference_sort_column_id' => null,
// 			'reference_sorts_reverse' => null,
// 			'reference_max_row' => null,
// 			'reference_copy_map' => null,
// 			'options_alignment' => null,
// 			'properties' => Util::encodeJSON($column->getRawProperties() ? $column->getRawProperties() : array()),
// 		);

// // 		$this->db->update($query, $params);
// // 		return $this->db->lastInsertId('id');
// 		return array($query, $params);
// 	}



	public function deleteRow(Row $row)
	{
		$query = 'delete from `row` where `id` = ?';
		$params = array(
			$row->getId()
		);
		return $this->db->update($query, $params);
	}

	public function insertReleaseControl(Table $trunk, Table $release, $hasPenning = false)
	{
		$query = 'insert into `release_control` (table_id, trunk_revision,
			release_revision, has_pending_release)
			values (?, ?, ?, ?)';
		$params = array(
			$trunk->getId(),
			$trunk->getRevision(),
			$release->getRevision(),
			$hasPenning ? 1 : 0
		);
		return $this->db->update($query, $params);
	}

	/**
	 * @param Row $row
	 */
	public function updateRowVersion(Row $row)
	{
		$query = "update `row` set version = version + 1 where id = ? and version = ?";
		$params = array(
			$row->getId(),
			$row->getVersion()
		);
		return $this->db->update($query, $params);
	}

	public function insertTableDef(Table $table, $by)
	{
		$query = "insert into table_def
			(id, revision, name, description, icon_filekey, is_public, is_default,is_api_enabled, api_key,
			is_write_api_enabled, write_api_key, owner, round_type, round_scale, is_template, permission_control,
			is_comment_enabled, is_calendar_enabled, is_comment_following_enabled, is_read_check_enabled, is_six_columns_enabled) values
			(:id, :revision, :name, :description, :icon_filekey, :is_public, :is_default, :is_api_enabled, :api_key,
			:is_write_api_enabled, :write_api_key, :owner, :round_type, :round_scale, :is_template, :permission_control,
			:is_comment_enabled, :is_calendar_enabled, :is_comment_following_enabled, :is_read_check_enabled, :is_six_columns_enabled)";
		$params = array(
			'id' => $table->getId(),
			'revision' => $table->getRevision(),
			'name' => $table->getName(),
			'description' => $table->getDescription(),
			'icon_filekey' => $table->getIcon() ? $table->getIcon()->getFilekey() : null,
			'is_public' => Util::toBoolInt($table->isPublic(), 0),
			'is_default' => null,
			'is_api_enabled' => Util::toBoolInt(false),
			'api_key' => $table->getApiKey(),
			'is_write_api_enabled' => Util::toBoolInt(false),
			'write_api_key' => $table->getWriteApiKey(),
			'owner' => $by->getId(),
			'round_type' => $table->getRoundType() ?: 'default',
			'round_scale' => $table->getRoundScale() ?: 4,
			'is_template' => 0,
			'permission_control' => Util::toBoolInt($table->getPermissionControl(), 0),
			'is_comment_enabled' => Util::toBoolInt($table->isCommentEnabled()),
			'is_calendar_enabled' => Util::toBoolInt($table->isCalendarEnabled()),
			'is_comment_following_enabled' => Util::toBoolInt($table->isCommentFollowingEnabled()),
            'is_read_check_enabled' => Util::toBoolInt($table->isReadCheckEnabled()),
			'is_six_columns_enabled' => Util::toBoolInt($table->isSixColumnsEnabled()),
		);

		$this->db->update($query, $params);
	}

	public function deleteIndexesByRow(Row $row)
	{
		$query = 'delete from `indexes` where `row_id` = ?';
		$params = array(
			$row->getId()
		);
		$this->db->update($query, $params);
	}

	public function deleteDataByRow(Row $row)
	{
		$query = 'delete from `data` where `row_id` = ?';
		$params = array(
			$row->getId()
		);
		$this->db->update($query, $params);
	}

	public function selectQueryCount(Table $table, Criteria $criteria, $base = null)
	{
		list ($query, $params ) = $this->buildSearchQuery($table, $criteria, true, null, null, $base);
		return intval($this->db->query($query, $params, true));
	}

	public function selectSubtotal(Table $table, Criteria $criteria, $callable)
	{
		list ($query, $params) = $this->buildSubtotalQuery($table, $criteria);
		$this->db->fetch($query, $params, $callable);
	}

	private function buildSubtotalQuery(Table $table, Criteria $criteria)
	{
		$q1 = array();	// select querey
		$p1 = array();
		$subtotal = $criteria->getSubtotal();	/* @var $subtotal Subtotal */

		// 条件で使うインデックスを収集
		$columnIds = array();
		foreach ($criteria->getConditions() as $term)
			$columnIds[] = $term->getColumn()->getId();
		if ($subtotal->getFunction() == Criteria::FN_SUM)
			$columnIds[] = $subtotal->getAggregateColumn()->getId();
		$columnIds = array_unique($columnIds);

		$q1[] = 'select `option`.id as option_id, ifnull(`value`, 0) as value';
		$q1[] = 'from `option` ';
		$q1[] = 'left outer join (';
		$q1[] = 'select indexes_option.integer_value as id ';
		if ($subtotal->getFunction() == Criteria::FN_COUNT) {
			$q1[] = ',count(1) as `value`';
		} else if ($subtotal->getFunction() == Criteria::FN_SUM) {
			$q1[] = ',sum(ifnull(`aggval_`, 0)) as `value`';
		}
		$q1[] = 'from (';
		$q1[] = "\n";
		$q1[] = 'select distinct `row`.id';
		if ($subtotal->getFunction() == Criteria::FN_SUM) {
			$q1[] =  sprintf(', indexes_%d.%s_value as `aggval_`',
				$subtotal->getAggregateColumn()->getId(),
				$subtotal->getAggregateColumn()->getIndexType());
		}
// 		// Sort keys
// 		foreach ($sort as $term) {	/* @var $sort Sort*/
// 			$q1[] = sprintf(', indexes_%1$d.%2$s_value as sortkey_%1$d',
// 				$term->getColumn()->getId(),
// 				$term->getColumn()->getIndexType());
// 		}
		$q1[] = 'from `row`';
		foreach ($columnIds as $columnId) {
			$q1[] = sprintf('join indexes as indexes_%1$d '."\n", $columnId);
			$q1[] = sprintf('   on (indexes_%1$d.column_id = %1$d and indexes_%1$d.row_id = row.id)'."\n", $columnId);
		}

		if (is_not_empty($criteria->getConditions())) {
			$q1[] = 'where'."\n";
			$firstDone = false;
			foreach ($criteria->getConditions() as $condition) {	/* @var $cond \TxCore\Condition */
				if ($firstDone)
					$q1[] = $criteria->getBoolOperator();
				$indexName = sprintf('indexes_%d.%s_value /*%s*/',
					$condition->getColumn()->getId(), $condition->getColumn()->getIndexType(), $condition->getColumn()->getType());
				list ($subQuery, $subParams) = $this->buildWhereTermQuery($condition, $indexName);
				$q1[] = $subQuery;
				$p1 = array_merge($p1, $subParams);
				$firstDone = true;
			}
			////
		}

		$q1[] = ') as row_';
		$q1[] = "\n";
		$q1[] = "join `indexes` as indexes_option on (row_.id = indexes_option.row_id and indexes_option.column_id = ?)";
		$q1[] = "group by indexes_option.integer_value";
		$q1[] = ") as `option_selected`  on(`option`.id = `option_selected`.id)";
		$q1[] = "where `option`.revision = ? and `option`.column_id = ?";
		$q1[] = "order by `option`.seq";
		//$q1[] = "order by `value` desc, `option`.seq";

		$p1[] = $subtotal->getGroupedColumn()->getId();
		$p1[] = $table->getRevision();
		$p1[] = $subtotal->getGroupedColumn()->getId();

		return array(join(' ', $q1), $p1);
	}


	public function buildSearchQuery(Table $table, Criteria $criteria, $countQuery = false, $offset = null, $limit = null, $base = null)
	{
		$q1 = array();	// select querey
		$p1 = array();
		$q2 = array();	// count query
		$p2 = array();
		$sort = $criteria->getSort();


		// ソートキーがない場合は、レコード番号をソートキーに追加する
		//（レコード番号は必ずある）
		if (is_empty($sort)) {
			$rownum = null;
			array_each($table->getColumns(), function($column) use (&$rownum){
				if ($column->getType() === Column::ROWNUM)
					$rownum = $column;
			});
			if ($rownum === null) {
				throw new TxCoreException("table has no rownum column.",
					TxCoreException::INTERNAL);
			}
			$sort[] = new Sort($rownum);
		}
		// 条件とソートで使うインデックスを収集
		$columnIds = array();
		foreach ($criteria->getConditions() as $term)
			$columnIds[] = $term->getColumn()->getId();
		foreach ($sort as $term)
			$columnIds[] = $term->getColumn()->getId();
		$columnIds = array_unique($columnIds);

		// オプションを持つソート条件
		$optionSortMap = array();
		foreach ($sort as $term)
			$optionSortMap[$term->getColumn()->getId()] = $term->getColumn()->hasOptions();

		$q1[] = 'select `data`.*, _row.version, _row.row_no';
		$q1[] = 'from `data` join (';

		// Begin subqueries for row table
		$q1[] = "\n";
		$q1[] = 'select distinct `row`.*';
		$q2[] = 'select count(distinct `row`.id) as count';

		// Sort keys
		foreach ($sort as $term) {	/* @var $term Sort*/
			if ($optionSortMap[$term->getColumn()->getId()] !== true) {
				$q1[] = sprintf(', indexes_%1$d.%2$s_value as sortkey_%1$d',
					$term->getColumn()->getId(),
					$term->getColumn()->getIndexType());
			} else {
				$q1[] = sprintf(', option_%1$d.seq as sortkey_%1$d',
					$term->getColumn()->getId());
			}
		}
		if ($base !== null) {
			if ($base['name'] === 'relationship') {
				$q2[] = $q1[] = 'from `relationship`'."\n";
				$q2[] = $q1[] = 'join `row` on (';
				$q2[] = $q1[] = 'relationship.source_row_id = ?';
				$q2[] = $q1[] = 'and relationship.source_column_id = ?';
				$q2[] = $q1[] = 'and relationship.target_row_id = row.id';
				$q2[] = $q1[] = ') '."\n";
				$p2[] = $p1[] = $base['params']['sourceRow']->getId();
				$p2[] = $p1[] = $base['params']['sourceColumn']->getId();
			} else {
				throw new TxCoreException();
			}
		} else {
			$q2[] = $q1[] = 'from `row`'."\n";
		}
		foreach ($columnIds as $columnId) {
			$q2[] = $q1[] = sprintf('join indexes as indexes_%1$d '."\n", $columnId);
			$q2[] = $q1[] = sprintf('   on (indexes_%1$d.column_id = %1$d and indexes_%1$d.row_id = row.id)'."\n", $columnId);
		}
		foreach ($optionSortMap as $columnId => $hasOptions) {
			if ($hasOptions) {
				$q2[] = $q1[] = sprintf('left join `option` as option_%1$d '."\n", $columnId);
				$q2[] = $q1[] = sprintf('   on (option_%1$d.column_id = indexes_%1$d.column_id and option_%1$d.id = indexes_%1$d.integer_value and option_%1$d.revision = %2$d)'."\n", $columnId, $table->getRevision());
			}
		}

		if (is_not_empty($criteria->getConditions())) {
			$q2[] = $q1[] = 'where'."\n";
			$firstDone = false;
			foreach ($criteria->getConditions() as $condition) {	/* @var $cond \TxCore\Condition */
				if ($firstDone)
					$q2[] = $q1[] = $criteria->getBoolOperator();
				$indexName = sprintf('indexes_%d.%s_value /*%s*/',
					$condition->getColumn()->getId(), $condition->getColumn()->getIndexType(), $condition->getColumn()->getType());
				list ($subQuery, $subParams) = $this->buildWhereTermQuery($condition, $indexName);
				$q1[] = $q2[] = $subQuery;
				$p1 = array_merge($p1, $subParams);
				$p2 = array_merge($p2, $subParams);
				$firstDone = true;
			}
		}

		if (is_not_empty($sort)) {
			$q1[] = 'order by'."\n";
			foreach ($sort as $i => $term) {
				$q1[] = sprintf("sortkey_%d %s,",
					$term->getColumn()->getId(),
					$term->isReverse() ? 'desc' : '');
			}
			$q1[] = '`row`.id';
		}

		if ($limit === null && $offset !== null) {
			throw new TxCoreException();
		}
		if ($limit !== null && $offset !== null) {
			$q1[] = sprintf('limit %d,%d', $offset, $limit);
		} else if ($limit !== null) {
			$q1[] = sprintf('limit %d', $limit);
		}


		$q1[] = ') as _row on (_row.id = data.row_id)';
		$q1[] = "\n";
		$q1[] = 'order by ';
		if (is_not_empty($sort)) {
			foreach ($sort as $i => $sortTerm) {
				$q1[] = sprintf("_row.sortkey_%d %s,",
					$sortTerm->getColumn()->getId(), $sortTerm->isReverse() ? 'desc' : '');
			}
		}
		$q1[] = 'data.row_id,';
		$q1[] = 'data.column_id';
		return $countQuery ? array(join(' ', $q2), $p2) : array(join(' ', $q1), $p1);
	}

	private function expandDateCalcExpression($index, $format)
	{
		$type = Index::TIME;
		if ($format === "date") $type = Index::DATE;
		if ($format === "datetime") $type = Index::DATETIME;
		if ($type !== Index::TIME) {
			$dummyIndex = new Index($type, $index->getValue());
			$values = $this->expandDateExpression($dummyIndex);
		} else {
			$values = $index->getValue();
		}
		return $values;
	}

	private function buildDateCalcCond($cond, $index, $indexName, $format, $i = 0)
	{
		$dtformat = $cond->getColumn()->getDateFormat();
		$values = $this->expandDateCalcExpression($index, $dtformat);
		$query = sprintf($format, $indexName, $this->fromTimeStampFormat[$dtformat]);
		if (!is_array($values)) {
			$param = $values;
		} else {
			$param = $values[$i];
		}
		return array($query, $param);
	}

	private function buildDateCalcEqCond($query, $params, $cond, $index, $indexName, $eq = true)
	{
		$dtformat = $cond->getColumn()->getDateFormat();
		$values = $this->expandDateCalcExpression($index, $dtformat);
		if ($eq) {
			if (is_array($values)) {
				$query[] = sprintf('FROM_UNIXTIME(%s, "%s") between ? and ?', $indexName, $this->fromTimeStampFormat[$dtformat]);
				$params[] = $values[0];
				$params[] = $values[1];
			} else {
				$query[] = sprintf('FROM_UNIXTIME(%s, "%s") = ?', $indexName, $this->fromTimeStampFormat[$dtformat]);
				$params[] = $values;
			}
		} else {
			if (is_array($values)) {
				$query[] = sprintf('(FROM_UNIXTIME(%s, "%s") not between ? and ? or %s is null)', $indexName, $this->fromTimeStampFormat[$dtformat], $indexName);
				$params[] = $values[0];
				$params[] = $values[1];
			} else {
				$query[] = sprintf('(%s != ? or %s is null)', $indexName, $indexName);
				$query[] = sprintf('(FROM_UNIXTIME(%s, "%s") != ? or %s is null)', $indexName, $this->fromTimeStampFormat[$dtformat], $indexName);
				$params[] = $values;
			}
		}
		return array($query, $params);
	}

	/**
	 * @param Condition $cond_
	 * @param $indexName
	 * @return array
	 * @throws TxCoreException
	 */
	private function buildWhereTermQuery($cond_, $indexName)
	{
		// Rewriting handles.
		$cond = clone $cond_;
		$cond->getColumn()->handleSearchRewrite($cond);

		$query = array();
		$params = array();
		switch ($cond->getOperator()) {
			case Criteria::EQUALS:
				$index = $cond->getColumn()->handleSearchKey($cond->getValue());
				if (is_empty($index->getValue())) {
					$query[] = sprintf('%s is null', $indexName);
				} else if ($index->getType() === Index::NUMBER) {
					$colType = $cond->getColumn()->getType();
					if ($colType === Column::DATECALC) {
						list($query, $params) = $this->buildDateCalcEqCond($query, $params, $cond, $index, $indexName);
					} else {
						$query[] = sprintf('%s = cast(? as decimal(30,10))', $indexName);
						$params[] = $index->getValue();
					}
				} elseif ($index->getType() == Index::DATE ||
					$index->getType() == Index::DATETIME) {
					$values = $this->expandDateExpression($index);
					if (is_array($values)) {
						$query[] = sprintf('%s between ? and ?', $indexName);
						$params[] = $values[0];
						$params[] = $values[1];
					} else {
						$query[] = sprintf('%s = ?', $indexName);
						$params[] = $values;
					}
				} else {
					$query[] = sprintf('%s = ?', $indexName);
					$params[] = $index->getValue();
				}
				break;
			case Criteria::NOT_EQUALS:
				$index = $cond->getColumn()->handleSearchKey($cond->getValue());
				if (is_empty($index->getValue())) {
					$query[] = sprintf('%s is not null', $indexName);
				} else if ($index->getType() === Index::NUMBER) {
					$colType = $cond->getColumn()->getType();
					if ($colType === Column::DATECALC) {
						list($query, $params) = $this->buildDateCalcEqCond($query, $params, $cond, $index, $indexName, false);
					} else {
						$query[] = sprintf('(%s != cast(? as decimal(30,10)) or %s is null)', $indexName, $indexName);
						$params[] = $index->getValue();
					}
				} elseif ($index->getType() == Index::DATE ||
					$index->getType() == Index::DATETIME) {
					$values = $this->expandDateExpression($index);
					if (is_array($values)) {
						$query[] = sprintf('(%s not between ? and ? or %s is null)', $indexName, $indexName);
						$params[] = $values[0];
						$params[] = $values[1];
					} else {
						$query[] = sprintf('(%s != ? or %s is null)', $indexName, $indexName);
						$params[] = $values;
					}
				} else {
					$query[] = sprintf('(%s != ? or %s is null)', $indexName, $indexName);
					$params[] = $index->getValue();
				}
				break;
			case Criteria::GREATER_EQUALS:
				$index = $cond->getColumn()->handleSearchKey($cond->getValue());
				if ($index->getType() === Index::NUMBER) {
					$colType = $cond->getColumn()->getType();
					if ($colType === Column::DATECALC) {
						list($q, $p) = $this->buildDateCalcCond($cond, $index, $indexName, 'FROM_UNIXTIME(%s, "%s") >= ?');
						$query[] = $q;
						$params[] = $p;
					} else {
						$query[] = sprintf('%s %s cast(? as decimal(30,10))', $indexName, $cond->getOperator());
						$params[] = $index->getValue();
					}
				} elseif ($index->getType() == Index::DATE ||
					$index->getType() == Index::DATETIME) {
					$values = $this->expandDateExpression($index);
					if (is_array($values)) {
						$query[] = sprintf('%s >= ?', $indexName);
						$params[] = $values[0];
					} else {
						$query[] = sprintf('%s >= ?', $indexName);
						$params[] = $values;
					}
				} else {
					$query[] = sprintf('%s >= ?', $indexName);
					$params[] = $index->getValue();
				}
				break;
			case Criteria::GREATER:
				$index = $cond->getColumn()->handleSearchKey($cond->getValue());
				if ($index->getType() === Index::NUMBER) {
					$colType = $cond->getColumn()->getType();
					if ($colType === Column::DATECALC) {
						list($q, $p) = $this->buildDateCalcCond($cond, $index, $indexName, 'FROM_UNIXTIME(%s, "%s") > ?', 1);
						$query[] = $q;
						$params[] = $p;
					} else {
						$query[] = sprintf('%s %s cast(? as decimal(30,10))', $indexName, $cond->getOperator());
						$params[] = $index->getValue();
					}
				} elseif ($index->getType() == Index::DATE ||
					$index->getType() == Index::DATETIME) {
					$values = $this->expandDateExpression($index);
					if (is_array($values)) {
						$query[] = sprintf('%s > ?', $indexName);
						$params[] = $values[1];
					} else {
						$query[] = sprintf('%s > ?', $indexName);
						$params[] = $values;
					}
				} else {
					$query[] = sprintf('%s > ?', $indexName);
					$params[] = $index->getValue();
				}
				break;
			case Criteria::LESS_EQUALS:
				$index = $cond->getColumn()->handleSearchKey($cond->getValue());
				if ($index->getType() === Index::NUMBER) {
					$colType = $cond->getColumn()->getType();
					if ($colType === Column::DATECALC) {
						list($q, $p) = $this->buildDateCalcCond($cond, $index, $indexName, 'FROM_UNIXTIME(%s, "%s") <= ?', 1);
						$query[] = $q;
						$params[] = $p;
					} else {
						$query[] = sprintf('%s %s cast(? as decimal(30,10))', $indexName, $cond->getOperator());
						$params[] = $index->getValue();
					}
				} elseif ($index->getType() == Index::DATE ||
					$index->getType() == Index::DATETIME) {
					$values = $this->expandDateExpression($index);
					if (is_array($values)) {
						$query[] = sprintf('%s <= ?', $indexName);
						$params[] = $values[1];
					} else {
						$query[] = sprintf('%s <= ?', $indexName);
						$params[] = $values;
					}
				} else {
					$query[] = sprintf('%s <= ?', $indexName);
					$params[] = $index->getValue();
				}
				break;
			case Criteria::LESS:
				$index = $cond->getColumn()->handleSearchKey($cond->getValue());
				if ($index->getType() === Index::NUMBER) {
					$colType = $cond->getColumn()->getType();
					if ($colType === Column::DATECALC) {
						list($q, $p) = $this->buildDateCalcCond($cond, $index, $indexName, 'FROM_UNIXTIME(%s, "%s") < ?');
						$query[] = $q;
						$params[] = $p;
					} else {
						$query[] = sprintf('%s %s cast(? as decimal(30,10))', $indexName, $cond->getOperator());
						$params[] = $index->getValue();
					}
				} elseif ($index->getType() == Index::DATE ||
					$index->getType() == Index::DATETIME) {
					$values = $this->expandDateExpression($index);
					if (is_array($values)) {
						$query[] = sprintf('%s < ?', $indexName);
						$params[] = $values[0];
					} else {
						$query[] = sprintf('%s < ?', $indexName);
						$params[] = $values;
					}
				} else {
					$query[] = sprintf('%s < ?', $indexName);
					$params[] = $index->getValue();
				}
				break;
			case Criteria::LIKE:
				$index = $cond->getColumn()->handleSearchKey($cond->getValue());
				$query[] = sprintf('%s like ?', $indexName);
				$params[] = '%'.Util::escapeLike($index->getValue()) .'%';
				break;
			case Criteria::NOT_LIKE:
				$index = $cond->getColumn()->handleSearchKey($cond->getValue());
				$query[] = sprintf('(%s not like ? or %s is null)', $indexName, $indexName);
				$params[] = '%'.Util::escapeLike($index->getValue()) .'%';
				break;
			case Criteria::CONTAINS:
				$value = $cond->getValue();
				$query[] = '(';
				foreach ($value as $j => $nestedValue) {
					$index = $cond->getColumn()->handleSearchKey($nestedValue);
					if ($j > 0) {
						$query[] = 'or';
					}
					if (is_empty($index->getValue())) {
						$query[] = sprintf('%s is null', $indexName);
					} else {
						$query[] = sprintf('%s = ?', $indexName);
						$params[] = $index->getValue();
					}
				}
				$query[] = ')';
				break;
			case Criteria::NOT_CONTAINS:
				$value = $cond->getValue();
				$query[] = 'not exists (';
				$query[] = 'select 1 from `indexes` as indexes_0';
				$query[] = 'where `row`.id = indexes_0.row_id and';
				$query[] = 'indexes_0.column_id = ? and (';
				$params[] = $cond->getColumn()->getId();
				$foundNull = false;
				$foundNotNull = false;
				$firstDone2 = false;
				foreach ($value as $j => $nestedValue) {
					$index = $cond->getColumn()->handleSearchKey($nestedValue);
					if (is_empty($index->getValue())) {
						$foundNull = true;
						continue;
					} else {
						$foundNotNull = true;
					}
					if ($firstDone2) {
						$query[] = 'or';
					}
					$query[] = sprintf('`indexes_0`.%s_value = ?', $index->getType());
					$params[] = $index->getValue();
					$firstDone2 = true;
				}
				if ($foundNull) {
					if ($foundNotNull) {
						$query[] = 'or';
					}
					$query[] = 'indexes_0.integer_value is null';
				}
				$query[] = '))';
				break;
			default:
				throw new TxCoreException("Unknown criteria was specified.",
				TxCoreException::INTERNAL);
		}

		return array(join(' ', $query), $params);
	}

	public function insertTableDefBranch(Table $table, $newRevision)
	{
		$query = 'insert into table_def (id, revision, name, description, icon_filekey, is_public, is_default,is_api_enabled,
			api_key, is_write_api_enabled, write_api_key, owner, round_type, round_scale, is_template, permission_control,
			is_comment_enabled, is_calendar_enabled, is_comment_following_enabled, is_read_check_enabled, is_six_columns_enabled)
			select id, ?, name, description, icon_filekey, is_public, is_default,is_api_enabled,
			api_key, is_write_api_enabled, write_api_key, owner, round_type, round_scale, is_template, permission_control,
			is_comment_enabled, is_calendar_enabled, is_comment_following_enabled, is_read_check_enabled, is_six_columns_enabled
			from table_def where id = ? and revision = ?';
		$params = array(
			$newRevision,
			$table->getId(),
			$table->getRevision()
		);
		return $this->db->update($query, $params);
	}

	private function expandDateExpression($index)
	{
		$now = Engine::factory()->getNow();
		$matches = array();
		$value = strtolower($index->getValue());
		if (str_starts_with('/', $value)) {
			$value = substr($value, 1);
		}
		if ($index->getType() == Index::DATE) {
			if ($value == 'now') {
				return date('Y-m-d', $now);
			} else if ($value == 'last_month') {
				return array(
					date('Y-m-01', strtotime('last month', $now)),
					date('Y-m-d', strtotime('last day of last month', $now))
				);
			} else if (preg_match('#^((?:\d+)|(?:last_day))/last_month$#i', $value, $matches)) {
				if (strtolower($matches[1]) == 'last_day') {
					return date('Y-m-d', strtotime('last day of last month', $now));
				} else {
					return date('Y-m-'.$matches[1], strtotime('last month', $now));
				}
			} else if ($value == 'current_month') {
				return array(
					date('Y-m-01', $now),
					date('Y-m-d', strtotime('last day of this month', $now))
				);
			} else if (preg_match('#^((?:\d+)|(?:last_day))/current_month$#i', $value, $matches)) {
				if (strtolower($matches[1]) == 'last_day') {
					return date('Y-m-d', strtotime('last day of this month', $now));
				} else {
					return date('Y-m-'.$matches[1], $now);
				}
			} else if ($value == 'current_year') {
				return array(
					date('Y-01-01', $now),
					date('Y-12-31', $now)
				);
			} else if (preg_match('/\d{4}\-\d{2}-\d{2}/', $value)) {
				return $value;
			} else {
				throw new TxCoreException(sprintf('Invalid search value for date : %s', $value),
					TxCoreException::INTERNAL);
			}
		} else if ($index->getType() == Index::DATETIME) {
			if ($value == 'now') {
				return array(
					date('Y-m-d H:i:00', $now),
					date('Y-m-d H:i:59', $now)
				);
			} else if ($value == 'today') {
				return array(
					date('Y-m-d 00:00:00', $now),
					date('Y-m-d 23:59:59', $now)
				);
			} else if ($value == 'last_month') {
				return array(
					date('Y-m-01 00:00:00', strtotime('last month', $now)),
					date('Y-m-d 23:59:59', strtotime('last day of last month', $now))
				);
			} else if (preg_match('#^((?:\d+)|(?:last_day))/last_month$#i', $value, $matches)) {
				if (strtolower($matches[1]) == 'last_day') {
					return array(
						date('Y-m-d 00:00:00', strtotime('last day of last month', $now)),
						date('Y-m-d 23:59:59', strtotime('last day of last month', $now))
					);
				} else {
					return array(
						date('Y-m-'.$matches[1].' 00:00:00', strtotime('last month', $now)),
						date('Y-m-'.$matches[1].' 23:59:59', strtotime('last month', $now))
					);
				}
			} else if ($value == 'current_month') {
				return array(
					date('Y-m-01 00:00:00', $now),
					date('Y-m-d 23:59:59', strtotime('last day of this month', $now))
				);
			} else if (preg_match('#^((?:\d+)|(?:last_day))/current_month$#i', $value, $matches)) {
				if (strtolower($matches[1]) == 'last_day') {
					return array(
						date('Y-m-d 00:00:00', strtotime('last day of this month', $now)),
						date('Y-m-d 23:59:59', strtotime('last day of this month', $now))
					);
				} else {
					return array(
						date('Y-m-'.$matches[1].' 00:00:00', $now),
						date('Y-m-'.$matches[1].' 23:59:59', $now)
					);
				}
			} else if ($value == 'current_year') {
				return array(
					date('Y-01-01 00:00:00', $now),
					date('Y-12-31 23:59:59', $now)
				);
			} else if (preg_match('/\d{4}\-\d{2}-\d{2} \d{2}:\d{2}/', $value)) {
				return $value.':00';
			} else {
				throw new TxCoreException(sprintf('Invalid search value for datedate : %s', $value),
					TxCoreException::INTERNAL);
			}
		}
	}

	public function deleteRowsByTable(Table $table)
	{
		$query = 'delete from `row` where `table_id` = ?';
		$params = array(
			$table->getId()
		);
		$this->db->update($query, $params);
	}

	public function deleteDataByTable(Table $table)
	{
		$query = 'delete from `data` where `table_id` = ?';
		$params = array(
			$table->getId()
		);
		$this->db->update($query, $params);
	}


	public function deleteIndexesByTable(Table $table)
	{
		$query = 'delete from `indexes` where `table_id` = ?';
		$params = array(
			$table->getId()
		);
		$this->db->update($query, $params);
	}

	public function deleteIndexesByColumn($column)
	{
		$query = 'delete from `indexes` where `column_id` = ?';
		$params = array(
			$column->getId()
		);
		$this->db->update($query, $params);
	}

// 	public function selectCountNonNullIndexesByColumn($column)
// 	{
// 		$indexType = $column->getIndexType();
// 		$query = sprintf(
// 				'select count(*) as `count` from `indexes` where `column_id` = ? and `%s_value` is not null',
// 				$indexType
// 		);
// 		$params = array(
// 			$column->getId()
// 		);
// 		return intval($this->db->query($query, $params, true));
// 	}

// 	public function selectCountDistinctNonNullIndexesByColumn($column)
// 	{
// 		$indexType = $column->getIndexType();
// 		$query = sprintf(
// 				'select count(distinct `%s_value`) as `count` from `indexes` where `column_id` = ? and `%s_value` is not null',
// 				$indexType, $indexType
// 		);
// 		$params = array(
// 			$column->getId()
// 		);
// 		return intval($this->db->query($query, $params, true));
// 	}

	public function selectCountDuplicatedNonNullIndexesByColumn($column)
	{
		$indexType = $column->getIndexType();
		$query = sprintf(
				'select count(*) as `count` from '
				.'(select `%s_value` from `indexes` where `column_id` = ? and `%s_value` is not null '
				.'group by `%s_value` having count(*) > 1) as d',
				$indexType, $indexType, $indexType
		);
		$params = array(
			$column->getId()
		);
		return intval($this->db->query($query, $params, true));
	}

	public function selectMaxLengthOfIndexesByColumn($column)
	{
		$indexType = $column->getIndexType();
		$query = sprintf(
				'select max(char_length(`%s_value`)) as `count` from `indexes` where `column_id` = ?',
				$indexType
		);
		$params = array(
			$column->getId()
		);
		return intval($this->db->query($query, $params, true));
	}

	public function deleteDataByColumn($column)
	{
		$query = 'delete from `data` where `column_id` = ?';
		$params = array(
			$column->getId()
		);
		$this->db->update($query, $params);
	}

	public function insertEmptyColumnIndexes(Column $column)
	{

		$query = "insert into `indexes` (id, table_id, column_id, string_value,
			integer_value, number_value, date_value, time_value, datetime_value, row_id)
			select null, ?, ?, null, null, null, null, null, null, id from `row` where table_id = ?";
		$params = array(
			$column->getParent()->getId(),
			$column->getId(),
			$column->getParent()->getId(),
		);

		$this->db->update($query, $params);
	}

	public function selectChangedColumns(Table $trunk, Table $release, $callback)
	{
		$query = "
			select released.*, 'removed' as changed from column_def as released
			where
				released.table_id = :table_id and released.`revision` = :rev_release and
				not exists (
					select 1 from column_def as trunk
					where trunk.table_id = :table_id and
					trunk.`revision` = :rev_trunk and
					trunk.id = released.id
					)
			union all
			select trunk.*, 'add' as changed from column_def as trunk
			where
				trunk.table_id = :table_id and trunk.`revision` = :rev_trunk and
				not exists (
					select 1 from column_def as released
					where released.table_id = :table_id and
					released.`revision` = :rev_released and
					released.id = trunk.id
					)
		";
		$params = array(
			'table_id' => $release->getId(),
			'rev_release' => $release->getRevision(),
			'rev_trunk' => $release->getRevision(),
		);
		$this->db->fetch($query, $params, $callback);
	}

	public function updateRevisionHasPendding(Table $table)
	{
		$query = '
			update release_control set has_pending_release = 1
			where table_id = ?';
		$params = array(
			$table->getId()
		);
		return $this->db->update($query, $params);
	}

	public function updateReleaseControl(Table $trunk, Table $release, $hasPenning = false)
	{
		$query = 'update `release_control` set trunk_revision = ?, release_revision = ?, has_pending_release = ?
			where table_id = ?';
		$params = array(
			$trunk->getRevision(),
			$release->getRevision(),
			$hasPenning ? 1 : 0,
			$trunk->getId(),
		);
		return $this->db->update($query, $params);
	}

	public function deleteTableDef(Table $table, $allVers = false)
	{
		if ($allVers) {
			$query = 'delete from `table_def` where id = ?';
			$params = array(
				$table->getId()
			);
			return $this->db->update($query, $params);
		} else {
			$query = 'delete from `table_def` where id = ? and `revision` = ?';
			$params = array(
				$table->getId(),
				$table->getRevision()
			);
			return $this->db->update($query, $params);
		}

	}

// 	public function deleteTableOptionsAll(Table $table)
// 	{
// 		$this->db->update("delete from `option` where table_id = ?", array($table->getId()));
// 	}

	public function deleteFilterByTable(Table $table)
	{
		$this->db->update("delete from `filter` where table_id = ?", array($table->getId()));
	}

	public function deleteViewColumnsByTable(Table $table, $allVers = false)
	{
		if ($allVers) {
			$query = 'delete from `view_column` where table_id = ?';
			$params = array(
				$table->getId()
			);
			return $this->db->update($query, $params);
		} else {
			$query = 'delete from `view_column` where table_id = ? and `revision` = ?';
			$params = array(
				$table->getId(),
				$table->getRevision()
			);
			return $this->db->update($query, $params);
		}
	}

	public function deletePermissionsByTable(Table $table, $allVers = false)
	{
		if ($allVers) {
			$query = 'delete from `permission` where table_id = ?';
			$params = array(
				$table->getId()
			);
			return $this->db->update($query, $params);
		} else {
			$query = 'delete from `permission` where table_id = ? and `revision` = ?';
			$params = array(
				$table->getId(),
				$table->getRevision()
			);
			return $this->db->update($query, $params);
		}
	}

	public function deletePermissionsByTableOtherRevision(Table $table)
	{
		$query = 'delete from `permission` where table_id = ? and revision <> ?';
		$params = array(
				$table->getId(),
				$table->getRevision(),
		);
		return $this->db->update($query, $params);
	}

// 	public function deleteTableColumnDef(Table $table)
// 	{
// 		$this->db->update("delete from `column_def` where table_id = ?", array($table->getId()));
// 	}




// 	public function buildDeleteViewListQuery(Table $table)
// 	{
// 		return array('delete from view_column where table_id = ?',
// 			array($work->getId()));
// 	}

// 	private function buildInsertViewListQuery(Table $table, $columns)
// 	{
// 		$query = array();
// 		$params = array();
// 		$query[] = "insert into view_column (table_id, column_id, `seq`) values";
// 		$seq = 0;
// 		$firstDone = false;
// 		foreach ($columns as $column) {
// 			if ($firstDone)
// 				$query[] = ",";
// 			$columnId = ($column instanceof Column) ? $column->getId() : $column;
// 			$query[] = "(?, ?, ?)";
// 			$params[] = $table->getId();
// 			$params[] = $columnId;
// 			$params[] = $seq;
// 			$seq++;
// 			$firstDone = true;
// 		}
// 		return array(join(' ', $query), $params);
// 	}

	public function executeOptimization()
	{
		$query = "";
		$query .= "alter table `indexes` engine InnoDB;";
		$query .= "alter table `data` engine InnoDB;";
		$query .= "alter table `row` engine InnoDB;";

		$this->db->update($query, array());
	}

	public function findGroupByName($name, $ignoreId = null)
	{
		$name = trim($name);
		$result = null;
		if ($ignoreId !== null) {
			$result = $this->db->query('
				select *
				from `group` where `group`.name = ? and `group`.id <> ?
				', array($name, $ignoreId));
		} else {
			$result = $this->db->query('
				select *
				from `group` where `group`.name = ?
				', array($name));
		}
		return is_empty($result) ? null : $result[0];
	}

	public function selectGroupUsers($groupId)
	{
		return $this->db->query('
			select * from `user_group` inner join `user` on (`user`.id = `user_group`.user_id)
			where group_id = ? order by `user`.screen_name
			', array($groupId));
	}

	public function deleteUserGroupByGroupId($groupId)
	{
		$query = 'delete from `user_group` where `group_id` = ?';
		$params = array(
			$groupId
		);
		return $this->db->update($query, $params);
	}

	public function insertUserGroup($userId, $groupId)
	{
		$query = 'insert into user_group (user_id, group_id) values (?, ?)';
		$params = array( $userId, $groupId );
		return $this->db->update($query, $params);
	}

	public function deleteCommentByRow(Row $row)
	{
		$query = 'delete from `comment` where `comment`.row_id = ?';
		$params = array(
			$row->getId()
		);
		return $this->db->update($query, $params);
	}

	public function deleteCommentByTable(Table $table)
	{
		$query = 'delete from `comment` where `comment`.table_id = ?';
		$params = array(
			$table->getId()
		);
		return $this->db->update($query, $params);
	}

	public function selectMaxCommentNo(Row $row)
	{
		return intval($this->db->query('select max(`no`) as no from `comment` where row_id = ?',
			array($row->getId()), true));
	}

	public function insertComment(Row $row, Comment $comment)
	{
		$targets = array_map(function($entity) {
			if ($entity instanceof User) {
				return array('type'=>'user', 'id'=>$entity->getId());
			} else if ($entity instanceof Group) {
				return array('type'=>'group', 'id'=>$entity->getId());
			} else {
				assert(false);
			}
		}, $comment->getNotificationTargets());

		$query = 'insert into `comment`
			(id, no, text, posted_time, posted_user_id, row_id, table_id, notification_targets) values
			(null,?, ?, current_timestamp, ?, ?,
			(select table_id from `row` where `id`=?), ?)';
		$params = array(
			$comment->getNo(),
			$comment->getText(),
			$comment->getPostedUser()->getId(),
			$row->getId(),
			$row->getParent()->getId(),
			Util::encodeJSON($targets)
		);

		$this->db->update($query, $params);
		return $this->db->lastInsertId('id');
	}

	/**
	 * @param Row $row
	 * @return mixed
	 */
	public function selectCommentsCountByRow(Row $row)
	{
		$query = 'select count(*) as count from `comment` where row_id = ?';
		$list = $this->db->query($query,
			array($row->getId()));
		return $list[0]->count;
	}

	/**
	 * @param $id
	 * @param $revision
	 * @return array
	 */
	public function selectViewCalendarSetting($id, $revision)
	{
		$query = 'select * from `view_calendar_setting`
			where table_id = ? and revision = ?';
		return $this->db->query($query,
			array($id, $revision));
	}

    /**
     * @param $id
     * @param $revision
     * @return array
     */
    public function selectReadCheckSettings($id, $revision)
    {
        $query = 'select * from `read_check_setting`
			where table_id = ? and revision = ?';
        return $this->db->query($query,
            array($id, $revision));
    }

    /**
     * Delete read log by specified table id.
     * @param $tableId
     * @return int Number of rows affected.
     */
    public function deleteReadLogByTable($tableId)
    {
        $query = 'delete from `read_log` where table_id = ?';
        $params = array(
            $tableId
        );
        return $this->db->update($query, $params);
    }

	public function deleteViewCalendarSettingByTable(Table $table, $allVers = false)
	{
		if ($allVers) {
			$query = 'delete from `view_calendar_setting` where table_id = ?';
			$params = array(
				$table->getId()
			);
			return $this->db->update($query, $params);
		} else {
			$query = 'delete from `view_calendar_setting` where table_id = ? and `revision` = ?';
			$params = array(
				$table->getId(),
				$table->getRevision()
			);
			return $this->db->update($query, $params);
		}
	}

    /**
     * @param $tableId
     * @param null $revision
     * @return int Number of rows affected.
     */
    public function deleteReadCheckSettingsByTable($tableId, $revision = null)
    {
        if ($revision === null) {
            $query = 'delete from `read_check_setting` where table_id = ?';
            $params = array(
                $tableId
            );
            return $this->db->update($query, $params);
        } else {
            $query = 'delete from `read_check_setting` where table_id = ? and `revision` = ?';
            $params = array(
                $tableId,
                $revision
            );
            return $this->db->update($query, $params);
        }
    }

	public function insertViewCalendarSetting(Table $table)
	{
		$query = array();
		$query[] = 'insert into `view_calendar_setting`
				(  `table_id` , `revision`, `date_column_id`, `user_column_id`,
				`start_time_column_id`, `end_time_column_id`, `title_column_id`) values (?, ?, ?, ?, ?, ?, ?)';
		$params = array(
			$table->getId(),
			$table->getRevision(),
			empty_to_null($table->getCalendarSettings()->getDateColumnId()),
			empty_to_null($table->getCalendarSettings()->getUserColumnId()),
			empty_to_null($table->getCalendarSettings()->getStartTimeColumnId()),
			empty_to_null($table->getCalendarSettings()->getEndTimeColumnId()),
			empty_to_null($table->getCalendarSettings()->getTitleColumnId()),
		);
		$this->db->update(join(' ', $query), $params);
	}

    /**
     * @param $tableId
     * @param $revision
     * @param ReadCheckSettings $readCheckSettings
     * @return int
     */
    public function insertReadCheckSetting($tableId, $revision, ReadCheckSettings $readCheckSettings)
    {
        $query = array();
        $query[] = 'insert into `read_check_setting`
				(  `table_id` , `revision`, `is_autoread_enabled`, `is_onlist_enabled`, `check_targets`)
				values (?, ?, ?, ?, ?)';
        $params = array(
            $tableId,
            $revision,
            Util::toBoolInt($readCheckSettings->isAutoreadEnabled()),
            Util::toBoolInt($readCheckSettings->isOnlistEnabled()),
            Util::encodeJSON($readCheckSettings->getCheckTargetsArray()),
        );
        return $this->db->update(join(' ', $query), $params);
    }

	public function selectNotificationViewColumns(Table $table)
	{
		$query = 'select * from notification_view_column where table_id = ? and revision = ? order by `seq`';
		$params = array($table->getId(), $table->getRevision());
		return $this->db->query($query, $params);
	}

	public function deleteNotificationViewColumnsByTable(Table $table, $allVers = false)
	{
		if ($allVers) {
			$query = 'delete from `notification_view_column` where table_id = ?';
			$params = array(
				$table->getId()
			);
			return $this->db->update($query, $params);
		} else {
			$query = 'delete from `notification_view_column` where table_id = ? and `revision` = ?';
			$params = array(
				$table->getId(),
				$table->getRevision()
			);
			return $this->db->update($query, $params);
		}
	}

	public function insertNotificationViewColumns(Table $table)
	{
		$query = array();
		$query[] = 'insert into `notification_view_column`
				(table_id,revision, column_id,seq) values ';
		$params = array();

		$seq = 1;
		$firstDone = false;
		foreach ($table->getNotificationViewColumns() as $i => $column) {
			if ($firstDone)
				$query[] = ',';
			$query[] = '(?, ?, ?, ?)';
			$params = array_merge($params, array(
				$table->getId(),
				$table->getRevision(),
				$column->getId(),
				$seq++
			));
			$firstDone = true;
		}
		if (!$firstDone)
			return;
		$this->db->update(join(' ', $query), $params);
	}

	public function updateNotificationViewColumnRevision(Table $table, $newRevision)
	{
		$query = ' update `notification_view_column` set revision = ? where table_id = ? and revision = ?';
		$params = array(
			$newRevision,
			$table->getId(),
			$table->getRevision(),
		);
		$this->db->update($query, $params);
	}

	public function selectNotificationBasicRules(Table $table)
	{
		$query = 'select * from notification_basic_rule where table_id = ? and revision = ? order by `no`';
		$params = array($table->getId(), $table->getRevision());
		return $this->db->query($query, $params);
	}

	public function deleteNotificationBasicRulesByTable(Table $table, $allVers = false)
	{
		if ($allVers) {
			$query = 'delete from `notification_basic_rule` where table_id = ?';
			$params = array(
				$table->getId()
			);
			return $this->db->update($query, $params);
		} else {
			$query = 'delete from `notification_basic_rule` where table_id = ? and `revision` = ?';
			$params = array(
				$table->getId(),
				$table->getRevision()
			);
			return $this->db->update($query, $params);
		}
	}

	public function insertNotificationBasicRules(Table $table)
	{
		$seq = 1;
		/* @var $rule NotificationBasicRule */
		foreach ($table->getNotificationBasicRules() as $rule) {
			$query = array();
			$query[] = 'insert into `notification_basic_rule` ('
				.'table_id,revision,no,targeting_type,targets,'
				.'is_on_row_insert,is_on_row_update,is_on_comment_insert'
				.') values ';
			$query[] = '(?, ?, ?, ?, ?, ?, ?, ?)';
			$params = array(
				$table->getId(),
				$table->getRevision(),
				$seq++,
				$rule->getTargetingType(),
				Util::encodeJSON($rule->getTargetsArray()),
				Util::toBoolInt($rule->isOnRowInsert()),
				Util::toBoolInt($rule->isOnRowUpdate()),
				Util::toBoolInt($rule->isOnCommentInsert()),
			);
			$this->db->update(join(' ', $query), $params);
		}
	}

	public function updateNotificationBasicRuleRevision(Table $table, $newRevision)
	{
		$query = ' update `notification_basic_rule` set revision = ? where table_id = ? and revision = ?';
		$params = array(
			$newRevision,
			$table->getId(),
			$table->getRevision(),
		);
		$this->db->update($query, $params);
	}

	public function selectNotificationConditionalRules(Table $table)
	{
		$query = 'select * from notification_conditional_rule where table_id = ? and revision = ? order by `no`';
		$params = array($table->getId(), $table->getRevision());
		return $this->db->query($query, $params);
	}

	public function deleteNotificationConditionalRulesByTable(Table $table, $allVers = false)
	{
		if ($allVers) {
			$query = 'delete from `notification_conditional_rule` where table_id = ?';
			$params = array(
				$table->getId()
			);
			return $this->db->update($query, $params);
		} else {
			$query = 'delete from `notification_conditional_rule` where table_id = ? and `revision` = ?';
			$params = array(
				$table->getId(),
				$table->getRevision()
			);
			return $this->db->update($query, $params);
		}
	}

	public function insertNotificationConditionalRules(Table $table)
	{
		$seq = 1;
		/* @var $rule NotificationConditionalRule */
		foreach ($table->getNotificationConditionalRules() as $rule) {
			$query = array();
			$query[] = 'insert into `notification_conditional_rule` ('
				.'table_id,revision,no,targeting_type,targets,'
				.'expression,message'
				.') values ';
			$query[] = '(?, ?, ?, ?, ?, ?, ?)';
			$params = array(
				$table->getId(),
				$table->getRevision(),
				$seq++,
				$rule->getTargetingType(),
				Util::encodeJSON($rule->getTargetsArray()),
				Util::encodeJSON($rule->getCriteria()->toArray()),
				$rule->getMessage(),
			);
			$this->db->update(join(' ', $query), $params);
		}
	}

	public function updateNotificationConditionalRuleRevision(Table $table, $newRevision)
	{
		$query = ' update `notification_conditional_rule` set revision = ? where table_id = ? and revision = ?';
		$params = array(
			$newRevision,
			$table->getId(),
			$table->getRevision(),
		);
		$this->db->update($query, $params);
	}

	public function selectNotification($id)
	{
		$query = 'select * from `notification` where `notification`.id = ?';
		$params = array(
			$id
		);
		return $this->db->query($query, $params);
	}

	public function selectNotificationReceipt($notificationId, User $user)
	{
		$query = 'select * from `notification_receipt` where notification_id = ? and recipient_id = ?';
		$params = array(
			$notificationId,
			$user->getId(),
		);
		return $this->db->query($query, $params);
	}

	public function insertNotification(
		Notification $notification,
		User $by, Table $table, Row $row, $comment = null)
	{
		$now = Engine::factory()->getNow();
		$query = 'insert into notification (notifier_id, notified_on, message, changes, table_id, row_id, comment_id) '
			.'values (?, ?, ?, ?, ?, ?, ?)';
		$params = array(
			$by->getId(),
			date('Y-m-d H:i:s', $now),
			$notification->getMessage(),
			Util::encodeJSON($notification->getChangesArray()),
			$table->getId(),
			$row->getId(),
			$comment ? $comment->getId() : null,
		);
		$result = $this->db->update($query, $params);

		$notification->_setId($this->db->lastInsertId('id'));
		$notification->_setNotifiedOn($now);
		return $notification;
	}

	public function deleteNotificationsByTable(Table $table)
	{
		$query = 'delete from `notification` where table_id = ?';
		$params = array(
			$table->getId(),
		);
		$this->db->update($query, $params);
	}

	public function deleteNotificationsByRow(Row $row)
	{
		$query = 'delete from `notification` where row_id = ?';
		$params = array(
			$row->getId(),
		);
		$this->db->update($query, $params);
	}

	public function deleteNotificationsByComment(Comment $comment)
	{
		$query = 'delete from `notification` where comment_id = ?';
		$params = array(
			$comment->getId(),
		);
		$this->db->update($query, $params);
	}

	public function updateNotificationReceipt(
		NotificationReceipt $receipt)
	{
		$query = 'update notification_receipt set is_read = ? '
			. 'where notification_id = ? and recipient_id = ?';
		$params = array(
			Util::toBoolInt($receipt->isRead()),
			$receipt->_getNotificationId(),
			$receipt->_getRecipientId(),
		);
		$this->db->update($query, $params);
	}

	public function insertNotificationReceipts(
		Notification $notification, $recipients = array(),
		User $by, Table $table, Row $row, $comment = null)
	{
		$query = array();
		$query[] = 'insert into `notification_receipt`
				(notification_id, recipient_id, is_read, notified_on, table_id, row_id, comment_id) values ';
		$params = array();

		$seq = 1;
		$firstDone = false;
		foreach ($recipients as $recipient) {
			/* @var recipient User */
			if ($firstDone)
				$query[] = ',';
			$query[] = '(?, ?, ?, ?, ?, ?, ?)';
			$params[] = $notification->getId();
			$params[] = $recipient->getId();
			$params[] = 0; // remain unread
			$params[] = date('Y-m-d H:i:s',
				$notification->getNotifiedOn());
			$params[] = $table->getId();
			$params[] = $row->getId();
			$params[] = $comment ? $comment->getId() : null;
			$firstDone = true;
		}
		if (!$firstDone)
			return;
		$this->db->update(join(' ', $query), $params);
	}

	public function deleteNotificationReceiptsByTable(Table $table)
	{
		$query = 'delete from `notification_receipt` where table_id = ?';
		$params = array(
			$table->getId(),
		);
		$this->db->update($query, $params);
	}

	public function deleteNotificationReceiptsByRow(Row $row)
	{
		$query = 'delete from `notification_receipt` where row_id = ?';
		$params = array(
			$row->getId(),
		);
		$this->db->update($query, $params);
	}

	public function deleteNotificationReceiptsByComment(Comment $comment)
	{
		$query = 'delete from `notification_receipt` where comment_id = ?';
		$params = array(
			$comment->getId(),
		);
		$this->db->update($query, $params);
	}

	public function deleteNotificationReceiptsByRecipient(User $recipient)
	{
		$query = 'delete from `notification_receipt` where recipient_id = ?';
		$params = array(
			$recipient->getId(),
		);
		$this->db->update($query, $params);
	}

	public function selectNotificationReceiptsCountByRecipient(
		User $recipient, $read = false)
	{
		$query = 'select count(*) as count from `notification_receipt` where recipient_id = ? and is_read = ? ';
		$params = array(
			$recipient->getId(),
			Util::toBoolInt($read)
		);
		$list = $this->db->query($query, $params);
		return $list[0]->count;
	}

	public function selectRelationships(
		Row $sourceRow, Column $sourceColumn)
	{
		$query = 'select * from `relationship` where source_row_id = ? and source_column_id = ? ';
		$params = array(
			$sourceRow->getId(),
			$sourceColumn->getId()
		);
		$list = $this->db->query($query, $params);
		return $list;
	}

	public function selectRelationshipsCount(
		Row $sourceRow, Column $sourceColumn)
	{
		$query = 'select count(*) as count from `relationship` where source_row_id = ? and source_column_id = ? ';
		$params = array(
			$sourceRow->getId(),
			$sourceColumn->getId()
		);
		$list = $this->db->query($query, $params);
		return $list[0]->count;
	}

	public function selectRelationshipsCountByColumn(
		$sourceColumn)
	{
		$query = 'select count(*) as count from `relationship` where source_column_id = ?';
		$params = array(
			$sourceColumn->getId()
		);
		$list = $this->db->query($query, $params);
		return $list[0]->count;
	}

	public function insertRelationship(
		Column $column, Row $row, Row $targetRow)
	{
		$type = 'relational_input';
		$query = 'insert into relationship (relationship_type, source_row_id, target_row_id, source_table_id, target_table_id, source_column_id) values (?, ?, ?, ?, ?, ?)';
		$params = array(
			$type,
			$row->getId(),
			$targetRow->getId(),
			$row->getParent()->getId(),
			$targetRow->getParent()->getId(),
			$column->getId(),
		);
		return $this->db->update($query, $params);
	}

	public function deleteRelationship(
		Column $column, Row $row, Row $targetRow)
	{
		$query = 'delete from `relationship` where source_row_id = ? and target_row_id = ? and source_column_id = ? limit 1';
		$params = array(
			$row->getId(),
			$targetRow->getId(),
			$column->getId(),
		);
		$this->db->update($query, $params);
	}

	/**
	 * @return bloolean
	 */
	public function selectIsTableChild(Table $table,
		$fromTrunkOrRelease = false)
	{
		$query = array();
		$params = array();

		$query[] = "select if(count(*)>0, 1, 0) as is_child";
		$query[] = "from reference rf";
		if (!$fromTrunkOrRelease) {
			$query[] = "join release_control rc";
			$query[] = "on (rc.table_id = rf.table_id and rc.release_revision = rf.revision)";
		}
		$query[] = "join column_def c";
		$query[] = "on (c.id = rf.column_id and c.revision = rf.revision)";
		$query[] = "where rf.target_table_id = ?";
		$query[] = "and c.column_type = 'relational_input'";
		$params[] = $table->getId();

		$list = $this->db->query(join(' ', $query), $params);
		return $list[0]->is_child;
	}

	/**
	 * @return bloolean
	 */
	public function selectIsTableParent(Table $table,
		$fromTrunkOrRelease = false)
	{
		$query = array();
		$params = array();

		$query[] = "select if(count(*)>0, 1, 0) as is_parent";
		$query[] = "from reference rf";
		if (!$fromTrunkOrRelease) {
			$query[] = "join release_control rc";
			$query[] = "on (rc.table_id = rf.table_id and rc.release_revision = rf.revision)";
		}
		$query[] = "join column_def c";
		$query[] = "on (c.id = rf.column_id and c.revision = rf.revision)";
		$query[] = "where rf.table_id = ?";
		$query[] = "and c.column_type = 'relational_input'";
		$params[] = $table->getId();

		$list = $this->db->query(join(' ', $query), $params);
		return $list[0]->is_parent;
	}

	public function selectParentTableIdsByTable($tableId)
	{
		$query = "select distinct r.table_id from reference r, column_def c where target_table_id = ? and c.column_type = 'relational_input' and r.column_id = c.id and r.revision = c.revision";
		$params = array();
		$params[] = $tableId;
		$list = $this->db->query($query, $params);
		
		return $list;
	}
	


    /**
     * @param int $rowId
     * @return array
     */
    public function selectReadLogs($rowId)
    {
        $query = "select * from `read_log` where row_id = ?";
        $params = array();
        $params[] = $rowId;
        return $this->db->query($query, $params);
    }

    /**
     * @param int $rowId
     * @return array
     */
    public function selectReadLog($rowId, $userId)
    {
        $query = "select * from `read_log` where row_id = ? and read_user_id = ?";
        $params = array();
        $params[] = $rowId;
        $params[] = $userId;
        return $this->db->query($query, $params);
    }

    /**
     * @param int $id
     * @return int
     */
    public function selectRowReadLogCount($id)
    {
        $query = "select count(*) as count from `read_log` where row_id = ?";
        $params = array();
        $params[] = $id;
        $list = $this->db->query($query,
            $params);
        return $list[0]->count;
    }

    /**
     * @param int $tableId
     * @param int $rowId
     * @param $comment
     * @param int $userId
     * @return int
     */
    public function insertToReadReadLog($tableId, $rowId, $userId, $comment)
    {
        $query = 'insert into `read_log` (table_id, row_id, read_user_id, read_on, comment)
          values (?, ?, ?, current_timestamp, ?)';
        $params = array(
            $tableId,
            $rowId,
            $userId,
            empty_to_null($comment)
        );
        return $this->db->update($query, $params);
    }

    /**
     * @param int $tableId
     * @param int $rowId
     * @param int $userId
     * @return int
     */
    public function deleteReadLog($tableId, $rowId, $userId)
    {
        $query = 'delete from `read_log`
          where table_id = ? and row_id = ? and read_user_id = ?';
        $params = array(
            $tableId,
            $rowId,
            $userId
        );
        return $this->db->update($query, $params);
    }

    public function selectReadLogByUser($rowId, $userId)
    {
        $query = "select * from `read_log` where row_id = ? and read_user_id = ?";
        $params = array(
            $rowId,
            $userId
        );
        return $this->db->query($query, $params);
    }

    /**
     * @param int $userId
     * @return int
     */
    public function deleteReadLogsByUser($userId)
    {
        $query = 'delete from `read_log`
          where read_user_id = ?';
        $params = array(
            $userId
        );
        return $this->db->update($query, $params);
    }

    /**
     * @param $rowId
     * @param $userId
     * @param $comment
     * @return int
     */
    public function updateReadLogComment($rowId, $userId, $comment)
    {
        $query = 'update `read_log` set comment = ? where row_id = ? and read_user_id = ?';
        $params = array(
            empty_to_null($comment),
            $rowId,
            $userId
        );
        return $this->db->update($query, $params);
    }

    /**
     * @param int $id Row id to delete.
     * @return int
     */
    public function deleteReadLogByRow($id)
    {
        $query = 'delete from `read_log` where row_id = ?';
        $params = array(
            $id
        );
        return $this->db->update($query, $params);
    }

	public function selectAggregate(Table $table, Criteria $criteria, $callable)
	{
		list ($query, $params) = $this->buildAggregateQuery($table, $criteria);
		if(is_not_empty($query)){
			$this->db->fetch($query, $params, $callable);
		}
	}

	private function buildAggregateQuery(Table $table, Criteria $criteria)
	{
		$q1 = array();
		$p1 = array();
		$aggregate = $criteria->getAggregate();

		if(!$aggregate) return array(null, null);

		$aggregateColumns = array();
		$fns = array();
		
		foreach($aggregate as $pair){
			$aggregateColumns[] = $pair[0];
			$fns = array_merge($fns, $pair[1]);
		}
		$fns = array_unique($fns);

		// 集約関数または集計対象のカラムが存在しない場合
		if(is_empty($fns) || is_empty($aggregateColumns)){
			return array(null, null);
		}

		$term = $criteria->getConditions();
		$targetColumn = $term[0]->getColumn();
		$conditionValue = $term[0]->getValue();

		// クエリ構築
		$firstDone = false;
		foreach($aggregateColumns as $aggregateColumn){
			if($firstDone){
				$q1[] = "\n" . 'union all' . "\n";
			}
			
			$q1[] = sprintf('select indexes_%1$d.column_id as `column_id`' . "\n", $aggregateColumn->getId());

			foreach($fns as $fn){
				switch($fn){
					case Criteria::FN_SUM:
						$q1[] =  sprintf(', sum(ifnull(indexes_%1$d.%2$s_value, 0)) as `sum_value`' . "\n",
							$aggregateColumn->getId(),
							$aggregateColumn->getIndexType()
						);
						break;
					case Criteria::FN_AVG:
						$q1[] =  sprintf(', avg(ifnull(indexes_%1$d.%2$s_value, 0)) as `avg_value`' . "\n",
							$aggregateColumn->getId(),
							$aggregateColumn->getIndexType()
						);
						break;
					case Criteria::FN_MAX:
						$q1[] =  sprintf(', max(ifnull(indexes_%1$d.%2$s_value, 0)) as `max_value`' . "\n",
							$aggregateColumn->getId(),
							$aggregateColumn->getIndexType()
						);
						break;
					case Criteria::FN_MIN:
						$q1[] =  sprintf(', min(ifnull(indexes_%1$d.%2$s_value, 0)) as `min_value`' . "\n",
							$aggregateColumn->getId(),
							$aggregateColumn->getIndexType()
						);
						break;
				}
			}

			$q1[] = 'from (select distinct `data`.row_id as id from `data` join (' . "\n";
			$q1[] = 'select row_.* from `row` as row_';
			$q1[] = sprintf('join indexes as indexes_%1$d '."\n", $targetColumn->getId());
			$q1[] = sprintf('   on (indexes_%1$d.column_id = %1$d and indexes_%1$d.row_id = row_.id)'."\n", $targetColumn->getId());

			if (is_not_empty($criteria->getConditions())) {
				$q1[] = 'where'."\n";
				$firstDone2 = false;
				foreach ($criteria->getConditions() as $condition) {	/* @var $cond TxCore\Condition */
					if ($firstDone2)
						$q1[] = $criteria->getBoolOperator();
					$indexName = sprintf('indexes_%1$d.%2$s_value /*%3$s*/',
						$condition->getColumn()->getId(), $condition->getColumn()->getIndexType(), $condition->getColumn()->getType());
					list ($subQuery, $subParams) = $this->buildWhereTermQuery($condition, $indexName);
					$q1[] = $subQuery;
					$p1 = array_merge($p1, $subParams);
					$firstDone2 = true;
				}
			}
			$q1[] = "\n";
			$q1[] = ') as _row on (_row.id = data.row_id)' . "\n";
			$q1[] = ') as row';
			$q1[] = sprintf('join indexes as indexes_%1$d '."\n", $aggregateColumn->getId());
			$q1[] = sprintf('   on (indexes_%1$d.column_id = %1$d and indexes_%1$d.row_id = row.id)'."\n", $aggregateColumn->getId());

			$firstDone = true;
		}

		return array(join(' ', $q1), $p1);
	}

}




