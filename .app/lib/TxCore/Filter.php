<?php
namespace TxCore;

use TxCore\Engine;

/**
 * Table definition.
 */
class Filter
{
	private $id;
	private $name;
	private $criteria;
	private $parent;
	
	public function getId()
	{
		return $this->id;
	}
	public function _setId($value)
	{
		$this->id = Util::toInt($value);
	}
	
	/**
	 * @return Table
	 */
	public function getParent()
	{
		return $this->parent;
	}
	public function _setParent($value)
	{
		$this->parent = $value;
	}
	
	
	public function getName()
	{
		return $this->name;
	}
	public function setName($value)
	{
		$this->name = $value;
	}
	
	public function getCriteria()
	{
		return $this->criteria;
	}
	public function setCriteria(Criteria $value)
	{
		$this->criteria = $value;
	}
	public function save(User $by)
	{
		Engine::factory()->saveFilter($this, $by);
	}
	public function remove(User $by)
	{
		Engine::factory()->removeFilter($this, $by);
	}
}

