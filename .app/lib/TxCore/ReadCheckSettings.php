<?php
namespace TxCore;

/**
 * Read check (the circular) view settings.
 */
class ReadCheckSettings
{
    /**
     * @var bool
     */
    private $isAutoreadEnabled;

    /**
     * @var bool
     */
    private $isOnlistEnabled;

    /**
     * @var array
     */
    private $checkTargets = array();

    /**
     * @return boolean
     */
    public function isAutoreadEnabled()
    {
        return $this->isAutoreadEnabled;
    }

    /**
     * @param boolean $isAutoreadEnabled
     */
    public function setAutoreadEnabled($isAutoreadEnabled)
    {
        $this->isAutoreadEnabled = $isAutoreadEnabled;
    }

    /**
     * @return boolean
     */
    public function isOnlistEnabled()
    {
        return $this->isOnlistEnabled;
    }

    /**
     * @param boolean $isOnlistEnabled
     */
    public function setOnlistEnabled($isOnlistEnabled)
    {
        $this->isOnlistEnabled = $isOnlistEnabled;
    }

    /**
     * @param User|Group $entity
     */
    public function addCheckTarget($entity)
    {
        assert($entity instanceof User || $entity instanceof Group);
        $this->checkTargets[] = $entity;
    }

    /**
     * @return User[]|Group[]
     */
    public function getCheckTargets()
    {
        return $this->checkTargets;
    }

    /**
     * @param array $checkTargets
     */
    public function setCheckTargets($checkTargets)
    {
        $this->checkTargets = $checkTargets;
    }

    /**
     * @param $obj
     * @throws TxCoreException
     */
    public function setCheckTargetsArray($obj)
    {
        $entities = array();
        foreach ($obj ?: array() as $optionObj) {
            $entity = null;
            if ($optionObj->type == 'user') {
                $entity = Engine::factory()->getUser($optionObj->id, false, true);
                if ($entity === null) {
                    continue;
                }
            } else if ($optionObj->type == 'group') {
                $entity = Engine::factory()->getGroup($optionObj->id, true);
                if ($entity === null) {
                    continue;
                }
            } else {
                assert(false);
            }

            $entities[] = $entity;
        }
        $this->checkTargets = $entities;
    }

    /**
     * @return array
     */
    public function getCheckTargetsArray()
    {
        $this_ = $this;
        $entities = $this->getCheckTargets();
        $targets = array_map(function($t) use ($this_) { /** @var $t Group|User */
            return array('id'=>$t->getId(), 'type'=>($t instanceof User ? 'user' : 'group'));
        }, $entities);
        return $targets;
    }




}

