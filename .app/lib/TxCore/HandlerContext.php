<?php

namespace TxCore;

use \ArrayAccess;
use \ArrayIterator;
use \IteratorAggregate;

use TxCore\Engine;

/**
 * 
 */
class HandlerContext 
{
	private $action;
	private $by;
	public function __construct($action, $by) 
	{
		$this->action = $action;
		$this->by = $by;
	}
	public function getAction() 
	{
		return $this->action;
	}
	public function getBy() 
	{
		return $this->by;
	}	
}
