<?php

namespace TxCore;

class ReadLog
{
    /**
     * @var string
     */
    private $readOn;

    /**
     * @var User
     */
    private $readUser;

    /**
     * @var string
     */
    private $comment;

    /**
     * @return \DateTime
     */
    public function getReadOn()
    {
        return $this->readOn;
    }

    /**
     * @param string $readOn
     */
    public function _setReadOn($readOn)
    {
        $this->readOn = $readOn;
    }

    /**
     * @return User
     */
    public function getReadUser()
    {
        return $this->readUser;
    }

    /**
     * @param User $user
     */
    public function _setReadUser(User $user)
    {
        $this->readUser = $user;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function _setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function canEdit(User $user)
    {
        return $user->getId() === $this->getReadUser()->getId();
    }

}
