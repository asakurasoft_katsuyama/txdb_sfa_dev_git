<?php

namespace TxCore;

class NotificationRule
{

    const TARGETING_USERGROUP = 'user_group';
    const TARGETING_COLUMN = 'column';

    protected $targetingType;
    protected $targets = array();

    protected $parent;

    /**
     * @return Table
     */
    public function getParent()
    {
        return $this->parent;
    }
    public function _setParent($value)
    {
        $this->parent = $value;
    }

    public function getTargetingType()
    {
        return $this->targetingType;
    }
    public function setTargetingType($value)
    {
        $this->targetingType = $value;
    }

    /**
     * @return User[]|Group[]
     */
    public function getTargets()
    {
        return $this->targets;
    }
    /**
     * @param User|Group $entity
     */
    public function addTarget($entity)
    {
        assert($entity instanceof User || $entity instanceof Group);
        $this->targets[] = $entity;
    }
    /**
     * @param $entities
     */
    public function _setTargets($entities)
    {
        $this->targets = $entities;
    }
    public function setTargetsArray($obj)
    {
        $entities = array();
        foreach ($obj ?: array() as $optionObj) {
            $entity = null;
            if ($optionObj->type == 'user') {
                $entity = Engine::factory()->getUser($optionObj->id, false, true);
                if ($entity === null) {
                    continue;
                }
            } else if ($optionObj->type == 'group') {
                $entity = Engine::factory()->getGroup($optionObj->id, true);
                if ($entity === null) {
                    continue;
                }
            } else if ($optionObj->type == 'column') {
                $table = $this->parent;
                $entity = $table->getColumnFor($optionObj->id, true);
                if ($entity === null) {
                    continue;
                }
            } else {
                assert(false);
            }

            $entities[] = $entity;
        }
        $this->targets = $entities;
    }

    public function getTargetsArray()
    {
        $this_ = $this;
        $entities = $this->getTargets();
        $targets = array_map(function($t) use ($this_) { /** @var $t Group|User|Column */
            if ($this_->getTargetingType() == NotificationRule::TARGETING_COLUMN)
                return array('id'=>$t->getId(), 'type'=>'column');
            else
                return array('id'=>$t->getId(), 'type'=>($t instanceof User ? 'user' : 'group'));
        }, $entities);
        return $targets;
    }

    /**
     * @return Message[]
     */
    public function validate()
    {
        if ($this->getTargetingType() == self::TARGETING_COLUMN
            && count($this->getTargets()) > 1) {
                assert(false);
        }
        if (count($this->getTargets()) == 0) {
            return array(new Message(
                ':table_modify.notification.targets.required', array(), $this));
        }
        return array();
    }
}
