<?php
namespace TxCore;

use \ArrayAccess;
use \ArrayIterator;
use \Countable;
use \IteratorAggregate;

/**
 * Search result.
 * @see IteratorAggregate
 */
class List_ implements IteratorAggregate, ArrayAccess, Countable 
{
	private $rows = array();
	private $offset = 0;
	private $rowLimit = 0;
	private $totalRowCount = 0;
	private $columns = array();
	private $type;

    /**
     * @var Table
     */
	private $table;
	
	/**
	 * (non-PHPdoc)
	 * @see IteratorAggregate::getIterator()
	 * @return \Iterator
	 */
	public function getIterator()
	{
		return new ArrayIterator($this->rows);
	}

	public function offsetExists($offset)  { return isset($this->rows[$offset]); }
	public function offsetGet($offset)  
	{ 
		if (!isset($this->rows[$offset])) {
			throw new TxCoreException(sprintf("Undefined offset %d", $offset));
		}
		return $this->rows[$offset]; 
	}
	public function offsetSet($offset, $value) {}
	public function offsetUnset($offset) {}
	public function count() { return count($this->rows); } 
	
	/**
	 * @return Row[]
	 */
	public function &getRows()
	{
		return $this->rows;
	}
	/** @return Row */
	public function &getRow($offset)
	{
		return $this->rows[$offset];
	}
	public function _setRows($rows)
	{
		$this->rows = $rows;
	}
	public function getRowCount() 
	{
		return count($this->rows);
	}
	public function getOffset() 
	{
		return $this->offset;
	}
	public function _setOffset($value)
	{
		$this->offset = Util::toInt($value);
	}
	/** @return int */
	public function getTotalRowCount()
	{
		return $this->totalRowCount;
	}
	public function _setTotalRowCount($value)
	{
		$this->totalRowCount = Util::toInt($value);
	}

    /**
     * @return Column[]
     */
	public function getColumns()
	{
		return $this->columns;
	}
	public function getRowLimit()
	{
		return $this->rowLimit;
	}

    /**
     * @param int $offset
     * @return Column
     */
	public function getColumn($offset)
	{
		return $this->columns[$offset];
	}
	
	public function _setColumns($columns)
	{
		$this->columns = $columns;
	}
	public function _setType($value)
	{
		$this->type = $value;
	}
	public function _setRowLimit($value)
	{
		$this->rowLimit = Util::toInt($value);
	}
	public function getType()
	{
		return $this->type;
	}
	public function _setTable($value)
	{
		$this->table = $value;
	}

    /**
     * @return Table
     */
	public function getTable()
	{
		return $this->table;
	}
}



