<?php
namespace TxCore;

use \ArrayAccess;
use \ArrayIterator;
use \Countable;
use \IteratorAggregate;

/**
 */
class UserSearchCriteria
{
    private $keyword;
    private $inEntities;

    /**
     * @return mixed
     */
    public function getKeyword()
    {
        return $this->keyword;
    }

    /**
     * @param mixed $keyword
     */
    public function setKeyword($keyword)
    {
        $this->keyword = $keyword;
    }

    /**
     * @return mixed
     */
    public function getInEntities()
    {
        return $this->inEntities;
    }

    /**
     * @param mixed $inEntities
     */
    public function setInEntities($inEntities)
    {
        $this->inEntities = $inEntities;
    }
    //private $groupOrUser;

}



