<?php
namespace TxCore;

use TxCore\Engine;
use TxCore\Column;


/**
 * Table definition.
 */
class Template
{
	private $id;
	private $name;
	private $description;
	private $icon;
	private $isPublic;
	private $isDefault;
	private $roundScale;
	private $roundType;
	private $revision;
	private $permissionControl;
// 	private $columns = null;
// 	private $viewColumns = null;

	public function __construct($id) 
	{
		$this->_setId($id);
	}
	
	public function _setId($value)
	{
		$this->id = Util::toInt($value);
	}
	public function getId()
	{
		return $this->id;
	}
	public function setName($value)
	{
		$this->name = $value;
	}
	public function getName()
	{
		return $this->name;
	}
	public function setDescription($value)
	{
		$this->description = $value;
	}
	public function getDescription()
	{
		return $this->description;
	}
	/** @return Icon */
	public function getIcon()
	{
		return $this->icon;
	}
	public function setIcon(Icon $value = null)
	{
		$this->icon = $value;
	}
	public function isPublic()
	{
		return $this->isPublic;
	}
	public function setPublic($value)
	{
		$this->isPublic = $value;
	}
	public function isDefault()
	{
		return $this->isDefault;
	}
	public function setDefault($value)
	{
		$this->isDefault = $value;
	}
	public function getRoundScale()
	{
		return $this->roundScale;
	}
	public function setRoundScale($value)
	{
		$this->roundScale = $value;
	}
	public function getRoundType()
	{
		return $this->roundType;
	}
	public function setRoundType($value)
	{
		$this->roundType = $value;
	}
	public function _setRevision($value)
	{
		$this->revision = $value;
	}
	public function getRevision()
	{
		return $this->revision;
	}
	public function getPermissionControl()
	{
		return $this->permissionControl;
	}
	public function setPermissionControl($value)
	{
		$this->permissionControl = $value;
	}
	/**
	 * @param User $by
	 * @return Table New created table.
	 */
	public function createTable(User $by) 
	{
		return Engine::factory()->createTableFromTemplate($this, $by);
	}
// 	public function getIconUrl()
// 	{
// 		$baseUrl = get_config('app', 'icon_dir_url');
// 		return $this->iconFilekey ? concat_path($baseUrl, $this->iconFilekey) : null;
// 	}
// 	/**
// 	 * @return array of Column
// 	 */
// 	public function &getColumns()
// 	{
// 		if ($this->columns === null) {
// 			if ($this->id !== null) {
// 				$table = new Table();
// 				$table->_setId($this->id);
// 				$this->columns = Engine::factory()->getTableColumns($table);
// 			} else {
// 				$this->columns = array();
// 			}
// 		}
// 		return $this->columns;
// 	}

// 	/**
// 	 * @return array of Column
// 	 */
// 	public function &getViewColumns()
// 	{
// 		if ($this->viewColumns === null) {
// 			if ($this->id !== null) {
// 				$table = new Table();
// 				$table->_setId($this->id);
// 				$this->viewColumns = Engine::factory()->getViewColumns($table);
// 			} else {
// 				$this->viewColumns = array();
// 			}
// 		}
// 		return $this->viewColumns;
// 	}
	
	
}

