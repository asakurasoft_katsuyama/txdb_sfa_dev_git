<?php
namespace TxCore;

use TxCore\Condition;
use TxCore\Engine;
use TxCore\Table;

class Criteria
{
	const OR_ = 'or';
	const AND_ = 'and';
	const FN_SUM = 'sum';
	const FN_AVG = 'avg';
	const FN_MAX = 'max';
	const FN_MIN = 'min';
	const FN_COUNT = 'count';	
	const UNSEL = null;	// 未選択
	
	const EQUALS = '=';
	const NOT_EQUALS = '!=';
	const LIKE = 'like';
	const NOT_LIKE = 'not like';
	
	const GREATER_EQUALS = '>=';
	const LESS_EQUALS = '<=';
	const GREATER = '>';
	const LESS = '<';
	const CONTAINS = 'contains';
	const NOT_CONTAINS = 'not contains';

	private $offset = null;
	private $limit = null;
	private $boolOperator = self::AND_;
	protected $conditions = array();
	public $sort = array();
	private $subtotal;
	private $aggregate = null;

// 	/** @var Filter */
// 	private $filter;
	
	/** @var array */
	private $listColumns = array();

	/**
	 * @param Column[] $columns
	 */
	public function setListColumns(array $columns)
	{
		array_each($columns, function($column) {
			if (!$column->isListable()) {
	 			throw new TxCoreException(
					sprintf("Invalid list column '%s' was specified. (is not listable)", $column->getType()),
					TxCoreException::INTERNAL);
			}
		});
		
		$this->listColumns = $columns;
	}

	/**
	 * @return Column[]
	 */
	public function getListColumns()
	{
		return $this->listColumns;
	}
	public function addCondition(Column $column, $operator, $value) 
	{
		if (!$column->isSearchable($operator)) {
			throw new TxCoreException(
				sprintf("Invalid criterion column %s was specified. (is not searchable or unsupported operator type '%s')", 
					$column->getType(), $operator),
				TxCoreException::INTERNAL);
		}
// 		if (in_array($operator, array(self::LIKE, self::NOT_LIKE, self::LESS, self::GREATER), true) && is_empty($value)) {
// 			throw new TxCoreException(sprintf("Invalid value was specified."),
// 				TxCoreException::INTERNAL); 
// 		}
// 		if (in_array($operator, array(self::CONTAINS,self::NOT_CONTAINS), true) && 
// 			(!is_array($value) || is_empty($value))) {
// 			throw new TxCoreException(sprintf("Invalid value was specified (contains/not contains must be array)."),
// 				TxCoreException::INTERNAL); 
// 		}
		if (in_array($operator, array(self::CONTAINS,self::NOT_CONTAINS), true) && !is_array($value)) {
			throw new TxCoreException(sprintf("Invalid value was specified (contains/not contains must be array)."),
				TxCoreException::INTERNAL);
		}
		$this->conditions[] = new Condition($column, $operator, $value);
	}
	public function equals(Column $column, $value)
	{
		$this->addCondition($column, self::EQUALS, $value);
	}
	public function like(Column $column, $value)
	{
		$this->addCondition($column, self::LIKE, $value);
	}	
	public function notLike(Column $column, $value)
	{
		$this->addCondition($column, self::NOT_LIKE, $value);
	}
	public function notEquals(Column $column, $value)
	{
		$this->addCondition($column, self::NOT_EQUALS, $value);
	}	
	public function notContains(Column $column, $value)
	{
		$this->addCondition($column, self::NOT_CONTAINS, $value);
	}
// 	public function setFilter(Filter $filter)
// 	{
// 		$this->filter = $filter;
// 	}
	/**
	 * @return \TxCore\Condition[]
	 */
	public function getConditions()
	{
		return $this->conditions;
	}
	public function _setConditions($conditions = array())
	{
		$this->conditions = $conditions;
	}
	public function setBoolOperator($boolOperator) 
	{
		$this->boolOperator = $boolOperator;
	}
	public function getBoolOperator()
	{
		return $this->boolOperator;
	}
	public function setSubtotal(Column $groupedColumn, $func, $aggregateColumn = null) 
	{
		$this->subtotal = new Subtotal($groupedColumn, $func, $aggregateColumn);
	}
	/**
	 * @return \TxCore\Subtotal
	 */
	public function getSubtotal()
	{
		return $this->subtotal;
	}

	public function setAggregate($aggregation = null)	
	{
		$this->aggregate = $aggregation;
	}
	public function getAggregate()
	{
		return $this->aggregate;
	}

	public function addSort(Column $column, $reverse = false)
	{
		if (!$column->isSortable()) {
			throw new TxCoreException(
				sprintf("Invalid sort column '%s' was specified. (is not sortable)", $column->getType()),
				TxCoreException::INTERNAL);
		}
				
		$this->sort[] = new Sort($column, $reverse);
		return $this;
	}
	
	/**
	 * @return Sort[]
	 */
	public function getSort()
	{
		return $this->sort;
	}
	
// 	public function setLimit($limit, $offset = 0)
// 	{
// 		$this->limit = $limit;
// 		$this->offset = $offset;
// 		return $this;		
// 	}
// 	/** @param Table|int $table Table or table id */
// 	public function setTableId($table)
// 	{
// 		$this->Table = Engine::factory()->getTable($table);
// 		return $this;
// 	}
// 	public function setTable($table)
// 	{
// 		$this->Table = $table;
// 		return $this;
// 	}
	
// 	public function getLimit() 
// 	{
// 		return $this->limit;
// 	}
// 	public function getOffset()
// 	{
// 		return $this->offset;
// 	}
	
	/**
	 * @return Message[]
	 */
	public function validate() 
	{
		$messages = array();
		foreach ($this->conditions as $condition) {	/* @var $condition Condition */
			$messages = array_merge($messages, $condition->getColumn()->handleValidateSearch($condition));
		}
		return $messages;
	}
// 	/** @return Filter*/
// 	public function getFilter()
// 	{
// 		return $this->filter;
// 	}

	public function toArray() 
	{
		$conditions = $this->_getConditionsArray();
		$sort = array();
		foreach ($this->getSort() as $term) {
			$sort[] = array(
				'column' => array('id'=>$term->getColumn()->getId()),
				'isReverse' => $term->isReverse(),
			);
		}
		return array(
			'boolOperator' => $this->getBoolOperator(),
			'conditions' => $conditions,
			'sort' => $sort,
		);
	}

	protected function _getConditionsArray()
	{
		$conditions = array();
		foreach ($this->getConditions() as $condition) {
			$column = $condition->getColumn();
			$value = $condition->getValue();
			if ($column->hasOptions()) {
				$value = array_map(function($t) {	/** @var $t Option */
					if (is_empty($t)) 
						return null;	// 未選択
					else
						return array('id'=>$t->getId());
				}, $value);
			} else if ($column->getType() == Column::CREATED_BY ||
					$column->getType() == Column::UPDATED_BY) {
				$value = array_map(function($t) {	/** @var $t User */
					if (is_empty($t))
						return null;	// 未選択
					else
						return array('id'=>$t->getId());
				}, $value);
			} else if ($column->getType() == Column::USER_SELECTOR) {
				$value = array_map(function($t) {	/** @var $t Group|User */
					if ($t === null || is_empty($t->getId()))
						return array('id'=>null, 'type'=>'user');
					else
						return array('id'=>$t->getId(), 'type'=>($t instanceof User ? 'user' : 'group'));
				}, $value);
			}
			$conditions[] = array(
				'column' => array(
					'id'=>$condition->getColumn()->getId(),
					'type'=>$condition->getColumn()->getType()
				),
				'operator' => $condition->getOperator(),
				'value' => $value,
			);
		}
		return $conditions;
	}
	
	public static function fromArray($obj, Table $table)
	{
		$obj = is_string($obj) ? Util::decodeJSON($obj) : $obj;
		$criteria = new Criteria();
		if ($obj === null || is_empty($obj)) {
			return $criteria;
		}
		$criteria->setBoolOperator($obj->boolOperator);
		$criteria->_setConditionsFromArray($obj->conditions, $table);
		foreach ($obj->sort as $term) {
			$column = $table->getColumnFor($term->column->id, true);
			if ($column !== null) {
				$criteria->addSort($column, $term->isReverse);
			}
		}
		return $criteria;
	}

	protected function _setConditionsFromArray($conditions, Table $table)
	{
		$criteria = $this;
		foreach ($conditions as $term) {
			// Note : カラム変更があった場合、カラムが存在しない場合がありうる
			$column = $table->getColumnFor($term->column->id, true);
			if ($column !== null) {
				if ($column->hasOptions()) {
					$options = array();
					$hasRemoved = false;
					foreach ($term->value ?: array() as $optionObj) {
						if (is_empty($optionObj)) {
							$options[] = null;	// "未選択"
							continue;
						}
						$option = $column->getOptionFor($optionObj->id, true);
						if ($option === null) {
							$hasRemoved = true;
							continue;
						}
						$options[] = $option;
					}
					// オプションが削除されていてかつ、条件値が0件になる場合は、検索時にエラーになるので条件を削除
					if ($hasRemoved && is_empty($options))
						continue;
					$criteria->addCondition($column, $term->operator, $options);
				} else if ($column->getType() == Column::USER_SELECTOR) {
					$entities = array();
					$hasRemoved = false;
					foreach ($term->value ?: array() as $optionObj) {
						$entity = null;
						if ($optionObj->type == 'user') {
							if (is_empty($optionObj->id)) {// "未選択"
								$user = new User();
								$entities[] = $user;	// "未選択"
								continue;
							}
							$entity = Engine::factory()->getUser($optionObj->id, false, true);
							if ($entity === null) {
								$hasRemoved = true;
								continue;
							}
						} else if ($optionObj->type == 'group') {
							$entity = Engine::factory()->getGroup($optionObj->id, true);
							if ($entity === null) {
								$hasRemoved = true;
								continue;
							}
						} else {
							assert(false);
						}

						$entities[] = $entity;
					}
					// オプションが削除されていてかつ、条件値が0件になる場合は、検索時にエラーになるので条件を削除
					if ($hasRemoved && is_empty($entities))
						continue;
					$criteria->addCondition($column, $term->operator, $entities);
				} else if ($column->getType() == Column::CREATED_BY ||
					$column->getType() == Column::UPDATED_BY) {
					$users = array();
					$hasRemoved = false;
					foreach ($term->value as $userObj) {
						$user = Engine::factory()->getUser($userObj->id, false, true);
						if ($user === null) {
							$hasRemoved = true;
							continue;
						}
						$users[] = $user;
					}
					if ($hasRemoved && is_empty($users))
						continue;
					$criteria->addCondition($column, $term->operator, $users);
				} else {
					$criteria->addCondition($column, $term->operator, $term->value);
				}
			}
		}
	}
	
}
