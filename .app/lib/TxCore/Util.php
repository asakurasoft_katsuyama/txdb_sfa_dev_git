<?php
namespace TxCore;

use TxCore\Math\MathContext;

use TxCore\Math\Math;

use \DirectoryIterator;
use JSON\JSON;

class Util
{
	const DATE_FORMAT = "Y-m-d";
	const DATETIME_FORMAT = "Y-m-d H:i";
	const TIME_FORMAT = "H:i";

	/**
	 * 
	 * @param mixed $number
	 * @param mixed $ifNull
	 */
	public static function toBool($number, $ifNull = null)
	{
		if (is_bool($number)) return $number;
		return $number === null ? $ifNull : ($number == 1);
	}
	
	/**
	 * true/false -> 1/0
	 * 
	 * @param bool $bool
	 * @param mixed $ifNull
	 */
	public static function toBoolInt($bool, $ifNull = null)
	{
		return $bool === null ? $ifNull : ($bool ? 1 : 0);
	}
	
	public static function toInt($str, $ifNull = null)
	{
		return ($str === null || $str === '') ? $ifNull : intval($str);
	}
	
	public static function generateApiKey()
	{
		return sha1( uniqid( mt_rand() , true ) );
	}
	
	public static function generateFileHash()
	{
		return sha1( uniqid( mt_rand() , true ) );
	}
	
	public static function getUniqeFileName($ext)
	{
		return sha1( uniqid( mt_rand() , true ) ) . '.'.$ext;
	}
	
	public static function encodeJSON($str)
	{
		return JSON::serialize($str);
// 		return json_encode($str);
	}
	
	public static function decodeJSON($str)
	{
		$ret= json_decode($str);
		if (json_last_error() !== JSON_ERROR_NONE) {
			throw new TxCoreException(sprintf('JSON parse errror : %s ...', substr($str, 0, 100)));
		}
		return $ret;
	}

	public static function formatDate($date_str, $expression = false)
	{
		if (is_empty($date_str))
			return $date_str;
		if ($expression && $date_str === 'now') {
			$date_str = Engine::factory()->getNow(self::DATE_FORMAT);
		}
		return date(self::DATE_FORMAT, is_int($date_str) ? $date_str : strtotime($date_str));
	}
	
	public static function formatDateTime($date_str, $expression = false)
	{
		if (is_empty($date_str))
			return $date_str;
		if ($expression && $date_str === 'now') {
			$date_str = Engine::factory()->getNow(self::DATETIME_FORMAT);
		}
		return date(self::DATETIME_FORMAT, is_int($date_str) ? $date_str : strtotime($date_str));
	}
	
	public static function formatTime($date_str, $expression = false)
	{
		if (is_empty($date_str))
			return $date_str;
		if ($expression && $date_str === 'now') {
			$date_str = date(self::TIME_FORMAT, Engine::factory()->getNow());
		}
// 		return $date_str;
		return date(self::TIME_FORMAT, is_int($date_str) ? $date_str : strtotime($date_str, 0));
	}
	
	public static function formatHours($date, MathContext $context)
	{
		if (is_empty($date))
			return $date;
		return Math::bcround(bcdiv($date, 3600, 10), $context).'時間';
	}
	
	public static function formatDays($date, MathContext $context)
	{
		if (is_empty($date))
			return $date;
		return Math::bcround(bcdiv($date, 3600 * 24, 10), $context).'日';
	}
	
	public static function getIconUrl($filekey, $preset = false) 
	{
		if (is_empty($filekey))
			return null;
		$baseUrl = $preset ?
			get_config('app', 'preset_icon_dir_url') :
			get_config('app', 'user_icon_dir_url');
		return $filekey ? concat_path($baseUrl, $filekey) : null;
	}
	
	public static function copyPresetIcon($filekey)
	{
		if (is_empty($filekey))
			return null;
		$source = substr(concat_path(get_config('app', 'preset_icon_dir_url'), $filekey), 1);
		$dest = substr(concat_path(get_config('app', 'user_icon_dir_url'), $filekey), 1);
//		copy($internalPath, $dest);
		copy($source, $dest); // 書き損じ？
	}
	
	public static function isValidEmail($value) 
	{
		return preg_match('#^[\x01-\x7F]+@(([-a-z0-9]+\.)([-a-z0-9]+\.)*[a-z]+|\[\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}\])$#i', $value);
	}
	
	public static function isValidTelno($value)
	{
		return true;	// Throught anything.
	}
	
	public static function isValidUrl($value)
	{
		return preg_match('#^https?://#', $value);
	}
	public static function isValidLocalUrl($value)
	{
		return true;	// Throught anything.
	}
	public static function isValidDate($value, $expression = false)
	{
		if ($expression &&
			preg_match('#^(/?now)|(((\d+|last_day)/)?(last_month|current_month))|(/?current_year)$#i', $value)) {
			return true;
		}
		
		$matches = array();
		if (!preg_match('#^(\d{4})\-(\d{1,2})\-(\d{1,2})$#', $value, $matches)) {
			return false;
		}
		if (checkdate($matches[2], $matches[3], $matches[1]) === false) {
			return false;
		}
		if ($matches[1] < 1000) {
			return false;
		}
		return true;
	}
	public static function isValidTime($value)
	{
		$matches = array();
		if (!preg_match('#^(\d{1,2}):(\d{1,2})$#', $value, $matches)) {
			return false;
		}
		if (intval($matches[1]) >= 0 && intval($matches[1]) <= 23 && 
			intval($matches[2]) >= 0 && intval($matches[2]) <= 59) {
			return true;
		}
		return false;
	}
	
	public static function isValidDateTime($value, $expression = false)
	{
		if ($expression && 
			preg_match('#^(/?now)|(/?today)|(((\d+|last_day)/)?(last_month|current_month))|(/?current_year)$#i', $value)) {
			return true;
		}
		$parts = explode(' ', $value);
		if (count($parts) !== 2) {
			return false;
		}
		return self::isValidDate($parts[0]) && 
			self::isValidTime($parts[1]);
	}
	
	/**
	 * Normalize keywords for index. 
	 * @param sting $str
	 * @param boolean $asHtml
	 * @return string
	 */
	public static function normalizeKeyword($str, $asHtml = false) 
	{
// 	   *a	全角英数字を半角英数字に変換する
// 		A	半角英数字を全角英数字に変換する
// 	   *c	全角カタカナを全角ひらがなに変換する
// 	    C	全角ひらがなを全角カタカナに変換する
// 		k	全角カタカナを半角カタカナに変換する
// 	    K	半角カタカナを全角カタカナに変換する
// 		h	全角ひらがなを半角カタカナに変換する
//     *H	半角カタカナを全角ひらがなに変換する
// 		n	全角数字を半角数字に変換する
// 		N	半角数字を全角数字に変換する
// 		r	全角英文字を半角英文字に変換する
// 		R	半角英文字を全角英文字に変換する
// 		n	全角数字を半角数字に変換する
// 		N	半角数字を全角数字に変換する
// 	    s	全角スペースを半角スペースに変換する (U+3000 → U+0020)
// 		S	半角スペースを全角スペースに変換する (U+0020 → U+3000)
// 	   *V	濁点つきの文字を１文字に変換する (K、H と共に利用する）
	
		// タグを除去
		if ($asHtml) {
			$str = strip_tags($str);
		}
		// 改行を削除
		$str = preg_replace('/[\r\n\t\s\x3000]/u', '', $str);
		// 可視文字が残っていなければ未入力扱い（例: "<p></p>"）
		if (is_empty($str))
			return null;
		// 文字を正規化
		$str = mb_convert_kana($str, "HVcas");
		// ABC -> abc
		$str = strtolower($str);
		return $str;
	}
	
	public static function escapeLike($value)
	{
		$value = str_replace('%', '\%', $value);
		$value = str_replace('_', '\_', $value);
		return $value;
	}

	public static function formatNumber($value)
	{
		$unit = 3;
		
		if (is_empty($value))
			return $value;
		$dp = strpos($value, '.');
		$decimalPart = '';
		if ($dp !== false) {
			$decimalPart = substr($value, $dp);
			$value = substr($value, 0, $dp);
		}
		$foramted = "";
		$p = 0;
		$is_minus = $value[0] == '-';
		$p_end = $is_minus ? 1 : 0;
		for ($i = strlen($value) - 1; $i >= $p_end; $i--) {
			if ($p > 0 && $p % $unit == 0)
				$foramted = "," . $foramted;
			$foramted = $value[$i] . $foramted;
			$p++;
		}
		if ($is_minus)
			$foramted =  '-'.$foramted;
		
		return $foramted. $decimalPart;
	}
	
	public static function isValidNumericRange($value) 
	{
		return preg_match('/^[+-]?(\d{0,20})?(\.\d{1,10})?$/', $value);
	}
	
	public static function toUnixTimestamp($str, $offset = false) 
	{
		if (is_empty($str))
			return null;
// 		return strtotime($str, 0) - ($offset ? strtotime('1970/01/01') : 0);
		return strtotime($str, 0); 
	}
	
// 	public static function toSecondsInDay($str) 
// 	{
// 		if (is_empty($str))
// 			return null;
// 		return strtotime($str) - strtotime('today');
// 	}

	public static function removeFiles($dirpath, $removeDir = false) 
	{
		$dir = new DirectoryIterator($dirpath);
		foreach ($dir as $fileinfo) {
			if ($fileinfo->isFile() || $fileinfo->isLink()) {
				unlink($fileinfo->getPathName());
			} elseif (!$fileinfo->isDot() && $fileinfo->isDir()) {
				self::removeFiles($fileinfo->getPathName(), true);
			}
		}
		if ($removeDir) 
			rmdir($dirpath);
	}
	
	public static function makeDirs($dir, $mode = 0777, $recursive = true) 
	{
		if( is_null($dir) || $dir === "" ){
			return false;
		}
		if( is_dir($dir) || $dir === "/" ){
			return true;
		}
		if( self::makeDirs(dirname($dir), $mode, $recursive) ){
			return mkdir($dir, $mode);
		}
		return false;
	}
	
	public static function isJSONRequest($url)
	{
		return str_ends_with('.json', $url) ||
			strpos($url, '.json?') !== false;
	}
	
	
}
