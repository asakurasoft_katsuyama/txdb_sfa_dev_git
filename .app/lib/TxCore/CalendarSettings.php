<?php
namespace TxCore;

/**
 * Calendar view settings.
 */
class CalendarSettings
{
	/**
	 * @var Table
	 */
	private $parent;


	/**
	 * @var int
	 */
	private $dateColumnId;

	/**
	 * @var int
	 */
	private $userColumnId;

	/**
	 * @var int
	 */
	private $startTimeColumnId;

	/**
	 * @var int
	 */
	private $endTimeColumnId;

	/**
	 * @var int
	 */
	private $titleColumnId;

	/**
	 * @param Table $parent
	 */
	public  function __construct($parent = null)
	{
		$this->parent = $parent;
	}

	/**
	 * @return int
	 */
	public function getDateColumnId()
	{
		return $this->dateColumnId;
	}

	/**
	 * @return Column
	 */
	public function getDateColumn()
	{
		return $this->parent->getColumnFor($this->dateColumnId);
	}

	/**
	 * @param int $id
	 */
	public function setDateColumnId($id)
	{
		$this->dateColumnId = Util::toInt($id);
	}

	/**
	 * @return int
	 */
	public function getUserColumnId()
	{
		return $this->userColumnId;
	}

	/**
	 * @return Column
	 */
	public function getUserColumn()
	{
		return $this->parent->getColumnFor($this->userColumnId);
	}

//	/**
//	 * @return Column
//	 */
//	public function getUserColumn()
//	{
//		//return $this->userColumn;
//	}

	/**
	 * @param Column $id
	 */
	public function setUserColumnId($id)
	{
		$this->userColumnId = Util::toInt($id);
	}

	/**
	 * @return int
	 */
	public function getStartTimeColumnId()
	{
		return $this->startTimeColumnId;
	}

	/**
	 * @param Column $id
	 */
	public function setStartTimeColumnId($id)
	{
		$this->startTimeColumnId = Util::toInt($id);
	}

	/**
	 * @return int
	 */
	public function getEndTimeColumnId()
	{
		return $this->endTimeColumnId;
	}

	/**
	 * @param Column $id
	 */
	public function setEndTimeColumnId($id)
	{
		$this->endTimeColumnId = Util::toInt($id);
	}

	/**
	 * @return int
	 */
	public function getTitleColumnId()
	{
		return $this->titleColumnId;
	}

	/**
	 * @return Column
	 */
	public function getTitleColumn()
	{
		return $this->parent->getColumnFor($this->titleColumnId);
	}

	/**
	 * @param Column $id
	 */
	public function setTitleColumnId($id)
	{
		$this->titleColumnId = Util::toInt($id);
	}

    /**
     * @return Column|null
     */
    public function getStartTimeColumn()
    {

        return $this->startTimeColumnId ?
            $this->parent->getColumnFor($this->startTimeColumnId) : null;
    }

    /**
     * @return Column|null
     */
    public function getEndTimeColumn()
    {
        return $this->endTimeColumnId ?
            $this->parent->getColumnFor($this->endTimeColumnId) : null;
    }

}

