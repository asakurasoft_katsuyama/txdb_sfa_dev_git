<?php

namespace TxCore;

class NotificationBasicRule extends NotificationRule
{

    protected $isOnRowInsert = false;
    protected $isOnRowUpdate = false;
    protected $isOnCommentInsert = false;

    public static $messages = array(
        'row_insert' => 'データが登録されました。',
        'row_update' => 'データが更新されました。',
        'comment_insert' => 'コメントが書き込まれました。',
    );

    public static function getMessage($action = null)
    {
        if (!isset(self::$messages[$action])) {
            assert(false);
        }
        return self::$messages[$action];
    }

    public function isOnRowInsert()
    {
        return $this->isOnRowInsert;
    }
    public function setOnRowInsert($value)
    {
        $this->isOnRowInsert = $value;
    }

    public function isOnRowUpdate()
    {
        return $this->isOnRowUpdate;
    }
    public function setOnRowUpdate($value)
    {
        $this->isOnRowUpdate = $value;
    }

    public function isOnCommentInsert()
    {
        return $this->isOnCommentInsert;
    }
    public function setOnCommentInsert($value)
    {
        $this->isOnCommentInsert = $value;
    }

}
