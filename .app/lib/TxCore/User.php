<?php

namespace TxCore;

use TxCore\Engine;
use TxCore\User;

/**
 * User interface.
 */
class User  
{
	const ADMIN = "admin";
	const USER = "user";
	
	private $id;
	private $role;
//	public $isSuper = false;
	private $screenName;
	private $loginId;
	private $profile;
	private $seq;
	private $password;
	private $deleted;

	private $notificationCount = 0;

// 	const LOGIN_ID_CHARACTERS = "/^[\\x01-\\x7F]+@(([-a-z0-9]+\\.)([-a-z0-9]+\\.)*[a-z]+|\\[\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\.\\d{1,3}\\])$/i";
	const LOGIN_ID_CHARACTERS = "/^[0-9A-Za-z@\\!\\.\\,<>:;_\\=\\*\\+\\-\\/\\?\\#\\$\\%\\&\\'\\(\\)\\‾\\^\\`\\\\\\|\\[\\]\\{\\}]+$/";
	const PASSWORD_CHARACTERS = "/^[0-9A-Za-z@\\!\\.\\,<>:;_\\=\\*\\+\\-\\/\\?\\#\\$\\%\\&\\'\\(\\)\\‾\\^\\`\\\\\\|\\[\\]\\{\\}]+$/";

// 	/**
// 	 * バッチなどで使う特権ユーザーを作成する.
// 	 */
// 	public static function getSystemUser() 
// 	{
// 		$user = new self();
// 		$user->setRole(self::ADMIN);
// 		$user->setScreenName('SYSTEM');
// 		$user->_setId(0);
// 		return $user;
// 	}
	
	public function _setId($value)
	{
		$this->id = Util::toInt($value);
	}
	public function getId() 
	{
		return $this->id;
	}
	public function setRole($value)
	{
		$this->role = $value;
	}
	public function getRole()
	{
		return $this->role;
	}
	public function isAdmin()
	{
		return $this->role == self::ADMIN;
	}
	public function setPassword($value) 
	{
		$this->password = $value;
	}
	public function getPassword() 
	{
		return $this->password;
	}
	public function setLoginId($value) 
	{
		$this->loginId = $value;
	}
	public function getLoginId() 
	{
		return $this->loginId;
	}
	public function setProfile($value) 
	{
		$this->profile = $value;
	}
	public function getProfile() 
	{
		return $this->profile;
	}
	public function setSeq($value) 
	{
		$this->seq = $value;
	}
	public function getSeq() 
	{
		return $this->seq;
	}
	public function setScreenName($value)
	{
		$this->screenName = $value;
	}
	public function getScreenName()
	{
		return $this->screenName;
	}
	public function create(User $by)
	{
		Engine::factory()->createUser($this, $by);
	}
	public function createOrReplace(User $by)
	{
		Engine::factory()->createOrReplaceUser($this, $by);
	}
	public function update(User $by)
	{
		Engine::factory()->updateUser($this, $by);
	}
	public function delete(User $by)
	{
		Engine::factory()->deleteUser($this, $by);
	}
	
	public static function validateLoginId($value) 
	{
		return preg_match(self::LOGIN_ID_CHARACTERS, $value) > 0;
	}
	
	public static function validatePassword($value) {
		return preg_match(self::PASSWORD_CHARACTERS, $value) > 0;
	}

	/**
	 * @return mixed
	 */
	public function _isDeleted()
	{
		return $this->deleted;
	}

	/**
	 * @param mixed $deleted
	 */
	public function _setDeleted($deleted)
	{
		$this->deleted = $deleted;
	}

	public function validate()
	{
		$messages = array();
		if (is_empty($this->getScreenName())) {
			$messages[] = new Message('表示名を入力してください。');
		} else if (mb_strlen($this->getScreenName()) > 64) {
			$messages[] = new Message('表示名は64文字以内で入力してください。');
		}
		if (is_empty($this->getLoginId())) {
			$messages[] = new Message('ログインidを入力してください。');
		} else if (mb_strlen($this->getLoginId()) > 64) {
			$messages[] = new Message('ログインidは64文字以内で入力してください。');
		} else if (!User::validateLoginId($this->getLoginId())) {
			$messages[] = new Message('ログインidに使用できない文字が含まれています。');
		}
		if (is_empty($this->getPassword())) {
			$messages[] = new Message('パスワードを入力してください。');
		} else if (mb_strlen($this->getPassword()) > 64) {
			$messages[] = new Message('パスワードは64文字以内で入力してください。');
		} else if (!User::validatePassword($this->getPassword())) {
			$messages[] = new Message('パスワードに使用できない文字が含まれています。');
		}
		if (mb_strlen($this->getProfile()) > 100) {
			$messages[] = new Message('所属等は100文字以内で入力してください。');
		}
		if (is_not_empty($this->getSeq())) {
			if (preg_match('/^[0-9]+$/', $this->getSeq()) < 1) {
				$messages[] = new Message('表示順は数字で入力してください。');
			}
		}
		return $messages;
	}

}
