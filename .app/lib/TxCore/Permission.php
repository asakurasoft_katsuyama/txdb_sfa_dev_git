<?php

namespace TxCore;

/**
 * Permission interface.
 */
class Permission
{
	private $tableId;
	private $revision;
	private $userId;
	private $isReadable = false;
	private $isWritable = false;
	
	
	public function setTableId($value)
	{
		$this->tableId = Util::toInt($value);
	}
	
	public function getTableId() 
	{
		return $this->tableId;
	}
	
	public function setRevision($value)
	{
		$this->revision = $value;
	}
	
	public function getRevision()
	{
		return $this->revision;
	}
	
	public function setUserId($value) 
	{
		$this->userId = Util::toInt($value);
	}
	
	public function getUserId() 
	{
		return $this->userId;
	}
	
	public function setIsReadable($value) 
	{
		$this->isReadable = Util::toBool($value);
	}
	
	public function getIsReadable() 
	{
		return $this->isReadable;
	}
	
	public function setIsWritable($value) 
	{
		$this->isWritable = Util::toBool($value);
	}
	
	public function getIsWritable() 
	{
		return $this->isWritable;
	}
	
	
}
