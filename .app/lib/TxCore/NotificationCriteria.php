<?php
namespace TxCore;

class NotificationCriteria extends Criteria
{
	public function addCondition(Column $column, $operator, $value) 
	{
		if (!$column->hasNotificationConditionalOperator($operator)) {
			throw new TxCoreException(
				sprintf("Invalid criterion column %s was specified. (is not searchable or unsupported operator type '%s')", 
					$column->getType(), $operator),
				TxCoreException::INTERNAL);
		}
		if (in_array($operator, array(self::CONTAINS,self::NOT_CONTAINS), true) && !is_array($value)) {
			throw new TxCoreException(sprintf("Invalid value was specified (contains/not contains must be array)."),
				TxCoreException::INTERNAL);
		}
		$this->conditions[] = new Condition($column, $operator, $value);
	}

	public function toArray()
	{
		$conditions = $this->_getConditionsArray();
		return array(
			'boolOperator' => $this->getBoolOperator(),
			'conditions' => $conditions,
		);
	}
	
	public static function fromArray($obj, Table $table)
	{
		$obj = is_string($obj) ? Util::decodeJSON($obj) : $obj;
		$criteria = new NotificationCriteria();
		if ($obj === null || is_empty($obj)) {
			return $criteria;
		}
		$criteria->setBoolOperator($obj->boolOperator);
		$criteria->_setConditionsFromArray($obj->conditions, $table);
		return $criteria;
	}

	public function matches(Row $row)
	{
		$conditions = $this->getConditions();
		if (empty($conditions)) {
			return false;
		}

		$andBind = $this->getBoolOperator() == self::AND_;

		foreach ($this->getConditions() as $condition) {	/* @var $cond \TxCore\Condition */
			$column = $condition->getColumn();
			$matched = $column->handleValueMatch($condition, $row);
			if ($andBind) {
				if (!$matched) {
					return false;
				}
			} else {
				if ($matched) {
					return true;
				}
			}
		}

		return $andBind ? true : false;
	}

}
