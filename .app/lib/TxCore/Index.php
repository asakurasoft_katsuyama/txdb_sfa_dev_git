<?php
namespace TxCore;

class Index 
{
	const NUMBER = 'number';
	const STRING = 'string';
	const INTEGER = 'integer';
	const DATE = 'date';
	const DATETIME = 'datetime';
	const TIME = 'time';
	
	/**
	 * @var string
	 */
	private $type;
	
	/**
	 * @var mixed
	 */
	private $value;
	
	/**
	 * 日付の式用
	 * @var boolean
	 */
	private $isRange = false;
	
	/**
	 * @param string $type
	 * @param mixed $value
	 * @param boolean $range
	 */
	public function __construct($type, $value, $range = false) 
	{
		$this->type = $type;
		$this->value = $value;
		$this->isRange = $range;
	}
	
	public function getType() 
	{
		return $this->type;
	}
	
	public function getValue() 
	{
		return $this->value;
	}
	
	public function isRange()
	{
		return $this->isRange;
	}
}
