<?php
namespace TxCore;

use TxCore\Engine;
use TxCore\Table;
use TxCore\Filter;

class Condition
{
	private $column = null;
	private $operator;
	private $value;
	
	public function __construct($column, $operator, $value) 
	{
		$this->column = $column;
		$this->operator = $operator;
		$this->value = $value;
	}
	
	/**
	 * @return Column
	 */
	public function getColumn() 
	{
		return $this->column;
	}

	/**
	 * @param Column $column
	 */
	public function _setColumn($column)
	{
		$this->column = $column;
	}

	/**
	 * @return string
	 */
	public function getOperator()
	{
		return $this->operator;
	}
	
	/**
	 * @return mixed
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * @param $value
	 */
	public function _setValue($value)
	{
		$this->value = $value;
	}


	public static function expandDateExpression($value, $now)
	{
		$matches = array();
		$value = strtolower($value);
		if (str_starts_with('/', $value)) {
			$value = substr($value, 1);
		}
		if ($value == 'now') {
			return date('Y-m-d', $now);
		} else if ($value == 'last_month') {
			return array(
				date('Y-m-01', strtotime('last month', $now)),
				date('Y-m-d', strtotime('last day of last month', $now))
			);
		} else if (preg_match('#^((?:\d+)|(?:last_day))/last_month$#i', $value, $matches)) {
			if (strtolower($matches[1]) == 'last_day') {
				return date('Y-m-d', strtotime('last day of last month', $now));
			} else {
				return date('Y-m-'.$matches[1], strtotime('last month', $now));
			}
		} else if ($value == 'current_month') {
			return array(
				date('Y-m-01', $now),
				date('Y-m-d', strtotime('last day of this month', $now))
			);
		} else if (preg_match('#^((?:\d+)|(?:last_day))/current_month$#i', $value, $matches)) {
			if (strtolower($matches[1]) == 'last_day') {
				return date('Y-m-d', strtotime('last day of this month', $now));
			} else {
				return date('Y-m-'.$matches[1], $now);
			}
		} else if ($value == 'current_year') {
			return array(
				date('Y-01-01', $now),
				date('Y-12-31', $now)
			);
		} else if (preg_match('/\d{4}\-\d{2}-\d{2}/', $value)) {
			return $value;
		} else {
			throw new TxCoreException(sprintf('Invalid search value for date : %s', $value),
				TxCoreException::INTERNAL);
		}
	}

	public static function expandDateTimeExpression($value, $now)
	{
		$matches = array();
		$value = strtolower($value);
		if (str_starts_with('/', $value)) {
			$value = substr($value, 1);
		}
		if ($value == 'now') {
			return array(
				date('Y-m-d H:i:00', $now),
				date('Y-m-d H:i:59', $now)
			);
		} else if ($value == 'today') {
			return array(
				date('Y-m-d 00:00:00', $now),
				date('Y-m-d 23:59:59', $now)
			);
		} else if ($value == 'last_month') {
			return array(
				date('Y-m-01 00:00:00', strtotime('last month', $now)),
				date('Y-m-d 23:59:59', strtotime('last day of last month', $now))
			);
		} else if (preg_match('#^((?:\d+)|(?:last_day))/last_month$#i', $value, $matches)) {
			if (strtolower($matches[1]) == 'last_day') {
				return array(
					date('Y-m-d 00:00:00', strtotime('last day of last month', $now)),
					date('Y-m-d 23:59:59', strtotime('last day of last month', $now))
				);
			} else {
				return array(
					date('Y-m-'.$matches[1].' 00:00:00', strtotime('last month', $now)),
					date('Y-m-'.$matches[1].' 23:59:59', strtotime('last month', $now))
				);
			}
		} else if ($value == 'current_month') {
			return array(
				date('Y-m-01 00:00:00', $now),
				date('Y-m-d 23:59:59', strtotime('last day of this month', $now))
			);
		} else if (preg_match('#^((?:\d+)|(?:last_day))/current_month$#i', $value, $matches)) {
			if (strtolower($matches[1]) == 'last_day') {
				return array(
					date('Y-m-d 00:00:00', strtotime('last day of this month', $now)),
					date('Y-m-d 23:59:59', strtotime('last day of this month', $now))
				);
			} else {
				return array(
					date('Y-m-'.$matches[1].' 00:00:00', $now),
					date('Y-m-'.$matches[1].' 23:59:59', $now)
				);
			}
		} else if ($value == 'current_year') {
			return array(
				date('Y-01-01 00:00:00', $now),
				date('Y-12-31 23:59:59', $now)
			);
		} else if (preg_match('/\d{4}\-\d{2}-\d{2} \d{2}:\d{2}/', $value)) {
			return $value.':00';
		} else {
			throw new TxCoreException(sprintf('Invalid search value for datedate : %s', $value),
				TxCoreException::INTERNAL);
		}
	}
}
