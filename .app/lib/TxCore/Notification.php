<?php

namespace TxCore;

class Notification
{
    private $id;
    private $notifiedOn;
    private $message;
    private $changes;
    private $notifierId;
    private $tableId;
    private $rowId;
    private $commentId;

    private $notifier;
    private $table;
    private $row;
    private $comment;

    public function getId()
    {
        return $this->id;
    }
    public function _setId($value)
    {
        $this->id = Util::toInt($value);
    }

    public function getNotifiedOn($asTimestamp = false)
    {
        if ($asTimestamp) {
            if ($this->notifiedOn == null) {
                return null;
            }
            return strtotime($this->notifiedOn);
        }
        return $this->notifiedOn;
    }
    public function _setNotifiedOn($value)
    {
        $this->notifiedOn = $value;
    }

    public function getMessage()
    {
        return $this->message;
    }
    public function setMessage($value)
    {
        $this->message = $value;
    }

    public function getChanges()
    {
        return $this->changes;
    }
    public function setChanges($value)
    {
        $this->changes = $value;
    }
    public function getChangesArray()
    {
        if ($this->getChanges() === null) {
            return;
        }
        $changes = array();
        foreach ($this->getChanges() as $change) {
            /* @var $changes RowColumnChange */
            $changes[] = $change->toArray();
        }
        return $changes;
    }
    public function setChangesFromArray($obj)
    {
        $obj = is_string($obj) ? Util::decodeJSON($obj) : $obj;
        if (empty($obj)) {
            $this->changes = null;
            return;
        }
        $changes = array();
        foreach ($obj as $change) {
            $changes[] = RowColumnChange::fromArray($change);
        }
        $this->changes = $changes;
    }

    public function getNotifier()
    {
        if ($this->notifierId !== null && $this->notifier === null) {
            $this->notifier = Engine::factory()->getUser($this->notifierId, false, true);
        }
        return $this->notifier;
    }
    public function _setNotifierId($value)
    {
        $this->notifierId = Util::toInt($value);
    }

    public function getTable()
    {
        if ($this->tableId !== null && $this->table === null) {
            $this->table = Engine::factory()->getTable($this->tableId, Engine::RELEASE, null, true);
        }
        return $this->table;
    }
    public function _setTableId($value)
    {
        $this->tableId = Util::toInt($value);
    }

    public function getRowId()
    {
        return $this->rowId;
    }
    public function getRow()
    {
        if ($this->rowId !== null && $this->row === null) {
            $this->row = Engine::factory()->getRow($this->getTable(), $this->rowId, null, false, true);
        }
        return $this->row;
    }
    public function _setRowId($value)
    {
        $this->rowId = Util::toInt($value);
    }

    public function getComment()
    {
        if ($this->commentId !== null && $this->comment === null) {
            $this->comment = Engine::factory()->getComment($this->commentId, true);
        }
        return $this->comment;
    }
    public function _setCommentId($value)
    {
        $this->commentId = Util::toInt($value);
    }

}
