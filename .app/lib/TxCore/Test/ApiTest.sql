--
-- APIのテストはデータをコミットします。
-- UT用のデータベースを作成して、env_ut テーブルを作成してください。
--
-- mysqldump -u root -p txdb | mysql -u root -p txdb_ut
-- mysql -u root -p CREATE TABLE env_ut (`dummy` varchar(1));
--
--
delete from env_ut;

delete from user;
delete from table_def;
delete from column_def;
delete from `reference`;
delete from `data`;
delete from `row`;
delete from `indexes`;
delete from `option`;
delete from `release_control`;
delete from `row_no`;
delete from `group`;
delete from `user_group`;
delete from `relationship`;

insert into `group` (id, name)
  values (11, 'グループ１');

insert into `user_group` (user_id, group_id)
  values (102, 11);

insert into user
  (id, role, login_id, password, screen_name, profile) values
  (101, 'admin', 'user1@sample.com', 'password', 'test user01', 'my company'),
  (102, 'user', 'user2@sample.com', 'password', 'test user02', 'my company')
;

insert into table_def
  (id, revision, name, description, icon_filekey, is_public, is_template, is_default, is_api_enabled, api_key, is_write_api_enabled, write_api_key, round_type, round_scale, owner) values
  (1001, 1, 'table 1', 'This is table 1', 'default.png', 1, 0, NULL, 1, 'APIKEY', 1, 'APIKEY', NULL, NULL, 101),
  (1002, 1, 'table 2', 'This is table 2', 'default.png', 1, 0, NULL, 0, 'APIKEY', 1, NULL, NULL, NULL, 101), -- API非公開
  (1003, 1, 'table 3', 'This is table 3', 'default.png', 1, 0, NULL, 1, 'APIKEY', 0, 'APIKEY', NULL, NULL, 101), -- 書込API非公開
  (1004, 1, 'table 4', 'This is table 4', 'default.png', 0, 0, NULL, 1, 'APIKEY', 1, 'APIKEY', NULL, NULL, 101), -- 非公開DB
  (1005, 1, 'table 5', 'This is table 5', 'default.png', 1, 0, NULL, 1, 'APIKEY', 1, 'APIKEY', NULL, NULL, 101)  -- 明細入力の参照先DB
;

insert into row_no (table_id, row_no) values
  (1001, 1),
  (1002, 0),
  (1003, 0),
  (1004, 0),
  (1005, 2)
;

insert into column_def
  (id, revision, table_id, column_type, code, label, properties) values
  (100101, 1, 1001, 'rownum', 'rownum_1', '行番号１', NULL),
  (100102, 1, 1001, 'created_by', 'created_by_1', '作成者１', NULL),
  (100103, 1, 1001, 'created_on', 'created_on_1', '作成日時１', NULL),
  (100104, 1, 1001, 'updated_by', 'updated_by_1', '更新者１', NULL),
  (100105, 1, 1001, 'updated_on', 'updated_on_1', '更新日時１', NULL),
  (100106, 1, 1001, 'text', 'text_1', '一行テキスト１', NULL),
  (100107, 1, 1001, 'dropdown', 'dropdown_1', 'ドロップダウン１', '{}'),
  (100108, 1, 1001, 'radio', 'radio_1', 'ラジオボタン１', '{"defaultValueId":"10010801"}'),
  (100109, 1, 1001, 'relational', 'relational_1', '関連レコード１', '{"referenceTableId":1,"referenceTargetColumnId":1,"referenceSourceColumnId":1,"referenceColumnIds":[1],"referenceSortColumnId":1,"referenceSortsReverse":false,"referenceMaxRow":30}'),
  (100110, 1, 1001, 'checkbox', 'checkbox_1', 'チェックボックス１', '{}'),
  (100111, 1, 1001, 'file', 'file_1', 'ファイル１', NULL),
  (100112, 1, 1001, 'user_selector', 'user_selector_1', 'ユーザー選択', NULL),
  (100113, 1, 1001, 'relational_input', 'relational_input_1', '明細入力１', '{"referenceTableId":1005,"referenceColumnIds":[100501],"referenceSortColumnId":100501,"referenceSortsReverse":false}'),
  (100201, 1, 1002, 'dropdown', 'dropdown_1', 'ドロップダウン１', NULL),
  (100202, 1, 1002, 'radio', 'radio_1', 'ラジオボタン１', NULL),
  (100203, 1, 1002, 'relational', 'relational_1', '関連レコード１', NULL),
  (100301, 1, 1003, 'dropdown', 'dropdown_1', 'ドロップダウン１', NULL),
  (100302, 1, 1003, 'radio', 'radio_1', 'ラジオボタン１', NULL),
  (100303, 1, 1003, 'relational', 'relational_1', '関連レコード１', NULL),
  (100401, 1, 1004, 'dropdown', NULL, 'ドロップダウン１', NULL),
  (100402, 1, 1004, 'radio', NULL, 'ラジオボタン１', NULL),
  (100403, 1, 1004, 'relational', NULL, '関連レコード１', NULL),
  (100501, 1, 1005, 'rownum', 'rownum_1', '行番号１', NULL),
  (100502, 1, 1005, 'created_by', 'created_by_1', '作成者１', NULL),
  (100503, 1, 1005, 'created_on', 'created_on_1', '作成日時１', NULL),
  (100504, 1, 1005, 'updated_by', 'updated_by_1', '更新者１', NULL),
  (100505, 1, 1005, 'updated_on', 'updated_on_1', '更新日時１', NULL),
  (100506, 1, 1005, 'text', 'text_1', '一行テキスト１', NULL)
;

insert into `reference`
  (column_id, revision, no, table_id, target_column_id, target_table_id) values
  (100113, 1, 1, 1001, 100501, 1005), -- 明細入力から参照
  (100113, 1, 2, 1001, 100501, 1005)  -- 明細入力から参照
;

insert into `option`
  (id, revision, table_id, column_id, name, seq) values
  (10010701, 1, 1001, 100107, 'dropdown_1_1', 1),
  (10010702, 1, 1001, 100107, 'dropdown_1_2', 2),
  (10010801, 1, 1001, 100108, 'radio_1_1', 1),
  (10010802, 1, 1001, 100108, 'radio_1_2', 2),
  (10011001, 1, 1001, 100110, 'checkbox_1_1', 1),
  (10011002, 1, 1001, 100110, 'checkbox_1_2', 2)
;

insert into `row`
  (id, table_id, row_no, version) values
  (10010001, 1001, 1, 1),
  (10050001, 1005, 1, 1),
  (10050002, 1005, 2, 1)
;

insert into `data`
  (row_id, column_id, table_id, value) values
  (10010001, 100101, 1001, 1),
  (10010001, 100102, 1001, 101),
  (10010001, 100103, 1001, '2014-01-01 00:00'),
  (10010001, 100104, 1001, 101),
  (10010001, 100105, 1001, '2014-01-01 00:00'),
  (10010001, 100106, 1001, '一行テキスト値１'),
  (10010001, 100107, 1001, '10010701'),
  (10010001, 100108, 1001, '10010801'),
  (10010001, 100110, 1001, '[10011001]'),
  (10010001, 100111, 1001, '[]'),
  (10010001, 100112, 1001, '[101, 102]'),
  (10050001, 100501, 1005, 1),
  (10050001, 100502, 1005, 101),
  (10050001, 100503, 1005, '2014-01-01 00:00'),
  (10050001, 100504, 1005, 101),
  (10050001, 100505, 1005, '2014-01-01 00:00'),
  (10050001, 100506, 1005, '一行テキスト値_10050001'),
  (10050002, 100501, 1005, 2),
  (10050002, 100502, 1005, 101),
  (10050002, 100503, 1005, '2014-01-01 00:00'),
  (10050002, 100504, 1005, 101),
  (10050002, 100505, 1005, '2014-01-01 00:00'),
  (10050002, 100506, 1005, '一行テキスト値_10050002')
;

insert into `relationship`
  (source_row_id, target_row_id, source_table_id, target_table_id, source_column_id) values
  (10010001, 10050001, 1001, 1005, 100113),
  (10010001, 10050002, 1001, 1005, 100113)
;


insert into `indexes`
  (id, table_id, column_id, string_value, integer_value, number_value, date_value, time_value, datetime_value, row_id) values
  (null, 1001, 100101, null, 1, null, null, null, null, 10010001),
  (null, 1001, 100102, null, null, null, null, null, '2014-01-01 00:00', 10010001),
  (null, 1001, 100103, null, 101, null, null, null, null, 10010001),
  (null, 1001, 100104, null, null, null, null, null, '2014-01-01 00:00', 10010001),
  (null, 1001, 100105, null, 101, null, null, null, null, 10010001),
  (null, 1001, 100106, '一行テキスト値１', null, null, null, null, null, 10010001),
  (null, 1001, 100107, '10010701', null, null, null, null, null, 10010001),
  (null, 1001, 100108, '10010801', null, null, null, null, null, 10010001),
  (null, 1001, 100110, '10011001', null, null, null, null, null, 10010001),
  (null, 1001, 100111, '', null, null, null, null, null, 10010001),
  (null, 1005, 100501, null, 1, null, null, null, null, 10050001),
  (null, 1005, 100502, null, null, null, null, null, '2014-01-01 00:00', 10050001),
  (null, 1005, 100503, null, 101, null, null, null, null, 10050001),
  (null, 1005, 100504, null, null, null, null, null, '2014-01-01 00:00', 10050001),
  (null, 1005, 100505, null, 101, null, null, null, null, 10050001),
  (null, 1005, 100506, '一行テキスト値_10050001', null, null, null, null, null, 10050001),
  (null, 1005, 100501, null, 2, null, null, null, null, 10050002),
  (null, 1005, 100502, null, null, null, null, null, '2014-01-01 00:00', 10050002),
  (null, 1005, 100503, null, 101, null, null, null, null, 10050002),
  (null, 1005, 100504, null, null, null, null, null, '2014-01-01 00:00', 10050002),
  (null, 1005, 100505, null, 101, null, null, null, null, 10050002),
  (null, 1005, 100506, '一行テキスト値_10050002', null, null, null, null, null, 10050002)
;

insert into release_control
  (table_id, trunk_revision, release_revision, has_pending_release) VALUES
  (1001, 1, 1, 0),
  (1002, 1, 1, 0),
  (1003, 1, 1, 0),
  (1004, 1, 1, 0),
  (1005, 1, 1, 0)
;
