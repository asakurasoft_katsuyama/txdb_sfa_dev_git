<?php
namespace TxCore\Test;

use TxCore\Util;

class UtilTest extends BaseTestCase
{
	public function testNormalizeKeyword() 
	{
		$this->assertIdentical('あいう', Util::normalizeKeyword("ｱｲｳ"));
		$this->assertIdentical('abc', Util::normalizeKeyword("ABC"));
		$this->assertIdentical('abc', Util::normalizeKeyword("ａｂｃ"));
		$this->assertIdentical('abc', Util::normalizeKeyword("ＡＢＣ"));
		$this->assertIdentical(null, Util::normalizeKeyword(" "));
		$this->assertIdentical(null, Util::normalizeKeyword("\t\n"));
		$this->assertIdentical("aあ", Util::normalizeKeyword("a　あ"));
		$this->assertIdentical(null, Util::normalizeKeyword("<p>", true));
		$this->assertIdentical("ab", Util::normalizeKeyword("a<br/>b", true));
		$this->assertIdentical(null, Util::normalizeKeyword("<p></p>", true));
		$this->assertIdentical("a", Util::normalizeKeyword("<p>A</p>", true));
		$this->assertIdentical("a", Util::normalizeKeyword("<a href=\"http://xxxxxxx/\">A</a>\n\t", true));
	}
	
	public function testFormatNumber()
	{
		$this->assertIdentical('10.1', Util::formatNumber("10.1"));
		$this->assertIdentical('100.1', Util::formatNumber("100.1"));
		$this->assertIdentical('1,000.1', Util::formatNumber("1000.1"));
	}
	
	public function testToUnixTimestamp()
	{
		$this->assertIdentical(null, Util::toUnixTimestamp(null));
		$this->assertIdentical(null, Util::toUnixTimestamp(""));
		$this->assertIdentical(strtotime('2012-01-01'), Util::toUnixTimestamp("2012-01-01"));
		$this->assertIdentical(strtotime('2012-01-01'), Util::toUnixTimestamp("2012-01-01 00:00"));
		$this->assertIdentical(strtotime('2012-01-01')+60, Util::toUnixTimestamp("2012-01-01 00:01"));
		$this->assertIdentical(strtotime('2012-01-01')+61, Util::toUnixTimestamp("2012-01-01 00:01:01"));
	}
	
	public function testToSecondsInDay()
	{
//		$this->assertIdentical(null, Util::toSecondsInDay(null)); // なくなったメソッド
//		$this->assertIdentical(null, Util::toSecondsInDay("")); // なくなったメソッド
//		$this->assertIdentical(0, Util::toSecondsInDay("00:00")); // なくなったメソッド
//		$this->assertIdentical(60, Util::toSecondsInDay("00:01")); // なくなったメソッド
//		$this->assertIdentical(0, Util::toSecondsInDay("00:00:00")); // なくなったメソッド
//		$this->assertIdentical(61, Util::toSecondsInDay("00:01:01")); // なくなったメソッド
	}
	
	public function testIsValidDate()
	{
		$this->assertIdentical(true, Util::isValidDate('2012-01-01'));
		$this->assertIdentical(true, Util::isValidDate('2012-01-1'));
		$this->assertIdentical(true, Util::isValidDate('2012-1-1'));
		$this->assertIdentical(true, Util::isValidDate('2012-2-28'));
		$this->assertIdentical(false, Util::isValidDate('2012-02-30'));
		$this->assertIdentical(false, Util::isValidDate('2012/02/1'));
		$this->assertIdentical(false, Util::isValidDate(null));
	}
}
