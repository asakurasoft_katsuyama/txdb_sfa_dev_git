<?php
namespace TxCore\Test;

use TxCore\LookupValue;

class Engine_getTableRowTest extends BaseTestCase
{
	public function setUp()
	{
        parent::setUpDB(__DIR__.'/EngineTest.sql');
	}

	public function testGetRowFor_すべての列が返ること()
	{
		$table = $this->mockUpTableFully();
		$table->create($this->target->getUser(1001));
		
		$row = $table->addRow();
		$row->setValue($table->getColumn(0), '1');
		$row->setValue($table->getColumn(1), '2');
		$row->setValue($table->getColumn(2), '<p>3</p>');
		$row->setValue($table->getColumn(3), '4.1');
// 		$row->setValue($table->getColumn(4), '');
		$row->setValue($table->getColumn(5), $table->getColumn(5)->getOption(0));
		$row->setValue($table->getColumn(6), array($table->getColumn(6)->getOption(0), $table->getColumn(6)->getOption(1)));
		$row->setValue($table->getColumn(7), $table->getColumn(7)->getOption(0));
		$row->setValue($table->getColumn(8), '2014-01-01');
		$row->setValue($table->getColumn(9), '01:01');
		$row->setValue($table->getColumn(10), '2014-01-01 01:01');
		$row->setValue($table->getColumn(11), array($this->mockUpFile()));
		$row->setValue($table->getColumn(12), 'sample@sample.com');
// 		$row->setValue($table->getColumn(13), '');
		$lookup = new LookupValue();
		$row->setValue($table->getColumn(14), $lookup);
		$table->insertRows($this->target->getUser(1001));
		
		$row = $table->getRowFor($row->getId());
		$this->markTestIncomplete('Do implement');//TODO:
	}


	public function testCalculate_TODO_再計算が走ること()
	{
		$this->markTestIncomplete('Do implement');//TODO:
	}
	
	public function testGetRow_アップロードファイルが参照できること()
	{
		$this->markTestIncomplete('Do implement');//TODO:
	}
	

	
	
}
