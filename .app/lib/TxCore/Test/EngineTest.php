<?php
namespace TxCore\Test;

use TxCore\Column;
use TxCore\Comment;
use TxCore\Engine;
use TxCore\Table;
use TxCore\TxCoreException;
use TxCore\User;

class EngineTest extends BaseTestCase
{
	public function setUp()
	{
        parent::setUpDB(__DIR__.'/EngineTest.sql');
	}

	public function testAuthenticateUser_ログインに成功()
	{
		$user = $this->target->authenticateUser('user1@sample.com', 'password');
		$this->assertNotNull($user);
		$this->assertIdentical(1001, $user->getId());
	}

	public function testAuthenticateUser_前後スペース()
	{
		$user = $this->target->authenticateUser('user1@sample.com ', "password\n");
		$this->assertNotNull($user);
		$this->assertIdentical(1001, $user->getId());
	}

	public function testAuthenticateUser_ログインに失敗()
	{
		try {
			$user = $this->target->authenticateUser('user1@sample.com_', 'password');
			$this->fail('Should not be successfull');
		} catch (TxCoreException $e) {
			$this->assertIdentical(TxCoreException::AUTHENTICATION_FAILED, $e->getCode());
		}
	}



	public function testGetIcons_正常系()
	{
		$icons = $this->target->getIcons();
		$this->assertIdentical(4, count($icons));
		$this->assertIdentical('icon01.png', $icons[0]->getFilekey());
		$this->assertIdentical(true, $icons[0]->isPreset());
	}

	public function testGetIcon_正常系()
	{
		$icon = $this->target->getIcon(10001);
		$this->assertIdentical('icon01.png', $icon->getFilekey());
		$this->assertIdentical(true, $icon->isPreset());
	}

	public function testGetIcon_存在しないアイコン()
	{
		try {
			$icon = $this->target->getIcon(-1);	// 例外が発生する
			$this->fail('Should not be successful');
		} catch (TxCoreException $e) {
			$this->assertIdentical(TxCoreException::ROW_NOT_FOUND, $e->getCode());
		}
	}

	public function testSearchUsers_全件()
	{
		// ◇ 全件ヒットすること、表示名で並ぶこと
		$users = $this->target->searchUsers();
		$this->assertIdentical('user', $users->getType());
		$this->assertIdentical(11, count($users));
		$this->assertIdentical(1006, $users[0]->getId());
		$this->assertIdentical('user6@sample.com', $users[0]->getLoginId());
		$this->assertIdentical('password', $users[0]->getPassword());
		$this->assertIdentical('abc user', $users[0]->getScreenName());
		$this->assertIdentical('my company', $users[0]->getProfile());
		$this->assertIdentical('user', $users[0]->getRole());
		$this->assertIdentical(false, $users[0]->isAdmin());
		$this->assertIdentical(1001, $users[1]->getId());
		$this->assertIdentical(1002, $users[2]->getId());
	}

	public function testSearchUsers_キーワード検索()
	{
		// ログインID、表示名、プロフィールにヒット
		$users = $this->target->searchUsers('abc');
		$this->assertIdentical(3, count($users));
		$this->assertIdentical(1006, $users[0]->getId());
		$this->assertIdentical(1005, $users[1]->getId());
		$this->assertIdentical(1007, $users[2]->getId());
	}

	public function testSearchUsers_表示件数()
	{
		$users = $this->target->searchUsers(null, 0, 10);
		$this->assertIdentical(10, $users->getRowCount());
		$this->assertIdentical(11, $users->getTotalRowCount());
		$this->assertIdentical(1006, $users[0]->getId());
		$this->assertIdentical(1001, $users[1]->getId());

		$users = $this->target->searchUsers(null, 9, 10);
		$this->assertIdentical(2, $users->getRowCount());
		$this->assertIdentical(11, $users->getTotalRowCount());
		$this->assertIdentical(1010, $users[0]->getId());
		$this->assertIdentical(1011, $users[1]->getId());
	}

	public function testSearchUsers_メタ文字のエスケープ()
	{
		$users = $this->target->searchUsers('%');
		$this->assertIdentical(0, count($users));
	}



	public function testGetUser_正常系()
	{
		$user  = $this->target->getUser(1002);
		$this->assertNotNull($user);
		$this->assertIdentical(1002, $user->getId());
		$this->assertIdentical('user2@sample.com', $user->getLoginId());
		$this->assertIdentical('password', $user->getPassword());
		$this->assertIdentical('test user02', $user->getScreenName());
		$this->assertIdentical('my company', $user->getProfile());
		$this->assertIdentical('admin', $user->getRole());
		$this->assertIdentical(true, $user->isAdmin());
	}

	public function testDeleteUser_正常系()
	{
		$user = $this->target->getUser(1001);
		$user->delete($this->target->getUser(1002));

		$this->assertNotRegistered('user', array(@id => 1001));
	}

	public function testCreateOrReplaceUser_新規登録()
	{
		$by  = $this->target->getUser(1002); // 管理者
		$user = new User();
		$user->setLoginId("login id");
		$user->setPassword("password");
		$user->setScreenName("screen name");
		$user->setRole(User::ADMIN);
		$user->createOrReplace($by);

		$this->assertRegistered('user', array(@login_id => 'login id', @role => 'admin', @password => 'password', @screen_name => 'screen name'));
	}

	public function testCreateOrReplaceUser_同じログインが登録されている()
	{
		$by  = $this->target->getUser(1002); // 管理者
		$user = new User();
		$user->setLoginId("user1@sample.com");
		$user->setPassword("password");
		$user->setScreenName("screen name");
		$user->setRole(User::ADMIN);
		$user->createOrReplace($by);

		$this->assertRegistered('user', array(@id => 1001, @login_id => 'user1@sample.com', @role => 'admin', @password => 'password', @screen_name => 'screen name'));
	}

	public function testCreateUser_正常系()
	{
		$by  = $this->target->getUser(1002); // 管理者
		$user = new User();
		$user->setLoginId("login id");
		$user->setPassword("password");
		$user->setScreenName("screen name");
		$user->setRole(User::ADMIN);
		$user->create($by);

		$this->assertNotNull($user->getId());
		$this->assertRegistered('user', array(@login_id => 'login id', @role => 'admin', @password => 'password', @screen_name => 'screen name'));
	}

	public function testCreateUser_同じログインが登録されている()
	{
		$by  = $this->target->getUser(1002);

		$user = new User();
		$user->setLoginId("user1@sample.com");
		$user->setPassword("password");
		$user->setScreenName("screen name");
		$user->setRole(User::ADMIN);

		try {
			$user->create($by);	// 例外が発生する
			$this->fail('Should not be successfull');
		} catch (TxCoreException $e) {
			$this->assertIdentical(TxCoreException::LOGIN_ID_WAS_DUPLICATED, $e->getCode());
		}
	}

	public function testCreateUser_権限チェック()
	{
		$by = $this->target->getUser(1001);	// 一般ユーザー
		try {
			$user = new User();
			$user->setScreenName("new User");
			$user->create($by);
			$this->fail('Should not be successfull');
		} catch (TxCoreException $e) {
			$this->assertIdentical(TxCoreException::PERMISSION_DENIED, $e->getCode());
		}
	}

	public function testUpdateUser_正常系()
	{
		$user = $this->target->getUser(1001);
		$user->setLoginId("new login id");
		$user->setPassword("new password");
		$user->setScreenName("new screen name");
		$user->setRole(User::USER);
		$user->update($user);	// 自分を更新

		$this->assertRegistered('user', array(@login_id => 'new login id', @role=> 'user', @password => 'new password', @screen_name => 'new screen name'));
	}

	public function testUpdateUser_ログイン変更なし()
	{
		$user = $this->target->getUser(1001);
		$user->setLoginId("user1@sample.com");
		$user->update($user);	// エラーにならない
	}

	public function testUpdateUser_同じログインが登録されている()
	{
		$user = $this->target->getUser(1001);
		$user->setLoginId("user2@sample.com");
		try {
			$user->update($user);	// 例外
			$this->fail('Should not be successfull');
		} catch (TxCoreException $e) {
			$this->assertIdentical(TxCoreException::LOGIN_ID_WAS_DUPLICATED, $e->getCode());
		}
	}

	public function testUpdateUser_権限チェック()
	{
		// 自分を更新
		$user = $this->target->getUser(1001);
		$user->update($user);	// 自分を更新

		// 管理者が更新
		$by = $this->target->getUser(1002);
		$user->update($by);

		// 他人が更新 → 例外
		try {
			$by = $this->target->getUser(1003);
			$user = $this->target->getUser(1001);
			$user->update($by);
			$this->fail('Should not be successfull');
		} catch (TxCoreException $e) {
			$this->assertIdentical(TxCoreException::PERMISSION_DENIED, $e->getCode());
		}
	}

	public function testCreateOrRepalceUser_登録されていない()
	{
		$by = $this->target->getUser(1002);

		$user = new User();
		$user->setLoginId("new login id");
		$user->setPassword("new password");
		$user->setScreenName("new screen name");
		$user->setRole(User::USER);
		$user->createOrReplace($by);	// 新規登録される

		$this->assertRegistered('user', array(@login_id => 'new login id', @role=> 'user', @password => 'new password', @screen_name => 'new screen name'));
	}

	public function testCreateOrRepalceUser_同じログインが登録されている()
	{
		$by = $this->target->getUser(1002);

		$user = new User();
		$user->setLoginId(" user2@sample.com ");
		$user->setPassword("new password");
		$user->setScreenName("new screen name");
		$user->setRole(User::USER);
		$user->createOrReplace($by);	// 既存のユーザー情報が更新される

		$this->assertRegistered('user', array(@id => 1002, @login_id => 'user2@sample.com', @role=> 'user', @password => 'new password', @screen_name => 'new screen name'));
	}

	public function testGetUser_存在しない場合例外()
	{
		try {
			$this->target->getUser(-1);
			$this->fail('Should not be successfull');
		} catch (TxCoreException $e) {
			$this->assertIdentical(TxCoreException::ROW_NOT_FOUND, $e->getCode());
		}
	}

	public function testGetUserTables_正常系()
	{
		// 自分のものと他の人の公開テーブルが見える
		$user = $this->target->getUser(1001);
		$Tables  = $this->target->getUserTables($user);
		$this->assertIdentical(2, count($Tables));
		$this->assertIdentical(1001, $Tables[0]->getId());
		$this->assertIdentical('table 1', $Tables[0]->getName());
// 		$this->assertIdentical('production', $Tables[0]->getDefType());
		$this->assertIdentical('/asset/icons/default.png', $Tables[0]->getIcon()->getUrl());
		$this->assertIdentical(true, $Tables[0]->isPublic());
		$this->assertIdentical(1005, $Tables[1]->getId());	// 他人の
	}

	public function testGetUserTables_管理者から実行()
	{
		// 管理者の場合、すべてのテーブルが見える
		$user = $this->target->getUser(1002);
		$Tables  = $this->target->getUserTables($user);
		$this->assertIdentical(4, count($Tables));
	}

	public function testGetTempletes_正常系()
	{
		$tempaltes  = $this->target->getTemplates();
		$this->assertIdentical(2, count($tempaltes));
		$this->assertIdentical(2001, $tempaltes[0]->getId());
		$this->assertIdentical('sample template 1', $tempaltes[0]->getName());
		$this->assertIdentical('This is sample template 1.', $tempaltes[0]->getDescription());
		$this->assertIdentical('default1.png', $tempaltes[0]->getIcon()->getFilekey());
		$this->assertIdentical('/asset/icons/default1.png', $tempaltes[0]->getIcon()->getUrl());
		$this->assertIdentical(2002, $tempaltes[1]->getId());
	}

	public function testGetTemplate_正常系()
	{
		$template = $this->target->getTemplate(2002);
		$this->assertNotNull($template);
		$this->assertIdentical(2002, $template->getId());
		$this->assertIdentical('sample template 2', $template->getName());
		$this->assertIdentical('This is sample template 2.', $template->getDescription());
		$this->assertIdentical('default2.png', $template->getIcon()->getFilekey());
		$this->assertIdentical(false, $template->getIcon()->isPreset());
// 		$this->assertIdentical(3, count($template->getColumns()));
	}

	public function testGetTemplateFromTable_正常系()
	{
		$table = $this->target->getTable(1001);
		$template = $this->target->getTemplateFromTable($table);
		$this->assertNotNull($template);
		$this->assertIdentical(1001, $template->getId());
		$this->assertIdentical('table 1', $template->getName());
		$this->assertIdentical('This is table 1', $template->getDescription());
		$this->assertIdentical('default.png', $template->getIcon()->getFilekey());
		$this->assertIdentical(false, $template->getIcon()->isPreset());
// 		$this->assertIdentical(3, count($template->getColumns()));
	}

	public function testGetTemplate_テンプレートが存在しない()
	{
		try {
			$template = $this->target->getTemplate(-1);	// 例外が発生する
			$this->fail('Should not be successfull');
		} catch (TxCoreException $e) {
			$this->assertIdentical(TxCoreException::ROW_NOT_FOUND, $e->getCode());
		}
	}


	public function testGetTable_リリース()
	{
		$table = $this->target->getTable(1001);
		$this->assertIdentical(1001, $table->getId());	// リリースが返ること
		$this->assertIdentical('table 1', $table->getName());
		$this->assertIdentical('This is table 1', $table->getDescription());
		$this->assertIdentical(true, $table->isApiEnabled());
		$this->assertIdentical(true, $table->isPublic());
		$this->assertIdentical('default.png', $table->getIcon()->getFilekey());
		$this->assertIdentical('/asset/icons/default.png', $table->getIcon()->getUrl());
		$this->assertNotNull($table->getOwner());
		$this->assertIdentical(1001, $table->getOwner()->getId());
		$this->assertIdentical('test user01', $table->getOwner()->getScreenName());
		$this->assertNotNull($table->getColumns());
		$this->assertIdentical(3, $table->getRoundScale());
		$this->assertIdentical('ceil', $table->getRoundType());
// 		$work = $table->_getWorkTable();
// 		$this->assertNotNull($work);
// 		$this->assertIdentical(1002, $work->getId());
		// TODO : Assertion
	}

	public function testGetTable_リビジョン指定あり()
	{
		$by = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$table->create($by);

		// 指定なしはrelease
		$relase1 = $this->target->getTable($table->getId());
		$this->assertIdentical($table->getId(), $relase1->getId());
		$this->assertIdentical($table->getRevision(), $relase1->getRevision());

		// release
		$relase2 = $this->target->getTable($table->getId(), Engine::RELEASE);
		$this->assertIdentical($table->getId(), $relase2->getId());
		$this->assertIdentical($table->getRevision(), $relase2->getRevision());

		// trunk
		$trunkRev = $this->target->getTable($table->getId(), Engine::TRUNK);
		$this->assertIdentical($table->getId(), $trunkRev->getId());
		$this->assertNotIdentical($table->getRevision(), $trunkRev->getRevision());	// 異なるRevが返る

		// リビジョンNo指定
		$trunkRev2 = $this->target->getTable($table->getId(), $trunkRev->getRevision());
		$this->assertIdentical($trunkRev2->getId(), $trunkRev->getId());
		$this->assertIdentical($trunkRev2->getRevision(), $trunkRev->getRevision());
	}

// 	public function testPublishTable_正常系()
// 	{
// 		$Table = $this->target->getTable(1003);
// 		$Table->publishTable();
// 		$this->assertIdentical(true, $Table->isPublic());
// 		$this->assertRegistered('table_def', array(array(@id => 1003, @is_public => 1)));
// 	}

// 	public function testUnpublishTable_正常系()
// 	{
// 		$Table = $this->target->getTable(1001);
// 		$Table->unpublishTable();
// 		$this->assertIdentical(false, $Table->isPublic());
// 		$this->assertRegistered('table_def', array(array(@id => 1001, @is_public => 0)));
// 	}

	public function testGetTable_存在しない場合例外()
	{
		try {
			$table = $this->target->getTable(-1);
			$this->fail('Should not be successfull');
		} catch (TxCoreException $e) {
			$this->assertIdentical(TxCoreException::ROW_NOT_FOUND, $e->getCode());
		}
	}

	public function testGetTableColumns()
	{
		$by = $this->target->getUser(1002);
		$table = $this->mockUpTableFully();
		$table->create($by);

		$columns = $table->getColumns();
		$this->assertIdentical(26, count($columns));	// すべてのカラムが返ること
		$this->assertIdentical($columns[0]->getType(), Column::TEXT);
// 		$this->assertIdentical(1, $columns[0]->getPositionX());
// 		$this->assertIdentical(2, $columns[0]->getPositionY());
		$this->assertIdentical($columns[1]->getType(), Column::MULTITEXT);
		$this->assertIdentical($columns[2]->getType(), Column::RICHTEXT);
		$this->assertIdentical($columns[3]->getType(), Column::NUMBER);
		$this->assertIdentical($columns[4]->getType(), Column::CALC);
		$this->assertIdentical($columns[5]->getType(), Column::RADIO);
		$this->assertIdentical($columns[6]->getType(), Column::CHECKBOX);
		$this->assertIdentical($columns[7]->getType(), Column::DROPDOWN);
		$this->assertIdentical($columns[8]->getType(), Column::DATE);
		$this->assertIdentical($columns[9]->getType(), Column::TIME);
		$this->assertIdentical($columns[10]->getType(), Column::DATETIME);
		$this->assertIdentical($columns[11]->getType(), Column::FILE);
		$this->assertIdentical($columns[12]->getType(), Column::LINK);
		$this->assertIdentical($columns[13]->getType(), Column::RELATIONAL);
		$this->assertIdentical($columns[14]->getType(), Column::LOOKUP);
		$this->assertIdentical($columns[15]->getType(), Column::ROWNUM);
		$this->assertIdentical($columns[16]->getType(), Column::CREATED_BY);
		$this->assertIdentical($columns[17]->getType(), Column::CREATED_ON);
		$this->assertIdentical($columns[18]->getType(), Column::UPDATED_BY);
		$this->assertIdentical($columns[19]->getType(), Column::UPDATED_ON);
		$this->assertIdentical($columns[20]->getType(), Column::LABEL);
		$this->assertIdentical($columns[21]->getType(), Column::SPACE);
		$this->assertIdentical($columns[22]->getType(), Column::LINE);
		$this->assertIdentical($columns[23]->getType(), Column::LOOKUP);
		$this->assertIdentical($columns[24]->getType(), Column::USER_SELECTOR);
		// TODO : Assertion
	}

	public function testGetTableRow()
	{
		$by = $this->target->getUser(1001);
		$table = $this->mockUpTable();
		$column_0 = $table->addColumn(Column::TEXT);
		$table->create($by);
		$row = $table->addRow();
		$row->setValue($column_0, 'textbox');
		$table->insertRows($table->getOwner());

		$row2 = $table->getRowFor($row->getId());	// IDで検索できること
		$this->assertIdentical($row2->getId(), $row->getId());
		$this->assertIdentical("textbox", $row2->getValue($column_0));
	}

	public function testCreateComment()
	{
		$table = $this->target->getTable(1001);
		$user = $this->target->getUser(1001);
		$group = $this->target->getGroup(11);
		$row = $table->getRowFor(1002);
		$comment = new Comment();
		$comment->setText("コメントです\n");
		$comment->addNotificationTarget($user);
		$comment->addNotificationTarget($group);
		$this->target->createRowComment($row, $comment, $user);

		$comments = $this->selectTable('comment', '');
		$this->assertSame(1, sizeof($comments));
		$this->assertSame("1", $comments[0]->no);
		$this->assertSame("コメントです\n", $comments[0]->text);
		$this->assertSame("1002", $comments[0]->row_id);
		$this->assertSame("1001", $comments[0]->table_id);
		$this->assertSame("1001", $comments[0]->posted_user_id);
		$this->assertSame('[{"type":"user","id":1001},{"type":"group","id":11}]', $comments[0]->notification_targets);
		$this->assertNotNull($comments[0]->posted_time);
	}

	public function testDeleteComment()
	{
		$table = $this->target->getTable(1001);
		$user = $this->target->getUser(1003);
		$row = $table->getRowFor(1002);
		$comment = new Comment();
		$comment->setText("コメントです\n");
		$this->target->createRowComment($row, $comment, $user);

		$comments = $this->selectTable('comment');
		$this->assertSame(1, sizeof($comments));

		$this->target->deleteComment($comment, $user);

		$comments = $this->selectTable('comment');
		$this->assertSame(0, sizeof($comments));
	}

	public function testDeleteComment_権限なし()
	{
		$table = $this->target->getTable(1001);
		$user = $this->target->getUser(1001);
		$someone = $this->target->getUser(1003);
		$row = $table->getRowFor(1002);
		$comment = new Comment();
		$comment->setText("コメントです\n");
		$this->target->createRowComment($row, $comment, $user);

		try {
			$this->target->deleteComment($comment, $someone);
			$this->fail('Should not be successful.');
		} catch (TxCoreException $e) {
			$this->assertSame(TxCoreException::PERMISSION_DENIED, $e->getCode());
		}
	}

	public function testDeleteComment_管理者は削除できる()
	{
		$table = $this->target->getTable(1001);
		$user = $this->target->getUser(1001);
		$admin = $this->target->getUser(1002);
		$row = $table->getRowFor(1002);
		$comment = new Comment();
		$comment->setText("コメントです\n");
		$this->target->createRowComment($row, $comment, $user);

		// 管理者は削除できる
		$this->target->deleteComment($comment, $admin);
		$comments = $this->selectTable('comment');
		$this->assertSame(0, sizeof($comments));
	}

	/**
	 * - Noの降順になること
	 */
	public function testSearchComment()
	{
		$table = $this->target->getTable(1001);
		$by = $this->target->getUser(1001);
		$row = $table->getRowFor(1002);
		$group = $this->target->getGroup(11);
		$comment = new Comment();
		$comment->setText("コメントです\n");
		$comment->addNotificationTarget($by);
		$comment->addNotificationTarget($group);
		$this->target->createRowComment($row, $comment, $by);
		$this->target->createRowComment($row, $comment, $by);

		$list_ = $this->target->searchRowComments($row);
		$targets1 = $list_[0]->getNotificationTargets();
		$this->assertSame(2, $list_[0]->getNo());
		$this->assertSame(2, sizeof($targets1));
		$this->assertSame(1001, $targets1[0]->getId());
		$this->assertSame(11, $targets1[1]->getId());

		$this->assertSame(2, $list_->count());
		$this->assertSame(1, $list_[1]->getNo());
		$this->assertSame("コメントです\n", $list_[1]->getText());
		$this->assertNotNull($list_[1]->getPostedTime());
		$this->assertNotNull($list_[1]->getPostedUser());
		$this->assertSame(1001, $list_[1]->getPostedUser()->getId());

	}

	/**
	 * -　通知先グループが存在しない（削除された）場合、取得時に一覧から除外される
	 */
	public function testSearchComment_グループが消された()
	{
		$table = $this->target->getTable(1001);
		$by = $this->target->getUser(1001);
		$admin = $this->target->getUser(1002);
		$row = $table->getRowFor(1002);
		$group = $this->target->getGroup(11);
		$comment = new Comment();
		$comment->setText("コメントです\n");
		$comment->addNotificationTarget($group);
		$this->target->createRowComment($row, $comment, $by);

		$list_ = $this->target->searchRowComments($row);
		$this->assertSame(1, $list_->count());
		$this->assertSame(1, sizeof($list_[0]->getNotificationTargets()));

		// Deleted group by admin.
		$this->target->deleteGroup($group, $admin);

		$list_ = $this->target->searchRowComments($row);
		$this->assertSame(1, $list_->count());
		$this->assertSame(1, $list_[0]->getNo());
		$this->assertSame(0, sizeof($list_[0]->getNotificationTargets()));	// 消されたので出てこない
	}

	/**
	 * -　通知先ユーザーが存在しない（削除された）場合、取得時に一覧から除外される
	 */
	public function testSearchComment_ユーザーが消された()
	{
		$table = $this->target->getTable(1001);
		$by = $this->target->getUser(1001);
		$admin = $this->target->getUser(1002);
		$row = $table->getRowFor(1002);
		$comment = new Comment();
		$comment->setText("コメントです\n");
		$comment->addNotificationTarget($by);
		$this->target->createRowComment($row, $comment, $by);

		$list_ = $this->target->searchRowComments($row);
		$this->assertSame(1, $list_->count());
		$this->assertSame(1, sizeof($list_[0]->getNotificationTargets()));

		// Deleted user by admin.
		$this->target->deleteUser($by, $admin);

		$list_ = $this->target->searchRowComments($row);
		$this->assertSame(1, $list_->count());
		$this->assertSame(0, sizeof($list_[0]->getNotificationTargets()));	// 消されたので出てこない
	}


	public function testSearchComment_コメントユーザーが削除された()
	{
		$table = $this->target->getTable(1001);
		$user = $this->target->getUser(1001);
		$admin = $this->target->getUser(1002);
		$row = $table->getRowFor(1002);
		$comment = new Comment();
		$comment->setText("コメントです\n");
		$this->target->createRowComment($row, $comment, $user);

		$this->target->deleteUser($user, $admin);

		$list_ = $this->target->searchRowComments($row);
		$this->assertSame(1, $list_->count());
		$this->assertNull($list_[0]->getPostedUser());	// Null
	}

}
