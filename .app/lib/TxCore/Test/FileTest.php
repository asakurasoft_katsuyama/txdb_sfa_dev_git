<?php
namespace TxCore\Test;

use TxCore\Column;
use TxCore\File;

class FileTest extends BaseTestCase
{
	public function testCreate()
	{
		$by = $this->target->getUser(1001);
		$table  = $this->mockUpTable();
		$column_0 = $table->addColumn(Column::FILE);
		$table->create($by);
		
		$file = File::create(dirname(__FILE__).'/file.xls', '資料.xls', 'application/ms-excel');
		$this->assertIdentical('資料.xls', $file->getFileName());
		$this->assertPattern('/file.xls$/', $file->getFilekey());
// 		$this->assertPattern(sprintf('#^/asset/files/%d/%d/.*.xls$#', $table->getId(), $column_0->getId()), $file->getUrl());
// 		$this->assertTrue(file_exists(dirname(__FILE__) .'/asset/files/'.$table->getId().'/'. $column_0->getId().'/'.$file->getFilekey()));
// 		$this->assertIdentical(false, $file->hasThumbnail());
// 		$this->assertIdentical(null, $file->getThumbnailUrl());
	}
}
