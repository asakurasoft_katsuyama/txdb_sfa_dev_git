delete from env_ut;

delete from user;
delete from table_def;
delete from column_def;
delete from `data`;
delete from `row`;
delete from `indexes`;
delete from `option`;
delete from `release_control`;
delete from `row_no`;
delete from `group`;
delete from `user_group`;

