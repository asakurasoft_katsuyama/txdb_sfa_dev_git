<?php

namespace TxCore\Test;

use Barge\DB\DBClient;
use PHPUnit_Framework_TestCase;
use TxCore\Column;
use TxCore\Column__Datecalc;
use TxCore\Criteria;
use TxCore\Engine;
use TxCore\File;
use TxCore\Option;
use TxCore\Table;
use TxCore\TxCoreException;
use TxCore\User;

/**
 * SimplePHPを継承したベースクラスをPHPUnitを継承したものに変更
 *
 * Created by PhpStorm.
 * User: CRAID
 * Date: 2014/11/28
 */
class BaseTestCase extends PHPUnit_Framework_TestCase
{

	/**
	 * @var DBClient
	 */
	protected $db;

	/**
	 * @var Engine
	 */
	protected $target;

	protected $testClassName = null;

	public function __construct()
	{
		$this->target = Engine::factory();
		$this->target->setNow(strtotime('2014-01-02 23:59:00') - (9 * 60 * 60));	// UTCなので-9h
	}

//	public function before($method)
//	{
//
//		log_debug(sprintf("Invoking %s#%s", get_class($this), $method));
//		echo sprintf("Invoking %s#%s\n", get_class($this), $method);
//		parent::before($method);
//	}
//
//	public function after($method)
//	{
//		if (@$this->_reporter->_fail || @$this->_reporter->_error || !@$this->_reporter->_pass){
//			log_debug(">> Test failed.");
//			echo ">> Test *failed*.\n";
//		} else {
//			log_debug("Test success.");
//			echo ">> Test success.\n";
//		}
//
//		// 		log_debug(sprintf("<< End %s", $method));
//		// 		echo sprintf("<< End %s\n", $method);
//		parent::after($method);
//	}

//	public function exception($e)
//	{
//		$f = format_exception($e);
//		log_debug($f);
//		echo $f."\n";
//		parent::exception($e);
//	}
//
//	public function error($errno, $errstr, $errfile = null, $errline = null, array $contxt = null)
//	{
//		$message = sprintf("[%s] %s in %s (%d)",
//			'UNKNOWN_ERROR',
//			$errstr, $errfile, $errline);
//
//		log_debug($message);
//		echo $message."\n";
//		parent::error($errno, $errstr, $errfile, $errline, $contxt);
//	}

	public function setUp() {}

	protected function setUpDB($sqlFileName = null)
	{
		$this->db = DBClient::connect();
		$this->db->beginTrans();

		// 実行するテスト環境用SQL
		if (empty($sqlFileName)) {
			throw new \Exception();
		}
		//foreach (explode(';', file_get_contents(dirname(__FILE__) .'/'.$sqlFileName.'.sql')) as $sql) {
        foreach (explode(';', file_get_contents($sqlFileName)) as $sql) {
			if (trim($sql)  != "")
				$this->db->update($sql);
		}

		$this->db->commit();
		$this->db->close();
	}

    public function tearDown()
	{
//		if ($this->db)
//			$this->db->rollback();

		self::deleteDir(get_config('app', 'user_icon_dir'));
		self::deleteDir(get_config('app', 'user_file_dir'));
		self::deleteDir(get_config('app', 'user_temp_dir'));
	}
	public function assertNotRegistered($table, $data)
	{
		$this->assertRegistered($table, $data, 0);
	}
	public function selectTable($table, $condition = null)
	{

		$query = "select * from `$table`";
		$query .= $condition ?  (" where ". $condition) : "";
		return $this->db->query($query);
	}

	/**
	 * @param string $table
	 * @param string $condition
	 * @param string $delimiter
	 */
	protected function dumpTable($table, $condition = NULL, $delimiter= "\t")
	{
		$to_string = function($value) {
			if ($value === null) {
				return 'null';
			} else if (is_bool($value)) {
				return $value ? 'true' : 'false';
			} else if ($value instanceof \DateTime) {
				return $value->format('Y-m-d H:i:s');
			} else {
				return $value;
			}
		};
		$buff = "";
		$query = "select * from `$table`";
		if ($condition)
			$query .= "where ". $condition;
		$firstDone = false;
		$list = $this->db->query($query, array());
		$buff.= '---- Dump for '.$table." ----\n";
		$i =0;
		foreach ($list as $obj) {
			if ($i == 0) {
				foreach ($obj as $key => $_) {
					$buff.= $key.$delimiter;
				}
				$buff.= "\n";
			}
			$buff.= join($delimiter, array_map($to_string, get_object_vars($obj))) . "\n";
			$i++;
		}

		log_debug($buff);
	}

	public function countRecords($table, $data = array())
	{
		$query = "select * from `$table` where";
		$where = "";
		$params = array();
		$properties = array();
		if (count($data) == 0 || (count($data) == 1 &&
			array_key_exists('properties', $data))) {
			$where .= " 1=1 ";
		}
		foreach ($data as $key => $value) {
			// For json
			if ($key === 'properties' && is_array($value)) {
// 				foreach ($value as $key2 => $value2) {
// 					if (is_not_empty($where))
// 						$where.=" and ";
// 					if (str_ends_with('!', $key2)) {
// 						$where.= " $key not like ? ";
// 						$key2 = substr($key2, 0, -1);
// 					} else {
// 						$where.= " $key like ? ";
// 					}
// 					if ($value2 === null) {
// 						$params[] = '%"'.$key2.'":null%';
// 					} else if (is_int($value2)) {
// 						$params[] = '%"'.$key2.'":'.$value2.'%';
// 					} else if (is_bool($value2)) {
// 						$params[] = '%"'.$key2.'":'.($value2?'true':'false').'%';
// 					} else if (is_array($value2)) {
// 						$params[] = '%"'.$key2.'":'.json_encode($value2).'%';
// 					} else {
// 						$params[] = '%"'.$key2.'":"'.$value2.'"%';
// 					}
// 				}
				$properties = $value;
				continue;
			}

			if (is_not_empty($where))
				$where.=" and ";
			if (str_ends_with('!~', $key)) {
				$where .= substr($key, 0, -2)." NOT REGEXP ?";
				$params[] = $value;
			} else if (str_ends_with('~', $key)) {
				$where .= substr($key, 0, -1)." REGEXP ?";
				$params[] = $value;
			} else if (str_ends_with('!*', $key)) {
				$where .= ' '.substr($key, 0, -2)." not like ?";
				$params[] = "%".$value."%";
			} else if (str_ends_with('*', $key)) {
				$where .= ' '.substr($key, 0, -1)." like ?";
				$params[] = "%".$value."%";
			} else if (str_ends_with('!=', $key)) {
				// TODO :
			} else if (str_ends_with('!', $key)) {
				if ($value === null) {
					$where .= ' '.substr($key, 0, -1)." is not null ";
				} else {
					$where .= ' '.substr($key, 0, -1)." != ? ";
					$params[] = $value;
				}
			} else {
				if ($value === null) {
					$where .= " $key is null ";
				} else {
					$where .= " $key = ? ";
					$params[] = $value;
				}
			}
		}

		$list = $this->db->query($query.$where, $params);
		$matches = count($list);
		if ($properties) {
			foreach ($list as $row) {
				$real = json_decode($row->properties);
				foreach ($properties as $key2 => $value2) {
					if (str_ends_with('!', $key2)) {
						$key2 = substr($key2, 0, -1);
						if (!property_exists($real, $key2)) {
							echo sprintf(" Exclude matches due to [%s dose not exist]\n", $key2);
							$matches--;
							break;
						}
						if ($value2 == $real->{$key2}) {
							echo sprintf(" Exclude matches due to [%s] != [%s]\n", to_string($value2), to_string($real->{$key2}));
							$matches--;
							break;
						}
					} else {
						if ($value2 != $real->{$key2}) {
							echo sprintf(" Exclude matches due to [%s] == [%s]\n", to_string($value2), to_string($real->{$key2}));
							$matches--;
							break;
						}
					}
				}
			}
		}
		return $matches;
	}

	public function assertRegistered($table, $data, $excepts = 1)
	{
		$matches = $this->countRecords($table, $data);
		if (((is_int($excepts) && $matches == $excepts)) ||
			(!is_int($excepts) && eval('('.$matches.$excepts.')') === true)) {
			$this->pass();
		} else {
			$this->fail(sprintf("Expected %d records but %d records registered (%s:%s)",
				$excepts, $matches, $table, to_string($data)));

			if (is_int($excepts)) {
				echo "--- dump for $table ---\n";
				if ($excepts > $matches) {
					$list = $this->db->query("select * from `$table`");
					foreach ($list as $_data)
						echo to_string($_data) ."\n";
				} else {
					foreach ($list as $_data)
						echo to_string($_data) ."\n";
				}
			}

		}
	}

	public function mockUpFile()
	{
		$file = new File();
		$file->_setContentType('image/jpeg');
		$file->_setFilekey('xxxxx.jpg');
		$file->_setFileName('画像.jpg');
		return $file;
	}

	/**
	 * @param string $name
	 * @return Table
	 * @throws TxCoreException
	 */
	public function mockUpTableFully($name = 'sample table')
	{
		$referenceTable = $this->target->getTable(1001);

		$icons = $this->target->getIcons();
		$table = new Table();
		$table->setName('new table');
// 		$table->setDescription('description of table');
		$table->setIcon($icons[0]);
// 		$table->setApiEnabled(true);
		$column1 = $column = $table->addColumn(Column::TEXT); // [0]
		$column->setLabel("Label text");
		$column->setCode($column->getTypeName());

		$column = $table->addColumn(Column::MULTITEXT); // [1]
		$column->setLabel("Label multitext");
		$column->setCode($column->getTypeName());

		$column = $table->addColumn(Column::RICHTEXT);	// [2]
		$column->setLabel("Label richtext");
		$column->setCode($column->getTypeName());

		$column_4 = $table->addColumn(Column::NUMBER); // [3]
		$column_4->setLabel("Label 4");
		$column_4->setCode($column_4->getTypeName());

		$column = $table->addColumn(Column::CALC); // [4]
		$column->setLabel("Label calc");
		$column->setExpression("2 * ".$column_4->getTypeName());
		$column->setCode($column->getTypeName());

		$column6 = $table->addColumn(Column::RADIO); // [5]
		$column6->setLabel("Label radio");
		$option6_1 = new Option();
		$option6_1->setName("New Radio 1");
		$column6->_addOption($option6_1);
		$option6_2 = new Option();
		$option6_2->setName("New Radio 2");
		$column6->_addOption($option6_2);
		$option6_3 = new Option();
		$option6_3->setName("New Radio 3");
		$column6->_addOption($option6_3);

		$column7 = $table->addColumn(Column::CHECKBOX);	// [6]
		$column7->setLabel("Label checkbox");
		$option7_1 = new Option();
		$option7_1->setName("New Checkbox 1");
		$column7->_addOption($option7_1);
		$option7_2 = new Option();
		$option7_2->setName("New Checkbox 2");
		$column7->_addOption($option7_2);
		$column7->setCode($column7->getTypeName());
// 			$column7->setDefaultValue(array($option7_1->getId(), $option7_2->getId()));	// 初期選択
		$column8 = $table->addColumn(Column::DROPDOWN);
		$column8->setLabel("Label 8");
		$option8_1 = new Option();
		$option8_1->setName("New Option 1");
		$column8->_addOption($option8_1);
		$option8_2 = new Option();
		$option8_2->setName("New Option 2");
		$column8->_addOption($option8_2);
// 			$column8->setDefaultValue($option8_2->getId());	// 初期選択
		$column = $table->addColumn(Column::DATE);
		$column->setLabel("Label date");
		$column_date = $column;
		$column = $table->addColumn(Column::TIME);	// [9]
		$column->setLabel("Label time");
		$column = $table->addColumn(Column::DATETIME);	// [10]
		$column->setLabel("Label datetime");
		$column = $table->addColumn(Column::FILE);	// [11]
		$column->setLabel("Label file");
		$column->setThumbnailMaxSize(200);
		$column = $table->addColumn(Column::LINK);
		$column->setLabel("Label link");
		$column->setLinkType('email');
		$column = $table->addColumn(Column::RELATIONAL);
		$column->setLabel("Label relational");
// 			$reference = new Reference();
		$column->setReferenceTable($referenceTable);
		$column->setReferenceTargetColumn($referenceTable->getColumn(0));
// 			$reference->setSourceColumn($column1);
		$column->setReferenceColumns(array($referenceTable->getColumn(0), $referenceTable->getColumn(1)));
		$column->setReferenceSortColumn($referenceTable->getColumn(0));
		$column->setReferenceSortsReverse(true);
		$column->setReferenceMaxRow(100);
// 			$column->setReferenceCopyMap(array(array($referenceTable->getColumn(0), $column1)));
// 			$column->setReference($reference);
		$column = Column::create(Column::LOOKUP, $table);	// [14]
		$column->setLabel("Label lookup");
// 			$reference = new Reference();
		$column->setReferenceTable($referenceTable);
		$column->setReferenceTargetColumn($referenceTable->getColumn(0));
// 			$column->setReferenceSourceColumn($column1);
		$column->setReferenceColumns(array($referenceTable->getColumn(0), $referenceTable->getColumn(1)));
// 			$column->setReferenceSortColumn($referenceTable->getColumn(0));
// 			$column->setReferenceSortsReverse(true);
// 			$column->setReferenceMaxRow(100);
		$column->setReferenceCopyMap(array(array($referenceTable->getColumn(0), $column1)));
// 			$column->setReference($reference);
		$column = Column::create(Column::ROWNUM, $table);
		$column->setLabel("Label rownum");
		$column = Column::create(Column::CREATED_BY, $table);
		$column->setLabel("Label 17");
		$column = Column::create(Column::CREATED_ON, $table);
		$column->setLabel("Label 18");
		$column = Column::create(Column::UPDATED_BY, $table);
		$column->setLabel("Label 19");
		$column = Column::create(Column::UPDATED_ON, $table);	// [19]
		$column->setLabel("Label 20");
		$column = Column::create(Column::LABEL, $table);
		$column->setLabel("Label 21");	//実際は設定できない
		$column->setLabelHtml("<p>abc</p>");
		$column = Column::create(Column::SPACE, $table);
		$column->setLabel("Label 22");	//実際は設定できない
		$column = Column::create(Column::LINE, $table);		// [22]
		$column->setLabel("Label 23");	//実際は設定できない

		$column = Column::create(Column::LOOKUP, $table);	// [23]
		$column->setLabel("Label lookup (number)");
		$column->setReferenceTable($referenceTable);
		$column->setReferenceTargetColumn($referenceTable->getColumn(2));
		$column->setReferenceColumns(array($referenceTable->getColumn(0), $referenceTable->getColumn(1)));
		$column->setReferenceCopyMap(array(array($referenceTable->getColumn(0), $column1)));

		$column = Column::create(Column::USER_SELECTOR, $table);	// [24]
		$column->setLabel("Label 24");

		$column = Column::create(Column::DATECALC, $table);//[25]
		$column->setExpression('3');
		$column->setDateFormat(Column__Datecalc::FORMAT_DATE);
		$column->setDatecalcUnit('month');
		$column->setDatecalcBeforeAfter('after');
		$column->setDateSourceColumn($column_date);
		$column->setLabel("Label 25");

		$table->setViewColumns(array($column1, $column7, $column8));
		return $table;
	}

    /**
     * @param $id
     * @param string $name
     * @param bool $isAdmin
     * @throws TxCoreException
     */
    public function createUser($id, $name = 'sample user', $isAdmin= false)
    {
        $adam = new User();
        $adam->setRole(User::ADMIN);

        $user = new User();
        $user->_setId($id);
        $user->setLoginId($id);
        $user->setPassword($id);
        $user->setScreenName($name);
        $user->setRole($isAdmin ? USer::ADMIN : User::USER);
        $this->target->createUser($user, $adam);
        return $user;
    }


	public function mockUpTable($name = 'sample table')
	{
		$icons = $this->target->getIcons();
		$table = new Table();
		$table->setIcon($icons[0]);
		$table->setName($name);
// 		$table->setPublic($as_public);
		return $table;

// 		if ($columns === 'full' ||
// 			@is_array('text', $columns, true)) {
// 			$column = $table->addColumn(Column::TEXT);
// 			$column->setLabel("Label 1");
// 			$column->setCode($column->getTypeName());
// 		}

// 		if ($columns === 'full' ||
// 			@is_array('text', $columns, true)) {
// 			$column = $table->addColumn(Column::MULTITEXT);
// 			$column->setLabel("Label 2");
// 			$column->setCode($column->getTypeName());

// 		$column = $table->addColumn(Column::RICHTEXT);
// 		$column->setLabel("Label 3");
// 		$column->setCode($column->getTypeName());

// 		$column = $table->addColumn(Column::NUMBER);
// 		$column->setLabel("Label 4");

// 		$column = $table->addColumn(Column::CALC);
// 		$column->setLabel("Label 5");
// 		$column->setCode($column->getTypeName());

// 		$column = new RadioColumn();
// 		$column->setLabel("Label 6");
// 		$column->setCode($column->getTypeName());

// 		$column7 = $table->addColumn(Column::CHECKBOX);
// 		$column7->setLabel("Label 7");
// 		$column->setCode($column->getTypeName());
// 		$option7_1 = new Option();
// 		$option7_1->setName("New Dropdown 1");
// 		$column7->_addOption($option7_1);
// 		$option7_2 = new Option();
// 		$option7_2->setName("New Dropdown 2");
// 		$column7->_addOption($option7_2);
// 		$column7->setDefaultValue(array($option7_1->getId(), $option7_2->getId()));	// 初期選択

// 		$column8 = $table->addColumn(Column::DROPDOWN);
// 		$column8->setLabel("Label 8");
// 		$option8_1 = new Option();
// 		$option8_1->setName("New Option 1");
// 		$column8->_addOption($option8_1);
// 		$option8_2 = new Option();
// 		$option8_2->setName("New Option 2");
// 		$column8->_addOption($option8_2);
// 		$column8->setDefaultValue($option8_2->getId());	// 初期選択

// 		$column = $table->addColumn(Column::DATE);
// 		$column->setLabel("Label 9");

// 		$column = $table->addColumn(Column::TIME);
// 		$column->setLabel("Label 10");

// 		$column = $table->addColumn(Column::DATETIME);
// 		$column->setLabel("Label 11");

// 		$column = $table->addColumn(Column::FILE);
// 		$column->setLabel("Label 12");
// 		$column->setThumbnailMaxSize("200");

// 		$column = $table->addColumn(Column::LINK);
// 		$column->setLabel("Label 13");
// 		$column->setLinkType('email');

// 		$column = $table->addColumn(Column::RELATIONAL);
// 		$column->setLabel("Label 14");

// 		$reference = new Reference();
// 		$reference->setTable($referenceTable);
// 		$reference->setTargetColumn($referenceTable->getColumn(0));
// 		$reference->setSourceColumn($column1);
// 		$reference->setColumns(array($referenceTable->getColumn(0), $referenceTable->getColumn(1)));
// 		$reference->setSortColumn($referenceTable->getColumn(0));
// 		$reference->setSortsReverse(true);
// 		$reference->setMaxRow(100);
// 		$reference->setCopyMap(array(array($referenceTable->getColumn(0), $column1)));
// 		$column->setReference($reference);

// 		$column = $table->addColumn(Column::LOOKUP);
// 		$column->setLabel("Label 15");
// 		$column = $table->addColumn(Column::ROWNUM);
// 		$column->setLabel("Label 16");
// 		$column = $table->addColumn(Column::CREATED_BY);
// 		$column->setLabel("Label 17");
// 		$column = $table->addColumn(Column::CREATED_ON);
// 		$column->setLabel("Label 18");
// 		$column = $table->addColumn(Column::UPDATED_BY);
// 		$column->setLabel("Label 19");
// 		$column = $table->addColumn(Column::UPDATED_ON);
// 		$column->setLabel("Label 20");

// 		$column = $table->addColumn(Column::LABEL);
// 		$column->setLabelHtml("<p>abc</p>");

// 		$column = $table->addColumn(Column::SPACE);

// 		$column = $table->addColumn(Column::LINE);

// 		return $table;
	}

	public function assertSearchHit(Column $column, $term, $hit)
	{
		$criteria = new Criteria();
		if ($hit === false) {
			try {
				$criteria->addCondition($column, $term[0], $term[1]);
				$column->getParent()->search($criteria, $column->getParent()->getOwner());
				$this->fail('Should not be successful');
			} catch (TxCoreException $e) {
			}
		} else {
			$criteria->addCondition($column, $term[0], $term[1]);
			$list = $column->getParent()->search($criteria, $column->getParent()->getOwner());
			$this->assertIdentical($hit, $list->getTotalRowCount());
		}
	}

	protected function getNextRowno(Table $table)
	{
		$list = $this->db->query('select get_new_row_no(?) as val',
			array($table->getId()));
		return intval($list[0]->val) + 1;
	}

	private static function deleteDir($dirPath, $top = true)
	{
		if (is_empty($dirPath) || !is_dir($dirPath)) {
			throw new \InvalidArgumentException("$dirPath must be a directory");
		}
		if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
			$dirPath .= '/';
		}
		$files = glob($dirPath . '*', GLOB_MARK);
		foreach ($files as $file) {
			if (is_dir($file)) {
				self::deleteDir($file, false);
			} else {
				unlink($file);
			}
		}
		if (!$top)
			rmdir($dirPath);
	}

	protected function assertIdentical($expected, $actual, $message = null)
	{
		$this->assertSame($expected, $actual, $message);
	}

	protected function assertNotIdentical($expected, $actual, $message = null)
	{
		$this->assertNotSame($expected, $actual, $message);
	}

	protected function assertPattern($pattern, $string, $message = '')
	{
		$this->assertRegExp($pattern, $string, $message);
	}

	protected function assertNoPattern($pattern, $string, $message = '')
	{
		$this->assertNotRegExp($pattern, $string, $message);
	}

	protected function pass($message = null)
	{
		// passing the test
	}
}
