<?php
namespace TxCore\Test;

use TxCore\Column;
use TxCore\Criteria;
use TxCore\Math\MathContext;

class Engine_workTest extends BaseTestCase
{
	public function setUp()
	{
        parent::setUpDB(__DIR__.'/EngineTest.sql');
	}

	public function testSearchTable_集計関数_カウント()
	{
		$by = $this->target->getUser(1001);
		$table = $this->mockUpTable();
		$column_0 = $table->addColumn(Column::CHECKBOX);
		$option_0 = $column_0->addOption();
		$option_0->setName('Option1');
		$option_1 = $column_0->addOption();
		$option_1->setName('Option2');
		$option_2 = $column_0->addOption();
		$option_2->setName('Option3');
		$table->create($by);
	
		$row = $table->addRow();
		$row->setValue($column_0, array($option_0, $option_1));
		$row = $table->addRow();
		$row->setValue($column_0, array($option_0));
		$row = $table->addRow();
		$row->setValue($column_0, array());
		$row = $table->addRow();
		$row->setValue($column_0, array($option_1, $option_2));
		$table->insertRows($by);

		$criteria = new Criteria();
		$criteria->notEquals($table->getRownum(), 4);
		$criteria->setSubtotal($column_0, Criteria::FN_COUNT);
		$list = $table->search($criteria, $by);
	
		$this->assertIdentical(3, $list->getTotalRowCount());
		$this->assertIdentical(2, count($list->getColumns()));
//		$this->assertIdentical("レコード数", $list->getColumn(1)->getLabel());
		$this->assertIdentical("データ数", $list->getColumn(1)->getLabel()); // 名称変更
		$this->assertIdentical('Option1', $list[0]->getText($list->getColumn(0)));
		$this->assertIdentical('2', $list[0]->getValue($list->getColumn(1)));
		$this->assertIdentical('Option2', $list[1]->getText($list->getColumn(0)));
		$this->assertIdentical('1', $list[1]->getValue($list->getColumn(1)));
		$this->assertIdentical('Option3', $list[2]->getText($list->getColumn(0)));
		$this->assertIdentical('0', $list[2]->getValue($list->getColumn(1)));
		
	}
	
	public function testSearchTable_集計関数_合計()
	{
		$by = $this->target->getUser(1001);
		$table = $this->mockUpTable();
		$column_0 = $table->addColumn(Column::CHECKBOX);
		$option_0 = $column_0->addOption();
		$option_0->setName('Option1');
		$option_1 = $column_0->addOption();
		$option_1->setName('Option2');
		$option_2 = $column_0->addOption();
		$option_2->setName('Option3');
		
		$column_1 = $table->addColumn(Column::NUMBER);
		$table->setRoundScale(2);
		$table->setRoundType(MathContext::ROUND_UP);
		$table->create($by);
	
		$row = $table->addRow();
		$row->setValue($column_0, array($option_0, $option_1));
		$row->setValue($column_1, 1.111);
		$row = $table->addRow();
		$row->setValue($column_0, array($option_0));
		$row->setValue($column_1, 2.222);
		$row = $table->addRow();
		$row->setValue($column_0, array());
		$row->setValue($column_1, 3.333);
		$row = $table->addRow();
		$row->setValue($column_0, array($option_1, $option_2));	// 条件により除外される
		$row->setValue($column_1, 4.444);
		$row = $table->addRow();
		$row->setValue($column_0, array($option_0));
		$row->setValue($column_1, null);	// NVLが効いていることのテスト
		$table->insertRows($by);
	
		$criteria = new Criteria();
		$criteria->notEquals($table->getRownum(), 4);
		$criteria->setSubtotal($column_0, Criteria::FN_SUM, $column_1);
		$list = $table->search($criteria, $by);
	
		$this->assertIdentical(3, $list->getTotalRowCount());
		$this->assertIdentical(2, count($list->getColumns()));
		$this->assertIdentical('Option1', $list[0]->getText($list->getColumn(0)));
		$this->assertIdentical('3.35', $list[0]->getValue($list->getColumn(1)));
		$this->assertIdentical('Option2', $list[1]->getText($list->getColumn(0)));
		$this->assertIdentical('1.12', $list[1]->getValue($list->getColumn(1)));
		$this->assertIdentical('Option3', $list[2]->getText($list->getColumn(0)));
		$this->assertIdentical('0', $list[2]->getValue($list->getColumn(1)));
	}
	
	public function testSearchTable_集計関数_日数()
	{
		
	}
	
	
}
