<?php
namespace TxCore\Test;

use TxCore\Column;
use TxCore\Criteria;
use TxCore\File;
use TxCore\LookupValue;
use TxCore\Math\MathContext;
use TxCore\Row;
use TxCore\Table;
use TxCore\TxCoreException;

class RowTest extends BaseTestCase
{
	public function setUp()
	{
        parent::setUpDB(__DIR__.'/EngineTest.sql');
	}

	public function testSerachReferal_関連レコードの検索()
	{
		$byAdmin = $this->target->getUser(1002);
		$icons = $this->target->getIcons();
	
		$table1 = new Table();
		$table1->setName('Activities');
		$table1->setIcon($icons[0]);
		$column1_1 = $table1->addColumn(Column::NUMBER);
		$column1_1->setLabel("Staff No");
		$column1_2 = $table1->addColumn(Column::DATE);
		$column1_2->setLabel("Date");
		$column1_3 = $table1->addColumn(Column::MULTITEXT);
		$column1_3->setLabel("Action");
		$table1->create($byAdmin);
	
		for ($i = 1; $i <= 2; $i++) {
			for ($j = 0; $j < 50; $j++) {
				$row = new Row($table1);
				$row->setValue($column1_1, $i);
				$row->setValue($column1_2, date('Y-m-d', strtotime('2014-01-01 + '.$j.'days')));
				$row->setValue($column1_3, "memo of my activity");
				$table1->insertRows($byAdmin);
			}
		}
	
		$table2 = new Table();
		$table2->setName('Staff');
		$table2->setIcon($icons[0]);
		$column2_1 = $table2->addColumn(Column::NUMBER);
		$column2_1->setLabel("Staff No");
// var_dump($column2_1->getParent());
		$column2_2 = $table2->addColumn(Column::RELATIONAL);
		$column2_2->setLabel("Latest activities");
		$column2_2->setReferenceTable($table1);
		$column2_2->setReferenceMaxRow(10);
		$column2_2->setReferenceSortColumn($column1_2);
		$column2_2->setReferenceSortsReverse(true);
		$column2_2->setReferenceSourceColumn($column2_1);
		$column2_2->setReferenceTargetColumn($column1_1);
		$column2_2->setReferenceColumns(array($column1_2, $column1_3));
// 		$column2_2->setReference($reference);
// 		$table2->addColumn_old($column2_2);
		$table2->create($byAdmin);
	
		$row = $table2->addRow();
		$row->setValue($column2_1, 2);
		$table2->insertRows($byAdmin);
	
		// 参照、検索
		$list = $row->serachReferal($column2_2, $byAdmin, null, 1);
		$this->assertIdentical(2, count($list->getColumns()));
		$this->assertIdentical(1, $list->getOffset());
		$this->assertIdentical(10, $list->getRowCount());
		$this->assertIdentical(50, $list->getTotalRowCount());
		$this->assertIdentical('2', $list[0]->getValue($table1->getColumn(0)));
		$this->assertIdentical('2014-02-18', $list[0]->getValue($table1->getColumn(1)));
		$this->assertIdentical('2', $list[1]->getValue($table1->getColumn(0)));
		$this->assertIdentical('2014-02-17', $list[1]->getValue($table1->getColumn(1)));
	}

	public function testGetText_リッチテキスト()
	{
		$byAdmin = $this->target->getUser(1002);
		$icons = $this->target->getIcons();
		
		$table = new Table();
		$table->setName('New table');
		$table->setIcon($icons[0]);
		$column_1 = $table->addColumn(Column::TEXT);
		$column_2 = $table->addColumn(Column::MULTITEXT);
		$column_3 = $table->addColumn(Column::RICHTEXT);
		$table->create($byAdmin);
		
		$row = $table->addRow();
		$row->setValue($column_3, str_repeat('A', 200));

		$this->assertIdentical(str_repeat('A', 100).'...', $row->getText($column_3));
	}
	
	public function testSerachRelational_ルックアップの検索_数値の場合()
	{
		$text = Column::TEXT;	// ClassLoading
	
		$byAdmin = $this->target->getUser(1002);
		$icons = $this->target->getIcons();
	
		$table1 = new Table();
		$table1->setName('Activities');
		$table1->setIcon($icons[0]);
		$column1_1 = $table1->addColumn(Column::NUMBER);
		$column1_1->setLabel("Staff No");
		$column1_2 = $table1->addColumn(Column::DATE);
		$column1_2->setLabel("Date");
		$column1_3 = $table1->addColumn(Column::MULTITEXT);
		$column1_3->setLabel("Action");
		$table1->create($byAdmin);
	
		$row = $table1->addRow();
		$row->setValue($column1_1, 1);
		$row = $table1->addRow();
		$row->setValue($column1_1, 2);
		$row = $table1->addRow();
		$row->setValue($column1_1, 2);
		
		$table1->insertRows($byAdmin);
	
		$table2 = new Table();
		$table2->setName('Staff');
		$table2->setIcon($icons[0]);
		$column2_1 = $table2->addColumn(Column::NUMBER);
		$column2_1->setLabel("Staff No");
		$column2_2 = $table2->addColumn(Column::LOOKUP);
		$column2_2->setLabel("Latest activities");
		$column2_2->setReferenceTable($table1);
		$column2_2->setReferenceTargetColumn($column1_1);
		$column2_2->setReferenceColumns(array($column1_2, $column1_3));
		$table2->create($byAdmin);
	
		// ルックアップ（一意に特定できる）
		$lookup = new LookupValue();
		$lookup->setValue(1);
		
		$row = $table2->addRow();
		$row->setValue($column2_2, $lookup);
		
		$list = $row->serachReferal($column2_2, $byAdmin, null, 0);
		$this->assertIdentical(1, $list->getTotalRowCount());
	}
	
	public function testGetText_数値のフォーマット()
	{
		$table = $this->mockUpTable();
		$column_0 = $table->addColumn(Column::NUMBER);
		$column_0->setCode('A');
		$column_1 = $table->addColumn(Column::NUMBER);
		$column_1->setCode('B');
		$column_1->setShowsDigit(true);
// 		$column_2 = $table->addColumn(Column::CALC);
// 		$column_2->setExpression('A*2');
// 		$column_2->setShowsDigit(true);
// 		$table->addColumn_old($column_2);
		$table->create($this->target->getUser(1001));
		$row = $table->addRow();
		$row->setValue($column_0, '1000');
		$row->setValue($column_1, '1000.2');
		$row->calculate($table);
	
		$this->assertIdentical('1000', $row->getText($column_0));
		$this->assertIdentical('1,000.2', $row->getText($column_1));
	}
	
	public function testValidate_必須チェック()
	{
		$by = $this->target->getUser(1001);
		$refTable = $this->mockUpTable();
		$refTable->create($by);
		
		$table = $this->mockUpTable();
		for ($i = 0; $i < 2; $i++) {
			$column = $table->addColumn(Column::TEXT);
			$column->setRequired($i == 0);
			$column->setLabel($column->getTypeName().'_'.$i);
			$column = $table->addColumn(Column::MULTITEXT);
			$column->setRequired($i == 0);
			$column->setLabel($column->getTypeName().'_'.$i);
			$column = $table->addColumn(Column::RICHTEXT);
			$column->setRequired($i == 0);
			$column->setLabel($column->getTypeName().'_'.$i);
			$column = $table->addColumn(Column::NUMBER);
			$column->setRequired($i == 0);
			$column->setLabel($column->getTypeName().'_'.$i);
			$column = $table->addColumn(Column::RADIO);
			$column->setRequired($i == 0);
			$column->setLabel($column->getTypeName().'_'.$i);
			$column = $table->addColumn(Column::CHECKBOX);
			$column->setRequired($i == 0);
			$column->setLabel($column->getTypeName().'_'.$i);
			$column = $table->addColumn(Column::DROPDOWN);
			$column->setRequired($i == 0);
			$column->setLabel($column->getTypeName().'_'.$i);
			$column = $table->addColumn(Column::DATE);
			$column->setRequired($i == 0);
			$column->setLabel($column->getTypeName().'_'.$i);
			$column = $table->addColumn(Column::TIME);
			$column->setRequired($i == 0);
			$column->setLabel($column->getTypeName().'_'.$i);
			$column = $table->addColumn(Column::DATETIME);
			$column->setRequired($i == 0);
			$column->setLabel($column->getTypeName().'_'.$i);
			$column = $table->addColumn(Column::FILE);
			$column->setRequired($i == 0);
			$column->setLabel($column->getTypeName().'_'.$i);
			$column = $table->addColumn(Column::LINK);
			$column->setRequired($i == 0);
			$column->setLabel($column->getTypeName().'_'.$i);
			$column = $table->addColumn(Column::LOOKUP);
			$column->setRequired($i == 0);
			$column->setLabel($column->getTypeName().'_'.$i);
			$column->setReferenceTable($refTable);
			$column->setReferenceTargetColumn($refTable->getColumn(0));
		}
		
		$table->create($by);
		$row = $table->addRow();
		$messages = $row->validate($by);
	
		$this->assertIdentical(13, count($messages));
//		$this->assertIdentical('文字列_0', $messages[0]->getContext()->getLabel());
		$this->assertIdentical('文字列（一行）_0', $messages[0]->getContext()->getLabel()); // 名称変更
//		$this->assertIdentical('文字列_0を入力してください。', $messages[0]->getMessage());
		$this->assertIdentical('文字列（一行）_0を入力してください。', $messages[0]->getMessage()); // 名称変更
	}
	
	public function testValidate_数値チェック()
	{
		$by = $this->target->getUser(1001);
		
		$table = $this->mockUpTable();
		$column_1 = $table->addColumn(Column::NUMBER);
		$column_1->setRequired(false);
		$column_1->setLabel('数値_0');
		$column_2 = $table->addColumn(Column::NUMBER);
		$column_2->setRequired(false);
		$column_2->setLabel('数値_1');
		$table->create($by);
		
		$row = $table->addRow();
		$row->setValue($column_1, "a");
		$row->setValue($column_2, "あ");
		$messages = $row->validate($by);
	
		$this->assertIdentical(2, count($messages));
		$this->assertIdentical('数値_0', $messages[0]->getContext()->getLabel());
		$this->assertIdentical('数値_0は数字で入力してください。', $messages[0]->getMessage());
	}
	
	public function testValidate_文字数範囲()
	{
		$by = $this->target->getUser(1001);
		
		$table = $this->mockUpTable();
		$column_1 = $table->addColumn(Column::TEXT);
		$column_1->setRequired(false);
		$column_1->setLabel('文字列_0');
		$column_1->setMaxLength(10);
		$column_2 = $table->addColumn(Column::LINK);
		$column_2->setRequired(false);
		$column_2->setLabel('リンク_1');
		$column_2->setMinLength(3);
		$table->create($by);
	
		$row = $table->addRow();
		$row->setValue($column_1, "12345678901");
		$row->setValue($column_2, "12");
		$messages = $row->validate($by);
	
		$this->assertIdentical(2, count($messages));
		$this->assertIdentical('文字列_0', $messages[0]->getContext()->getLabel());
		$this->assertIdentical('文字列_0は10文字以内で入力してください。', $messages[0]->getMessage());
		$this->assertIdentical('リンク_1', $messages[1]->getContext()->getLabel());
		$this->assertIdentical('リンク_1は3文字以上で入力してください。', $messages[1]->getMessage());
		
		// エラーにならないケース
		$row = $table->addRow();
		$row->setValue($column_1, "1234567890");
		$row->setValue($column_2, "123");
		$messages = $row->validate($by);
		$this->assertIdentical(0, count($messages));
	}
	
	public function testValidate_ユニーク制約()
	{
	$by = $this->target->getUser(1001);
		
		$table = $this->mockUpTable();
		$column_1 = $table->addColumn(Column::TEXT);
		$column_1->setLabel("文字列");
		$column_1->setUnique(true);
		$table->create($by);
	
		$row = $table->addRow();
		$row->setValue($column_1, "a");
		$messages = $row->validate($by);
		$this->assertIdentical(0, count($messages));
		$table->insertRows($by);
		
		$row = $table->addRow();
		$row->setValue($column_1, "a");
		$messages = $row->validate($by);
		$this->assertIdentical(1, count($messages));
		$this->assertIdentical("文字列", $messages[0]->getContext()->getLabel());
	}
	
	public function testValidate_数値の桁数チェック()
	{
		$by = $this->target->getUser(1001);
		
		$table = $this->mockUpTable();
		$column_1 = $table->addColumn(Column::NUMBER);
		$column_1->setLabel("数値_0");
		$table->create($by);
	
		$row = $table->addRow();
		$row->setValue($column_1, "99999999999999999999.9999999999");
		$messages = $row->validate($by);
		$this->assertIdentical(0, count($messages));
		
		$row = $table->addRow();
		$row->setValue($column_1, "-99999999999999999999.9999999999");
		$messages = $row->validate($by);
		$this->assertIdentical(0, count($messages));
		
		$row = $table->addRow();
		$row->setValue($column_1, "999999999999999999999.9999999999");
		$messages = $row->validate($by);
		$this->assertIdentical(1, count($messages));
		$this->assertIdentical("数値_0", $messages[0]->getContext()->getLabel());
		
		$row = $table->addRow();
		$row->setValue($column_1, "99999999999999999999.99999999999");
		$messages = $row->validate($by);
		$this->assertIdentical(1, count($messages));
		$this->assertIdentical("数値_0", $messages[0]->getContext()->getLabel());
	}
	
	public function testGetText_未設定値()
	{
// 		$table = $this->target->getTable(1003);
		$table = $this->mockUpTableFully();
		$table->create($this->target->getUser(1001));
		
		$row = $table->addRow();
		$row->setValue($table->getColumn(5), $table->getColumn(5)->getOption(0));	// ラジオボタンは未設定にできないので設定
		$table->insertRows($table->getOwner());
		$row = $table->getRowFor($row->getId());
		
		$this->assertIdentical(null, $row->getValue($table->getColumn(0)));
		$this->assertIdentical(null, $row->getText($table->getColumn(0)));
		$this->assertIdentical(null, $row->getValue($table->getColumn(1)));
		$this->assertIdentical(null, $row->getText($table->getColumn(1)));
		$this->assertIdentical(null, $row->getValue($table->getColumn(2)));
		$this->assertIdentical(null, $row->getText($table->getColumn(2)));
		$this->assertIdentical(null, $row->getValue($table->getColumn(3)));	// number
		$this->assertIdentical(null, $row->getText($table->getColumn(3)));
		$this->assertIdentical(null, $row->getValue($table->getColumn(4)));	// calc
		$this->assertIdentical(null, $row->getText($table->getColumn(4)));
// 		$this->assertIdentical(null, $row->getValue($table->getColumn(5)));	// radio
// 		$this->assertIdentical(null, $row->getText($table->getColumn(5)));
		$this->assertIdentical(array(), $row->getValue($table->getColumn(6)));	// checkbox （存在しない option を value が参照
		$this->assertIdentical(array(), $row->getText($table->getColumn(6)));
		$this->assertIdentical(null, $row->getValue($table->getColumn(7)));	// Dropdown（未選択）- nullではなく空配列
		$this->assertIdentical(null, $row->getText($table->getColumn(7)));
		$this->assertIdentical(null, $row->getValue($table->getColumn(8)));
		$this->assertIdentical(null, $row->getText($table->getColumn(8)));
		$this->assertIdentical(null, $row->getValue($table->getColumn(9)));
		$this->assertIdentical(null, $row->getText($table->getColumn(9)));
		$this->assertIdentical(null, $row->getValue($table->getColumn(10)));
		$this->assertIdentical(null, $row->getText($table->getColumn(10)));
		$this->assertIdentical(array(), $row->getValue($table->getColumn(11)));
		$this->assertIdentical(array(), $row->getText($table->getColumn(11)));
		$this->assertIdentical(null, $row->getValue($table->getColumn(12)));	// リンク
		$this->assertIdentical(null, $row->getText($table->getColumn(12)));
		$this->assertIdentical(null, $row->getValue($table->getColumn(13)));	// ルックアップ
		$this->assertIdentical(null, $row->getText($table->getColumn(13)));
		// レコード番号、作成者などはNullにならないのでスキップ
		$this->assertNotNull($row->getValue($table->getColumn(23)));	// ルックアップ（Number型）
		$this->assertIdentical(null, $row->getValue($table->getColumn(23))->getValue());	// ルックアップ（Number型）
		$this->assertIdentical(null, $row->getText($table->getColumn(23)));
	}

	public function testUpdate_データの更新()
	{
		$byAdmin = $this->target->getUser(1002);
		$table = $this->mockUpTableFully();
		$table->create($byAdmin);
		
		$row = $table->addRow();
		$row->setValue($table->getColumn(0), 'text');
		$row->setValue($table->getColumn(1), 'multitext');
		$row->setValue($table->getColumn(2), '<p>richtext</p>');
		$row->setValue($table->getColumn(3), '120');
		$row->setValue($table->getColumn(5), $table->getColumn(5)->getOption(0));
		$row->setValue($table->getColumn(6), array($table->getColumn(6)->getOption(0)));
		$row->setValue($table->getColumn(7), $table->getColumn(7)->getOption(0));
		$row->setValue($table->getColumn(8), '2014-01-01');
		$row->setValue($table->getColumn(9), '12:11');
		$row->setValue($table->getColumn(10), '2012-01-01 12:00');
		$row->setValue($table->getColumn(11), array($this->mockUpFile()));
		$row->setValue($table->getColumn(12), 'sample@sample.com');
		$lookup = new LookupValue();
		$row->setValue($table->getColumn(14), $lookup);
		$table->insertRows($byAdmin);
		
		$row->update($byAdmin);

		$this->markTestIncomplete('Do implement');
	}

	public function testGetValue_値とテキストの取得()
	{
		$table = $this->target->getTable(1003);
	
		$criteria = new Criteria();
		$criteria->equals($table->getColumn(15), 16);
		$list = $table->search($criteria, $table->getOwner());
	
		$this->assertIdentical(1, $list->getRowCount());
		$this->assertIdentical(1, $list->getTotalRowCount());
		$this->assertIdentical('table', $list->getType());
		$this->assertIdentical('value 1', $list[0]->getValue($list->getColumn(0)));
		$this->assertIdentical('value 1', $list[0]->getText($list->getColumn(0)));
		$this->assertIdentical("value \n2", $list[0]->getValue($list->getColumn(1)));
		$this->assertIdentical("value \n2", $list[0]->getText($list->getColumn(1)));
		$this->assertIdentical("value <br>2", $list[0]->getHtml($list->getColumn(1)));
		$this->assertIdentical("<font color=red>value 3</font>", $list[0]->getValue($list->getColumn(2)));
		$this->assertIdentical("value 3", $list[0]->getText($list->getColumn(2)));
		$this->assertIdentical("4", $list[0]->getValue($list->getColumn(3)));	// number
		$this->assertIdentical("4", $list[0]->getText($list->getColumn(3)));
		$this->assertIdentical("5", $list[0]->getValue($list->getColumn(4)));	// calc
		$this->assertIdentical("5", $list[0]->getText($list->getColumn(4)));
		$this->assertIdentical(10030601, $list[0]->getValue($list->getColumn(5))->getId());	// radio
		$this->assertIdentical("Option 1-1", $list[0]->getText($list->getColumn(5)));
		$options = $list[0]->getValue($list->getColumn(6));
		$this->assertIdentical(2, count($options));	// checkbox （存在しない option を value が参照）
		$this->assertIdentical(10030704, $options[0]->getId());
		$this->assertIdentical(10030701, $options[1]->getId());
// 		$this->assertIdentical(array("Option 2-1", null), $list[0]->getText($list->getColumn(6)));
		$this->assertIdentical(null, $list[0]->getValue($list->getColumn(7)));	// Dropdown（未選択）- nullではなく空配列
		$this->assertIdentical(null, $list[0]->getText($list->getColumn(7)));
		$this->assertIdentical('2014-01-09', $list[0]->getValue($list->getColumn(8)));
		$this->assertIdentical('2014-01-09', $list[0]->getText($list->getColumn(8)));
		$this->assertIdentical('00:00:10', $list[0]->getValue($list->getColumn(9)));
		$this->assertIdentical('00:00:10', $list[0]->getText($list->getColumn(9)));
		$this->assertIdentical('2014-01-11 00:00', $list[0]->getValue($list->getColumn(10)));
		$this->assertIdentical('2014-01-11 00:00', $list[0]->getText($list->getColumn(10)));
		$files = $list[0]->getValue($list->getColumn(11));	// ファイル
		$this->assertIdentical(1, count($files));
		$this->assertIdentical('filename12.txt', $files[0]->getFileName());
		$this->assertIdentical('text/plain', $files[0]->getContentType());
		$this->assertIdentical('/asset/files/1003/100312/filename12.txt', $files[0]->getUrl());
		$this->assertIdentical(array('filename12.txt'), $list[0]->getText($list->getColumn(11)));
		$this->assertIdentical('value13@sample.com', $list[0]->getValue($list->getColumn(12)));	// リンク
		$this->assertIdentical('value13@sample.com', $list[0]->getText($list->getColumn(12)));
		$this->assertIdentical('<a href="mailto:value13@sample.com">value13@sample.com</a>', $list[0]->getHtml($list->getColumn(12)));
		$lookup  = $list[0]->getValue($list->getColumn(13));	// ルックアップ
		$this->assertIdentical(1010, $lookup->getKey());
		$this->assertIdentical("user15", $lookup->getValue());
		$this->assertIdentical("user15", $list[0]->getText($list->getColumn(13)));	// 常にNull
		$this->assertIdentical('16', $list[0]->getValue($list->getColumn(14)));	// レコード番号
		$this->assertIdentical('16', $list[0]->getText($list->getColumn(14)));
		$createdBy = $list->getRow(0)->getValue($list->getColumn(15));	// 作成者
		$this->assertIdentical(1001, $createdBy->getId());
		$this->assertIdentical('test user01', $list[0]->getText($list->getColumn(15))); // スクリーン名
		$this->assertIdentical('2014-01-18 00:00', $list[0]->getValue($list->getColumn(16)));	// 作成日時
		$this->assertIdentical('2014-01-18 00:00', $list[0]->getText($list->getColumn(16)));
		$createdBy = $list->getRow(0)->getValue($list->getColumn(17));	// 更新者
		$this->assertIdentical(1002, $createdBy->getId());
		$this->assertIdentical('test user02', $list[0]->getText($list->getColumn(17))); // スクリーン名
		$this->assertIdentical('2014-01-20 00:00', $list[0]->getValue($list->getColumn(18)));	// 更新日時
		$this->assertIdentical('2014-01-20 00:00', $list[0]->getText($list->getColumn(18)));
		$lookup  = $list[0]->getValue($list->getColumn(19));	// ルックアップ(Number)
		$this->assertIdentical(1010, $lookup->getKey());
		$this->assertIdentical(15, $lookup->getValue());
		$this->assertIdentical(15, $list[0]->getText($list->getColumn(19)));
	}

	public function testGetValue_チェックボックス並びの正規化()
	{
		$byAdmin = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$column_0 = $table->addColumn(Column::CHECKBOX);
		$option_0 = $column_0->addOption();
		$option_1 = $column_0->addOption();
		$table->create($byAdmin);
	
		$row = $table->addRow();
		$row->setValue($column_0, array($option_1, $option_0));	// 逆順で登録する
		$table->insertRows($byAdmin);
	
		$row = $table->getRowFor($row->getId());
		$options = $row->getValue($column_0);
		$this->assertIdentical(2, count($options));
		$this->assertIdentical($option_0->getId(), $options[0]->getId());	// 正規化されて返る
		$this->assertIdentical($option_1->getId(), $options[1]->getId());
	}
	
	public function testDelete_データベースの更新()
	{
		$byAdmin = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$table->addColumn(Column::TEXT);
		$table->create($byAdmin);
	
		$row = $table->addRow();
		$row->setValue($table->getColumn(0), "text 1");
		$table->insertRows($table->getOwner());
	
		$row_id = $row->getId();
		$row->delete($byAdmin);
	
		$this->assertRegistered('row', array(@id=>$row_id), 0);
		$this->assertRegistered('indexes', array(@row_id=>$row_id), 0);
	}
	
	public function testDelete_ファイルも削除されること()
	{
		$byAdmin = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$column0 = $table->addColumn(Column::FILE);
		$table->create($byAdmin);

		copy(dirname(__FILE__).'/file.xls', dirname(__FILE__).'/asset/temp/file.xls');
		$file = File::create(dirname(__FILE__).'/asset/temp/file.xls', 'ファイル.xls', 'application/excel');
		$row = $table->addRow();
		$row->setValue($table->getColumn(0), array($file));
		$table->insertRows($table->getOwner());
		
		$filePath = dirname(__FILE__).'/asset/files/'.$file->getFilekey();
		$this->assertIdentical(true, file_exists($filePath));
		$row->delete($byAdmin);
		$this->assertIdentical(true, file_exists($filePath));
		$this->target->getStorage()->flush();
		$this->assertIdentical(false, file_exists($filePath));
	}
	
	public function testDelete_レコードが存在しない()
	{
		$byAdmin = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$table->addColumn(Column::NUMBER);
		$table->create($byAdmin);
	
		$row = $table->addRow();
		$row->setValue($table->getColumn(0), 1);
		$table->insertRows($table->getOwner());
	
		$this->db->update('delete from `row` where id =?',
			array($row->getId()));
	
		try {
			$row->delete($byAdmin);
			$this->fail('Should not be successfull');
		} catch (TxCoreException $e) {
			$this->pass();
		}
	}
	
	public function testDelete_権限チェック()
	{
		$by = $this->target->getUser(1001);
		$byOther = $this->target->getUser(1003);
		$byAdmin = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$table->create($by);
	
		$row0 = new Row($table);
		$row1 = new Row($table);
		$table->insertRows($table->getOwner());
	
		try {
			$row0->delete($byOther);
			$this->fail('Should not be successfull');
		} catch (TxCoreException $e) {
		}
	
		$row0->delete($by);
		$row1->delete($byAdmin);
		$this->pass();
	}
	
	
	public function testGetText_計算のフォーマット_時間()
	{
		$byAdmin = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$column_0 = $table->addColumn(Column::CALC);
		$column_0->setExpression('日付');
		$column_0->setFormat('hours');	// 時間
		$column_1 = $table->addColumn(Column::DATE);
		$column_1->setCode('日付');
		$table->create($byAdmin);
	
		$row = $table->addRow();
		$row->setValue($column_1, '1970-01-01');
		$table->insertRows($byAdmin);
	
		$row = $table->getRowFor($row->getId());
		$this->assertIdentical('0時間', $row->getText($column_0));		// 1970-01-01 9:00 が基準になるので
	}
	
	public function testGetText_時間の可算_時間()
	{
		$byAdmin = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$column_0 = $table->addColumn(Column::CALC);
		$column_0->setExpression('時刻_1+時刻_2');
		$column_0->setFormat('time');
		$column_1 = $table->addColumn(Column::TIME);
		$column_1->setCode('時刻_1');
		$column_2 = $table->addColumn(Column::TIME);
		$column_2->setCode('時刻_2');
		$table->create($byAdmin);
	
		$row = $table->addRow();
		$row->setValue($column_1, '10:10');
		$row->setValue($column_2, '02:20');
		$table->insertRows($byAdmin);
	
		$row = $table->getRowFor($row->getId());
		$this->assertIdentical('12:30', $row->getText($column_0));
	}
	
	public function testGetText_計算のフォーマット_時刻の計算結果を時間変換()
	{
		$byAdmin = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$column_0 = $table->addColumn(Column::CALC);
		$column_0->setExpression('日付2-日付1');
		$column_0->setFormat('hours');	// 時間
		$column_1 = $table->addColumn(Column::TIME);
		$column_1->setCode('日付1');
		$column_2 = $table->addColumn(Column::TIME);
		$column_2->setCode('日付2');
		$table->create($byAdmin);
	
		$row = $table->addRow();
		$row->setValue($column_1, '9:00');
		$row->setValue($column_2, '23:00');
		$table->insertRows($byAdmin);
	
		$row = $table->getRowFor($row->getId());
		$this->assertIdentical('14時間', $row->getText($column_0));
	}
	
	public function testGetText_計算のフォーマット_時刻を時間変換()
	{
		$byAdmin = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$column_0 = $table->addColumn(Column::CALC);
		$column_0->setExpression('日付');
		$column_0->setFormat('hours');	// 時間
		$column_1 = $table->addColumn(Column::TIME);
		$column_1->setCode('日付');
		$table->create($byAdmin);
	
		$row = $table->addRow();
		$row->setValue($column_1, '10:10');
		$table->insertRows($byAdmin);
	
		$row = $table->getRowFor($row->getId());
		$this->assertIdentical('10時間', $row->getText($column_0));// 1970-01-01 9:00 が基準になるので 10-9
	}
	
	public function testGetText_計算のフォーマット_日時を日時変換()
	{
		$byAdmin = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$column_0 = $table->addColumn(Column::CALC);
		$column_0->setExpression('日付_1');
		$column_0->setFormat('datetime');	// 日時
		$column_0->setCode("計算");
		$column_1 = $table->addColumn(Column::DATETIME);
		$column_1->setCode('日付_1');
		$table->create($byAdmin);
	
		$row = $table->addRow();
		$row->setValue($column_1, '2012-01-01 10:01');
		$table->insertRows($byAdmin);
		$row = $table->getRowFor($row->getId());
		$this->assertIdentical('2012-01-01 10:01', $row->getText($column_0));	// 同じであればOK
	}
	
	public function testGetText_計算のフォーマット_日付を日時変換()
	{
		$byAdmin = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$column_0 = $table->addColumn(Column::CALC);
		$column_0->setExpression('日付_1');
		$column_0->setFormat('datetime');	// 日時
		$column_1 = $table->addColumn(Column::DATE);
		$column_1->setCode('日付_1');
		$table->create($byAdmin);
	
		$row = $table->addRow();
		$row->setValue($column_1, '2012-01-01');
		$table->insertRows($byAdmin);
		$row = $table->getRowFor($row->getId());
		$this->assertIdentical('2012-01-01 00:00', $row->getText($column_0));	// 同じであればOK
	}
	
	public function testGetText_計算のフォーマット_日付を日数変換()
	{
		$byAdmin = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$table->setRoundScale(1);
		$table->setRoundType(MathContext::ROUNDHALF_UP);
		$column_0 = $table->addColumn(Column::CALC);
		$column_0->setExpression('日付_1');
		$column_0->setFormat('days');	// 日時
		$column_1 = $table->addColumn(Column::DATE);
		$column_1->setCode('日付_1');
		$table->create($byAdmin);
	
		$row = $table->addRow();
		$row->setValue($column_1, '1970-01-02');
		$table->insertRows($byAdmin);
		$row = $table->getRowFor($row->getId());
		$this->assertIdentical('1日', $row->getText($column_0));
	}
	
	public function testValidate_日時の形式が不正()
	{
		$byAdmin = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$table->create($byAdmin);
		$column_0 = $table->addColumn(Column::DATETIME);
		$column_0->setLabel('<日時>');
		$column_0->setCode('日時');
	
		$row = $table->addRow();
		$row->setValue($column_0, 'x');
		$errors = $row->validate();
	
		$this->assertIdentical(1, count($errors));
		$this->assertIdentical('<日時>は日時の形式で入力してください。', $errors[0]->getMessage());
	}
	
	public function testValidate_日付の形式が不正()
	{
		$byAdmin = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$table->create($byAdmin);
		$column_0 = $table->addColumn(Column::DATE);
		$column_0->setLabel('<日付>');
		$column_0->setCode('日付');
	
		$row = $table->addRow();
		$row->setValue($column_0, 'x');
		$errors = $row->validate();
	
		$this->assertIdentical(1, count($errors));
		$this->assertIdentical('<日付>は日付の形式で入力してください。', $errors[0]->getMessage());
	}
	
	
	public function testValidate_時刻の形式が不正()
	{
		$byAdmin = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$table->create($byAdmin);
		$column_0 = $table->addColumn(Column::TIME);
		$column_0->setLabel('<時刻>');
		$column_0->setCode('時刻');
	
		$row = $table->addRow();
		$row->setValue($column_0, 'x');
		$errors = $row->validate();
	
		$this->assertIdentical(1, count($errors));
		$this->assertIdentical('<時刻>は時刻の形式で入力してください。', $errors[0]->getMessage());
	}
	
	public function testValidate_メールアドレスが不正()
	{
		$byAdmin = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$table->create($byAdmin);
		$column_0 = $table->addColumn(Column::LINK);
		$column_0->setLabel('<メアド>');
		$column_0->setCode('メアド');
		$column_0->setLinkType('email');
	
		$row = $table->addRow();
		$row->setValue($column_0, 'x');
		$errors = $row->validate();
	
		$this->assertIdentical(1, count($errors));
		$this->assertIdentical('<メアド>はメールアドレスを入力してください。', $errors[0]->getMessage());
	}
	
	public function testValidate_ユーアールエルが不正()
	{
		$byAdmin = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$table->create($byAdmin);
		$column_0 = $table->addColumn(Column::LINK);
		$column_0->setLabel('<URL>');
		$column_0->setCode('URL');
		$column_0->setLinkType('url');
	
		$row = $table->addRow();
		$row->setValue($column_0, 'x');
		$errors = $row->validate();
	
		$this->assertIdentical(1, count($errors));
		$this->assertIdentical('<URL>は http:// か https:// で始まるURLを入力してください。', $errors[0]->getMessage());
	}
}
