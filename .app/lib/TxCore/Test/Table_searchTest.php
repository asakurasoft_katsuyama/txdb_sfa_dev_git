<?php
namespace TxCore\Test;

use TxCore\Column;
use TxCore\Criteria;
use TxCore\File;
use TxCore\Row;
use TxCore\TxCoreException;
use TxCore\User;

class Table_searchTest extends BaseTestCase
{
	public function setUp()
	{
        parent::setUpDB(__DIR__.'/EngineTest.sql');
	}

	public function testSearchTable_ビュー設定あり()
	{
		$table = $this->target->getTable(3001);
		$row = new Row($table);
		$row->setValue($table->getColumn(0), 'a');
		$row->setValue($table->getColumn(1), 'b');
		$row->setValue($table->getColumn(2), 'c');
		$table->insertRow($row, $table->getOwner());
		
		$criteria = new Criteria();
		$list = $table->search($criteria, $table->getOwner());
		$this->assertIdentical(4, count($list->getColumns()));
		$this->assertIdentical('text', $list->getColumn(0)->getType());
		$this->assertIdentical('multitext', $list->getColumn(1)->getType());
		$this->assertIdentical('richtext', $list->getColumn(2)->getType());
		$this->assertIdentical('a', $list[0]->getValue($table->getColumn(0)));
		$this->assertIdentical('b', $list[0]->getValue($table->getColumn(1)));
		$this->assertIdentical('c', $list[0]->getValue($table->getColumn(2)));
	}
	
	public function testSearchTable_非ソート可能フィールドが指定された場合()
	{
		$table = $this->target->getTable(3001);
		
		foreach ($table->getColumns() as $column) {	/* @var $column \TxCore\Column */
			if (in_array($column->getType(), array('text', 'number', 'calc', 'radio', 'dropdown',
				'date', 'time', 'datetime', 'link', 'lookup', 'rownum',
				'created_by',
				'created_on',
				'updated_by',
				'updated_on'))) {
				$criteria = new Criteria();
				$criteria->addSort($column);
				$table->search($criteria, $table->getOwner());
			} else {
				$criteria = new Criteria();
				try {
					$criteria->addSort($column);
					$table->search($criteria, $table->getOwner());
					$this->fail(sprintf('Should not be successfull %s', $column->getType()));
				} catch (TxCoreException $e) {
					$this->assertIdentical(TxCoreException::INTERNAL, $e->getCode());
				}
			}
		}
	}
	
	public function testSearchTable_非リスト可能フィールドが指定された場合()
	{
		$table = $this->target->getTable(3001);
		$criteria = new Criteria();
		
		foreach ($table->getColumns() as $column) {	/* @var $column \TxCore\Column */
			if (in_array($column->getType(), array('relational', 'label', 'space', 'line'))) {
				try {
					$criteria->setListColumns(array($column));
					$table->search($criteria, $table->getOwner());
					$this->fail(sprintf('Should not be successfull %s', $column->getType()));
				} catch (TxCoreException $e) {
					$this->assertIdentical(TxCoreException::INTERNAL, $e->getCode());
				}
			} else {
				$criteria->setListColumns(array($table->getColumn(0)));
				$table->search($criteria, $table->getOwner());
			}
		}
	}
	
	public function testSearchTable_ビューカラム設定なし()
	{
		$this->db->update('delete from view_column');
		
		// 全カラム（ラベルなどはリストから除外される）
		$table = $this->target->getTable(3001);
		$criteria = new Criteria();
		$list = $table->search($criteria, $table->getOwner());
		$this->assertIdentical(20, count($list->getColumns()));
		$columns = $list->getColumns();
		$this->assertTrue($columns[0]->getType() == Column::TEXT);
		$this->assertTrue($columns[1]->getType() == Column::MULTITEXT);
		$this->assertTrue($columns[2]->getType() == Column::RICHTEXT);
		$this->assertTrue($columns[3]->getType() == Column::NUMBER);
		$this->assertTrue($columns[4]->getType() == Column::CALC);
		$this->assertTrue($columns[5]->getType() == Column::RADIO);
		$this->assertTrue($columns[6]->getType() == Column::CHECKBOX);
		$this->assertTrue($columns[7]->getType() == Column::DROPDOWN);
		$this->assertTrue($columns[8]->getType() == Column::DATE);
		$this->assertTrue($columns[9]->getType() == Column::TIME);
		$this->assertTrue($columns[10]->getType() == Column::DATETIME);
		$this->assertTrue($columns[11]->getType() == Column::FILE);
		$this->assertTrue($columns[12]->getType() == Column::LINK);
// 		$this->assertTrue($columns[13]->getType() == Column::Relational);
		$this->assertTrue($columns[13]->getType() == Column::LOOKUP);
		$this->assertTrue($columns[14]->getType() == Column::ROWNUM);
		$this->assertTrue($columns[15]->getType() == Column::CREATED_BY);
		$this->assertTrue($columns[16]->getType() == Column::CREATED_ON);
		$this->assertTrue($columns[17]->getType() == Column::UPDATED_BY);
		$this->assertTrue($columns[18]->getType() == Column::UPDATED_ON);
		$this->assertTrue($columns[19]->getType() == Column::LOOKUP);
// 		$this->assertTrue($columns[20]->getType() == Column::Label);
// 		$this->assertTrue($columns[21]->getType() == Column::Space);
// 		$this->assertTrue($columns[22]->getType() == Column::Line);
// 		$this->assertTrue($columns[23]->getType() == Column::Lookup);
	}

	public function testSearchTable_カラム指定()
	{
		$table = $this->target->getTable(3001);
		$row = new Row($table);
		$row->setValue($table->getColumn(0), 'a');
		$row->setValue($table->getColumn(1), 'b');
		$row->setValue($table->getColumn(2), 'c');
		$table->insertRow($row, $table->getOwner());
		
		$criteria = new Criteria();
		$criteria->setListColumns(array($table->getColumn(0), $table->getColumn(2)));
		$list = $table->search($criteria, $table->getOwner());
		$this->assertIdentical(2, count($list->getColumns()));
		$this->assertIdentical('text', $list->getColumn(0)->getType());
		$this->assertIdentical('richtext', $list->getColumn(1)->getType());
	}
	
	public function testSearchTable_リストにできないカラムを指定()
	{
		$by = $this->target->getUser(1001);
		$table = $this->target->getTable(3001);
		
		$criteria = new Criteria();
		try {
			$criteria->setListColumns(array($table->getColumn(13)));
			$list = $table->search($criteria, $table->getOwner());
			$this->fail('Should not be successfull');
		} catch (TxCoreException $e) {}
	}

	public function testSearchTable_チェックボックスの未選択はインデックスにヌルを登録しておいてヌルで検索する()
	{
		$by = $this->target->getUser(1001);
		$table = $this->mockUpTable();
		$column_0 = $table->addColumn(Column::CHECKBOX);
		$table->create($by);
		
		$row = $table->addRow();
		$row->setValue($column_0, null);
		$row = $table->addRow();
		$table->insertRows($by);
		
		$this->assertSearchHit($column_0, array(Criteria::CONTAINS, array(null)), 2);
		$this->assertSearchHit($column_0, array(Criteria::CONTAINS, array("")), 2);
	}
	
	public function testSearchTable_文字列フィールドの空文字でヌルのインデックスがヒットすること()
	{
		$by = $this->target->getUser(1001);
		$table = $this->mockUpTable("sample");
		$column_0 = $table->addColumn(Column::TEXT);
		$table->create($by);
		
		$row = $table->addRow();
		$row->setValue($column_0, null);
		$row = $table->addRow();
		$row->setValue($column_0, "");
		$table->insertRows($by);
		
		$this->assertSearchHit($column_0, array(Criteria::EQUALS, null), 2);
		$this->assertSearchHit($column_0, array(Criteria::EQUALS, ""), 2);
	}

	public function testSearchTable_デフォルトのソート順()
	{
		$table = $this->target->getTable(1001);
		
		$criteria = new Criteria();
		$list = $table->search($criteria, $table->getOwner());	// rownum でソートされる
		$this->assertIdentical(3, $list->getRowCount());
		$this->assertIdentical(3, $list->getTotalRowCount());
		$this->assertIdentical('0001', $list[0]->getValue($table->getColumn(0)));
		$this->assertIdentical('0003', $list[1]->getValue($table->getColumn(0)));
		$this->assertIdentical('0002', $list[2]->getValue($table->getColumn(0)));
	}
	
	public function testSearchTable_ソート()
	{
		$table = $this->target->getTable(1001);
		
		$criteria = new Criteria();
		$criteria->addSort($table->getColumn(2), false);
		$criteria->addSort($table->getColumn(0), true);
		$list = $table->search($criteria, $table->getOwner());
		
		$this->assertIdentical(3, count($list));
		$this->assertIdentical('0002', $list[0]->getValue($table->getColumn(0)));
		$this->assertIdentical('Banana', $list[0]->getValue($table->getColumn(1)));
		$this->assertIdentical('120', $list[0]->getValue($table->getColumn(2)));
	}
	
	public function testSearchTable_ＯＲ検索()
	{
		$table = $this->target->getTable(1001);
		
		$criteria = new Criteria();
		$criteria->setBoolOperator(Criteria::OR_);
		$criteria->equals($table->getColumn(0), '0001');
		$criteria->equals($table->getColumn(0), '0002');
		$data = $table->search($criteria, $table->getOwner());
		$this->assertIdentical(2, count($data));
	}

	public function testSearchTable_ページング()
	{
		$by = $this->target->getUser(1001);
		$table = $this->mockUpTable("sample");
		$column = $table->addColumn(Column::TEXT);
		$table->create($by);
		
		array_each(range(1, 20), function($i) use ($table, $by) {
			$row = new Row($table);
			$row->setValue($table->getColumn(0), sprintf("row %02d", $i));
			$table->insertRow($row, $by);
		});
		
		$criteria = new Criteria();
		$criteria->addSort($table->getColumn(0), true);
// 		$criteria->setLimit(3, 1);
		$list = $table->search($criteria, $table->getOwner(), 1, 3);
		$this->assertIdentical(1, $list->getOffset());
		$this->assertIdentical(3, $list->getRowCount());
		$this->assertIdentical(20, $list->getTotalRowCount());
		$this->assertIdentical('row 19', $list[0]->getValue($table->getColumn(0)));
		$this->assertIdentical('row 18', $list[1]->getValue($table->getColumn(0)));
		$this->assertIdentical('row 17', $list[2]->getValue($table->getColumn(0)));
	}
	
	
	public function testSearchTable_ｌｉｋｅ検索時のエスケープ()
	{
		$table = $this->target->getTable(3001);
	
		$row = $table->addRow();
		$row->setValue($table->getColumn(0), 'textbox');
		$row = $table->addRow();
		$row->setValue($table->getColumn(0), 'text%box');
		$table->insertRows($table->getOwner());
	
		// like
		$criteria = new Criteria();
		$criteria->like($table->getColumn(0), 't');
		$list = $table->search($criteria, $table->getOwner());
		$this->assertIdentical(2, $list->getTotalRowCount());
	
		// like (ヒットしない）
		$criteria = new Criteria();
		$criteria->like($table->getColumn(0), '%');
		$list = $table->search($criteria, $table->getOwner());
		$this->assertIdentical(1, $list->getTotalRowCount());
	
		// like (ヒットしない）
		$criteria = new Criteria();
		$criteria->like($table->getColumn(0), '_');
		$list = $table->search($criteria, $table->getOwner());
		$this->assertIdentical(0, $list->getTotalRowCount());
	}
	
	
	public function testSearchTable_文字列()
	{
		$by = $this->target->getUser(1001);
		$table = $this->mockUpTable();
		$column = $table->addColumn(Column::TEXT);
		$table->create($by);
	
		$row = $table->addRow();
		$row->setValue($column, 'abc');
		$row = $table->addRow();
		$row->setValue($column, "");// 空文字
		$row = $table->addRow();
		$row->setValue($column, null);// Null
		$row = $table->addRow();
		$table->insertRows($by);
			
		$this->assertSearchHit($column, array(Criteria::EQUALS, 'abc'), 1);
		$this->assertSearchHit($column, array(Criteria::EQUALS, 'bc'), 0);
		$this->assertSearchHit($column, array(Criteria::EQUALS, null), 3);
		$this->assertSearchHit($column, array(Criteria::EQUALS, ""), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, "abc"), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, ""), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, 'abc'), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, 'ab'), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, 'bc'), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, 'b'), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, 'd'), 0);
		$this->assertSearchHit($column, array(Criteria::LIKE, ''), false);
		$this->assertSearchHit($column, array(Criteria::LIKE, null), false);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'abc'), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'ab'), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'bc'), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'b'), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'd'), 4);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, ''), false);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, null), false);
		$this->assertSearchHit($column, array(Criteria::GREATER, 'abc'), false);
		$this->assertSearchHit($column, array(Criteria::LESS, 'abc'), false);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array('abc')), false);
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array('abc')), false);
	}
	
	
	public function testSearchTable_複数文字列()
	{
		$by = $this->target->getUser(1001);
		$table = $this->target->getTable(3001);
		$column = $table->getColumn(1);
	
		$row = $table->addRow();
		$row->setValue($column, "aB\tｃ\r \nＤｱい　ウ");
		$row = $table->addRow();
		$row->setValue($column, null);// Null
		$row = $table->addRow();
		$row->setValue($column, "");// ""
		$row = $table->addRow();
		$table->insertRows($table->getOwner());
	
		$this->assertSearchHit($column, array(Criteria::EQUALS, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, "abc"), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::LIKE, 'ＡｂCdあｲう'), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, 'ＡｂCdあｲ'), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, 'ｂCdあｲう'), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, 'ｂCdあｲ'), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, 'え'), 0);
		$this->assertSearchHit($column, array(Criteria::LIKE, ''), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::LIKE, null), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'ＡｂCdあｲう'), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'ＡｂCdあｲ'), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'ｂCdあｲう'), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'ｂCdあｲ'), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'え'), 4);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, ''), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, null), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::GREATER, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::LESS, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array('abc')), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array('abc')), false);	// エラー
	}
	
	public function testSearchTable_リッチエディタ()
	{
		$by = $this->target->getUser(1001);
		$table = $this->target->getTable(3001);
		$column = $table->getColumn(2);
	
		$row = new Row($table);
		$row->setValue($column, "<p>aB\tｃ\r <a>\nＤｱい</a><br/>　ウ");
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, null);// Null
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, "");// ""
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);	// 未指定
		$table->insertRow($row, $table->getOwner());
	
		$this->assertSearchHit($column, array(Criteria::EQUALS, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, "abc"), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::LIKE, 'ＡｂCdあｲう'), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, 'ＡｂCdあｲ'), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, 'ｂCdあｲう'), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, 'ｂCdあｲ'), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, 'え'), 0);
		$this->assertSearchHit($column, array(Criteria::LIKE, ''), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::LIKE, null), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'ＡｂCdあｲう'), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'ＡｂCdあｲ'), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'ｂCdあｲう'), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'ｂCdあｲ'), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'え'), 4);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, ''), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, null), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::GREATER, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::LESS, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array('abc')), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array('abc')), false);	// エラー
	}
	
	public function testSearchTable_数値()
	{
		$by = $this->target->getUser(1001);
		$table = $this->target->getTable(3001);
		$column = $table->getColumn(3);
		$this->assertIdentical('number', $column->getType());
	
		$row = new Row($table);
		$row->setValue($column, "120");
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, "120.2");
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, "90.0");
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, "-80.0");
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, null);// Null
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, "");// ""
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);	// 未指定
		$table->insertRow($row, $table->getOwner());
	
		$this->assertSearchHit($column, array(Criteria::EQUALS, '120.0'), 1);
		$this->assertSearchHit($column, array(Criteria::EQUALS, '120.2'), 1);
		$this->assertSearchHit($column, array(Criteria::EQUALS, '90'), 1);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, "90"), 6);
		$this->assertSearchHit($column, array(Criteria::LIKE, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, '90'), 3);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, '120.0'), 2);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, '90'), 2);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, '120.1'), 3);
		$this->assertSearchHit($column, array(Criteria::GREATER, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::LESS, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array('abc')), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array('abc')), false);	// エラー
	}
	
	public function testSearchTable_数値_デシマル対応()
	{
		// Note : MySQLのDECIMAL型に対して、number_value = '253560' とすると検索がヒットしない問題への対応
		//（特定の数値でSQLのキャストが正しく動いてくれないみたい）
	
		$by = $this->target->getUser(1001);
		$table = $this->target->getTable(3001);
		$column = $table->getColumn(3);
		$this->assertIdentical('number', $column->getType());
	
		$row = new Row($table);
		$row->setValue($column, "253560");
		$table->insertRow($row, $table->getOwner());
	
		$this->assertSearchHit($column, array(Criteria::EQUALS, '253560'), 1);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, "253560"), 0);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, '253560'), 1);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, '253560'), 1);
	}
	
	public function testSearchTable_ラジオボタン()
	{
		$by = $this->target->getUser(1001);
		$table = $this->mockUpTable();
		$column = $table->addColumn(Column::RADIO);
		$column->setRequired(false);
		$option0 = $column->addOption();
		$option1 = $column->addOption();
		$option2 = $column->addOption();
		$table->create($by);
	
		$row = $table->addRow();
		$row->setValue($column, $option0);
		$row = $table->addRow();
		$row->setValue($column, $option1);
		$row = $table->addRow();
		$row->setValue($column, null);	// Null
		$row = $table->addRow();
		$row->setValue($column, "");	// ""
		$row = $table->addRow();	// 未指定
		$table->insertRows($table->getOwner());
	
		$this->assertSearchHit($column, array(Criteria::EQUALS, $option0), false);// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, $option0), false);// エラー
		$this->assertSearchHit($column, array(Criteria::LIKE, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::GREATER, '90'), false);
		$this->assertSearchHit($column, array(Criteria::LESS, '90'), false);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array($option0, $option1)), 2);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array($option0, $option2)), 1);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array($option0)), 1);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array(null, $option0)), 4);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array("")), 3);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array(null)), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array($option0)), 4);
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array($option0, $option1, $option2)), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array(null, $option0)), 1);
	}
	
	public function testSearchTable_チェックボックス()
	{
		$by = $this->target->getUser(1001);
		$table = $this->mockUpTable();
		$column = $table->addColumn(Column::CHECKBOX);
		$option_0 = $column->addOption();
		$option_1 = $column->addOption();
		$option_2 = $column->addOption();
		$table->create($by);
	
		$row = new Row($table);
		$row->setValue($column, array($option_0));
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, array($option_0, $option_1));
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, array());	// Null
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, null);	// ""
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);	// 未指定
		$table->insertRow($row, $table->getOwner());
	
		$this->assertSearchHit($column, array(Criteria::EQUALS, $option_0), false);// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, $option_0), false);// エラー
		$this->assertSearchHit($column, array(Criteria::LIKE, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::GREATER, '90'), false);
		$this->assertSearchHit($column, array(Criteria::LESS, '90'), false);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array($option_0, $option_1)), 2);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array($option_0, $option_2)), 2);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array($option_0)), 2);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array($option_2)), 0);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array(null, $option_0)), 5);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array(null, $option_2)), 3);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array("")), 3);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array(null)), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array($option_0)), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array($option_0, $option_1, $option_2)), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array(null, $option_1)), 1);
	}
	
	
	public function testSearchTable_ドロップダウン()
	{
		$by = $this->target->getUser(1001);
		$table = $this->target->getTable(3001);
		$column = $table->getColumn(7);
		$this->assertIdentical('dropdown', $column->getType());
	
		$row = new Row($table);
		$row->setValue($column, $column->getOptionFor(30010801));
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, $column->getOptionFor(30010802));
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, null);	// Null
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, "");	// ""
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);	// 未指定
		$table->insertRow($row, $table->getOwner());
	
		$this->assertSearchHit($column, array(Criteria::EQUALS, $column->getOptionFor(30010801)), false);// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, $column->getOptionFor(30010801)), false);// エラー
		$this->assertSearchHit($column, array(Criteria::LIKE, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::GREATER, '90'), false);
		$this->assertSearchHit($column, array(Criteria::LESS, '90'), false);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array($column->getOptionFor(30010801), $column->getOptionFor(30010802))), 2);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array($column->getOptionFor(30010801), $column->getOptionFor(30010803))), 1);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array($column->getOptionFor(30010801))), 1);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array(null, $column->getOptionFor(30010801))), 4);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array("")), 3);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array(null)), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array($column->getOptionFor(30010801))), 4);
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array($column->getOptionFor(30010801), $column->getOptionFor(30010802), $column->getOptionFor(30010803))), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array(null, $column->getOptionFor(30010801))), 1);
	}
	
	public function testSearchTable_日付()
	{
		$by = $this->target->getUser(1001);
		$table = $this->target->getTable(3001);
		$column = $table->getColumn(8);
		$this->assertIdentical('date', $column->getType());
	
		$row = new Row($table);
		$row->setValue($column, "2014-01-01");
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, "2014-01-02");
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, null);// Null
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, "");// ""
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);	// 未指定
		$table->insertRow($row, $table->getOwner());
	
		$this->assertSearchHit($column, array(Criteria::EQUALS, '2014-01-01'), 1);
		$this->assertSearchHit($column, array(Criteria::EQUALS, ''), 3);
		$this->assertSearchHit($column, array(Criteria::EQUALS, null), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, '2014-01-01'), 4);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, null), 2);
		$this->assertSearchHit($column, array(Criteria::LIKE, '2014-01-01'), false);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, '2014-01-01'), false);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, '2014-01-01'), 2);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, '2014-01-01'), 1);
		$this->assertSearchHit($column, array(Criteria::GREATER, '2014-01-01'), 1);
		$this->assertSearchHit($column, array(Criteria::LESS, '2014-01-02'), 1);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array('abc')), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array('abc')), false);	// エラー
	}
	

	
	public function testSearchTable_時刻()
	{
		$by = $this->target->getUser(1001);
		$table = $this->target->getTable(3001);
		$column = $table->getColumn(9);
		$this->assertIdentical('time', $column->getType());
	
		$row = new Row($table);
		$row->setValue($column, "12:10");
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, "16:1");
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, null);// Null
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, "");// ""
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);	// 未指定
		$table->insertRow($row, $table->getOwner());
	
		$this->assertSearchHit($column, array(Criteria::EQUALS, '12:10'), 1);
		$this->assertSearchHit($column, array(Criteria::EQUALS, ''), 3);
		$this->assertSearchHit($column, array(Criteria::EQUALS, null), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, '12:10'), 4);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, null), 2);
		$this->assertSearchHit($column, array(Criteria::LIKE, '00:00'), false);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, '00:00'), false);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, '12:10'), 2);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, '16:00'), 1);
		$this->assertSearchHit($column, array(Criteria::GREATER, '12:10'), 1);
		$this->assertSearchHit($column, array(Criteria::LESS, '16:01'), 1);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array('00:00')), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array('00:00')), false);	// エラー
	}
	
	
	
	public function testSearchTable_添付ファイル()
	{
		$by = $this->target->getUser(1001);
		$table = $this->target->getTable(3001);
		$column = $table->getColumn(11);
		$this->assertIdentical('file', $column->getType());
	
		$file1 = new File();
		$file1->_setFileName('aあいう.jpg');
		$file1->_setFilekey('00001.jpg');
		$file2 = new File();
		$file2->_setFileName('ａｂｃ.jpg');
		$file2->_setFilekey('00002.jpg');
	
		$row = new Row($table);
		$row->setValue($column, array($file1, $file2));
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, array());// Null
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, null);
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);	// 未指定
		$table->insertRow($row, $table->getOwner());
	
		$this->assertSearchHit($column, array(Criteria::EQUALS, 'abc'), false);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, 'abc'), false);
		$this->assertSearchHit($column, array(Criteria::LIKE, 'abc'), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, 'ｊｐｇ'), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, null), false);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'a'), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'あ'), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'd'), 4);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, 'abc'), false);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, 'abc'), false);
		$this->assertSearchHit($column, array(Criteria::GREATER, 'abc'), false);
		$this->assertSearchHit($column, array(Criteria::LESS, 'abc'), false);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array('abc')), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array('abc')), false);	// エラー
	}
	
	public function testSearchTable_リンク()
	{
		$by = $this->target->getUser(1001);
		$table = $this->target->getTable(3001);
		$column = $table->getColumn(12);
		$this->assertIdentical('link', $column->getType());
	
		$row = new Row($table);
		$row->setValue($column, 'abc');
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, "");// 空文字
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);
		$row->setValue($column, null);// Null
		$table->insertRow($row, $table->getOwner());
		$row = new Row($table);	// 未指定
		$table->insertRow($row, $table->getOwner());
			
		$this->assertSearchHit($column, array(Criteria::EQUALS, 'abc'), 1);
		$this->assertSearchHit($column, array(Criteria::EQUALS, 'bc'), 0);
		$this->assertSearchHit($column, array(Criteria::EQUALS, null), 3);
		$this->assertSearchHit($column, array(Criteria::EQUALS, ""), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, "abc"), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, ""), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, 'abc'), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, 'ab'), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, 'bc'), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, 'b'), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, 'd'), 0);
		$this->assertSearchHit($column, array(Criteria::LIKE, ''), false);
		$this->assertSearchHit($column, array(Criteria::LIKE, null), false);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'abc'), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'ab'), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'bc'), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'b'), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'd'), 4);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, ''), false);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, null), false);
		$this->assertSearchHit($column, array(Criteria::GREATER, 'abc'), false);
		$this->assertSearchHit($column, array(Criteria::LESS, 'abc'), false);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array('abc')), false);
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array('abc')), false);
	}
	
	public function testSearchTable_ルックアップ()
	{
		$this->markTestIncomplete('Do implement');//TODO:
	}
	
	public function testSearchTable_レコード番号()
	{
		// 数値と同じ
		$by = $this->target->getUser(1001);
		$table = $this->target->getTable(3001);
		$column = $table->getColumn(15);
		$this->assertIdentical('rownum', $column->getType());
	
		$rownum = $this->getNextRowno($table);
		$row_0 = new Row($table);
		$table->insertRow($row_0, $table->getOwner());	// rownum =1
		$row_1 = new Row($table);
		$table->insertRow($row_1, $table->getOwner());	// rownum =2

		$this->dumpTable('row');
		$this->dumpTable('data');
		$this->assertSearchHit($column, array(Criteria::EQUALS, $rownum), 1);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, $rownum), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, $rownum), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, $rownum), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, $rownum), 2);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, $rownum + 1), 1);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, $rownum + 1), 2);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, $rownum), 1);
		$this->assertSearchHit($column, array(Criteria::GREATER, $rownum), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::LESS, $rownum + 1), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array($rownum)), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array($rownum)), false);	// エラー
	}
	
	public function testSearchTable_作成者()
	{
		$user1 = $this->target->getUser(1001);
		$user2 = $this->target->getUser(1002);
		$user3 = $this->target->getUser(1003);
		$table = $this->target->getTable(3001);
		$table->setPublic(true);
		$column = $table->getColumn(16);
		$this->assertIdentical('created_by', $column->getType());
	
		$row0 = new Row($table);
		$table->insertRow($row0, $user1);
		$row1 = new Row($table);
		$table->insertRow($row1, $user2);
	
		$this->assertSearchHit($column, array(Criteria::EQUALS, '120.0'), false);// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, "90"), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::LIKE, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, '90'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, '120.0'), false);// エラー
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, '120.1'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::GREATER, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::LESS, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array($user1)), 1);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array($user1, $user2)), 2);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array($user3)), 0);
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array($user1)), 1);
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array($user1, $user2)), 0);
	}
	
	public function testSearchTable_作成日時()
	{
		$by = $this->target->getUser(1001);
		$table = $this->mockUpTable();
		$table->create($by);
		$column = $table->getColumn(2);
		$this->assertIdentical('created_on', $column->getType());
	
		$row = $table->addRow();
		$table->insertRows($table->getOwner());
	
		$this->assertSearchHit($column, array(Criteria::EQUALS, '2014-01-02 23:59'), 1);
		$this->assertSearchHit($column, array(Criteria::EQUALS, '2014-01-02 23:58'), 0);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, '2014-01-02 23:59'), 0);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, '2014-01-02 23:58'), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, '2014-01-01 00:00'), false);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, '2014-01-01 00:00'), false);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, '2014-01-02 23:58'), 1);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, '2014-01-02 23:59'), 1);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, '2014-01-03 00:00'), 0);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, '2014-01-02 23:58'), 0);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, '2014-01-02 23:59'), 1);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, '2014-01-03 00:00'), 1);
		$this->assertSearchHit($column, array(Criteria::GREATER, '2014-01-02 23:58'), 1);
		$this->assertSearchHit($column, array(Criteria::GREATER, '2014-01-02 23:59'), 0);
		$this->assertSearchHit($column, array(Criteria::GREATER, '2014-01-03 00:00'), 0);
		$this->assertSearchHit($column, array(Criteria::LESS, '2014-01-02 23:58'), 0);
		$this->assertSearchHit($column, array(Criteria::LESS, '2014-01-02 23:59'), 0);
		$this->assertSearchHit($column, array(Criteria::LESS, '2014-01-03 00:00'), 1);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array('2014-01-01 12:10')), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array('2014-01-01 12:10')), false);	// エラー
	}
	
	public function testSearchTable_更新日時()
	{
		$by = $this->target->getUser(1001);
		$table = $this->mockUpTable();
		$table->create($by);
		$column = $table->getColumn(4);
		$this->assertIdentical('updated_on', $column->getType());
	
		$row = $table->addRow();
		$table->insertRows($table->getOwner());
	
		$this->assertSearchHit($column, array(Criteria::EQUALS, '2014-01-02 23:59'), 1);
		$this->assertSearchHit($column, array(Criteria::EQUALS, '2014-01-02 23:58'), 0);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, '2014-01-02 23:59'), 0);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, '2014-01-02 23:58'), 1);
		$this->assertSearchHit($column, array(Criteria::LIKE, '2014-01-01 00:00'), false);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, '2014-01-01 00:00'), false);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, '2014-01-02 23:58'), 1);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, '2014-01-02 23:59'), 1);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, '2014-01-03 00:00'), 0);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, '2014-01-02 23:58'), 0);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, '2014-01-02 23:59'), 1);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, '2014-01-03 00:00'), 1);
		$this->assertSearchHit($column, array(Criteria::GREATER, '2014-01-02 23:58'), 1);
		$this->assertSearchHit($column, array(Criteria::GREATER, '2014-01-02 23:59'), 0);
		$this->assertSearchHit($column, array(Criteria::GREATER, '2014-01-03 00:00'), 0);
		$this->assertSearchHit($column, array(Criteria::LESS, '2014-01-02 23:58'), 0);
		$this->assertSearchHit($column, array(Criteria::LESS, '2014-01-02 23:59'), 0);
		$this->assertSearchHit($column, array(Criteria::LESS, '2014-01-03 00:00'), 1);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array('2014-01-01 12:10')), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array('2014-01-01 12:10')), false);	// エラー
	}
	
	public function testSearchTable_更新者()
	{
		$user1 = $this->target->getUser(1001);
		$user2 = $this->target->getUser(1002);
		$user3 = $this->target->getUser(1003);
		$table = $this->target->getTable(3001);
		$table->setPublic(true);
		$column = $table->getColumn(18);
		$this->assertIdentical('updated_by', $column->getType());
	
		$row0 = new Row($table);
		$table->insertRow($row0, $user1);
		$row1 = new Row($table);
		$table->insertRow($row1, $user2);
	
		$this->assertSearchHit($column, array(Criteria::EQUALS, '120.0'), false);// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, "90"), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::LIKE, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, '90'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, '120.0'), false);// エラー
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, '120.1'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::GREATER, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::LESS, 'abc'), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array($user1)), 1);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array($user1, $user2)), 2);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array($user3)), 0);
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array($user1)), 1);
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array($user1, $user2)), 0);
	}

	
	public function testSearchTable_日付_式()
	{
		$by = $this->target->getUser(1001);
		$table = $this->mockUpTable();
		$column = $table->addColumn(Column::DATE);
		$table->create($by);
	
		$row = $table->addRow();
		$row->setValue($column, '2013-12-01');
		$row = $table->addRow();
		$row->setValue($column, '2013-12-31');
		$row = $table->addRow();
		$row->setValue($column, '2014-01-01');
		$row = $table->addRow();
		$row->setValue($column, '2014-01-02');	// now
		$row = $table->addRow();
		$row->setValue($column, '2014-01-31');
		$row = $table->addRow();
		$row->setValue($column, '2014-12-31');
		$table->insertRows($table->getOwner());
	
		// var_dump(date('Y-m-d', strtotime('last day of this month')));
		$this->assertSearchHit($column, array(Criteria::EQUALS, 'now'), 1);
// 		$this->assertSearchHit($column, array(Criteria::EQUALS, '/now'), 1);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, 'NOW'), 5);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, 'NOW'), 3);
		$this->assertSearchHit($column, array(Criteria::GREATER, 'NOW'), 2);
		$this->assertSearchHit($column, array(Criteria::LESS, 'NOW'), 3);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, 'NOW'), 4);
		$this->assertSearchHit($column, array(Criteria::EQUALS, 'LAST_MONTH'), 2);
// 		$this->assertSearchHit($column, array(Criteria::EQUALS, '/LAST_MONTH'), 2);
		$this->assertSearchHit($column, array(Criteria::EQUALS, '1/LAST_MONTH'), 1);
		$this->assertSearchHit($column, array(Criteria::EQUALS, 'LAST_DAY/LAST_MONTH'), 1);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, 'LAST_MONTH'), 4);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, 'LAST_MONTH'), 6);
		$this->assertSearchHit($column, array(Criteria::GREATER, 'LAST_MONTH'), 4);
		$this->assertSearchHit($column, array(Criteria::LESS, 'LAST_MONTH'), 0);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, 'LAST_MONTH'), 2);
		$this->assertSearchHit($column, array(Criteria::EQUALS, 'current_month'), 3);
// 		$this->assertSearchHit($column, array(Criteria::EQUALS, '/current_month'), 3);
		$this->assertSearchHit($column, array(Criteria::EQUALS, '2/current_month'), 1);
		$this->assertSearchHit($column, array(Criteria::EQUALS, 'LAST_DAY/current_month'), 1);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, 'current_month'), 3);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, 'current_month'), 4);
		$this->assertSearchHit($column, array(Criteria::GREATER, 'current_month'), 1);
		$this->assertSearchHit($column, array(Criteria::LESS, 'current_month'), 2);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, 'current_month'), 5);
		$this->assertSearchHit($column, array(Criteria::EQUALS, 'Current_year'), 4);
// 		$this->assertSearchHit($column, array(Criteria::EQUALS, '/Current_year'), 4);
		$this->assertSearchHit($column, array(Criteria::EQUALS, '2014-01-02'), 1);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, 'Current_year'), 2);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, 'Current_year'), 4);
		$this->assertSearchHit($column, array(Criteria::GREATER, 'Current_year'), 0);
		$this->assertSearchHit($column, array(Criteria::LESS, 'Current_year'), 2);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, 'Current_year'), 6);
	}
	
	public function testSearchTable_日時_式()
	{
		$by = $this->target->getUser(1001);
		$table = $this->mockUpTable();
		$column = $table->addColumn(Column::DATETIME);
		$table->create($by);
	
		$row = $table->addRow();
		$row->setValue($column, '2013-12-01 00:00');	// last month >>
		$row = $table->addRow();
		$row->setValue($column, '2013-12-31 00:00');	//
		$row = $table->addRow();
		$row->setValue($column, '2013-12-31 23:59');	// << Last month
		$row = $table->addRow();
		$row->setValue($column, '2014-01-01 00:00');	// current year >>  current month >>
		$row = $table->addRow();
		$row->setValue($column, '2014-01-02 00:00');	// today >>
		$row = $table->addRow();
		$row->setValue($column, '2014-01-02 12:12');	//
		$row = $table->addRow();
		$row->setValue($column, '2014-01-02 23:58');	//
		$row = $table->addRow();
		$row->setValue($column, '2014-01-02 23:59');	// Now << today
		$row = $table->addRow();
		$row->setValue($column, '2014-01-03 00:00');	//
		$row = $table->addRow();
		$row->setValue($column, '2014-01-31 23:59');	// << Current month
		$row = $table->addRow();
		$row->setValue($column, '2014-02-01 01:01');	//
		$row = $table->addRow();
		$row->setValue($column, '2014-12-31 23:59');	// << current year.
		$row = $table->addRow();
		$row->setValue($column, '2015-01-01 00:00');	// Next year >>
		$table->insertRows($table->getOwner());
	
		// var_dump(date('Y-m-d', strtotime('last day of this month')));
		$this->assertSearchHit($column, array(Criteria::EQUALS, 'now'), 1);
// 		$this->assertSearchHit($column, array(Criteria::EQUALS, '/now'), 1);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, 'now'), 12);
		$this->assertSearchHit($column, array(Criteria::GREATER, 'now'), 5);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, 'now'), 6);
		$this->assertSearchHit($column, array(Criteria::LESS, 'now'), 7);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, 'now'), 8);
		$this->assertSearchHit($column, array(Criteria::EQUALS, 'today'), 4);
// 		$this->assertSearchHit($column, array(Criteria::EQUALS, '/today'), 4);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, 'today'), 9);
		$this->assertSearchHit($column, array(Criteria::GREATER, 'today'), 5);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, 'today'), 9);
		$this->assertSearchHit($column, array(Criteria::LESS, 'today'), 4);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, 'today'), 8);
		$this->assertSearchHit($column, array(Criteria::EQUALS, 'last_month'), 3);
		$this->assertSearchHit($column, array(Criteria::EQUALS, '31/last_month'), 2);
		$this->assertSearchHit($column, array(Criteria::EQUALS, '2/last_month'), 0);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, 'last_month'), 10);
		$this->assertSearchHit($column, array(Criteria::GREATER, 'last_month'), 10);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, 'last_month'), 13);
		$this->assertSearchHit($column, array(Criteria::LESS, 'last_month'), 0);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, 'last_month'), 3);
		$this->assertSearchHit($column, array(Criteria::EQUALS, 'current_month'), 7);
		$this->assertSearchHit($column, array(Criteria::EQUALS, '2/current_month'), 4);
		$this->assertSearchHit($column, array(Criteria::EQUALS, '10/current_month'), 0);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, 'current_month'), 6);
		$this->assertSearchHit($column, array(Criteria::GREATER, 'current_month'), 3);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, 'current_month'), 10);
		$this->assertSearchHit($column, array(Criteria::LESS, 'current_month'), 3);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, 'current_month'), 10);
		$this->assertSearchHit($column, array(Criteria::EQUALS, 'current_year'), 9);
// 		$this->assertSearchHit($column, array(Criteria::EQUALS, '/current_year'), 9);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, 'current_year'), 4);
		$this->assertSearchHit($column, array(Criteria::GREATER, 'current_year'), 1);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, 'current_year'), 10);
		$this->assertSearchHit($column, array(Criteria::LESS, 'current_year'), 3);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, 'current_year'), 12);
	}
	
	public function testSearchTable_日時()
	{
		$by = $this->target->getUser(1001);
		$table = $this->target->getTable(3001);
		$column = $table->getColumn(10);
		$this->assertIdentical('datetime', $column->getType());
	
		$row = $table->addRow();
		$row->setValue($column, "2014-01-01 12:10");
		$row = $table->addRow();
		$row->setValue($column, "2014-01-01 12:11");
		$row = $table->addRow();
		$row->setValue($column, "2014-01-01 00:00");
		$row = $table->addRow();
		$row->setValue($column, null);// Null
		$row = $table->addRow();
		$row->setValue($column, "");// ""
		$row = $table->addRow();	// 未指定
		$table->insertRows($table->getOwner());
	
		$this->assertSearchHit($column, array(Criteria::EQUALS, '2014-01-01 12:10'), 1);
		$this->assertSearchHit($column, array(Criteria::EQUALS, '2014-01-01 00:00'), 1);
		$this->assertSearchHit($column, array(Criteria::EQUALS, ''), 3);
		$this->assertSearchHit($column, array(Criteria::EQUALS, null), 3);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, '2014-01-01 12:10'), 5);
		$this->assertSearchHit($column, array(Criteria::NOT_EQUALS, null), 3);
		$this->assertSearchHit($column, array(Criteria::LIKE, '2014-01-01 00:00'), false);
		$this->assertSearchHit($column, array(Criteria::NOT_LIKE, '2014-01-01 00:00'), false);
		$this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, '2014-01-01 12:10'), 2);
		$this->assertSearchHit($column, array(Criteria::LESS_EQUALS, '2014-01-01 12:10'), 2);
		$this->assertSearchHit($column, array(Criteria::GREATER, '2014-01-01 12:10'), 1);
		$this->assertSearchHit($column, array(Criteria::GREATER, ''), false);
		$this->assertSearchHit($column, array(Criteria::GREATER, null), false);
		$this->assertSearchHit($column, array(Criteria::LESS, '2014-01-01 12:10'), 1);
		$this->assertSearchHit($column, array(Criteria::LESS, ''), false);
		$this->assertSearchHit($column, array(Criteria::LESS, null), false);
		$this->assertSearchHit($column, array(Criteria::CONTAINS, array('2014-01-01 12:10')), false);	// エラー
		$this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array('2014-01-01 12:10')), false);	// エラー
	}

    public function testSearchTable_ユーザー選択()
    {
        $by = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $column = $table->addColumn(Column::USER_SELECTOR);
        $this->target->createTable($table, $by);

        $row = $table->addRow();
        $row->setValue($column, array($by));
        $row = $table->addRow();
        $row->setValue($column, array());
        $table->insertRows($table->getOwner());

        $this->assertSearchHit($column, array(Criteria::EQUALS, []), false);// エラー
        $this->assertSearchHit($column, array(Criteria::NOT_EQUALS, []), false);// エラー
        $this->assertSearchHit($column, array(Criteria::LIKE, 'abc'), false);	// エラー
        $this->assertSearchHit($column, array(Criteria::NOT_LIKE, 'abc'), false);	// エラー
        $this->assertSearchHit($column, array(Criteria::GREATER, '90'), false);
        $this->assertSearchHit($column, array(Criteria::LESS, '90'), false);
        $this->assertSearchHit($column, array(Criteria::GREATER_EQUALS, 'abc'), false);	// エラー
        $this->assertSearchHit($column, array(Criteria::LESS_EQUALS, 'abc'), false);	// エラー
        $this->assertSearchHit($column, array(Criteria::CONTAINS, array($by)), 1);
        $this->assertSearchHit($column, array(Criteria::CONTAINS, array(new User())), 1);  // 未選択
        $this->assertSearchHit($column, array(Criteria::CONTAINS, array(new User(), $by)), 2);
        $this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array($by)), 1);
        $this->assertSearchHit($column, array(Criteria::NOT_CONTAINS, array(new User())), 1);  // 未選択
    }




}
