<?php
namespace TxCore\Test;

use TxCore\Column;
use TxCore\Criteria;
use TxCore\Engine;
use TxCore\File;
use TxCore\Option;
use TxCore\Table;
use TxCore\TxCoreException;

class TableTest extends BaseTestCase
{
	public function setUp()
	{
		parent::setUpDB(__DIR__.'/EngineTest.sql');
	}

	public function testAddRow_初期値が設定される()
	{
		$table  = $this->mockUpTableFully();
		$table->getColumn(0)->setDefaultValue('value 1');
		$table->getColumn(1)->setDefaultValue('value 2');
		$table->getColumn(2)->setDefaultValue('value 3');
		$table->getColumn(3)->setDefaultValue('99999999999999999999.9999999999');
		$table->getColumn(5)->setDefaultValue($table->getColumn(5)->getOption(0));
		$table->getColumn(6)->setDefaultValue(array($table->getColumn(6)->getOption(0), $table->getColumn(6)->getOption(1)));
		$table->getColumn(7)->setDefaultValue($table->getColumn(7)->getOption(0));
		$table->getColumn(8)->setDefaultValue('2014-01-01');
		$table->getColumn(9)->setDefaultValue('10:10');
		$table->getColumn(10)->setDefaultValue('2014-01-01 10:10');
		$table->getColumn(12)->setDefaultValue('sample@sample');
		$table->create($this->target->getUser(1001));
	
		$row = $table->addRow(); // 初期値が設定され返る
		
		$this->assertIdentical('value 1', $row->getValue($table->getColumn(0)));
		$this->assertIdentical('value 2', $row->getValue($table->getColumn(1)));
		$this->assertIdentical('value 3', $row->getValue($table->getColumn(2)));
		$this->assertIdentical('99999999999999999999.9999999999', $row->getValue( $table->getColumn(3)));
		$this->assertNotNull($table->getColumn(5));
		$this->assertIdentical($table->getColumn(5)->getOption(0)->getId(), $row->getValue($table->getColumn(5))->getId());
		$this->assertIdentical(2, count($row->getValue($table->getColumn(6))));
		$this->assertNotNull($table->getColumn(7));
		$this->assertIdentical($table->getColumn(7)->getOption(0)->getId(), $row->getValue( $table->getColumn(7))->getId());
		$this->assertIdentical('2014-01-01', $row->getValue( $table->getColumn(8)));
		$this->assertIdentical('10:10', $row->getValue($table->getColumn(9)));
		$this->assertIdentical('2014-01-01 10:10', $row->getValue($table->getColumn(10)));
		$this->assertIdentical('sample@sample', $row->getValue($table->getColumn(12)));
	}
	
	public function testAddRow_初期値登録()
	{
		$table  = $this->mockUpTableFully();
		$table->create($this->target->getUser(1001));
	
		$row = $table->addRow(); // 初期化される

		$this->assertIdentical(null, $row->getValue($table->getColumn(0)));
		$this->assertIdentical(null, $row->getValue($table->getColumn(1)));
		$this->assertIdentical(null, $row->getValue($table->getColumn(2)));
		$this->assertIdentical(null, $row->getValue( $table->getColumn(3)));
		$this->assertIdentical(null, $row->getValue($table->getColumn(5)));
		$this->assertIdentical(array(), $row->getValue($table->getColumn(6)));
		$this->assertIdentical(null, $row->getValue($table->getColumn(7)));
//		$this->assertIdentical(null, $row->getValue($table->getColumn(8))); // 初期値が変わった？
		$this->assertIdentical('', $row->getValue($table->getColumn(8)));
		$this->assertIdentical(null, $row->getValue($table->getColumn(9)));
//		$this->assertIdentical(null, $row->getValue($table->getColumn(10)));
		$this->assertIdentical('', $row->getValue($table->getColumn(10))); // 初期値が変わった？
		$this->assertIdentical(array(), $row->getValue($table->getColumn(11))); //ファイル
		$this->assertIdentical(null, $row->getValue($table->getColumn(12)));
	}
	
	public function testAddRow_初期式が評価されること()
	{
		$table  = $this->mockUpTableFully();
		$table->getColumn(8)->setDefaultValue('now');	// 日付
		$table->getColumn(9)->setDefaultValue('now');	// 日時
		$table->getColumn(10)->setDefaultValue('now');	// 時間
		$table->create($this->target->getUser(1001));
	
		$row = $table->addRow(); // 初期値が設定され返る
	
		$this->assertIdentical(date('2014-01-02'), $row->getValue($table->getColumn(8)));
		$this->assertIdentical(date('23:59'), $row->getValue($table->getColumn(9)));
		$this->assertIdentical(date('2014-01-02 23:59'), $row->getValue($table->getColumn(10)));
	}
	
	public function testDrop_データの削除()
	{
		$by = $this->target->getUser(1002);
		$table = $this->target->getTable(1001);
		$id = $table->getId();
		$table->drop($by);
	
		$this->assertIdentical(null, $table->getId());
		$this->assertNotRegistered('table_def', array(@id => $id));
		$this->assertNotRegistered('column_def', array(@table_id => $id));
		$this->assertNotRegistered('indexes', array(@table_id => $id));
		$this->assertNotRegistered('data', array(@table_id => $id));
		$this->assertNotRegistered('filter', array(@table_id => $id));
		$this->assertNotRegistered('view_column', array(@table_id => $id));
		$this->assertNotRegistered('option', array(@table_id => $id));
		$this->assertNotRegistered('row', array(@table_id => $id));
	}

	public function testDrop_ファイルの削除()
	{
		$byAdmin = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$table->create($byAdmin);
		
		$baseDir = dirname(__FILE__).'/asset/files';
		mkdir($baseDir.'/'.$table->getId(), 0777);
		mkdir($baseDir.'/'.$table->getId().'/'.$table->getColumn(0)->getId(), 0777);
		file_put_contents($baseDir.'/'.$table->getId().'/'.$table->getColumn(0)->getId().'/xxx.text', 'abc');
		$table->drop($byAdmin);
		
		$this->assertIdentical(true, file_exists($baseDir.'/'.$table->getId()));
		$this->assertIdentical(false, file_exists($baseDir.'/'.$table->getId().'/'.$table->getColumn(0)->getId()));
		$this->assertIdentical(false, file_exists($baseDir.'/'.$table->getId().'/'.$table->getColumn(0)->getId().'/xxx.text'));
	}
	
	public function testDrop_権限チェック()
	{
		$by = $this->target->getUser(1001);	
		$table = $this->mockUpTable();
		$table->create($by);
		try {
			$table->drop($this->target->getUser(1003));	// 例外が発生する
			$this->fail('Should not be successfull');
		} catch (TxCoreException $e) {
			$this->assertIdentical(TxCoreException::PERMISSION_DENIED, $e->getCode());
		}
		
		$byAdmin = $this->target->getUser(1002);	// 管理者
		$table->drop($byAdmin);	// 例外は発生しない
		$this->pass();
	}

	
	public function testFetch_データの取得()
	{
		$this_ = $this;
		$by = $this->target->getUser(1003);	// 他の一般ユーザー
		$table = $this->mockUpTable();
		$table->create($by);
		
		$row = $table->addRow();
		$row = $table->addRow();
		$table->insertRows($by);
		
		$rows = null;
		$table->fetch(new Criteria(), function($row) use (&$rows) {
			$rows[] = $row;
		});
		$this->assertIdentical(2, count($rows));
	}

	public function testResetApiKey()
	{
		$by = $this->target->getUser(1003);	// 他の一般ユーザー
		$table = $this->mockUpTable();
		$table->create($by);
		$apiKey = $table->getApiKey();
		$this->assertNotNull($apiKey);
		
		$table->resetApiKey();
		$this->assertNotIdentical($apiKey, $table->getApiKey());	// APIキーがリセットされていること
	}
	
	public function testSaveChanges_カラムの追加()
	{
		$by = $this->target->getUser(1003);
		$table = $this->mockUpTable();
		$table->create($by);
			
		$trunkTable = $this->target->getTable($table->getId(), Table::TRUNK);
		$column_0 = $trunkTable->addColumn(Column::TEXT);
		$column_0->setLabel('Label 1');
		$trunkTable->saveChanges($by);
	
		$this->assertNotRegistered('column_def', array(@table_id=>$table->getId(), @revision=> $table->getRevision(), @label => 'Label 1', @column_type => 'text'));
		$this->assertRegistered('column_def', array(@table_id=>$table->getId(), @revision=> $trunkTable->getRevision(), @label => 'Label 1', @column_type => 'text'));
		$this->assertRegistered('release_control', array(@table_id=>$table->getId(), @has_pending_release=> 1));
	}

	
	public function testSaveChanges_データ更新()
	{
		$icons = $this->target->getIcons();
		$by = $this->target->getUser(1003);	// 他の一般ユーザー
		$table = $this->mockUpTable();
		$table->setName('Old name');
		$table->setDescription('Old description');
		$table->setPublic(false);
		$table->setRoundScale(3);
		$table->setRoundType('default');
		$table->setApiEnabled(false);
		$table->create($by);
	
		$trunkTable = $this->target->getTable($table->getId(), Table::TRUNK);
		$trunkTable->setName('New name');
		$trunkTable->setDescription('New description');
		$trunkTable->setPublic(true);
		$trunkTable->setRoundScale(2);
		$trunkTable->setRoundType('ceil');
		$trunkTable->setIcon($icons[1]);
		$trunkTable->setApiEnabled(true);
		$trunkTable->saveChanges($by);
	
	
		$this->assertRegistered('table_def', array(@id=>$table->getId(), @name=>'Old name', @description => 'Old description', @is_public => 0, @is_api_enabled => 0, @Round_Scale => 3, @Round_type => 'default'));
		$this->assertRegistered('table_def', array(@id=>$table->getId(), @name=>'New name', @description => 'New description', @is_public => 1, @is_api_enabled => 1, @Round_Scale => 2, @Round_type => 'ceil'));
		$this->assertRegistered('release_control', array(@table_id=>$table->getId(), @has_pending_release=> 1));
	
		$this->assertNotIdentical($trunkTable->getIcon()->getFilekey(), $icons[0]->getFilekey());	// アイコンがアップロードされていること
		$this->assertIdentical(true, file_exists($trunkTable->getIcon()->_getLocalPath()));	// ファイルが存在すること
	}
	
	public function testSaveChanges_カラムの削除()
	{
		$by = $this->target->getUser(1003);
		$table = $this->mockUpTable();
		$column_0 = $table->addColumn(Column::TEXT);
		$column_0->setLabel('Label 1');
		$column_1 = $table->addColumn(Column::NUMBER);
		$column_1->setLabel('Label 2');
		$column_2 = $table->addColumn(Column::RADIO);
		$option_2_1 = $column_2->addOption();
		$option_2_1->setName('Radio 1');
		$column_3 = $table->addColumn(Column::DROPDOWN);
		$option_3_1 = $column_3->addOption();
		$option_3_1->setName('Dropdown 1');
		$table->create($by);
		
		// Preconditonal test
		$this->assertRegistered('option', array(@table_id=>$table->getId(), 'name'=>'Dropdown 1'), 2);
		$this->assertRegistered('option', array(@table_id=>$table->getId(), 'name'=>'Radio 1'), 2);
		$this->assertIdentical(1, $table->getRevision());

		$trunkTable = $this->target->getTable($table->getId(), Table::TRUNK);
		$trunkTable->removeColumn($trunkTable->getColumn(0));
		$trunkTable->removeColumn($trunkTable->getColumn(3));
		$trunkTable->saveChanges($by);

		$this->assertRegistered('column_def', array(@table_id=>$table->getId(), @revision=>$table->getRevision(), @label => 'Label 1', @column_type => 'text'));
		$this->assertNotRegistered('column_def', array(@table_id=>$table->getId(), @revision=>$trunkTable->getRevision(), @label => 'Label 1', @column_type => 'text'));
		$this->assertRegistered('column_def', array(@table_id=>$table->getId(), @revision=>$table->getRevision(), @label => 'Label 2', @column_type => 'number'));
		$this->assertRegistered('column_def', array(@table_id=>$table->getId(), @revision=>$trunkTable->getRevision(), @label => 'Label 2', @column_type => 'number'));
		$this->assertRegistered('release_control', array(@table_id=>$table->getId(), @has_pending_release=> 1));
		$this->assertRegistered('option', array(@id=>$option_2_1->getId(), @table_id=>$table->getId(), @revision=>$table->getRevision(), 'name'=>'Radio 1'));	// 残る
		$this->assertRegistered('option', array(@id=>$option_2_1->getId(), @table_id=>$table->getId(), @revision=>$trunkTable->getRevision(), 'name'=>'Radio 1'));// 残る
		$this->assertRegistered('option', array(@table_id=>$table->getId(), @revision=>$table->getRevision(), 'name'=>'Dropdown 1'));
		$this->assertNotRegistered('option', array(@table_id=>$table->getId(), @revision=>$trunkTable->getRevision(), 'name'=>'Dropdown 1'));
	}
	
	public function testSaveChanges_ビューカラムの変更()
	{
		$by = $this->target->getUser(1003);
		$table = $this->mockUpTable();
		$column_0 = $table->addColumn(Column::TEXT);
		$table->setViewColumns(array($column_0));
		$table->create($by);
		
		// Preconditonal test
		$this->assertIdentical(1, $table->getRevision());
		$this->assertRegistered('view_column', array(@table_id=>$table->getId(), @revision=>1, 'column_id'=>$column_0->getId()), 1);
		$this->assertRegistered('view_column', array(@table_id=>$table->getId(), @revision=>2, 'column_id'=>$column_0->getId()), 1);
		
		$trunkTable = $this->target->getTable($table->getId(), Table::TRUNK);
		$column_1 = $trunkTable->addColumn(Column::NUMBER);
		$trunkTable->setViewColumns(array($column_1));
		$trunkTable->saveChanges($by);
		
		$this->assertRegistered('view_column', array(@table_id=>$table->getId(), @revision=>$table->getRevision()), 1);
		$this->assertRegistered('view_column', array(@table_id=>$table->getId(), @revision=>$table->getRevision(), 'column_id'=>$column_0->getId()), 1);
		$this->assertRegistered('view_column', array(@table_id=>$table->getId(), @revision=>$trunkTable->getRevision()), 1);
		$this->assertRegistered('view_column', array(@table_id=>$table->getId(), @revision=>$trunkTable->getRevision(), 'column_id'=>$column_1->getId()), 1);
	}
	
	public function testSaveChanges_オプションの追加と削除()
	{
		$by = $this->target->getUser(1003);
		$table = $this->mockUpTable();
		$column_2 = $table->addColumn(Column::RADIO);
		$option_2_1 = $column_2->addOption();
		$option_2_1->setName('Radio 1');
		$column_3 = $table->addColumn(Column::DROPDOWN);
		$option_3_1 = $column_3->addOption();
		$option_3_1->setName('Dropdown 1');
		$table->create($by);
		
		// Preconditonal test
		$this->assertRegistered('option', array(@table_id=>$table->getId(), 'name'=>'Dropdown 1'), 2);
		$this->assertRegistered('option', array(@table_id=>$table->getId(), 'name'=>'Radio 1'), 2);
		$this->assertIdentical(1, $table->getRevision());
		
		$trunkTable = $this->target->getTable($table->getId(), Table::TRUNK);
		$trunkTable->getColumn(0)->removeOption($trunkTable->getColumn(0)->getOption(0));
		$option = $trunkTable->getColumn(1)->addOption();
		$option->setName('Dropdown 2');
		$trunkTable->saveChanges($by);
		
		$this->assertRegistered('option', array(@id=>$option_2_1->getId(), @table_id=>$table->getId(), @revision=>$table->getRevision(), 'name'=>'Radio 1'));	// 残る
		$this->assertNotRegistered('option', array(@id=>$option_2_1->getId(), @table_id=>$table->getId(), @revision=>$trunkTable->getRevision(), 'name'=>'Radio 1'));// 消える
		$this->assertRegistered('option', array(@table_id=>$table->getId(), @revision=>$table->getRevision(), 'name'=>'Dropdown 1'));
		$this->assertRegistered('option', array(@table_id=>$table->getId(), @revision=>$trunkTable->getRevision(), 'name'=>'Dropdown 1'));
		$this->assertNotRegistered('option', array(@table_id=>$table->getId(), @revision=>$table->getRevision(), 'name'=>'Dropdown 2'));
		$this->assertRegistered('option', array(@table_id=>$table->getId(), @revision=>$trunkTable->getRevision(), 'name'=>'Dropdown 2'));
	}
	
	public function testSaveChanges_カラム属性の更新()
	{
		$by = $this->target->getUser(1003);
		$table = $this->mockUpTable();
		$column_0 = $table->addColumn(Column::TEXT);
		$column_0->setCode('Code old');
		$column_0->setLabel('Label old');
		$column_0->setPositionX(1);
		$column_0->setPositionY(2);
		$table->create($by);
			
		$trunkTable = $this->target->getTable($table->getId(), Table::TRUNK);
		$column_0 = $trunkTable->getColumn(0);
		$column_0->setCode('Code New');
		$column_0->setLabel('Label New');
		$column_0->setPositionX(2);
		$column_0->setPositionY(1);
		$trunkTable->saveChanges($by);

		$columnDefs = $this->selectTable('column_def', sprintf('table_id =%d and revision=%d', $table->getId(), $table->getRevision()));
		$this->assertSame(6, sizeof($columnDefs));
		$this->assertSame('Label old', $columnDefs[0]->label);
		$this->assertSame('Code old', $columnDefs[0]->code);
		$this->assertSame('text', $columnDefs[0]->column_type);
		$this->assertSame(1, json_decode($columnDefs[0]->properties)->positionX);
		$this->assertSame(2, json_decode($columnDefs[0]->properties)->positionY);

		$columnDefs = $this->selectTable('column_def', sprintf('table_id =%d and revision=%d', $table->getId(), $trunkTable->getRevision()));
		$this->assertSame(6, sizeof($columnDefs));
		$this->assertSame('Label New', $columnDefs[0]->label);
		$this->assertSame('Code New', $columnDefs[0]->code);
		$this->assertSame('text', $columnDefs[0]->column_type);
		$this->assertSame(2, json_decode($columnDefs[0]->properties)->positionX);
		$this->assertSame(1, json_decode($columnDefs[0]->properties)->positionY);

		$columnDefs = $this->selectTable('release_control', sprintf('table_id =%d and has_pending_release=1', $table->getId()));
		$this->assertSame(1, sizeof($columnDefs));
	}
	
	public function testFetch_ハンドラ内で行操作()
	{
		$this_ = $this;
		$by = $this->target->getUser(1003);	// 他の一般ユーザー
		$table = $this->mockUpTable();
		$table->create($by);
	
		$row = $table->addRow();
		$row = $table->addRow();
		$table->insertRows($by);
	
		$this->assertRegistered('row', array(@table_id => $table->getId()), 2);
		$table->fetch(new Criteria(), function($row) use($by) {
			$row->delete($by);
		});
		$this->assertRegistered('row', array(@table_id => $table->getId()), 0);
// 		$this->assertIdentical(2, count($rows));
	}
	
	public function testCreateTable_フルスタック()
	{
		$byAdmin = $this->target->getUser(1002);
		$icons = $this->target->getIcons();
		$referenceTable = $this->target->getTable(1001);
	
		$table = new Table();
		$table->setName('new table');
		$table->setDescription('description of table');
		$table->setIcon($icons[0]);
		$table->setApiEnabled(true);
		$table->setRoundScale(3);
		$table->setRoundType('ceil');
		$column1 = $column = $table->addColumn(Column::TEXT);
		$column->setLabel("Label 1");
		$column->setCode("文字列");
		$column->setShowsField(true);
		$column->setProperty('showsLabel', true);
		$column->setPositionX(1);
		$column->setPositionY(2);
		$column->setRequired(true);
		$column->setUnique(true);
		// 		$column->setRequired(true);
		// 		$column->setUnique(true);
		$column->setMinLength(1);
		$column->setMaxLength(100);
		$column->setDefaultValue('default');
		$column->setExpression('更新者');
		$column = $table->addColumn(Column::MULTITEXT);
		$column->setLabel("Label 2");
		$column->setShowsField(false);
		$column->setShowsLabel(false);
		$column->setSizeHeight(3);
		$column->setSizeWidth(5);
		$column = $table->addColumn(Column::RICHTEXT);
		$column->setLabel("Label 3");
		$column = $table->addColumn(Column::NUMBER);
		$column->setLabel("Label 4");
		$column->setRequired(false);
		$column->setUnique(false);
		$column->setMinValue("-100");
		$column->setMaxValue("100");
		$column->setShowsDigit(true);
		$column = $table->addColumn(Column::CALC, $table);
		$column->setLabel("Label 5");
		$column->setCode($column->getTypeName());
		$column6 = $table->addColumn(Column::RADIO);
		$column6->setLabel("Label 6");
		// var_dump($column6->getRawProperties());
		$column7 = $table->addColumn(Column::CHECKBOX);
		$column7->setLabel("Label 7");
		$option7_1 = new Option();
		$option7_1->setName("New Dropdown 1");
		$column7->_addOption($option7_1);
		$option7_2 = new Option();
		$option7_2->setName("New Dropdown 2");
		$column7->_addOption($option7_2);
		$column7->setCode($column7->getTypeName());
		$column7->setDefaultValue(array($option7_1, $option7_2));	// 初期選択
		$column8 = $table->addColumn(Column::DROPDOWN);
		$column8->setLabel("Label 8");
		$option8_1 = new Option();
		$option8_1->setName("New Option 1");
		$column8->_addOption($option8_1);
		$option8_2 = new Option();
		$option8_2->setName("New Option 2");
		$column8->_addOption($option8_2);
		$column8->setDefaultValue($option8_2);	// 初期選択
		$column = $table->addColumn(Column::DATE);
		$column->setLabel("Label 9");
		$column = $table->addColumn(Column::TIME);
		$column->setLabel("Label 10");
		$column = $table->addColumn(Column::DATETIME);
		$column->setLabel("Label 11");
		$column = $table->addColumn(Column::FILE);
		$column->setLabel("Label 12");
		$column->setThumbnailMaxSize(200);
		$column = $table->addColumn(Column::LINK);
		$column->setLabel("Label 13");
		$column->setLinkType('email');
		$column = $table->addColumn(Column::RELATIONAL);
		$column->setLabel("Label 14");
		// 			$reference = new Reference();
		$column->setReferenceTable($referenceTable);
		$column->setReferenceTargetColumn($referenceTable->getColumn(0));
		$column->setReferenceSourceColumn($column1);
		$column->setReferenceColumns(array($referenceTable->getColumn(0), $referenceTable->getColumn(1)));
		$column->setReferenceSortColumn($referenceTable->getColumn(0));
		$column->setReferenceSortsReverse(true);
		$column->setReferenceMaxRow(100);
		// 			$column->setReferenceCopyMap(array(array($referenceTable->getColumn(0), $column1)));
		// 			$column->setReference($reference);
		$column = $table->addColumn(Column::LOOKUP);
		$column->setLabel("Label 15");
		$column->setReferenceTable($referenceTable);
		$column->setReferenceTargetColumn($referenceTable->getColumn(0));
		$column = $table->addColumn(Column::ROWNUM);
		$column->setLabel("Label 16");
		$column = $table->addColumn(Column::CREATED_BY);
		$column->setLabel("Label 17");
		$column = $table->addColumn(Column::CREATED_ON);
		$column->setLabel("Label 18");
		$column = $table->addColumn(Column::UPDATED_BY);
		$column->setLabel("Label 19");
		$column = $table->addColumn(Column::UPDATED_ON);
		$column->setLabel("Label 20");
		$column = $table->addColumn(Column::LABEL);
		$column->setLabel("Label 21");	//実際は設定できない
		$column->setProperty('labelHtml', "<p>abc</p>");
		$column = $table->addColumn(Column::SPACE);
		$column->setLabel("Label 22");	//実際は設定できない
		$column = $table->addColumn(Column::LINE);
		$column->setLabel("Label 23");	//実際は設定できない
		$column = $table->addColumn(Column::USER_SELECTOR);
		$column->setLabel("Label 24");	//実際は設定できない
		$table->setViewColumns(array($column1, $column7, $column8));
		$table->create($byAdmin);
	
		$this->assertNotNull($table->getId());
		$this->assertNotNull($table->getRevision());
		$this->assertNotNull($table->getOwner());
		$this->assertNotNull($table->getOwner()->getId());
	
		// trunk/release テーブルが作成されていること
		$rows = $this->selectTable('table_def', 'id='.$table->getId());
		$this->assertIdentical(2, count($rows));
		$this->assertIdentical('new table', $rows[0]->name);
		$this->assertIdentical('new table', $rows[1]->name);
		$this->assertIdentical('description of table', $rows[0]->description);
		$this->assertIdentical('description of table', $rows[1]->description);
		$this->assertIdentical($table->getIcon()->getFilekey(), $rows[0]->icon_filekey);
		$this->assertIdentical($table->getIcon()->getFilekey(), $rows[1]->icon_filekey);
		// 		$this->assertIdentical('production', $rows[0]->def_type);
		$this->assertIdentical('0', $rows[0]->is_public);
		$this->assertIdentical('0', $rows[1]->is_public);
		// 		$this->assertIdentical(null, $rows[0]->is_saved);
		$this->assertIdentical('0', $rows[0]->is_api_enabled);
		$this->assertIdentical('0', $rows[1]->is_api_enabled);
		$this->assertIdentical('1002', $rows[0]->owner);
		$this->assertIdentical('1002', $rows[1]->owner);
		// 		$this->assertIdentical('1', $rows[0]->version);
		// 		$this->assertIdentical('1', $rows[1]->version);
		$this->assertIdentical('ceil', $rows[0]->round_type);
		$this->assertIdentical('ceil', $rows[1]->round_type);
		$this->assertIdentical('3', $rows[0]->round_scale);
		$this->assertIdentical('3', $rows[1]->round_scale);
	
		// 		$this->assertRegistered('table_def', array(@id => $table->getId(), @name => 'new table', @description=>'description of table',
		// 			'icon_filekey' => $table->getIcon()->getFilekey(), @def_type=>'production', @is_public => 0,
		// 			@is_saved => 0, @is_api_enabled => 0, @owner => 1002, @version => 1,
		// 			@round_type=>'ceil', @round_scale=>3));
		// 		$this->assertRegistered('table_def', array(@name => 'new table', @description=>'description of table',
		// 			'icon_filekey' => $table->getIcon()->getFilekey(), @def_type=>'work', @is_public => 0, @is_saved => null, @is_api_enabled => 0, @owner => 1002, @version => 1,
		// 			@round_type=>'ceil', @round_scale=>3));
	
		// 		// ワークテーブルに新しいID
		// 		$this->assertNotIdentical($table->getId(), $table->_getWorkTable()->getId());
	
		// カラムが作成され、すべてのプロパティーが設定されていること
		// 		$rows = $this->selectTable('column_def', "label= 'Label 1'");
		// 		$this->assertIdentical(2, count($rows));
		// 		$this->assertIdentical(Column::TEXT, $rows[0]->column_type);
		// 		$this->assertIdentical('Label 1', $rows[0]->label);
		// 		$this->assertIdentical('文字列', $rows[0]->code);
		// 		$this->assertPattern('/"showsField":true/', $rows[0]->properties);
		// 		$this->assertPattern('/"showsLabel":true/', $rows[0]->properties);
		// 		$this->assertPattern('/"positionX":1[,}]/', $rows[0]->properties);
		// 		$this->assertPattern('/"positionY":2[,}]/', $rows[0]->properties);
		// 		$this->assertPattern('/"isUnique":true/', $rows[0]->properties);
		// 		$this->assertPattern('/"isRequired":true/', $rows[0]->properties);
		// 		$this->assertPattern('/"minLength":1[,}]/', $rows[0]->properties);
		// 		$this->assertPattern('/"maxLength":100/', $rows[0]->properties);
		// 		$this->assertPattern('/"defaultValue":"default"/', $rows[0]->properties);
		// 		$this->assertPattern('/"expression":"更新者"/', $rows[0]->properties);
	
		// 		$rows = $this->selectTable('column_def', "label= 'Label 2'");
		// 		$this->assertIdentical(2, count($rows));
		// 		$this->assertIdentical(Column::MULTITEXT, $rows[0]->column_type);
		// 		$this->assertIdentical('Label 2', $rows[0]->label);
		// 		$this->assertPattern('/"showsField":false/', $rows[0]->properties);
		// 		$this->assertPattern('/"showsLabel":false/', $rows[0]->properties);
		// 		$this->assertPattern('/"positionX":null/', $rows[0]->properties);
		// 		$this->assertPattern('/"positionY":null/', $rows[0]->properties);
		// 		$this->assertPattern('/"sizeHeight":3/', $rows[0]->properties);
		// 		$this->assertPattern('/"sizeWidth":5/', $rows[0]->properties);
		// 		$this->assertPattern('/"isRequired":false/', $rows[0]->properties);
			
		$this->assertRegistered('column_def', array(@column_type => Column::TEXT, 'label' =>  'Label 1', 'code'=> '文字列', 'properties' => array(  @showsField =>true, @showsLabel => true, @positionX => 1, @positionY => 2, @isUnique => true, @isRequired => true, @minLength => 1, @maxLength=>100,  @defaultValue => "default", @expression => '更新者')), 2);
		$this->assertRegistered('column_def', array(@column_type => Column::MULTITEXT, 'label' =>  'Label 2', 'properties' => array('showsField' => false, @showsLabel => false, @positionX => null,  @positionY => null, @sizeHeight => 3, @sizeWidth => 5, @isRequired => false)), 2);
		$this->assertRegistered('column_def', array(@table_id=> $table->getId(), @column_type => Column::RICHTEXT, 'label' =>  'Label 3', 'properties'=> array('showsField' => true, 'showsLabel' => true)), 2);
		$this->assertRegistered('column_def', array(@table_id=> $table->getId(), @column_type => Column::NUMBER, 'label' =>  'Label 4', @properties => array(@isUnique => false, @isRequired => false, @minValue => -100, @maxValue => 100, @showsDigit => true)), 2);
		$this->assertRegistered('column_def', array(@table_id=> $table->getId(), @column_type => Column::CALC, 'label' =>  'Label 5'), 2);
		$this->assertRegistered('column_def', array(@table_id=> $table->getId(), @column_type => Column::RADIO, 'label' =>  'Label 6'), 2);
		$this->assertRegistered('column_def', array(@table_id=> $table->getId(), @column_type => Column::CHECKBOX, 'label' =>  'Label 7', @properties => array(@defaultValueIds => array($option7_1->getId(), $option7_2->getId()))), 2);
		$this->assertRegistered('column_def', array(@table_id=> $table->getId(), @column_type => Column::DROPDOWN, 'label' =>  'Label 8', @properties => array(@defaultValueId => $option8_2->getId())), 2);
		$this->assertRegistered('column_def', array(@column_type => Column::DATE, 'label' =>  'Label 9'), 2);
		$this->assertRegistered('column_def', array(@column_type => Column::TIME, 'label' =>  'Label 10'), 2);
		$this->assertRegistered('column_def', array(@column_type => Column::DATETIME, 'label' =>  'Label 11'), 2);
		$this->assertRegistered('column_def', array(@column_type => Column::FILE, 'label' =>  'Label 12', @properties => array(@thumbnailMaxSize => 200)), 2);
		$this->assertRegistered('column_def', array(@column_type => Column::LINK, 'label' =>  'Label 13', @properties => array(@linkType => 'email')), 2);
		$this->assertRegistered('column_def', array(@column_type => Column::RELATIONAL, 'label' =>  'Label 14',  @properties => array(
			@referenceTableId => $referenceTable->getId(),
			@referenceTargetColumnId => 1,
			@referenceSourceColumnId => $column1->getId(),
			@referenceColumnIds => array($referenceTable->getColumn(0)->getId(), $referenceTable->getColumn(1)->getId()),
			@referenceSortColumnId => 1,
			@referenceSortsReverse => true,
			@referenceMaxRow=> 100)
		), 2);
		$this->assertRegistered('column_def', array(@column_type => Column::LOOKUP, 'label' => 'Label 15'), 2);
		$this->assertRegistered('column_def', array(@column_type => Column::ROWNUM, 'label' => 'Label 16'), 2);
		$this->assertRegistered('column_def', array(@column_type => Column::CREATED_BY, 'label' => 'Label 17'), 2);
		$this->assertRegistered('column_def', array(@column_type => Column::CREATED_ON, 'label' => 'Label 18'), 2);
		$this->assertRegistered('column_def', array(@column_type => Column::UPDATED_BY, 'label' => 'Label 19'), 2);
		$this->assertRegistered('column_def', array(@column_type => Column::UPDATED_ON, 'label' => 'Label 20'), 2);
		$this->assertRegistered('column_def', array(@column_type => Column::LABEL, 'label' => 'Label 21', @properties => array( @labelHtml => '<p>abc</p>' )), 2);
		$this->assertRegistered('column_def', array(@column_type => Column::SPACE, 'label' => 'Label 22' ), 2);
		$this->assertRegistered('column_def', array(@column_type => Column::LINE, 'label' => 'Label 23' ), 2);
		$this->assertRegistered('column_def', array(@column_type => Column::USER_SELECTOR, 'label' => 'Label 24' ), 2);
	
		// オプションが登録される
		$this->assertRegistered('option', array(@id => $option8_1->getId(), @name=>'New Option 1', @seq=> 1, @table_id => $table->getId(), @column_id => $column8->getId() ), 2);
		$this->assertRegistered('option', array(@id => $option8_2->getId(), @name=>'New Option 2', @seq=> 2, @table_id => $table->getId(), @column_id => $column8->getId() ), 2);
		// ビューがコピーされる
		$this->assertRegistered('view_column', array('table_id' => $table->getId() ), 3 * 2);
		$this->assertRegistered('view_column', array('table_id' => $table->getId(), 'column_id' => $column1->getId(), 'seq' => 1 ), 2);
		$this->assertRegistered('view_column', array('table_id' => $table->getId(), 'column_id' => $column7->getId(), 'seq' => 2 ), 2);
		$this->assertRegistered('view_column', array('table_id' => $table->getId(), 'column_id' => $column8->getId(), 'seq' => 3 ), 2);
		$this->assertRegistered('view_column', array('table_id' => $table->getId() ), 3 * 2);
		// カレンダー設定はない
		$this->assertRegistered('view_calendar_setting', array(1=>1), 0);
	}
	
	public function testCreateTable_コードの必須チェック()
	{
		$table = $this->mockUpTable();
		$table->create($this->target->getUser(1001));
		$column_0 = $table->addColumn(Column::TEXT);
		$column_0->setCode("");
		$column_0->setLabel("<文字列>");
	
		$errors = $table->validate();
		$this->assertIdentical(1, count($errors));
		$this->assertIdentical('入力してください。', $errors[0]->getMessage());
	}
	
	public function testCreateTable_コードの重複チェック()
	{
		$table = $this->mockUpTable();
		$table->create($this->target->getUser(1001));
		$column_0 = $table->addColumn(Column::TEXT);
		$column_0->setCode("A");
		$column_0->setLabel("<文字列>");
		$column_1 = $table->addColumn(Column::TEXT);
		$column_1->setCode("A");	//重複
		$column_1->setLabel("<文字列>");
	
		$errors = $table->validate();
		$this->assertIdentical(2, count($errors));
		$this->assertIdentical('他のフィールドと重複しています。', $errors[0]->getMessage());
		$this->assertIdentical('他のフィールドと重複しています。', $errors[1]->getMessage());
	}
	
	public function testCreateTableFromTemplate_テンプレートから作成する()
	{
		copy(dirname(__FILE__). '/icon.png', dirname(__FILE__).'/asset/icons/default1.png');
	
		// 基本的に既存テーブルから作成するパターンと同じなので割愛
		$byAdmin = $this->target->getUser(1002);
		$templates = $this->target->getTemplates();
		$table = $templates[0]->createTable($byAdmin);
		$this->pass();
	}

	public function testHasPendingRelease()
	{
		$byAdmin = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$table->create($byAdmin);
		$this->assertIdentical(false, $table->hasPendingRelease());
	}
	
	public function testCreateTable_共通フィールドが自動登録される()
	{
		$byAdmin = $this->target->getUser(1002);
		$icons = $this->target->getIcons();

		$table = new Table();
		$table->setName('new table');
		$table->setDescription('description of table');
		$table->setIcon($icons[0]);
		$column = $table->addColumn(Column::TEXT);
		$column->setCode("Col1");
		$table->create($byAdmin);
	
		$this->assertIdentical(6, count($table->getColumns()));

		// 自動カラムが追加され、必須プロパティが設定される
		$this->assertRegistered('column_def', array( 'column_type' => Column::TEXT, 'code' => 'Col1', ), 2);
		$this->assertRegistered('column_def', array( 'column_type' => Column::CREATED_ON, 'code' => '作成日時', 'label'=>'作成日時', @properties => array( @showsField => false, @showsLabel=> true )), 2);
		$this->assertRegistered('column_def', array( 'column_type' => Column::CREATED_BY, 'code' => '作成者', 'label'=>'作成者', @properties => array( @showsField =>false, @showsLabel=> true , )), 2);
		$this->assertRegistered('column_def', array( 'column_type' => Column::UPDATED_ON, 'code' => '更新日時', 'label'=>'更新日時', @properties => array( @showsField =>false, @showsLabel=> true ) ), 2);
		$this->assertRegistered('column_def', array( 'column_type' => Column::UPDATED_BY, 'code' => '更新者', 'label'=>'更新者', @properties => array( @showsField =>false, @showsLabel=> true )), 2);
		$this->assertRegistered('column_def', array( 'column_type' => Column::ROWNUM, 'code' => 'データ番号', 'label'=>'データ番号', @properties => array( @showsField =>false, @showsLabel=> true )), 2); // フィールド名変更
	}
	
	public function testCreateTable_共通フィールドが設定されている場合()
	{
		$byAdmin = $this->target->getUser(1002);
		$icons = $this->target->getIcons();
		$referenceTable = $this->target->getTable(1001);
	
		$table = new Table();
		$table->setName('new table');
		$table->setDescription('description of table');
		$table->setIcon($icons[0]);
		$column = $table->addColumn(Column::TEXT);
		$column->setLabel("Col1");
		$column = $table->addColumn(Column::CREATED_ON);
		$column->setLabel('Col2');
		$column = $table->addColumn(Column::CREATED_BY);
		$column->setLabel('Col3');
		$column = $table->addColumn(Column::UPDATED_ON);
		$column->setLabel('Col4');
		$column = $table->addColumn(Column::UPDATED_BY);
		$column->setLabel('Col5');
		$column = $table->addColumn(Column::ROWNUM);
		$column->setLabel('Col6');
		$table->create($byAdmin);
	
		// 自動追加しない
		$this->assertRegistered('column_def', array( 'column_type' => Column::TEXT, 'label' => 'Col1', ), 2);	// 追加されているものを使用する
		$this->assertRegistered('column_def', array( 'column_type' => Column::CREATED_ON, 'label' => 'Col2' ), 2);
		$this->assertRegistered('column_def', array( 'column_type' => Column::CREATED_BY, 'label' => 'Col3' ), 2);
		$this->assertRegistered('column_def', array( 'column_type' => Column::UPDATED_ON, 'label' => 'Col4' ), 2);
		$this->assertRegistered('column_def', array( 'column_type' => Column::UPDATED_BY, 'label' => 'Col5' ), 2);
		$this->assertRegistered('column_def', array( 'column_type' => Column::ROWNUM, 'label' => 'Col6' ), 2);
	}
	
	public function testCreateTable_アイコンがコピーされる()
	{
		$byAdmin = $this->target->getUser(1002);
		$icons = $this->target->getIcons();
	
		$table = new Table();
		$table->setName('new table');
		$table->setDescription('description of table');
		$table->setIcon($icons[0]);
		$table->setApiEnabled(true);
		$table->create($byAdmin);
	
		$this->assertNotIdentical($table->getIcon()->getFilekey(), $icons[0]->getFilekey());
		$this->assertTrue(file_exists($table->getIcon()->_getLocalPath()), "file dose not exists ".$table->getIcon()->_getLocalPath());	// ファイルが存在すること
	
		$this->assertRegistered('table_def', array(@name => 'new table', 'description'=>'description of table',
			'icon_filekey' => $table->getIcon()->getFilekey()), 2);
	}
	
	public function testCreateTableFromTemplate_既存テーブルから作成()
	{
		$byAdmin = $this->target->getUser(1002);
		$baseTable = $this->target->getTable(3001);
		copy(dirname(__FILE__).'/preset/icon01.png', $baseTable->getIcon()->_getLocalPath());	// アイコンがないと落ちるので
	
		$template = $this->target->getTemplateFromTable($baseTable);
		$template->setName("new table from template");
		$template->setDescription("新しいテーブル");
		$table = $template->createTable($byAdmin);
	
		$this->assertNotNull($table->getId());
		$this->assertNotIdentical($table->getId(), $baseTable->getId());
		$this->assertNotIdentical($baseTable->getIcon()->getFilekey(), $table->getIcon()->getFilekey());	// アイコンが複製され別ファイルになること
		$this->assertNotIdentical($baseTable->getApiKey(), $table->getApiKey());	// 異なるAPIキーが発行されること
		$this->assertTrue(file_exists($table->getIcon()->_getLocalPath()));	// アイコンが存在すること
		// 		$this->assertNotIdentical($table->getId(), $table->_getWorkTable()->getId());
		$this->assertIdentical($table->getId(), $table->getColumn(0)->getParent()->getId());	//　内部状態が更新されること
		$this->assertIdentical($table->getId(), $table->getViewColumn(0)->getParent()->getId());
	
		// 新しいテーブルが作成される
		$this->assertRegistered('table_def', array(@id => $table->getId(), @name => 'new table from template', @description=>'新しいテーブル', 'icon_filekey!*' => 'icon301', @is_public => 0, @is_api_enabled => 0, @owner => 1002, @round_type=>'ceil', @round_scale=>'3', @is_template => 0), 2);
		// カラムが作成されること（カラムIDは新規発番）
		$this->assertRegistered('column_def', array(@table_id => $table->getId(), 'id!' => 300101) , 24 * 2);
		// オプションがコピーされる
		$this->assertRegistered('option', array(@table_id => $table->getId(), 'id!' => 30010601) , 9 * 2);
		// ビューがコピーされる
		$this->assertRegistered('view_column', array(@table_id => $table->getId()) , 4 * 2);
	}
	
	public function testRelease_データの更新()
	{
		$this_ = $this;
		$by = $this->target->getUser(1003);	// 他の一般ユーザー
		$release = $this->mockUpTable();
		$release->create($by);
	
		$row = $release->addRow();
		$release->insertRows($by);
	
		$trunk = $this->target->getTable($release->getId(), Engine::TRUNK);
		$trunk->setName('Changed table');
		$column = $trunk->addColumn(Column::TEXT);
		$column->setLabel('New text column');
		$trunk->saveChanges($by);
	
		// Precondition test.
		$this->assertRegistered('column_def', array('label'=>'New text column', 'id'=>$column->getId()), 1);
		$this->assertRegistered('table_def', array('name'=>'Changed table', 'id'=>$trunk->getId()), 1);
		$this->assertRegistered('release_control', array('table_id'=>$trunk->getId(), 'trunk_revision'=>$trunk->getRevision(), 'release_revision'=>$release->getRevision()), 1);
		$this->assertNotRegistered('indexes', array('column_id'=>$column->getId()));

		$trunkRevisionBeforeRelease = $trunk->getRevision();

		// Do release
		$trunk->release($by);
	
		$this->assertRegistered('column_def', array('label'=>'New text column', 'id'=>$column->getId()), 2);
		$this->assertRegistered('table_def', array('name'=>'Changed table', 'id'=>$trunk->getId()), 2);
		$this->assertRegistered('release_control', array('table_id'=>$trunk->getId(), 'trunk_revision'=>$trunkRevisionBeforeRelease+1, 'release_revision'=>$trunkRevisionBeforeRelease), 1);
		$this->assertRegistered('indexes', array('column_id'=>$column->getId()), 1);	// インデックスが作成されていること（検索できるようにするため）
	}
	
	public function testGetOwner_テーブルの作成者が存在しない()
	{
		$admin = $this->target->getUser(1002);
		$by = $this->target->getUser(1001);
		$table = $this->mockUpTable();
		$table->create($by);
	
		$table->getOwner()->delete($admin);	//作成者を消す
		$table = $this->target->getTable($table->getId());
		$this->assertNull($table->getOwner());//Nullになる
	}
	
	public function testGetRowFor()
	{
		$by = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$table->create($by);
		
		$row = $table->addRow();
		$table->insertRows($by);
	
		$row_1 = $table->getRowFor($row->getId());
		$this->assertIdentical($row->getId(), $row_1->getId());
	}
	
	public function testGetRowFor_レコード番号で検索()
	{
		$by = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$table->create($by);
	
		$row = $table->addRow();
		$table->insertRows($by);
	
		$row_1 = $table->getRowFor(1, null, true);
		$this->assertIdentical($row->getId(), $row_1->getId());
	}
	
	public function testGetRowFor_レコードが存在しない()
	{
		$table = $this->mockUpTableFully();
		$table->create($this->target->getUser(1001));
	
		try {
			$table->getRowFor(-1);	// 例外が発生する
			$this->fail(sprintf('Should not be successfull.'));
		} catch (TxCoreException $e) {
		}
		$this->pass();
	}
	
	public function testRelease_カラムの削除()
	{
		$this_ = $this;
		$storage = $this->target->getStorage();
		$by = $this->target->getUser(1003);	// 他の一般ユーザー
		$release = $this->mockUpTableFully();
		$release->create($by);
	
		$file = File::create(dirname(__FILE__).'/file.xls', 'sample.xls', 'application/excel');
		$row = $release->addRow();
		$row->setValue($release->getColumn(5), $release->getColumn(5)->getOption(0));
		$row->setValue($release->getColumn(11), array($file));
		$release->insertRows($by);
		
		$filePath = $storage->getFilePath($file->getFilekey());
		$this->assertIdentical(true, file_exists($filePath));	// ファイルができる
	
		$trunk = $this->target->getTable($release->getId(), Engine::TRUNK);
		$trunk->removeColumn($column_5 = $trunk->getColumn(5));	// Radio
		$trunk->removeColumn($column_11 = $trunk->getColumn(11));	// File
		$trunk->saveChanges($by);
	
		// Precondition test.
		$this->assertNotRegistered('column_def', array('column_type'=>Column::RADIO, 'table_id'=>$trunk->getId(), 'revision'=>2));
		$this->assertNotRegistered('column_def', array('column_type'=>Column::FILE, 'table_id'=>$trunk->getId(), 'revision'=>2));
		$this->assertNotRegistered('option', array('column_id'=>$column_5->getId(),'revision'=>2));
		$this->assertRegistered('column_def', array('column_type'=>Column::RADIO, 'table_id'=>$trunk->getId(), 'revision'=>1));
		$this->assertRegistered('column_def', array('column_type'=>Column::FILE, 'table_id'=>$trunk->getId(), 'revision'=>1));
		$this->assertRegistered('option', array('column_id'=>$column_5->getId(),'revision'=>1), 3);
		$this->assertRegistered('data', array('column_id'=>$column_5->getId()), 1);
		$this->assertRegistered('indexes', array('column_id'=>$column_5->getId()), 1);
	
		// リリースする
		$trunk->release($by);
	
		$this->assertNotRegistered('column_def', array('column_type'=>Column::RADIO, 'table_id'=>$trunk->getId(), 'revision'=>3));
		$this->assertNotRegistered('column_def', array('column_type'=>Column::FILE, 'table_id'=>$trunk->getId(), 'revision'=>3));
		$this->assertNotRegistered('option', array('column_id'=>$column_5->getId(),'revision'=>3));
		$this->assertNotRegistered('column_def', array('column_type'=>Column::RADIO, 'table_id'=>$trunk->getId(), 'revision'=>2));
		$this->assertNotRegistered('column_def', array('column_type'=>Column::FILE, 'table_id'=>$trunk->getId(), 'revision'=>2));
		$this->assertNotRegistered('option', array('column_id'=>$column_5->getId(),'revision'=>2));
		// データが削除されている
		$this->assertNotRegistered('data', array('column_id'=>$column_5->getId()));
		$this->assertNotRegistered('indexes', array('column_id'=>$column_5->getId()));
		
		// カラムが削除されたのでファイルも削除されている
		$storage->flush();
		$this->assertIdentical(false, file_exists($filePath));
	}
	
	public function testValidate_計算式が不正()
	{
		$table = $this->mockUpTable();
		$table->create($this->target->getUser(1001));
		$column_0 = $table->addColumn(Column::CALC);
		$column_0->setCode('計算_1');
		$column_0->setExpression('*');
		$column_0->setLabel('<計算>');
		$column_1 = $table->addColumn(Column::NUMBER);
		$column_1->setCode('数値_1');
		$column_1->setLabel('<数値>');
	
		$errors = $table->validate();
		$this->assertIdentical(1, count($errors));
		$this->assertPattern('/<計算>/u', $errors[0]->getMessage());
	}
	
	public function testValidate_文字列式が不正()
	{
		$table = $this->mockUpTable();
		$table->create($this->target->getUser(1001));
		$column_0 = $table->addColumn(Column::TEXT);
		$column_0->setExpression('*');
		$column_0->setLabel('<文字>');
		$column_0->setCode('文字_1');
	
		$errors = $table->validate();
		$this->assertIdentical(1, count($errors));
		$this->assertPattern('/<文字>/u', $errors[0]->getMessage());
	}
	
	public function testValidate_文字列式のコードが存在しない()
	{
		$table = $this->mockUpTable();
		$table->create($this->target->getUser(1001));
		$column_0 = $table->addColumn(Column::TEXT);
		$column_0->setExpression('NUMBER_1&"あいう"');
		$column_0->setLabel('<文字>');
		$column_0->setCode('文字列');
	
		$errors = $table->validate();
		$this->assertIdentical(1, count($errors));
		$this->assertPattern('/<文字>/u', $errors[0]->getMessage());
	}
	
	public function testValidate_文字列式が式を持つ文字列カラムを参照している()
	{
		$table = $this->mockUpTable();
		$table->create($this->target->getUser(1001));
		$column_0 = $table->addColumn(Column::TEXT);
		$column_0->setExpression('文字_1');
		$column_0->setLabel('<文字_0>');
		$column_0->setCode('文字_0');
		$column_1 = $table->addColumn(Column::TEXT);
		$column_1->setExpression('文字_2');
		$column_1->setCode('文字_1');
		$column_1->setLabel('<文字_1>');
		$column_2 = $table->addColumn(Column::TEXT);
		$column_2->setCode('文字_2');
		$column_2->setLabel('<文字_2>');
	
		$errors = $table->validate();
		$this->assertIdentical(1, count($errors));
		$this->assertPattern('/<文字_0>/u', $errors[0]->getMessage());
	}
	
	public function testValidate_計算式のコードが存在しない()
	{
		$table = $this->mockUpTable();
		$table->create($this->target->getUser(1001));
		$column_0 = $table->addColumn(Column::CALC);
		$column_0->setExpression('1*XXX');
		$column_0->setLabel('<計算>');
		$column_0->setCode("計算");
	
		$errors = $table->validate();
		$this->assertIdentical(1, count($errors));
		$this->assertPattern('/<計算>/u', $errors[0]->getMessage());
		$this->assertPattern('/XXX/u', $errors[0]->getMessage());
	}
	
	public function testValidate_式がオペランド不可のカラム()
	{
		$table = $this->mockUpTable();
		$table->create($this->target->getUser(1001));
		$column_0 = $table->addColumn(Column::CALC);
		$column_0->setExpression('ドロップダウン*1');
		$column_0->setLabel('<計算>');
		$column_0->setCode('計算');
		$column_1 = $table->addColumn(Column::DROPDOWN);
		$column_1->setLabel('<ドロップダウン>');
		$column_1->setCode('ドロップダウン');
	
		$errors = $table->validate();
		$this->assertIdentical(1, count($errors));
		$this->assertPattern('/<計算>/u', $errors[0]->getMessage());
		$this->assertPattern('/'.$column_1->getTypeName().'/u', $errors[0]->getMessage());
	}
	
	public function testValidate_初期値の数値チェック()
	{
		$table = $this->mockUpTable();
		$table->create($this->target->getUser(1001));
		$column_0 = $table->addColumn(Column::NUMBER);
		$column_0->setLabel('<数値>');
		$column_0->setCode('数値');
		$column_0->setDefaultValue('a');
	
		$errors = $table->validate();
		$this->assertIdentical(1, count($errors));
		$this->assertIdentical('数字で入力してください。', $errors[0]->getMessage());
		
		$column_0->setDefaultValue('0.0000');
		$this->assertIdentical(0, count($table->validate()));
		$column_0->setDefaultValue('-10.0000');
		$this->assertIdentical(0, count($table->validate()));
	}
}
