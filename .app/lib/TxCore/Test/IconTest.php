<?php
namespace TxCore\Test;

use TxCore\Icon;

class IconTest extends BaseTestCase
{
	public function testUpload() 
	{
		$icon = Icon::upload(dirname(__FILE__) .'/icon.png');
		$this->assertNotNull($icon);
		$this->assertNoPattern('/(?=icon)/', $icon->getFilekey());
		$this->assertTrue(file_exists($icon->_getLocalPath()));
	}
}
