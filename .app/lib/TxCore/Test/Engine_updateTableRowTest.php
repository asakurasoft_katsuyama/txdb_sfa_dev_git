<?php
namespace TxCore\Test;

use TxCore\Row;
use TxCore\TxCoreException;
use TxCore\Column;

class Engine_updateTableRowTest extends BaseTestCase
{
	public function setUp()
	{
        parent::setUpDB(__DIR__.'/EngineTest.sql');
	}

	public function testUpdateTableRow_共通カラムの自動設定()
	{
		$by = $this->target->getUser(1001);
		$table = $this->mockUpTable();
		$table->create($by);
	
		$row = new Row($table);
		$table->insertRow($row, $by);
		$row = $table->getRowFor($row->getId());
	
		$this->assertIdentical('1', $row->getValue($table->getColumn(0)));
		$this->assertIdentical(1001, $row->getValue($table->getColumn(1))->getId());
		$this->assertNotNull($row->getValue($table->getColumn(2)));
		$this->assertIdentical(1001, $row->getValue($table->getColumn(3))->getId());
		$this->assertNotNull($row->getValue($table->getColumn(4)));
		$dateWhenInserted = $row->getValue($table->getColumn(4));
	
		$this->target->setNow(time());
		$by = $this->target->getUser(1002);
		$row->update($by);	// 共通カラムが自動更新される
		
		$row1 = $table->getRowFor($row->getId());
		$this->assertIdentical('1', $row1->getValue($table->getColumn(0)));
		$this->assertIdentical(1001, $row1->getValue($table->getColumn(1))->getId());
		$this->assertNotNull($row1->getValue($table->getColumn(2)));
		$this->assertIdentical(1002, $row1->getValue($table->getColumn(3))->getId());	// 更新されていること
		$this->assertNotIdentical($dateWhenInserted, $row1->getValue($table->getColumn(4)));	// 更新されていること
		
		$this->target->setNow(null);
	}
	
	public function testUpdateTableRow_再計算()
	{
		$by = $this->target->getUser(1001);
	
		$table = $this->mockUpTable();
		$a = $table->addColumn(Column::NUMBER);
		$a->setCode("A");
		$c = $table->addColumn(Column::CALC);
		$c->setCode("C");
		$c->setExpression("A*2");
		$table->create($by);
	
		$row = new Row($table);
		$row->setValue($a, 1);
		$table->insertRow($row, $table->getOwner());
	
		$row = $table->getRowFor($row->getId());
		$this->assertIdentical('2', $row->getValue($c));
		
		$row->setValue($a, 2);
		$row->update($table->getOwner());
		
		$row = $table->getRowFor($row->getId());
		$this->assertIdentical('4', $row->getValue($c));	// 再計算されること
	}

	public function testUpdateTableRow_計算に失敗した場合ヌルで更新される()
	{
		$by = $this->target->getUser(1001);
	
		$table = $this->mockUpTable();
		$c = $table->addColumn(Column::CALC);
		$c->setExpression("A*2");	// 不正な式
		$table->create($by);
	
		$row = new Row($table);
		$row->setValue($c, 1);
		$table->insertRow($row, $table->getOwner());
	
		$row = $table->getRowFor($row->getId());
		$this->assertIdentical(null, $row->getValue($c));
	}
		
	public function testUpdateTableRow_排他チェック()
	{
		$by = $this->target->getUser(1001);
	
		$table = $this->mockUpTable();
		$table->create($by);
	
		$row0 = $table->addRow();
		$table->insertRow($row0, $table->getOwner());
	
		$row1 = $table->getRowFor($row0->getId());
		$row1->update($table->getOwner());	// 他の人が更新する

		try {
			$row0->update($table->getOwner());	// 例外が発生する
			$this->fail(sprintf('Should not be successfull.'));	
		} catch (TxCoreException $e) {}
		
		$row1->update($table->getOwner());	// 正常に更新される
		$this->pass();
	}
}
