<?php
namespace TxCore\Test;

use TxCore\Column;
use TxCore\File;
use TxCore\Option;
use TxCore\Row;
use TxCore\TxCoreException;

class Engine_insertTableRowTest extends BaseTestCase
{
	public function setUp()
	{
        parent::setUpDB(__DIR__.'/EngineTest.sql');
	}

	public function testInsertTableRow_全カラムでテスト()
	{
		$table = $this->mockUpTableFully();
		$table->create($this->target->getUser(1001));

		$row = $table->addRow();
		$row->setValue($table->getColumn(0), 'textbox');
		$row->setValue($table->getColumn(1), 'multitext');
		$row->setValue($table->getColumn(2), 'richtext');
		$row->setValue($table->getColumn(3), '120');
// 		$row->setValue($table->getColumn(4), '');
		$row->setValue($table->getColumn(5), new Option());
		$table->insertRows($table->getOwner());

		$this->markTestIncomplete('Do implement');//TODO:
	}

	public function testInsertTableRow_共通カラムの自動更新()
	{
		$table = $this->mockUpTable();
		$column = $table->addColumn(Column::TEXT);
		$table->create($this->target->getUser(1001));

		$row = $table->addRow();
		$row->setValue($table->getColumn(0), 'textbox');
		$table->insertRows($table->getOwner());

		$row = $table->getRowFor($row->getId());
		$this->assertIdentical('1', $row->getValue($table->getColumn(1)));	// rownum
		$this->assertIdentical(1001, $row->getValue($table->getColumn(2))->getId());	// created_by
		$this->assertNotNull($row->getValue($table->getColumn(3)));	// created_on
		$this->assertIdentical(1001, $row->getValue($table->getColumn(4))->getId());	// updated_by
		$this->assertNotNull($row->getValue($table->getColumn(5)));	// updated_on
	}

	public function testInsertTableRow_リッチエディタのインデックス()
	{
		$table = $this->mockUpTable();
		$table->addColumn(Column::RICHTEXT);
		$table->create($this->target->getUser(1001));

		$row = $table->addRow();
		$row->setValue($table->getColumn(0), "<p>xxxxｱA\n</p>");
		$table->insertRows($table->getOwner());

		$this->assertRegistered('indexes', array(@row_id => $row->getId(), @string_value => 'xxxxあa'), 1);	// タグが削除され、文字が正規化される
	}

	public function testInsertTableRow_複数選択を未選択で登録した場合はインデックスにヌルを登録しておくただしｖａｌｕｅは空配列()
	{
		$this->markTestIncomplete('Do implement');//TODO:
	}

	public function testInsertTableRow_値を持たないカラムに値を設定する()
	{
		$this->markTestIncomplete('Do implement');//TODO:
	}

	public function testInsertTableRow_バリデーションが走ること()
	{
		$this->markTestIncomplete('Do implement');//TODO:
	}

	public function testInsertTableRow_日付フィールドに９９９９が入る()
	{
		$this->markTestIncomplete('Do implement');//TODO:
	}

	public function testInsertTableRow_権限チェック()
	{
		$byOwner = $this->target->getUser(1001);
		$byAdmin = $this->target->getUser(1002);
		$bySomeone = $this->target->getUser(1003);

		$sharedTable = $this->mockUpTable();
		$sharedTable->setPublic(true);
		$sharedTable->create($byOwner);
		$secretTable = $this->mockUpTable();
		$secretTable->setPublic(false);
		$secretTable->create($byOwner);

		$sharedTable->insertRow(new Row($sharedTable), $byOwner);	// Successfull
		$sharedTable->insertRow(new Row($sharedTable), $byAdmin);	// Successfull
		$sharedTable->insertRow(new Row($sharedTable), $bySomeone);	// Successfull
		$secretTable->insertRow(new Row($secretTable), $byOwner);	// Successfull
		$secretTable->insertRow(new Row($secretTable), $byAdmin);	// Successfull
		try {
			$secretTable->insertRow(new Row($secretTable), $bySomeone);	// Fail
			$this->fail('Should not be successfull');
		} catch (TxCoreException $e) {}
		$this->pass();
	}

	public function testInsertTableRow_計算()
	{
		$by = $this->target->getUser(1001);

		$table = $this->mockUpTable();
		$a = $table->addColumn(Column::NUMBER);
		$a->setCode("A");
		$b = $table->addColumn(Column::NUMBER);
		$b->setCode("B");
		$c = $table->addColumn(Column::CALC);
		$c->setCode("C");
		$c->setExpression("A+B");
		$table->create($by);

		$row = new Row($table);
		$row->setValue($a, 1);
		$row->setValue($b, 2);
		$table->insertRow($row, $by);

		$row = $table->getRowFor($row->getId());
		$this->assertIdentical('3', $row->getValue($c));
	}

	public function testInsertTableRow_計算_複合参照()
	{
		$by = $this->target->getUser(1001);

		$table = $this->mockUpTable();
		$column_d = $table->addColumn(Column::CALC);	 // D=B+C
		$column_d->setCode("D");
		$column_d->setExpression("B+C");
		$column_a = $table->addColumn(Column::NUMBER);
		$column_a->setCode("A");
		$column_b = $table->addColumn(Column::CALC);	// B=C*2
		$column_b->setCode("B");
		$column_b->setExpression("C*2");
		$column_c = $table->addColumn(Column::CALC);	 // C=A+1
		$column_c->setCode("C");
		$column_c->setExpression("A+1");
		$table->create($by);

		$row = new Row($table);
		$row->setValue($column_a, 2);
		$table->insertRow($row, $by);

		$row = $table->getRowFor($row->getId());
		$this->assertIdentical('6', $row->getValue($column_b));	// 依存関係を正しく計算できる
		$this->assertIdentical('3', $row->getValue($column_c));
		$this->assertIdentical('9', $row->getValue($column_d));
	}

	public function testInsertTableRow_計算_存在しないコード()
	{
		$by = $this->target->getUser(1001);

		$table = $this->mockUpTable();
		$column_a = $table->addColumn(Column::NUMBER);
		$column_a->setCode("A");
		$column_b = $table->addColumn(Column::CALC);	// B=[Z]*2
		$column_b->setCode("B");
		$column_b->setExpression("Z * 2");
		$column_c = $table->addColumn(Column::CALC);	 // C=B+1
		$column_c->setCode("C");
		$column_c->setExpression("B + 1");
		$table->create($by);

		$row = new Row($table);
		$row->setValue($column_a, 2);
		$table->insertRow($row, $by);

		$row = $table->getRowFor($row->getId());
		$this->assertIdentical(null, $row->getValue($column_b));  // コード(Z)が存在しない場合Null
		$this->assertIdentical(null, $row->getValue($column_c));	 // 参照先がエラーの場合エラーを伝搬
	}

	public function testInsertTableRow_計算_オペランドに値なし()
	{
		$by = $this->target->getUser(1001);

		$table = $this->mockUpTable();
		$column_a = $table->addColumn(Column::NUMBER);
		$column_a->setCode("A");
		$column_b = $table->addColumn(Column::CALC);
		$column_b->setCode("B");
		$column_b->setExpression("A * 2");
		$table->create($by);

		$row = new Row($table);
		$table->insertRow($row, $by);	// A=未設定

		$row = $table->getRowFor($row->getId());
		$this->assertIdentical(null, $row->getValue($column_b)); // 未設定値の計算は常にNull
	}

	public function testInsertTableRow_計算_文字列式()
	{
		$this->markTestIncomplete('Do implement');//TODO:
	}

	public function testInsertTableRow_計算_作成者と作成日時を計算式に使う()
	{
		// TODO : 今の作りだとcalculateしたタイミングでは設定されていない
		$this->markTestIncomplete('Do implement');//TODO:
	}

	public function testInsertTableRow_計算_日時と時間の計算()
	{
		$this->markTestIncomplete('Do implement');//TODO:
	}

	public function testInsertTableRow_計算_計算として使用できないもの()
	{
// 		数値
// 		計算
// 		時刻
// 		日時
//		作成日時
//	更新日時
		$this->markTestIncomplete('Do implement');//TODO:
	}

	public function testInsertTableRow_時間は分まで入力()
	{
		$this->markTestIncomplete('Do implement');//TODO:
	}

	public function testInsertRows_ファイル()
	{
		$by = $this->target->getUser(1001);
		$table  = $this->mockUpTable();
		$column_0 = $table->addColumn(Column::FILE);
		$table->create($by);

		$file = File::create(dirname(__FILE__).'/file.xls', '資料.xls', 'application/ms-excel');
		$row = $table->addRow();
// 		$this->assertIdentical('資料.xls', $file->getFileName());
// 		$this->assertPattern('/file.xls$/', $file->getFilekey());
		// 		$this->assertPattern(sprintf('#^/asset/files/%d/%d/.*.xls$#', $table->getId(), $column_0->getId()), $file->getUrl());
		// 		$this->assertTrue(file_exists(dirname(__FILE__) .'/asset/files/'.$table->getId().'/'. $column_0->getId().'/'.$file->getFilekey()));
		// 		$this->assertIdentical(false, $file->hasThumbnail());
		// 		$this->assertIdentical(null, $file->getThumbnailUrl());
	}

	public function testInsertRows_画像()
	{
		$by = $this->target->getUser(1001);
		$table  = $this->mockUpTable();
		$column_0 = $table->addColumn(Column::FILE);
		$column_0->setThumbnailMaxSize(210);
		$table->create($by);

		$file = File::create(dirname(__FILE__).'/icon.png', 'アイコン.png', 'image/png');
		$this->assertIdentical('アイコン.png', $file->getFileName());
		$this->assertPattern('/icon.png$/', $file->getFilekey());
		// 		$this->assertPattern(sprintf('#^/asset/files/%d/%d/.*.png$#', $table->getId(), $column_0->getId()), $file->getUrl());
		// 		$this->assertTrue(file_exists(dirname(__FILE__) .'/asset/files/'.$table->getId().'/'. $column_0->getId().'/'.$file->getFilekey()));
		// 		$this->assertIdentical(true, $file->hasThumbnail());
		// 		$this->assertIdentical('/asset/thumb/'.$table->getId().'/'. $column_0->getId().'/'.$file->getFilekey().'?t210', $file->getThumbnailUrl());
	}

}
