<?php
namespace TxCore\Test;

use TxCore\Row;
use TxCore\Comment;
use TxCore\TxCoreException;
use TxCore\Column;
use TxCore\Column__Datecalc;
use TxCore\Index;
use TxCore\Option;
use TxCore\User;
use TxCore\File;
use TxCore\LookupValue;
use TxCore\NotificationRule;
use TxCore\NotificationConditionalRule;
use TxCore\NotificationBasicRule;
use TxCore\NotificationCriteria;
use TxCore\Criteria;
use TxCore\Condition;
use TxCore\Table;
use TxCore\Math\MathContext;

class Engine_notificationTest extends BaseTestCase
{
    public function setUp()
    {
        parent::setUpDB(__DIR__.'/EngineTest.sql');
    }

    public function testInsertRow_正常()
    {
        // setting
        $by = $this->target->getUser(1001);
        $recipient = $this->target->getUser(1002);

        /* @var $table \TxCore\Table */
        $table = $this->mockUpTable('Notification');
        $column6 = $table->addColumn(Column::RADIO);
        $column6->setLabel("Label radio");
        $option6_1 = new Option();
        $option6_1->setName("New Radio 1");
        $column6->_addOption($option6_1);
        $option6_2 = new Option();
        $option6_2->setName("New Radio 2");
        $column6->_addOption($option6_2);
        $option6_3 = new Option();
        $option6_3->setName("New Radio 3");
        $column6->_addOption($option6_3);

        $rules = array();
        $criteria = new NotificationCriteria();
        $criteria->addCondition(
            $column6, NotificationCriteria::CONTAINS, array($option6_1)
        );
        $rule1 = new NotificationConditionalRule();
        $rule1->_setParent($table);
        $rule1->setTargetingType(NotificationRule::TARGETING_USERGROUP);
        $rule1->_setTargets(array($recipient));
        $rule1->setCriteria($criteria);
        $rule1->setMessage('New Radio 1 で登録されました。');
        $rules[] = $rule1;
        $table->setNotificationConditionalRules($rules);
        $table->create($by);

        $counts1 = $this->countInsertedNotification(
            $table, $by, $recipient);

        // data operation
        $row = new Row($table);
        $row->setValue($column6, $option6_1);
        $table->insertRows($by, true);
        $row = $table->getRowFor($row->getId());

        $counts2 = $this->countInsertedNotification(
            $table, $by, $recipient, $row);

        // assertion
        $notificationsIncrement =
            $counts2['notifications']['all']
            - $counts1['notifications']['all'];
        $notificationsIncrementPerObject =
            $counts2['notifications']['per_object']
            - $counts1['notifications']['per_object'];
        $receiptsIncrement =
            $counts2['receipts']['all']
            - $counts1['receipts']['all'];
        $receiptsIncrementPerRecipient =
            $counts2['receipts']['per_recipient']
            - $counts1['receipts']['per_recipient'];

        $this->assertSame(1, $notificationsIncrement);
        $this->assertSame(1, $notificationsIncrementPerObject);
        $this->assertSame(1, $receiptsIncrement);
        $this->assertSame(1, $receiptsIncrementPerRecipient);

        $list = $this->target->searchNotificationReceipts($recipient);
        $this->assertIdentical(1, $list->getTotalRowCount());

        $receipts = $list->getRows();
        $notification = $receipts[0]->getNotification();
        $this->assertIdentical(
            'New Radio 1 で登録されました。', $notification->getMessage());


        $table->drop($by);
    }

    private function updateTableNotificationRules($trunk, $by,
        $basicRules = null, $conditionalRules = null)
    {
        if ($basicRules !== null) {
            $trunk->setNotificationBasicRules($basicRules);
        }
        if ($conditionalRules !== null) {
            $trunk->setNotificationConditionalRules($conditionalRules);
        }
        $trunk->saveChanges($by);
        $trunk->release($by);
        $release = $this->target->getTable($trunk->getId());
        return $release;
    }

    private function buildBasicRule($table, $targetingType, $targets,
        $isOnRowInsert, $isOnRowUpdate, $isOnCommentInsert)
    {
        $rule = new NotificationBasicRule();
        $rule->_setParent($table);
        $rule->setTargetingType($targetingType);
        $rule->_setTargets($targets);
        $rule->setOnRowInsert($isOnRowInsert);
        $rule->setOnRowUpdate($isOnRowUpdate);
        $rule->setOnCommentInsert($isOnCommentInsert);
        return $rule;
    }

    private function buildConditionalRule($table, $targetingType, $targets,
        $conditions, $boolOperator = Criteria::AND_, $message = 'messsage')
    {
        $rule = new NotificationConditionalRule();
        $rule->_setParent($table);
        $rule->setTargetingType($targetingType);
        $rule->_setTargets($targets);
        $rule->setMessage($message);
        $criteria = new NotificationCriteria();
        $criteria->setBoolOperator($boolOperator);
        foreach ($conditions as $condition) {
            $column = $condition[0];
            $operator = $condition[1];
            $value = $condition[2];
            $criteria->addCondition($column, $operator, $value);
        }
        $rule->setCriteria($criteria);
        $errors = $rule->validate();
        if (is_not_empty($errors)) {
            throw new TxCoreException('rule has any error : '. $errors[0]->getMessage(),
                TxCoreException::VALIDATION_FAILED, $errors);

        }
        return $rule;
    }

    private function setSingleConditionalRule(
        $tableId, $by, $recipient, $column, $operator, $operand)
    {
        $targetingType = NotificationConditionalRule::TARGETING_USERGROUP;
        $trunk = $this->target->getTable($tableId, Table::TRUNK);

        $rule = $this->buildConditionalRule($trunk, $targetingType,
            array($recipient),
            array(array($column, $operator, $operand)));
        $release = $this->updateTableNotificationRules(
            $trunk, $by, null, array($rule));
        return $release;
    }

    private function setSingleConditionalRuleThenInsertRow(
        $tableId, $by, $recipient, $column, $operator, $operand,
        $insertValue)
    {
        if ($insertValue === false) {
            $insertData = array();
        } else {
            $insertData = array(array($column, $insertValue));
        }

        try {
            $release = $this->setSingleConditionalRule(
                $tableId, $by, $recipient, $column, $operator, $operand);
            $count1 = $this->countRecords('notification_receipt');
            $row = $this->insertRow($release, $by, $insertData);
            $count2 = $this->countRecords('notification_receipt');
        } catch (TxCoreException $e) {
            return false;
        }
        return $count2 - $count1;
    }

    private function setSingleCalcConditionalRuleThenInsertRow(
        $tableId, $by, $recipient, $column, $operator, $operand,
        $dependents)
    {
        $insertData = array();
        foreach ($dependents as $dependent) {
            $insertColumn = $dependent[0];
            $insertValue = $dependent[1];
            $insertData[] = array($insertColumn, $insertValue);
        }

        try {
            $release = $this->setSingleConditionalRule(
                $tableId, $by, $recipient, $column, $operator, $operand);
            $count1 = $this->countRecords('notification_receipt');
            $row = $this->insertRow($release, $by, $insertData);
            $count2 = $this->countRecords('notification_receipt');
        } catch (TxCoreException $e) {
            log_debug(format_exception($e));
            return false;
        }
        return $count2 - $count1;
    }

    private function setSingleConditionalRuleThenUpdateRow(
        $tableId, $by, $recipient, $column, $operator, $operand,
        $insertValue, $updateValue)
    {
        $targetingType = NotificationConditionalRule::TARGETING_USERGROUP;
        $trunk = $this->target->getTable($tableId, Table::TRUNK);
        $rule = $this->buildConditionalRule($trunk, $targetingType,
            array($recipient),
            array(array($column, $operator, $operand)));
        $release = $this->updateTableNotificationRules(
            $trunk, $by, null, array($rule));
        $row = $this->insertRow($release, $by,
            array(array($column, $insertValue)));
        $count1 = $this->countRecords('notification_receipt');
        $row = $this->updateRow($release, $by,
            $row->getId(), array(array($column, $updateValue)));
        $count2 = $this->countRecords('notification_receipt');
        return $count2 - $count1;
    }


    private function countInsertedNotification(
        Table $table, User $by, User $recipient,
        Row $row = null, Comment $comment = null)
    {
        $notifications = array();
        $receipts = array();

        $notifications['all'] = $this->countRecords('notification');
        $notifications['per_object'] = $this->countRecords(
            'notification',
            array(
                'notifier_id'  => $by->getId(),
                'table_id'     => $table->getId(),
                'row_id'       => $row ? $row->getId() : null,
                'comment_id'   => $comment ? $comment->getId() : null,
            ));
        $receipts['all'] = $this->countRecords('notification_receipt');
        $receipts['per_recipient'] = $this->countRecords(
            'notification_receipt',
            array(
                'table_id'     => $table->getId(),
                'recipient_id' => $recipient->getId(),
                'is_read'      => 0,
                'table_id'     => $table->getId(),
                'row_id'       => $row ? $row->getId() : null,
                'comment_id'   => $comment ? $comment->getId() : null,
            ));

        return array(
            'notifications' => $notifications,
            'receipts' => $receipts,
        );
    }

    private function insertRow(Table $table, User $by,
        $valueArray = array())
    {
        $row = $table->addRow();
        foreach ($valueArray as $arr) {
            $column = $arr[0];
            $value = $arr[1];
            $row->setValue($column, $value);
        }
        $table->insertRows($by, true);
        return $row;
    }

    private function updateRow(Table $table, User $by, $rowId,
        $valueArray = array())
    {
        $row = $table->getRowFor($rowId);
        foreach ($valueArray as $arr) {
            $column = $arr[0];
            $value = $arr[1];
            $row->setValue($column, $value);
        }
        $row->update($by, true);
        return $row;
    }


    public function testDropTable()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $recipients = array($recipient);
        $targetingType = NotificationConditionalRule::TARGETING_USERGROUP;

        // 1 table, 2 notification
        $table1 = $this->mockUpTable();
        $table1->setPublic(true);
        $column = $table1->addColumn(Column::TEXT);
        $table1->create($by);
        $table1Id = $table1->getId();
        $trunk = $this->target->getTable($table1Id, Table::TRUNK);
        $rule = $this->buildConditionalRule($trunk, $targetingType,
            $recipients,
            array(array($column, Criteria::EQUALS, 'abcde')));
        $release = $this->updateTableNotificationRules(
            $trunk, $by, null, array($rule));
        $row1 = $this->insertRow($release, $by,
            array(array($column, 'abcde')));
        $row2 = $this->insertRow($release, $by,
            array(array($column, 'abcde')));

        // 1 table, 2 notification
        $table2 = $this->mockUpTable();
        $table2->setPublic(true);
        $column = $table2->addColumn(Column::TEXT);
        $table2->create($by);
        $table2Id = $table2->getId();
        $trunk = $this->target->getTable($table2Id, Table::TRUNK);
        $rule = $this->buildConditionalRule($trunk, $targetingType,
            $recipients,
            array(array($column, Criteria::EQUALS, 'abcde')));
        $release = $this->updateTableNotificationRules(
            $trunk, $by, null, array($rule));
        $row1 = $this->insertRow($release, $by,
            array(array($column, 'abcde')));
        $row2 = $this->insertRow($release, $by,
            array(array($column, 'abcde')));

        $this->assertEquals(4, $this->countRecords('notification'));
        $this->assertEquals(2, $this->countRecords('notification',
            array('table_id'=>$table1Id)));
        $this->assertEquals(2, $this->countRecords('notification',
            array('table_id'=>$table2Id)));
        $this->assertEquals(4, $this->countRecords('notification_receipt'));
        $this->assertEquals(2, $this->countRecords('notification_receipt',
            array('table_id'=>$table1Id)));
        $this->assertEquals(2, $this->countRecords('notification_receipt',
            array('table_id'=>$table2Id)));

        $this->target->getTable($table1Id)->drop($by);
        $this->assertEquals(0, $this->countRecords('notification',
            array('table_id'=>$table1Id)));
        $this->assertEquals(2, $this->countRecords('notification',
            array('table_id'=>$table2Id)));
        $this->assertEquals(0, $this->countRecords('notification_receipt',
            array('table_id'=>$table1Id)));
        $this->assertEquals(2, $this->countRecords('notification_receipt',
            array('table_id'=>$table2Id)));

        $this->target->getTable($table2Id)->drop($by);
        $this->assertEquals(0, $this->countRecords('notification'));
        $this->assertEquals(0, $this->countRecords('notification_receipt'));

    }

    public function testDeleteRow()
    {
        $by = $this->target->getUser(1002);
        $recipient1 = $this->target->getUser(1001);
        $recipient2 = $this->target->getUser(1003);
        $recipients = array($recipient1, $recipient2);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column1 = $table->addColumn(Column::TEXT);
        $table->create($by);

        $tableId = $table->getId();
        $targetingType = NotificationConditionalRule::TARGETING_USERGROUP;
        $trunk = $this->target->getTable($tableId, Table::TRUNK);
        $rule = $this->buildConditionalRule($trunk, $targetingType,
            $recipients,
            array(array($column1, Criteria::EQUALS, 'abcde')));
        $release = $this->updateTableNotificationRules(
            $trunk, $by, null, array($rule));

        $row1 = $this->insertRow($release, $by,
            array(array($column1, 'abcde')));
        $row2 = $this->insertRow($release, $by,
            array(array($column1, 'abcde')));

        $this->assertEquals(2, $this->countRecords('notification'));
        $this->assertEquals(4, $this->countRecords('notification_receipt'));
        $this->assertEquals(1, $this->countRecords('notification',
            array('row_id' => $row1->getId())));
        $this->assertEquals(1, $this->countRecords('notification',
            array('row_id' => $row2->getId())));
        $this->assertEquals(2, $this->countRecords('notification_receipt',
            array('row_id' => $row1->getId())));
        $this->assertEquals(2, $this->countRecords('notification_receipt',
            array('row_id' => $row2->getId())));

        $row1->delete($by);
        $this->assertEquals(0, $this->countRecords('notification',
            array('row_id' => $row1->getId())));
        $this->assertEquals(1, $this->countRecords('notification',
            array('row_id' => $row2->getId())));
        $this->assertEquals(0, $this->countRecords('notification_receipt',
            array('row_id' => $row1->getId())));
        $this->assertEquals(2, $this->countRecords('notification_receipt',
            array('row_id' => $row2->getId())));

        $row2->delete($by);
        $this->assertEquals(0, $this->countRecords('notification',
            array('row_id' => $row1->getId())));
        $this->assertEquals(0, $this->countRecords('notification',
            array('row_id' => $row2->getId())));
        $this->assertEquals(0, $this->countRecords('notification_receipt',
            array('row_id' => $row1->getId())));
        $this->assertEquals(0, $this->countRecords('notification_receipt',
            array('row_id' => $row2->getId())));

        $release->drop($by);


    }

    public function testDeleteComment()
    {
        $by = $this->target->getUser(1002);
        $recipient1 = $this->target->getUser(1001);
        $recipient2 = $this->target->getUser(1003);
        $recipients = array($recipient1, $recipient2);
        $table = $this->mockUpTable();
        $table->setCommentEnabled(true);
        $table->setPublic(true);
        $table->create($by);

        $tableId = $table->getId();
        $release = $this->target->getTable($tableId);
        $row = $release->addRow();
        $release->insertRows($by, true);

        $comment1 = new Comment();
        $comment1->setText('comment1');
        foreach ($recipients as $recipient) {
            $comment1->addNotificationTarget($recipient);
        }
        $row->createComment($comment1, $by);
        $comment2 = new Comment();
        $comment2->setText('comment2');
        foreach ($recipients as $recipient) {
            $comment2->addNotificationTarget($recipient);
        }
        $row->createComment($comment2, $by);
        $this->assertEquals(2, $this->countRecords('notification'));
        $this->assertEquals(4, $this->countRecords('notification_receipt'));
        $this->assertEquals(1, $this->countRecords('notification',
            array('comment_id' => $comment1->getId())));
        $this->assertEquals(1, $this->countRecords('notification',
            array('comment_id' => $comment2->getId())));
        $this->assertEquals(2, $this->countRecords('notification_receipt',
            array('comment_id' => $comment1->getId())));
        $this->assertEquals(2, $this->countRecords('notification_receipt',
            array('comment_id' => $comment2->getId())));

        $row->deleteComment($comment1, $by);
        $this->assertEquals(0, $this->countRecords('notification',
            array('comment_id' => $comment1->getId())));
        $this->assertEquals(1, $this->countRecords('notification',
            array('comment_id' => $comment2->getId())));
        $this->assertEquals(0, $this->countRecords('notification_receipt',
            array('comment_id' => $comment1->getId())));
        $this->assertEquals(2, $this->countRecords('notification_receipt',
            array('comment_id' => $comment2->getId())));

        $row->deleteComment($comment2, $by);
        $this->assertEquals(0, $this->countRecords('notification',
            array('comment_id' => $comment1->getId())));
        $this->assertEquals(0, $this->countRecords('notification',
            array('comment_id' => $comment2->getId())));
        $this->assertEquals(0, $this->countRecords('notification_receipt',
            array('comment_id' => $comment1->getId())));
        $this->assertEquals(0, $this->countRecords('notification_receipt',
            array('comment_id' => $comment2->getId())));

        $release->drop($by);
    }

    public function testDeleteUser()
    {
        $by = $this->target->getUser(1002);
        $recipient1 = $this->createUser(2001);
        $recipient2 = $this->createUser(2003);
        $recipients = array($recipient1, $recipient2);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $table->create($by);
        $tableId = $table->getId();
        $targetingType = NotificationConditionalRule::TARGETING_USERGROUP;

        $trunk = $this->target->getTable($tableId, Table::TRUNK);
        $rule = $this->buildBasicRule($trunk, $targetingType,
            $recipients, true, false, false);
        $release = $this->updateTableNotificationRules(
            $trunk, $by, array($rule));
        $this->insertRow($release, $by);
        $this->insertRow($release, $by);

        $this->assertEquals(2, $this->countRecords('notification'));
        $this->assertEquals(4, $this->countRecords('notification_receipt'));
        $this->assertEquals(2, $this->countRecords('notification_receipt',
            array('recipient_id' => $recipient1->getId())));
        $this->assertEquals(2, $this->countRecords('notification_receipt',
            array('recipient_id' => $recipient2->getId())));

        $this->target->deleteUser($recipient1, $by);
        $this->assertEquals(2, $this->countRecords('notification'));
        $this->assertEquals(2, $this->countRecords('notification_receipt'));
        $this->assertEquals(0, $this->countRecords('notification_receipt',
            array('recipient_id' => $recipient1->getId())));
        $this->assertEquals(2, $this->countRecords('notification_receipt',
            array('recipient_id' => $recipient2->getId())));

        $this->target->deleteUser($recipient2, $by);
        $this->assertEquals(2, $this->countRecords('notification'));
        $this->assertEquals(0, $this->countRecords('notification_receipt'));
        $this->assertEquals(0, $this->countRecords('notification_receipt',
            array('recipient_id' => $recipient1->getId())));
        $this->assertEquals(0, $this->countRecords('notification_receipt',
            array('recipient_id' => $recipient2->getId())));

        $release->drop($by);
    }

    public function testUpdateRow_条件指定_ブール演算子()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column1 = $table->addColumn(Column::TEXT);
        $column2 = $table->addColumn(Column::TEXT);
        $table->create($by);
        $tableId = $table->getId();
        $targetingType = NotificationConditionalRule::TARGETING_USERGROUP;

        // AND connection, all match
        $trunk = $this->target->getTable($tableId, Table::TRUNK);
        $rule = $this->buildConditionalRule($trunk, $targetingType,
            array($recipient),
            array(
                array($column1, Criteria::EQUALS, 'abcde'),
                array($column2, Criteria::LIKE, 'bce')
            ));
        $release = $this->updateTableNotificationRules(
            $trunk, $by, null, array($rule));
        $row = $this->insertRow($release, $by,
            array(
                array($column1, 'abc'),
                array($column2, 'ab')
            ));
        $count1 = $this->countRecords('notification');
        $row = $this->updateRow($release, $by,
            $row->getId(), array(
                array($column1, 'abcde'),
                array($column2, 'abce')
            ));
        $count2 = $this->countRecords('notification');
        $this->assertSame(1, $count2 - $count1);

        // AND connection, partial match -> all match
        $trunk = $this->target->getTable($tableId, Table::TRUNK);
        $rule = $this->buildConditionalRule($trunk, $targetingType,
            array($recipient),
            array(
                array($column1, Criteria::EQUALS, 'abcde'),
                array($column2, Criteria::LIKE, 'bcd')
            ));
        $release = $this->updateTableNotificationRules(
            $trunk, $by, null, array($rule));
        $row = $this->insertRow($release, $by,
            array(
                array($column1, 'abcde'), // single condition already matches,
                array($column2, 'ab') // but all not.
            ));
        $count1 = $this->countRecords('notification');
        $row = $this->updateRow($release, $by,
            $row->getId(), array(
                array($column1, 'abcde'),
                array($column2, 'abcd')
            ));
        $count2 = $this->countRecords('notification');
        $this->assertSame(1, $count2 - $count1);

        // AND connection, no match -> partial match
        $trunk = $this->target->getTable($tableId, Table::TRUNK);
        $rule = $this->buildConditionalRule($trunk, $targetingType,
            array($recipient),
            array(
                array($column1, Criteria::EQUALS, 'abcde'),
                array($column2, Criteria::LIKE, 'bcd')
            ));
        $release = $this->updateTableNotificationRules(
            $trunk, $by, null, array($rule));
        $row = $this->insertRow($release, $by,
            array(
                array($column1, 'abcd'),
                array($column2, 'ab')
            ));
        $count1 = $this->countRecords('notification');
        $row = $this->updateRow($release, $by,
            $row->getId(), array(
                array($column1, 'abc'),
                array($column2, 'abcd')
            ));
        $count2 = $this->countRecords('notification');
        $this->assertSame(0, $count2 - $count1);

        // AND connection, no match
        $trunk = $this->target->getTable($tableId, Table::TRUNK);
        $rule = $this->buildConditionalRule($trunk, $targetingType,
            array($recipient),
            array(
                array($column1, Criteria::EQUALS, 'abcde'),
                array($column2, Criteria::LIKE, 'bcd')
            ));
        $release = $this->updateTableNotificationRules(
            $trunk, $by, null, array($rule));
        $row = $this->insertRow($release, $by,
            array(
                array($column1, 'abcd'),
                array($column2, 'ab')
            ));
        $count1 = $this->countRecords('notification');
        $row = $this->updateRow($release, $by,
            $row->getId(), array(
                array($column1, 'abc'),
                array($column2, 'abc')
            ));
        $count2 = $this->countRecords('notification');
        $this->assertSame(0, $count2 - $count1);

        // OR connection, all match
        $trunk = $this->target->getTable($tableId, Table::TRUNK);
        $rule = $this->buildConditionalRule($trunk, $targetingType,
            array($recipient),
            array(
                array($column1, Criteria::EQUALS, 'abcde'),
                array($column2, Criteria::LIKE, 'bce')
            ), Criteria::OR_);
        $release = $this->updateTableNotificationRules(
            $trunk, $by, null, array($rule));
        $row = $this->insertRow($release, $by,
            array(
                array($column1, 'abc'),
                array($column2, 'ab')
            ));
        $count1 = $this->countRecords('notification');
        $row = $this->updateRow($release, $by,
            $row->getId(), array(
                array($column1, 'abcde'),
                array($column2, 'abce')
            ));
        $count2 = $this->countRecords('notification');
        $this->assertSame(1, $count2 - $count1);

        // OR connection, no match -> first match
        $trunk = $this->target->getTable($tableId, Table::TRUNK);
        $rule = $this->buildConditionalRule($trunk, $targetingType,
            array($recipient),
            array(
                array($column1, Criteria::EQUALS, 'abcde'),
                array($column2, Criteria::LIKE, 'bcd')
            ), Criteria::OR_);
        $release = $this->updateTableNotificationRules(
            $trunk, $by, null, array($rule));
        $row = $this->insertRow($release, $by,
            array(
                array($column1, 'abcd'),
                array($column2, 'ab')
            ));
        $count1 = $this->countRecords('notification');
        $row = $this->updateRow($release, $by,
            $row->getId(), array(
                array($column1, 'abcde'),
                array($column2, 'a')
            ));
        $count2 = $this->countRecords('notification');
        $this->assertSame(1, $count2 - $count1);

        // OR connection, no match -> last match
        $trunk = $this->target->getTable($tableId, Table::TRUNK);
        $rule = $this->buildConditionalRule($trunk, $targetingType,
            array($recipient),
            array(
                array($column1, Criteria::EQUALS, 'abcde'),
                array($column2, Criteria::LIKE, 'bcd')
            ), Criteria::OR_);
        $release = $this->updateTableNotificationRules(
            $trunk, $by, null, array($rule));
        $row = $this->insertRow($release, $by,
            array(
                array($column1, 'abcd'),
                array($column2, 'ab')
            ));
        $count1 = $this->countRecords('notification');
        $row = $this->updateRow($release, $by,
            $row->getId(), array(
                array($column1, 'abc'),
                array($column2, 'abcd')
            ));
        $count2 = $this->countRecords('notification');
        $this->assertSame(1, $count2 - $count1);

        // OR connection, no match
        $trunk = $this->target->getTable($tableId, Table::TRUNK);
        $rule = $this->buildConditionalRule($trunk, $targetingType,
            array($recipient),
            array(
                array($column1, Criteria::EQUALS, 'abcde'),
                array($column2, Criteria::LIKE, 'bcd')
            ), Criteria::OR_);
        $release = $this->updateTableNotificationRules(
            $trunk, $by, null, array($rule));
        $row = $this->insertRow($release, $by,
            array(
                array($column1, 'abcd'),
                array($column2, 'ab')
            ));
        $count1 = $this->countRecords('notification');
        $row = $this->updateRow($release, $by,
            $row->getId(), array(
                array($column1, 'abc'),
                array($column2, 'abc')
            ));
        $count2 = $this->countRecords('notification');
        $this->assertSame(0, $count2 - $count1);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__TEXT()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::TEXT);
        $table->create($by);
        $tableId = $table->getId();
        // EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', 'abc');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'Abc', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'あいう', 'あいう');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, null);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, '');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '', null);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', 'ab');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, 'ab');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'ab', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '', 'ab');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'ab', '');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'ab', false);
        $this->assertSame(0, $result);
        // NOT_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'abc', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, null, null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, null, '');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'abc', 'ab');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, null, 'ab');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'ab', null);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '', 'ab');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'ab', '');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'ab', false);
        $this->assertSame(1, $result);
        // LIKE
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'abc', 'abc');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'Abc', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', 'abc');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'あい', 'あいう');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'bc', 'abc');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'b', 'abc');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'd', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, '', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, null, 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'd', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'd', false);
        $this->assertSame(0, $result);
        // NOT_LIKE
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'abc', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'ab', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'bc', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'b', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'd', 'abc');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, '', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, null, 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'd', false);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, '', false);
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'abc', null);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, null, false);
        $this->assertSame(false, $result);
        // not supported
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, 'abc', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, 'abc', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, 'abc', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, 'abc', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array('abc'), 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array('abc'), 'abc');
        $this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testUpdateRow_条件指定__TEXT()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::TEXT);
        $table->create($by);
        $tableId = $table->getId();
        // EQUALS
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', 'ab', 'abc');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', null, 'abc');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', '', 'abc');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', 'abc', 'ab');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', 'bc', 'ab');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', 'abc', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', null, null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', '', '');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', '', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', null, '');
        $this->assertSame(0, $result);
        // NOT_EQUALS
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'abc', 'abc', 'ab');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'abc', 'ab', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'abc', null, 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'abc', null, 'ab');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'abc', '', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'abc', 'bc', 'ab');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'abc', 'abc', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'abc', null, null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'abc', '', '');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'abc', '', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'abc', null, '');
        $this->assertSame(0, $result);
        // LIKE
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', 'd', 'abc');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'bc', 'd', 'abc');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'abc', 'd', 'abc');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'b', '', 'abc');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', null, 'abc');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'abcd', 'd', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', 'abc', 'ab');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', 'd', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', 'd', '');
        $this->assertSame(0, $result);
        // NOT_LIKE
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'ab', 'd', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'ab', 'ab', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'b', 'abc', 'd');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'b', null, 'd');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'b', '', 'd');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenUpdateRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'b', 'd', 'd');
        $this->assertSame(0, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__MULTITEXT()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::MULTITEXT);
        $table->create($by);
        $tableId = $table->getId();

        $sample1 = "Ａあ漢エ９\n2行目";

        // EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, "Ａあ漢エ９\n2行目", $sample1);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, null);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, "");
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, "", null);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, "Ａあ漢エ９\n2行", $sample1);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, "Ａあ漢エ９2行目", $sample1);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, "a", "");
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, "a", null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, "", "a");
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, "a");
        $this->assertSame(0, $result);
        // NOT_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, "Ａあ漢エ９\n2行", $sample1);
        $this->assertSame(1, $result);
        // LIKE
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "Ａあ漢エ９", $sample1);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "Ａあ漢エ９", null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "Ａあ漢エ９", "");
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "Aあ漢エ９", $sample1);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "Ａあ漢エ９2", $sample1);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "2", $sample1);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "Ａあ漢エ", $sample1);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "あ漢エ９", $sample1);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "あ漢エ", $sample1);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "", $sample1);
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, null, $sample1);
        $this->assertSame(false, $result);
        // NOT_LIKE
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, "Aあ漢エ９", $sample1);
        $this->assertSame(1, $result);

        // not supported
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::EQUALS, 'abc', 'abc');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::NOT_EQUALS, 'ab', 'abc');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::LIKE 'ab', 'abc');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::NOT_LIKE 'abcd', 'abc');
        //$this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '99', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '101', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '-100', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '100', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array('1'), '1');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array('1'), '0');
        $this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__RICHTEXT()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::RICHTEXT);
        $table->create($by);
        $tableId = $table->getId();

        $sample1 = "<p><a href=\"http://example.com\">Ａ</a>あ<br/>漢エ９\n2行目</p>";

        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "Ａあ漢エ９", $sample1);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "Ａあ漢エ９", null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "Ａあ漢エ９", "");
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "Aあ漢エ９", $sample1);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "Ａあ漢エ９2", $sample1);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "2", $sample1);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "Ａあ漢エ", $sample1);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "あ漢エ９", $sample1);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "あ漢エ", $sample1);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "", $sample1);
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, null, $sample1);
        $this->assertSame(false, $result);
        // NOT_LIKE
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, "Aあ漢エ９", $sample1);
        $this->assertSame(1, $result);

        // not supported
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'ab', 'abc');
        $this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::LIKE 'ab', 'abc');
        $this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::NOT_LIKE 'abcd', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '99', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '101', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '-100', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '100', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array('1'), '1');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array('1'), '0');
        $this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__RICHTEXT_htmlタグを無視してマッチする()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::RICHTEXT);
        $table->create($by);
        $tableId = $table->getId();

        $sample1 = "<p><a href=\"http://example.com\">Ａ</a>あ<br/>漢エ９\n2行目</p>";

        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "Ａあ", $sample1);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "<a href=\"http://example.com\">", $sample1);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "http://example.com", $sample1);
        $this->assertSame(0, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__NUMBER()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $table->setRoundScale(4);
        $table->setRoundType(MathContext::ROUNDHALF_TOEVEN);
        $column = $table->addColumn(Column::NUMBER);
        $table->create($by);
        $tableId = $table->getId();

        // EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.456', '123.456');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.456', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, '123.456');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, null);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.457', '123.456');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.4561', '123.456');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123', '123.456');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '124', '123.456');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.456', '123.4560');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.456', '123.45605');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.456', '123.456051');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.456', '123.456049');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.456', '123.4561');
        $this->assertSame(0, $result);
        // NOT_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '123.456', '123.4561');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '123.456', '123.456');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '123.456', null);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, null, '123.456');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, null, null);
        $this->assertSame(0, $result);
        // GREATER_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '123.456', '123.45606');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '123.456', '123.456');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '123.456', '123.45595');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '123.456', '123.45594');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '123.456', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, null, '123.456');
        $this->assertSame(false, $result);
        // LESS_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '123.456', '123.45606');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '123.456', '123.456');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '123.456', '123.45595');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '123.456', '123.45594');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '123.456', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, null, '123.456');
        $this->assertSame(false, $result);

        // not supported
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::EQUALS, 'abc', 'abc');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::NOT_EQUALS, 'ab', 'abc');
        //$this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'abcd', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '99', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '101', '100');
        $this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::GREATER_EQUALS, '-100', '100');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::LESS_EQUALS, '100', '100');
        //$this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array('1'), '1');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array('1'), '0');
        $this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__NUMBER_条件値はまるめない()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $table->setRoundScale(4);
        $table->setRoundType(MathContext::ROUNDHALF_TOEVEN);
        $column = $table->addColumn(Column::NUMBER);
        $table->create($by);
        $tableId = $table->getId();

        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.4561', '123.456051');
        $this->assertSame(1, $result);

        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.456051', '123.456051');
        $this->assertSame(0, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__CALC()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $table->setRoundScale(4);
        $table->setRoundType(MathContext::ROUNDHALF_TOEVEN);
        $column_ = $table->addColumn(Column::NUMBER);
        $column_->setCode('数値');
        $column = $table->addColumn(Column::CALC);
        $column->setExpression('数値*2');
        $table->create($by);
        $tableId = $table->getId();

        // EQUALS
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.456',
            array(array($column_, '61.728')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.456',
            array(array($column_, null)));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null,
            array(array($column_, '61.728')));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null,
            array(array($column_, null)));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.457',
            array(array($column_, '61.728')));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.4561',
            array(array($column_, '61.728')));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123',
            array(array($column_, '61.728')));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '124',
            array(array($column_, '61.728')));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.456',
            array(array($column_, '61.7280')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.456',
            array(array($column_, '61.72805')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.456',
            array(array($column_, '61.728051')));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.456',
            array(array($column_, '61.728049')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.456',
            array(array($column_, '61.7281')));
        $this->assertSame(0, $result);
        // NOT_EQUALS
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '123.456',
            array(array($column_, '61.7281')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '123.456',
            array(array($column_, '61.728')));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '123.456',
            array(array($column_, null)));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, null,
            array(array($column_, '61.728')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, null,
            array(array($column_, null)));
        $this->assertSame(0, $result);
        // GREATER_EQUALS
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '123.456',
            array(array($column_, '61.72806')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '123.456',
            array(array($column_, '61.728')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '123.456',
            array(array($column_, '61.72795')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '123.456',
            array(array($column_, '61.72794')));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '123.456',
            array(array($column_, null)));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, null,
            array(array($column_, '61.728')));
        $this->assertSame(false, $result);
        // LESS_EQUALS
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '123.456',
            array(array($column_, '61.72806')));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '123.456',
            array(array($column_, '61.728')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '123.456',
            array(array($column_, '61.72795')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '123.456',
            array(array($column_, '61.72794')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '123.456',
            array(array($column_, null)));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, null,
            array(array($column_, '61.728')));
        $this->assertSame(false, $result);

        // not supported
        //$result = $this->setSingleCalcConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::EQUALS, 'abc',
        //    array(array($column_, 'abc')));
        //$this->assertSame(false, $result);
        //$result = $this->setSingleCalcConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::NOT_EQUALS, 'ab',
        //    array(array($column_, 'abc')));
        //$this->assertSame(false, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab',
            array(array($column_, 'abc')));
        $this->assertSame(false, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'abcd',
            array(array($column_, 'abc')));
        $this->assertSame(false, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '99',
            array(array($column_, '100')));
        $this->assertSame(false, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '101',
            array(array($column_, '100')));
        $this->assertSame(false, $result);
        //$result = $this->setSingleCalcConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::GREATER_EQUALS, '-100',
        //    array(array($column_, '100')));
        //$this->assertSame(false, $result);
        //$result = $this->setSingleCalcConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::LESS_EQUALS, '100',
        //    array(array($column_, '100')));
        //$this->assertSame(false, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array('1'),
            array(array($column_, '1')));
        $this->assertSame(false, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array('1'),
            array(array($column_, '0')));
        $this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__RADIO()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::RADIO);
        $column->setLabel("Label radio");
        $option_1 = new Option();
        $option_1->setName("New Radio 1");
        $column->_addOption($option_1);
        $option_2 = new Option();
        $option_2->setName("New Radio 2");
        $column->_addOption($option_2);
        $option_3 = new Option();
        $option_3->setName("New Radio 3");
        $column->_addOption($option_3);
        $table->create($by);
        $tableId = $table->getId();

        // CONTAINS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($option_1), $option_1);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($option_1, $option_2), $option_1);
        $this->assertSame(1, $result);
        // NOT_CONTAINS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($option_1), $option_1);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($option_1, $option_2), $option_3);
        $this->assertSame(1, $result);

        // not supported
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'ab', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'abcd', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '99', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '101', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '-100', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '100', '100');
        $this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::CONTAINS, array('1'), '1');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::NOT_CONTAINS, array('1'), '0');
        //$this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__RADIO_条件選択肢がなくなっていたら条件そのものを除外()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::RADIO);
        $column->setLabel("Label radio");
        $option_1 = new Option();
        $option_1->setName("New Radio 1");
        $column->_addOption($option_1);
        $option_2 = new Option();
        $option_2->setName("New Radio 2");
        $column->_addOption($option_2);
        $option_3 = new Option();
        $option_3->setName("New Radio 3");
        $column->_addOption($option_3);
        $table->create($by);
        $tableId = $table->getId();

        // CONTAINS
        $release = $this->setSingleConditionalRule(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($option_1));
        $trunk = $this->target->getTable($tableId, Table::TRUNK);
        $column = $trunk->getColumnFor($column->getId());
        $option = $column->getOptionFor($option_1->getId());
        $column->removeOption($option);
        $trunk->saveChanges($by);
        $trunk->release($by);
        $release = $this->target->getTable($trunk->getId());
        $count1 = $this->countRecords('notification_receipt');
        $row = $this->insertRow($release, $by,
            array(array($column, $option_2)));
        $count2 = $this->countRecords('notification_receipt');
        $this->assertSame(0, $count2 - $count1);
        // NOT_CONTAINS
        $release = $this->setSingleConditionalRule(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($option_2));
        $trunk = $this->target->getTable($tableId, Table::TRUNK);
        $column = $trunk->getColumnFor($column->getId());
        $option = $column->getOptionFor($option_2->getId());
        $column->removeOption($option);
        $trunk->saveChanges($by);
        $trunk->release($by);
        $release = $this->target->getTable($trunk->getId());
        $count1 = $this->countRecords('notification_receipt');
        $row = $this->insertRow($release, $by,
            array(array($column, $option_3)));
        $count2 = $this->countRecords('notification_receipt');
        $this->assertSame(0, $count2 - $count1);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__CHECKBOX()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::CHECKBOX);
        $column->setLabel("Label checkbox");
        $option_1 = new Option();
        $option_1->setName("New Checkbox 1");
        $column->_addOption($option_1);
        $option_2 = new Option();
        $option_2->setName("New Checkbox 2");
        $column->_addOption($option_2);
        $option_3 = new Option();
        $option_3->setName("New Checkbox 3");
        $column->_addOption($option_3);
        $option_4 = new Option();
        $option_4->setName("New Checkbox 4");
        $column->_addOption($option_4);
        $table->create($by);
        $tableId = $table->getId();

        // CONTAINS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($option_1), array($option_1));
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($option_1, $option_2), array($option_1));
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($option_1), array());
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($option_1, null), array());
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array(null), array());
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($option_1, $option_3), array($option_1, $option_2));
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($option_3, $option_4), array($option_1, $option_2));
        $this->assertSame(0, $result);
        // NOT_CONTAINS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($option_1), array($option_1));
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($option_1, $option_2), array($option_3));
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($option_1), array());
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array(null), array());
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($option_1, $option_3), array($option_1, $option_2));
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($option_3, $option_4), array($option_1, $option_2));
        $this->assertSame(1, $result);

        // not supported
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'ab', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'abcd', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '99', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '101', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '-100', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '100', '100');
        $this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::CONTAINS, array('1'), '1');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::NOT_CONTAINS, array('1'), '0');
        //$this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__CHECKBOX_条件選択肢がなくなっていたら条件そのものを除外()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::CHECKBOX);
        $column->setLabel("Label checkbox");
        $option_1 = new Option();
        $option_1->setName("New Checkbox 1");
        $column->_addOption($option_1);
        $option_2 = new Option();
        $option_2->setName("New Checkbox 2");
        $column->_addOption($option_2);
        $option_3 = new Option();
        $option_3->setName("New Checkbox 3");
        $column->_addOption($option_3);
        $option_4 = new Option();
        $option_4->setName("New Checkbox 4");
        $column->_addOption($option_4);
        $table->create($by);
        $tableId = $table->getId();

        // CONTAINS
        $release = $this->setSingleConditionalRule(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($option_1));
        $trunk = $this->target->getTable($tableId, Table::TRUNK);
        $column = $trunk->getColumnFor($column->getId());
        $option = $column->getOptionFor($option_1->getId());
        $column->removeOption($option);
        $trunk->saveChanges($by);
        $trunk->release($by);
        $release = $this->target->getTable($trunk->getId());
        $count1 = $this->countRecords('notification_receipt');
        $row = $this->insertRow($release, $by,
            array(array($column, array($option_2))));
        $count2 = $this->countRecords('notification_receipt');
        $this->assertSame(0, $count2 - $count1);
        // NOT_CONTAINS
        $release = $this->setSingleConditionalRule(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($option_2));
        $trunk = $this->target->getTable($tableId, Table::TRUNK);
        $column = $trunk->getColumnFor($column->getId());
        $option = $column->getOptionFor($option_2->getId());
        $column->removeOption($option);
        $trunk->saveChanges($by);
        $trunk->release($by);
        $release = $this->target->getTable($trunk->getId());
        $count1 = $this->countRecords('notification_receipt');
        $row = $this->insertRow($release, $by,
            array(array($column, array($option_3))));
        $count2 = $this->countRecords('notification_receipt');
        $this->assertSame(0, $count2 - $count1);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__DROPDOWN()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::DROPDOWN);
        $column->setLabel("Label dropdown");
        $option_1 = new Option();
        $option_1->setName("New Option 1");
        $column->_addOption($option_1);
        $option_2 = new Option();
        $option_2->setName("New Option 2");
        $column->_addOption($option_2);
        $option_3 = new Option();
        $option_3->setName("New Option 3");
        $column->_addOption($option_3);
        $table->create($by);
        $tableId = $table->getId();

        // CONTAINS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($option_1), $option_1);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($option_1, $option_2), $option_1);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($option_1), null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($option_1, null), null);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array(null), null);
        $this->assertSame(1, $result);
        // NOT_CONTAINS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($option_1), $option_1);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($option_1, $option_2), $option_3);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($option_1), null);
        $this->assertSame(1, $result);

        // not supported
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'ab', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'abcd', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '99', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '101', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '-100', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '100', '100');
        $this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::CONTAINS, array('1'), '1');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::NOT_CONTAINS, array('1'), '0');
        //$this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__DROPDOWN_条件選択肢がなくなっていたら条件そのものを除外()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::DROPDOWN);
        $column->setLabel("Label dropdown");
        $option_1 = new Option();
        $option_1->setName("New Option 1");
        $column->_addOption($option_1);
        $option_2 = new Option();
        $option_2->setName("New Option 2");
        $column->_addOption($option_2);
        $option_3 = new Option();
        $option_3->setName("New Option 3");
        $column->_addOption($option_3);
        $table->create($by);
        $tableId = $table->getId();

        // CONTAINS
        $release = $this->setSingleConditionalRule(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($option_1));
        $trunk = $this->target->getTable($tableId, Table::TRUNK);
        $column = $trunk->getColumnFor($column->getId());
        $option = $column->getOptionFor($option_1->getId());
        $column->removeOption($option);
        $trunk->saveChanges($by);
        $trunk->release($by);
        $release = $this->target->getTable($trunk->getId());
        $count1 = $this->countRecords('notification_receipt');
        $row = $this->insertRow($release, $by,
            array(array($column, null)));
        $count2 = $this->countRecords('notification_receipt');
        $this->assertSame(0, $count2 - $count1);
        // NOT_CONTAINS
        $release = $this->setSingleConditionalRule(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($option_2));
        $trunk = $this->target->getTable($tableId, Table::TRUNK);
        $column = $trunk->getColumnFor($column->getId());
        $option = $column->getOptionFor($option_2->getId());
        $column->removeOption($option);
        $trunk->saveChanges($by);
        $trunk->release($by);
        $release = $this->target->getTable($trunk->getId());
        $count1 = $this->countRecords('notification_receipt');
        $row = $this->insertRow($release, $by,
            array(array($column, null)));
        $count2 = $this->countRecords('notification_receipt');
        $this->assertSame(0, $count2 - $count1);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__DATE1()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::DATE);
        $table->create($by);
        $tableId = $table->getId();

        // EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '2014-12-31', '2014-12-31');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '2014-12-31', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, '2014-12-31');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, null);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '2014-12-30', '2014-12-31');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '2014-1-1', '2014-01-01');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '2014-01-01', '2014-1-1');
        $this->assertSame(1, $result);
        // NOT_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '2014-12-30', '2014-12-31');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '2014-12-31', '2014-12-31');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '2014-12-31', null);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, null, '2014-12-31');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, null, null);
        $this->assertSame(0, $result);
        // GREATER
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '2014-12-30', '2014-12-31');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '2014-12-31', '2014-12-31');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '2015-01-01', '2014-12-31');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '2014-12-31', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, null, '2014-12-31');
        $this->assertSame(false, $result);
        // LESS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '2014-12-30', '2014-12-31');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '2014-12-31', '2014-12-31');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '2015-01-01', '2014-12-31');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '2014-12-31', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, null, '2014-12-31');
        $this->assertSame(false, $result);
        // GREATER_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '2014-12-30', '2014-12-31');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '2014-12-31', '2014-12-31');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '2015-01-01', '2014-12-31');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '2014-12-31', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, null, '2014-12-31');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '2014-1-1', '2014-01-01');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '2014-01-01', '2014-1-1');
        $this->assertSame(1, $result);
        // LESS_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '2014-12-30', '2014-12-31');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '2014-12-31', '2014-12-31');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '2015-01-01', '2014-12-31');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '2014-12-31', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, null, '2014-12-31');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '2014-1-1', '2014-01-01');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '2014-01-01', '2014-1-1');
        $this->assertSame(1, $result);

        // not supported
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::EQUALS, 'abc', 'abc');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::NOT_EQUALS, 'ab', 'abc');
        //$this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'abcd', 'abc');
        $this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::GREATER, '99', '100');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::LESS, '101', '100');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::GREATER_EQUALS, '-100', '100');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::LESS_EQUALS, '100', '100');
        //$this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array('1'), '1');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array('1'), '0');
        $this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__DATE_式()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::DATE);
        $table->create($by);
        $tableId = $table->getId();

        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'now', '2014-01-02');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'now', '2014-01-03');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, 'current_month', '2013-12-31');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, 'current_month', '2014-01-01');
        $this->assertSame(0, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__DATETIME()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::DATETIME);
        $table->create($by);
        $tableId = $table->getId();

        // EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '2014-12-31 12:10', '2014-12-31 12:10');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '2014-12-31 12:10', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, '2014-12-31 12:10');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, null);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '2014-12-31 12:09', '2014-12-31 12:10');
        $this->assertSame(0, $result);
        // NOT_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '2014-12-31 12:09', '2014-12-31 12:10');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '2014-12-31 12:10', '2014-12-31 12:10');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '2014-12-31 12:10', null);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, null, '2014-12-31 12:10');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, null, null);
        $this->assertSame(0, $result);
        // GREATER
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '2014-12-31 12:09', '2014-12-31 12:10');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '2014-12-31 12:10', '2014-12-31 12:10');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '2014-12-31 12:11', '2014-12-31 12:10');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '2014-12-31 12:10', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, null, '2014-12-31 12:10');
        $this->assertSame(false, $result);
        // LESS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '2014-12-31 12:09', '2014-12-31 12:10');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '2014-12-31 12:10', '2014-12-31 12:10');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '2014-12-31 12:11', '2014-12-31 12:10');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '2014-12-31 12:10', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, null, '2014-12-31 12:10');
        $this->assertSame(false, $result);
        // GREATER_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '2014-12-31 12:09', '2014-12-31 12:10');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '2014-12-31 12:10', '2014-12-31 12:10');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '2014-12-31 12:11', '2014-12-31 12:10');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '2014-12-31 12:10', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, null, '2014-12-31 12:10');
        $this->assertSame(false, $result);
        // LESS_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '2014-12-31 12:09', '2014-12-31 12:10');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '2014-12-31 12:10', '2014-12-31 12:10');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '2014-12-31 12:11', '2014-12-31 12:10');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '2014-12-31 12:10', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, null, '2014-12-31 12:10');
        $this->assertSame(false, $result);

        // not supported
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::EQUALS, 'abc', 'abc');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::NOT_EQUALS, 'ab', 'abc');
        //$this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'abcd', 'abc');
        $this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::GREATER, '99', '100');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::LESS, '101', '100');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::GREATER_EQUALS, '-100', '100');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::LESS_EQUALS, '100', '100');
        //$this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array('1'), '1');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array('1'), '0');
        $this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__DATETIME_式()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::DATETIME);
        $table->create($by);
        $tableId = $table->getId();

        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'now', '2014-01-02 23:59');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'now', '2014-01-03 00:00');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '4/current_month', '2014-01-03 23:59');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '4/current_month', '2014-01-04 00:00');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '4/current_month', '2014-01-04 23:59');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '4/current_month', '2014-01-05 00:00');
        $this->assertSame(0, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__TIME()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::TIME);
        $table->create($by);
        $tableId = $table->getId();

        // EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '12:10', '12:10');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '12:10', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, '12:10');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, null);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '12:09', '12:10');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '12:09', '12:9');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '12:9', '12:09');
        $this->assertSame(1, $result);
        // NOT_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '12:09', '12:10');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '12:10', '12:10');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '12:10', null);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, null, '12:10');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, null, null);
        $this->assertSame(0, $result);
        // GREATER
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '12:09', '12:10');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '12:10', '12:10');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '12:11', '12:10');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '12:10', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, null, '12:10');
        $this->assertSame(false, $result);
        // LESS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '12:09', '12:10');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '12:10', '12:10');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '12:11', '12:10');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '12:10', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, null, '12:10');
        $this->assertSame(false, $result);
        // GREATER_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '12:09', '12:10');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '12:10', '12:10');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '12:11', '12:10');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '12:10', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, null, '12:10');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '12:09', '12:9');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '12:9', '12:09');
        $this->assertSame(1, $result);
        // LESS_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '12:09', '12:10');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '12:10', '12:10');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '12:11', '12:10');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '12:10', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, null, '12:10');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '12:09', '12:9');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '12:9', '12:09');
        $this->assertSame(1, $result);

        // not supported
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::EQUALS, 'abc', 'abc');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::NOT_EQUALS, 'ab', 'abc');
        //$this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'abcd', 'abc');
        $this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::GREATER, '99', '100');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::LESS, '101', '100');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::GREATER_EQUALS, '-100', '100');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::LESS_EQUALS, '100', '100');
        //$this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array('1'), '1');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array('1'), '0');
        $this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__DATECALC()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $table->setRoundScale(4);
        $table->setRoundType(MathContext::ROUNDHALF_TOEVEN);
        $column_ = $table->addColumn(Column::DATE);
        $column = $table->addColumn(Column::DATECALC);
        $column->setExpression('3');
        $column->setDateFormat(Column__Datecalc::FORMAT_DATE);
        $column->setDatecalcUnit('month');
        $column->setDatecalcBeforeAfter('after');
        $column->setDateSourceColumn($column_);
        $table->create($by);
        $tableId = $table->getId();

        // EQUALS
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '2015-03-31',
            array(array($column_, '2014-12-31')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '2015-03-31',
            array(array($column_, null)));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null,
            array(array($column_, '2014-12-31')));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null,
            array(array($column_, null)));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '2015-03-30',
            array(array($column_, '2014-12-31')));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '2015-2-28',
            array(array($column_, '2014-11-30')));
        $this->assertSame(false, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '2015-02-28',
            array(array($column_, '2014-11-30')));
        $this->assertSame(1, $result);
        // NOT_EQUALS
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '2015-03-30',
            array(array($column_, '2014-12-31')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '2015-03-31',
            array(array($column_, '2014-12-31')));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '2015-03-31',
            array(array($column_, null)));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, null,
            array(array($column_, '2014-12-31')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, null,
            array(array($column_, null)));
        $this->assertSame(0, $result);
        // GREATER
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '2015-03-30',
            array(array($column_, '2014-12-31')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '2015-03-31',
            array(array($column_, '2014-12-31')));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '2015-04-01',
            array(array($column_, '2014-12-31')));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '2015-03-31',
            array(array($column_, null)));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, null,
            array(array($column_, '2014-12-31')));
        $this->assertSame(false, $result);
        // LESS
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '2015-03-30',
            array(array($column_, '2014-12-31')));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '2015-03-31',
            array(array($column_, '2014-12-31')));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '2015-04-01',
            array(array($column_, '2014-12-31')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '2015-03-31',
            array(array($column_, null)));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, null,
            array(array($column_, '2014-12-31')));
        $this->assertSame(false, $result);
        // GREATER_EQUALS
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '2015-03-30',
            array(array($column_, '2014-12-31')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '2015-03-31',
            array(array($column_, '2014-12-31')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '2015-04-01',
            array(array($column_, '2014-12-31')));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '2015-03-31',
            array(array($column_, null)));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, null,
            array(array($column_, '2014-12-31')));
        $this->assertSame(false, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '2014-4-1',
            array(array($column_, '2014-01-01')));
        $this->assertSame(false, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '2014-04-01',
            array(array($column_, '2014-1-1')));
        $this->assertSame(1, $result);
        // LESS_EQUALS
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '2015-03-30',
            array(array($column_, '2014-12-31')));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '2015-03-31',
            array(array($column_, '2014-12-31')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '2015-04-01',
            array(array($column_, '2014-12-31')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '2015-03-31',
            array(array($column_, null)));
        $this->assertSame(0, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, null,
            array(array($column_, '2014-12-31')));
        $this->assertSame(false, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '2014-4-1',
            array(array($column_, '2014-01-01')));
        $this->assertSame(false, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '2014-04-01',
            array(array($column_, '2014-1-1')));
        $this->assertSame(1, $result);

        // not supported
        //$result = $this->setSingleCalcConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::EQUALS, 'abc',
        //    array(array($column_, 'abc')));
        //$this->assertSame(false, $result);
        //$result = $this->setSingleCalcConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::NOT_EQUALS, 'ab',
        //    array(array($column_, 'abc')));
        //$this->assertSame(false, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab',
            array(array($column_, 'abc')));
        $this->assertSame(false, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'abcd',
            array(array($column_, 'abc')));
        $this->assertSame(false, $result);
        //$result = $this->setSingleCalcConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::GREATER, '99',
        //    array(array($column_, '100')));
        //$this->assertSame(false, $result);
        //$result = $this->setSingleCalcConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::LESS, '101',
        //    array(array($column_, '100')));
        //$this->assertSame(false, $result);
        //$result = $this->setSingleCalcConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::GREATER_EQUALS, '-100',
        //    array(array($column_, '100')));
        //$this->assertSame(false, $result);
        //$result = $this->setSingleCalcConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::LESS_EQUALS, '100',
        //    array(array($column_, '100')));
        //$this->assertSame(false, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array('1'),
            array(array($column_, '1')));
        $this->assertSame(false, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array('1'),
            array(array($column_, '0')));
        $this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }
    public function testInsertRow_条件指定__DATECALC_DATETIME()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $table->setRoundScale(4);
        $table->setRoundType(MathContext::ROUNDHALF_TOEVEN);
        $column_ = $table->addColumn(Column::DATETIME);
        $column = $table->addColumn(Column::DATECALC);
        $column->setExpression('3');
        $column->setDateFormat(Column__Datecalc::FORMAT_DATETIME);
        $column->setDatecalcUnit('month');
        $column->setDatecalcBeforeAfter('after');
        $column->setDateSourceColumn($column_);
        $table->create($by);
        $tableId = $table->getId();

        // EQUALS
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '2015-03-31 01:01',
            array(array($column_, '2014-12-31 01:01')));
        $this->assertSame(1, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }
    public function testInsertRow_条件指定__DATECALC_TIME()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $table->setRoundScale(4);
        $table->setRoundType(MathContext::ROUNDHALF_TOEVEN);
        $column_ = $table->addColumn(Column::TIME);
        $column = $table->addColumn(Column::DATECALC);
        $column->setExpression('3');
        $column->setDateFormat(Column__Datecalc::FORMAT_TIME);
        $column->setDatecalcUnit('hour');
        $column->setDatecalcBeforeAfter('after');
        $column->setDateSourceColumn($column_);
        $table->create($by);
        $tableId = $table->getId();

        // EQUALS
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '04:01',
            array(array($column_, '01:01')));
        $this->assertSame(1, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }
    public function testInsertRow_条件指定__DATECALC_式()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $table->setRoundScale(4);
        $table->setRoundType(MathContext::ROUNDHALF_TOEVEN);
        $column_ = $table->addColumn(Column::DATE);
        $column = $table->addColumn(Column::DATECALC);
        $column->setExpression('1');
        $column->setDateFormat(Column__Datecalc::FORMAT_DATE);
        $column->setDatecalcUnit('day');
        $column->setDatecalcBeforeAfter('after');
        $column->setDateSourceColumn($column_);
        $table->create($by);
        $tableId = $table->getId();

        // EQUALS
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'current_month',
            array(array($column_, '2013-12-31')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '1/current_month',
            array(array($column_, '2013-12-31')));
        $this->assertSame(1, $result);
        $result = $this->setSingleCalcConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '2/current_month',
            array(array($column_, '2013-12-31')));
        $this->assertSame(0, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }
    public function testInsertRow_条件指定__FILE()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::FILE);
        $table->create($by);
        $tableId = $table->getId();

        $file1 = new File();
        $file1->_setFileName('Ａあ漢エ９.jpg');
        $file1->_setFilekey('00001.jpg');
        $file2 = new File();
        $file2->_setFileName('テスト.jpg');
        $file2->_setFilekey('00002.jpg');
        $files = array($file1, $file2);

        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "Ａあ漢エ９", $files);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "Ａあ漢エ９.jpg", $files);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "Ａあ漢エ９.jpg1", $files);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "テスト", $files);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "Aあ漢エ９", $files);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "Ａあ漢エ９", array());
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "Ａあ漢エ９", array($file1));
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "Ａあ漢エ９", array($file2));
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "Ａあ漢エ", $files);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "あ漢エ９", $files);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "あ漢エ", $files);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, "", $files);
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, null, $files);
        $this->assertSame(false, $result);
        // NOT_LIKE
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, "Aあ漢エ９", $files);
        $this->assertSame(1, $result);

        // not supported
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'ab', 'abc');
        $this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::LIKE 'ab', 'abc');
        $this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::NOT_LIKE 'abcd', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '99', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '101', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '-100', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '100', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array('1'), '1');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array('1'), '0');
        $this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }
    public function testInsertRow_条件指定__LINK()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::LINK);
        $table->create($by);
        $tableId = $table->getId();
        // EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', 'abc');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'Abc', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'あいう', 'あいう');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, null);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, '');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '', null);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', 'ab');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, 'ab');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'ab', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '', 'ab');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'ab', '');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'ab', false);
        $this->assertSame(0, $result);
        // NOT_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'abc', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, null, null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, null, '');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'abc', 'ab');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, null, 'ab');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'ab', null);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '', 'ab');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'ab', '');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'ab', false);
        $this->assertSame(1, $result);
        // LIKE
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'abc', 'abc');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'Abc', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', 'abc');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'あい', 'あいう');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'bc', 'abc');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'b', 'abc');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'd', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, '', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, null, 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'd', null);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'd', false);
        $this->assertSame(0, $result);
        // NOT_LIKE
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'abc', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'ab', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'bc', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'b', 'abc');
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'd', 'abc');
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, '', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, null, 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'd', false);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, '', false);
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'abc', null);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, null, false);
        $this->assertSame(false, $result);
        // not supported
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, 'abc', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, 'abc', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, 'abc', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, 'abc', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array('abc'), 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array('abc'), 'abc');
        $this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__LOOKUP_文字列()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::LOOKUP);
        $column->setLabel("Label lookup");
        $referenceTable = $this->target->getTable(1001);
        $column->setReferenceTable($referenceTable);
        $column->setReferenceTargetColumn($referenceTable->getColumn(0));
        $table->create($by);
        $tableId = $table->getId();

        $lookup1 = new LookupValue();
        $lookup1->setKey(1);
        $lookup1->setValue('abc');
        $lookup2 = new LookupValue();
        $lookup2->setKey(1);
        $lookup2->setValue('ab');
        $lookup3 = new LookupValue();
        $lookup3->setKey(1);
        $lookup3->setValue('');
        $lookup4 = new LookupValue();
        $lookup4->setKey(1);
        $lookup5 = null;

        // EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', $lookup1);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', $lookup2);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', $lookup3);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', $lookup4);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', $lookup5);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, $lookup1);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, $lookup2);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, $lookup3);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, $lookup4);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, $lookup5);
        $this->assertSame(1, $result);
        // NOT_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'abc', $lookup2);
        $this->assertSame(1, $result);
        // LIKE
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', $lookup1);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', $lookup2);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', $lookup3);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', $lookup4);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', $lookup5);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, null, $lookup1);
        $this->assertSame(false, $result);
        // NOT_LIKE
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'd', $lookup1);
        $this->assertSame(1, $result);
        // not supported
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, 'abc', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, 'abc', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, 'abc', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, 'abc', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array('abc'), 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array('abc'), 'abc');
        $this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__LOOKUP_数値()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $table->setRoundScale(4);
        $table->setRoundType(MathContext::ROUNDHALF_TOEVEN);
        $column = $table->addColumn(Column::LOOKUP);
        $column->setLabel("Label lookup(number)");
        $referenceTable = $this->target->getTable(1001);
        $column->setReferenceTable($referenceTable);
        $column->setReferenceTargetColumn($referenceTable->getColumn(2));
        $table->create($by);
        $tableId = $table->getId();

        $lookup1 = new LookupValue();
        $lookup1->setKey(1);
        $lookup1->setValue('123.456');
        $lookup2 = new LookupValue();
        $lookup2->setKey(1);
        $lookup2->setValue('123.45606');
        $lookup3 = new LookupValue();
        $lookup3->setKey(1);
        $lookup3->setValue('');
        $lookup4 = new LookupValue();
        $lookup4->setKey(1);
        $lookup5 = null;
        $lookup6 = new LookupValue();
        $lookup6->setKey(1);
        $lookup6->setValue('123.45595');
        $lookup7 = new LookupValue();
        $lookup7->setKey(1);
        $lookup7->setValue('123.45594');

        // EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.456', $lookup1);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.456', $lookup2);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.456', $lookup3);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.456', $lookup4);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.456', $lookup5);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, $lookup1);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, $lookup2);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, $lookup3);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, $lookup4);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, $lookup5);
        $this->assertSame(1, $result);
        // NOT_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '123.456', $lookup2);
        $this->assertSame(1, $result);
        // GREATER_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '123.456', $lookup1);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '123.456', $lookup5);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, null, $lookup1);
        $this->assertSame(false, $result);
        // LESS_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '123.456', $lookup1);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '123.456', $lookup5);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, null, $lookup1);
        $this->assertSame(false, $result);

        // not supported
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::EQUALS, 'abc', 'abc');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::NOT_EQUALS, 'ab', 'abc');
        //$this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'abcd', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '99', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '101', '100');
        $this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::GREATER_EQUALS, '-100', '100');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::LESS_EQUALS, '100', '100');
        //$this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array('1'), '1');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array('1'), '0');
        $this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__LOOKUP_数値_ルックアップされた数値はまるめられない()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $table->setRoundScale(4);
        $table->setRoundType(MathContext::ROUNDHALF_TOEVEN);
        $column = $table->addColumn(Column::LOOKUP);
        $column->setLabel("Label lookup(number)");
        $referenceTable = $this->target->getTable(1001);
        $column->setReferenceTable($referenceTable);
        $column->setReferenceTargetColumn($referenceTable->getColumn(2));
        $table->create($by);
        $tableId = $table->getId();

        $lookup1 = new LookupValue();
        $lookup1->setKey(1);
        $lookup1->setValue('123.456');
        $lookup2 = new LookupValue();
        $lookup2->setKey(1);
        $lookup2->setValue('123.45606');
        $lookup3 = new LookupValue();
        $lookup3->setKey(1);
        $lookup3->setValue('');
        $lookup4 = new LookupValue();
        $lookup4->setKey(1);
        $lookup5 = null;
        $lookup6 = new LookupValue();
        $lookup6->setKey(1);
        $lookup6->setValue('123.45595');
        $lookup7 = new LookupValue();
        $lookup7->setKey(1);
        $lookup7->setValue('123.45594');

        // EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, '123.4561', $lookup2);
        $this->assertSame(0, $result);
        // NOT_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, '123.4561', $lookup2);
        $this->assertSame(1, $result);
        // GREATER_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '123.456', $lookup2);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '123.456', $lookup6);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '123.456', $lookup7);
        $this->assertSame(0, $result);
        // LESS_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '123.456', $lookup2);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '123.456', $lookup6);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '123.456', $lookup7);
        $this->assertSame(1, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__ROWNUM()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $table->create($by);
        $tableId = $table->getId();
        $table = $this->target->getTable($tableId);
        $columns = $table->getColumns(array('type'=>Column::ROWNUM));
        $column = $columns[0];

        $rownum = 0;
        // EQUALS
        $rownum++;
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, $rownum, false);
        $this->assertSame(1, $result);
        $rownum++;
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, $rownum + 1, false);
        $this->assertSame(0, $result);
        $rownum++;
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, $rownum - 1, false);
        $this->assertSame(0, $result);
        $rownum++;
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, null, false);
        $this->assertSame(0, $result);
        // NOT_EQUALS
        $rownum++;
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, $rownum, false);
        $this->assertSame(0, $result);
        $rownum++;
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, $rownum + 1, false);
        $this->assertSame(1, $result);
        $rownum++;
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, $rownum - 1, false);
        $this->assertSame(1, $result);
        $rownum++;
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, null, false);
        $this->assertSame(1, $result);
        // GREATER_EQUALS
        $rownum++;
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, $rownum, false);
        $this->assertSame(1, $result);
        $rownum++;
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, $rownum + 1, false);
        $this->assertSame(0, $result);
        $rownum++;
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, $rownum - 1, false);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, null, false);
        $this->assertSame(false, $result);
        // LESS_EQUALS
        $rownum++;
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, $rownum, false);
        $this->assertSame(1, $result);
        $rownum++;
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, $rownum + 1, false);
        $this->assertSame(1, $result);
        $rownum++;
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, $rownum - 1, false);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, null, false);
        $this->assertSame(false, $result);

        // not supported
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::EQUALS, 'abc', 'abc');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::NOT_EQUALS, 'ab', 'abc');
        //$this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'abcd', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '99', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '101', '100');
        $this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::GREATER_EQUALS, '-100', '100');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::LESS_EQUALS, '100', '100');
        //$this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array('1'), '1');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array('1'), '0');
        $this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__CREATED_BY()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $table->create($by);
        $tableId = $table->getId();
        $table = $this->target->getTable($tableId);
        $columns = $table->getColumns(array('type'=>Column::CREATED_BY));
        $column = $columns[0];
        $user_1 = $by;
        $user_2 = $this->target->getUser(1003);
        $user_3 = $this->target->getUser(1004);

        // CONTAINS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($user_1), false);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($user_1, $user_2), false);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($user_2), false);
        $this->assertSame(0, $result);
        // NOT_CONTAINS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($user_1), false);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($user_2, $user_3), false);
        $this->assertSame(1, $result);

        // not supported
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'ab', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'abcd', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '99', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '101', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '-100', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '100', '100');
        $this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::CONTAINS, array('1'), '1');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::NOT_CONTAINS, array('1'), '0');
        //$this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__CREATED_BY_条件選択肢がなくなっていたら条件そのものを除外()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $table->create($by);
        $tableId = $table->getId();
        $table = $this->target->getTable($tableId);
        $columns = $table->getColumns(array('type'=>Column::CREATED_BY));
        $column = $columns[0];
        $user_1 = $this->createUser(3001);
        $user_2 = $this->createUser(3002);

        // CONTAINS
        $release = $this->setSingleConditionalRule(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($user_1));
        $count1 = $this->countRecords('notification_receipt');
        $this->target->deleteUser($user_1, $by);
        $row = $this->insertRow($release, $by);
        $count2 = $this->countRecords('notification_receipt');
        $this->assertSame(0, $count2 - $count1);
        // NOT_CONTAINS
        $release = $this->setSingleConditionalRule(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($user_2));
        $count1 = $this->countRecords('notification_receipt');
        $this->target->deleteUser($user_2, $by);
        $row = $this->insertRow($release, $by);
        $count2 = $this->countRecords('notification_receipt');
        $this->assertSame(0, $count2 - $count1);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__CREATED_ON()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $table->create($by);
        $tableId = $table->getId();
        $table = $this->target->getTable($tableId);
        $columns = $table->getColumns(array('type'=>Column::CREATED_ON));
        $column = $columns[0];

        // EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'now', false);
        $this->assertSame(1, $result);
        // NOT_CONTAINS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'now', false);
        $this->assertSame(0, $result);
        // GREATER
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, 'now', false);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '2000-01-01 00:00', false);
        $this->assertSame(1, $result);
        // LESS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, 'now', false);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '2100-01-01 00:00', false);
        $this->assertSame(1, $result);
        // GREATER_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, 'now', false);
        $this->assertSame(1, $result);
        // LESS_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, 'now', false);
        $this->assertSame(1, $result);

        // not supported
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::EQUALS, 'abc', 'abc');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::NOT_EQUALS, 'ab', 'abc');
        //$this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'abcd', 'abc');
        $this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::GREATER, '99', '100');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::LESS, '101', '100');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::GREATER_EQUALS, '-100', '100');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::LESS_EQUALS, '100', '100');
        //$this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array('1'), '1');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array('1'), '0');
        $this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__UPDATED_BY()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $table->create($by);
        $tableId = $table->getId();
        $table = $this->target->getTable($tableId);
        $columns = $table->getColumns(array('type'=>Column::UPDATED_BY));
        $column = $columns[0];
        $user_1 = $by;
        $user_2 = $this->target->getUser(1003);
        $user_3 = $this->target->getUser(1004);

        // CONTAINS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($user_1), false);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($user_1, $user_2), false);
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($user_2), false);
        $this->assertSame(0, $result);
        // NOT_CONTAINS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($user_1), false);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($user_2, $user_3), false);
        $this->assertSame(1, $result);

        // not supported
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'ab', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'abcd', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '99', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '101', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '-100', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '100', '100');
        $this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::CONTAINS, array('1'), '1');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::NOT_CONTAINS, array('1'), '0');
        //$this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__UPDATED_BY_条件選択肢がなくなっていたら条件そのものを除外()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $table->create($by);
        $tableId = $table->getId();
        $table = $this->target->getTable($tableId);
        $columns = $table->getColumns(array('type'=>Column::UPDATED_BY));
        $column = $columns[0];
        $user_1 = $this->createUser(3001);
        $user_2 = $this->createUser(3002);

        // CONTAINS
        $release = $this->setSingleConditionalRule(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($user_1));
        $count1 = $this->countRecords('notification_receipt');
        $this->target->deleteUser($user_1, $by);
        $row = $this->insertRow($release, $by);
        $count2 = $this->countRecords('notification_receipt');
        $this->assertSame(0, $count2 - $count1);
        // NOT_CONTAINS
        $release = $this->setSingleConditionalRule(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($user_2));
        $count1 = $this->countRecords('notification_receipt');
        $this->target->deleteUser($user_2, $by);
        $row = $this->insertRow($release, $by);
        $count2 = $this->countRecords('notification_receipt');
        $this->assertSame(0, $count2 - $count1);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__UPDATED_ON()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $table->create($by);
        $tableId = $table->getId();
        $table = $this->target->getTable($tableId);
        $columns = $table->getColumns(array('type'=>Column::UPDATED_ON));
        $column = $columns[0];

        // EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'now', false);
        $this->assertSame(1, $result);
        // NOT_CONTAINS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'now', false);
        $this->assertSame(0, $result);
        // GREATER
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, 'now', false);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '2000-01-01 00:00', false);
        $this->assertSame(1, $result);
        // LESS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, 'now', false);
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '2100-01-01 00:00', false);
        $this->assertSame(1, $result);
        // GREATER_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, 'now', false);
        $this->assertSame(1, $result);
        // LESS_EQUALS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, 'now', false);
        $this->assertSame(1, $result);

        // not supported
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::EQUALS, 'abc', 'abc');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::NOT_EQUALS, 'ab', 'abc');
        //$this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'abcd', 'abc');
        $this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::GREATER, '99', '100');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::LESS, '101', '100');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::GREATER_EQUALS, '-100', '100');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::LESS_EQUALS, '100', '100');
        //$this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array('1'), '1');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array('1'), '0');
        $this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定__USER_SELECTOR()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::USER_SELECTOR);
        $table->create($by);
        $tableId = $table->getId();
        $user_1 = $by;
        $user_2 = $this->target->getUser(1003);
        $user_3 = $this->target->getUser(1004);
        $user_4 = $this->target->getUser(1005);
        $user_5 = $this->target->getUser(1006);

        // CONTAINS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($user_1), array($user_1));
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($user_1, $user_2), array($user_1));
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($user_1), array());
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($user_1, null), array());
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array(null), array());
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($user_1, $user_3), array($user_1, $user_2));
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($user_3, $user_4), array($user_1, $user_2));
        $this->assertSame(0, $result);
        // NOT_CONTAINS
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($user_1), array($user_1));
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($user_1, $user_2), array($user_3));
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($user_1), array());
        $this->assertSame(1, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array(null), array());
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($user_1, $user_3), array($user_1, $user_2));
        $this->assertSame(0, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($user_3, $user_4), array($user_1, $user_2));
        $this->assertSame(1, $result);

        // not supported
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::EQUALS, 'abc', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_EQUALS, 'ab', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LIKE, 'ab', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_LIKE, 'abcd', 'abc');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER, '99', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS, '101', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::GREATER_EQUALS, '-100', '100');
        $this->assertSame(false, $result);
        $result = $this->setSingleConditionalRuleThenInsertRow(
            $tableId, $by, $recipient,
            $column, Criteria::LESS_EQUALS, '100', '100');
        $this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::CONTAINS, array('1'), '1');
        //$this->assertSame(false, $result);
        //$result = $this->setSingleConditionalRuleThenInsertRow(
        //    $tableId, $by, $recipient,
        //    $column, Criteria::NOT_CONTAINS, array('1'), '0');
        //$this->assertSame(false, $result);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }
    public function testInsertRow_条件指定__USER_SELECTOR_条件選択肢がなくなっていたら条件そのものを除外()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::USER_SELECTOR);
        $table->create($by);
        $tableId = $table->getId();
        $user_1 = $this->createUser(3001);
        $user_2 = $this->target->getUser(1003);
        $user_3 = $this->createUser(3002);
        $user_4 = $this->target->getUser(1004);

        // CONTAINS
        $release = $this->setSingleConditionalRule(
            $tableId, $by, $recipient,
            $column, Criteria::CONTAINS, array($user_1));
        $count1 = $this->countRecords('notification_receipt');
        $this->target->deleteUser($user_1, $by);
        $row = $this->insertRow($release, $by,
            array(array($column, array($user_2))));
        $count2 = $this->countRecords('notification_receipt');
        $this->assertSame(0, $count2 - $count1);
        // NOT_CONTAINS
        $release = $this->setSingleConditionalRule(
            $tableId, $by, $recipient,
            $column, Criteria::NOT_CONTAINS, array($user_3));
        $count1 = $this->countRecords('notification_receipt');
        $this->target->deleteUser($user_3, $by);
        $row = $this->insertRow($release, $by,
            array(array($column, array($user_4))));
        $count2 = $this->countRecords('notification_receipt');
        $this->assertSame(0, $count2 - $count1);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testInsertRow_条件指定_条件除外後、残り条件に一致()
    {
        $by = $this->target->getUser(1002);
        $recipient = $this->target->getUser(1001);
        $table = $this->mockUpTable();
        $table->setPublic(true);
        $column = $table->addColumn(Column::USER_SELECTOR);
        $table->create($by);
        $tableId = $table->getId();
        $table = $this->target->getTable($tableId);
        $columns = $table->getColumns(array('type'=>Column::CREATED_BY));
        $column2 = $columns[0];
        $user_1 = $this->createUser(3001);
        $user_2 = $this->target->getUser(1003);
        $user_3 = $this->createUser(3002);
        $user_4 = $this->target->getUser(1004);

        $targetingType = NotificationConditionalRule::TARGETING_USERGROUP;
        $trunk = $this->target->getTable($tableId, Table::TRUNK);
        $rule = $this->buildConditionalRule($trunk, $targetingType,
            array($recipient),
            array(
                array($column, Criteria::CONTAINS, array($user_1)),
                array($column2, Criteria::CONTAINS, array($by)),
            )
        );
        $release = $this->updateTableNotificationRules(
            $trunk, $by, null, array($rule));
        $count1 = $this->countRecords('notification_receipt');
        $this->target->deleteUser($user_1, $by);
        $row = $this->insertRow($release, $by,
            array(array($column, array($user_2))));
        $count2 = $this->countRecords('notification_receipt');
        $this->assertSame(1, $count2 - $count1);

        $release = $this->target->getTable($tableId);
        $release->drop($by);
    }

    public function testUpdateRow_正常()
    {
        // setting
        $by = $this->target->getUser(1001);
        $recipient = $this->target->getUser(1002);

        /* @var $table \TxCore\Table */
        $table = $this->mockUpTableFully();
        $table->setPublic(true);
        $table->setRoundScale(4);
        $table->setRoundType(MathContext::ROUNDHALF_TOEVEN);

        $rules = array();
        $rule1 = new NotificationBasicRule();
        $rule1->_setParent($table);
        $rule1->setTargetingType(NotificationRule::TARGETING_USERGROUP);
        $rule1->_setTargets(array($recipient));
        $rule1->setOnRowInsert(false);
        $rule1->setOnRowUpdate(true);
        $rule1->setOnCommentInsert(false);
        $rules[] = $rule1;
        $table->setNotificationBasicRules($rules);
        $table->create($by);

        $colidx = array(
            Column::TEXT => 0,
            Column::MULTITEXT => 1,
            Column::RICHTEXT => 2,
            Column::NUMBER => 3,
            Column::CALC => 4,
            Column::RADIO => 5,
            Column::CHECKBOX => 6,
            Column::DROPDOWN => 7,
            Column::DATE => 8,
            Column::TIME => 9,
            Column::DATETIME => 10,
            Column::FILE => 11,
            Column::LINK => 12,
            Column::RELATIONAL => 13,
            Column::LOOKUP.'_text' => 14,
            Column::ROWNUM => 15,
            Column::CREATED_BY => 16,
            Column::CREATED_ON => 17,
            Column::UPDATED_BY => 18,
            Column::UPDATED_ON => 19,
            Column::LABEL => 20,
            Column::SPACE => 21,
            Column::LINE => 22,
            Column::LOOKUP.'_number' => 23,
            Column::USER_SELECTOR => 24,
            Column::DATECALC => 25,
        );


        $counts1 = $this->countInsertedNotification(
            $table, $by, $recipient);

        // data insert operation
        $row = new Row($table);
        $row->setValue($table->getColumn($colidx[Column::RADIO]),
            $table->getColumn($colidx[Column::RADIO])->getOption(0));
        $table->insertRows($by, true);
        $row = $table->getRowFor($row->getId());

        $counts2 = $this->countInsertedNotification(
            $table, $by, $recipient, $row);

        // assertion
        $notificationsIncrement =
            $counts2['notifications']['all']
            - $counts1['notifications']['all'];
        $notificationsIncrementPerObject =
            $counts2['notifications']['per_object']
            - $counts1['notifications']['per_object'];
        $receiptsIncrement =
            $counts2['receipts']['all']
            - $counts1['receipts']['all'];
        $receiptsIncrementPerRecipient =
            $counts2['receipts']['per_recipient']
            - $counts1['receipts']['per_recipient'];

        $this->assertSame(0, $notificationsIncrement);
        $this->assertSame(0, $notificationsIncrementPerObject);
        $this->assertSame(0, $receiptsIncrement);
        $this->assertSame(0, $receiptsIncrementPerRecipient);

        //$list = $this->target->searchNotificationReceipts($recipient);
        //$this->assertIdentical(0, $list->getTotalRowCount());

        //$receipts = $list->getRows();
        //$notification = $receipts[0]->getNotification();
        //$this->assertIdentical(
        //    'New Radio 1 で登録されました。', $notification->getMessage());

        //
        // data update with no change
        //

        $counts1 = $counts2;
        $row = $table->getRowFor($row->getId());
        $row->update($by, true);
        $row = $table->getRowFor($row->getId());

        $counts2 = $this->countInsertedNotification(
            $table, $by, $recipient, $row);

        // assertion
        $notificationsIncrement =
            $counts2['notifications']['all']
            - $counts1['notifications']['all'];
        $notificationsIncrementPerObject =
            $counts2['notifications']['per_object']
            - $counts1['notifications']['per_object'];
        $receiptsIncrement =
            $counts2['receipts']['all']
            - $counts1['receipts']['all'];
        $receiptsIncrementPerRecipient =
            $counts2['receipts']['per_recipient']
            - $counts1['receipts']['per_recipient'];

        $this->assertSame(0, $notificationsIncrement);
        $this->assertSame(0, $notificationsIncrementPerObject);
        $this->assertSame(0, $receiptsIncrement);
        $this->assertSame(0, $receiptsIncrementPerRecipient);


        //
        // data update with no change 2
        //

        $counts1 = $counts2;
        $row = $table->getRowFor($row->getId());
        $row->setValue($table->getColumn($colidx[Column::TEXT]),
            null);
        $row->setValue($table->getColumn($colidx[Column::MULTITEXT]),
            null);
        $row->setValue($table->getColumn($colidx[Column::RICHTEXT]),
            null);
        $row->setValue($table->getColumn($colidx[Column::NUMBER]),
            null);
        //$row->setValue($table->getColumn($colidx[Column::RADIO]), '');
        $row->setValue($table->getColumn($colidx[Column::CHECKBOX]),
            null);
        $row->setValue($table->getColumn($colidx[Column::DROPDOWN]),
            null);
        $row->setValue($table->getColumn($colidx[Column::DATE]),
            null);
        $row->setValue($table->getColumn($colidx[Column::TIME]),
            null);
        $row->setValue($table->getColumn($colidx[Column::DATETIME]),
            null);
        $row->setValue($table->getColumn($colidx[Column::FILE]),
            null);
        $row->setValue($table->getColumn($colidx[Column::LINK]),
            null);
        $row->setValue($table->getColumn($colidx[Column::LOOKUP.'_text']),
            new LookupValue());
        $row->setValue($table->getColumn($colidx[Column::LOOKUP.'_number']),
            new LookupValue());
        $row->setValue($table->getColumn($colidx[Column::USER_SELECTOR]),
            null);
        $row->update($by, true);
        $row = $table->getRowFor($row->getId());

        $counts2 = $this->countInsertedNotification(
            $table, $by, $recipient, $row);

        // assertion
        $notificationsIncrement =
            $counts2['notifications']['all']
            - $counts1['notifications']['all'];
        $notificationsIncrementPerObject =
            $counts2['notifications']['per_object']
            - $counts1['notifications']['per_object'];
        $receiptsIncrement =
            $counts2['receipts']['all']
            - $counts1['receipts']['all'];
        $receiptsIncrementPerRecipient =
            $counts2['receipts']['per_recipient']
            - $counts1['receipts']['per_recipient'];

        $this->assertSame(0, $notificationsIncrement);
        $this->assertSame(0, $notificationsIncrementPerObject);
        $this->assertSame(0, $receiptsIncrement);
        $this->assertSame(0, $receiptsIncrementPerRecipient);

        //
        // data update with no change 3
        //

        $counts1 = $counts2;
        $row = $table->getRowFor($row->getId());
        $row->setValue($table->getColumn($colidx[Column::TEXT]),
            '');
        $row->setValue($table->getColumn($colidx[Column::MULTITEXT]),
            '');
        $row->setValue($table->getColumn($colidx[Column::RICHTEXT]),
            '');
        $row->setValue($table->getColumn($colidx[Column::NUMBER]),
            '');
        //$row->setValue($table->getColumn($colidx[Column::RADIO]), '');
        $row->setValue($table->getColumn($colidx[Column::CHECKBOX]),
            array());
        $row->setValue($table->getColumn($colidx[Column::DROPDOWN]),
            null);
        $row->setValue($table->getColumn($colidx[Column::DATE]),
            '');
        $row->setValue($table->getColumn($colidx[Column::TIME]),
            '');
        $row->setValue($table->getColumn($colidx[Column::DATETIME]),
            '');
        $row->setValue($table->getColumn($colidx[Column::FILE]),
            array());
        $row->setValue($table->getColumn($colidx[Column::LINK]),
            '');
        $row->setValue($table->getColumn($colidx[Column::LOOKUP.'_text']),
            new LookupValue());
        $row->setValue($table->getColumn($colidx[Column::LOOKUP.'_number']),
            new LookupValue());
        $row->setValue($table->getColumn($colidx[Column::USER_SELECTOR]),
            array());
        $row->update($by, true);
        $row = $table->getRowFor($row->getId());

        $counts2 = $this->countInsertedNotification(
            $table, $by, $recipient, $row);

        // assertion
        $notificationsIncrement =
            $counts2['notifications']['all']
            - $counts1['notifications']['all'];
        $notificationsIncrementPerObject =
            $counts2['notifications']['per_object']
            - $counts1['notifications']['per_object'];
        $receiptsIncrement =
            $counts2['receipts']['all']
            - $counts1['receipts']['all'];
        $receiptsIncrementPerRecipient =
            $counts2['receipts']['per_recipient']
            - $counts1['receipts']['per_recipient'];

        $this->assertSame(0, $notificationsIncrement);
        $this->assertSame(0, $notificationsIncrementPerObject);
        $this->assertSame(0, $receiptsIncrement);
        $this->assertSame(0, $receiptsIncrementPerRecipient);

        //
        // data update with updator user changing
        //
        $by = $this->target->getUser(1003);

        $counts1 = $counts2;
        $row = $table->getRowFor($row->getId());
        $row->setValue($table->getColumn($colidx[Column::TEXT]),
            '');
        $row->setValue($table->getColumn($colidx[Column::MULTITEXT]),
            '');
        $row->setValue($table->getColumn($colidx[Column::RICHTEXT]),
            '');
        $row->setValue($table->getColumn($colidx[Column::NUMBER]),
            '');
        //$row->setValue($table->getColumn($colidx[Column::RADIO]), '');
        $row->setValue($table->getColumn($colidx[Column::CHECKBOX]),
            array());
        $row->setValue($table->getColumn($colidx[Column::DROPDOWN]),
            null);
        $row->setValue($table->getColumn($colidx[Column::DATE]),
            '');
        $row->setValue($table->getColumn($colidx[Column::TIME]),
            '');
        $row->setValue($table->getColumn($colidx[Column::DATETIME]),
            '');
        $row->setValue($table->getColumn($colidx[Column::FILE]),
            array());
        $row->setValue($table->getColumn($colidx[Column::LINK]),
            '');
        $row->setValue($table->getColumn($colidx[Column::LOOKUP.'_text']),
            new LookupValue());
        $row->setValue($table->getColumn($colidx[Column::LOOKUP.'_number']),
            new LookupValue());
        $row->setValue($table->getColumn($colidx[Column::USER_SELECTOR]),
            array());
        $row->update($by, true);
        $row = $table->getRowFor($row->getId());

        $counts2 = $this->countInsertedNotification(
            $table, $by, $recipient, $row);

        // assertion
        $notificationsIncrement =
            $counts2['notifications']['all']
            - $counts1['notifications']['all'];
        $notificationsIncrementPerObject =
            $counts2['notifications']['per_object']
            - $counts1['notifications']['per_object'];
        $receiptsIncrement =
            $counts2['receipts']['all']
            - $counts1['receipts']['all'];
        $receiptsIncrementPerRecipient =
            $counts2['receipts']['per_recipient']
            - $counts1['receipts']['per_recipient'];

        $this->assertSame(1, $notificationsIncrement);
        $this->assertSame(1, $notificationsIncrementPerObject);
        $this->assertSame(1, $receiptsIncrement);
        $this->assertSame(1, $receiptsIncrementPerRecipient);

        $list = $this->target->searchNotificationReceipts($recipient);
        $this->assertIdentical(1, $list->getTotalRowCount());

        $receipts = $list->getRows();
        $notification = $receipts[0]->getNotification();
        $this->assertIdentical(
            1, count($notification->getChanges()));

        //
        // data update with non null values
        //

        $counts1 = $counts2;
        $row = $table->getRowFor($row->getId());
        $row->setValue($table->getColumn($colidx[Column::TEXT]),
            'abc');
        $row->setValue($table->getColumn($colidx[Column::MULTITEXT]),
            'abc');
        $row->setValue($table->getColumn($colidx[Column::RICHTEXT]),
            'abc');
        $row->setValue($table->getColumn($colidx[Column::NUMBER]),
            '10.00006'); // -> 10.0001
        //$row->setValue($table->getColumn($colidx[Column::RADIO]), '');
        $row->setValue($table->getColumn($colidx[Column::CHECKBOX]),
            array(
                $table->getColumn($colidx[Column::CHECKBOX])->getOption(0),
                $table->getColumn($colidx[Column::CHECKBOX])->getOption(1),
            )
        );
        $row->setValue($table->getColumn($colidx[Column::DROPDOWN]),
            $table->getColumn($colidx[Column::DROPDOWN])->getOption(0));
        $row->setValue($table->getColumn($colidx[Column::DATE]),
            '2015-06-22');
        $row->setValue($table->getColumn($colidx[Column::TIME]),
            '23:59');
        $row->setValue($table->getColumn($colidx[Column::DATETIME]),
            '2015-06-22 23:59');
        $file1 = new File();
        $file1->_setFileName('Ａあ漢エ９.jpg');
        $file1->_setFilekey('00001.jpg');
        $file2 = new File();
        $file2->_setFileName('テスト.jpg');
        $file2->_setFilekey('00002.jpg');
        $files = array($file1, $file2);
        $row->setValue($table->getColumn($colidx[Column::FILE]),
            array($file1, $file2));
        $row->setValue($table->getColumn($colidx[Column::LINK]),
            'somebody@example.com');
        $lookup1 = new LookupValue();
        $lookup1->setKey(1);
        $lookup1->setValue('abc');
        $row->setValue($table->getColumn($colidx[Column::LOOKUP.'_text']),
            $lookup1);
        $lookup2 = new LookupValue();
        $lookup2->setKey(2);
        $lookup2->setValue('10.00006');
        $row->setValue($table->getColumn($colidx[Column::LOOKUP.'_number']),
            $lookup2); // -> 10.00006
        $row->setValue($table->getColumn($colidx[Column::USER_SELECTOR]),
            array(
                $this->target->getUser(1001),
                $this->target->getUser(1003),
            )
        );
        $row->update($by, true);
        $row = $table->getRowFor($row->getId());

        $counts2 = $this->countInsertedNotification(
            $table, $by, $recipient, $row);

        // assertion
        $notificationsIncrement =
            $counts2['notifications']['all']
            - $counts1['notifications']['all'];
        $notificationsIncrementPerObject =
            $counts2['notifications']['per_object']
            - $counts1['notifications']['per_object'];
        $receiptsIncrement =
            $counts2['receipts']['all']
            - $counts1['receipts']['all'];
        $receiptsIncrementPerRecipient =
            $counts2['receipts']['per_recipient']
            - $counts1['receipts']['per_recipient'];

        $this->assertSame(1, $notificationsIncrement);
        $this->assertSame(1, $notificationsIncrementPerObject);
        $this->assertSame(1, $receiptsIncrement);
        $this->assertSame(1, $receiptsIncrementPerRecipient);

        $list = $this->target->searchNotificationReceipts($recipient);
        $this->assertIdentical(2, $list->getTotalRowCount());

        $receipts = $list->getRows();
        $notification = $receipts[0]->getNotification();
        $this->assertIdentical(16, count($notification->getChanges()));

        //
        // data update with no change
        //

        $counts1 = $counts2;
        $row = $table->getRowFor($row->getId());
        $row->update($by, true);
        $row = $table->getRowFor($row->getId());

        $counts2 = $this->countInsertedNotification(
            $table, $by, $recipient, $row);

        // assertion
        $notificationsIncrement =
            $counts2['notifications']['all']
            - $counts1['notifications']['all'];
        $notificationsIncrementPerObject =
            $counts2['notifications']['per_object']
            - $counts1['notifications']['per_object'];
        $receiptsIncrement =
            $counts2['receipts']['all']
            - $counts1['receipts']['all'];
        $receiptsIncrementPerRecipient =
            $counts2['receipts']['per_recipient']
            - $counts1['receipts']['per_recipient'];

        $this->assertSame(0, $notificationsIncrement);
        $this->assertSame(0, $notificationsIncrementPerObject);
        $this->assertSame(0, $receiptsIncrement);
        $this->assertSame(0, $receiptsIncrementPerRecipient);

        //
        // data update with near values
        //

        $counts1 = $counts2;
        $row = $table->getRowFor($row->getId());
        $row->setValue($table->getColumn($colidx[Column::NUMBER]),
            '10.00007'); // -> 10.0001
        $file1 = new File();
        $file1->_setFileName('Ａあ漢エ９.jpg');
        $file1->_setFilekey('00001.jpg');
        $file2 = new File();
        $file2->_setFileName('テスト.jpg');
        $file2->_setFilekey('00003.jpg'); // 00002 -> 00003
        $files = array($file1, $file2);
        $row->setValue($table->getColumn($colidx[Column::FILE]),
            array($file1, $file2));
        $lookup1->setKey(3);
        $lookup1->setValue('abc');
        $row->setValue($table->getColumn($colidx[Column::LOOKUP.'_text']),
            $lookup1);
        $lookup2 = new LookupValue();
        $lookup2->setKey(2);
        $lookup2->setValue('10.00007'); // 10.00006 -> 10.00007
        $row->setValue($table->getColumn($colidx[Column::LOOKUP.'_number']),
            $lookup2); // -> 10.00007
        $row->update($by, true);
        $row = $table->getRowFor($row->getId());

        $counts2 = $this->countInsertedNotification(
            $table, $by, $recipient, $row);

        // assertion
        $notificationsIncrement =
            $counts2['notifications']['all']
            - $counts1['notifications']['all'];
        $notificationsIncrementPerObject =
            $counts2['notifications']['per_object']
            - $counts1['notifications']['per_object'];
        $receiptsIncrement =
            $counts2['receipts']['all']
            - $counts1['receipts']['all'];
        $receiptsIncrementPerRecipient =
            $counts2['receipts']['per_recipient']
            - $counts1['receipts']['per_recipient'];

        $this->assertSame(1, $notificationsIncrement);
        $this->assertSame(1, $notificationsIncrementPerObject);
        $this->assertSame(1, $receiptsIncrement);
        $this->assertSame(1, $receiptsIncrementPerRecipient);

        $list = $this->target->searchNotificationReceipts($recipient);
        $this->assertIdentical(3, $list->getTotalRowCount());

        $receipts = $list->getRows();
        $notification = $receipts[0]->getNotification();
        $this->assertIdentical(3, count($notification->getChanges()));

        //
        // data update with non null new values
        //

        $counts1 = $counts2;
        $row = $table->getRowFor($row->getId());
        $row->setValue($table->getColumn($colidx[Column::TEXT]),
            'Abc');
        $row->setValue($table->getColumn($colidx[Column::MULTITEXT]),
            'Abc');
        $row->setValue($table->getColumn($colidx[Column::RICHTEXT]),
            'Abc');
        $row->setValue($table->getColumn($colidx[Column::NUMBER]),
            '10.0002');
        $row->setValue($table->getColumn($colidx[Column::RADIO]),
            $table->getColumn($colidx[Column::RADIO])->getOption(1));
        $row->setValue($table->getColumn($colidx[Column::CHECKBOX]),
            array(
                $table->getColumn($colidx[Column::CHECKBOX])->getOption(1),
            )
        );
        $row->setValue($table->getColumn($colidx[Column::DROPDOWN]),
            $table->getColumn($colidx[Column::DROPDOWN])->getOption(1));
        $row->setValue($table->getColumn($colidx[Column::DATE]),
            '2015-06-23');
        $row->setValue($table->getColumn($colidx[Column::TIME]),
            '23:58');
        $row->setValue($table->getColumn($colidx[Column::DATETIME]),
            '2015-06-23 23:59');
        $file1 = new File();
        $file1->_setFileName('Ａあ漢エ９.jpg');
        $file1->_setFilekey('00001.jpg');
        $file2 = new File();
        $file2->_setFileName('テスト.jpg');
        $file2->_setFilekey('00002.jpg');
        $files = array($file1, $file2);
        $row->setValue($table->getColumn($colidx[Column::FILE]),
            array($file1, $file2));
        $row->setValue($table->getColumn($colidx[Column::LINK]),
            'somebody1@example.com');
        $lookup1 = new LookupValue();
        $lookup1->setKey(1);
        $lookup1->setValue('abcd');
        $row->setValue($table->getColumn($colidx[Column::LOOKUP.'_text']),
            $lookup1);
        $lookup2 = new LookupValue();
        $lookup2->setKey(4);
        $lookup2->setValue('10.00007');
        $row->setValue($table->getColumn($colidx[Column::LOOKUP.'_number']),
            $lookup2); // -> 10.00006
        $row->setValue($table->getColumn($colidx[Column::USER_SELECTOR]),
            array(
                $this->target->getUser(1004),
                $this->target->getUser(1003),
            )
        );
        $row->update($by, true);
        $row = $table->getRowFor($row->getId());

        $counts2 = $this->countInsertedNotification(
            $table, $by, $recipient, $row);

        // assertion
        $notificationsIncrement =
            $counts2['notifications']['all']
            - $counts1['notifications']['all'];
        $notificationsIncrementPerObject =
            $counts2['notifications']['per_object']
            - $counts1['notifications']['per_object'];
        $receiptsIncrement =
            $counts2['receipts']['all']
            - $counts1['receipts']['all'];
        $receiptsIncrementPerRecipient =
            $counts2['receipts']['per_recipient']
            - $counts1['receipts']['per_recipient'];

        $this->assertSame(1, $notificationsIncrement);
        $this->assertSame(1, $notificationsIncrementPerObject);
        $this->assertSame(1, $receiptsIncrement);
        $this->assertSame(1, $receiptsIncrementPerRecipient);

        $list = $this->target->searchNotificationReceipts($recipient);
        $this->assertIdentical(4, $list->getTotalRowCount());

        $receipts = $list->getRows();
        $notification = $receipts[0]->getNotification();
        $this->assertIdentical(17, count($notification->getChanges()));

        //
        // data update with no clear fields
        //

        $counts1 = $counts2;
        $row = $table->getRowFor($row->getId());
        $row->setValue($table->getColumn($colidx[Column::TEXT]),
            '');
        $row->setValue($table->getColumn($colidx[Column::MULTITEXT]),
            '');
        $row->setValue($table->getColumn($colidx[Column::RICHTEXT]),
            '');
        $row->setValue($table->getColumn($colidx[Column::NUMBER]),
            '');
        //$row->setValue($table->getColumn($colidx[Column::RADIO]), '');
        $row->setValue($table->getColumn($colidx[Column::CHECKBOX]),
            array());
        $row->setValue($table->getColumn($colidx[Column::DROPDOWN]),
            null);
        $row->setValue($table->getColumn($colidx[Column::DATE]),
            '');
        $row->setValue($table->getColumn($colidx[Column::TIME]),
            '');
        $row->setValue($table->getColumn($colidx[Column::DATETIME]),
            '');
        $row->setValue($table->getColumn($colidx[Column::FILE]),
            array());
        $row->setValue($table->getColumn($colidx[Column::LINK]),
            '');
        $row->setValue($table->getColumn($colidx[Column::LOOKUP.'_text']),
            new LookupValue());
        $row->setValue($table->getColumn($colidx[Column::LOOKUP.'_number']),
            new LookupValue());
        $row->setValue($table->getColumn($colidx[Column::USER_SELECTOR]),
            array());
        $row->update($by, true);
        $row = $table->getRowFor($row->getId());

        $counts2 = $this->countInsertedNotification(
            $table, $by, $recipient, $row);

        // assertion
        $notificationsIncrement =
            $counts2['notifications']['all']
            - $counts1['notifications']['all'];
        $notificationsIncrementPerObject =
            $counts2['notifications']['per_object']
            - $counts1['notifications']['per_object'];
        $receiptsIncrement =
            $counts2['receipts']['all']
            - $counts1['receipts']['all'];
        $receiptsIncrementPerRecipient =
            $counts2['receipts']['per_recipient']
            - $counts1['receipts']['per_recipient'];

        $this->assertSame(1, $notificationsIncrement);
        $this->assertSame(1, $notificationsIncrementPerObject);
        $this->assertSame(1, $receiptsIncrement);
        $this->assertSame(1, $receiptsIncrementPerRecipient);

        $list = $this->target->searchNotificationReceipts($recipient);
        $this->assertIdentical(5, $list->getTotalRowCount());

        $receipts = $list->getRows();
        $notification = $receipts[0]->getNotification();
        $this->assertIdentical(16, count($notification->getChanges()));

        $by = $this->target->getUser(1001);
        $table->drop($by);
    }

}
