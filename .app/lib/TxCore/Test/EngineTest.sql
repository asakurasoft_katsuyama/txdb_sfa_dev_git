
delete from env_ut;
delete from `user`;
delete from table_def;
delete from column_def;
delete from `data`;
delete from `row`;
delete from row_no;
delete from `indexes`;
delete from `option`;
delete from `comment`;
delete from `view_column`;
delete from `icon`;
delete from `group`;
delete from `release_control`;
delete from reference;
delete from revision_no;
delete from `view_calendar_setting`;
delete from `notification_basic_rule`;
delete from `notification_conditional_rule`;
delete from `notification`;
delete from `notification_receipt`;

insert into `group` (id, name)
  values (11, 'グループ１');

insert into user 
	(id, role, login_id, password, screen_name, profile) values 
	(1001, 'user', 'user1@sample.com', 'password', 'test user01', 'my company'),
	(1002, 'admin', 'user2@sample.com', 'password', 'test user02', 'my company'),
	(1003, 'user', 'user3@sample.com', 'password', 'test user03', 'my company'),
	(1004, 'user', 'user4@sample.com', 'password', 'test user04', 'my company'),
	(1005, 'user', 'user5@abc.com', 'password', 'test user05', 'my company'),/*hit abc*/
	(1006, 'user', 'user6@sample.com', 'password', 'abc user', 'my company'),/*hit abc*/ 
	(1007, 'user', 'user7@sample.com', 'password', 'test user07', 'abc company'),/*hit abc*/
	(1008, 'user', 'user8@sample.com', 'password', 'test user08', 'my company'),
	(1009, 'user', 'user9@sample.com', 'password', 'test user09', 'my company'),
	(1010, 'user', 'use10@sample.com', 'password', 'test user10', 'my company'),
	(1011, 'user', 'use11@sample.com', 'password', 'test user11', 'my company')
;
insert into table_def 
	(id,revision, name,description, icon_filekey, is_public, is_api_enabled, api_key, owner, round_type, round_scale, is_template, is_comment_enabled, is_calendar_enabled) values
	(1001, 1, 'table 1', 'This is table 1', 'default.png', 1,   1,  'APIKEY', 1001, 'ceil', 3, 0, 0, 0),
	(1001, 2, 'table 1', 'This is table 1', 'default.png', 1, 	1, 	null,     1001, 'ceil', 3, 0, 0, 0),
	(1003, 1, 'table 2', 'This is table 2', 'default.png', 0,   1,  'APIKEY', 1002, 'ceil', 3, 0, 0, 0),
	(1003, 2, 'table 2', 'This is table 2', 'default.png', 0, 	1, 	null,     1002, 'ceil', 3, 0, 0, 0),
	(1005, 1, 'table 3', 'This is table 3', 'default.png', 1,   1,  'APIKEY', 1002, 'ceil', 3, 0, 0, 0),
	(1005, 2, 'table 3', 'This is table 3', 'default.png', 1, 	1,  null,    1002, 'ceil', 3, 0, 0, 0),
/*	(1007, 'table 4', 'This is table 4', 'default.png', 0,    null, 1008, 1,    'APIKEY', 1002, 'ceil', 3, 1),
	(1007, 'table 4', 'This is table 4', 'default.png', null, 0,    null, null,  null,    1002, 'ceil', 3, 1),*/
	(2001, 1, 'sample template 1', 'This is sample template 1.', 'default1.png', null, null, null, 1001, 'ceil', 3, 1, 0, 0),
	(2002, 2, 'sample template 2', 'This is sample template 2.', 'default2.png', null, null, null, 1001, 'ceil', 3, 1, 0, 0),
/*  (2003, 1, 'sample template 1', 'This is sample template 1.', 'default1.png', null, null, null, 1001, 'ceil', 3, 1),
	(2004, 2, 'sample template 2', 'This is sample template 2.', 'default2.png', null, null, null, 1001, 'ceil', 3, 1),*/
	(3001, 1, 'table 301', 'This is table 301', 'icon301.png', 0, 1,    'APIKEY', 1002, 'ceil', 3, 0, 0, 0),
	(3001, 2, 'table 301', 'This is table 301', 'default.png', 0, 1, null,     1002, 'ceil', 3, 0, 0, 0)
;
insert into release_control (table_id, trunk_revision, release_revision, has_pending_release) values 
	 (1001, 2, 1, 0)
	,(1003, 2, 1, 0)
	,(1005, 2, 1, 0)
	,(3001, 2, 1, 0)
;

insert into revision_no (table_id, revision_no) values
	 (1001, 2)
	,(1003, 2)
	,(1005, 2)
	,(3001, 2)
;

insert into column_def
	(id, revision, table_id, column_type, label, properties) values
	(1, 1, 1001, 'text', 'コード', '{"showsField":true,"positionX":1,"positionX":2}'),
	(2, 1, 1001, 'multitext', '名前', '{"showsField":true,"positionX":1,"positionX":2}'),
	(3, 1, 1001, 'number', '価格', '{"showsField":true,"positionX":1,"positionX":2}'),
	(4, 1, 1001, 'rownum', 'No', '{"showsField":false,"positionX":1,"positionX":2}');
insert into column_def
	(id, revision, table_id, column_type, properties) values
	(100301, 1, 1003, 'text', '{"positionX":1,"positionX":2}'),-- code
	(100302, 1, 1003, 'multitext', '{"positionX":1,"positionX":2}'),-- description
	(100303, 1, 1003, 'richtext', '{"positionX":1,"positionX":2}'),-- quantity
	(100304, 1, 1003, 'number', '{"positionX":1,"positionX":2}'),-- total amount (= unit x quantity)
	(100305, 1, 1003, 'calc', '{ "expression":"A*2","positionX":1,"positionX":2 }'),--
	(100306, 1, 1003, 'radio', '{"positionX":1,"positionX":2}'),--
	(100307, 1, 1003,'checkbox', '{"positionX":1,"positionX":2}'),--
	(100308, 1, 1003, 'dropdown', '{"positionX":1,"positionX":2}'),--
	(100309, 1, 1003, 'date', '{"positionX":1,"positionX":2}'),--
	(100310, 1, 1003, 'time', '{"positionX":1,"positionX":2}'),--
	(100311, 1, 1003, 'datetime', '{"positionX":1,"positionX":2}'),/*[10]*/
	(100312, 1, 1003, 'file', '{"positionX":1,"positionX":2}'),--
	(100313, 1, 1003, 'link', '{"linkType":"email","positionX":1,"positionX":2}'),--
	(100314, 1, 1003, 'relational', '{"positionX":1,"positionX":2}'),/*[13]*/
	(100315, 1, 1003, 'lookup', '{"referenceTableId":1001,"referenceTargetColumnId":1,"positionX":1,"positionX":2}'),--
	(100316, 1, 1003, 'rownum', '{"positionX":1,"positionX":2}'),--
	(100317, 1, 1003, 'created_by', '{"positionX":1,"positionX":2}'),--
	(100318, 1, 1003, 'created_on', '{"positionX":1,"positionX":2}'),--
	(100319, 1, 1003, 'updated_by', '{"positionX":1,"positionX":2}'),--
	(100320, 1, 1003, 'updated_on', '{"positionX":1,"positionX":1}'),
	(100321, 1, 1003, 'label', '{"positionX":1,"positionX":2}'),
	(100322, 1, 1003, 'space', '{"positionX":1,"positionX":2}'),
	(100323, 1, 1003, 'line', '{"positionX":1,"positionX":2}'),
	(100324, 1, 1003, 'lookup', '{"referenceTableId":1001,"referenceTargetColumnId":3,"positionX":1,"positionX":2}'),--
	--
/*	(1024, 1007, 'text', '{}'),--
	(100702, 1007, 'rownum', '{}'), --*/
	--
	(200201, 1, 2002, 'text', '{"positionX":1,"positionX":2}'),
	(200202, 1, 2002, 'number', '{"positionX":1,"positionX":2}'),
	(200203, 1, 2002, 'date', '{"positionX":1,"positionX":2}'),
	(200204, 1, 2002, 'radio', '{"positionX":1,"positionX":2}'),
	--
	(300101, 1, 3001, 'text', '{"showsField":true,"positionX":1,"positionX":2}'), -- code
	(300102, 1, 3001, 'multitext', '{"showsField":true,"positionX":1,"positionX":2}'), -- description
	(300103, 1, 3001, 'richtext', '{"showsField":true,"positionX":1,"positionX":2}'), -- quantity
	(300104, 1, 3001, 'number', '{"showsField":true,"positionX":1,"positionX":2}'), -- total amount (= unit x quantity)
	(300105, 1, 3001, 'calc', '{"showsField":true, "expression":"A*2","positionX":1,"positionX":2}'), --
	(300106, 1, 3001, 'radio', '{"showsField":true,"defaultValueId":"30010601","positionX":1,"positionX":2}'), --
	(300107, 1, 3001,'checkbox', '{"showsField": true,	"defaultValueIds":[30010702,30010703],"positionX":1,"positionX":2}'), --
	(300108, 1, 3001, 'dropdown', '{"showsField":true,"positionX":1,"positionX":2}'), --
	(300109, 1, 3001, 'date', '{"showsField":true,"positionX":1,"positionX":2}'), --
	(300110, 1, 3001, 'time', '{"showsField":true,"positionX":1,"positionX":2}'), --
	(300111, 1, 3001, 'datetime', '{"showsField":true,"positionX":1,"positionX":2}'), --
	(300112, 1, 3001, 'file', '{"showsField":true,"positionX":1,"positionX":2}'), --
	(300113, 1, 3001, 'link', '{"showsField":true,"positionX":1,"positionX":2}'), --
	(300114, 1, 3001, 'relational', '{"showsField":true,"positionX":1,"positionX":2,"referenceTableId":1003,"referenceTargetColumnId":100301,"referenceSourceColumnId":300101,"referenceColumnIds":[100301,100302],"referenceSortColumnId":100301,"referenceSortsReverse":true,"referenceMaxRow":10}'), --
	(300115, 1, 3001, 'lookup', '{"showsField":true,"positionX":1,"positionX":2,"referenceTableId":1003,"referenceTargetColumnId":100301,"referenceColumnIds":[100301,100302],"referenceCopyMapIds":[[100301,300101]]}'), /*文字列*/
	(300116, 1, 3001, 'rownum', '{"showsField":false,"positionX":1,"positionX":2,"referenceColumnIds":[100301,100302],"referenceCopyMapIds":[[100301,300101]],"referenceTableId":1003,"referenceTargetColumnId":100304}')
   ,(300117, 1, 3001, 'created_by', '{"showsField":false,"positionX":1,"positionX":2}')
   ,(300118, 1, 3001, 'created_on', '{"showsField":false,"positionX":1,"positionX":2}')
   ,(300119, 1, 3001, 'updated_by', '{"showsField":false,"positionX":1,"positionX":2}')
   ,(300120, 1, 3001, 'updated_on', '{"showsField":false,"positionX":1,"positionX":2}')
   ,(300121, 1, 3001, 'label', '{"showsField":true,"positionX":1,"positionX":2}')
   ,(300122, 1, 3001, 'space', '{"showsField":true,"positionX":1,"positionX":2}')
   ,(300123, 1, 3001, 'line', '{"showsField":true,"positionX":1,"positionX":2}')
   ,(300124, 1, 3001, 'lookup', '{"showsField":true,"positionX":1,"positionX":2,"referenceTableId":1003,"referenceTargetColumnId":100304,"referenceColumnIds":[100301,100302,100304],"referenceCopyMapIds":[[100301,300101]]}') /*数値*/
   -- work
   ,(300201, 2, 3001, 'text', '{"positionX":1,"positionX":2}')
;
update column_def set code = 'A' where id = 100304;
update column_def set code = 'A' where id = 300104;


update column_def set label = 'ラベル' where label is null;
insert into `option` 
	(id, revision, table_id, column_id, name, seq) values
	(10030601, 1, 1003, 100306, 'Option 1-1', 3),
	/*(10030602, 1, 1003, 100306, 'Option 1-2', 1),*/
	(10030603, 1, 1003, 100306, 'Option 1-3', 2),
	(10030701, 1, 1003, 100307, 'Option 2-1', 1),
	(10030702, 1, 1003, 100307, 'Option 2-2', 2),
	(10030703, 1, 1003, 100307, 'Option 2-3', 3),
	(10030801, 1, 1003, 100308, 'Option 3-1', 1),
	(10030802, 1, 1003, 100308, 'Option 3-2', 2),
	(10030803, 1, 1003, 100308, 'Option 3-3', 3),
	
	(30010601, 1, 3001, 300106, 'Radio 1', 1),
	(30010602, 1, 3001, 300106, 'Radio 2', 2),
	(30010603, 1, 3001, 300106, 'Radio 3', 3),
	(30010701, 1, 3001, 300107, 'Checkbox 1', 1),
	(30010702, 1, 3001, 300107, 'Checkbox 2', 2),
	(30010703, 1, 3001, 300107, 'Checkbox 3', 3),
	(30010801, 1, 3001, 300108, 'Dropdown 1', 1),
	(30010802, 1, 3001, 300108, 'Dropdown 2', 2)
   ,(30010803, 1, 3001, 300108, 'Dropdown 3', 3)
	
   ,(20020401, 1, 2002, 200204, 'Radio 1', 1)
   ,(20020402, 1, 2002, 200204, 'Radio 2', 1)
   ,(20020403, 1, 2002, 200204, 'Radio 3', 1)
;
insert into data
	(row_id, column_id, table_id, value) values
	(1001, 1, 1001, '0001'),
	(1001, 2, 1001, 'Apple'),
	(1001, 3, 1001, '1000'),
	(1001, 4, 1001, '1'),
	(1002, 1, 1001, '0002'),
	(1002, 2, 1001, 'Banana'),
	(1002, 3, 1001, '120'),
	(1002, 4, 1001, '3'),
	(1003, 1, 1001, '0003'),
	(1003, 2, 1001, 'Orange'),
	(1003, 3, 1001, '200'),
	(1003, 4, 1001, '2'),
	-- 全フィールド
	(100301, 100301, 1003, 'value 1'),
	(100301, 100302, 1003, 'value \n2'),
	(100301, 100303, 1003, '<font color=red>value 3</font>'),
	(100301, 100304, 1003, '4'),
	(100301, 100305, 1003, '5'),
	(100301, 100306, 1003, '10030601'),
	(100301, 100307, 1003, '[10030701,10030704]'),/*04:存在しないものを参照*/
	(100301, 100308, 1003, null),/*未選択*/
	(100301, 100309, 1003, '2014-01-09'),
	(100301, 100310, 1003, '00:00:10'),
	(100301, 100311, 1003, '2014-01-11 00:00:11'),
	(100301, 100312, 1003, '[{"filekey":"1003/100312/filename12.txt","contentType":"text/plain","fileName":"filename12.txt"}]'),
	(100301, 100313, 1003, 'value13@sample.com'),
	(100301, 100315, 1003, '{"value":"user15","key":1010}'),
	(100301, 100316, 1003, '16'),
	(100301, 100317, 1003, '1001'),
	(100301, 100318, 1003, '2014-01-18 00:00:18'),
	(100301, 100319, 1003, '1002'),
	(100301, 100320, 1003, '2014-01-20 00:00:20'),
	(100301, 100324, 1003, '{"value":15,"key":1010}'),
	-- 全フィールド（空値）
	(100302, 100301, 1003, null),
	(100302, 100302, 1003, null),
	(100302, 100303, 1003, null),
	(100302, 100304, 1003, null),
	(100302, 100305, 1003, null),
	(100302, 100306, 1003, null), /*radio*/
	(100302, 100307, 1003, '[]'),/*未選択*/
	(100302, 100308, 1003, null),/*未選択*/
	(100302, 100309, 1003, null),
	(100302, 100310, 1003, null),
	(100302, 100311, 1003, null),
	(100302, 100312, 1003, '[]'),
	(100302, 100313, 1003, null),
	(100302, 100315, 1003, null),
	(100302, 100316, 1003, '2'),
	(100302, 100317, 1003, '1001'),
	(100302, 100318, 1003, '2014-01-18 00:00:18'),
	(100302, 100319, 1003, '1002'),
	(100302, 100320, 1003, '2014-01-20 00:00:20'),
	(100302, 100324, 1003, null)
	-- 20件データ
/*
	(1004, 1024, 1007, 'row 1'),
	(1004, 100702, 1007, '1'),
	(1005, 1024, 1007, 'row 2'),
	(1005, 100702, 1007, '2'),
	(1006, 1024, 1007, 'row 3'),
	(1006, 100702, 1007, '3'),
	(1007, 1024, 1007, 'row 4'),
	(1007, 100702, 1007, '4'),
	(1008, 1024, 1007, 'row 5'),
	(1008, 100702, 1007, '5'),
	(1009, 1024, 1007, 'row 6'),
	(1009, 100702, 1007, '6'),
	(1010, 1024, 1007, 'row 7'),
	(1010, 100702, 1007, '7'),
	(1011, 1024, 1007, 'row 8'),
	(1011, 100702, 1007, '8'),
	(1012, 1024, 1007, 'row 9'),
	(1012, 100702, 1007, '9'),
	(1013, 1024, 1007, 'row 10'),
	(1013, 100702, 1007, '10'),
	(1014, 1024, 1007, 'row 11'),
	(1014, 100702, 1007, '11'),
	(1015, 1024, 1007, 'row 12'),
	(1015, 100702, 1007, '12'),
	(1016, 1024, 1007, 'row 13'),
	(1016, 100702, 1007, '13'),
	(1017, 1024, 1007, 'row 14'),
	(1017, 100702, 1007, '14'),
	(1018, 1024, 1007, 'row 15'),
	(1018, 100702, 1007, '15'),
	(1019, 1024, 1007, 'row 16'),
	(1019, 100702, 1007, '16'),
	(1020, 1024, 1007, 'row 17'),
	(1020, 100702, 1007, '17'),
	(1021, 1024, 1007, 'row 18'),
	(1021, 100702, 1007, '18')	,
	(1022, 1024, 1007, 'row 19'),
	(1022, 100702, 1007, '19'),
	(1023, 1024, 1007, 'row 20'),
	(1023, 100702, 1007, '20')*/
;
insert into `row`
	(id, table_id, row_no) values
	(1001, 1001, 1),
	(1002, 1001, 2),
	(1003, 1001, 3),
	(100301, 1003, 1),
	(100302, 1003, 2),
	--
	(1004, 1007, 1),
	(1005, 1007, 2),
	(1006, 1007, 3),
	(1007, 1007, 4),
	(1008, 1007, 5),
	(1009, 1007, 6),
	(1010, 1007, 7),
	(1011, 1007, 8),
	(1012, 1007, 9),
	(1013, 1007, 10),
	(1014, 1007, 11),
	(1015, 1007, 12),
	(1016, 1007, 13),
	(1017, 1007, 14),
	(1018, 1007, 15),
	(1019, 1007, 16),
	(1020, 1007, 17),
	(1021, 1007, 18),
	(1022, 1007, 19),
	(1023, 1007, 20);
;
insert into indexes
	(id, column_id, string_value, integer_value, number_value, date_value, time_value, datetime_value, row_id) values
	-- table 1001
	(null,	1,		'0001',       null,          null,      null,       null,       null,           1001),
	(null,	2,		'Apple',      null,          null,      null,       null,       null,           1001),
	(null,	3,		null,         null,          1000,		null,       null,       null,           1001),
	(null,	4,		null,         1,          	null,		null,       null,       null,           1001),
	(null,	1,		'0002',       null,          null,      null,       null,       null,           1002),
	(null,	2,		'Banana',     null,          null,		null,       null,       null,           1002),
	(null,	3,		null,		null,			120,		null,       null,       null,           1002),
	(null,	4,		null,         3,          	null,		null,       null,       null,           1002),
	(null,	1,		'0003',       null,          null,      null,       null,       null,           1003),
	(null,	2,		'Orange',     null,          null,		null,       null,       null,           1003),
	(null,	3,  	null,		null,			200,		null,       null,       null,           1003),
	(null,	4,		null,         2,          	null,		null,       null,       null,           1003),
	-- table 1003
	(null,	100301,  'value 1',	null,			null,		null,       null,       null,           100301),
	(null,	100302,  'value \n2',	null,			null,		null,       null,       null,           100301),
	(null,	100303,  'value 3',	null,			null,		null,       null,       null,           100301),
	(null,	100304,	null,   	null,			4,			null,       null,       null,           100301),
	(null,	100305,	null,   	null,			5,			null,       null,       null,           100301),
	(null,	100306,	null,   	10030601,			null,			null,       null,       null,           100301),
	(null,	100307,	null,   	10030701/**/,	null,		null,       null,       null,           100301),
	(null,	100307,	null,   	10030704/*存在しないものを参照*/,			null,			null,       null,       null,           100301),
	(null,	100308,	null,   	null/*未選択*/,			null,			null,       null,       null,           100301),
	(null,	100309,	null,   	null,			null,		'2014-01-09',       null,       null,           100301),
	(null,	100310,	null,   	null,			null,		null,       '00:00:10',       null,       100301),
	(null,	100311,	null,   	null,			null,		null,       null,       '2014-01-11 00:00:11',       100301),
	(null,	100312,	'filename12.txt',   	null,			null,		null,       null,       null,       100301),
	(null,	100313,	'value13@sample.com',   	null,		null,		null,       null,       null,       100301),
	(null,	100315,	'user15',   null,			null,		null,       null,       null,       100301),
	(null,	100316,	null,   	16,				null,		null,       null,       null,       100301),
	(null,	100317,	null,   	1001,			null,		null,       null,       null,       100301),
	(null,	100318,	null,   	null,			null,		null,       null,       '2014-01-18 00:00:18',       100301),
	(null,	100319,	null,   	1002,			null,		null,       null,       null,       100301),
	(null,	100320,	null,   	null,			null,		null,       null,       '2014-01-20 00:00:20',       100301),
	(null,	100324,	null,   	15,			null,		null,       null,       null,       100301), /*lookupの数値バージョン*/
	-- table 1003 (row 2)
	(null,	100301, null,		null,			null,			null,       null,       null,           100302),
	(null,	100302, null,		null,			null,			null,       null,       null,           100302),
	(null,	100303, null,		null,			null,			null,       null,       null,           100302),
	(null,	100304,	null,   	null,			null,			null,       null,       null,           100302),
	(null,	100305,	null,   	null,			null,			null,       null,       null,           100302),
	(null,	100306,	null,   	null,			null,			null,       null,       null,           100302),
	(null,	100307,	null,   	null,			null,			null,       null,       null,           100302),
	(null,	100308,	null,   	null,			null,			null,       null,       null,           100302),
	(null,	100309,	null,   	null,			null,		null,       null,       	null,           100302),
	(null,	100310,	null,   	null,			null,		null,       null,       	null,       100302),
	(null,	100311,	null,   	null,			null,		null,       null,       	null,       100302),
	(null,	100312,	null,   	null,			null,		null,       	null,       null,       100302),
	(null,	100313,	null,   	null,			null,		null,       null,       null,       100302),
	(null,	100315,	null,   	null,			null,		null,       null,       null,       100302),
	(null,	100316,	null,   	2,			null,			null,       null,       null,       100302),
	(null,	100317,	null,   	1001,			null,		null,       null,       null,       100302),
	(null,	100318,	null,   	null,			null,		null,       null,       '2014-01-18 00:00:18',       100302),
	(null,	100319,	null,   	1002,			null,		null,       null,       null,       100302),
	(null,	100320,	null,   	null,			null,		null,       null,       '2014-01-20 00:00:20',       100302),
	(null,	100324,	null,   	null,			null,		null,       null,       null,       100302), /*lookupの数値バージョン*/
	-- table 1007
	(null,	1024,	'row 01',	null,	null,         null,       null,       null,		1004	),
	(null,	1024,	'row 02',	null,	null,         null,       null,       null,		1005	),
	(null,	1024,	'row 03',	null,	null,         null,       null,       null,		1006	),
	(null,	1024,	'row 04',	null,	null,         null,       null,       null,		1004	),
	(null,	1024,	'row 05',	null,	null,         null,       null,       null,		1005	),
	(null,	1024,	'row 06',	null,   null,         null,       null,       null,		1006	),
	(null,	1024,	'row 07',	null,   null,         null,       null,       null,		1007	),
	(null,	1024,	'row 08',	null,   null,         null,       null,       null,		1008	),
	(null,	1024,	'row 09',	null,   null,         null,       null,       null,		1009	),
	(null,	1024,	'row 10',	null,	null,         null,       null,       null,		1010	),
	(null,	1024,	'row 11',     null,          null,         null,       null,       null,		1011	),
	(null,	1024,	'row 12',     null,          null,         null,       null,       null,		1012	),
	(null,	1024,	'row 13',     null,          null,         null,       null,       null,		1013	),
	(null,	1024,	'row 14',     null,          null,         null,       null,       null,		1014	),
	(null,	1024,	'row 15',     null,          null,         null,       null,       null,		1015	),
	(null,	1024,	'row 16',     null,          null,         null,       null,       null,		1016	),
	(null,	1024,	'row 17',     null,          null,         null,       null,       null,		1017	),
	(null,	1024,	'row 18',     null,          null,         null,       null,       null,		1018	),
	(null,	1024,	'row 19',     null,          null,         null,       null,       null,		1019	),
	(null,	1024,	'row 20',     null,          null,         null,       null,       null,		1020	),
	(null,	1024,	'row 21',     null,          null,         null,       null,       null,		1021	),
	(null,	1024,	'row 22',     null,          null,         null,       null,       null,		1022	),
	(null,	1024,	'row 23',     null,          null,         null,       null,       null,		1023	)
;

insert into `icon` (id, filekey, seq) values
    (10004, 'icon04.png', 4)
   ,(10003, 'icon03.png', 3)
   ,(10002, 'icon02.png', 2)
   ,(10001, 'icon01.png', 1)
; 

insert into view_column (table_id, revision, column_id, `seq`) values
    (2002, 1, 200204, 3)
   ,(2002, 1, 200203, 2)
   ,(2002, 1, 200201, 1)
   ,(3001, 1, 300104, 4)	
   ,(3001, 1, 300103, 3)
   ,(3001, 1, 300102, 2)
   ,(3001, 1, 300101, 1)
;

insert into row_no (table_id, row_no) select distinct id, 0 from table_def
;
