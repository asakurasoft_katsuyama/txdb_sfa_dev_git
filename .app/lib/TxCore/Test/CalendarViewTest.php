<?php
namespace TxCore\Math\Test;


use TxCore\CalendarSettings;
use TxCore\Column;
use TxCore\Test\BaseTestCase;

use TxCore\CalendarView;

class CalendarViewTest extends BaseTestCase
{
    public function setUp()
    {
        parent::setUpDB(__DIR__.'/CalendarViewTest.sql');
    }

    public function test()
    {
        $user = $this->createUser(1);
        $user2 = $this->createUser(2);

        $table = $this->mockUpTable('Calendar');
        $column0 = $table->addColumn(Column::TEXT);
        $column0->setLabel('Title');
        $column1 = $table->addColumn(Column::DATE);
        $startTime = $table->addColumn(Column::TIME);
        $startTime->setLabel('Start time');
        $endTime = $table->addColumn(Column::TIME);
        $endTime->setLabel('End time');
        $column5 = $table->addColumn(Column::USER_SELECTOR);
        $column5->setLabel('User');

        $setting = new CalendarSettings($table);
        $setting->setTitleColumnId($column0->getId());
        $setting->setDateColumnId($column1->getId());
        $setting->setEndTimeColumnId($endTime->getId());
        $setting->setStartTimeColumnId($startTime->getId());
        $setting->setUserColumnId($column5->getId());
        $table->setCalendarEnabled(true);
        $table->setCalendarSettings($setting);
        $table->create($user);

        $row = $table->addRow();
        $row->setValue($column0, 'MTG');
        $row->setValue($column1, '2015-01-01');
        $row->setValue($startTime, '10:00');
        $row->setValue($endTime, '10:30');
        $row->setValue($column5, array($user));
        $table->insertRow($row, $user);

        $calendar = CalendarView::search($table, array(), $user, 0, 10, '2015-01-01');
        $this->assertSame('2015-01-01', $calendar->getStartDate());
        $this->assertSame('2015-01-08', $calendar->getNextStartDate());
        $this->assertSame('2014-12-25', $calendar->getPrevStartDate());
        $this->assertSame(2, $calendar->getUserCount());
        $this->assertSame(0, $calendar->getUserOffset());
        $this->assertSame(2, sizeof($calendar->getUserEntries()));

        $userEntries = $calendar->getUserEntries();
        $this->assertSame($user->getId(), $userEntries[0]->getUser()->getId());

        $dateEntries1 = $userEntries[0]->getDateEntries();
        $this->assertSame(7, sizeof($dateEntries1));
        $this->assertSame('2015-01-01', $dateEntries1[0]->getDate());

        $items1 = $dateEntries1[0]->getItems();
        $this->assertSame(1, sizeof($items1));
        $this->assertSame('MTG', $items1[0]->getTitle());
        $this->assertSame('10:00', $items1[0]->getStartTime());
        $this->assertSame('10:30', $items1[0]->getEndTime());
        $this->assertSame($row->getId(), $items1[0]->getRowId());
        $this->assertSame('2015-01-07', $dateEntries1[6]->getDate());
        $this->assertSame($user2->getId(), $userEntries[1]->getUser()->getId());
    }
}
