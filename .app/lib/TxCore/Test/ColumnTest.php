<?php
namespace TxCore\Test;

use TxCore\TxCoreException;

class ColumnTest extends BaseTestCase
{
	public function setUp()
	{
        parent::setUpDB(__DIR__.'/EngineTest.sql');
	}

	public function testGetDefaultValue_初期値が返ること()
	{
		$table  = $this->mockUpTableFully();
		$table->getColumn(0)->setDefaultValue('value 1');
		$table->getColumn(1)->setDefaultValue('value 2');
		$table->getColumn(2)->setDefaultValue('value 3');
		$table->getColumn(3)->setDefaultValue('120');
		$table->getColumn(5)->setDefaultValue($table->getColumn(5)->getOption(0));
		$table->getColumn(6)->setDefaultValue(array($table->getColumn(6)->getOption(0), $table->getColumn(6)->getOption(1)));
		$table->getColumn(7)->setDefaultValue($table->getColumn(7)->getOption(0));
		$table->getColumn(8)->setDefaultValue('2014-01-01');
		$table->getColumn(9)->setDefaultValue('10:10');
		$table->getColumn(10)->setDefaultValue('2014-01-01 10:10');
		$table->getColumn(12)->setDefaultValue('sample@sample');
		$table->create($this->target->getUser(1001));
	
		$table = $this->target->getTable($table->getId());	// Relaod
		$this->assertIdentical('value 1', $table->getColumn(0)->getDefaultValue());
		$this->assertIdentical('value 2', $table->getColumn(1)->getDefaultValue());
		$this->assertIdentical('value 3', $table->getColumn(2)->getDefaultValue());
		$this->assertIdentical('120', $table->getColumn(3)->getDefaultValue());
		$this->assertNotNull($table->getColumn(5)->getDefaultValue());
		$this->assertIdentical($table->getColumn(5)->getOption(0)->getId(), $table->getColumn(5)->getDefaultValue()->getId());
		$this->assertIdentical(2, count($table->getColumn(6)->getDefaultValue()));
		$this->assertNotNull($table->getColumn(7)->getDefaultValue());
		$this->assertIdentical($table->getColumn(7)->getOption(0)->getId(), $table->getColumn(7)->getDefaultValue()->getId());
		$this->assertIdentical('2014-01-01', $table->getColumn(8)->getDefaultValue());
		$this->assertIdentical('10:10', $table->getColumn(9)->getDefaultValue());
		$this->assertIdentical('2014-01-01 10:10', $table->getColumn(10)->getDefaultValue());
		$this->assertIdentical('sample@sample', $table->getColumn(12)->getDefaultValue());
	}	
	
	public function testGetOptions_オプションも持たないカラムは例外()
	{
		$by = $this->target->getUser(1002);
		$table = $this->mockUpTable();
		$table->create($by);
		try {
			$options = $table->getColumn(0)->getOptions();
			$this->fail('Should not be successfull');
		} catch (TxCoreException $e) {
			$this->assertIdentical(TxCoreException::INTERNAL, $e->getCode());
		}
	}
	
}
