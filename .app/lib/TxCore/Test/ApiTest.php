<?php
namespace TxCore\Test;

use TxCore\Column;
use TxCore\Criteria;
use TxCore\Engine;
use TxCore\Option;
use TxCore\Row;
use TxCore\Table;
use TxCore\User;

/**
 * [[ このテストケースは、HTTP経由でテストするので config-ut.ini と config.ini の接続先を同じにしてください。
 * 	また、テスト用のゴミデータでcommitがかかるので気をつけてください ]]
 */
class ApiTest extends BaseTestCase
{
	public function setUp()
	{
		parent::setUpDB(__DIR__.'/ApiTest.sql');
	}

	private function getApiHost()
	{
		return get_config('app', 'myhost', 'https://localhost');
	}
	
	private function getFilePath($path)
	{
		return __DIR__ . '/ApiTest/' . $path;
	}
	
	private function search($params) 
	{
		$url = $this->getApiHost().'/api/data/search.json?'. http_build_query($params);
		echo ("Request : $url\n");
		
		$p = curl_init();
		curl_setopt($p, CURLOPT_URL, $url);
		curl_setopt($p, CURLOPT_POST, false);
		curl_setopt($p, CURLOPT_HEADER, true);
		curl_setopt($p, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($p, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($p, CURLOPT_SSL_VERIFYPEER, FALSE);  // オレオレ証明書対策
		curl_setopt($p, CURLOPT_SSL_VERIFYHOST, FALSE);  //
		
		$response = curl_exec($p);
		$hlength  = curl_getinfo($p, CURLINFO_HEADER_SIZE);
		$httpCode = curl_getinfo($p, CURLINFO_HTTP_CODE);
		$body     = substr($response, $hlength);
		
		curl_close($p);
		
		return array($httpCode, json_decode($body));
	}
	
	private function upload($loginId, $params, $file)
	{
		$url = $this->getApiHost().'/api/file/upload.json';
		return $this->post($url, $params, $this->createHeaders($loginId), array($file));
	}
	
	private function upload_with_multifile($loginId, $params, $files) // always occurs bad api request.
	{
		$url = $this->getApiHost().'/api/file/upload.json';
		return $this->post($url, $params, $this->createHeaders($loginId), $files);
	}
	
	private function upload_with_zerofile($loginId, $params) // always occurs bad api request.
	{
		$url = $this->getApiHost().'/api/file/upload.json';
		return $this->post($url, $params, $this->createHeaders($loginId), array());
	}
	
	private function insert($loginId, $params)
	{
		$url = $this->getApiHost().'/api/data/insert.json';
		return $this->post($url, http_build_query($params), $this->createHeaders($loginId));
	}
	
	private function update($loginId, $params)
	{
		$url = $this->getApiHost().'/api/data/update.json';
		return $this->post($url, http_build_query($params), $this->createHeaders($loginId));
	}
	
	private function delete($loginId, $params)
	{
		$url = $this->getApiHost().'/api/data/delete.json';
		return $this->post($url, http_build_query($params), $this->createHeaders($loginId));
	}
	
	private function createHeaders($loginId)
	{
		$headers = array();
		if ($loginId) {
			$headers[] = 'X-TsukaeruCloudDB-User: '.base64_encode($loginId);
		}
		return $headers;
	}
	
	private function post($url, $data, $headers = array(), $files = null) {
	
		// 通信の設定
		$p = curl_init();                                 // 初期化
		curl_setopt($p, CURLOPT_URL, $url);               // URLをセットする。
		curl_setopt($p, CURLOPT_POST, true);
		curl_setopt($p, CURLOPT_HEADER, true);
		curl_setopt($p, CURLOPT_RETURNTRANSFER, true);       // レスポンスをすぐ出力させず、文字列として返させる。
		curl_setopt($p, CURLOPT_CONNECTTIMEOUT, 10);
		curl_setopt($p, CURLOPT_SSL_VERIFYPEER, FALSE);  // オレオレ証明書対策
		curl_setopt($p, CURLOPT_SSL_VERIFYHOST, FALSE);  //
	
		if ($files === null) {
			curl_setopt($p, CURLOPT_POSTFIELDS, $data);
			curl_setopt($p, CURLOPT_HTTPHEADER, $headers);
		} else {
			$this->curl_custom_postfields($p, $data, $files, $headers);
		}

		$response = curl_exec($p);
		$hlength  = curl_getinfo($p, CURLINFO_HEADER_SIZE);
		$httpCode = curl_getinfo($p, CURLINFO_HTTP_CODE);
		$body     = substr($response, $hlength);
		
		curl_close($p);
		
		return array($httpCode, json_decode($body));
	}
	
	/**
	 * マルチパートボディを組み立てて文字列としてセットする。
	 *
	 * @param resource $ch cURLリソース
	 * @param array $assoc 「送信する名前 => 送信する値」の形の連想配列
	 * @param array $files 「送信する名前 => ファイルパス」の形の連想配列
	 * @return bool 成功 or 失敗
	 */
	private function curl_custom_postfields($ch, array $assoc = array(), array $files = array(), array $headers = array()) {
		static $disallow = array("\0", "\"", "\r", "\n");
		$body = array();
		foreach ($assoc as $k => $v) {
			$k = str_replace($disallow, "_", $k);
			$body[] = implode("\r\n", array(
					"Content-Disposition: form-data; name=\"{$k}\"",
					"",
					filter_var($v),
			));
		}
		foreach ($files as $k => $f) {
			switch (true) {
				case false === $v = $f['path']:
				case !is_file($v):
				case !is_readable($v):
					continue; // return false や throw new InvalidArgumentException もアリ
			}
			$data = file_get_contents($v);
			$v = call_user_func("end", explode(DIRECTORY_SEPARATOR, $v));
			list($k, $v) = str_replace($disallow, "_", array($k, $v));
			$body[] = implode("\r\n", array(
					"Content-Disposition: form-data; name=\"{$f['param_name']}\"; filename=\"{$f['name']}\"",
					"Content-Type: {$f['type']}",
					"",
					$data,
			));
		}
		do {
			$boundary = "---------------------" . md5(mt_rand() . microtime());
		} while (preg_grep("/{$boundary}/", $body));
		array_walk($body, function (&$part) use ($boundary) {
			$part = "--{$boundary}\r\n{$part}";
		});
		$body[] = "--{$boundary}--";
		$body[] = "";
		return curl_setopt_array($ch, array(
				CURLOPT_POSTFIELDS => implode("\r\n", $body),
				CURLOPT_HTTPHEADER => array_merge($headers, array(
// 						"Expect: 100-continue",
						"Content-Type: multipart/form-data; boundary={$boundary}",
				)),
		));
	}

	public function testSearch_ａｐｉキーなし()
	{
		list($staus, $json) = $this->search(array('table'=>12123));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('api_key は必須です。', $json->errors[0]->message);
		$this->assertIdentical('api_key', $json->errors[0]->context);
	}

	public function testSearch_テーブルなし()
	{
		list($staus, $json) = $this->search(array('api_key'=>'xxxx'));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('table は必須です。', $json->errors[0]->message);
		$this->assertIdentical('table', $json->errors[0]->context);
	}

	public function testSearch_テーブルが存在しない()
	{
		list($staus, $json) = $this->search(array('table'=>'0', 'api_key'=>'xxxx'));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('DB 0 が存在しません。', $json->errors[0]->message);
		$this->assertIdentical('table', $json->errors[0]->context);
	}

	public function testSearch_ａｐｉキー不正()
	{
		$table = $this->getTable();

		list($staus, $json) = $this->search(array('table'=>$table->getId(), 'api_key'=>'xxxx'));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('不正な api_key です。', $json->errors[0]->message);
		$this->assertIdentical('api_key', $json->errors[0]->context);
	}

	public function testSearch_ａｐｉ非公開()
	{
		$table = $this->getTable(false);

		list($staus, $json) = $this->search(array('table'=>$table->getId(), 'api_key'=>'xxxx'));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('DB '.$table->getId().' が存在しません。', $json->errors[0]->message);
	}

	public function testSearch_カラム指定なし()
	{
		$table = $this->getTable();

		list($staus, $json) = $this->search(array('table'=>$table->getId(), 'api_key'=>$table->getApiKey()));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('columns は必須です。', $json->errors[0]->message);
		$this->assertIdentical('columns', $json->errors[0]->context);
	}

	public function testSearch_カラムが存在しない()
	{
		$table = $this->getTable();

		list($staus, $json) = $this->search(array('table'=>$table->getId(), 'api_key'=>$table->getApiKey(), 'columns' => "x".$table->getColumn(0)->getCode()));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('フィールド x'.$table->getColumn(0)->getCode().' が存在しません。', $json->errors[0]->message);
		$this->assertIdentical('columns', $json->errors[0]->context);
	}

	public function testSearch_リスト指定できないカラム()
	{
		$table = $this->getTable();
		$relationalCols = $table->getColumns(array(@type=>Column::RELATIONAL));

		list($staus, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => $relationalCols[0]->getCode()
		));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
//		$this->assertIdentical('関連レコード一覧 はリスト指定できません。', $json->errors[0]->message);
		$this->assertIdentical('関係データ一覧 はリスト指定できません。', $json->errors[0]->message); // フィールド名変更
		$this->assertIdentical('columns', $json->errors[0]->context);
	}

	public function testSearch_オフセット不正()
	{
		$table = $this->getTable();

		list($staus, $json) = $this->search(array('table'=>$table->getId(), 'api_key'=>$table->getApiKey(), 'columns' => $table->getColumn(0)->getCode(), 'offset' => 'x'));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('offset の形式が不正です。', $json->errors[0]->message);
		$this->assertIdentical('offset', $json->errors[0]->context);
	}

	public function testSearch_リミット不正()
	{
		$table = $this->getTable();

		list($staus, $json) = $this->search(array('table'=>$table->getId(), 'api_key'=>$table->getApiKey(), 'columns' => $table->getColumn(0)->getCode(), 'limit' => '-1'));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('limit の形式が不正です。', $json->errors[0]->message);
		$this->assertIdentical('limit', $json->errors[0]->context);
	}

	public function testSearch_リミットが１００を超える()
	{
		$table = $this->getTable();

		list($staus, $json) = $this->search(array('table'=>$table->getId(), 'api_key'=>$table->getApiKey(), 'columns' => $table->getColumn(0)->getCode(), 'limit' => '101'));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('limit には、最大100まで指定可能です。', $json->errors[0]->message);
		$this->assertIdentical('limit', $json->errors[0]->context);
	}

	public function testSearch_検索条件不正１()
	{
		$table = $this->getTable();

		list($staus, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => $table->getColumn(0)->getCode(),
			'conditions[0][column]' => $table->getColumn(0)->getCode()
		));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('条件の組み合わせが不正です。', $json->errors[0]->message);
		$this->assertIdentical('conditions[0]', $json->errors[0]->context);
	}

	public function testSearch_オペレータ不正()
	{
		$table = $this->getTable();

		list($staus, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => $table->getColumn(0)->getCode(),
			'conditions[0][column]' => $table->getColumn(0)->getCode(),
			'conditions[0][operator]' => 'x',
			'conditions[0][value]' => 1
		));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('operator の形式が不正です。', $json->errors[0]->message);
		$this->assertIdentical('conditions[0][operator]', $json->errors[0]->context);
	}

	public function testSearch_条件カラムが存在しない()
	{
		$table = $this->getTable();

		list($staus, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => $table->getColumn(0)->getCode(),
			'conditions[0][column]' => $table->getColumn(0)->getCode()."x",
			'conditions[0][operator]' => '=',
			'conditions[0][value]' => 1
		));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('フィールド '.$table->getColumn(0)->getCode()."x".' が存在しません。', $json->errors[0]->message);
		$this->assertIdentical('conditions[0][column]', $json->errors[0]->context);
	}

	public function testSearch_結合条件が不正()
	{
		$table = $this->getTable();

		list($staus, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => $table->getColumn(0)->getCode(),
			'boolOperator' => 'xx'
		));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('boolOperator の形式が不正です。', $json->errors[0]->message);
		$this->assertIdentical('boolOperator', $json->errors[0]->context);
	}


	public function testSearch_条件のインデックスが１からスタート()
	{
		$table = $this->getTable();

		list($staus, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => $table->getColumn(0)->getCode(),
			'conditions[1][column]' => $table->getColumn(0)->getCode(),
			'conditions[1][operator]' => '=',
			'conditions[1][value]' => 1
		));
		$this->assertIdentical(200, $staus);
		$this->assertIdentical(false, isset($json->errors));
	}

	public function testSearch_ソートのカラムが存在しない()
	{
		$table = $this->getTable();

		list($staus, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => $table->getColumn(0)->getCode(),
			'sorts[0][column]' => $table->getColumn(0)->getCode()."x",
		));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('フィールド '.$table->getColumn(0)->getCode()."x".' が存在しません。', $json->errors[0]->message);
		$this->assertIdentical('sorts[0][column]', $json->errors[0]->context);
	}

	public function testSearch_ソートの組み合わせがエラー()
	{
		$table = $this->getTable();

		list($staus, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => $table->getColumn(0)->getCode(),
			'sorts' => 'x',
		));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('sorts の形式が不正です。', $json->errors[0]->message);
		$this->assertIdentical('sorts[0]', $json->errors[0]->context);
	}

	public function testSearch_ソート指定できないカラム()
	{
		$table = $this->getTable();
		$labelCols = $table->getColumns(array(@type=>Column::RELATIONAL));

		list($staus, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => $table->getColumn(0)->getCode(),
			'sorts[0][column]' => $labelCols[0]->getCode(),
		));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
//		$this->assertIdentical('関連レコード はソート指定できません。', $json->errors[0]->message);
		$this->assertIdentical($labelCols[0]->getCode().' はソート指定できません。', $json->errors[0]->message);
		$this->assertIdentical('sorts[0][column]', $json->errors[0]->context);
	}

	public function testSearch_使えないオペレータ()
	{
		$table = $this->getTable();
		$radioCols = $table->getColumns(array(@type=>Column::RADIO));

		list($staus, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => $table->getColumn(0)->getCode(),
			'conditions[0][column]' => $radioCols[0]->getCode(),
			'conditions[0][operator]' => '=',	// ★=は使えない
			'conditions[0][value][]' => $radioCols[0]->getOption(0)->getName()
		));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('条件の組み合わせが不正です。', $json->errors[0]->message);
		$this->assertIdentical('conditions[0]', $json->errors[0]->context);
	}

	public function testSearch_検索条件指定できないカラム()
	{
		$table = $this->getTable();
		$labelCols = $table->getColumns(array(@type=>Column::RELATIONAL));

		list($staus, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => $table->getColumn(0)->getCode(),
			'conditions[0][column]' => $labelCols[0]->getCode(),
			'conditions[0][operator]' => Criteria::EQUALS,
			'conditions[0][value]' => 'x',
		));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
//		$this->assertIdentical('関連レコード一覧 は検索条件指定できません。', $json->errors[0]->message);
		$this->assertIdentical('関係データ一覧 は検索条件指定できません。', $json->errors[0]->message); // フィールド名変更
		$this->assertIdentical('conditions[0][column]', $json->errors[0]->context);
	}

	public function testSearch_オプションが存在しない()
	{
		$table = $this->getTable();
		$radioCols = $table->getColumns(array(@type=>Column::RADIO));

		list($staus, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => $table->getColumn(0)->getCode(),
			'conditions[0][column]' => $radioCols[0]->getCode(),
			'conditions[0][operator]' => 'contains',
			'conditions[0][value][]' => '123123'	// ★登録されていない
		));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('オプション 123123 が存在しません。', $json->errors[0]->message);
		$this->assertIdentical('conditions[0][value]', $json->errors[0]->context);
	}


	public function testSearch_ユーザーが存在しない()
	{
		$table = $this->getTable();
		$createdByColumns = $table->getColumns(array(@type=>Column::CREATED_BY));

		list ($staus, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => $table->getColumn(0)->getCode(),
			'conditions[0][column]' => $createdByColumns[0]->getCode(),
			'conditions[0][operator]' => 'contains',
			'conditions[0][value][]' => 'xxxx'	// ★登録されていない
		));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('ユーザー xxxx が存在しません。', $json->errors[0]->message);
		$this->assertIdentical('conditions[0][value]', $json->errors[0]->context);
	}

	public function testSearch_クライテリアのエラー()
	{
		$table = $this->getTable();

		list($staus, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => $table->getColumn(0)->getCode(),
			'conditions[0][column]' => $table->getRownum()->getCode(),
			'conditions[0][operator]' => '=',
			'conditions[0][value]' => 'aaaa'	// ★数字でない！
		));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
//		$this->assertIdentical('レコード番号は数字で入力してください。', $json->errors[0]->message);
		$this->assertIdentical($table->getRownum()->getLabel().'は数字で入力してください。', $json->errors[0]->message); // エラーメッセージの内容が変更
		$this->assertIdentical('conditions', $json->errors[0]->context);
	}

	public function testSearch_正常系()
	{
		$table = $this->getTable();
		$radioCols = $table->getColumns(array(@type=>Column::RADIO));
		$createdByCols = $table->getColumns(array(@type=>Column::CREATED_BY));

		list($staus, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => $table->getColumn(0)->getCode(),
			'conditions[0][column]' => $radioCols[0]->getCode(),
			'conditions[0][operator]' => 'contains',
			'conditions[0][value][]' => $radioCols[0]->getOption(0)->getName(),
			'conditions[0][value][]' => $radioCols[0]->getOption(1)->getName(),
			'conditions[1][column]' => $createdByCols[0]->getCode(),
			'conditions[1][operator]' => 'contains',
			'conditions[1][value][]' => $table->getOwner()->getLoginId()
		));

		$this->assertIdentical(200, $staus);
// 		$this->assertIdentical(1, count($json->errors));
// 		$this->assertIdentical('オプション 123123 が登録されていません。', $json->errors[0]->message);
// 		$this->assertIdentical('conditions[0][value]', $json->errors[0]->context);
	}

	public function testSearch_正常系_レスポンス()
	{
		$table = $this->getTable();
		$row = $this->findTestableRow($table);

		$textCols = $table->getColumns(array('type'=>Column::TEXT));
		$radioCols = $table->getColumns(array('type'=>Column::RADIO));
		$createdByCols = $table->getColumns(array('type'=>Column::CREATED_BY));
		$checkboxCols = $table->getColumns(array('type'=>Column::CHECKBOX));
		$fileCols = $table->getColumns(array('type'=>Column::FILE));
		$columns = array(
			$textCols[0],
			$radioCols[0],
			$createdByCols[0],
			$checkboxCols[0],
			$fileCols[0],
		);	/** @var $columns Column[] */

		list ($status, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => join(',', array_map(function(Column $t) {return $t->getCode();}, $columns)),
			'conditions[0][column]' => $table->getRownum()->getCode(),
			'conditions[0][operator]' => '=',
			'conditions[0][value]' => $row->getValue($table->getRownum()),
		));

		$this->assertIdentical(200, $status);
		$this->assertIdentical(1, $json->totalRowCount);
		$this->assertIdentical(0, $json->offset);
		$this->assertIdentical(20, $json->rowLimit);
		$this->assertIdentical(1, $json->rowCount);
		$this->assertIdentical(count($columns), count($json->columns));
		$this->assertIdentical($columns[0]->getId(), $json->columns[0]->id);
		$this->assertIdentical($columns[0]->getType(), $json->columns[0]->type);
		$this->assertIdentical($columns[0]->getLabel(), $json->columns[0]->label);
		$this->assertIdentical($columns[0]->getCode(), $json->columns[0]->code);
		$this->assertIdentical(1, count($json->rows));
		$this->assertIdentical($row->getId(), $json->rows[0]->id);
		$this->assertIdentical(count($columns), count((array)$json->rows[0]->values));
		$this->assertIdentical($row->getValue($columns[0]), $json->rows[0]->values->{$columns[0]->getCode()});
		$this->assertIdentical($row->getValue($columns[1])->getId(), $json->rows[0]->values->{$columns[1]->getCode()}->id);	// ラジオ
		$this->assertIdentical($row->getValue($columns[1])->getName(), $json->rows[0]->values->{$columns[1]->getCode()}->name);
		$this->assertIdentical($row->getValue($columns[2])->getId(), $json->rows[0]->values->{$columns[2]->getCode()}->id);	// ユーザー
		$this->assertIdentical($row->getValue($columns[2])->getScreenName(), $json->rows[0]->values->{$columns[2]->getCode()}->screenName);
		$this->assertIdentical(3, count((array)$json->rows[0]->values->{$columns[2]->getCode()}));	// その他プロパティ（パスワードとか）は非公開
		$this->assertIdentical('array', gettype($json->rows[0]->values->{$columns[3]->getCode()}));	// チェックボックス
		$this->assertIdentical('array', gettype($json->rows[0]->values->{$columns[4]->getCode()}));	// ファイル
	}


	public function testSearch_正常系_ユーザー選択()
	{
		$table = new Table();
		$table->setName('テーブル１');
		$table->setPublic(true);
		$table->setApiEnabled(true);
		$table->setIcon(Engine::factory()->uploadIcon(__DIR__.'/ApiTest.gif'));
		$column1 = $table->addColumn(Column::USER_SELECTOR);
		$column1->setLabel("ユーザー選択");
		$column1->setCode($column1->getTypeName());
		$table->setViewColumns(array($column1));
		$user = Engine::factory()->getUser(101);
		$table->create($user);

		$trunk = Engine::factory()->getTable($table->getId(), Engine::TRUNK);
		$trunk->setApiEnabled(true);
		$trunk->saveChanges($user);
		$trunk->release($user);

		$row = $table->addRow();
		$row->setValue($column1, array($user));
		$table->insertRow($row, $user);

		list ($status, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => join(',', array_map(function(Column $t) {return $t->getCode();}, $table->getColumns())),
			'conditions[0][column]' => $column1->getCode(),
			'conditions[0][operator]' => 'contains',
			'conditions[0][value][]' => $user->getLoginId(),
//			'conditions[0][value][0][code]' => $user->getLoginId(),
//			'conditions[0][value][0][type]' => 'group',
//			'conditions[0][value][0][name]' => $group->getLoginId(),
		));


		$this->assertIdentical(200, $status);
		$this->assertIdentical(array(), isset($json->errors) ? $json->errors : array());
		$this->assertIdentical(1, $json->totalRowCount);
		$this->assertIdentical(0, $json->offset);
		$this->assertIdentical(20, $json->rowLimit);
		$this->assertIdentical(1, $json->rowCount);
		$this->assertIdentical(6, count($json->columns));
		$this->assertIdentical(array('user_selector', 'rownum', 'created_by', 'created_on', 'updated_by', 'updated_on'),
			array_map(function(Column $column) { return $column->getType();}, $table->getColumns()));
		$this->assertIdentical(array('id'=>101,'code'=>'user1@sample.com','screenName' => 'test user01'),
			get_object_vars($json->rows[0]->values->{$column1->getCode()}[0]));
	}

	/**
	 * - contains : type=user でユーザーが検索できる
	 * - contains : type=group でグループ検索
	 * - contains : ユーザーとグループの複合
	 * - not contains
	 */
	public function testSearch_正常系_ユーザー選択_タイプ指定()
	{
		$user = Engine::factory()->getUser(101);
		$user2 = Engine::factory()->getUser(102);
//		$group = Engine::factory()->getGroup(11);

		$table = new Table();
		$table->setName('テーブル１');
		$table->setPublic(true);
		$table->setApiEnabled(true);
		$table->setIcon(Engine::factory()->uploadIcon(__DIR__.'/ApiTest.gif'));
		$column1 = $table->addColumn(Column::USER_SELECTOR);
		$column1->setLabel("ユーザー選択");
		$column1->setCode($column1->getTypeName());
		$table->setViewColumns(array($column1));
		$table->create($user);

		$trunk = Engine::factory()->getTable($table->getId(), Engine::TRUNK);
		$trunk->setApiEnabled(true);
		$trunk->saveChanges($user);
		$trunk->release($user);

		$row = $table->addRow();
		$row->setValue($column1, array($user));
		$row = $table->addRow();
		$row->setValue($column1, array($user2));	// group=11
		$row = $table->addRow();
		$row->setValue($column1, array());
		$table->insertRow($row, $user);


		// ユーザーで検索
		list ($status, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => join(',', array_map(function(Column $t) {return $t->getCode();}, $table->getColumns())),
			'conditions[0][column]' => $column1->getCode(),
			'conditions[0][operator]' => 'contains',
			'conditions[0][value][0][type]' => 'user',
			'conditions[0][value][0][code]' => $user->getLoginId(),
		));

		$this->assertIdentical(array(), isset($json->errors) ? $json->errors : array());
		$this->assertIdentical(200, $status);
		$this->assertIdentical(1, $json->totalRowCount);
		$this->assertIdentical(array('id'=>101,'code'=>'user1@sample.com','screenName' => 'test user01'),
			get_object_vars($json->rows[0]->values->{$column1->getCode()}[0]));


		// グループで検索
		list ($status, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => join(',', array_map(function(Column $t) {return $t->getCode();}, $table->getColumns())),
			'conditions[0][column]' => $column1->getCode(),
			'conditions[0][operator]' => 'contains',
			'conditions[0][value][1][type]' => 'group',
			'conditions[0][value][1][name]' => 'グループ１',
		));


		$this->assertIdentical(array(), isset($json->errors) ? $json->errors : array());
		$this->assertIdentical(200, $status);
		$this->assertIdentical(1, $json->totalRowCount);
		$this->assertIdentical(array('id'=>102,'code'=>'user2@sample.com','screenName' => 'test user02'),
			get_object_vars($json->rows[0]->values->{$column1->getCode()}[0]));

		// ユーザー＋グループで検索
		list ($status, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => join(',', array_map(function(Column $t) {return $t->getCode();}, $table->getColumns())),
			'conditions[0][column]' => $column1->getCode(),
			'conditions[0][operator]' => 'contains',
			'conditions[0][value][0][type]' => 'user',
			'conditions[0][value][0][code]' => $user->getLoginId(),
			'conditions[0][value][1][type]' => 'group',
			'conditions[0][value][1][name]' => 'グループ１',
		));

		$this->assertIdentical(array(), isset($json->errors) ? $json->errors : array());
		$this->assertIdentical(200, $status);
		$this->assertIdentical(2, $json->totalRowCount);

		// Not contains .
		list ($status, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => join(',', array_map(function(Column $t) {return $t->getCode();}, $table->getColumns())),
			'conditions[0][column]' => $column1->getCode(),
			'conditions[0][operator]' => 'not contains',
			'conditions[0][value][1][type]' => 'group',
			'conditions[0][value][1][name]' => 'グループ１',
		));

		$this->assertIdentical(array(), isset($json->errors) ? $json->errors : array());
		$this->assertIdentical(200, $status);
		$this->assertIdentical(2, $json->totalRowCount);


		// 未選択を検索
		list ($status, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => join(',', array_map(function(Column $t) {return $t->getCode();}, $table->getColumns())),
			'conditions[0][column]' => $column1->getCode(),
			'conditions[0][operator]' => 'contains',
			'conditions[0][value][0][type]' => 'user',
			'conditions[0][value][0][code]' => null,
		));

		$this->assertIdentical(array(), isset($json->errors) ? $json->errors : array());
		$this->assertIdentical(200, $status);
		$this->assertIdentical(1, $json->totalRowCount);
		$this->assertIdentical(array(),
			$json->rows[0]->values->{$column1->getCode()});
	}

	/**
	 * ユーザー選択フィールド
	 * - type=group の場合にグループが登録されていない
	 */
	public function testSearch_異常系_ユーザー選択_グループが登録されていない()
	{
		$user = Engine::factory()->getUser(101);
		$user2 = Engine::factory()->getUser(102);
//		$group = Engine::factory()->getGroup(11);

		$table = new Table();
		$table->setName('テーブル１');
		$table->setPublic(true);
		$table->setApiEnabled(true);
		$table->setIcon(Engine::factory()->uploadIcon(__DIR__.'/ApiTest.gif'));
		$column1 = $table->addColumn(Column::USER_SELECTOR);
		$column1->setLabel("ユーザー選択");
		$column1->setCode($column1->getTypeName());
		$table->setViewColumns(array($column1));
		$table->create($user);

		$trunk = Engine::factory()->getTable($table->getId(), Engine::TRUNK);
		$trunk->setApiEnabled(true);
		$trunk->saveChanges($user);
		$trunk->release($user);

		// ユーザーで検索
		list ($status, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => join(',', array_map(function(Column $t) {return $t->getCode();}, $table->getColumns())),
			'conditions[0][column]' => $column1->getCode(),
			'conditions[0][operator]' => 'contains',
			'conditions[0][value][0][type]' => 'group',
			'conditions[0][value][0][name]' => '',
		));

		$this->assertIdentical(400, $status);
		$this->assertIdentical('グループ  が存在しません。', $json->errors[0]->message);
		$this->assertIdentical('conditions[0][value]', $json->errors[0]->context);
	}

	/**
	 * ユーザー選択フィールド
	 * - ユーザーが登録されていない
	 */
	public function testSearch_異常系_ユーザー選択_ユーザーが登録されていない()
	{
		$user = Engine::factory()->getUser(101);
		$user2 = Engine::factory()->getUser(102);
//		$group = Engine::factory()->getGroup(11);

		$table = new Table();
		$table->setName('テーブル１');
		$table->setPublic(true);
		$table->setApiEnabled(true);
		$table->setIcon(Engine::factory()->uploadIcon(__DIR__.'/ApiTest.gif'));
		$column1 = $table->addColumn(Column::USER_SELECTOR);
		$column1->setLabel("ユーザー選択");
		$column1->setCode($column1->getTypeName());
		$table->setViewColumns(array($column1));
		$table->create($user);

		$trunk = Engine::factory()->getTable($table->getId(), Engine::TRUNK);
		$trunk->setApiEnabled(true);
		$trunk->saveChanges($user);
		$trunk->release($user);

		// ユーザーで検索
		list ($status, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => join(',', array_map(function(Column $t) {return $t->getCode();}, $table->getColumns())),
			'conditions[0][column]' => $column1->getCode(),
			'conditions[0][operator]' => 'contains',
			'conditions[0][value][0][type]' => 'user',
			'conditions[0][value][0][code]' => 'xxx',
		));

		$this->assertIdentical(400, $status);
		$this->assertIdentical('ユーザー xxx が存在しません。', $json->errors[0]->message);
		$this->assertIdentical('conditions[0][value]', $json->errors[0]->context);
	}

	public function testSearch_正常系_全件()
	{
		$table = $this->getTable();
		$list_ = $table->search(new Criteria(), $table->getOwner(), 0, 100);

		$textCols = $table->getColumns(array('type'=>Column::TEXT));
		$columns = array(
			$textCols[0],
		);

		list($status, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => join(',', array_map(function(Column $t) { return $t->getCode();}, $columns)),
			'limit' => 100
		));

		$this->assertIdentical(200, $status);
		$this->assertIdentical($list_->getTotalRowCount(), $json->totalRowCount);
		$this->assertIdentical($list_->getOffset(), $json->offset);
		$this->assertIdentical(100, $json->rowLimit);
		$this->assertIdentical($list_->getRowCount(), $json->rowCount);
	}

	public function testSearch_正常系_ページング()
	{
		$table = $this->getTable();
		$list_ = $table->search(new Criteria(), $table->getOwner(), 10, 100);

		$textCols = $table->getColumns(array('type'=>Column::TEXT));
		$columns = array(
			$textCols[0],
		);

		list($staus, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => join(',', array_map(function(Column $t) {
				return $t->getCode();
			}, $columns)),
			'offset' => 10,
			'limit' => 100,
		));

		$this->assertIdentical(200, $staus);
		$this->assertIdentical($list_->getTotalRowCount(), $json->totalRowCount);
		$this->assertIdentical($list_->getOffset(), $json->offset);
		$this->assertIdentical($list_->getRowLimit(), $json->rowLimit);
		$this->assertIdentical($list_->getRowCount(), $json->rowCount);
	}

	public function testSearch_正常系_明細入力参照先DB()
	{
		$table = $this->target->getTable(1005);

		$textCols = $table->getColumns(array('type'=>Column::TEXT));
		$columns = array(
			$textCols[0],
		);

		list($status, $json) = $this->search(array(
			'table'=>$table->getId(),
			'api_key'=>$table->getApiKey(),
			'columns' => join(',', array_map(function(Column $t) { return $t->getCode();}, $columns)),
			'limit' => 2,
			'parent_table' => 1001,
			'parent_row' => 1,
			'parent_column' => 'relational_input_1',
		));

		$this->assertIdentical(200, $status);
		$this->assertIdentical(2, $json->totalRowCount);
		$this->assertIdentical(0, $json->offset);
		$this->assertIdentical(2, $json->rowLimit);
		$this->assertIdentical(2, $json->rowCount);
		$this->assertIdentical('一行テキスト値_10050001',
			$json->rows[0]->values->{$textCols[0]->getCode()}
		);
	}

	/**
	 *
	 * @param bool $isApiEnabled
	 * @param bool $isWriteApiEnabled
	 * @param bool $isPublic
	 * @return Table
	 */
	private function getTable($isApiEnabled = true, $isWriteApiEnabled = null, $isPublic = null)
	{
		//テストできそうなテーブルを探す
		$users = Engine::factory()->searchUsers(null, 0, 100);
		foreach ($users as $user) {
			$tables = Engine::factory()->getUserTables($user);
			foreach ($tables as $table) {
				if (is_not_empty($table->getColumns(array('type'=>Column::DROPDOWN))) &&
					is_not_empty($table->getColumns(array('type'=>Column::RADIO))) &&
					is_not_empty($table->getColumns(array('type'=>Column::RELATIONAL))) &&
					$table->isApiEnabled() === $isApiEnabled &&
					($isWriteApiEnabled === null || $table->isWriteApiEnabled() === $isWriteApiEnabled) &&
					($isPublic === null || $table->isPublic() === $isPublic) &&
					$table->getOwner() !== null) {
					return $table;
				}
			}
		}

		die("テストできそうなテーブルがありません。");
	}

	private function getChildTable()
	{
		return Engine::factory()->getTable(1005);
	}

	/**
	 * @param string $role
	 * @return User
	 */
	private function getUser($role = null)
	{
		//テストできそうなユーザーを探す
		$users = Engine::factory()->searchUsers(null, 0, 100);
		foreach ($users as $user) {
			if ($role === null) {
				return $user;
			} else {
				if ($user->getRole() === $role) {
					return $user;
				}
			}
		}

		die("テストできそうなユーザーがありません。");
	}

	/**
	 * @param Table $table
	 * @return Row
	 */
	private function findTestableRow(Table $table)
	{
		$criteria = new Criteria();
		$criteria->notEquals(array_get($table->getColumns(array(@type=>Column::TEXT)), 0), null);
		$criteria->notContains(array_get($table->getColumns(array(@type=>Column::RADIO)), 0), array(null));	// ラジオが未選択だと都合が悪い
		$list = $table->search(new Criteria(), $table->getOwner());
		return $list->getRow(0);
	}

	// 登録系API共通

	public function testInsert_ユーザーなし()
	{
		list($staus, $json) = $this->insert(null, array('table'=>12123));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('権限がありません。', $json->errors[0]->message);
	}

	public function testInsert_ユーザー不正()
	{
		list($staus, $json) = $this->insert('jfaeiofja@example.com', array('table'=>12123));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('権限がありません。', $json->errors[0]->message);
	}

	// レコード登録API

	public function testInsert_ａｐｉキーなし()
	{
		$user = $this->getUser(User::ADMIN);

		list($staus, $json) = $this->insert($user->getLoginId(), array('table'=>12123));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('api_key は必須です。', $json->errors[0]->message);
		$this->assertIdentical('api_key', $json->errors[0]->context);
	}

	public function testInsert_テーブルなし()
	{
		$user = $this->getUser(User::ADMIN);

		list($staus, $json) = $this->insert($user->getLoginId(), array('api_key'=>'xxxx'));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('table は必須です。', $json->errors[0]->message);
		$this->assertIdentical('table', $json->errors[0]->context);
	}

	public function testInsert_データなし()
	{
		$user = $this->getUser(User::ADMIN);

		list($staus, $json) = $this->insert($user->getLoginId(), array('table' => '12123', 'api_key'=>'xxxx'));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('rows は必須です。', $json->errors[0]->message);
		$this->assertIdentical('rows', $json->errors[0]->context);
	}

	public function testInsert_テーブルが存在しない()
	{
		$user = $this->getUser(User::ADMIN);
		for ($i=0;$i<1;$i++) {
			$rows[] = array( 'values' => array('xxx'=>'yyy') );
		}

		list($staus, $json) = $this->insert($user->getLoginId(), array('table'=>'0', 'api_key'=>'xxxx', 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('DB 0 が存在しません。', $json->errors[0]->message);
		$this->assertIdentical('table', $json->errors[0]->context);
	}

	public function testInsert_ａｐｉキー不正()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		for ($i=0;$i<1;$i++) {
			$rows[] = array( 'values' => array('xxx'=>'yyy') );
		}

		list($staus, $json) = $this->insert($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>'xxxx', 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('不正な api_key です。', $json->errors[0]->message);
		$this->assertIdentical('api_key', $json->errors[0]->context);
	}

	public function testInsert_ａｐｉ非公開()
	{
		$table = $this->getTable(false);
		$user = $table->getOwner();
		for ($i=0;$i<1;$i++) {
			$rows[] = array( 'values' => array('xxx'=>'yyy') );
		}

		list($staus, $json) = $this->insert($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>'xxxx', 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('DB '.$table->getId().' が存在しません。', $json->errors[0]->message);
	}

	public function testInsert_書き込みａｐｉ非公開()
	{
		$table = $this->getTable(true, false);
		$user = $table->getOwner();
		$rows = array();
		for ($i=0;$i<1;$i++) {
			$rows[] = array( 'values' => array('xxx'=>'yyy') );
		}

		list($status, $json) = $this->insert($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>'xxxx', 'rows'=>$rows));
		$this->assertIdentical(400, $status);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('DB '.$table->getId().' が存在しません。', $json->errors[0]->message);
	}

	public function testInsert_データ０件()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();

		list($status, $json) = $this->insert($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey(), 'rows'=>''));
		$this->assertIdentical(400, $status);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('rows は必須です。', $json->errors[0]->message);
		$this->assertIdentical('rows', $json->errors[0]->context);
	}

	public function testInsert_データ上限オーバー()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		$rows = array();
		for ($i=0;$i<101;$i++) {
			$rows[] = array( 'values' => array('xxx'=>'yyy') );
		}

		list($status, $json) = $this->insert($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey(), 'rows'=>$rows));
		$this->assertIdentical(400, $status);
		$this->assertIdentical(1, count($json->errors));
//		$this->assertIdentical('一度に操作できるレコード数は最大100までです。', $json->errors[0]->message);
		$this->assertIdentical('一度に操作できるデータ数は最大100までです。', $json->errors[0]->message); // エラーメッセージ変更
		$this->assertIdentical('rows', $json->errors[0]->context);
	}

	public function testInsert_権限なし()
	{
		$table = $this->getTable(true, true, false);
		$user = $this->getUser(User::USER);
		$rows = array();
		for ($i=0;$i<1;$i++) {
			$rows[] = array( 'values' => array('xxx'=>'yyy') );
		}

		list($staus, $json) = $this->insert($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey(), 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('権限がありません。', $json->errors[0]->message);
		$this->assertIdentical('table', @$json->errors[0]->context);
	}

	public function testInsert_レコード番号指定不可()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		$rows = array();
		for ($i=1;$i<2;$i++) {
			$rows[] = array( 'no' => $i );
		}

		list($staus, $json) = $this->insert($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey(), 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('no は指定できません。', $json->errors[0]->message);
		$this->assertIdentical('rows[0][no]', $json->errors[0]->context);
	}

	public function testInsert_正常()
	{
		// 下記要領でデータを作成
		// ・ラジオボタンフィールドを１つ目の選択肢へセット
		// ・testcaseというキーのテキストフィールドがあれば、テスト名をセット
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		$debugCols = $table->getColumns(array('code'=>'testcase', 'type'=>Column::TEXT));
		$debugCol = is_not_empty($debugCols) ? $debugCols[0] : null;
		$cols = $table->getColumns(array('type'=>Column::RADIO));
		$values = array();
		foreach ($cols as $col) {
			foreach ($table->getColumns(array('type'=>Column::LOOKUP)) as $lookupCol) {
				foreach ($lookupCol->getReferenceCopyMap() as $copyMap) {
					if ($copyMap[1]->getId() == $col->getId()) {
						continue 3;
					}
				}
			}
			$options = $col->getOptions();
			$values[$col->getCode()] = array('name' => $options[0]->getName());
		}
		if (is_empty($values)) {
			die("書き込みできるラジオボタンフィールドが見つかりません。tableId={$table->getId()}");
		}

		// 1 record
		$length = 1;
		$rows = array();
		for ($i=0;$i<$length;$i++) {
			if ($debugCol) $values[$debugCol->getCode()] = __METHOD__ . "（{$length}件）[{$i}]";
			$rows[] = array ('values' => $values );
		}
		$list_ = $table->search(new Criteria(), $table->getOwner());
		$count0 = $list_->getTotalRowCount();
		list($staus, $json) = $this->insert($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey(), 'rows'=>$rows));
		$list_ = $table->search(new Criteria(), $table->getOwner());
		$count1 = $list_->getTotalRowCount();
		$this->assertIdentical(200, $staus);
		$this->assertIdentical($length, count($json->rows));
		$this->assertIdentical($count0 + $length, $count1);

		// 2 records
		$length = 2;
		$rows = array();
		for ($i=0;$i<$length;$i++) {
			if ($debugCol) $values[$debugCol->getCode()] = __METHOD__ . "（{$length}件）[{$i}]";
			$rows[] = array ('values' => $values );
		}
		$list_ = $table->search(new Criteria(), $table->getOwner());
		$count0 = $list_->getTotalRowCount();
		list($staus, $json) = $this->insert($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey(), 'rows'=>$rows));
		$list_ = $table->search(new Criteria(), $table->getOwner());
		$count1 = $list_->getTotalRowCount();
		$this->assertIdentical(200, $staus);
		$this->assertIdentical($length, count($json->rows));
		$this->assertIdentical($count0 + $length, $count1);
	}

	public function testInsert_正常_フィールド省略()
	{
		// （注）デフォルト値のない必須フィールドがあるとこけます。
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		$cols = $table->getColumns(array('type'=>Column::TEXT));
		$values = array();
		foreach ($cols as $col) {
			foreach ($table->getColumns(array('type'=>Column::LOOKUP)) as $lookupCol) {
				foreach ($lookupCol->getReferenceCopyMap() as $copyMap) {
					if ($copyMap[1]->getId() == $col->getId()) {
						continue 3;
					}
				}
			}
			$values[$col->getCode()] = "api example value";
		}
		if (is_empty($values)) {
			die("書き込みできるテキストフィールドが見つかりません。tableId={$table->getId()}");
		}

		// 2 records
		$length = 2;
		$rows = array();
		for ($i=0;$i<$length;$i++) {
			$rows[] = array ('values' => $values );
		}
		$list_ = $table->search(new Criteria(), $table->getOwner());
		$count0 = $list_->getTotalRowCount();
		list($staus, $json) = $this->insert($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey(), 'rows'=>$rows));
		$list_ = $table->search(new Criteria(), $table->getOwner());
		$count1 = $list_->getTotalRowCount();
		$this->assertIdentical(200, $staus);
		$this->assertIdentical($length, count($json->rows));
		$this->assertIdentical($count0 + $length, $count1);
	}

	public function testInsert_ユーザー選択()
	{
		$table = new Table();
		$table->setName('テーブル１');
		$table->setPublic(true);
		$table->setIcon(Engine::factory()->uploadIcon(__DIR__.'/ApiTest.gif'));
		$column1 = $table->addColumn(Column::USER_SELECTOR);
		$column1->setLabel("ユーザー選択");
		$column1->setCode('user_selector');
		$table->setViewColumns(array($column1));
		$user = Engine::factory()->getUser(101);
		$table->create($user);

		$trunk = Engine::factory()->getTable($table->getId(), Engine::TRUNK);
		$trunk->setApiEnabled(true);
		$trunk->setWriteApiEnabled(true);
		$trunk->saveChanges($user);
		$trunk->release($user);

//		$row = $table->addRow();
//		$row->setValue($column1, array($user));
//		$table->insertRow($row, $user);

		list($status, $json) = $this->insert($user->getLoginId(), array(
			'table' => $table->getId(),
			'api_key' => $table->getWriteApiKey(),
			'rows[0]'=> '',	// []
			'rows[1][values][user_selector]'=> '',	// []
			'rows[2][values][user_selector][0]'=> $user->getLoginId(),
//			'rows[2][values][user_selector][0][type]'=> 'group',
//			'rows[2][values][user_selector][0][code]'=> null,
			'rows[3][values][user_selector][0][type]'=> 'group',
			'rows[3][values][user_selector][0][name]'=> 'グループ１',
			'rows[4][values][user_selector][0][type]'=> 'user',
			'rows[4][values][user_selector][0][code]'=> $user->getLoginId(),
			'rows[5][values][user_selector][0][type]'=> 'user',
			'rows[5][values][user_selector][0][code]'=> $user->getLoginId(),
			'rows[5][values][user_selector][1][type]'=> 'group',
			'rows[5][values][user_selector][1][name]'=> 'グループ１',
		));

		$this->assertIdentical(array(), isset($json->errors) ? $json->errors : array());
		$this->assertIdentical(200, $status);

		$criteria = new Criteria();
		$list = $table->search($criteria, $user);

		$this->assertSame(6, $list->getTotalRowCount());
		$this->assertSame(array(), $list->getRow(0)->getValue($column1));
		$this->assertSame(array(), $list->getRow(1)->getValue($column1));
		$this->assertSame(array( $user->getId() ), array_map(function(User $user) { return $user->getId(); }, $list->getRow(2)->getValue($column1)));
		$this->assertSame(array( 102 ), array_map(function(User $user) { return $user->getId(); }, $list->getRow(3)->getValue($column1)));
		$this->assertSame(array( 101 ), array_map(function(User $user) { return $user->getId(); }, $list->getRow(4)->getValue($column1)));
		$this->assertSame(array( 101, 102 ), array_map(function(User $user) { return $user->getId(); }, $list->getRow(5)->getValue($column1)));
		//Engine::factory()->searchTable()
//		$list_ = $table->search(new Criteria(), $table->getOwner());
//		$count1 = $list_->getTotalRowCount();
//		$this->assertIdentical(200, $staus);
//		$this->assertIdentical($length, count($json->rows));
//		$this->assertIdentical($count0 + $length, $count1);

//		// 2 records
//		$length = 2;
//		$rows = array();
//		for ($i=0;$i<$length;$i++) {
//			if ($debugCol) $values[$debugCol->getCode()] = __METHOD__ . "（{$length}件）[{$i}]";
//			$rows[] = array ('values' => $values );
//		}
//		$list_ = $table->search(new Criteria(), $table->getOwner());
//		$count0 = $list_->getTotalRowCount();
//		list($staus, $json) = $this->insert($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey(), 'rows'=>$rows));
//		$list_ = $table->search(new Criteria(), $table->getOwner());
//		$count1 = $list_->getTotalRowCount();
//		$this->assertIdentical(200, $staus);
//		$this->assertIdentical($length, count($json->rows));
//		$this->assertIdentical($count0 + $length, $count1);
	}


	public function testInsert_ユーザー選択_エラー()
	{
		$table = new Table();
		$table->setName('テーブル１');
		$table->setPublic(true);
		$table->setIcon(Engine::factory()->uploadIcon(__DIR__ . '/ApiTest.gif'));
		$column1 = $table->addColumn(Column::USER_SELECTOR);
		$column1->setLabel("ユーザー選択");
		$column1->setCode('user_selector');
		$table->setViewColumns(array($column1));
		$user = Engine::factory()->getUser(101);
		$table->create($user);

		$trunk = Engine::factory()->getTable($table->getId(), Engine::TRUNK);
		$trunk->setApiEnabled(true);
		$trunk->setWriteApiEnabled(true);
		$trunk->saveChanges($user);
		$trunk->release($user);

		// グループが存在しない
		list($status, $json) = $this->insert($user->getLoginId(), array(
			'table' => $table->getId(),
			'api_key' => $table->getWriteApiKey(),
			'rows[0][values][user_selector][0][type]' => 'group',
			'rows[0][values][user_selector][0][name]' => 'xxx',
		));

		$this->assertIdentical(400, $status);
		$this->assertIdentical('グループ xxx が存在しません。', $json->errors[0]->message);
		$this->assertIdentical('rows[0][values][user_selector][0][name]', $json->errors[0]->context);

		// ユーザーが存在しない
		list($status, $json) = $this->insert($user->getLoginId(), array(
			'table' => $table->getId(),
			'api_key' => $table->getWriteApiKey(),
			'rows[0][values][user_selector][0][type]' => 'user',
			'rows[0][values][user_selector][0][code]' => 'xxx',
		));

		$this->assertIdentical(400, $status);
		$this->assertIdentical('ユーザー xxx が存在しません。', $json->errors[0]->message);
		$this->assertIdentical('rows[0][values][user_selector][0][code]', $json->errors[0]->context);

		// typeが不正
		list($status, $json) = $this->insert($user->getLoginId(), array(
			'table' => $table->getId(),
			'api_key' => $table->getWriteApiKey(),
			'rows[0][values][user_selector][0][type]' => 'xxx',
			'rows[0][values][user_selector][0][code]' => 'xxx',
		));

		$this->assertIdentical(400, $status);
		$this->assertIdentical('type は、user または group でなければなりません。', $json->errors[0]->message);
		$this->assertIdentical('rows[0][values][user_selector][0][type]', $json->errors[0]->context);

	}

	public function testInsert_正常_空レコード()
	{
		// （注）デフォルト値のない必須フィールドがあるとこけます。
		$table = $this->getTable(true, true);
		$user = $table->getOwner();

		// 2 records
		$length = 2;
		$rows = array();
		for ($i=0;$i<$length;$i++) {
			$rows[] = '';
		}
		$list_ = $table->search(new Criteria(), $table->getOwner());
		$count0 = $list_->getTotalRowCount();
		list($staus, $json) = $this->insert($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey(), 'rows'=>$rows));
		$list_ = $table->search(new Criteria(), $table->getOwner());
		$count1 = $list_->getTotalRowCount();
		$this->assertIdentical(200, $staus);
		$this->assertIdentical($length, count($json->rows));
		$this->assertIdentical($count0 + $length, $count1);
	}

	public function testInsert_子テーブル()
	{
		$table = $this->getChildTable();
		$user = $table->getOwner();
		$rows = array();
		for ($i=0;$i<1;$i++) {
			$rows[] = array( 'values' => array('xxx'=>'yyy') );
		}

		list($status, $json) = $this->insert($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>'APIKEY', 'rows'=>$rows));
		$this->assertIdentical(400, $status);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('DB '.$table->getId().' への直接の書き込みはできません。', $json->errors[0]->message);
	}

	// レコード削除API

	public function testDelete_ａｐｉキーなし()
	{
		$user = $this->getUser(User::ADMIN);

		list($staus, $json) = $this->delete($user->getLoginId(), array('table'=>12123));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('api_key は必須です。', $json->errors[0]->message);
		$this->assertIdentical('api_key', $json->errors[0]->context);
	}

	public function testDelete_テーブルなし()
	{
		$user = $this->getUser(User::ADMIN);

		list($staus, $json) = $this->delete($user->getLoginId(), array('api_key'=>'xxxx'));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('table は必須です。', $json->errors[0]->message);
		$this->assertIdentical('table', $json->errors[0]->context);
	}

	public function testDelete_データなし()
	{
		$user = $this->getUser(User::ADMIN);

		list($staus, $json) = $this->delete($user->getLoginId(), array('table' => '12123', 'api_key'=>'xxxx'));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('rows は必須です。', $json->errors[0]->message);
		$this->assertIdentical('rows', $json->errors[0]->context);
	}

	public function testDelete_テーブルが存在しない()
	{
		$user = $this->getUser(User::ADMIN);
		for ($i=1;$i<2;$i++) {
			$rows[] = array('no'=>$i);
		}

		list($staus, $json) = $this->delete($user->getLoginId(), array('table'=>'0', 'api_key'=>'xxxx', 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('DB 0 が存在しません。', $json->errors[0]->message);
		$this->assertIdentical('table', $json->errors[0]->context);
	}

	public function testDelete_ａｐｉキー不正()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		for ($i=1;$i<2;$i++) {
			$rows[] = array('no'=>$i);
		}

		list($staus, $json) = $this->delete($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>'xxxx', 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('不正な api_key です。', $json->errors[0]->message);
		$this->assertIdentical('api_key', $json->errors[0]->context);
	}

	public function testDelete_ａｐｉ非公開()
	{
		$table = $this->getTable(false);
		$user = $table->getOwner();
		for ($i=1;$i<2;$i++) {
			$rows[] = array('no'=>$i);
		}

		list($staus, $json) = $this->delete($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>'xxxx', 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('DB '.$table->getId().' が存在しません。', $json->errors[0]->message);
	}

	public function testDelete_書き込みａｐｉ非公開()
	{
		$table = $this->getTable(true, false);
		$user = $table->getOwner();
		for ($i=1;$i<2;$i++) {
			$rows[] = array('no'=>$i);
		}

		list($staus, $json) = $this->delete($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>'xxxx', 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('DB '.$table->getId().' が存在しません。', $json->errors[0]->message);
	}

	public function testDelete_データ０件()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();

		list($staus, $json) = $this->delete($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey(), 'rows'=>''));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('rows は必須です。', $json->errors[0]->message);
		$this->assertIdentical('rows', $json->errors[0]->context);
	}

	public function testDelete_子テーブル()
	{
		$table = $this->getChildTable(true, false);
		$user = $table->getOwner();
		for ($i=1;$i<2;$i++) {
			$rows[] = array('no'=>$i);
		}

		list($staus, $json) = $this->delete($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>'APIKEY', 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('DB '.$table->getId().' への直接の書き込みはできません。', $json->errors[0]->message);
	}

	public function testDelete_データ上限オーバー()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		$rows = array();
		for ($i=1;$i<102;$i++) {
			$rows[] = array('no'=>$i);
		}

		list($staus, $json) = $this->delete($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey(), 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
//		$this->assertIdentical('一度に操作できるレコード数は最大100までです。', $json->errors[0]->message);
		$this->assertIdentical('一度に操作できるデータ数は最大100までです。', $json->errors[0]->message); // エラーメッセージ変更
		$this->assertIdentical('rows', $json->errors[0]->context);
	}

	public function testDelete_権限なし()
	{
		$table = $this->getTable(true, true, false);
		$user = $this->getUser(User::USER);
		$rows = array();
		for ($i=1;$i<2;$i++) {
			$rows[] = array('no'=>$i);
		}

		list($staus, $json) = $this->delete($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey(), 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('権限がありません。', $json->errors[0]->message);
		$this->assertIdentical('table', @$json->errors[0]->context);
	}

	public function testDelete_レコード番号指定なし()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		$rows = array();
		for ($i=1;$i<2;$i++) {
			$rows[] = array( 'xxx' => $i );
		}

		list($staus, $json) = $this->delete($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey(), 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('no は必須です。', $json->errors[0]->message);
		$this->assertIdentical('rows[0][no]', $json->errors[0]->context);
	}

	public function testDelete_レコードが存在しない()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		$rows = array();
		for ($i=1;$i<2;$i++) {
			$rows[] = array( 'no' => '0' );
		}

		list($staus, $json) = $this->delete($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey(), 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
//		$this->assertIdentical('指定されたレコードが存在しません。', $json->errors[0]->message);
		$this->assertIdentical('指定されたデータが存在しません。', $json->errors[0]->message); // エラーメッセージの変更
		$this->assertIdentical('rows[0][no]', $json->errors[0]->context);
	}

	public function testDelete_正常()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		$debugCols = $table->getColumns(array('code'=>'testcase', 'type'=>Column::TEXT));
		$debugCol = is_not_empty($debugCols) ? $debugCols[0] : null;

		// delete 1 record
		$list_ = $table->search(new Criteria(), $table->getOwner());
		$count0 = $list_->getTotalRowCount();
		list($staus, $json) = $this->_testDelete(1, $table, $user);
		$list_ = $table->search(new Criteria(), $table->getOwner());
		$count1 = $list_->getTotalRowCount();

		$this->assertIdentical(200, $staus);
		$this->assertIdentical($count0, $count1);
		$json = (array)$json;
		$this->assertIdentical(true, empty($json));

		// delete 2 records
		$list_ = $table->search(new Criteria(), $table->getOwner());
		$count0 = $list_->getTotalRowCount();
		list($staus, $json) = $this->_testDelete(2, $table, $user);
		$list_ = $table->search(new Criteria(), $table->getOwner());
		$count1 = $list_->getTotalRowCount();

		$this->assertIdentical(200, $staus);
		$this->assertIdentical($count0, $count1);
		$json = (array)$json;
		$this->assertIdentical(true, empty($json));
	}

	/**
	 * insert rows, then call api to delete the rows
	 * @param unknown $length
	 * @param unknown $table
	 * @param unknown $user
	 */
	private function _testDelete($length, $table, $user)
	{
		$rowObjects = array();
		for ($i=0;$i<$length;$i++) {
			$rowObjects[] = $table->addRow();
		}
		$table->insertRows($user);

		$rows = array();
		for ($i=0;$i<$length;$i++) {
			$rows[] = array( 'no' => $rowObjects[$i]->getRownum() );
		}
		return $this->delete($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey(), 'rows'=>$rows));
	}

	// ファイルアップロードAPI

	public function testUpload_ａｐｉキーなし()
	{
		$user = $this->getUser(User::ADMIN);
		$file = array (
				'param_name' => "file",
				'path' => $this->getFilePath("test.png"),
				'name' => "test.png",
				'type' => "image/png",
		);

		list($staus, $json) = $this->upload($user->getLoginId(), array('table'=>12123), $file);
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('api_key は必須です。', $json->errors[0]->message);
		$this->assertIdentical('api_key', $json->errors[0]->context);
	}

	public function testUpload_テーブルなし()
	{
		$user = $this->getUser(User::ADMIN);
		$file = array (
				'param_name' => "file",
				'path' => $this->getFilePath("test.png"),
				'name' => "test.png",
				'type' => "image/png",
		);

		list($staus, $json) = $this->upload($user->getLoginId(), array('api_key'=>'xxxx'), $file);
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('table は必須です。', $json->errors[0]->message);
		$this->assertIdentical('table', $json->errors[0]->context);
	}

	public function testUpload_テーブルが存在しない()
	{
		$user = $this->getUser(User::ADMIN);
		$file = array (
				'param_name' => "file",
				'path' => $this->getFilePath("test.png"),
				'name' => "test.png",
				'type' => "image/png",
		);

		list($staus, $json) = $this->upload($user->getLoginId(), array('table'=>'0', 'api_key'=>'xxxx'), $file);
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('DB 0 が存在しません。', $json->errors[0]->message);
		$this->assertIdentical('table', $json->errors[0]->context);
	}

	public function testUpload_ａｐｉキー不正()
	{
		$table = $this->getTable(true, true);
		$file = array (
				'param_name' => "file",
				'path' => $this->getFilePath("test.png"),
				'name' => "test.png",
				'type' => "image/png",
		);
		$user = $table->getOwner();

		list($staus, $json) = $this->upload($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>'xxxx'), $file);
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('不正な api_key です。', $json->errors[0]->message);
		$this->assertIdentical('api_key', $json->errors[0]->context);
	}

	public function testUpload_ａｐｉ非公開()
	{
		$table = $this->getTable(false);
		$user = $table->getOwner();
		$file = array (
				'param_name' => "file",
				'path' => $this->getFilePath("test.png"),
				'name' => "test.png",
				'type' => "image/png",
		);

		list($staus, $json) = $this->upload($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>'xxxx'), $file);
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('DB '.$table->getId().' が存在しません。', $json->errors[0]->message);
	}

	public function testUpload_書き込みａｐｉ非公開()
	{
		$table = $this->getTable(true, false);
		$user = $table->getOwner();
		$file = array (
				'param_name' => "file",
				'path' => $this->getFilePath("test.png"),
				'name' => "test.png",
				'type' => "image/png",
		);

		list($staus, $json) = $this->upload($user->getLoginId(),
				array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey()), $file);
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('DB '.$table->getId().' が存在しません。', $json->errors[0]->message);
	}

	public function testUpload_ファイルなし()
	{
		$table = $this->getTable(true, true);
		$user = $this->getUser(User::ADMIN);
		$file = array (
				'param_name' => "file",
				'path' => $this->getFilePath("test.png"),
				'name' => "test.png",
				'type' => "image/png",
		);

		list($staus, $json) = $this->upload_with_zerofile($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey()));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('アップロードに失敗しました。', $json->errors[0]->message);
		$this->assertIdentical('file', $json->errors[0]->context);
	}

	public function testUpload_ファイル上限オーバー()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		$file1 = array (
			'param_name' => "file[0]",
			'path' => $this->getFilePath("test.png"),
			'name' => "test.png",
			'type' => "image/png",
		);
		$file2 = array (
			'param_name' => "file[1]",
			'path' => $this->getFilePath("test.txt"),
			'name' => "test.txt",
			'type' => "text/plain",
		);

		list($staus, $json) = $this->upload_with_multifile(
			$user->getLoginId(),
			array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey()),
			array($file1, $file2)
		);
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('アップロードに失敗しました。', $json->errors[0]->message);
		$this->assertIdentical('file', $json->errors[0]->context);
	}

	public function testUpload_正常()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		$file = array (
			'param_name' => "file",
			'path' => $this->getFilePath("test.png"),
			'name' => "test.png",
			'type' => "image/png",
		);

		list($staus, $json) = $this->upload(
				$user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey()), $file);
		$this->assertIdentical(200, $staus);
		$this->assertIdentical(1, count($json->filekey));
	}

	// レコード更新API

	public function testUpdate_ａｐｉキーなし()
	{
		$user = $this->getUser(User::ADMIN);

		list($staus, $json) = $this->update($user->getLoginId(), array('table'=>12123));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('api_key は必須です。', $json->errors[0]->message);
		$this->assertIdentical('api_key', $json->errors[0]->context);
	}

	public function testUpdate_テーブルなし()
	{
		$user = $this->getUser(User::ADMIN);

		list($staus, $json) = $this->update($user->getLoginId(), array('api_key'=>'xxxx'));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('table は必須です。', $json->errors[0]->message);
		$this->assertIdentical('table', $json->errors[0]->context);
	}

	public function testUpdate_データなし()
	{
		$user = $this->getUser(User::ADMIN);

		list($staus, $json) = $this->update($user->getLoginId(), array('table' => '12123', 'api_key'=>'xxxx'));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('rows は必須です。', $json->errors[0]->message);
		$this->assertIdentical('rows', $json->errors[0]->context);
	}

	public function testUpdate_テーブルが存在しない()
	{
		$user = $this->getUser(User::ADMIN);
		for ($i=1;$i<2;$i++) {
			$rows[] = array('no'=>$i);
		}

		list($staus, $json) = $this->update($user->getLoginId(), array('table'=>'0', 'api_key'=>'xxxx', 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('DB 0 が存在しません。', $json->errors[0]->message);
		$this->assertIdentical('table', $json->errors[0]->context);
	}

	public function testUpdate_ａｐｉキー不正()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		for ($i=1;$i<2;$i++) {
			$rows[] = array('no'=>$i);
		}

		list($staus, $json) = $this->update($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>'xxxx', 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('不正な api_key です。', $json->errors[0]->message);
		$this->assertIdentical('api_key', $json->errors[0]->context);
	}

	public function testUpdate_ａｐｉ非公開()
	{
		$table = $this->getTable(false);
		$user = $table->getOwner();
		for ($i=1;$i<2;$i++) {
			$rows[] = array('no'=>$i);
		}

		list($staus, $json) = $this->update($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>'xxxx', 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('DB '.$table->getId().' が存在しません。', $json->errors[0]->message);
	}

	public function testUpdate_書き込みａｐｉ非公開()
	{
		$table = $this->getTable(true, false);
		$user = $table->getOwner();
		for ($i=1;$i<2;$i++) {
			$rows[] = array('no'=>$i);
		}

		list($staus, $json) = $this->update($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>'xxxx', 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('DB '.$table->getId().' が存在しません。', $json->errors[0]->message);
	}

	public function testUpdate_子テーブル()
	{
		$table = $this->getChildTable();
		$user = $table->getOwner();
		for ($i=1;$i<2;$i++) {
			$rows[] = array('no'=>$i);
		}

		list($staus, $json) = $this->update($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>'APIKEY', 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('DB '.$table->getId().' への直接の書き込みはできません。', $json->errors[0]->message);
	}

	public function testUpdate_データ０件()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();

		list($staus, $json) = $this->update($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey(), 'rows'=>''));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('rows は必須です。', $json->errors[0]->message);
		$this->assertIdentical('rows', $json->errors[0]->context);
	}

	public function testUpdate_データ上限オーバー()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		$rows = array();
		for ($i=1;$i<102;$i++) {
			$rows[] = array('no'=>$i);
		}

		list($staus, $json) = $this->update($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey(), 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
//		$this->assertIdentical('一度に操作できるレコード数は最大100までです。', $json->errors[0]->message);
		$this->assertIdentical('一度に操作できるデータ数は最大100までです。', $json->errors[0]->message); // エラーメッセージ変更
		$this->assertIdentical('rows', $json->errors[0]->context);
	}

	public function testUpdate_権限なし()
	{
		$table = $this->getTable(true, true, false);
		$user = $this->getUser(User::USER);
		$rows = array();
		for ($i=1;$i<2;$i++) {
			$rows[] = array('no'=>$i);
		}

		list($staus, $json) = $this->update($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey(), 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('権限がありません。', $json->errors[0]->message);
		$this->assertIdentical('table', @$json->errors[0]->context);
	}

	public function testUpdate_レコード番号指定なし()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		$rows = array();
		for ($i=1;$i<2;$i++) {
			$rows[] = array( 'xxx' => $i );
		}

		list($staus, $json) = $this->update($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey(), 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
		$this->assertIdentical('no は必須です。', $json->errors[0]->message);
		$this->assertIdentical('rows[0][no]', $json->errors[0]->context);
	}

	public function testUpdate_レコードが存在しない()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		$rows = array();
		for ($i=1;$i<2;$i++) {
			$rows[] = array( 'no' => '0' );
		}

		list($staus, $json) = $this->update($user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey(), 'rows'=>$rows));
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(1, count($json->errors));
//		$this->assertIdentical('指定されたレコードが存在しません。', $json->errors[0]->message);
		$this->assertIdentical('指定されたデータが存在しません。', $json->errors[0]->message); // エラーメッセージの変更
		$this->assertIdentical('rows[0][no]', $json->errors[0]->context);
	}

	public function testUpdate_正常()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		$debugCols = $table->getColumns(array('code'=>'testcase', 'type'=>Column::TEXT));
		$debugCol = is_not_empty($debugCols) ? $debugCols[0] : null;

		$file = array (
				'param_name' => "file",
				'path' => $this->getFilePath("test.png"),
				'name' => "test.png",
				'type' => "image/png",
		);
		list($staus, $json) = $this->upload(
				$user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey()), $file);
		$this->assertIdentical(200, $staus);
		$filekey = $json->filekey;

		$cols = $table->getColumns();
		$values = array();
		foreach ($cols as $col) {
			foreach ($table->getColumns(array('type'=>Column::LOOKUP)) as $lookupCol) {
				foreach ($lookupCol->getReferenceCopyMap() as $copyMap) {
					if ($copyMap[1]->getId() == $col->getId()) {
						continue 3;
					}
				}
			}
			switch ($col->getType()) {
				case Column::RADIO:
					$options = $col->getOptions();
					$values[$col->getCode()] = array('name' => $options[0]->getName());
					break;
				case Column::FILE:
					$values[$col->getCode()] = array();
					$values[$col->getCode()][0] = array('filekey' => $filekey);
					break;
				default:
			}
		}
		if (is_empty($values)) {
			die("書き込みできるフィールドが見つかりません。tableId={$table->getId()}");
		}

		$rows = array();
		$rows[0] = array( 'values' => $values);
		$rows[1] = array( 'values' => $values);

		// update 1 record
		list($staus, $json) = $this->_testUpdate($rows, 1, $table, $user, $debugCol, __METHOD__);
		$this->assertIdentical(200, $staus);
		$this->assertIdentical(1, count($json->rows));

		// update 2 record
		list($staus, $json) = $this->_testUpdate($rows, 2, $table, $user, $debugCol, __METHOD__);
		$this->assertIdentical(200, $staus);
		$this->assertIdentical(2, count($json->rows));
	}

	/**
	 *
	 * insert rows, then call api to update the rows
	 * @param unknown $rows 更新データの入った rows のデータ
	 * @param unknown $length 使用する
	 * @param unknown $table
	 * @param unknown $user
	 * @param string $debugCol デバッグ用のフィールド
	 * @param string $testcase デバッグ用のフィールドに入れるケース名
	 * @return multitype:NULL unknown
	 */
	private function _testUpdate($rows, $length, $table, $user, $debugCol = null, $testcase = '')
	{
		$rowObjects = array();
		for ($i=0;$i<$length;$i++) {
			$rowObjects[] = $table->addRow();
		}
		$table->insertRows($user);

		for ($i=0;$i<$length;$i++) {
			// debugCol（通常は呼び出し元でキーtestcaseのテキストフィールドを取得）があれば、テスト名をセット
			if ($debugCol) $rows[$i]['values'][$debugCol->getCode()] = "{$testcase}（{$length}件）[{$i}]";
			$rows[$i]['no'] = $rowObjects[$i]->getRownum();
		}

		array_splice($rows, $length);

		return $this->update($user->getLoginId(), array('table'=>$table->getId(),
				'api_key'=>$table->getWriteApiKey(), 'rows'=>$rows));
	}

	public function testUpdate_ユーザー選択()
	{
		$table = new Table();
		$table->setName('テーブル１');
		$table->setPublic(true);
		$table->setIcon(Engine::factory()->uploadIcon(__DIR__.'/ApiTest.gif'));
		$column1 = $table->addColumn(Column::USER_SELECTOR);
		$column1->setLabel("ユーザー選択");
		$column1->setCode('user_selector');
		$table->setViewColumns(array($column1));
		$user = Engine::factory()->getUser(101);
		$table->create($user);

		$trunk = Engine::factory()->getTable($table->getId(), Engine::TRUNK);
		$trunk->setApiEnabled(true);
		$trunk->setWriteApiEnabled(true);
		$trunk->saveChanges($user);
		$trunk->release($user);

		$row = $table->addRow();
		$row->setValue($column1, array());
		$table->insertRow($row, $user);

		list($status, $json) = $this->update($user->getLoginId(), array(
			'table' => $table->getId(),
			'api_key' => $table->getWriteApiKey(),
			'rows[0][no]' => $row->getRowNo(),
			'rows[0][values][user_selector][0][type]'=> 'group',
			'rows[0][values][user_selector][0][name]'=> 'グループ１',
		));

		$this->assertIdentical(array(), isset($json->errors) ? $json->errors : array());
		$this->assertIdentical(200, $status);

		$criteria = new Criteria();
		$list = $table->search($criteria, $user);

		$this->assertSame(1, $list->getTotalRowCount());
		$this->assertSame(array( 102 ), array_map(function(User $user) { return $user->getId(); }, $list->getRow(0)->getValue($column1)));
	}

	/**
	 * Update - 複数選択可能なフィールドについて、クリアができるかのテスト
	 */
	public function testUpdate_複数選択_クリア()
	{
		$table = new Table();
		$table->setName('テーブル１');
		$table->setPublic(true);
		$table->setIcon(Engine::factory()->uploadIcon(__DIR__.'/ApiTest.gif'));
		$column1 = $table->addColumn(Column::CHECKBOX);
		$column1->setLabel("チェックボックス");
		$column1->setCode('checkbox1');
		$option1 = new Option();
		$option1->setName("オプション１");
		$column1->addOption(array($option1));
		$column2 = $table->addColumn(Column::USER_SELECTOR);
		$column2->setCode("user_selector1");
		$table->setViewColumns(array($column1));
		$user = Engine::factory()->getUser(101);
		$table->create($user);

		$trunk = Engine::factory()->getTable($table->getId(), Engine::TRUNK);
		$trunk->setApiEnabled(true);
		$trunk->setWriteApiEnabled(true);
		$trunk->saveChanges($user);
		$trunk->release($user);

		$row = $table->addRow();
		$row->setValue($column1, array($option1));
		$row->setValue($column2, array($user));
		$table->insertRow($row, $user);

		list($status, $json) = $this->update($user->getLoginId(), array(
			'table' => $table->getId(),
			'api_key' => $table->getWriteApiKey(),
			'rows[0][no]' => $row->getRowNo(),
			'rows[0][values][checkbox1]'=> '', // クリア
			'rows[0][values][user_selector1]'=> '', // クリア
		));

		$this->assertIdentical(array(), isset($json->errors) ? $json->errors : array());
		$this->assertIdentical(200, $status);

		$criteria = new Criteria();
		$list = $table->search($criteria, $user);

		$this->assertSame(1, $list->getTotalRowCount());
		$this->assertSame(array(), array_map(function(Option $user) { return $user->getId(); }, $list->getRow(0)->getValue($column1)));
		$this->assertSame(array(), array_map(function(User $user) { return $user->getId(); }, $list->getRow(0)->getValue($column2)));
	}


	public function testUpdate_エンジンのバリデーションエラー()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		$debugCols = $table->getColumns(array('code'=>'testcase', 'type'=>Column::TEXT));
		$debugCol = is_not_empty($debugCols) ? $debugCols[0] : null;

		$cols = $table->getColumns(array('type'=>Column::RADIO));
		$values = array();
		$errorValues = array();
		foreach ($cols as $col) {
			foreach ($table->getColumns(array('type'=>Column::LOOKUP)) as $lookupCol) {
				foreach ($lookupCol->getReferenceCopyMap() as $copyMap) {
					if ($copyMap[1]->getId() == $col->getId()) {
						continue 3;
					}
				}
			}
			$options = $col->getOptions();
			$values[$col->getCode()] = array('name' => $options[0]->getName());
			$errorValues[$col->getCode()] = '';
		}
		if (is_empty($values)) {
			die("書き込みできるラジオボタンフィールドが見つかりません。tableId={$table->getId()}");
		}

		$rows = array();
		$rows[0] = array( 'values' => $values);
		$rows[1] = array( 'values' => $errorValues);

		// update 2 record
		list($staus, $json) = $this->_testUpdate($rows, 2, $table, $user, $debugCol, __METHOD__);
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(true, count($json->errors) > 0);
		$codes = array_keys($values);
//		$this->assertIdentical("ラジオボタンを入力してください。", $json->errors[0]->message);
		$this->assertIdentical($cols[0]->getLabel()."を入力してください。", $json->errors[0]->message); // エラーメッセージの変更
		$this->assertIdentical("rows[1][values][{$codes[0]}]", $json->errors[0]->context);
	}

	public function testUpdate_ラジオボタン選択肢誤り()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		$debugCols = $table->getColumns(array('code'=>'testcase', 'type'=>Column::TEXT));
		$debugCol = is_not_empty($debugCols) ? $debugCols[0] : null;

		$cols = $table->getColumns(array('type'=>Column::RADIO));
		$values = array();
		$errorValues = array();
		foreach ($cols as $col) {
			foreach ($table->getColumns(array('type'=>Column::LOOKUP)) as $lookupCol) {
				foreach ($lookupCol->getReferenceCopyMap() as $copyMap) {
					if ($copyMap[1]->getId() == $col->getId()) {
						continue 3;
					}
				}
			}
			$options = $col->getOptions();
			$values[$col->getCode()] = array('name' => $options[0]->getName());
			$errorValues[$col->getCode()] = array('name' => 'xxxxx');
		}
		if (is_empty($values)) {
			die("書き込みできるラジオボタンフィールドが見つかりません。tableId={$table->getId()}");
		}

		$rows = array();
		$rows[0] = array( 'values' => $values);
		$rows[1] = array( 'values' => $errorValues);

		// update 2 record
		list($staus, $json) = $this->_testUpdate($rows, 2, $table, $user, $debugCol, __METHOD__);
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(true, count($json->errors) > 0);
		$codes = array_keys($values);
		$this->assertIdentical("値が不正です。", $json->errors[0]->message);
		$this->assertIdentical("rows[1][values][{$codes[0]}][name]", $json->errors[0]->context);
	}

	public function testUpdate_チェックボックス選択肢誤り()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		$debugCols = $table->getColumns(array('code'=>'testcase', 'type'=>Column::TEXT));
		$debugCol = is_not_empty($debugCols) ? $debugCols[0] : null;

		$cols = $table->getColumns(array('type'=>Column::CHECKBOX));
		$values = array();
		$errorValues = array();
		foreach ($cols as $col) {
			foreach ($table->getColumns(array('type'=>Column::LOOKUP)) as $lookupCol) {
				foreach ($lookupCol->getReferenceCopyMap() as $copyMap) {
					if ($copyMap[1]->getId() == $col->getId()) {
						continue 3;
					}
				}
			}
			$options = $col->getOptions();
			$values[$col->getCode()] = array();
			$values[$col->getCode()][0] = array('name' => $options[0]->getName());
			$values[$col->getCode()][1] = array('name' => $options[1]->getName());
			$errorValues[$col->getCode()] = array();
			$errorValues[$col->getCode()][0] = array('name' => $options[0]->getName());
			$errorValues[$col->getCode()][1] = array('name' => 'xxxxx');
		}
		if (is_empty($values)) {
			die("書き込みできるチェックボックスフィールドが見つかりません。tableId={$table->getId()}");
		}

		$rows = array();
		$rows[0] = array( 'values' => $values);
		$rows[1] = array( 'values' => $errorValues);

		// update 2 record
		list($staus, $json) = $this->_testUpdate($rows, 2, $table, $user, $debugCol, __METHOD__);
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(true, count($json->errors) > 0);
		$codes = array_keys($values);
		$this->assertIdentical("値が不正です。", $json->errors[0]->message);
		$this->assertIdentical("rows[1][values][{$codes[0]}][1][name]", $json->errors[0]->context);
	}

	public function testUpdate_フィールドキーあやまり()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		$debugCols = $table->getColumns(array('code'=>'testcase', 'type'=>Column::TEXT));
		$debugCol = is_not_empty($debugCols) ? $debugCols[0] : null;

		$cols = $table->getColumns(array('type'=>Column::RADIO));
		$values = array();
		foreach ($cols as $col) {
			foreach ($table->getColumns(array('type'=>Column::LOOKUP)) as $lookupCol) {
				foreach ($lookupCol->getReferenceCopyMap() as $copyMap) {
					if ($copyMap[1]->getId() == $col->getId()) {
						continue 3;
					}
				}
			}
			$options = $col->getOptions();
			$values[$col->getCode()] = array('name' => $options[0]->getName());
		}
		if (is_empty($values)) {
			die("書き込みできるラジオボタンフィールドが見つかりません。tableId={$table->getId()}");
		}
		$errorValues = array('xxxxxxxxxx' => 'xxxxxxxxxxx');

		$rows = array();
		$rows[0] = array( 'values' => $values);
		$rows[1] = array( 'values' => $errorValues);

		// update 2 record
		list($staus, $json) = $this->_testUpdate($rows, 2, $table, $user, $debugCol, __METHOD__);
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(true, count($json->errors) > 0);
		$codes = array_keys($errorValues);
		$this->assertIdentical("フィールド {$codes[0]} が存在しません。", $json->errors[0]->message);
		$this->assertIdentical("rows[1][values][{$codes[0]}]", $json->errors[0]->context);

	}

	public function testUpdate_書き込み不可のフィールド()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		$debugCols = $table->getColumns(array('code'=>'testcase', 'type'=>Column::TEXT));
		$debugCol = is_not_empty($debugCols) ? $debugCols[0] : null;

//		$values = ''; // １レコード目は空データ
		$values = array(); // １レコード目は空データ
		$readonlyCols = $table->getColumns(array('type'=>Column::UPDATED_ON));
		$errorValues = array();
		$errorValues[$readonlyCols[0]->getCode()] = 123;

		$rows = array();
		$rows[0] = array( 'values' => $values);
		$rows[1] = array( 'values' => $errorValues);

		// update 2 record
		list($staus, $json) = $this->_testUpdate($rows, 2, $table, $user, $debugCol, __METHOD__);
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(true, count($json->errors) > 0);
		$codes = array_keys($errorValues);
		$this->assertIdentical("フィールド {$codes[0]} は指定できません。", $json->errors[0]->message);
		$this->assertIdentical("rows[1][values][{$codes[0]}]", $json->errors[0]->context);

	}

	public function testUpdate_バージョン誤り()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		$debugCols = $table->getColumns(array('code'=>'testcase', 'type'=>Column::TEXT));
		$debugCol = is_not_empty($debugCols) ? $debugCols[0] : null;

		$rows = array();
		$rows[0] = array( 'version' => 1);
		$rows[1] = array( 'version' => 2);

		// update 2 record
		list($staus, $json) = $this->_testUpdate($rows, 2, $table, $user, $debugCol, __METHOD__);
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(true, count($json->errors) > 0);
//		$this->assertIdentical(true, preg_match('/このレコードは、.+に.+によって更新されています。/', $json->errors[0]->message) > 0);
		$this->assertIdentical(true, preg_match('/このデータは、.+に.+によって更新されています。/', $json->errors[0]->message) > 0); // エラーメッセージ変更
		$this->assertIdentical("rows[1][version]", $json->errors[0]->context);

	}

	public function testUpdate_ファイルキー誤り()
	{
		$table = $this->getTable(true, true);
		$user = $table->getOwner();
		$debugCols = $table->getColumns(array('code'=>'testcase', 'type'=>Column::TEXT));
		$debugCol = is_not_empty($debugCols) ? $debugCols[0] : null;

		$file = array (
				'param_name' => "file",
				'path' => $this->getFilePath("test.png"),
				'name' => "test.png",
				'type' => "image/png",
		);
		list($staus, $json) = $this->upload(
				$user->getLoginId(), array('table'=>$table->getId(), 'api_key'=>$table->getWriteApiKey()), $file);
		$this->assertIdentical(200, $staus);
		$filekey = $json->filekey;

		$cols = $table->getColumns(array('type'=>Column::FILE));
		if (empty($cols)) {
			die("書き込みできるファイルフィールドが見つかりません。tableId={$table->getId()}");
		}

		$values = array();
		$values[$cols[0]->getCode()] = array();
		$values[$cols[0]->getCode()][0] = array( 'filekey' => $filekey);
		$errorValues = array();
		$errorValues[$cols[0]->getCode()] = array();
		$errorValues[$cols[0]->getCode()][0] = array( 'filekey' => $filekey);
		$errorValues[$cols[0]->getCode()][1] = array( 'filekey' => 'xxxxx');

		$rows = array();
		$rows[0] = array( 'values' => $values);
		$rows[1] = array( 'values' => $errorValues);

		// update 2 record
		list($staus, $json) = $this->_testUpdate($rows, 2, $table, $user, $debugCol, __METHOD__);
		$this->assertIdentical(400, $staus);
		$this->assertIdentical(true, count($json->errors) > 0);
		$codes = array_keys($errorValues);
		$this->assertIdentical("ファイルが存在しません。", $json->errors[0]->message);
		$this->assertIdentical("rows[1][values][{$codes[0]}][1][filekey]", $json->errors[0]->context);
	}
	
	
}

