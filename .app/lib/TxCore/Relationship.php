<?php
namespace TxCore;

/**
 *
 */
class Relationship
{
	private $sourceColumn;
	private $sourceRow;
	private $targetRowId;

	public function getSourceColumn()
	{
		return $this->sourceColumn;
	}
	public function setSourceColumn($value)
	{
		$this->sourceColumn = $value;
	}

	public function getSourceRow()
	{
		return $this->sourceRow;
	}
	public function setSourceRow($value)
	{
		$this->sourceRow = $value;
	}

	public function _getTargetRowId()
	{
		return $this->targetRowId;
	}
	public function _setTargetRowId($value)
	{
		$this->targetRowId = $value;
	}

	public function getTargetTable()
	{
		return $this->targetTable;
	}
	public function setTargetTable($value)
	{
		$this->targetTable = $value;
	}

}
