<?php
namespace TxCore;

use TxCore\User;
use TxCore\Row;
use TxCore\TxCoreException;

/**
 * File abstraction layer
 * @author kenji
 */
class Storage 
{
	/**
	 * @var array
	 */
	private $penddingRemoves = array();
	
	/**
	 * @var string
	 */
	private $baseDir;
	
	/**
	 * @var string
	 */
	private $baseUrl;
	
	public function __construct() 
	{
		$this->baseDir = get_config('app', 'user_file_dir');
		$this->baseUrl = get_config('app', 'user_file_dir_url');
	} 
	
	/**
	 * @param string $pathname Virtual path in storage.
	 * @param string $srcPath Path to upload (Physical path).
	 */
	public function createFile($filekey, $srcPath) 
	{
		if (!file_exists($srcPath)) {
			throw new TxCoreException(sprintf('File dose not exist %s.', $srcPath),
									TxCoreException::INTERNAL);
		}
		
		if (strpos($filekey, '/') !== false) {
			Util::makeDirs(concat_path($this->baseDir, dirname($filekey)), 0777, true);
		}
		
		$physicalPath = concat_path($this->baseDir, $filekey);
		copy($srcPath, $physicalPath);
		chmod($physicalPath, 0777);
		return concat_path($this->baseUrl, $filekey);
	}
	
	public function getFileSize($pathname)
	{
		return filesize(concat_path($this->baseDir, $pathname));
	}
	
	public function removeFile($filekey) 
	{
		$this->penddingRemoves[] = $filekey;
	}

	/**
	 * Returns HTTP-accessible Url. 
	 * @param string $pathname
	 */
	public function getFileUrl($pathname)
	{
		return concat_path($this->baseUrl, $pathname);
	}
	
	/**
	 * @param string $pathname
	 * @return string
	 */
	public function getFilePath($pathname, $silently = false)
	{
		$physical = concat_path($this->baseDir, $pathname);
		if (!file_exists($physical)) {
			if ($silently) {
				return null;
			}
			throw new TxCoreException(sprintf('File dose not exist %s.', $physical),
									TxCoreException::INTERNAL);
		}
		return $physical;
	}
	
	public function removeDir($pathname)
	{
		$this->penddingRemoves[] = $pathname;
	}
	
	public function flush() 
	{
		foreach ($this->penddingRemoves as $name) {
			$physicalPath = concat_path($this->baseDir, $name);
			if (is_dir($physicalPath)) {
				Util::removeFiles($physicalPath, true);
			} else if (file_exists($physicalPath)) {
 				@unlink($physicalPath);
			}
		}
		$this->penddingRemoves = array();
	}
	
	public function __destruct() 
	{
		$this->flush();
	}
}

