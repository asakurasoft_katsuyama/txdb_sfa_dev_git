<?php
namespace TxCore;

/**
 * Comment.
 */
class Comment
{
    private $id;

    private $no;


    private $text;

//    private $rowId;
//    private $tableId;


    private $postedTime;
    private $postedUserId;
    private $postedUser;

    /**
     * @var User[]|Group[]
     */
    private $notificationTargets = array();

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function _setId($id)
    {
        $this->id = Util::toInt($id);
    }

    /**
     * @return mixed
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @param mixed $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return mixed
     */
    public function getPostedTime($asTimestamp = false)
    {
        if ($asTimestamp) {
            if ($this->postedTime == null) {
                return null;
            }
            return strtotime($this->postedTime);
        }
        return $this->postedTime;
    }

    /**
     * @param mixed $postTime
     */
    public function _setPostedTime($postTime)
    {
        $this->postedTime = $postTime;
    }

    /**
     * @param mixed $postedUserId
     */
    public function _setPostedUserId($postedUserId)
    {
        $this->postedUserId = $postedUserId;
    }

    /**
     * @return User
     * @throws TxCoreException
     */
    public function getPostedUser()
    {
        if ($this->postedUserId !== null && $this->postedUser === null) {
            $this->postedUser = Engine::factory()->getUser($this->postedUserId, false, true);
        }
        return $this->postedUser;
    }

    /**
     * @return User[]|Group[]
     */
    public function getNotificationTargets()
    {

        return $this->notificationTargets;
    }

    /**
     * @param User|Group $entity
     */
    public function addNotificationTarget($entity)
    {
        assert($entity instanceof User || $entity instanceof Group);
        $this->notificationTargets[] = $entity;
    }

    /**
     * @param $entities
     */
    public function _setNotificationTargets($entities)
    {
        $this->notificationTargets = $entities;
    }

    /**
     * @return mixed
     */
    public function getNo()
    {
        return $this->no;
    }

    /**
     * @param mixed $no
     */
    public function _setNo($no)
    {
        $this->no = Util::toInt($no);
    }
}



