<?php
namespace TxCore;

/**
 * User interface.
 */
class Group
{
	private $id;

	private $name;

	/**
	 * Users in group.
	 * @var User[]
	 */
	private $users;

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getId()
	{
		return $this->id;
	}

	/**
	 * @param mixed $id
	 */
	public function _setId($id)
	{
		$this->id = Util::toInt($id);
	}


	public function validate()
	{
		$messages = array();
		if (is_empty($this->getName())) {
			$messages[] = new Message('グループ名を入力してください。');
		} else if (mb_strlen($this->getName()) > 64) {
			$messages[] = new Message('グループ名は64文字以内で入力してください。');
		}
		return $messages;
	}

	public function create(User $by)
	{
		Engine::factory()->createGroup($this, $by);
	}
	public function update(User $by)
	{
		Engine::factory()->updateGroup($this, $by);
	}
	public function delete(User $by)
	{
		Engine::factory()->deleteGroup($this, $by);
	}

	/**
	 * @return User[]
	 */
	public function &getUsers()
	{
		if ($this->users === null) {
			$this->users = Engine::factory()->getGroupUsers($this);
		}

		return $this->users;
	}

	/**
	 * (overload)
	 * @param array $userIds
	 */
	public function replaceUsers_(array $userIds)
	{
		Engine::factory()->replaceGroupUsers_($this, $userIds);
	}

}
