<?php
namespace TxCore;

use TxCore\Math\MathContext;

use TxCore\Engine;
use TxCore\Column;

/**
 * Table.
 */
class Table
{
	const RELEASE = 'release';
	const TRUNK = 'trunk';

	private $id;
	private $name;
	private $description;
	private $icon;
	private $isPublic = false;
	private $isApiEnabled = false;
	private $apiKey;
	private $isWriteApiEnabled = false;
	private $writeApiKey;
	private $ownerId;
	private $owner;
	private $columns = null;
	private $removedColumns = array();

    /**
     * @var null|ViewColumn[]
     */
	private $viewColumns = null;
	private $roundType;
	private $roundScale;
	private $revision;
	private $isTrunk ;
	private $permissions = null;
	private $permissionControl = false;

	private $penddingRows = array();

	private $isCommentEnabled = false;
	private $isCalendarEnabled = false;
    private $isReadCheckEnabled = false;

	private $isSixColumnsEnabled = false;

	/**
	 * @var CalendarSettings
	 */
	private $calendarSettings;

    /**
     * @var ReadCheckSettings
     */
    private $readCheckSettings;

	private $isCommentFollowingEnabled = false;
	private $notificationViewColumns = null;
	private $notificationBasicRules = null;
	private $notificationConditionalRules = null;
	private $notificationBasicRecipientsCache = null;
	private $readCheckUsersCache = null;

	private $isChild = null;
	private $isParent = null;

	public function __construct($id = null)
	{
		if ($id !== null) {
			$this->id = Util::toInt($id);
		} else {
			$this->id = Engine::factory()->generateObjectId();
		}
	}


	public function _setId($value)
	{
		$this->id = Util::toInt($value);
	}
	public function getId()
	{
		return $this->id;
	}

	public function setName($value)
	{
		$this->name = $value;
	}

    /**
     * @return ViewColumn[]
     */
	public function &getViewColumns()
	{
		if ($this->viewColumns === null) {
			$this->viewColumns = Engine::factory()->getViewColumns($this);
		}
		return $this->viewColumns;
	}

//	public function addViewColumn(Column $column)
//	{
//		$viewColumns =&  $this->getViewColumns();
//		$viewColumns[] = $column;
//	}

    /**
     * @param int $offset
     * @return Column
     * @throws TxCoreException
     */
	public function &getViewColumn($offset)
	{
		$viewColumns = &$this->getViewColumns();
		if (!isset($viewColumns[$offset]))
			throw new TxCoreException(sprintf("Undefined offset %d for view columns", $offset),
				TxCoreException::INTERNAL);
        $column = $viewColumns[$offset]->getColumn();
		return $column;
	}

	/**
	 * @return Filter[]
	 */
	public function getFilters()
	{
		return Engine::factory()->getFilters($this);
	}

    /**
     * @param $id
     * @param bool $silently
     * @return Filter
     * @throws TxCoreException
     */
	public function getFilterFor($id, $silently = false)
	{
		return Engine::factory()->getFilter($this, $id, $silently);
	}

	public function saveFilter(Filter $filter, User $by)
	{
		$filter->_setParent($this);
		Engine::factory()->saveFilter($filter, $by);
	}

	/**
	 * @param ViewColumn[]|Column[] $columns
	 */
	public function setViewColumns($columns)
	{
        if (is_not_empty($columns) && $columns[0] instanceof Column) {
            $self = $this;
            $columns = array_map(function(Column $column) use ($self) {
                $viewColumn = new ViewColumn($self);
                $viewColumn->setColumn($column);
                return $viewColumn;
            }, $columns);
        }

		$this->viewColumns = $columns;
	}

	public function setPermissions($permissions)
	{
		$this->permissions = $permissions;
	}
	public function _addPenddingRow(Row $row)
	{
		$this->penddingRows[] = $row;
	}
	public function allows($action, User $toUser)
	{
		$permissionControl = $this->getPermissionControl();

		switch ($action) {
			case 'select':	// search etc.
				if($permissionControl){
					// ログインユーザの権限情報を取得
					$permission = $this->getPermissionForUserId($toUser->getId(), true);
					if(!$toUser->isAdmin() && ($this->getOwner() === null || $this->getOwner()->getId() != $toUser->getId())
					&& (is_null($permission) || !$permission->getIsReadable())){
						return false;
					}
				}
				if ($toUser->isAdmin() ||
					($this->getOwner() !== null && $this->getOwner()->getId() == $toUser->getId()) ||
					$this->isPublic()) {
					return true;
				}
				break;
			case 'insert':
			case 'update':
			case 'delete':
			if($permissionControl){
					$permission = $this->getPermissionForUserId($toUser->getId(), true);
					if(!$toUser->isAdmin() && ($this->getOwner() === null || $this->getOwner()->getId() != $toUser->getId())
					&& (is_null($permission) || !$permission->getIsWritable())){
						return false;
					}
				}
				if ($toUser->isAdmin() ||
					($this->getOwner() !== null && $this->getOwner()->getId() == $toUser->getId()) ||
					$this->isPublic()) {
					return true;
				}
				break;
			case 'create':	// 自分のテーブルとして作成することは可能
				return true;
			case 'drop':
			case 'truncate':
			case 'alter':
				if ($toUser->isAdmin() ||
				($this->getOwner() !== null && $this->getOwner()->getId() == $toUser->getId())) {
					return true;
				}
				break;
		}
		return false;
	}
	public function getAllowedActions(User $toUser)
	{
		$actions = array();
		if ($this->allows('select', $toUser))
			$actions[] = 'select';
		if ($this->allows('insert', $toUser))
			$actions[] = 'insert';
		if ($this->allows('update', $toUser))
			$actions[] = 'update';
		if ($this->allows('delete', $toUser))
			$actions[] = 'delete';
		if ($this->allows('create', $toUser))
			$actions[] = 'create';
		if ($this->allows('drop', $toUser))
			$actions[] = 'drop';
		if ($this->allows('drop', $toUser))
			$actions[] = 'alter';
		return $actions;

	}
	public function _setViewColumns($columns)
	{
		$this->viewColumns = $columns;
	}
	public function _setPermissions($permissions)
	{
		$this->permissions = $permissions;
	}
	public function getName()
	{
		return $this->name;
	}
	public function setDescription($value)
	{
		$this->description = $value;
// 		$this->getWorkTable()->setDescription($value);
	}
	public function getDescription()
	{
		return $this->description;
	}
// 	public function _setSaved($value)
// 	{
// 		$this->isSaved = $value;
// 	}
// 	public function isSaved()
// 	{
// 		return $this->isSaved;
// 	}
// 	public function setDefType($value)
// 	{
// 		$this->defType = $value;
// 	}
// 	public function getDefType()
// 	{
// 		return $this->defType;
// 	}
	public function _setApiKey($value)
	{
		$this->apiKey = $value;
	}
	public function _setWriteApiKey($value)
	{
		$this->writeApiKey = $value;
	}
	/**
	 * @return \TxCore\Icon
	 */
	public function getIcon()
	{
		return $this->icon;
	}
	public function setIcon(Icon $value = null)
	{
		$this->icon = $value;
	}
	public function getApiKey()
	{
		return $this->apiKey;
	}
	public function setApiKey($value)
	{
		$this->_setApiKey($value);
	}
	public function getWriteApiKey()
	{
		return $this->writeApiKey;
	}
	public function setWriteApiKey($value)
	{
		$this->_setWriteApiKey($value);
	}
	public function isApiEnabled()
	{
		return $this->isApiEnabled;
	}
	public function setApiEnabled($value)
	{
		$this->isApiEnabled = Util::toBool($value);
	}
	public function isWriteApiEnabled()
	{
		return $this->isWriteApiEnabled;
	}
	public function setWriteApiEnabled($value)
	{
		$this->isWriteApiEnabled = Util::toBool($value);
	}
	public function isCommentEnabled()
	{
		return $this->isCommentEnabled;
	}
	public function setCommentEnabled($value)
	{
		$this->isCommentEnabled = Util::toBool($value);
	}
	public function isCalendarEnabled()
	{
		return $this->isCalendarEnabled;
	}
	public function setCalendarEnabled($value)
	{
		$this->isCalendarEnabled = Util::toBool($value);
	}
	public function isSixColumnsEnabled()
	{
		return $this->isSixColumnsEnabled;
	}
	public function setSixColumnsEnabled($value)
	{
		$this->isSixColumnsEnabled = Util::toBool($value);
	}

    /**
     * @return boolean
     */
    public function isReadCheckEnabled()
    {
        return $this->isReadCheckEnabled;
    }

    /**
     * @param boolean $isReadCheckEnabled
     */
    public function setReadCheckEnabled($isReadCheckEnabled)
    {
        $this->isReadCheckEnabled = $isReadCheckEnabled;
    }


// 	public function _setWorkTableId($value)
// 	{
// 		$this->workTableId = Util::toInt($value);
// 	}
// 	/**
// 	 * @return Table
// 	 */
// 	public function _getWorkTable()
// 	{
// 		return $this->getWorkTable();
// 	}
// 	/**
// 	 * @return WorkTable
// 	 */
// 	public function getWorkTable()
// 	{
// 		if ($this->workTableId != null &&
// 			$this->workTable == null) {
// 			$this->workTable = Engine::factory()->getTable($this->workTableId);
// 		}
// 		return $this->workTable;
// 	}
	public function setPublic($value)
	{
		$this->isPublic = Util::toBool($value);
	}
	public function isPublic()
	{
		return $this->isPublic;
	}
	public function setPermissionControl($value)
	{
		$this->permissionControl = Util::toBool($value);
	}
	public function getPermissionControl()
	{
		return $this->permissionControl;
	}
	public function _setOwnerId($value)
	{
		$this->ownerId = Util::toInt($value);
		$this->owner = null;	// Clear cache
	}
	/** @return User */
	public function &getOwner()
	{
		if ($this->ownerId !== null && $this->owner === null) {
			$this->owner = Engine::factory()->getUser($this->ownerId, false, true);
		}
		return $this->owner;
	}
	public function _setRevision($value)
	{
		$this->revision = Util::toInt($value);
	}
	public function getRevision()
	{
		return $this->revision;
	}
	/**
	 * @param int $offset
	 * @return \TxCore\Column
	 */
	public function &getColumn($offset)
	{
		$columns = $this->getColumns();
		if (!isset($columns[$offset]))
			throw new TxCoreException(sprintf("Undefined offset %d for columns", $offset),
				TxCoreException::INTERNAL);

		return $columns[$offset];
	}
// 	/**
// 	 * @param int $offset
// 	 * @return Column
// 	 */
// 	public function &findColumnBy($type)
// 	{
// 	}

	public function release(User $by)
	{
		return Engine::factory()->releaseTable($this, $by);
	}

	public function discardChanges(User $by)
	{
		return Engine::factory()->discardTableChanges($this, $by);
	}

// 	public function rollback(User $by)
// 	{
// 		return Engine::factory()->rollbackTable($this, $by);
// 	}

	/**
	 * @param TxCore\Column[]
	 */
	public function _setColumns($columns)
	{
		$this->columns = $columns;
	}
	/**
	 * @return \TxCore\Column[]
	 */
	public function &getColumns($filter = array())
	{
		if ($this->columns === null) {
			if ($this->id !== null) {
				$this->columns = Engine::factory()->getTableColumns($this);
			} else {
				$this->columns = array();
			}
		}
		if (is_not_empty($filter)) {
			$columns = array_grep($this->columns, function(Column $column) use ($filter) {
				if (isset($filter['type']) && $filter['type'] != $column->getType())
					return false;
				if (isset($filter['code']) && $filter['code'] != $column->getCode())
					return false;
				if (isset($filter['inputable']) && $filter['inputable'] != $column->isInputable())
					return false;
				if (isset($filter['listable']) && $filter['listable'] != $column->isListable())
					return false;
				return true;
			});
			return $columns;
		}
		return $this->columns;
	}
// 	/**
// 	 * @return array
// 	 */
// 	public function getColumnCodeMap()
// 	{
// 		$map = array();
// 		foreach ($this->columns as $column) {
// 			if (is_not_empty($column->getCode())) {
// 				$map[$column->getCode()] = $column;
// 			}
// 		}
// 		return $map;
// 	}

	/**
	 * @return Column
	 */
	public function addColumn($columnType)
	{
// 		$column = Column::create($columnType, $this);
// 		$columns[] = $column;
// 		$column = Column::create($columnType, $this);
// var_dump($column->getId());
		return Column::create($columnType, $this);
	}

	/**
	 * @param Column $column
	 * @return Column Returns removed column.
	 * @throws TxCoreException Throws if columns dose not exist.
	 */
	public function removeColumn(Column $column)
	{
		if ($column->getParent()->getId() != $this->getId() ||
			$column->getParent()->getRevision() != $this->getRevision()) {
			throw new TxCoreException(
				sprintf("Parent mismatched"),
				TxCoreException::INTERNAL);
		}

		$columns =& $this->getColumns();
		foreach ($columns as $i => $column_) {
			if ($column->getId() === $column_->getId()) {
				$this->removedColumns[] = $column;
				unset($columns[$i]);
				return $column;
			}
		}
		throw new TxCoreException(
			sprintf("Column id %d dose not found on this table %d.", $id, $this->id),
			TxCoreException::INTERNAL);
	}

	/**
	 * @return \TxCore\Column[]
	 */
	public function _addColumn(Column $column)
	{
		$columns =& $this->getColumns();
		$columns[] = $column;
	}

	/**
	 * @return \TxCore\Column[]
	 */
	public function _getRemovedColumns()
	{
		return $this->removedColumns;
	}

	/**
	 * @param int $id Column Id
	 * @throws TxCoreException Throws if removed column dose not exist.
	 * @return \TxCore\Column
	 */
	public function getRemovedColumnFor($id, $silently = false)
	{
		$columns = $this->_getRemovedColumns();
		foreach ($columns as $column) {
			if ($column->getId() === Util::toInt($id))
				return $column;
		}
		if ($silently) {
			return null;
		}
		throw new TxCoreException(
			sprintf("Removed column id %d dose not found on table %d.", $id, $this->getId()),
			TxCoreException::INTERNAL);
	}

	/**
	 * @param Column $column
	 * @return Column Returns remove cancelled column.
	 * @throws TxCoreException Throws if column dose not exist.
	 */
	public function restoreRemovedColumn($column)
	{
		$columns = $this->removedColumns;
		foreach ($columns as $i => $column_) {
			if ($column->getId() == $column_->getId()) {
				$this->_addColumn($column);
				unset($this->removedColumns[$i]);
				return $column;
			}
		}
		throw new TxCoreException(
				sprintf("Removed column id %d dose not found on this table %d.", $id, $this->getId()),
				TxCoreException::INTERNAL);
	}

	/**
	 * @return Column__Rownum
	 */
	public function getRownum()
	{
		foreach ($this->getColumns() as $column) {
			if ($column->getType() === Column::ROWNUM) {
				return $column;
			}
		}
		return null;
	}


	/**
	 * @return bool
	 */
	public function hasPendingRelease()
	{
		return Engine::factory()->hasPendingRelease($this);
	}

	public function create(User $by)
	{
		Engine::factory()->createTable($this, $by);
	}

	/**
	 * @param Criteria $criteria
	 * @param User $by
	 * @param int $offset
	 * @param int $limit
	 * @return \TxCore\List_|Row[]
	 */
	public function search(Criteria $criteria, User $by, $offset = null, $limit = null)
	{
		return Engine::factory()->searchTable($this, $criteria, $by, $offset, $limit);
	}

	public function fetch(Criteria $criteria, $callable)
	{
		return Engine::factory()->featchTable($this, $criteria, $callable);
	}

	public function drop(User $by)
	{
		return Engine::factory()->dropTable($this, $by);
	}

	/**
	 * @param Row $row
	 * @param user $by
	 */
	public function insertRow(Row $row, User $by)
	{
// 		$this->addRow($row);
		$this->insertRows($by);
	}
	public function insertRows(User $by, $notify = false)
	{
		foreach ($this->penddingRows as $row)
			Engine::factory()->insertRow($row, $by, $notify);
		$this->penddingRows = array();
	}
// 	public function truncate(User $by)
// 	{
// 		Engine::factory()->truncateTable($this, $by);
// 	}
// 	public function deleteRow($row, $by)
// 	{
// 		return Engine::factory()->deleteTableRow($this, $row, $by);
// 	}
	public function getRowFor($rowId, User $by = null, $useRownum = false, $silently = false, $recursively = false)
	{
		return Engine::factory()->getRow($this, Util::toInt($rowId), $by, $useRownum, $silently, $recursively);
	}

	/**
	 * @return \TxCore\Row
	 */
	public function addRow()
	{
		return new Row($this);
// 		$row->_setParent($this);
// 		foreach ($this->getColumns() as $column)
// 			$column->handleDefaultValue($row);
// 		foreach ($this->getColumns() as $column)
// 			$column->handleValidate($row);

	}

	/**
	 * @param int $id Column Id
	 * @throws TxCoreException Throws if column dose not exist.
	 * @return \TxCore\Column
	 */
	public function getColumnFor($id, $silently = false)
	{
		$columns = $this->getColumns();
		foreach ($columns as $column) {
			if ($column->getId() === Util::toInt($id))
				return $column;
		}
		if ($silently) {
			return null;
		}
		throw new TxCoreException(
			sprintf("Column id %d dose not found on table %d.", $id, $this->id),
			TxCoreException::INTERNAL);
	}

	public function setRoundType($value)
	{
		$this->roundType = $value;
	}
	public function getRoundType()
	{
		return $this->roundType;
	}
	public function setRoundScale($value)
	{
		$this->roundScale = Util::toInt($value);
	}
	public function getRoundScale()
	{
		return $this->roundScale;
	}
	public function saveChanges(User $by)
	{
		return Engine::factory()->saveTableChanges($this, $by);
	}

	/**
	 * Re-generate API key.
	 */
	public function resetApiKey()
	{
		$this->_setApiKey(Util::generateApiKey());
	}
	public function resetWriteApiKey()
	{
		$this->_setWriteApiKey(Util::generateApiKey());
	}

// 	public function isWork()
// 	{
// 		return $this->defType == self::WORK;
// 	}

// 	public function getViewColumns()
// 	{
// 		return Engine::factory()->findTableRow($this, $rowId);
// 	}

// 	public function insertRow(Row $row)
// 	{
// 		return Engine::factory()->insertTableRow($this, $row, $by);
// 	}


// 	public function _set($property, $value)
// 	{
// 		$this->{$property} = $value;
// 	}
// 	/**
// 	 * @param int $id
// 	 * @return Column
// 	 * @throws Exception
// 	 */
// 	public function findColumn($id)
// 	{
// 		if ($this->ColumnsMap == null) {
// 			$this->ColumnsMap = array();
// 			$Columns = $this->getColumns();
// 			foreach ($Columns as $Column) {
// 				$this->ColumnsMap[$Column->getId()] = $Column;
// 			}
// 		}
// 		if  (!isset($this->ColumnsMap[$id])) {
// 			throw new Exception(sprintf('Table has not column id:%d.', $id));
// 		}
// 		return $this->ColumnsMap[$id];
// 	}

	/**
	 * @return Message[]
	 */
	public function validate()
	{
		$message = array();
		$columns = $this->getColumns();
		foreach ($columns as $column) {	/* @var $column Column */
			$message = array_merge($message, $column->handleValidateProperties());
		}
		return $message;
	}

	/**
	 * @return Message[]
	 */
	public function validateRelease()
	{
		$message = Engine::factory()->validateTableRelease($this);
		if (is_not_empty($message)) {
			return $message;
		}

		$columns = $this->getColumns();
		foreach ($columns as $column) {	/* @var $column Column */
			$message = array_merge($message, $column->handleValidateRelease());
		}
		return $message;
	}

	/**
	 * @return MathContext
	 */
	public function getMathContext()
	{
		return new MathContext($this->getRoundScale(), $this->getRoundType());
	}

// 	/** @return array of Reference */
// 	public function getReferrers()
// 	{
// 		return Engine::factory()->getReferrersByTable($this);
// 	}

	/** @return array of Table */
	public function getReferrerTables()
	{
		return Engine::factory()->getReferrerTablesByTable($this);
	}

	/**
	 * Lock table.
	 */
	public function lock()
	{
		return Engine::factory()->lockTable($this);
	}

	/**
	 * @return Permission[]
	 */
	public function getPermissions()
	{
		if ($this->permissions === null) {
			$this->permissions = Engine::factory()->getPermissions($this);
		}
		return $this->permissions;
	}

	/**
	 * @param int $id User Id
	 * @throws TxCoreException Throws if permission dose not exist.
	 * @return \TxCore\Permission
	 */
	public function getPermissionForUserId($id, $silently = false)
	{
		$permissions = $this->getPermissions();
		foreach ($permissions as $permission) {
			if ($permission->getUserId() === Util::toInt($id))
				return $permission;
		}
		if ($silently) {
			return null;
		}
		throw new TxCoreException(
			sprintf("User id of permission %d dose not found on table %d.", $id, $this->id),
			TxCoreException::INTERNAL);
	}

	/**
	 * @return CalendarSettings
	 */
	public function getCalendarSettings()
	{
		return $this->calendarSettings;
	}

    /**
     * @return ReadCheckSettings
     */
    public function getReadCheckSettings()
    {
        return $this->readCheckSettings;
    }

    /**
	 * @param CalendarSettings $settings
	 * @return CalendarSettings
	 */
	public function setCalendarSettings(CalendarSettings $settings)
	{
		$this->calendarSettings = $settings;
	}

    /**
     * @param ReadCheckSettings $readCheckSettings
     */
    public function setReadCheckSettings($readCheckSettings)
    {
        $this->readCheckSettings = $readCheckSettings;
    }

	public function setCommentFollowingEnabled($value)
	{
		$this->isCommentFollowingEnabled = Util::toBool($value);
	}
	public function isCommentFollowingEnabled()
	{
		return $this->isCommentFollowingEnabled;
	}

    /**
     * @return Column[]
     */
	public function &getNotificationViewColumns()
	{
		if ($this->notificationViewColumns === null) {
			$this->notificationViewColumns = Engine::factory()->getNotificationViewColumns($this);
		}
		return $this->notificationViewColumns;
	}

	/**
	 * @param Column[] $columns
	 */
	public function setNotificationViewColumns($columns)
	{
		$this->notificationViewColumns = $columns;
	}

    /**
     * @return NotificationBasicRule[]
     */
	public function &getNotificationBasicRules()
	{
		if ($this->notificationBasicRules === null) {
			$this->notificationBasicRules = Engine::factory()->getNotificationBasicRules($this);
		}
		return $this->notificationBasicRules;
	}

	/**
	 * @param NotificationBasicRule[] $rules
	 */
	public function setNotificationBasicRules($rules)
	{
		$this->notificationBasicRules = $rules;
	}

    /**
     * @return NotificationConditionalRule[]
     */
	public function &getNotificationConditionalRules()
	{
		if ($this->notificationConditionalRules === null) {
			$this->notificationConditionalRules = Engine::factory()->getNotificationConditionalRules($this);
		}
		return $this->notificationConditionalRules;
	}

	/**
	 * @param NotificationConditionalRule[] $rules
	 */
	public function setNotificationConditionalRules($rules)
	{
		$this->notificationConditionalRules = $rules;
	}

	public function updateNotificationBasicRecipientsCache($action)
	{
		$cache = array('users'=>array(), 'columns'=>array());

		$users = array();
		$columns = array();
		$groups = array();
		$usersInGroups = array();

		$rules = $this->getNotificationBasicRules();

		// extract users and groups and columns
		/* @var $rule \TxCore\NotificationBasicRule */
		foreach ($rules as $rule) {
			if ($action == 'row_insert') {
				if (!$rule->isOnRowInsert()) continue;
			} else if ($action == 'row_update') {
				if (!$rule->isOnRowUpdate()) continue;
			} else if ($action == 'comment_insert') {
				if (!$rule->isOnCommentInsert()) continue;
			}
			if($rule->getTargetingType() ==
				NotificationRule::TARGETING_USERGROUP) {
				foreach ($rule->getTargets() as $entity) {
					/* @var $entity User|Group */
					if ($entity instanceof User) {
						$users[$entity->getId()] = $entity;
					} else if ($entity instanceof Group) {
						$groups[$entity->getId()] = $entity;
						$usersInGroups[$entity->getId()] = array();
					}
				}
			} else if($rule->getTargetingType() ==
				NotificationRule::TARGETING_COLUMN) {
				foreach ($rule->getTargets() as $entity) {
					/* @var $entity Column */
					$columns[$entity->getId()] = $entity;
				}
			}
		}

		// extract users from groups
		foreach ($groups as $id => $entity) {
			$usersInGroup = Engine::factory()->getGroupUsers($entity);
			foreach ($usersInGroup as $user) {
				$users[$user->getId()] = $user;
			}
		}

		foreach ($users as $user) {
			if ($this->allows('select', $user)) {
				$cache['users'][$user->getId()] = $user;
			}
		}
		foreach ($columns as $column) {
			$cache['columns'][$column->getId()] = $column;
		}

		$this->notificationBasicRecipientsCache[$action] = $cache;
	}

	public function getNotificationBasicRecipientsCache($action)
	{
		if ($this->notificationBasicRecipientsCache === null) {
			$this->notificationBasicRecipientsCache = array();
		}
		if (!isset($this->notificationBasicRecipientsCache[$action])) {
			$this->updateNotificationBasicRecipientsCache($action);
		}
		return $this->notificationBasicRecipientsCache[$action];
	}


	public function expandNotificationBasicRecipients(
		$action, User $by, Row $row, Comment $comment = null)
	{
		$cache = $this->getNotificationBasicRecipientsCache($action);
		$recipients = $cache['users'];
		$columns = $cache['columns'];
		$users = array();   /** @var User[] $users */
		$groups = array();
		$userInGroups = array();

		/* @var $row \TxCore\Row */

		// extract users from column values
		foreach ($columns as $column) {
			$value = $row->getValue($column);
			if (!is_array($value)) {
				$value = array($value);
			}
			foreach ($value as $user) { /** @var $user User */
				if ($user !== null && !$user->_isDeleted()) {
					$users[$user->getId()] = $user;
				}
			}
		}

		if ($comment) {
			log_debug('comment1:'.$comment->getText());
			// extract users and groups from comment
			foreach ($comment->getNotificationTargets() as $entity) {
				/* @var $entity User|Group */
				if ($entity instanceof User) {
					$users[$entity->getId()] = $entity;
				} else if ($entity instanceof Group) {
					$groups[$entity->getId()] = $entity;
					$usersInGroups[$entity->getId()] = array();
				}
			}
			// get users who posted so far
			if ($this->isCommentFollowingEnabled()) {
				foreach ($row->getComments() as $cmt) {
					$postedUser = $cmt->getPostedUser();
                    if ($postedUser !== null) {
                        $users[$postedUser->getId()] = $postedUser;
                    }
				}
			}
		}

		// extract users from groups
		foreach ($groups as $id => $entity) {
			$usersInGroup = Engine::factory()->getGroupUsers($entity);
			foreach ($usersInGroup as $user) {
				$users[$user->getId()] = $user;
			}
		}

		foreach ($users as $user) {
			if (!array_key_exists($user->getId(), $recipients)) {
				if ($this->allows('select', $user)) {
					$recipients[$user->getId()] = $user;
				}
			}
		}

		if (isset($recipients[$by->getId()])) {
			unset($recipients[$by->getId()]);
		}

		return $recipients;
	}

	public function isChild($trunkOrRelease = false)
	{
		if ($this->isChild === null ) {
			$this->isChild = Engine::factory()->isTableChild(
				$this, $trunkOrRelease);
		}
		return $this->isChild;
	}

	public function isParent($trunkOrRelease = false)
	{
		if ($this->isParent === null ) {
			$this->isParent = Engine::factory()->isTableParent(
				$this, $trunkOrRelease);
		}
		return $this->isParent;
	}

	public function searchForRelationships(Row $row, Column $column, Criteria $criteria, User $by, $offset = null, $limit = null)
	{
		return Engine::factory()->searchRelationships($this, $row, $column, $criteria, $by, $offset, $limit);
	}

	public function getRelationshipRows(Row $row, Column $column, User $by)
	{
		return Engine::factory()->getRelationshipRows($this, $row, $column, $by);
	}

	/** @return array of Table */
	public function getParentTables()
	{
		return Engine::factory()->getParentTablesByTable($this);
	}

	public function hasUniqueConstraint()
	{
		foreach ($this->getColumns() as $column) {
			if ($column->hasProperty('isUnique') && $column->isUnique()){
				return true;
			}
		}
		return false;
	}

    /**
     * @param User $user
     * @return bool
     */
    public function isReadCheckTarget(User $user)
    {
        if (!$this->isReadCheckEnabled()) {
            return false;
        }

        if (is_empty(
            $this->getReadCheckSettings()->getCheckTargets())) {
            return true;
        }

        foreach ($this->getReadCheckSettings()->getCheckTargets() as
                 $entity) { /** @var $entity Group|User */
            if ($entity instanceof User) {
                if ($entity->getId() === $user->getId())
                    return true;
            } else if ($entity instanceof Group) {
                foreach ($entity->getUsers() as $groupUser)
                    if ($groupUser->getId() === $user->getId())
                        return true;
            } else {
                assert(false);
            }
        };
        return false;
    }

    public function &getAllReadCheckUsers()
    {
        if ($this->readCheckUsersCache === null) {
            $list = Engine::factory()->searchReadCheckUsers($this, 0, PHP_INT_MAX);
            $this->readCheckUsersCache = $list->getRows();
        }
        return $this->readCheckUsersCache;
    }
}

