<?php
namespace TxCore;
use TxCore\Criteria;

use TxCore\Math\Math;

use Image\Resizer;

use Barge\DB\DBClient;
use Barge\Proxy;

use TxCore\User;
use TxCore\Table;
use TxCore\Column;
use TxCore\Row;
use TxCore\Filter;
use TxCore\List_;
use TxCore\TxCoreException;
use TxCore\Reference;
use TxCore\Relationship;
use TxCore\Util;
use TxCore\Permission;

/**
 * Interface of DB engine.
 */
class Engine
{
	/**
	 * Table revision names.
	 * @var string
	 */
	const RELEASE = 'release';
	const TRUNK = 'trunk';

	/**
	 * The singleton instance.
	 * @var Engine
	 */
	private static $instance;

	/**
	 * @var \Barge\DB\DBClient
	 */
	private $db;

	/**
	 * @var Storage
	 */
	private $storage;

	/**
	 * @var Queries
	 */
	private $queries;

	/**
	 * Current timestamp
	 * @var int
	 */
	private $now;

    /**
     * @var Intent
     */
    private $intent;

	/**
	 * @return Engine
	 */
	public static function factory()
	{
		if (self::$instance === null)
			self::$instance = new Engine();
		return self::$instance;
	}

	/**
	 * The constructor.
	 */
	private function __construct()
	{
		$this->db = DBClient::connect();
		$this->queries = new Queries($this->db);
		$this->storage = new Storage();
		// Note : ファイル操作の確定をコミット後に行うためにトランザクションをフック
		$this->db->registerCommitHook(array($this->storage, 'flush'));
	}

	/**
	 * [[[ このメソッドで得られるタイムスタンプを関数の外でdateを使いフォーマットしないこと! ]]
	 * @return int|string Unixtimestamp
	 */
	public function getNow()
	{
		$time = $this->now ? $this->now : time();
		if (date_default_timezone_get() === 'UTC') {
			$time = $time + ( 9 * 60 * 60 );
		}
		return $time;
	}

	/**
	 * @param int $now Unixtimestamp
	 */
	public function setNow($now)
	{
		$this->now = $now;
	}

    /**
     * @return Intent
     */
    public function getIntent()
    {
        return $this->intent;
    }

    /**
     * @param Intent $intent
     */
    public function setIntent(Intent $intent)
    {
        $this->intent = $intent;
    }

	/**
	 * テンプレートの一覧を返す
	 * @return Template[]
	 */
	public function getTemplates()
	{
		$raws = $this->queries->selectTemplateTableDefs();
		$tables = array();
		foreach ($raws as $raw) {
			$tables[] = $this->_mapTemplate($raw);
		}
		return $tables;
	}

	/**
	 * テンプレートを返す
	 * @param int $id Template id
	 * @return Template
	 * @throws TxCoreException
	 */
	public function getTemplate($id)
	{
		$tables = $this->queries->selectTemplateTableDef($id);
		if (is_empty($tables))
			throw new TxCoreException(
				sprintf('Template %d dose not exist.', $id), TxCoreException::ROW_NOT_FOUND);

		return $this->_mapTemplate($tables[0], false);
	}

	/**
	 * @param Table $table
	 * @return Filter[]
	 */
	public function getFilters(Table $table)
	{
		$filters = $this->queries->selectFilters($table);
		$mapedFilters = array();
		foreach ($filters as $filter) {
			$mapedFilters[] = $this->_mapFilter($filter, $table);
		}
		return $mapedFilters;
	}

	/**
	 * フィルターを取得する
	 * @param Table $table
	 * @param int $id Filter Id
	 * @param bool $silently
	 * @return Filter
	 * @throws TxCoreException
	 */
	public function getFilter(Table $table, $id, $silently = false)
	{
		$filter = $this->queries->selectFilter($id);
		if ($filter === null) {
			if ($silently)
				return null;
			throw new TxCoreException(
				sprintf('Filter %d dose not exist.', $id), TxCoreException::ROW_NOT_FOUND);
		}
		return $this->_mapFilter($filter, $table);
	}

	/**
	 * フィルターを保存する
	 * @param Filter $filter
	 * @param User $by
	 * @return Filter
	 * @throws TxCoreException
	 */
	public function saveFilter(Filter $filter, User $by)
	{
		$this->checkAutholity($filter->getParent(), 'select', $by);

		if ($filter->getId() == null) {
			$id = $this->queries->insertFilter($filter, $by);
			$filter->_setId($id);
		} else {
			$this->queries->updateFilter($filter, $by);
		}
	}

	/**
	 * フィルターを削除する
	 * @param Filter $filter
	 * @param User $by
	 */
	public function removeFilter(Filter $filter, User $by)
	{
		$this->checkAutholity($filter->getParent(), 'select', $by);

		$this->queries->deleteFilter($filter, $by);
		$filter->_setId(null);
	}

    /**
     * テーブルをテンプレートに変換する
     * @param Table $table
     * @return Template
     * @throws TxCoreException
     */
	public function getTemplateFromTable(Table $table)
	{
		$tables = $this->db->query("
			select * from table_def
			where
				id = ? and
				exists (select 1 from release_control
					where table_def.revision = release_control.release_revision and
						table_def.id = release_control.table_id)
			", array($table->getId()));

		if (is_empty($tables))
			throw new TxCoreException(
				sprintf('Template %d dose not exist.', $table->getId()), TxCoreException::ROW_NOT_FOUND);

		return $this->_mapTemplate($tables[0]);
	}

	/**
	 * ユーザー情報を取得する
	 * @param int $id User id
	 * @param bool $byLoginId
	 * @param bool $silently
	 * @return User
	 * @throws TxCoreException
	 */
	public function getUser($id, $byLoginId = false, $silently = false)
	{
		$raws = $this->queries->selectUser($id, $byLoginId);
		if (!isset($raws[0])) {
			if ($silently) {
				return null;
			}
			throw new TxCoreException(sprintf("User %d dose not exist.", $id),
				TxCoreException::ROW_NOT_FOUND);
		}
		return $this->_mapUser($raws[0]);
	}

	/**
	 * ユーザーを削除する
	 * @param User $user
	 * @param User $by
	 * @return int affected rows
	 */
	public function deleteUser(User $user, User $by)
	{
		// 権限チェック
		$this->checkAutholityForUser($user, 'drop', $by);

		$this->queries->deleteNotificationReceiptsByRecipient($user);
		$this->queries->deleteReadLogsByUser($user->getId());
		$this->queries->deleteUser($user);
		$user->_setId(null);
	}

	/**
	 * ユーザーを作成する
	 * @param User $user
	 * @param User $by
	 * @throws TxCoreException
	 */
	public function createUser(User $user, User $by)
	{
		// 権限チェック
		$this->checkAutholityForUser($user, 'create', $by);

		// 既に登録されている場合は、エラー
		if ($this->findUserByLoginId($user->getLoginId()) !== null) {
			throw new TxCoreException('Login id was already registered',
				TxCoreException::LOGIN_ID_WAS_DUPLICATED);
		}

		$affected = $this->queries->updateUser($user);
		if ($affected == 0) {
			throw new TxCoreException('User has not updated.',
				TxCoreException::ROW_NOT_FOUND);
		}
		$user->_setId($this->db->lastInsertId('id'));
	}


	/**
	 * グループを取得する
	 * @param int $id User id
	 * @param bool $silently
	 * @return Group
	 * @throws TxCoreException
	 */
	public function getGroup($id, $silently = false)
	{
		$rows = $this->queries->selectGroup($id);
		if (!isset($rows[0])) {
			if ($silently) {
				return null;
			}
			throw new TxCoreException(sprintf("Group %d dose not exist.", $id),
				TxCoreException::ROW_NOT_FOUND);
		}
		return $this->_mapGroup($rows[0]);
	}

	/**
	 * グループを名称から取得する
	 * @param $name
	 * @return Group
	 */
	public function findGroupByName($name)
	{
		$row = $this->queries->findGroupByName($name);
		return $row ? $this->_mapGroup($row) : null;
	}

	/**
	 * グループを削除する
	 * @param Group  $group
	 * @param User $by
	 * @return int affected rows
	 * @throws TxCoreException
	 */
	public function deleteGroup(Group $group, User $by)
	{
		// 権限チェック
		$this->checkAuthorityForGroup($group, 'drop', $by);

		$this->queries->deleteGroup($group);
		$group->_setId(null);
	}


	/**
	 * ユーザーを作成する
	 * @param Group $group
	 * @param User $by
	 * @throws TxCoreException
	 */
	public function createGroup(Group $group, User $by)
	{
		// 権限チェック
		$this->checkAuthorityForGroup($group, 'create', $by);

		// 既に登録されている場合は、エラー
		if ($this->queries->findGroupByName($group->getName()) !== null) {
			throw new TxCoreException('Login id was already registered',
				TxCoreException::GROUP_NAME_WAS_DUPLICATED);
		}

		$id = $this->queries->insertGroup($group);
		$group->_setId($id);
	}

	/**
	 * グループ情報を更新する
	 * @param Group $group
	 * @param User $by
	 * @throws TxCoreException
	 */
	public function updateGroup(Group $group, User $by)
	{
		// 権限チェック
		$this->checkAuthorityForGroup($group, 'update', $by);

		// 既に登録されている場合は、エラー
		if ($this->queries->findGroupByName($group->getName(), $group->getId()) !== null) {
			throw new TxCoreException('Login id was already registered',
				TxCoreException::GROUP_NAME_WAS_DUPLICATED);
		}

		// タイムスタンプを更新しない場合、affected rows が0になるので注意
		$affected = $this->queries->updateGroup($group);
	}

	/**
	 * @param Table $table
	 * @return bool
	 */
	public function hasPendingRelease(Table $table)
	{
		$ctrls = $this->queries->selectReleaseControl($table);
		return $ctrls[0]->has_pending_release == 1;
	}

	/**
	 * ユーザー情報を更新する
	 * @param User $user
	 * @param User $by
	 * @throws TxCoreException
	 */
	public function updateUser(User $user, User $by)
	{
		// 権限チェック
		$this->checkAutholityForUser($user, 'update', $by);

		// 既に登録されている場合は、エラー
		if (($foundUser = $this->findUserByLoginId($user->getLoginId())) !== null &&
			$foundUser->getId() !== $user->getId()) {
			throw new TxCoreException('Login id was already registered',
				TxCoreException::LOGIN_ID_WAS_DUPLICATED);
		}

		// タイムスタンプを更新しない場合、affected rows が0になるので注意
		$affected = $this->db->update('
			update user
			set login_id = ?,
			password = ?,
			role = ?,
			screen_name = ?,
			profile = ?,
			seq = ?,
			updated_on = current_timestamp
			where user.id = ?
			', array(
				trim_to_null($user->getLoginId()),
				trim_to_null($user->getPassword()),
				trim_to_null($user->getRole()),
				trim_to_null($user->getScreenName()),
				trim_to_null($user->getProfile()),
				trim_to_null($user->getSeq()),
				trim_to_null($user->getId())));
	}

	/**
	 * チェックしユーザー情報を登録または更新する（ログインID）
	 * @param User $user
	 * @param User $by
	 * @throws TxCoreException
	 */
	public function createOrReplaceUser(User $user, User $by)
	{
		// 既に同じログインIDで登録されている場合は更新
		if (($foundUser = $this->findUserByLoginId($user->getLoginId())) !== null &&
			$foundUser->getId() !== $user->getId()) {
			$user->_setId($foundUser->getId());
			$this->updateUser($user, $by);
		} else {
			$this->createUser($user, $by);
		}
	}

	/**
	 * ログインIDとパスワードでユーザーを認証する
	 * @param string $loginId
	 * @param string $password
	 * @return User
	 * @throws \Exception When authentication was failed.
	 */
	public function authenticateUser($loginId, $password)
	{
		$loginId = trim($loginId);
		$password = trim($password);

		$result = array();
		$users = $this->db->query('
			select * from user where login_id = ? and password = ?
			', array($loginId, $password));
		if (is_empty($users)) {
			throw new TxCoreException('Authentication failed.',
				TxCoreException::AUTHENTICATION_FAILED);
		} else {
			return $this->_mapUser($users[0]);
		}
	}

	/**
	 * @param Column $column
	 * @return Option[]
	 * @throws TxCoreException
	 */
	public function getColumnOptions(Column $column)
	{
		if (!$column->hasOptions()) {
			throw new TxCoreException(
				sprintf('Column %s has not options', $column->getType()),
				TxCoreException::INTERNAL);
		}

		$result = array();
		$table = $column->getParent();
		$options = $this->db->query('
			select * from `option` where column_id = ? and revision = ? order by seq
			', array($column->getId(), $table->getRevision()));
		foreach ($options as $option) {
			$result[] = $this->_mapOption($option, $column);
		}
		return $result;
	}

	/**
	 * Row を取得する
	 * @param Table $table
	 * @param int $rowId
	 * @param bool $byRownum
	 * @param bool $silently
	 */
	public function getRow(Table $table, $rowId, User $by = null, $byRownum = false, $silently = false, $recursively = false)
	{
		if ($by !== null) {
			$this->checkAutholity($table, 'select', $by);
		}

		$columnMap = $this->getAllListableColumnsMap($table);

		if ($byRownum) {
			$rownums = $table->getColumns(array('type'=>Column::ROWNUM));
			if (is_empty($rownums)) {
				throw new TxCoreException(sprintf("Table has no rownum column."),
					TxCoreException::ROW_NOT_FOUND);
			}

			$list = $this->db->query('
				select `data`.*, `_row`.version, `_row`.row_no from `data` join (
 					select `row`.* from `row`
 					join `indexes`
    				on (`indexes`.column_id = ? and `indexes`.row_id = row.id)
					where `indexes`.integer_value = ?
 				) as _row on (_row.id = data.row_id)
 				order by  data.column_id',
				array($rownums[0]->getId(), $rowId/*=rownum*/));
		} else {
			$list = $this->db->query('
				select `data`.*, `row`.version, `row`.row_no
				from `row` join `data` on (`row`.id = `data`.row_id) where `data`.row_id = ?
				order by column_id', array($rowId));
		}

		if (is_empty($list)) {
			if ($silently) {
				return null;
			}
			throw new TxCoreException(sprintf("Row dose not found : %d", $rowId),
				TxCoreException::ROW_NOT_FOUND);
		}

		$row = new Row();
		foreach ($columnMap as $column) {
			$row->_setRawValue($column, null);
		}

		foreach ($list as $data) {
			$column = $columnMap[$data->column_id];
			$row->_setId($data->row_id);
			$row->setVersion($data->version);
			$row->setRowNo($data->row_no);
			$row->_setParent($table);
			$row->_setRawValue($column, $data->value);
		}

		if ($recursively) {
			foreach ($table->getColumns() as $column) {
				if ($column->hasRelationship()) {
					$childRows = array();
					$referenceTable = $column->getReferenceTable();
					if ($by === null || $referenceTable->allows('select', $by)) {
						$relationships = $this->selectRelationships($row, $column);
						foreach ($relationships as $relationship) {
							$childRows[] = $this->getRow($referenceTable,
								$relationship->_getTargetRowId(), $by, false, $silently, true);
						}
						$criteria = new Criteria();
						if ($column->getReferenceSortColumn() !== null) {
							$criteria->addSort($column->getReferenceSortColumn(),
								$column->getReferenceSortsReverse());
						}
						$childRows = $this->sortRows($childRows, $criteria);
					}
					$row->setValue($column, $childRows);
				}
			}
		}

		return $row;
	}

	/**
	 * @param Table $table
	 * @param Criteria $criteria
	 * @param User $by
	 * @param int $limit Optional
	 * @param int $offset Optional
	 * @return List_
	 */
	public function searchTable(Table $table, Criteria $criteria, User $by, $offset = null, $limit = null)
	{
		// 参照権限をチェック
		$this->checkAutholity($table, 'select', $by);

		return $this->searchTableInternal($table, $criteria, $offset, $limit);
	}

	/**
	 * @param Table $table
	 * @param Criteria $criteria
	 * @param int $limit Optional
	 * @param int $offset Optional
	 */
	public function searchTableInternal(Table $table, Criteria $criteria, $offset = null, $limit = null)
	{
		$errors = $criteria->validate();
		if (is_not_empty($errors)) {
			throw new TxCoreException('Criteria has any error : '. $errors[0]->getMessage(),
				TxCoreException::VALIDATION_FAILED, $errors);
		}

		if ($criteria->getSubtotal() !== null) {
			return $this->subtotalTable($table, $criteria);
		}

		if ($criteria->getAggregate() !== null) {
			return $this->aggregateTable($table, $criteria);
		}

		$rows = array();
		$listColumns = $this->getSearchHeader($table, $criteria);
		$count = $this->queries->selectQueryCount($table, $criteria);

		if ($count > 0) {
			$this->featchTable($table, $criteria,
				function($row) use (&$rows) {
					$rows[] = $row;
				},
				$offset,
				$limit);
		}

		$list = new List_();
		$list->_setType('table');
		$list->_setRows($rows);
		$list->_setTotalRowCount($count);
		$list->_setOffset($offset ?: 0);
		$list->_setColumns($listColumns);
		$list->_setRowLimit($limit);
		$list->_setTable($table);
		return $list;
	}

	/**
	 * @param Row $row
	 * @param User $by
	 * @throws TxCoreException
	 */
	public function insertRow(Row $row, User $by, $notify = false)
	{
		$table = $row->getParent();
		if ($table->getId() === null) {
			throw new TxCoreException(sprintf('Table could not be created'),
				TxCoreException::INTERNAL);
		}

		// 権限チェック
		$this->checkAutholity($table, 'insert', $by);

		// Revalidation.
		$errors = $row->validate($by);
		if (is_not_empty($errors)) {
			throw new TxCoreException(sprintf('Validation failed : '. $errors[0]->getMessage()),
				TxCoreException::VALIDATION_FAILED,
				$errors);
		}

		// 自動設定（計算式に作成日時などを含む場合があるので、計算の前に実施）
		foreach ($table->getColumns() as $column)	/* @var $column Column */
			$column->handleAutoset($row, new HandlerContext('insert', $by));

		// 計算
		$row->calculate();

		// insert into row
		$rowId = $this->queries->insertRow($table, $row->getRownum());
		$row->_setId($rowId);
		$row->_setParent($table);
		$row->setVersion(1);

		// notification
		if ($notify) {
			$this->notify("row_insert", $by, $table, $row);
		}

		// indexes & data
		$this->queries->insertIndexes($table, $row);
		$this->queries->insertData($table, $row, $by);

		// child rows
		foreach ($table->getColumns() as $column) {	/* @var $column Column */
			if ($column->hasRelationship()) {
				$this->updateRelationships($by, $row, $column);
			}
		}
	}

	/**
	 * @param Table $table
	 * @throws TxCoreException
	 */
	public function lockTable(Table $table)
	{
		try {
// 			$this->db->query('set innodb_lock_wait_timeout = 1');
			$this->db->query('select * from `table_def` where id = ? and `revision` = ? for update',
				array($table->getId(), $table->getRevision()));
		} catch (\PDOException $e) {
			if (strpos($e->getMessage(), 'General error: 1205') !== false) {
				throw new TxCoreException('Lock failed', TxCoreException::LOCK_FAILED);
			}
			throw $e;
		}
	}

	/**
	 * @param Row $row
	 * @param User $by
	 * @param bool $notify
	 */
	public function updateRow(Row $row, User $by, $notify = false)
	{
		$table = $row->getParent();
		if ($row->getId() === null) {
			throw new TxCoreException(sprintf('Row dose not created'),
				TxCoreException::INTERNAL);
		}

		// 権限チェック
		$this->checkAutholity($table, 'update', $by);

		// Revalidation.
		$errors = $row->validate($by);
		if (is_not_empty($errors)) {
			throw new TxCoreException(sprintf('Validation failed.'),
				TxCoreException::VALIDATION_FAILED,
				$errors);
		}

		// 自動設定（計算式に作成日時などを含む場合があるので、計算の前に実施）
		foreach ($table->getColumns() as $column)	/* @var $column Column */
			$column->handleAutoset($row, new HandlerContext('update', $by));
		// 計算
		$row->calculate();

		// Row
		if ($this->queries->updateRowVersion($row) <= 0) {
			$row2 = $this->getRow($table, $row->getId());
			throw new TxCoreException('Row has updaed by other user.',
				TxCoreException::ROW_HAS_UPADTED, $row2);
		}
		$row->setVersion($row->getVersion() + 1);

		// notification
		if ($notify) {
			$row2 = $this->getRow($table, $row->getId());
			$this->notify("row_update", $by, $table, $row, $row2);
		}

		// data & indexes
		$this->queries->insertIndexes($table, $row, true);
		$this->queries->insertData($table, $row, $by, true);

		// child rows
		foreach ($table->getColumns() as $column) {	/* @var $column Column */
			if ($column->hasRelationship()) {
				$this->updateRelationships($by, $row, $column);
			}
		}
	}

	private function notify(
		$action, User $by, Table $table, Row $row,
		Row $oldrow = null, Comment $comment = null)
	{
		if ($table->isChild()) {
			return;
		}
		$this->applyNotificationBasicRules(
			$action, $by, $table, $row, $oldrow, $comment);
		if ($action == 'row_insert' || $action == 'row_update') {
			$this->applyNotificationConditionalRules(
				$action, $by, $table, $row, $oldrow);
		}
	}

	private function applyNotificationBasicRules(
		$action, User $by, Table $table, Row $row,
		Row $oldrow = null, Comment $comment = null)
	{
		$changes = null;
		if ($action == "row_update") {
			$changes = $this->getRowChange($table, $oldrow, $row);
			if (empty($changes)) return;
		}
		$recipients = $table->expandNotificationBasicRecipients(
			$action, $by, $row, $comment);
		if (count($recipients) == 0) return;
		$message = NotificationBasicRule::getMessage($action);
		$notification = new Notification();
		$notification->setMessage($message);
		$notification->setChanges($changes);
		$notification = $this->queries->insertNotification($notification, $by, $table, $row, $comment);
		$this->queries->insertNotificationReceipts(
			$notification, $recipients, $by, $table, $row, $comment);
	}

	private function applyNotificationConditionalRules(
		$action, User $by, Table $table, Row $row,
		Row $oldrow = null)
	{
		$changes = null;
		if ($action == "row_update") {
			$changes = $this->getRowChange($table, $oldrow, $row);
			if (empty($changes)) return;
		}
		$rules = $table->getNotificationConditionalRules();
		if (count($rules) == 0) return;
		foreach ($rules as $rule) {
			if (!$rule->matches($row, $oldrow)) continue;
			$recipients = $rule->getRecipients($by, $row);
			if (count($recipients) == 0) continue;
			$notification = new Notification();
			$notification->setMessage($rule->getMessage());
			$notification->setChanges($changes);
			$notification = $this->queries->insertNotification($notification, $by, $table, $row);
			$this->queries->insertNotificationReceipts(
				$notification, $recipients, $by, $table, $row);
		}
	}

	private function checkAutholity($target, $how, User $who)
	{
		if ($target instanceof Table) {
			if (!$target->allows($how, $who)) {
				throw new TxCoreException(
					sprintf('User %d has no authentication for %s table %d (owner:%s).',
						$who->getId(),
						$how,
						$target->getId(),
						($target->getOwner() ? $target->getOwner()->getId() : null)),
					TxCoreException::PERMISSION_DENIED);
			}
		}
	}

	public function checkAutholityForUser(User $user, $how, User $who = null)
	{
		switch ($how) {
		case 'update':
			// 管理者か本人
			if ($who->isAdmin() || $user->getId() == $who->getId()) {
				return true;
			}
			break;
		case 'create':
			// 管理者
			if ($who->isAdmin()) {
				return true;
			}
			break;
		case 'drop':
			// 管理者。ただし自分自身を削除できない。
			if ($who->isAdmin() && $user->getId() != $who->getId()) {
				return true;
			}
			break;
		}

		throw new TxCoreException(
			sprintf('User %d has no authentication for %s user.',
				($who ? $who->getId() : 'unauthenticated'), $how),
			TxCoreException::PERMISSION_DENIED);
	}

	/**
	 * @param Comment $comment
	 * @param $how
	 * @param User $who
	 * @return bool
	 * @throws TxCoreException
	 */
	public function checkAuthorityForComment(Comment $comment, $how, User $who)
	{
		$postedUser = $comment->getPostedUser() ;
		switch ($how) {
			case 'create':
				// Everyone.
				return true;
				break;
			case 'delete':
				// 管理者または、コメントの投稿者
				if ($who->isAdmin() || ($postedUser && $postedUser->getId() === $who->getId())) {
					return true;
				}
				break;
		}

		throw new TxCoreException(
			sprintf('User %d has no authentication for %s comment.',
				($who->getId()), $how),
			TxCoreException::PERMISSION_DENIED);
	}

	public function checkAuthorityForGroup(Group $user, $how, User $who = null)
	{
		assert(in_array($how, array('create', 'drop', 'update')));
		if ($who->isAdmin()) {
			return true;
		}

		throw new TxCoreException(
			sprintf('User %d has no authentication for %s user.',
				($who ? $who->getId() : 'unauthenticated'), $how),
			TxCoreException::PERMISSION_DENIED);
	}

	private function checkReferences($target)
	{
		if ($target instanceof Column) {
			$tables = $target->getReferrerTables();
			if (is_not_empty($tables)) {
				throw new TxCoreException(
						sprintf('Column %d is referenced by other tables.',
								$target->getId()),
						TxCoreException::REFERENCE_VIOLATION);
			}
		} else if ($target instanceof Table) {
			$tables = $target->getReferrerTables();
			if (is_not_empty($tables)) {
				throw new TxCoreException(
						sprintf('Table %d is referenced by other tables.',
								$target->getId()),
						TxCoreException::REFERENCE_VIOLATION);
			}
		}
	}

	/**
	 *
	 * @param Table $table
	 * @param Criteria $criteria
	 * @param callable $callable
	 * @param int $offset
	 * @param int $limit
	 * @throws \Exception
	 */
	public function featchTable($table, Criteria $criteria, $callable, $offset = null, $limit = null, $base = null)
	{
		$this_ = $this;
		list($query, $params) = $this->queries->buildSearchQuery($table, $criteria, false, $offset, $limit, $base);

		$columnMap = $this->getAllListableColumnsMap($table, $criteria);
		$optionCache = array();

		$row = null;	/** @var Row $row */
		$index = 0;
		$this->db->fetch($query, $params, function($data) use (&$row, &$columnMap, $table, $callable, &$index) {
			if ($row != null && $row->getId() != $data->row_id) {
				$ret = call_user_func_array($callable, array($row, $index));
				if ($ret === false) {
					return false;
				}
				$row = null;
				$index++;
			}
			if ($row === null) {
				$row = new Row();
				$row->_setId($data->row_id);
				$row->_setParent($table);
				$row->setVersion($data->version);
				$row->setRowNo($data->row_no);
				foreach ($columnMap as $column) {
					$row->_setRawValue($column, null);
				}
			}
			if(isset($columnMap[$data->column_id]))
				$row->_setRawValue($columnMap[$data->column_id], $data->value);
		});
		if ($row != null)
			call_user_func_array($callable, array($row, $index));
	}

    /**
     * Remove row in table.
     * @param Row $row
     * @param User $by
     * @throws TxCoreException
     */
	public function deleteRow(Row $row, User $by)
	{
		// 権限チェック
		$this->checkAutholity($row->getParent(), 'delete', $by);
		// child rows
		$table = $row->getParent();
		foreach ($table->getColumns() as $column) {	/* @var $column Column */
			if ($column->hasRelationship()) {
				$this->deleteRelationships($by, $row, $column);
			}
		}

		$affected = $this->queries->deleteRow($row);
		if ($affected  !== 1) {
			throw new TxCoreException(sprintf("Row dose not found : %d", $row->getId()),
				TxCoreException::ROW_NOT_FOUND);
		}

		$this->queries->deleteIndexesByRow($row);
		$this->queries->deleteDataByRow($row);

		$this->queries->deleteCommentByRow($row);
		$this->queries->deleteNotificationsByRow($row);
		$this->queries->deleteNotificationReceiptsByRow($row);
        $this->queries->deleteReadLogByRow($row->getId());

// 		$allFiles = array();
		foreach ($row->getParent()->getColumns() as $column)
			if ($column->hasValue())
				$column->handleRemove($row);

		$row->_setParent(null);
		$row->_setId(null);
	}

    /**
     * ユーザー情報を検索します
     * @param $keyword
     * @param int $offset
     * @param int $limit
     * @param string $orderType
     * @return List_|User[]
     * @throws \Exception
     */
	public function searchUsers($keyword = null, $offset = 0, $limit = 20, $orderType = 'name')
	{
        assert(in_array($orderType, array('name', 'seq')));

		$this_ = $this;
		$query = array();
		$params = array();
		$query[] = "select sql_calc_found_rows * from user ";
		if (is_not_empty($keyword)) {
			$query[] = "where login_id like :keyword or screen_name like :keyword or profile like :keyword";
			$params['keyword'] = '%'.Util::escapeLike($keyword).'%';
		}
		$query[] = sprintf("order by %s limit %d, %d",
            ($orderType === 'name' ? 'screen_name' : 'seq'),
            $offset,
            $limit);

		$list = new List_();
		$rows = array();
		$users = $this->db->fetch(join(' ', $query), $params, function($data) use (&$rows, $this_) {
			$rows[] = $this_->_mapUser($data);
		});

		$total = $this->db->query('SELECT FOUND_ROWS() as count', array());
		$list->_setType('user');
		$list->_setRows($rows);
		$list->_setTotalRowCount($total[0]->count);
		$list->_setOffset($offset ?: 0);
		$list->_setRowLimit($limit);
		$list->_setTable(null);
		return $list;
	}

	/**
	 * ユーザー情報を検索します (Overload)
	 * @param $criteria
	 * @param int $offset
	 * @param int $limit
	 * @return List_|User[]
	 * @throws \Exception
	 */
	public function searchUsers_(UserSearchCriteria $criteria, $offset = 0, $limit = 20)
	{
		$this_ = $this;
		$query = array();
		//$queryCondition = array();
		$params = array();
		$query[] = "select sql_calc_found_rows * from user ";
        $query[] = "where 1=1";
        if (is_not_empty($criteria->getKeyword()) ||
            is_not_empty($criteria->getInEntities())) {
            if (is_not_empty($criteria->getKeyword())) {
                $query[] = "and (login_id like ? or screen_name like ? or profile like ?)";
                $params[] = '%' . Util::escapeLike($criteria->getKeyword()) . '%';
                $params[] = '%' . Util::escapeLike($criteria->getKeyword()) . '%';
                $params[] = '%' . Util::escapeLike($criteria->getKeyword()) . '%';
            }
            if (is_not_empty($criteria->getInEntities())) {
                $query[] = "and";
                $query[] = "(";
                $query[] = "1 = 0";
                foreach ($criteria->getInEntities() as $entity) {
                    if ($entity instanceof User) {
                        $query[] = "or id = ?";
                        $params[] = $entity->getId();
                    } else if ($entity instanceof Group) {
                        $query[] = "or exists (select 1 from user_group where group_id = ? and user.id = user_group.user_id)";
                        $params[] = $entity->getId();
                    } else {
                        assert(false);
                    }
                }
                $query[] = ")";

            }
        }
		$query[] = "order by seq is null, seq, screen_name, id desc";
		$query[] = sprintf("limit %d, %d", $offset, $limit);

		$list = new List_();
		$rows = array();
		$this->db->fetch(join(' ', $query), $params, function($data) use (&$rows, $this_) {
			$rows[] = $this_->_mapUser($data);
		});

		$total = $this->db->query('SELECT FOUND_ROWS() as count', array());
		$list->_setType('user');
		$list->_setRows($rows);
		$list->_setTotalRowCount($total[0]->count);
		$list->_setOffset($offset ?: 0);
		$list->_setRowLimit($limit);
		$list->_setTable(null);
		return $list;
	}

	/**
	 * グループ情報を検索します
	 * @param $keyword
	 * @param int $includingUserId
	 * @return List_
	 * @throws \Exception
	 */
	public function searchGroups($keyword = null, $includingUserId = null)
	{
		$this_ = $this;
		$query = array();
		$params = array();
		$query[] = "select sql_calc_found_rows * from `group` ";
		$query[] = "where 1=1";
		if (is_not_empty($keyword)) {
			$query[] = "and name like :keyword";
			$params['keyword'] = '%'.Util::escapeLike($keyword).'%';
		}
		if (is_not_empty($includingUserId)) {
			$query[] = "and `group`.id in (select user_group.group_id from user_group where user_id = :userId)";
			$params['userId'] = $includingUserId;
		}
		$query[] = "order by name";

		$list = new List_();
		$rows = array();
		$this->db->fetch(join(' ', $query), $params, function($data) use (&$rows, $this_) {
			$rows[] = $this_->_mapGroup($data);
		});

		$total = $this->db->query('SELECT FOUND_ROWS() as count', array());
		$list->_setType('group');
		$list->_setRows($rows);
		$list->_setTotalRowCount($total[0]->count);
		$list->_setOffset(0);
		$list->_setRowLimit(null);
		$list->_setTable(null);
		return $list;
	}

	/**
	 * グループ情報を検索します
	 * @param Group $group
	 * @return User[]
	 */
	public function getGroupUsers(Group $group)
	{
		$result = array();
		$options = $this->queries->selectGroupUsers($group->getId());
		foreach ($options as $option) {
			$result[] = $this->_mapUser($option);
		}
		return $result;
	}

	/**
	 * (overload)
	 * @param Group $group
	 * @param array $userIds
	 */
	public function replaceGroupUsers_(Group $group, $userIds)
	{
		// Delete and insert.
		$this->queries->deleteUserGroupByGroupId($group->getId());
		foreach ($userIds as $userId) {
			$this->queries->insertUserGroup($userId, $group->getId());
		}
	}


	/**
	 * Returns count of registerd users.
	 */
	public function countUsers()
	{
		return $this->queries->selectUsersCount();
	}

// 	/**
// 	 * @return array of Table.
// 	 */
// 	public function getTemplateTables()
// 	{
// 		$Tables = $this->db->query("
// 			select * from table_def
// 			where def_type = 'template'
// 		", array());

// 		$tables = array();
// 		foreach ($Tables as $_Table) {
// 			$tables[] = $this->_mapTable($_Table);
// 		}
// 		return $tables;
// 	}

	/**
	 * @param User $user
	 * @return Table[]
	 */
	public function getUserTables(User $user)
	{
		$this_ = $this;
		$tables = array();
		$this->queries->selectUserTableDefs($user, function($data) use ($this_, &$tables) {
			$tables[] = $this_->_mapTable($data);
		});
		return $tables;
	}

	/**
	 * Returns count of registerd tables.
	 */
	public function countTables()
	{
		return $this->queries->selectTablesCount();
	}

	/**
	 * @param array $filter
	 * @return Table[]
	 */
	public function getTablesInternal($filter = array())
	{
		$this_ = $this;
		$tables = array();
		$this->queries->selectTableDefs($filter, function($data) use ($this_, &$tables) {
			$tables[] = $this_->_mapTable($data);
		});
		return $tables;
	}

	/**
	 * @param Table|int $table Table or table id.
	 * @return array of Column.
	 */
	public function getTableColumns(Table $table)
	{
		$this_ = $this;
		$result = array();
		$this->queries->selectColumnDefByTable($table, function($data) use (&$result, $table, $this_) {
			$result[] = $this_->_mapColumn($data, $table);
		});
		return $result;
	}

	/**
	 * @param int $id Table id
	 * @param string $revision
	 * @param User $by
	 * @param bool $silently
	 * @return Table
	 * @throws TxCoreException
	 */
	public function getTable($id, $revision = self::RELEASE, User $by = null, $silently = false)
	{
		$tables = array();
		if ($revision === self::RELEASE) {
			$tables = $this->queries->selectReleasedTableDef($id);
		} else if ($revision === self::TRUNK) {
			$tables = $this->queries->selectTrunkTableDef($id);
		} else {
			$tables = $this->queries->selectTableDef($id, $revision);
		}

		if (is_empty($tables)) {
			if ($silently) {
				return null;
			}
			throw new TxCoreException(sprintf("Table dose not found : %d", $id),
				TxCoreException::ROW_NOT_FOUND);
		}

		$table = $this->_mapTable($tables[0]);
		if ($by !== null) {
			if ($revision === self::TRUNK) {
				$this->checkAutholity($table, 'alter', $by);
			}
		}

		$calendarSettings = $this->queries->selectViewCalendarSetting($tables[0]->id,
			$tables[0]->revision);
		if (is_not_empty($calendarSettings)) {
			$table->setCalendarSettings($this->_mapCalendarSetting($calendarSettings[0], $table));
		}

        $readCheckSettings = $this->queries->selectReadCheckSettings($tables[0]->id,
            $tables[0]->revision);
        if (is_not_empty($readCheckSettings)) {
            $table->setReadCheckSettings($this->_mapReadCheckSetting($readCheckSettings[0]));
        }


		return $table;
	}

	/**
	 * @param Template $template
	 * @param User $by
	 * @return Table
	 */
	public function createTableFromTemplate(Template $template, User $by)
	{
		$baseTable = $this->getTable($template->getId(), $template->getRevision());
		$table = $this->copyTable($baseTable);
		$table->setName($template->getName());
		$table->setIcon($template->getIcon());
		$table->setDescription($template->getDescription());
		$table->setPublic($template->isPublic());	// ユーザー選択値
		$table->setRoundType($template->getRoundType());
		$table->setRoundScale($template->getRoundScale());
		$table->setPermissionControl($template->getPermissionControl());
		$table->create($by);
		return $table;
	}

	/**
	 * Returns all preset icons.
	 * @param $id
	 * @return Icon
	 * @throws TxCoreException
	 */
	public function getIcon($id)
	{
		$raws = $this->queries->selectIcon($id);
		if (is_empty($raws)) {
			throw new TxCoreException(sprintf("Icon dose not found : %d", $id),
				TxCoreException::ROW_NOT_FOUND);
		}
		return new Icon($raws[0]->filekey, true);
	}

	/**
	 * Returns all preset icons.
	 * @return \TxCore\Icon[]
	 */
	public function getIcons()
	{
		$raws = $this->queries->selectIcons();
		$icons = array();
		foreach ($raws as $raw) {
			$icons[] = new Icon($raw->filekey, true);
		}
		return $icons;
	}

	/**
	 * @return int
	 * @throws TxCoreException
	 */
	public function generateObjectId()
	{
		$id = null;
		try {
			$id = $this->queries->selectNewObjectId();
		} catch (\PDOException $e) {
			if (strpos($e->getMessage(), 'General error: 1205') !== false) {
				throw new TxCoreException('Lock failed', TxCoreException::LOCK_FAILED);
			}
			throw $e;
		}
		return Util::toInt($id);
	}

	/**
	 * @param Table|int $table
	 * @return int
	 * @throws TxCoreException
	 */
	public function generateRownum($table)
	{
		$rownum = null;
		try {
			$tableId = ($table instanceof Table) ? $table->getId() :  $table;
			$rownum = $this->queries->selectNextRownum($tableId);
		} catch (\PDOException $e) {
			if (strpos($e->getMessage(), 'General error: 1205') !== false) {
				throw new TxCoreException('Lock failed', TxCoreException::LOCK_FAILED);
			}
			throw $e;
		}
		return Util::toInt($rownum);
	}

	/**
	 * @param Table|int $table
	 * @return int|null
	 * @throws TxCoreException
	 */
	public function generateRevision($table)
	{
		$revno = null;
		try {
			$tableId = ($table instanceof Table) ? $table->getId() :  $table;
			$revno = $this->queries->selectNextRevisionNo($tableId);
		} catch (\PDOException $e) {
			if (strpos($e->getMessage(), 'General error: 1205') !== false) {
				throw new TxCoreException('Lock failed', TxCoreException::LOCK_FAILED);
			}
			throw $e;
		}
		return Util::toInt($revno);
	}

	/**
	 * @param string $path
	 * @return \TxCore\Icon
	 */
	public function uploadIcon($path)
	{
		$size = get_config('app', 'icon_size');

		$resizer = new Resizer(Resizer::IMAGICK);
		$resizer->setStyle(Resizer::COVER);
		$resizer->setAllowScaleUp(true);
		$resizer->resize($path, $path, $size, $size);

		$fileName = Util::getUniqeFileName(pathinfo($path, PATHINFO_EXTENSION));
		$dest = concat_path(get_config('app', 'user_icon_dir'), $fileName);
		copy($path, $dest);
		return new Icon($fileName, false);
	}


	public function validateTableRelease(Table $trunk)
	{
		// リリースを取得
		$release = $this->getTable($trunk->getId(), self::RELEASE);

		// 参照チェック
		$column = null;
		try {
			foreach ($this->diffColumns($release, $trunk) as $column) {
					$this->checkReferences($column);
			}
		} catch (TxCoreException $e) {
			if ($e->getCode() == TxCoreException::REFERENCE_VIOLATION) {
				$referrers = $column->getReferrerTables();
				$message = "削除しようとしている列({$column->getLabel()})が他のDBから参照されています。\n";
				$message .= "参照元:\n";
				foreach ($referrers as $table) {
					$message .= "・{$table->getName()}\n";
				}
				return array(new Message($message));
			}
		}

		if ($trunk->isCalendarEnabled()) {
			if ($trunk->getCalendarSettings() === null) {
				return array(new Message('カレンダー表示を使用する場合は、カレンダー表示の設定を行ってください。'));
			}
		}

		return array();
	}

	/**
	 * @param Table $trunk
	 * @param User $by
	 * @throws TxCoreException
	 */
	public function releaseTable(Table $trunk, User $by)
	{
		$this_ = $this;
		if ($this->getRevisionName($trunk) !== self::TRUNK)
			throw new TxCoreException(sprintf('release table dose not support operation for release revison : '),
				TxCoreException::INTERNAL);

		// 権限チェック
		$this->checkAutholity($trunk, 'alter', $by);
		// リリースを取得
		$release = $this->getTable($trunk->getId(), self::RELEASE);
		// ロック
		$release->lock();

		// 参照チェック
		foreach ($this->diffColumns($release, $trunk) as $column) {
			$this->checkReferences($column);
		}

		// リリースにあって、ワークにないカラムを取得して、data, indexes、とファイルのディレクトリ を消す
		foreach ($this->diffColumns($trunk, $release) as $column) {
			if ($column->hasValue())
				$this_->queries->insertEmptyColumnIndexes($column);
		}
		foreach ($this->diffColumns($release, $trunk) as $column) {
			$this_->queries->deleteIndexesByColumn($column);
			$this_->queries->deleteDataByColumn($column);
			$this_->storage->removeDir(concat_path($column->getParent()->getId(), $column->getId()));
			$column->handleDrop($by);
		}

		// 古い参照を削除
		$this_->queries->deleteReferencesByTableOtherRevision($trunk);

		// 古い権限を削除
		$this_->queries->deletePermissionsByTableOtherRevision($trunk);

		// ワークをコピーしてNewプロダクションへ
		$trunk2 = $this->branchTable($trunk);


		// リリースを更新
		$this->queries->updateReleaseControl($trunk2, $trunk, false);
		$trunk->_setRevision($trunk->getRevision());
	}

	public function checkUniqueColumnValueDuplication(Column $column)
	{
		return ($this->queries->selectCountDuplicatedNonNullIndexesByColumn($column) == 0);
	}

	public function checkUniqueColumnValueLength(Column $column)
	{
		return ($this->queries->selectMaxLengthOfIndexesByColumn($column) <= 64);
	}

	/**
	 * @param Table $table1
	 * @param Table $table2
	 * @return Column[]
	 */
	private function diffColumns(Table $table1, Table $table2)
	{
		$columns1 = $table1->getColumns();
		$columns2 = $table2->getColumns();
		$diffs = array();
		foreach ($columns1 as $column1) {
			$found = false;
			foreach ($columns2 as $column2) {
				if ($column1->getId() === $column2->getId()) {
					$found = true;
					break;
				}
			}
			if (!$found) {
				$diffs[] = $column1;
			}
		}
		return $diffs;
	}

	/**
	 * @param Table $trunk
	 * @param User $by
	 * @throws TxCoreException
	 */
	public function saveTableChanges(Table $trunk, User $by)
	{
		if ($this->getRevisionName($trunk) !== self::TRUNK)
			throw new TxCoreException(sprintf('save changes dose not support operation for release revison.'),
				TxCoreException::INTERNAL);

		// 権限チェック
		$this->checkAutholity($trunk, 'alter', $by);

		// リビジョンチェック
		$this->checkTrunkRevision($trunk);

		// 参照チェック
		foreach ($trunk->_getRemovedColumns() as $column) { /*@var $column Column*/
			$this->checkReferences($column);
		}
// 		// ロック
// 		$trunk->lock();

		// アイコンを複製
		// TODO: workが使用しなくなったアイコンを消すことができない。
		// テーブルの初期作成時、productionからworkが、同じアイコンを参照しているため
		// workに対してアイコンを変更した回数分、ファイルが残る。

		$release = $this->getTable($trunk->getId(), self::RELEASE);
		// プリセットからアイコンを複製
		if ($trunk->getIcon()->isPreset()) {
			$trunk->setIcon($this->copyIcon($trunk->getIcon()));
		}

		// Table_def更新
		$this->queries->updateTableDef($trunk);
		$this->queries->deleteReferencesByTable($trunk);
		foreach ($trunk->getColumns() as $column) { /*@var $column Column*/
			$this->queries->insertColumnDef($column, true);
			if ($column->hasOptions()) {
				$this->queries->insertOptions($column, true);
				foreach ($column->_getRemovedOptions() as $option)
					$this->queries->deleteOption($option);
			}
			if ($column->hasReference()) {
				$this->queries->insertReferences($column);
			}
		}
		foreach ($trunk->_getRemovedColumns() as $column) { /*@var $column Column*/
			$this->queries->deleteColumnDef($column);
			$this->queries->deleteOptionsByColumn($column);
		}
		$trunk->getViewColumns();	// Upate cache.
		$this->queries->deleteViewColumnsByTable($trunk);
		$this->queries->insertViewColumns($trunk);

		// Calendar更新
		$this->queries->deleteViewCalendarSettingByTable($trunk);
		if ($trunk->isCalendarEnabled()) {
			$this->queries->insertViewCalendarSetting($trunk);
		}

        // 回覧板更新
        $this->queries->deleteReadCheckSettingsByTable($trunk->getId(), $trunk->getRevision());
        if ($trunk->isReadCheckEnabled() && $trunk->getReadCheckSettings() !== null) {
            $this->queries->insertReadCheckSetting($trunk->getId(),
                $trunk->getRevision(), $trunk->getReadCheckSettings());
        }

        // Permission更新
		$trunk->getPermissions();
		$this->queries->deletePermissionsByTable($trunk);
		$this->queries->insertPermissions($trunk);

		// NotificationViewColumn更新
		$trunk->getNotificationViewColumns(); // Update
		$this->queries->deleteNotificationViewColumnsByTable($trunk);
		$this->queries->insertNotificationViewColumns($trunk);

		// NotificationBasicRule更新
		$trunk->getNotificationBasicRules(); // Update
		$this->queries->deleteNotificationBasicRulesByTable($trunk);
		$this->queries->insertNotificationBasicRules($trunk);

		// NotificationConditionalRule更新
		$trunk->getNotificationConditionalRules(); // Update
		$this->queries->deleteNotificationConditionalRulesByTable($trunk);
		$this->queries->insertNotificationConditionalRules($trunk);

		// Revesion
		$newRevision = $this->generateRevision($trunk);
		$this->queries->updateTableRevision($trunk, $newRevision);
		$this->queries->updateColumnRevision($trunk, $newRevision);
		$this->queries->updateOptionRevision($trunk, $newRevision);
		$this->queries->updateViewColumnRevision($trunk, $newRevision);
		$this->queries->updateViewCalendarSettingRevision($trunk, $newRevision);
		$this->queries->updatePermissionRevision($trunk, $newRevision);
		$this->queries->updateReferenceRevision($trunk, $newRevision);
		$this->queries->updateNotificationViewColumnRevision(
			$trunk, $newRevision);
		$this->queries->updateNotificationBasicRuleRevision(
			$trunk, $newRevision);
		$this->queries->updateNotificationConditionalRuleRevision(
			$trunk, $newRevision);
        $this->queries->updateReadCheckSettingsRevision($trunk, $newRevision);
		$trunk->_setRevision($newRevision);

		// Releaee_contorl 更新
		$this->queries->updateReleaseControl($trunk, $release, true);
	}

	private function checkTrunkRevision(Table $trunk)
	{
		$ctrls = $this->queries->selectReleaseControl($trunk);
		if ($trunk->getRevision() != $ctrls[0]->trunk_revision) {
			throw new TxCoreException('table has updated by other user.',
					TxCoreException::TABLE_HAS_UPADTED, $trunk);
		}
	}

	/**
	 * @param Table $trunk
	 * @param User $by
	 * @throws TxCoreException
	 */
	public function discardTableChanges(Table $trunk, User $by)
	{
		$this_ = $this;
		if ($this->getRevisionName($trunk) !== self::TRUNK)
			throw new TxCoreException(sprintf('discard table dose not support operation for release revison : '),
					TxCoreException::INTERNAL);

		// 権限チェック
		$this->checkAutholity($trunk, 'alter', $by);

		// プロダクションをコピーしてワークへ
		$release = $this->getTable($trunk->getId(), self::RELEASE);
		$trunk->setApiEnabled($release->isApiEnabled());
		$trunk->setApiKey($release->getApiKey());
		$trunk->setWriteApiEnabled($release->isWriteApiEnabled());
		$trunk->setWriteApiKey($release->getWriteApiKey());
		$trunk->setDescription($release->getDescription());
		$trunk->setIcon($release->getIcon());
		$trunk->setName($release->getName());
		$trunk->setPublic($release->isPublic());
		$trunk->setRoundScale($release->getRoundScale());
		$trunk->setRoundType($release->getRoundType());
		$trunk->setPermissionControl($release->getPermissionControl());
		$trunk->setCalendarEnabled($release->isCalendarEnabled());
		$trunk->setCommentEnabled($release->isCommentEnabled());
		$trunk->setCommentFollowingEnabled($release->isCommentFollowingEnabled());
		$trunk->setReadCheckEnabled($release->isReadCheckEnabled());
		$trunk->setSixColumnsEnabled($release->isSixColumnsEnabled());

		$this->queries->updateTableDef($trunk);
		$this->db->update("delete from `option` where table_id = ? and revision = ? ", array($trunk->getId(), $trunk->getRevision()));
		$this->db->update("delete from `view_column` where table_id = ? and revision = ? ", array($trunk->getId(), $trunk->getRevision()));
		$this->db->update("delete from `view_calendar_setting` where table_id = ? and revision = ? ", array($trunk->getId(), $trunk->getRevision()));
		$this->db->update("delete from `column_def` where table_id = ? and revision = ? ", array($trunk->getId(), $trunk->getRevision()));
		$this->db->update("delete from `reference` where table_id = ? and revision = ? ", array($trunk->getId(), $trunk->getRevision()));
		$this->db->update("delete from `permission` where table_id = ? and revision = ? ", array($trunk->getId(), $trunk->getRevision()));
		$this->db->update("delete from `notification_view_column` where table_id = ? and revision = ? ", array($trunk->getId(), $trunk->getRevision()));
		$this->db->update("delete from `notification_basic_rule` where table_id = ? and revision = ? ", array($trunk->getId(), $trunk->getRevision()));
		$this->db->update("delete from `notification_conditional_rule` where table_id = ? and revision = ? ", array($trunk->getId(), $trunk->getRevision()));
		$this->db->update("delete from `read_check_setting` where table_id = ? and revision = ? ", array($trunk->getId(), $trunk->getRevision()));

		$this->copyColumnDefs($release, $trunk->getRevision());
		$this->copyOptions($release, $trunk->getRevision());
		$this->copyViewColumns($release, $trunk->getRevision());
		$this->copyViewCalendarSetting($release, $trunk->getRevision());
		$this->copyReadCheckSetting($release, $trunk->getRevision());
		$this->copyReferences($release, $trunk->getRevision());
		$this->copyPermissions($release, $trunk->getRevision());
		$this->copyNotificationViewColumns($release, $trunk->getRevision());
		$this->copyNotificationBasicRules($release, $trunk->getRevision());
		$this->copyNotificationConditionalRules($release, $trunk->getRevision());

		// Revision
		$newRevision = $this->generateRevision($trunk);
		$this->queries->updateTableRevision($trunk, $newRevision);
		$this->queries->updateColumnRevision($trunk, $newRevision);
		$this->queries->updateOptionRevision($trunk, $newRevision);
		$this->queries->updateViewColumnRevision($trunk, $newRevision);
		$this->queries->updateViewCalendarSettingRevision($trunk, $newRevision);
		$this->queries->updatePermissionRevision($trunk, $newRevision);
		$this->queries->updateReferenceRevision($trunk, $newRevision);
		$this->queries->updateNotificationViewColumnRevision(
			$trunk, $newRevision);
		$this->queries->updateNotificationBasicRuleRevision(
			$trunk, $newRevision);
		$this->queries->updateNotificationConditionalRuleRevision(
			$trunk, $newRevision);
		$this->queries->updateReadCheckSettingsRevision($trunk, $newRevision);
		$trunk->_setRevision($newRevision);

		// pendingを解除
		$this->queries->updateReleaseControl($trunk, $release, false);

	}

	/**
	 * @param Table $table
	 * @param User $by
	 * @return Table created table id
	 * @throws TxCoreException
	 */
	public function createTable(Table $table, User $by)
	{
		if ($table->getIcon() === null ||
			is_empty($table->getName())) {
			throw new TxCoreException(sprintf('Icon and name is required.'),
					TxCoreException::INTERNAL);
		}

		$codeMap = array();
		foreach ($table->getColumns() as $column) {
			if ($column->getId() === null) {
				throw new TxCoreException(sprintf('Column has no id.'),
					TxCoreException::INTERNAL);
			}
		}

		// アイコンを複製
		$table->setIcon($this->copyIcon($table->getIcon()));

		// 共通カラムを挿入
		$this->appendAutoColumns($table);
		// APIキーを設定
		$table->resetApiKey();
		$table->resetWriteApiKey();

		// releaseテーブルを作成
		$this->queries->insertRownum($table); // initialization
		$this->queries->insertRevisionNo($table); // initialization
		$table->_setRevision($this->generateRevision($table));
		$this->queries->insertTableDef($table, $by);
		foreach ($table->getColumns() as $column) { /*@var $column Column*/
			$this->queries->insertColumnDef($column);
			if ($column->hasOptions())
				$this->queries->insertOptions($column);
			if ($column->hasReference())
				$this->queries->insertReferences($column);
		}
		$this->queries->insertViewColumns($table);
		if ($table->getCalendarSettings()) {
			$this->queries->insertViewCalendarSetting($table);
		}
        if ($table->getReadCheckSettings() !== null) {
            $this->queries->insertReadCheckSetting($table->getId(),
                $table->getRevision(), $table->getReadCheckSettings());
        }
		$this->queries->insertPermissions($table);
		$this->queries->insertNotificationViewColumns($table);
		$this->queries->insertNotificationBasicRules($table);
		$this->queries->insertNotificationConditionalRules($table);

		// trunk 用にブランチを作成、リリースコントロールに追加
		$trunkTable = $this->branchTable($table);
		$this->queries->insertReleaseControl($trunkTable, $table);

// 		$table->_setWorkTableId($tableWork->getId());
		$table->_setOwnerId($by->getId());
		$table->_setViewColumns(null);	// clear cache.
		$table->_setColumns(null);	// clear cache.
		$table->_setPermissions(null);	// clear cache.
		$table->setNotificationViewColumns(null);	// clear cache.
		$table->setNotificationBasicRules(null);	// clear cache.
		$table->setNotificationConditionalRules(null);	// clear cache.

	}

	/**
	 * Create branch.
	 * @param Table $table
	 * @return Table The branched table
	 */
	private function branchTable(Table $table)
	{
		$table_ = clone $table;
		$rev = $this->generateRevision($table);
		$this->queries->insertTableDefBranch($table, $rev);
		$this->copyColumnDefs($table, $rev);
		$this->copyOptions($table, $rev);
		$this->copyViewColumns($table, $rev);
		$this->copyViewCalendarSetting($table, $rev);
		$this->copyReferences($table, $rev);
		$this->copyPermissions($table, $rev);
		$this->copyNotificationBasicRules($table, $rev);
		$this->copyNotificationConditionalRules($table, $rev);
		$this->copyNotificationViewColumns($table, $rev);
        $this->copyReadCheckSetting($table, $rev);
		$table_->_setRevision($rev);
		return $table_;
	}

	private function copyColumnDefs(Table $table, $newRevision)
	{
		$this->db->update('
			insert into `column_def` (id,revision, table_id,column_type,code,label, properties)
			select id,?, table_id,column_type,code,label, properties from column_def where table_id = ? and revision = ?',
			array($newRevision, $table->getId(), $table->getRevision()));
	}

	private function copyOptions(Table $table, $newRevision)
	{
		$this->db->update('
			insert into `option` (id,revision, table_id,column_id,name,seq)
				select id,?, table_id,column_id,name,seq from `option` where table_id = ? and revision = ?',
			array($newRevision, $table->getId(), $table->getRevision()));
	}

	private function copyViewColumns(Table $table, $newRevision)
	{
		$this->db->update('
			insert into `view_column` (table_id,revision, column_id,width, seq)
			select table_id,?,column_id,width, seq from `view_column` where table_id = ? and revision = ?',
			array($newRevision, $table->getId(), $table->getRevision()));
	}

	private function copyPermissions(Table $table, $newRevision)
	{
		$this->db->update('
		insert into `permission` (table_id, revision, user_id, is_readable, is_writable)
		select table_id, ?, user_id, is_readable, is_writable from `permission` where table_id = ? and revision = ?',
			array($newRevision, $table->getId(), $table->getRevision()));
	}
	
	private function copyReferences(Table $table, $newRevision)
	{
		$this->db->update('
			insert into `reference` (column_id,revision,table_id,no,target_column_id,target_table_id)
			select column_id,?,table_id,no,target_column_id,target_table_id from `reference` where table_id = ? and revision = ?',
			array($newRevision, $table->getId(), $table->getRevision()));
	}


	private function copyViewCalendarSetting(Table $table, $newRevision)
	{
		$this->db->update('
		insert into `view_calendar_setting`
				(  `table_id` , `revision`, `date_column_id`, `user_column_id`,
				`start_time_column_id`, `end_time_column_id`, `title_column_id`)
		select table_id, ?, `date_column_id`, `user_column_id`,
				`start_time_column_id`, `end_time_column_id`, `title_column_id` from `view_calendar_setting`
				where table_id = ? and revision = ?',
			array($newRevision, $table->getId(), $table->getRevision()));
	}

    private function copyReadCheckSetting(Table $table, $newRevision)
    {
        $this->db->update('
		insert into `read_check_setting`
				(  `table_id` , `revision`, `is_autoread_enabled`, `is_onlist_enabled`,
				`check_targets`)
		select table_id, ?, `is_autoread_enabled`, `is_onlist_enabled`,
				`check_targets` from `read_check_setting`
				where table_id = ? and revision = ?',
            array($newRevision, $table->getId(), $table->getRevision()));
    }

	private function copyNotificationViewColumns(Table $table, $newRevision)
	{
		$this->db->update('
			insert into `notification_view_column` (table_id,revision, column_id,seq)
			select `table_id`,?,column_id,seq from `notification_view_column` where table_id = ? and revision = ?',
			array($newRevision, $table->getId(), $table->getRevision()));
	}

	private function copyNotificationBasicRules(Table $table, $newRevision)
	{
		$this->db->update('
		insert into `notification_basic_rule`
			(`table_id`, `revision`, `no`, `targeting_type`, `targets`,
			`is_on_row_insert`, `is_on_row_update`, `is_on_comment_insert`)
		select
			`table_id`, ?, `no`, `targeting_type`, `targets`,
			`is_on_row_insert`, `is_on_row_update`, `is_on_comment_insert`
			from `notification_basic_rule`
			where table_id = ? and revision = ?',
			array($newRevision, $table->getId(), $table->getRevision()));
	}

	private function copyNotificationConditionalRules(Table $table, $newRevision)
	{
		$this->db->update('
		insert into `notification_conditional_rule`
			(`table_id`, `revision`, `no`, `targeting_type`, `targets`,
			`expression`, `message`)
		select `table_id`, ?, `no`, `targeting_type`, `targets`,
			`expression`, `message`
			from `notification_conditional_rule`
				where table_id = ? and revision = ?',
			array($newRevision, $table->getId(), $table->getRevision()));
	}

	/**
	 * Deep copy and generate new object ids.
	 * @param Table $table
	 * @param array $objectIdMap
	 */
	private function &copyTable($table)
	{
		$objectIdMap = array();
		$table2 = clone $table;	/* @var $table2 Table */

		// 明細フィールドをコピーしない
		$filter = array('type'=>Column::RELATIONAL_INPUT);
		foreach ($table->getColumns($filter) as $column) {
			$table->removeColumn($column);
		}

		$columns = $table->getColumns();
		$viewColumns = $table->getViewColumns();    /** @var $viewColumns ViewColumn[] */
		$calendarSettings = $table->getCalendarSettings();
        $readCheckSettings = $table->getReadCheckSettings();
		$permissions = $table->getPermissions();
		$notificationViewColumns = $table->getNotificationViewColumns();
		$notificationBasicRules = $table->getNotificationBasicRules();
		$notificationConditionalRules 
			= $table->getNotificationConditionalRules();

		// IDをすべて発番
		$objectIdMap[$table->getId()] = $this->generateObjectId();
		foreach ($columns as $column) { /*@var $column Column*/
			$objectIdMap[$column->getId()] = $this->generateObjectId();
			if ($column->hasOptions()) {
				foreach ($column->getOptions() as $option) {
					$objectIdMap[$option->getId()] = $this->generateObjectId();
				}
			}
		}

		$columns2 = array();    /** @var $columns2 Column[] */
        foreach ($columns as $column) {
			$column2 = clone $column;
			// Replace initial value having column id.
			if ($column->hasOptions() && $column->getDefaultValue()) {
				$value = $column->getDefaultValue();
				if (is_array($value)) {	// checkbox
					$column2->setDefaultValue(array_map(function($option) use($objectIdMap) {   /** @var $option Option */
						$option2 = clone $option;
						$option2->_setId($objectIdMap[$option->getId()]);
						return $option2;
					}, $value));
				} else {
					if (!($value instanceof Option))
						throw new TxCoreException(sprintf('Invalid option type : %s (%s).', gettype($value), $column->getType()),
							TxCoreException::INTERNAL);
					$option2 = clone $value;
					$option2->_setId($objectIdMap[$value->getId()]);
					$option2->_setParent($column2);
					$column2->setDefaultValue($option2);
				}
			}
			// Option.
			if ($column->hasOptions()) {
				$options2 = array();
				foreach ($column->getOptions() as $option) {
					$option2 = clone $option;
					$option2->_setId($objectIdMap[$option->getId()]);
					$option2->_setParent($column2);
					$options2[] = $option2;
				}
				$column2->_setOptions($options2);
			}
			$column2->_setId($objectIdMap[$column->getId()]);
			$column2->_setParent($table2);
			$columns2[$column->getId()] = $column2;
		}

		foreach ($columns as $column) {
			$column2 = $columns2[$column->getId()];
			// Replace layout parent.
			if ($column->hasProperty('layoutParentId')) {
				$layoutParent = $column->getLayoutParent();
				if ($layoutParent) {
					$column2->setLayoutParent($columns2[$layoutParent->getId()]);
				}
			}
			if ($column->hasProperty('dateSourceColumnId')) {
				$dateSourceColumn = $column->getDateSourceColumn();
				if ($dateSourceColumn) {
					$column2->setDateSourceColumn($columns2[$dateSourceColumn->getId()]);
				}
				
			}
		}

		$viewColumns2 = array();
		foreach ($viewColumns as $viewColumn) {
            $column = $viewColumn->getColumn();
            if ($column !== null) {
                $viewColumn2 = new ViewColumn($table2);
                $viewColumn2->setWidth($viewColumn->getWidth());
                $viewColumn2->setColumn($columns2[$column->getId()]);
			    $viewColumns2[] = $viewColumn2;
            }
		}

		// カレンダー設定
		if ($calendarSettings) {
			$calendarSettings->setDateColumnId($objectIdMap[$calendarSettings->getDateColumnId()]);
			$calendarSettings->setUserColumnId($objectIdMap[$calendarSettings->getUserColumnId()]);
			$calendarSettings->setStartTimeColumnId($calendarSettings->getStartTimeColumnId() ? $objectIdMap[$calendarSettings->getStartTimeColumnId()] : null);
			$calendarSettings->setEndTimeColumnId($calendarSettings->getEndTimeColumnId() ? $objectIdMap[$calendarSettings->getEndTimeColumnId()] : null);
			$calendarSettings->setTitleColumnId($objectIdMap[$calendarSettings->getTitleColumnId()]);
		}

		$permissions2 = array();
		foreach($permissions as $permission){
			$permission2 = clone $permission;
			
			// 既に存在しないユーザは除外する
			if(is_null($this->getUser($permission2->getUserId(), false, true)))
				continue;
			
			$permission2->setTableId($objectIdMap[$table->getId()]);
			$permissions2[] = $permission2;
		}

		foreach ($columns as $column) {
			if (!$column->hasReference())
				continue;

			// Replace reference .
			if ($column->getReferenceTable() !== null) {

				if ($column->hasProperty('referenceSourceColumnId') &&
					($sourceColumn = $column->getReferenceSourceColumn()) !== null) {
					$columns2[$column->getId()]->setReferenceSourceColumn($columns2[$sourceColumn->getId()]);
				}
				if (@$column->getReferenceCopyMap() !== null) {
					$replaced = array();
					foreach ($column->getReferenceCopyMap() as $copyEntry) {
						$replaced[] = array(
							$copyEntry[0],
							$columns2[$copyEntry[1]->getId()]
							);
					}
					$columns2[$column->getId()]->setReferenceCopyMap($replaced);
				}
			}
		}

		// 通知設定
		//   通知表示カラム
		$notificationViewColumns2 = array();
		foreach ($notificationViewColumns as $column) {
			$notificationViewColumns2[] = $columns2[$column->getId()];
		}
		//   基本通知
		$notificationBasicRules2 = array();
		foreach ($notificationBasicRules as $rule) {
			$rule2 = clone $rule;
			if ($rule2->getTargetingType()
				== NotificationRule::TARGETING_COLUMN) {
				$targets2 = array();
				foreach ($rule->getTargets() as $column) {
					$targets2[] = $columns2[$column->getId()];
				}
				$rule2->_setTargets($targets2);
			}
			$notificationBasicRules2[] = $rule2;
		}
		//   条件指定通知
		$notificationConditionalRules2 = array();
		foreach ($notificationConditionalRules as $rule) {
			$rule2 = clone $rule;
			if ($rule2->getTargetingType()
				== NotificationRule::TARGETING_COLUMN) {
				$targets2 = array();
				foreach ($rule->getTargets() as $column) {
					$targets2[] = $columns2[$column->getId()];
				}
				$rule2->_setTargets($targets2);
			}
			// copy criteria
			$criteria = $rule->getCriteria();
			$criteria2 = clone $criteria;
			$conditions2 = array();
			foreach ($criteria->getConditions() as $condition) {
				// copy condition
				$condition2 = clone $condition;
				$column = $condition->getColumn();
				$column2 = $columns2[$column->getId()];
				$condition2->_setColumn($column2);
				// options in condition value
				if ($column->hasOptions()) {
					$options2 = array();
					$options = $condition->getValue();  /** @var $option Option */
					foreach ($options as $option) {
						if ($option == null) {
							$options2[] = null;
							continue;
						} else {
							$optionId = $option->getId();
							$optionId2 = $objectIdMap[$optionId];
							$options2[] = $column2->getOptionFor($optionId2, false, false);
						}
					}
					$condition2->_setValue($options2);
				}
				$conditions2[] = $condition2;
			}
			$criteria2->_setConditions($conditions2);
			$rule2->setCriteria($criteria2);

			$notificationConditionalRules2[] = $rule2;
		}

		$table2->_setColumns(array_values($columns2));
		$table2->setViewColumns($viewColumns2);
		if ($calendarSettings !== null) {
			$table2->setCalendarSettings($calendarSettings);
		}
        if ($readCheckSettings !== null)
            $table2->setReadCheckSettings($readCheckSettings);
		$table2->setNotificationViewColumns($notificationViewColumns2);
		$table2->setNotificationBasicRules($notificationBasicRules2);
		$table2->setNotificationConditionalRules(
			$notificationConditionalRules2);
		$table2->_setPermissions($permissions2);
		$table2->_setId($objectIdMap[$table->getId()]);

// $radioCols = $table2->getColumns(array('type'=>Column::RADIO));
// print_r($radioCols);
// exit;
		return $table2;
	}

	/**
	 * @return Storage
	 */
	public function getStorage()
	{
		return $this->storage;
	}

	/**
	 * @param Table $table
	 * @param User $by
	 * @throws TxCoreException
	 */
	public function dropTable(Table $table, User $by)
	{
		// 権限チェック
		$this->checkAutholity($table, 'drop', $by);

		// 参照チェック
		$this->checkReferences($table);

		$this->queries->deleteRowsByTable($table);
		$this->queries->deleteIndexesByTable($table);
		$this->queries->deleteDataByTable($table);
		$this->queries->deleteTableDef($table, true);
		$this->queries->deleteOptionsByTable($table, true);
		$this->queries->deleteFilterByTable($table);
		$this->queries->deleteViewColumnsByTable($table, true);
		$this->queries->deleteReferencesByTable($table, true);
		$this->queries->deleteColumnDefsByTable($table, true);
		$this->queries->deletePermissionsByTable($table, true);
		$this->queries->deleteNotificationViewColumnsByTable($table, true);
		$this->queries->deleteNotificationBasicRulesByTable($table, true);
		$this->queries->deleteNotificationConditionalRulesByTable($table, true);
		$this->queries->deleteCommentByTable($table);
		$this->queries->deleteNotificationsByTable($table);
		$this->queries->deleteNotificationReceiptsByTable($table);
        $this->queries->deleteViewCalendarSettingByTable($table, true);
        $this->queries->deleteReadCheckSettingsByTable($table->getId());
        $this->queries->deleteReadLogByTable($table->getId());

		// Remove icon
		$this->removeIcon($table->getIcon());

		// Remove files
		$this->storage->removeDir($table->getId());

		$table->_setId(null);
	}

	/**
	 * @param Table $table
	 * @param User $by
	 * @throws TxCoreException
	 */
	public function clearTableData(Table $table, User $by)
	{
		// 権限チェック
		$this->checkAutholity($table, 'delete', $by);

		$this->queries->deleteRowsByTable($table);
		$this->queries->deleteIndexesByTable($table);
		$this->queries->deleteDataByTable($table);
		$this->queries->deleteCommentByTable($table);
		$this->queries->deleteNotificationsByTable($table);
		$this->queries->deleteNotificationReceiptsByTable($table);
		$this->queries->deleteReadLogByTable($table->getId());

		// Remove files
		$this->storage->removeDir($table->getId());
	}

	// -- Private functions -----------------------------


	/**
	 * @param Table $table
	 */
	private function appendAutoColumns(Table $table)
	{
		$autoColumns = array(
			Column::ROWNUM,
			Column::CREATED_BY,
			Column::CREATED_ON,
			Column::UPDATED_BY,
			Column::UPDATED_ON,
		);
		foreach ($table->getColumns() as $column) {
			$p = array_search($column->getType(), $autoColumns);
			if ($p !== false) {
				unset($autoColumns[$p]);
			}
		}
		$addColumns = array();
		foreach ($autoColumns as $columnType) {
			$column = $table->addColumn($columnType);
			$column->setCode($column->getTypeName());
			$column->setLabel($column->getTypeName());
			$column->setShowsField(false);
			$column->setShowsLabel(true);
		}
	}

	/**
	 * @param $loginId
	 * @param $ignoreId (optional) User id for ignoring
	 * @return User|null Returns <code>true</code> if specified login id was available.
	 */
	public function findUserByLoginId($loginId, $ignoreId = null)
	{
		$loginId = trim($loginId);

		$result = null;
		if ($ignoreId != null) {
			$result = $this->db->query('
				select *
				from user where user.login_id = ? and user.id <> ?
				', array($loginId, $ignoreId));
		} else {
			$result = $this->db->query('
				select *
				from user where user.login_id = ?
				', array($loginId));
		}
		return is_empty($result) ? null : $this->_mapUser($result[0]);
	}

	private function &getAllListableColumnsMap(Table $table)
	{
		$map = array();
		foreach ($table->getColumns() as $column) {	/*@var $column Column */
			if ($column->isListable())
				$map[$column->getId()] = $column;
		}
		return $map;
	}

	private function &getSearchHeader(Table $table, Criteria $criteria)
	{
		$selectColumns = $criteria->getListColumns();
		if (is_empty($selectColumns))
			$selectColumns = array_map(function(ViewColumn $viewColumn) {
                return $viewColumn->getColumn();
            }, $table->getViewColumns());
		if (is_empty($selectColumns))
			$selectColumns = array_values($this->getAllListableColumnsMap($table));

		foreach ($selectColumns as $column) {/*@var $column Column */
			if (!$column->isListable()) {
				throw new TxCoreException(sprintf("Cound not specify column %s for list column.", $column->getType()),
					TxCoreException::INTERNAL);
			}
		}
		return $selectColumns;
	}

	public function _mapOption($obj, Column $parent)
	{
		$option = new Option($obj->id);
		$option->setName($obj->name);
		$option->_setParent($parent);
		$option->setSeq($obj->seq);
		return $option;
	}

	public function _mapUser($obj)
	{
		$user = new User();
		$user->_setId($obj->id);
		$user->setRole($obj->role);
		$user->setScreenName($obj->screen_name);
		$user->setLoginId($obj->login_id);
		$user->setProfile($obj->profile);
		$user->setSeq($obj->seq);
		$user->setPassword($obj->password);
		return $user;
	}

	/**
	 * @param $obj
	 * @return Group
	 */
	public function _mapGroup($obj)
	{
		$user = new Group();
		$user->_setId($obj->id);
		$user->setName($obj->name);
		return $user;
	}

	public function _mapColumn($obj, Table $parent)
	{
		$column = Column::factory($obj->column_type);
		$column->_setId($obj->id);
		$column->_setParent($parent);
// 		$column->_setRevision($obj->revision);
		$column->setLabel($obj->label);
		$column->setCode($obj->code);
		$column->setRawProperties((array)Util::decodeJSON($obj->properties));
		return $column;
	}

	/**
	 * @param \stdClass $obj
	 * @param $table
	 * @return Template
	 * @throws TxCoreException
	 */
	public function _mapFilter($obj, $table)
	{
		$filter = new Filter();
		$filter->_setId($obj->id);
		$filter->setName($obj->name);
		$filter->setCriteria(Criteria::fromArray(Util::decodeJSON($obj->expression), $table));
		return $filter;
	}

	/**
	 * @param \stdClass $obj
	 * @return Template
	 */
	public function _mapTemplate($obj)
	{
		$table = new Template($obj->id);
		$table->setName($obj->name);
		$table->setDescription($obj->description);
		$table->setIcon($obj->icon_filekey ? new Icon($obj->icon_filekey, false) : null);	// テンプレートはプリセットアイコンを持たない
		$table->setPublic(false);
		$table->setDefault($obj->is_default == 1);
		$table->setRoundScale($obj->round_scale);
		$table->setRoundType($obj->round_type);
		$table->_setRevision($obj->revision);
		$table->setPermissionControl($obj->permission_control == 1);
		return $table;
	}

	/**
	 * @param $obj
	 * @return Table
	 */
	public function _mapTable($obj)
	{
		$table = new Table($obj->id);
// 		$table->_setId($obj->id);
		$table->_setRevision($obj->revision);
		$table->setName($obj->name);
		$table->setDescription($obj->description);
		$table->setIcon($obj->icon_filekey ? new Icon($obj->icon_filekey, false) : null);
		$table->setPublic($obj->is_public == 1);
// 		$table->_setDefType($obj->def_type);
// 		$table->_setSaved($obj->is_saved == 1);
// 		$table->_setWorkTableId($obj->work_table_id);
		$table->setApiEnabled($obj->is_api_enabled == 1);
		$table->_setApiKey($obj->api_key);
		$table->setWriteApiEnabled($obj->is_write_api_enabled == 1);
		$table->_setWriteApiKey($obj->write_api_key);
		$table->_setOwnerId($obj->owner);
		$table->setRoundScale($obj->round_scale);
		$table->setRoundType($obj->round_type);
		$table->setPermissionControl($obj->permission_control == 1);
		$table->setCommentEnabled($obj->is_comment_enabled == 1);
		$table->setCalendarEnabled($obj->is_calendar_enabled == 1);
		$table->setCommentFollowingEnabled($obj->is_comment_following_enabled == 1);
        $table->setReadCheckEnabled(Util::toBool($obj->is_read_check_enabled));
		$table->setSixColumnsEnabled($obj->is_six_columns_enabled == 1);

		return $table;
	}

	/**
	 * @param $obj
	 * @return Permission
	 */
	public function _mapPermission($obj) 
	{
		$permission = new Permission();
		$permission->setTableId($obj->table_id);
		$permission->setRevision($obj->revision);
		$permission->setUserId($obj->user_id);
		$permission->setIsReadable($obj->is_readable == 1);
		$permission->setIsWritable($obj->is_writable == 1);
		return $permission;
	}

	/**
	 * @param Column $column
	 * @return array of Table
	 */
	public function getReferrerTables(Column $column)
	{
		$referrers = $this->db->query(
				'select distinct table_id from reference where target_column_id = ? ',
				array($column->getId())
		);

		$tables = array();
		foreach ($referrers as $referrer) {
			$tables[] = $this->getTable($referrer->table_id);
		}
		return $tables;
	}

	/**
	 * @param Table $table
	 * @return array of Table
	 */
	public function getReferrerTablesByTable(Table $table)
	{
		$referrers = $this->db->query(
				'select distinct table_id from reference where target_table_id = ? ',
				array($table->getId())
		);

		$tables = array();
		foreach ($referrers as $referrer) {
			$tables[] = $this->getTable($referrer->table_id);
		}
		return $tables;
	}

	/**
	 * @param $obj
	 * @return \TxCore\Reference
	 */
	public function _mapReference($obj)
	{
		$reference = new Reference();
		$reference->setColumnId($obj->column_id);
		$reference->setTableId($obj->table_id);
		$reference->setRevision($obj->revision);
		$reference->setTargetColumnId($obj->target_column_id);
		$reference->setTargetTableId($obj->target_table_id);
		return $reference;
	}

	/**
	 * @param Table $table
	 * @return ViewColumn[]
	 */
	public function getViewColumns(Table $table)
	{
		$list = $this->db->query('
			select * from view_column where table_id = ? and revision = ? order by `seq`',
			array($table->getId(), $table->getRevision())
		);
		$columns = array();
		foreach ($list as $row) {
            $viewColumn  = new ViewColumn($table);
            $viewColumn->_setColumnId($row->column_id);
            $viewColumn->setWidth($row->width);
			$columns[] = $viewColumn;
		}
		return $columns;
	}

	/**
	 * @param Icon $icon
	 * @throws TxCoreException
	 * @return \TxCore\Icon
	 */
	private function copyIcon(Icon $icon)
	{
		$fileName = Util::getUniqeFileName(pathinfo($icon->getFilekey(), PATHINFO_EXTENSION));
		$dest = get_config('app', 'user_icon_dir');
		if (!file_exists($icon->_getLocalPath())) {
			throw new TxCoreException('Icon file dose not exists : '.$icon->_getLocalPath());
		}

		copy($icon->_getLocalPath(), $dest.'/'.$fileName);
		return new Icon($fileName, false);
	}

	/**
	 * @param Icon $icon
	 */
	private function removeIcon(Icon $icon)
	{
		@unlink($icon->_getLocalPath());
	}

	/**
	 * リリース管理されている、リビジョン名を取得する
	 * @param Table $table
	 * @return string
	 */
	private function getRevisionName(Table $table)
	{
		$ctrls = $this->db->query('
			select * from release_control
			where table_id = ?
			', array($table->getId()));
		if (is_empty($ctrls))
			return null;
		if (util::toInt($ctrls[0]->release_revision) == $table->getRevision()) {
			return self::RELEASE;
		} else if (util::toInt($ctrls[0]->trunk_revision) == $table->getRevision()) {
			return self::TRUNK;
		} else {
			return null;
		}
	}

	/**
	 * @param Table $table
	 * @param Criteria $criteria
	 */
	private function subtotalTable(Table $table, Criteria $criteria)
	{
		$this_ = $this;
		$rows = array();
		$columns = $this->getSubtotalHeader($table, $criteria);

		$this->queries->selectSubtotal($table, $criteria, function($data) use (&$rows, $columns, $criteria, $table, $this_) {
			$subtotal = $criteria->getSubtotal();
			$row = new Row();
			$row->_setRawValue($columns[0], $subtotal->getGroupedColumn()->getOptionFor($data->option_id)->getName());
			$row->_setRawValue($columns[1], $data->value);
			$rows[] = $row;
		});

		$list = new List_();
		$list->_setType('table');
		$list->_setRows($rows);
		$list->_setTotalRowCount(count($rows));
		$list->_setOffset(0);
		$list->_setColumns($columns);
		$list->_setRowLimit(null);
		$list->_setTable($table);
		return $list;
	}

	/**
	 * @param Table $table
	 * @param Criteria $criteria
	 */
	private function getSubtotalHeader(Table $table, Criteria $criteria)
	{
		$label = null;
		$format = null;
		if ($criteria->getSubtotal()->getFunction() == Criteria::FN_COUNT) {
			$label = 'データ数';
		} else if ($criteria->getSubtotal()->getFunction() == Criteria::FN_SUM) {
			$label = '合計（'.$criteria->getSubtotal()->getAggregateColumn()->getLabel(). '）';
			$aggregateColumn = $criteria->getSubtotal()->getAggregateColumn();
			if ($aggregateColumn->getType() == Column::CALC &&
				$aggregateColumn->getFormat() == Column__Calc::FORMAT_HOURS) {
				$format = Column__Anonymous::FORMAT_HOURS;
			} else if ($aggregateColumn->getType() == Column::CALC &&
				$aggregateColumn->getFormat() == Column__Calc::FORMAT_DAYS) {
				$format = Column__Anonymous::FORMAT_DAYS;
			} else {
				$format = Column__Anonymous::FORMAT_DECIMAL;
			}
		}

		$groupedColumn = new Column__Anonymous(-1, null);
		$aggregateColumn = new Column__Anonymous(-2, $label, $format);
		$aggregateColumn->_setParent($table);
		return array($groupedColumn, $aggregateColumn);
	}

	/**
	 * @param Table $table
	 * @param string $default
	 */
	public function changeTableToTemplate(Table $table, $default = false)
	{
		$this->queries->updateTableDefToTemplate($table, $default);
	}

	public function scheduleOptimization()
	{
		if (get_config('app', 'optimization_scheduling_enabled') != 1) {
			return;
		}

		$lockFile = get_config('app', 'optimization_scheduling_lockfile');
		if (file_exists($lockFile)) {
			return;
		}

		$scheduleCommand = get_config('app', 'optimization_scheduling_command');

		$moratoriumOptions = get_config('app', 'optimization_scheduling_hour_moratorium');
		$moratoriums = explode(',', $moratoriumOptions);
		shuffle($moratoriums);
		$moratorium = $moratoriums[0]; // 猶予

		$hourOptions = get_config('app', 'optimization_scheduling_hour');
		$hours = explode(',', $hourOptions);
		shuffle($hours);
		$hour = $hours[0]; // 予定時刻

		$now = $this->getNow();
		$time = mktime(date('H', $now) + $moratorium, date('i', $now), date('s', $now),
					date('m', $now), date('d', $now), date('Y', $now));
		if (date('H', $time) >= $hour) {
			// 猶予後の時刻が、予定時刻より大きい場合、さらに翌日まで猶予する
			$mod = 24 - date('H', $time);
			$moratorium = $moratorium + $mod + $hour;
		} else {
			// 猶予後の時刻に対し、予定時刻までの差分を追加する
			$mod = $hour - date('H', $time);
			$moratorium = $moratorium + $mod;
		}

		$scheduleCommand = str_replace('{hours}', $moratorium, $scheduleCommand);
		exec($scheduleCommand);
		file_put_contents($lockFile, '');
	}

	public function executeOptimization()
	{
		$lockFile = get_config('app', 'optimization_scheduling_lockfile');
		$this->queries->executeOptimization();
		@unlink($lockFile);
	}

	/**
	 * @param Table $table
	 * @return Permission[]
	 */
	public function getPermissions(Table $table)
	{
		$list = $this->db->query('
			select * from permission where table_id = ? and revision = ? order by `user_id`', 
			array($table->getId(), $table->getRevision())
		);
		$permissions = array();
		foreach ($list as $row) {
			$permissions[] = $this->_mapPermission($row);
}
		return $permissions;
	}

	/**
	 * ユーザー情報（指定ID以外）を検索します
	 * @param $keyword
	 * @return array
	 */
	public function searchUsersExceptFor($exception = null, $keyword = null, $offset = 0, $limit = 20)
	{
		$this_ = $this;
		$query = array();
		$params = array();
		$query[] = "select sql_calc_found_rows * from user ";
		if(is_not_empty($exception) || is_not_empty($keyword)){
			$query[] = "where ";
			
			if (is_not_empty($keyword)) {
				$query[] = "(screen_name like :keyword or profile like :keyword) ";
				$params['keyword'] = '%'.Util::escapeLike($keyword).'%';

				if(is_not_empty($exception)){
					$query[] = " and ";
				}
			}
			if(is_not_empty($exception)){
				if(is_array($exception)){
					$query[] = "id NOT IN(";
					$firstDone = false;
					foreach($exception as $id){
						if($firstDone){
							$query[] = ",";
						}else{
							$firstDone = true;
						}
						$query[] = sprintf("'%d'", $id);
					}
					$query[] = ")";
				}else{
					$query[] = sprintf("id NOT IN('%d')", $exception);
				}
			}
		}
		
		$query[] = sprintf("order by screen_name limit %d, %d",
			$offset, $limit);
		
		$list = new List_();
		$rows = array();
		$users = $this->db->fetch(join(' ', $query), $params, function($data) use (&$rows, $this_) {
			$rows[] = $this_->_mapUser($data);
		});
		
		$total = $this->db->query('SELECT FOUND_ROWS() as count', array());
		$list->_setType('user');
		$list->_setRows($rows);
		$list->_setTotalRowCount($total[0]->count);
		$list->_setOffset($offset ?: 0);
		$list->_setRowLimit($limit);
		$list->_setTable(null);
		return $list;
	}

	/**
	 * @param Row $row
	 * @param Comment $comment
	 * @param User $by
	 * @throws TxCoreException
	 */
	public function createRowComment(Row $row, Comment $comment, User $by)
	{
		$table = $row->getParent();

		// 権限チェック
		$this->checkAuthorityForComment($comment, 'create', $by);

		//$comment->get
//		$comment->_setTableId($row->getId());
		$comment->_setNo($this->queries->selectMaxCommentNo($row) + 1);
		$comment->_setPostedUserId($by->getId());
		$comment->_setPostedTime(new \DateTime());

		$id = $this->queries->insertComment($row, $comment);
		$comment->_setId($id);

		// notification
		$this->notify("comment_insert", $by, $table, $row, null, $comment);
	}

	/**
	 * @param Comment $comment
	 * @param User $by
	 * @throws TxCoreException
	 */
	public function deleteComment(Comment $comment, User $by)
	{
		// 権限チェック
		$this->checkAuthorityForComment($comment, 'delete', $by);

		$this->queries->deleteNotificationsByComment($comment);
		$this->queries->deleteNotificationReceiptsByComment($comment);
		$this->queries->deleteComment($comment);
		$comment->_setId(null);
	}

	/**
	 * @param Row $row
	 * @return List_|Comment[]
	 * @throws \Exception
	 */
	public function searchRowComments(Row $row)
	{
		$this_ = $this;
		$query = array();
		$params = array();
		$query[] = "select sql_calc_found_rows * from `comment` where row_id = ?
				order by no desc";
		$params[] = $row->getId();

		$list = new List_();
		$rows = array();
		$this->db->fetch(join(' ', $query), $params, function($data) use (&$rows, $this_) {
			$rows[] = $this_->_mapComment($data);
		});

		$total = $this->db->query('SELECT FOUND_ROWS() as count', array());
		$list->_setType('comment');
		$list->_setRows($rows);
		$list->_setTotalRowCount($total[0]->count);
		$list->_setOffset(0);
		$list->_setRowLimit(null);
		$list->_setTable(null);
		return $list;
	}

	/**
	 * @param Row $row
	 * @return int
	 * @throws \Exception
	 */
	public function getRowCommentsCount(Row $row)
	{
		return intval($this->queries->selectCommentsCountByRow($row));
	}

	/**
	 * @param $id
	 * @param bool $silently
	 * @return Comment
	 * @throws TxCoreException
	 */
	public function getComment($id, $silently = false)
	{
		$rows = $this->queries->selectComment($id);
		if (!isset($rows[0])) {
			if ($silently) {
				return null;
			}
			throw new TxCoreException(sprintf("Comment %d dose not exist.", $id),
				TxCoreException::ROW_NOT_FOUND);
		}
		return $this->_mapComment($rows[0]);
	}

	/**
	 * @param $obj
	 * @return Comment
	 */
	public function _mapComment($obj)
	{
		$comment = new Comment();
		$comment->setText($obj->text);
		$comment->_setId($obj->id);
		$comment->_setNo($obj->no);
		$comment->_setPostedTime($obj->posted_time);
		$comment->_setPostedUserId($obj->posted_user_id);
		if ($obj->notification_targets) {
			$comment->_setNotificationTargets($this->_mapEntities(Util::decodeJSON($obj->notification_targets)));
		}
		return $comment;
	}

	/**
	 * @param array $entities idとtypeを持ったObject
	 * @return array
	 */
	private function _mapEntities($entities)
	{
		$users = array();
		foreach ($entities as $entity) {
			if ($entity->type === 'user') {
				$user = $this->getUser($entity->id, false, true);
				if ($user)
					$users[] = $user;
			} else if ($entity->type === 'group') {
				$group = $this->getGroup($entity->id, true);
				if ($group)
					$users[] = $group;
			}
		}
		return $users;
	}

	/**
	 * @param object $setting
	 * @param Table $table
	 * @return CalendarSettings
	 */
	private function _mapCalendarSetting($setting, $table)
	{
		$settings = new CalendarSettings($table);
		$settings->setDateColumnId($setting->date_column_id);
		$settings->setEndTimeColumnId($setting->end_time_column_id);
		$settings->setStartTimeColumnId($setting->start_time_column_id);
		$settings->setTitleColumnId($setting->title_column_id);
		$settings->setUserColumnId($setting->user_column_id);
		return $settings;
	}

    /**
     * @param object $setting
     * @return ReadCheckSettings
     */
    private function _mapReadCheckSetting($setting)
    {
        $settings = new ReadCheckSettings();
        $settings->setAutoreadEnabled(Util::toBool($setting->is_autoread_enabled));
        $settings->setOnlistEnabled(Util::toBool($setting->is_onlist_enabled));
        $settings->setCheckTargetsArray(Util::decodeJSON($setting->check_targets));
        return $settings;
    }

	private function getRowChange(Table $table, Row $oldrow, Row $newrow)
	{
		$changes = array();
		foreach ($table->getColumns() as $column) { /* @var $column Column */
			$change = $column->handleGetValueChange($oldrow, $newrow);
			if ($change) $changes[] = $change;
		}
		return $changes;
	}

	public function getNotificationViewColumns(Table $table)
	{
		$list = $this->queries->selectNotificationViewColumns($table);
		$columns = array();
		foreach ($list as $row) {
			$column = $table->getColumnFor(Util::toInt($row->column_id), true);
			if ($column !== null) {
				$columns[] = $column;
			}
		}
		return $columns;
	}

	public function getNotificationBasicRules(Table $table)
	{
		$result = array();
		$list = $this->queries->selectNotificationBasicRules($table);
		foreach ($list as $row) {
			$result[] = $this->_mapNotificationBasicRule($row, $table);
		}
		return $result;
	}

	private function _mapNotificationBasicRule($obj, Table $table)
	{
		$rule = new NotificationBasicRule();
		$rule->_setParent($table);
		$rule->setTargetingType($obj->targeting_type);
		$rule->setTargetsArray(Util::decodeJSON($obj->targets));
		$rule->setOnRowInsert(Util::toBool($obj->is_on_row_insert));
		$rule->setOnRowUpdate(Util::toBool($obj->is_on_row_update));
		$rule->setOnCommentInsert(Util::toBool($obj->is_on_comment_insert));
		return $rule;
	}

	public function getNotificationConditionalRules(Table $table)
	{
		$result = array();
		$list = $this->queries->selectNotificationConditionalRules($table);
		foreach ($list as $row) {
			$result[] = $this->_mapNotificationConditionalRule($row, $table);
		}
		return $result;
	}

	private function _mapNotificationConditionalRule($obj, Table $table)
	{
		$rule = new NotificationConditionalRule();
		$rule->_setParent($table);
		$rule->setTargetingType($obj->targeting_type);
		$rule->setTargetsArray(Util::decodeJSON($obj->targets));
		$rule->setCriteria(NotificationCriteria::fromArray($obj->expression, $table));
		$rule->setMessage($obj->message);
		return $rule;
	}

	/**
	 * 受信通知を検索します
	 * @param $keyword
	 * @param int $includingUserId
	 * @return List_
	 * @throws \Exception
	 */
	public function searchNotificationReceipts(
		User $user, $read = false, $offset = 0, $limit = 20)
	{
		$this_ = $this;
		$query = array();
		$params = array();
		$query[] = "select sql_calc_found_rows * from `notification_receipt` ";
		$query[] = "where 1=1";
		$query[] = "and is_read = :read";
		$query[] = "and recipient_id = :recipient_id";
		$query[] = "order by notified_on desc, notification_id desc";
		$query[] = sprintf("limit %d, %d", $offset, $limit);
		$params['read'] = Util::toBoolInt($read);
		$params['recipient_id'] = $user->getId();

		$list = new List_();
		$rows = array();
		$this->db->fetch(join(' ', $query), $params, function($data) use (&$rows, $this_) {
			$rows[] = $this_->_mapNotificationReceipt($data);
		});

		$total = $this->db->query('SELECT FOUND_ROWS() as count', array());
		$list->_setType('notification_receipt');
		$list->_setRows($rows);
		$list->_setTotalRowCount($total[0]->count);
		$list->_setOffset($offset ?: 0);
		$list->_setRowLimit($limit);
		$list->_setTable(null);
		return $list;
	}

	/**
	 * @param $id
	 * @param bool $silently
	 * @return Notification
	 * @throws TxCoreException
	 */
	public function getNotification($id, $silently = false)
	{
		$rows = $this->queries->selectNotification($id);
		if (!isset($rows[0])) {
			if ($silently) {
				return null;
			}
			throw new TxCoreException(sprintf("Notification %d dose not exist.", $id),
				TxCoreException::ROW_NOT_FOUND);
		}
		return $this->_mapNotification($rows[0]);
	}

	private function _mapNotification($obj)
	{
		$notification = new Notification();
		$notification->_setId($obj->id);
		$notification->_setNotifiedOn($obj->notified_on);
		$notification->setMessage($obj->message);
		$notification->setChangesFromArray($obj->changes);
		$notification->_setNotifierId($obj->notifier_id);
		$notification->_setTableId($obj->table_id);
		$notification->_setRowId($obj->row_id);
		$notification->_setCommentId($obj->comment_id);
		return $notification;
	}

	/**
	 * @param $id
	 * @param bool $silently
	 * @return NotificationReceipt
	 * @throws TxCoreException
	 */
	public function getNotificationReceipt($notificationId, User $user,
		$silently = false)
	{
		$rows = $this->queries->selectNotificationReceipt(
			$notificationId, $user);
		if (!isset($rows[0])) {
			if ($silently) {
				return null;
			}
			throw new TxCoreException(sprintf("NotificationReceipt (Notirication %d, User %d) dose not exist.", $notificationId, $user->getId()),
				TxCoreException::ROW_NOT_FOUND);
		}
		return $this->_mapNotificationReceipt($rows[0]);
	}

	public function _mapNotificationReceipt($obj)
	{
		$receipt = new NotificationReceipt();
		$receipt->_setNotificationId($obj->notification_id);
		$receipt->_setRecipientId($obj->recipient_id);
		$receipt->setRead(Util::toBool($obj->is_read));
		return $receipt;
	}

	/**
	 * 通知を保存する
	 * @param NotificationReceipt $receipt
	 * @throws TxCoreException
	 */
	public function saveNotificationReceipt(
		NotificationReceipt $receipt)
	{
		$this->queries->updateNotificationReceipt($receipt);
	}

	/**
	 * @param User $recipient
	 * @param bool $read
	 * @return int
	 * @throws \Exception
	 */
	public function countNotificationReceipts(User $recipient, $read = false)
	{
		return intval($this->queries->selectNotificationReceiptsCountByRecipient($recipient, $read));
	}

	private function updateRelationships(User $by, Row $row, Column $column, $notify = false)
	{
		$referenceTable = $column->getReferenceTable();
		$rows = $row->getValue($column);
		$rows = is_array($rows) ? $rows : array();
		foreach ($rows as $child) {
			switch ($child->getAction()) {
				case Row::ACTION_INSERT:
					$this->insertRow($child, $by, $notify);
					$this->insertRelationship($column, $row, $child);
					break;
				case Row::ACTION_UPDATE:
					$this->updateRow($child, $by, $notify);
					break;
				case Row::ACTION_DELETE:
					$this->deleteRelationship($column, $row, $child);
					$this->deleteRow($child, $by);
					break;
			}
		}
	}

	private function deleteRelationships(User $by, Row $row, Column $column)
	{
		$table = $column->getReferenceTable();
		$rows = $table->getRelationshipRows($row, $column, $by);
		foreach ($rows as $child) {
			$this->deleteRelationship($column, $row, $child);
			$this->deleteRow($child, $by);
		}
	}

	private function selectRelationships(
		Row $srcRow, Column $srcColumn)
	{
		$list = $this->queries->selectRelationships($srcRow, $srcColumn);
		$relationships = array();
		foreach ($list as $data) {
			$relationships[] = $this->_mapRelationship(
				$data, $srcRow, $srcColumn);
		}
		return $relationships;
	}

	public function _mapRelationship($data,
		Row $srcRow, Column $srcColumn)
	{
		$obj = new Relationship();
		$obj->setSourceColumn($srcColumn);
		$obj->setSourceRow($srcRow);
		$obj->_setTargetRowId($data->target_row_id);
		//$obj->setTargetTable($targetTable);
		return $obj;
	}

	/**
	 * @param Table $table
	 * @param Row $row
	 * @param Column $column
	 * @param Criteria $criteria
	 * @param User $by
	 * @param int $limit Optional
	 * @param int $offset Optional
	 * @return List_
	 */
	public function searchRelationships(Table $table, Row $row, Column $column, Criteria $criteria, User $by, $offset = null, $limit = null)
	{
		// 参照権限をチェック
		$this->checkAutholity($table, 'select', $by);

		return $this->searchRelationshipsInternal($table, $row, $column, $criteria, $offset, $limit);
	}

	/**
	 * @param Table $table
	 * @param Row $row
	 * @param Column $column
	 * @param User $by
	 * @return array
	 */
	public function getRelationshipRows(Table $table, Row $row, Column $column, User $by)
	{
		// 参照権限をチェック
		$this->checkAutholity($table, 'select', $by);

		$rows = array();
		$this->featchRelationships($table, $row, $column, new Criteria(),
			function($row) use (&$rows) {
				$rows[] = $row;
			},
			null,
			null);

		return $rows;
	}

	/**
	 * @param Table $table
	 * @param Row $row
	 * @param Column $column
	 * @param Criteria $criteria
	 * @param int $limit Optional
	 * @param int $offset Optional
	 */
	public function searchRelationshipsInternal(Table $table, Row $row, Column $column, Criteria $criteria, $offset = null, $limit = null)
	{
		$errors = $criteria->validate();
		if (is_not_empty($errors)) {
			throw new TxCoreException('Criteria has any error : '. $errors[0]->getMessage(),
				TxCoreException::VALIDATION_FAILED, $errors);
		}

		$rows = array();
		$listColumns = $this->getSearchHeader($table, $criteria);
		$count = $this->countRelationships($row, $column);

		if ($count > 0) {
			$this->featchRelationships($table, $row, $column, $criteria,
				function($row) use (&$rows) {
					$rows[] = $row;
				},
				$offset,
				$limit);
		}

		$rows = $this->sortRows($rows, $criteria);

		$list = new List_();
		$list->_setType('table');
		$list->_setRows($rows);
		$list->_setTotalRowCount($count);
		$list->_setOffset($offset ?: 0);
		$list->_setColumns($listColumns);
		$list->_setRowLimit($limit);
		$list->_setTable($table);
		return $list;
	}

	/**
	 * @param Table $table
	 * @param Criteria $criteria
	 * @param int $limit Optional
	 * @param int $offset Optional
	 */
	public function searchRelationshipsForRowInternal(Table $table, Row $row, Column $column, Criteria $criteria, $offset = null, $limit = null)
	{
		$errors = $criteria->validate();
		if (is_not_empty($errors)) {
			throw new TxCoreException('Criteria has any error : '. $errors[0]->getMessage(),
				TxCoreException::VALIDATION_FAILED, $errors);
		}

		$rows = array();
		$listColumns = $this->getSearchHeader($table, $criteria);
		$base = array(
			'name' => 'relationship',
			'params' => array(
				'sourceRow' => $row,
				'sourceColumn' => $column,
			),
		);
		$count = $this->queries->selectQueryCount($table, $criteria, $base);

		if ($count > 0) {
			$this->featchTable($table, $criteria,
				function($row) use (&$rows) {
					$rows[] = $row;
				},
				$offset,
				$limit,
				$base);
		}

		$list = new List_();
		$list->_setType('table');
		$list->_setRows($rows);
		$list->_setTotalRowCount($count);
		$list->_setOffset($offset ?: 0);
		$list->_setColumns($listColumns);
		$list->_setRowLimit($limit);
		$list->_setTable($table);
		return $list;
	}

	public function sortRows($rows = array(), Criteria $criteria = null)
	{
		if (is_empty($rows)) {
			return $rows;
		}
		if (is_empty($criteria)) {
			return $rows;
		}
		$sorts = $criteria->getSort();
		if (is_empty($sorts)) {
			return $rows;
		}
		$hasRownum = false;
		foreach ($sorts as $sort) {
			if ($sort->getColumn()->getType() == Column::ROWNUM) {
				$hasRownum = true;
				break;
			}
		}
		if (!$hasRownum) {
			foreach ($rows as $row) {
				$rownum = $row->getParent()->getRownum();
				$sorts[] = new Sort($rownum, false);
				break;
			}
		}
		uasort($rows, function($elem1, $elem2) use ($sorts) {
			$row1 = $elem1;
			$row2 = $elem2;
			return $row1->compareToSort($row2, $sorts);
		});
		return $rows;
	}

	/**
	 *
	 * @param Table $table
	 * @param Row $sourceRow
	 * @param Column $sourceColumn
	 * @param Criteria $criteria
	 * @param callable $callable
	 * @param int $offset
	 * @param int $limit
	 * @throws \Exception
	 */
	public function featchRelationships($table, Row $sourceRow, Column $sourceColumn, Criteria $criteria, $callable, $offset = null, $limit = null)
	{
		$this_ = $this;
		//list($query, $params) = $this->queries->buildRelationshipsSearchQuery($table, $sourceRow, $sourceColumn, $criteria, false, $offset, $limit);

		$columnMap = $this->getAllListableColumnsMap($table, $criteria);
		$optionCache = array();

		$query = '
			select `data`.*, `row`.version, `row`.row_no
			from
				`relationship`
				join `row`
					on (`relationship`.target_row_id = `row`.id)
				join `data`
					on (`row`.id = `data`.row_id)
			where
				`relationship`.source_row_id = ? and
				`relationship`.source_column_id = ?
			order by
				`data`.row_id,
				`data`.column_id';
		$params =  array($sourceRow->getId(), $sourceColumn->getId());

		$row = null;	/** @var Row $row */
		$index = 0;
		$this->db->fetch($query, $params, function($data) use (&$row, &$columnMap, $table, $callable, &$index) {
			if ($row != null && $row->getId() != $data->row_id) {
				$ret = call_user_func_array($callable, array($row, $index));
				if ($ret === false) {
					return false;
				}
				$row = null;
				$index++;
			}
			if ($row === null) {
				$row = new Row();
				$row->_setId($data->row_id);
				$row->_setParent($table);
				$row->setVersion($data->version);
				$row->setRowNo($data->row_no);
				foreach ($columnMap as $column) {
					$row->_setRawValue($column, null);
				}
			}
			if(isset($columnMap[$data->column_id]))
				$row->_setRawValue($columnMap[$data->column_id], $data->value);
		});
		if ($row != null)
			call_user_func_array($callable, array($row, $index));
	}

	/**
	 * @return bloolean
	 */
	public function isTableChild(Table $table, $trunkOrRelease = false)
	{
		return $this->queries->selectIsTableChild(
			$table, $trunkOrRelease) == 1;
	}

	/**
	 * @return bloolean
	 */
	public function isTableParent(Table $table, $trunkOrRelease = false)
	{
		return $this->queries->selectIsTableParent(
			$table, $trunkOrRelease) == 1;
	}

	public function countRelationships(Row $row, Column $column)
	{
		return $this->queries->selectRelationshipsCount($row, $column);
	}

	public function countRelationshipsByColumn(Column $sourceColumn) {
		return $this->queries->selectRelationshipsCountByColumn(
			$sourceColumn);
	}

	/**
	 * @param Row $column
	 * @param Row $row
	 * @param Row $targetRow
	 * @throws \Exception
	 */
	public function insertRelationship(Column $column, Row $row, Row $targetRow)
	{
		$this->queries->insertRelationship($column, $row, $targetRow);
	}

	/**
	 * @param Row $column
	 * @param Row $row
	 * @param Row $targetRow
	 * @throws \Exception
	 */
	public function deleteRelationship(Column $column, Row $row, Row $targetRow)
	{
		$this->queries->deleteRelationship($column, $row, $targetRow);
	}

	/**
	 * @param Table $table
	 * @return array of Table
	 */
	public function getParentTablesByTable(Table $table)
	{
		$tableIds = $this->queries->selectParentTableIdsByTable(
			$table->getId());

		$tables = array();
		foreach ($tableIds as $obj) {
			$tables[] = $this->getTable($obj->table_id);
		}
		return $tables;
	}

    /**
     * @param Row $row
     * @param User $user
     * @return ReadLog
     */
    public function getReadState(Row $row, User $user)
    {
        $readLogs_ = $this->queries->selectReadLog($row->getId(), $user->getId());
        if (is_empty($readLogs_)) {
            $readLog = new ReadLog();
            $readLog->_setReadUser($user);
        } else {
            $readLog = new ReadLog();
            $readLog->_setReadUser($user);
            $readLog->_setReadOn($readLogs_[0]->read_on);
            $readLog->_setComment($readLogs_[0]->comment);
        }
        return $readLog;
    }

    /**
     * @param Row $row
     * @param int $offset
     * @param int $limit
     * @return List_|ReadLog[]
     * @throws TxCoreException
     */
    public function getRowReadList(Row $row, $offset = 0, $limit = 20)
    {
        $table = $row->getParent();

        $userList = $this->searchReadCheckUsers($table, $offset, $limit);
        $userList->_setType(null);

        $readLogs_ = $this->queries->selectReadLogs($row->getId());

        // Map
        $map = array();
        foreach ($readLogs_ as $log_) {
            $map[$log_->read_user_id] = $log_;
        }

        // Merge.
        $logs = array();
        $rows = &$userList->getRows();   /** @var $rows User[] */
        foreach ($rows as $user)
        {
            $log = new ReadLog();
            $log->_setReadUser($user);
            if (isset($map[strval($user->getId())])) {
                $log->_setReadOn($map[strval($user->getId())]->read_on);
                $log->_setComment($map[strval($user->getId())]->comment);
            }

            $logs[] = $log;
        }

        $userList->_setRows($logs);
        return $userList;
    }

    /**
     * @param Row $row
     * @return int
     */
    public function getRowReadLogCount(Row $row)
    {
		$parent = $row->getParent();
		if (!$parent) return null;
        $readSettings = $parent->getReadCheckSettings();
        $checkTargets = $readSettings->getCheckTargets();
        if (is_empty($readSettings->getCheckTargets())) {
            $data = $this->queries->selectRowReadLogCount($row->getId());
            return intval($data);
        } else {
            return $this->countRowReadLogs($row);
        }
    }

    private function countRowReadLogs($row)
    {
        $table = $row->getParent();
        $users = &$table->getAllReadCheckUsers();
        $readLogs_ = $this->queries->selectReadLogs($row->getId());

        // Map
        $map = array();
        foreach ($readLogs_ as $log_) {
            $map[$log_->read_user_id] = $log_;
        }

        // Count.
        $count = 0;
        foreach ($users as $user)
        {
            if (isset($map[strval($user->getId())])) {
                $count++;
            }
        }

        return $count;
    }

    public function searchReadCheckUsers($table, $offset = 0, $limit = 20)
    {
        if (!$table->isReadCheckEnabled()) {
            throw new TxCoreException(
                sprintf('Read check not enabled.'), TxCoreException::UNSUPPORTED);
        }

        // Expand into user array.
        $checkTargets = $table->getReadCheckSettings()->getCheckTargets();

        $criteria = new UserSearchCriteria;
        if (is_not_empty($checkTargets)) {
            $criteria->setInEntities($table->getReadCheckSettings()->getCheckTargets());
        }
        $userList = $this->searchUsers_($criteria, $offset, $limit);
        return $userList;
    }

    /**
     * @param Row $row
     * @param User $user
     * @param $comment
     * @throws TxCoreException
     */
    public function makeRowRead(Row $row, User $user, $comment = null)
    {
        if (!$row->getParent()->isReadCheckTarget($user)) {
            throw new TxCoreException(
                sprintf('User is not target.'), TxCoreException::INTERNAL);
        }

        if ($this->hasUserReadRow($row, $user)) {
            $this->queries->updateReadLogComment(
                $row->getId(), $user->getId(), $comment);
        } else {
            $this->queries->insertToReadReadLog($row->getParent()->getId(),
                $row->getId(), $user->getId(), $comment);
        }
    }

    /**
     * @param Row $row
     * @param User $user
     * @return bool
     */
    public function hasUserReadRow(Row $row, User $user)
    {
        $readLogs_ = $this->queries->selectReadLogByUser(
            $row->getId(), $user->getId());
        return sizeof($readLogs_) > 0;
    }

	/**
	 * @param Table $table
	 * @param Criteria $criteria
	 */
	private function aggregateTable(Table $table, Criteria $criteria)
	{
		$this_ = $this;
		$rows = array();
		$fns = array();
		$aggregateColumnMap = array();
		$aggregateFunctionMap = array();
		
		// 集計するカラムの情報を取得
		$aggregate =  $criteria->getAggregate();

		// 集計対象のカラムマップ・使用する集約関数情報の作成
		foreach($aggregate as $pair){
			$aggregateFunctionMap[$pair[0]->getId()] = $pair[1];
			$fns = array_merge($fns, $pair[1]);
		}
		$fns = array_unique($fns);
		
		if(in_array(Criteria::FN_SUM, $fns)) $rows[Criteria::FN_SUM] = new Row();
		if(in_array(Criteria::FN_AVG, $fns)) $rows[Criteria::FN_AVG] = new Row();
		if(in_array(Criteria::FN_MAX, $fns)) $rows[Criteria::FN_MAX] = new Row();
		if(in_array(Criteria::FN_MIN, $fns)) $rows[Criteria::FN_MIN] = new Row();
		
		// 一覧に表示するカラムを取得
		$listColumns = $this->getSearchHeader($table, $criteria);

		// Row初期化＆カラムマップ生成
		$columnMap = array();
		foreach($listColumns as $column){
			// カラムマップ生成
			$columnMap[$column->getId()] = $column;
		}

		// listColumnsの先頭に集約関数の種類を示すラベル表示用カラムを挿入する
		$tmpColumn = new Column__AggregateType(-1, null);
		$tmpColumn->_setType('aggregate_type');
		array_unshift($listColumns, $tmpColumn);
		foreach($fns as $fn){
			switch($fn){
				case Criteria::FN_SUM:
					$rows[Criteria::FN_SUM]->_setRawValue($tmpColumn, '合計');
					break;
				case Criteria::FN_AVG:
					$rows[Criteria::FN_AVG]->_setRawValue($tmpColumn, '平均');
					break;
				case Criteria::FN_MAX:
					$rows[Criteria::FN_MAX]->_setRawValue($tmpColumn, '最大');
					break;
				case Criteria::FN_MIN:
					$rows[Criteria::FN_MIN]->_setRawValue($tmpColumn, '最小');
			}
		}

		$this->queries->selectAggregate($table, $criteria,
			function($data) use (&$rows, $criteria, $table, $this_, $columnMap, $fns, $aggregateFunctionMap) {
				if(isset($columnMap[$data->column_id])){
					$column = $columnMap[$data->column_id];
					
					foreach($fns as $fn){
						switch($fn){
							case Criteria::FN_SUM:
								// 集約対象のカラムの場合のみ値をセットする
								if(array_key_exists($data->column_id, $aggregateFunctionMap) &&
									in_array(Criteria::FN_SUM ,$aggregateFunctionMap[$data->column_id])){
										$value = Math::bcround($data->sum_value, $column->getParent()->getMathContext());
										$rows[Criteria::FN_SUM]->_setRawValue($column, $value);
								}
								break;
							case Criteria::FN_AVG:
								if(array_key_exists($data->column_id, $aggregateFunctionMap) &&
									in_array(Criteria::FN_AVG ,$aggregateFunctionMap[$data->column_id])){
										$value = Math::bcround($data->avg_value, $column->getParent()->getMathContext());
										$rows[Criteria::FN_AVG]->_setRawValue($column, $value);
								}
								break;
							case Criteria::FN_MAX:
								if(array_key_exists($data->column_id, $aggregateFunctionMap) &&
									in_array(Criteria::FN_MAX ,$aggregateFunctionMap[$data->column_id])){
										$value = Math::bcround($data->max_value, $column->getParent()->getMathContext());
										$rows[Criteria::FN_MAX]->_setRawValue($column, $value);
								}
								break;
							case Criteria::FN_MIN:
								if(array_key_exists($data->column_id, $aggregateFunctionMap) &&
									in_array(Criteria::FN_MIN ,$aggregateFunctionMap[$data->column_id])){
										$value = Math::bcround($data->min_value, $column->getParent()->getMathContext());
										$rows[Criteria::FN_MIN]->_setRawValue($column, $value);
								}
								break;
						}
					}
				}
			}
		);

		$list = new List_();
		$list->_setType('table');
		$list->_setRows($rows);
		$list->_setTotalRowCount(count($rows));
		$list->_setOffset(0);
		$list->_setColumns($listColumns);
		$list->_setRowLimit(null);
		$list->_setTable($table);
		return $list;
	}

}
