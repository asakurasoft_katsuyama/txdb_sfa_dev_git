<?php
namespace TxCore;

use TxCore\Engine;
use TxCore\Table;
use TxCore\Filter;

class Sort
{

	private $column;
	private $reverse = false;
	
	public function __construct($column, $reverse = false) 
	{
		$this->column = $column;
		$this->reverse = $reverse;
	}
	
	/**
	 * @return Column
	 */
	public function getColumn () 
	{
		return $this->column;
	}
	
	/**
	 * @return bool
	 */
	public function isReverse()
	{
		return $this->reverse;
	}
}
