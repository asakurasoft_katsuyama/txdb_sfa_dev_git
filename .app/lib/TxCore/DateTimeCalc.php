<?php
namespace TxCore;

use \DateTime;
use \DateTimeZone;
use \DateInterval;

class DateTimeCalc
{
	protected $datetime;

	protected $intervals = array(
		'year' => array(12, 'month'),
		'month' => array(30, 'day'),
		'week' => array(7, 'day'),
		'day' => array(24, 'hour'),
		'hour' => array(60, 'minute'),
		'minute' => array(60, 'second'),
	);

	protected $scale = 8;

	public static function create($time = "now", DateTimeZone $timezone = null)
	{
		$timezone = is_null($timezone) ? new DateTimeZone('UTC') : $timezone ;
		return new self(new DateTime($time, $timezone));
	}

	public function __construct(DateTime $datetime)
	{
		$this->datetime = $datetime;
	}

	public function convertTimeInterval($value, $unit, $ba = 'after')
	{
		$sign = $ba === 'before' ? -1 : 1 ;
		if ($value < 0) {
			$value = abs($value);
			$sign *= -1;
		}
		$spec = $this->convertUnitFormatToIntervalSpec($value, $unit, $ba);
		$interval = new DateInterval($spec);
		if ($sign < 0) $interval->invert = 1;
		return $interval;
	}

	public function checkLastdayOfMonth($year, $month, $day, $ba = after)
	{
		$curryear = (int) $this->datetime->format('Y');
		$currmonth = (int) $this->datetime->format('m');
		$currday = (int) $this->datetime->format('d');

		if ($currday > 28) {
			if ($ba === 'after') {
				$newmonth = ($currmonth + $month) % 12;
				$newmonth = $newmonth == 0 ? 12 : $newmonth ;
				$newyear = $curryear + $year + (int)(($currmonth + $month - 1) / 12) ;
			} else {
				$newmonth = ($currmonth - $month) % 12;
				$newmonth = $newmonth <= 0 ? $newmonth + 12 : $newmonth ;
				$newyear = $curryear - $year - (int)((12 - $currmonth + $month) / 12) ;
			}

			$newday = $currday;

			//echo "Now checking {$newyear}-{$newmonth}-{$currday}\n";

			if (!checkdate($newmonth, $currday, $newyear)) {
				$ut = mktime(0, 0, 0, $newmonth, 1, $newyear);
				$newday = (int) date('t', $ut);
				$base = (int) $this->datetime->format('U');
				$lastday = mktime(0, 0, 0, $newmonth, $newday, $newyear);
				$delta = (int)(abs($lastday - $base) / 86400);
				$year = 0;
				$month = 0;
				$day = $delta + $day;
			}
		}

		//echo " => {$year}/{$month}/{$day}\n";

		return array($year, $month, $day);
	}

	public function convertUnitFormatToIntervalSpec($value, $unit, $ba = 'after')
	{
		list($year, $month, $day, $hour, $minute) = $this->convertUnitFormatToTMD($value, $unit);

		//echo "Delta {$year}/{$month}/{$day}/{$hour}/{$minute}\n";

		list($year, $month, $day) = $this->checkLastdayOfMonth($year, $month, $day, $ba);

		$period = "P";
		if ($year > 0) $period .= $year . 'Y';
		if ($month > 0) $period .= $month . 'M';
		if ($day > 0) $period .= $day . 'D';
		$period .= "T";
		if ($hour > 0) $period .= $hour . 'H';
		if ($minute > 0) $period .= $minute . 'M';
		if (substr($period, -1) === 'T') $period = substr($period, 0, -1);
		return $period;
	}

	public function convertUnitFormatToTMD($value, $unit)
	{
		$scale = $this->scale;
		$result = array();
		foreach($this->intervals as $k => $v) {
			//echo "k: {$k}, value: {$value}\n";
			if ($unit !== $k) {
				if ($k !== 'week') $result[] = 0;
				continue;
			}
			if ($k !== 'week') {
				$intval = (int) $value;
				$result[] = $intval;
				//$value = ($value - $intval) * $v[0];
				$value = bcmul(bcsub($value, $intval, $scale), $v[0], $scale);
			} else {
				$value = bcmul($value, $v[0], $scale);
			}
			$unit = $v[1];
		}
		return $result;
	}

	public function add($value, $unit, $ba = 'after')
	{
		$this->datetime->add($this->convertTimeInterval($value, $unit, $ba));
		return $this;
	}

	public function sub($value, $unit, $ba = 'after')
	{
		$this->datetime->sub($this->convertTimeInterval($value, $unit, $ba));
		return $this;
	}

	public function format($format)
	{
		return $this->datetime->format($format);
	}

}

/*

function test($answer, $date, $val, $unit, $ba = 'after')
{
	$dt = DateTimeCalc::create($date);
	$result = $dt->add($val, $unit, $ba)->format('Y-m-d');
	echo "{$date}: {$val} {$unit} {$ba} : {$answer} == {$result} " . ($answer === $result ? "Success!!" : "FAILED") . "\n";
}

test('2014-09-07', '2014-08-31', 1, 'week');
test('2014-09-14', '2014-08-31', 2, 'week');
test('2014-09-21', '2014-08-31', 3, 'week');
test('2014-09-28', '2014-08-31', 4, 'week');
test('2014-10-05', '2014-08-31', 5, 'week');
test('2014-10-12', '2014-08-31', 6, 'week');

echo "---------------------\n";

test('2014-08-24', '2014-08-31', 1, 'week', 'before');
test('2014-08-17', '2014-08-31', 2, 'week', 'before');
test('2014-08-10', '2014-08-31', 3, 'week', 'before');
test('2014-08-03', '2014-08-31', 4, 'week', 'before');
test('2014-07-27', '2014-08-31', 5, 'week', 'before');
test('2014-07-20', '2014-08-31', 6, 'week', 'before');

echo "---------------------\n";

test('2014-09-30', '2014-08-31', 1, 'month');
test('2014-10-31', '2014-08-31', 2, 'month');
test('2014-11-30', '2014-08-31', 3, 'month');
test('2014-12-31', '2014-08-31', 4, 'month');
test('2015-01-31', '2014-08-31', 5, 'month');
test('2015-02-28', '2014-08-31', 6, 'month');

echo "---------------------\n";

test('2014-07-31', '2014-08-31', 1, 'month', 'before');
test('2014-06-30', '2014-08-31', 2, 'month', 'before');
test('2014-05-31', '2014-08-31', 3, 'month', 'before');
test('2014-04-30', '2014-08-31', 4, 'month', 'before');
test('2014-03-31', '2014-08-31', 5, 'month', 'before');
test('2014-02-28', '2014-08-31', 6, 'month', 'before');
test('2014-01-31', '2014-08-31', 7, 'month', 'before');
test('2013-12-31', '2014-08-31', 8, 'month', 'before');
test('2013-11-30', '2014-08-31', 9, 'month', 'before');

echo "---------------------\n";

test('2005-02-28', '2004-02-29', 1, 'year');
test('2006-02-28', '2004-02-29', 2, 'year');
test('2007-02-28', '2004-02-29', 3, 'year');
test('2008-02-29', '2004-02-29', 4, 'year');
test('2009-02-28', '2004-02-29', 5, 'year');
test('2010-02-28', '2004-02-29', 6, 'year');

echo "---------------------\n";

test('2003-02-28', '2004-02-29', 1, 'year', 'before');
test('2002-02-28', '2004-02-29', 2, 'year', 'before');
test('2001-02-28', '2004-02-29', 3, 'year', 'before');
test('2000-02-29', '2004-02-29', 4, 'year', 'before');
test('1999-02-28', '2004-02-29', 5, 'year', 'before');
test('1998-02-28', '2004-02-29', 6, 'year', 'before');

echo "---------------------\n";

test('2014-10-03', '2014-08-31', 1.1, 'month');
test('2014-11-03', '2014-08-31', 2.1, 'month');
test('2014-12-03', '2014-08-31', 3.1, 'month');
test('2015-01-03', '2014-08-31', 4.1, 'month');
test('2015-02-03', '2014-08-31', 5.1, 'month');
test('2015-03-03', '2014-08-31', 6.1, 'month');

echo "---------------------\n";

test('2014-07-28', '2014-08-31', 1.1, 'month', 'before');
test('2014-06-27', '2014-08-31', 2.1, 'month', 'before');
test('2014-05-28', '2014-08-31', 3.1, 'month', 'before');
test('2014-04-27', '2014-08-31', 4.1, 'month', 'before');
test('2014-03-28', '2014-08-31', 5.1, 'month', 'before');
test('2014-02-25', '2014-08-31', 6.1, 'month', 'before');
test('2014-01-28', '2014-08-31', 7.1, 'month', 'before');
test('2013-12-28', '2014-08-31', 8.1, 'month', 'before');
test('2013-11-27', '2014-08-31', 9.1, 'month', 'before');

*/
