<?php
namespace TxCore;

class Subtotal
{
	private $groupedColumn = null;
	private $function;
	private $aggregateColumn;
	
	public function __construct($groupedColumn, $function, $aggregateColumn = null) 
	{
		$this->groupedColumn = $groupedColumn;
		$this->function = $function;
		$this->aggregateColumn = $aggregateColumn;
	}
	
	/**
	 * @return Column
	 */
	public function getGroupedColumn() 
	{
		return $this->groupedColumn;
	}

	/**
	 * @return string
	 */
	public function getFunction()
	{
		return $this->function;
	}
	
	/**
	 * @return Column
	 */
	public function getAggregateColumn()
	{
		return $this->aggregateColumn;
	}
}
