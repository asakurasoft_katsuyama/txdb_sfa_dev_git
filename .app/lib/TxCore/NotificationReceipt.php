<?php

namespace TxCore;

class NotificationReceipt
{
    private $notificationId;
    private $recipientId;
    private $isRead;

    private $notification;
    private $recipient;

    public function _getNotificationId()
    {
        return $this->notificationId;
    }
    public function _setNotificationId($value)
    {
        $this->notificationId = Util::toInt($value);
    }
    public function getNotification()
    {
        if ($this->notificationId !== null && $this->notification === null) {
            $this->notification = Engine::factory()->getNotification($this->notificationId, true);
        }
        return $this->notification;
    }

    public function _getRecipientId()
    {
        return $this->recipientId;
    }
    public function _setRecipientId($value)
    {
        $this->recipientId = Util::toInt($value);
    }
    public function getRecipient()
    {
        if ($this->recipientId !== null && $this->recipient === null) {
            $this->recipient = Engine::factory()->getUser($this->recipientId, false, true);
        }
        return $this->recipient;
    }

    public function isRead()
    {
        return $this->isRead;
    }
    public function setRead($value)
    {
        $this->isRead = $value;
    }

	public function save()
	{
		Engine::factory()->saveNotificationReceipt($this);
	}

}
