<?php
namespace TxCore;

/**
 *
 */
class Option
{
	private $id;
	private $name;
	private $seq;
// 	private $tableId;
// 	private $columnId;
	private $workOptionId;
	private $deleted;
	private $parent;
// 	private $revision;

	public function __construct($id = null)
	{
		if ($id !== null) {
			$this->id = Util::toInt($id);
		} else {
			$this->id = Util::toInt(Engine::factory()->generateObjectId());
		}
	}
	public function _setId($value) 
	{
		$this->id = Util::toInt($value);
	}
	public function getId()
	{
		return $this->id;
	}
	public function getName() 
	{
		return $this->name;
	}
	public function setName($value) 
	{
		$this->name = $value;
	}
	
	public function getParent()
	{
		return $this->parent;
	}
	public function _setParent(Column $column)
	{
		$this->parent = $column;
	}
// 	public function _setRevision($value)
// 	{
// 		$this->revision = Util::toInt($value);
// 	}
// 	public function getRevision()
// 	{
// 		return $this->revision;
// 	}
	public function getSeq()
	{
		return $this->seq;
	}
	public function setSeq($value)
	{
		$this->seq = $value;
	}
// 	public function _getTableId()
// 	{
// 		return $this->tableId;
// 	}
// 	public function _setTableId($value)
// 	{
// 		$this->tableId = $value;
// 	}
// 	public function _getColumnId()
// 	{
// 		return $this->tableId;
// 	}
// 	public function _setTableId($value)
// 	{
// 		$this->tableId = $value;
// 	}

	public function _isDeleted()
	{
		return $this->deleted;
	}
	
	public function _setDeleted($value)
	{
		$this->deleted = $value;
	}
	
}

