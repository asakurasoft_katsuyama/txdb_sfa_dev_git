<?php

namespace TxCore;

use \ArrayAccess;
use \ArrayIterator;
use \IteratorAggregate;

use TxCore\Engine;

/**
 * Row in table.
 */
class Row 
{
	const ACTION_INSERT = 'insert';
	const ACTION_UPDATE = 'update';
	const ACTION_DELETE = 'delete';

	public $id;
	public $version;
	public $rowNo;
	public $table = null;
	public $values = array();
	public $removedValues = array();
	public $action = null; // default is null
	
	public function __construct(Table $parent = null)
	{
		if ($parent !== null) {
			$this->table = $parent;
			$this->applyDefaultValue();
			$parent->_addPenddingRow($this);
		}
	}

	public function getId()
	{
		return $this->id;
	}

    /**
     * @param Column $column
     * @return mixed
     */
	public function getValue(Column $column)
	{
		return isset($this->values[$column->getId()]) ? 
			$this->values[$column->getId()] : null;
	}
	
	/**
	 * @param Column $column
	 * @return string|array
	 */
	public function getText(Column $column)
	{
		return $column->handleGetText($this->getValue($column));
	}
	/**
	 * @param Column $column
	 * @return string|array
	 */
	public function getHtml(Column $column)
	{
		return $column->handleGetHtml($this->getValue($column));
	}
	/**
	 * @param Column $column
	 * @return \TxCore\List_
	 */
	public function serachReferal(Column $column, User $by, Criteria $critera = null, $offset = 0)
	{
		return $column->handleSearchReferal($this, $by, $critera, $offset);
	}
// 	/**
// 	 * @param Column $column
// 	 * @return TxCore\Row
// 	 */
// 	public function serachLookup(Column $column, User $by)
// 	{
// 		return $column->handleLookup($this, $by);
// 	}
	/**
	 * @param Column $column
	 * @param mixed $value
	 */
	public function setValue(Column $column, $value)
	{
		$this->values[$column->getId()] = $column->handleSetValue($value, $this);
	}
	public function _createStoreValue(Column $column)
	{
		// Note : 値が設定されなくても登録できるようにNull
		$value = isset($this->values[$column->getId()]) ? $this->values[$column->getId()] : null;
		return $column->handleStoreValue($value, $this);
	}
	public function _createIndexKey(Column $column)
	{
// var_dump($column->getId());
		$value = isset($this->values[$column->getId()]) ? $this->values[$column->getId()] : null;	// setValueしていない場合は、Null
		return $column->handleIndexKey($value);
	}
	public function _setRawValue(Column $column, $rawValue)
	{
		$this->values[$column->getId()] = $column->handleGetValue($rawValue);
	}
	public function _setParent(Table $value = null)
	{
		$this->table = $value;
	}
	public function delete(User $by) 
	{
		Engine::factory()->deleteRow($this, $by);
	}
	public function update(User $by, $notify = false) 
	{
		Engine::factory()->updateRow($this, $by, $notify);
	}
	public function setVersion($value)
	{
		$this->version = Util::toInt($value);
	}
	public function getVersion()
	{
		return $this->version;
	}
	public function setRowNo($value)
	{
		$this->rowNo = Util::toInt($value);
	}
	public function getRowNo()
	{
		return $this->rowNo;
	}
	/**
	 * @return \TxCore\Table
	 */
	public function getParent()
	{
		return $this->table;
	}
	public function calculate()
	{
		foreach ($this->getParent()->getColumns() as $column)	/* @var $column Column */
			if ($column->isCalculable())
				$column->handleCalculate($this);
	}

	public function setAction($value)
	{
		$this->action = $value;
	}

	public function getAction()
	{
		return $this->action;
	}

	/**
	 * @return Message[] Error messages.
	 */
	public function validate()
	{
		$message = array();
		$columns = $this->getParent()->getColumns();
		foreach ($columns as $column) {	/* @var $column Column */
			$message = array_merge($message, $column->handleValidate($this)); 
		}
		return $message;
	}
	
	public function _setId($value)
	{
		$this->id = Util::toInt($value);
	}
	
	private function applyDefaultValue()
	{
		foreach ($this->table->getColumns() as $column) {
			$column->handleDefaultValue($this);
		}
	}

	/**
	 * @return User
	 */
	public function getCreatedBy() 
	{
		return $this->getTypeValue(Column::CREATED_BY);
	}
	
	public function getCreatedOn()
	{
		return $this->getTypeValue(Column::CREATED_ON);
	}

	public function getUpdatedBy()
	{
		return $this->getTypeValue(Column::UPDATED_BY);
	}
	
	public function getUpdatedOn()
	{
		return $this->getTypeValue(Column::UPDATED_ON);
	}
	
	public function getRownum()
	{
		return $this->getTypeValue(Column::ROWNUM);
	}
	
	private function getTypeValue($type)
	{
		foreach ($this->getParent()->getColumns() as $column)
			if ($column->getType() == $type)
				return $this->getValue($column);
		return null;
	}

	/**
	 * @return Comment[]
	 */
	public function getComments()
	{
		return Engine::factory()->searchRowComments($this);
	}

	/**
	 * @param $id
	 * @param bool $silently
	 * @return Comment
	 * @throws TxCoreException
	 */
	public function getCommentFor($id, $silently = false)
	{
		return Engine::factory()->getComment($id, $silently);
	}

	/**
	 * @param Comment $comment
	 * @param User $by
	 */
	public function createComment($comment, User $by)
	{
		Engine::factory()->createRowComment($this, $comment, $by);
	}

	/**
	 * @param Comment $comment
	 * @param User $by
	 */
	public function deleteComment(Comment $comment, User $by)
	{
		Engine::factory()->deleteComment($comment, $by);
	}

	/**
	 * @return int Count of comments.
	 */
	public function getCommentsCount()
	{
		return Engine::factory()->getRowCommentsCount($this);
	}

    /**
     * @param Column $column
     * @param File $file
     */
	public function removeFile($column, $file)
	{
		if (!isset($this->removedValues[$column->getId()])) {
			$this->removedValues[$column->getId()] = array();
		}
		$this->removedValues[$column->getId()][] = $file;
	}

    /**
     * @param Column $column
     * @return null
     */
	public function getRemovedValue($column)
	{
		$value = null;
		if (isset($this->removedValues[$column->getId()])) {
			$value = $this->removedValues[$column->getId()];
		}
		return $value;
	}

	public function compareToSort(Row $row2, $sorts = array())
	{
		foreach ($sorts as $sort) {
			/* @var $sort \TxCore\Sort */
			$reverse = $sort->isReverse();
			$column = $sort->getColumn();
			$ret = $column->handleCompare($this, $row2);
			if ($ret > 0) {
				return $reverse ? -1 : 1;
			} else if ($ret < 0) {
				return $reverse ?  1 :-1;
			}
		}
		return 0;
	}

    /**
     * @return int
     */
    public function getReadUsersCount()
    {
        return Engine::factory()->getRowReadLogCount($this);
    }

    /**
     * @param int $offset
     * @param int $limit
     * @return List_|ReadLog[]
     */
    public function getReadList($offset = 0, $limit = 20)
    {
        return Engine::factory()->getRowReadList($this, $offset, $limit);
    }

    /**
     * @param User $user
     * @return ReadLog
     */
    public function getReadState(User $user)
    {
        return Engine::factory()->getReadState($this, $user);
    }

    /**
     * @param User $by
     * @param $comment
     */
    public function makeRead(User $by, $comment = null)
    {
        Engine::factory()->makeRowRead($this, $by, $comment);
    }

    /**
     * @param User $by
     * @return bool
     */
    public function hasRead(User $by)
    {
        return Engine::factory()->hasUserReadRow($this, $by);
    }
}
