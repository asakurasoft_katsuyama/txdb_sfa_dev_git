<?php
namespace TxCore;

class Message
{
	/**
	 * Error context
	 * @var mixed
	 */
	private $context;
	
	/**
	 * Message.
	 * @var string
	 */
	private $message;
	
	public function __construct($message, $args = array(), $context = array()) 
	{
		$message_ = null;
		if (str_starts_with(':', $message)) {
			$message_ = get_message(substr($message, 1), array_merge($args, array('', '', '', '')));
		} else if (is_not_empty($args)) {
			$message_ = sprintf($message, $args);
		} else {
			$message_ = $message; 
		}
		
		$this->context = $context;
		$this->message = $message_;
	}
	
	/**
	 * @return mixed
	 */
	public function getContext() 
	{
		return $this->context;
	}
	
	public function setContext($context) 
	{
		$this->context = $context;
	}
	
	public function getMessage()
	{
		return $this->message;
	}
	
	public function setMessage($message)
	{
		$this->message = $message;
	}
}

