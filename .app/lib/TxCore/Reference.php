<?php
namespace TxCore;

/**
 *
 */
class Reference
{
	private $tableId;
	private $revision;
	private $columnId;
	private $no;
	private $targetTableId;
	private $targetColumnId;
	
	/**
	 * @param Column[] $columns
	 * $columns[0] reference target
	 * $columns[1] reference source
	 */
	public function __construct($columns = null)
	{
		if (is_array($columns) && sizeof($columns) == 2) {
			$this->setColumnId($columns[1]->getId());
			$this->setRevision($columns[1]->getParent()->getRevision());
			$this->setTableId($columns[1]->getParent()->getId());
			$this->setTargetColumnId($columns[0]->getId());
			$this->setTargetTableId($columns[0]->getParent()->getId());
		}
	}
	
	public function getTableId()
	{
		return $this->tableId;
	}
	public function setTableId($value)
	{
		$this->tableId = $value;
	}
	
	public function getRevision()
	{
		return $this->revision;
	}
	public function setRevision($value)
	{
		$this->revision = $value;
	}
	
	public function getColumnId()
	{
		return $this->columnId;
	}
	public function setColumnId($value)
	{
		$this->columnId = $value;
	}
	
	public function getTargetTableId()
	{
		return $this->targetTableId;
	}
	public function setTargetTableId($value)
	{
		$this->targetTableId = $value;
	}
	
	public function getTargetColumnId()
	{
		return $this->targetColumnId;
	}
	public function setTargetColumnId($value)
	{
		$this->targetColumnId = $value;
	}
	
}