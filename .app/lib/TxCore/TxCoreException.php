<?php
namespace TxCore;

use \Exception;

class TxCoreException extends Exception
{
	const AUTHENTICATION_FAILED = 100001;	// 認証エラー
	const ROW_NOT_FOUND = 100002;	// 更新または取得対象が０件
	const LOGIN_ID_WAS_DUPLICATED = 100003;	// ログインIDが重複
	const GROUP_NAME_WAS_DUPLICATED = 100004;
// 	const LEXER_PARSE_ERROR = 100004;
	const ROW_HAS_UPADTED = 100005;	// 更新時に既に誰かが更新している場合
	const CIRCULAR_REFERENCE = 100006;
	const VALIDATION_FAILED = 100007;	// 検証エラー
	const PERMISSION_DENIED = 100008;	// 権限なし
	const INVALID_FORMULA = 100009;	// 計算式エラー
	const REFERENCE_VIOLATION = 100010;	// 参照違反
	const TABLE_HAS_UPADTED = 100011;	// 更新時に既に誰かが更新している場合
	const LOCK_FAILED = 100012;	// ロック失敗
	
	const INTERNAL = 999999;	// 内部エラー
	
	/**
	 * @var array()
	 */
	private $context;
	 
	public function __construct($message= null, $code= self::INTERNAL, $context = array())
	{
		parent::__construct($message, $code);
		$this->context = $context;
	}
	
	public function getContext() 
	{
		return $this->context;	
	}
}
