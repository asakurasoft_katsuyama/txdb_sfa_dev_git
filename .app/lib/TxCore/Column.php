<?php
namespace TxCore;

use TxCore\Math\MathContext;

use TxCore\Math\Math;

use TxCore\Math\RPN;

use Image\Resizer;

use TxCore\DateTimeCalc;

/**
 * Column defintion.
 */
abstract class Column
{
	const TEXT = 'text';
	const MULTITEXT = 'multitext';
	const RICHTEXT = 'richtext';
	const NUMBER = 'number';
	const CALC = 'calc';
	const RADIO = 'radio';
	const CHECKBOX = 'checkbox';
	const DROPDOWN = 'dropdown';
	const DATE = 'date';
	const DATETIME = 'datetime';
	const TIME = 'time';
	const DATECALC ='datecalc';
	const FILE = 'file';
	const LINK = 'link';
	const RELATIONAL = 'relational';
	const RELATIONAL_INPUT = 'relational_input';
	const LOOKUP = 'lookup';
	const ROWNUM = 'rownum';
	const CREATED_BY = 'created_by';
	const CREATED_ON = 'created_on';
	const UPDATED_BY = 'updated_by';
	const UPDATED_ON = 'updated_on';
	const LABEL = 'label';
	const SPACE = 'space';
	const LINE = 'line';
	const USER_SELECTOR = 'user_selector';
	const TAB_GROUP = 'tab_group';
	const TAB = 'tab';

	protected $id;
	protected $columnType;
	protected $label;
	protected $code;

	/**
	 * @var Table
	 */
	protected $parent;

	protected $properties_ = array();

	/**
	 * @var int
	 */
	protected $revision;

	protected $options = null;
	protected $removedOptions = array();
	protected $optionsCache = null;

	/**
	 * Cache for reference table.
	 * @var Table
	 */
	private $referenceTable;

	protected static $properties = array(
		self::TEXT =>       array('name' => '文字列（一行）',            'inputable' => true,   'operand' => array(self::TEXT => true),                                                 'indexType' => Index::STRING,   'searchable' => array(Criteria::EQUALS, Criteria::NOT_EQUALS, Criteria::LIKE, Criteria::NOT_LIKE),                                                     'lookup' => array('referable' => true,  'copyAcceptable' => true,   'copyAcceptees' => array(self::TEXT, self::RADIO, self::DROPDOWN, self::LOOKUP)),      'relational' => array('referable' => true, 'referrerKeys' => array(self::TEXT)), 'notification' => array('listable' => true)),
		self::MULTITEXT =>  array('name' => '文字列（複数行）',  'inputable' => true,                                                                                           'indexType' => Index::STRING,   'searchable' => array(Criteria::LIKE, Criteria::NOT_LIKE),                                                                                             'lookup' => array('referable' => false, 'copyAcceptable' => true,   'copyAcceptees' => array(self::TEXT, self::MULTITEXT, self::LOOKUP))),
		self::RICHTEXT =>   array('name' => '装飾テキスト',    'inputable' => true,                                                                                           'indexType' => Index::STRING,   'searchable' => array(Criteria::LIKE, Criteria::NOT_LIKE),                                                                                             'lookup' => array('referable' => false, 'copyAcceptable' => true,   'copyAcceptees' => array(self::TEXT, self::MULTITEXT, self::RICHTEXT, self::LOOKUP))),
		self::NUMBER =>     array('name' => '数値',              'inputable' => true,   'operand' => array(self::CALC => true, self::TEXT => true, self::DATECALC => true),     'indexType' => Index::NUMBER,   'searchable' => array(Criteria::EQUALS, Criteria::NOT_EQUALS,Criteria::GREATER_EQUALS, Criteria::LESS_EQUALS),                                         'lookup' => array('referable' => true,  'copyAcceptable' => true,   'copyAcceptees' => array(self::NUMBER, self::CALC, self::LOOKUP, self::ROWNUM)),       'relational' => array('referable' => true, 'referrerKeys' => array(self::NUMBER, self::CALC, self::ROWNUM))),
		self::CALC =>       array('name' => '計算',              'inputable' => false,  'operand' => array(self::CALC => true, self::TEXT => true, self::DATECALC => true),     'indexType' => Index::NUMBER,   'searchable' => array(Criteria::EQUALS, Criteria::NOT_EQUALS, Criteria::GREATER_EQUALS, Criteria::LESS_EQUALS),                                        'lookup' => array('referable' => true,  'copyAcceptable' => false,  'copyAcceptees' => array()),                                                           'relational' => array('referable' => true, 'referrerKeys' => array(self::NUMBER, self::CALC, self::ROWNUM))),
		self::RADIO =>      array('name' => '選択肢',      'inputable' => true,                                                                                           'indexType' => Index::INTEGER,  'searchable' => array(Criteria::CONTAINS, Criteria::NOT_CONTAINS),                                                                                     'lookup' => array('referable' => false, 'copyAcceptable' => true,   'copyAcceptees' => array(self::RADIO))),
		self::CHECKBOX =>   array('name' => '選択肢（複数）',  'inputable' => true,                                                                                           'indexType' => Index::INTEGER,  'searchable' => array(Criteria::CONTAINS, Criteria::NOT_CONTAINS)),
		self::DROPDOWN =>   array('name' => 'プルダウン選択肢',    'inputable' => true,                                                                                           'indexType' => Index::INTEGER,  'searchable' => array(Criteria::CONTAINS, Criteria::NOT_CONTAINS),                                                                                     'lookup' => array('referable' => false, 'copyAcceptable' => true,   'copyAcceptees' => array(self::DROPDOWN))),
		self::USER_SELECTOR =>  array('name' => 'ユーザー選択',   'inputable' => true,                                                                                           'indexType' => Index::INTEGER,  'searchable' => array(Criteria::CONTAINS, Criteria::NOT_CONTAINS), 'notification' => array('targettable' => true)),
		self::DATE =>       array('name' => '日付',              'inputable' => true,   'operand' => array(self::CALC => true, self::TEXT => true, self::DATECALC => true),     'indexType' => Index::DATE,     'searchable' => array(Criteria::EQUALS, Criteria::NOT_EQUALS, Criteria::LESS, Criteria::LESS_EQUALS, Criteria::GREATER, Criteria::GREATER_EQUALS),     'lookup' => array('referable' => false, 'copyAcceptable' => true,   'copyAcceptees' => array(self::DATE)), 'notification' => array('listable' => true)),
		self::TIME =>       array('name' => '時刻',              'inputable' => true,   'operand' => array(self::CALC => true, self::TEXT => true, self::DATECALC => true),     'indexType' => Index::TIME,     'searchable' => array(Criteria::EQUALS, Criteria::NOT_EQUALS, Criteria::LESS, Criteria::LESS_EQUALS, Criteria::GREATER, Criteria::GREATER_EQUALS),     'lookup' => array('referable' => false, 'copyAcceptable' => true,   'copyAcceptees' => array(self::TIME))),
		self::DATETIME =>   array('name' => '日時',              'inputable' => true,   'operand' => array(self::CALC => true, self::TEXT => true, self::DATECALC => true),     'indexType' => Index::DATETIME, 'searchable' => array(Criteria::EQUALS, Criteria::NOT_EQUALS, Criteria::LESS, Criteria::LESS_EQUALS, Criteria::GREATER, Criteria::GREATER_EQUALS),     'lookup' => array('referable' => false, 'copyAcceptable' => true,   'copyAcceptees' => array(self::DATETIME, self::CREATED_ON, self::UPDATED_ON))),
		self::DATECALC =>   array('name' => '日時計算',          'inputable' => false,  'operand' => array(self::CALC => true),                                                 'indexType' => Index::NUMBER,   'searchable' => array(Criteria::EQUALS, Criteria::NOT_EQUALS, Criteria::LESS, Criteria::LESS_EQUALS, Criteria::GREATER, Criteria::GREATER_EQUALS),     'lookup' => array('referable' => false, 'copyAcceptable' => false,  'copyAcceptees' => array())),
		self::FILE =>       array('name' => '添付ファイル',      'inputable' => true,                                                                                           'indexType' => Index::STRING,   'searchable' => array(Criteria::LIKE, Criteria::NOT_LIKE)),
		self::LINK =>       array('name' => 'リンク',            'inputable' => true,                                                                                           'indexType' => Index::STRING,   'searchable' => array(Criteria::EQUALS, Criteria::NOT_EQUALS, Criteria::LIKE, Criteria::NOT_LIKE),                                                     'lookup' => array('referable' => true,  'copyAcceptable' => true,   'copyAcceptees' => array(self::LINK, self::LOOKUP)),                                   'relational' => array('referable' => true, 'referrerKeys' => array(self::TEXT, self::LINK))),
		self::RELATIONAL => array('name' => '関係データ一覧',  'inputable' => false,                                                                                          'indexType' => null),
		self::RELATIONAL_INPUT => array('name' => '明細入力',  'inputable' => true,                                                                                          'indexType' => null),
		self::LOOKUP =>     array('name' => '参照コピー',      'inputable' => true,                                                                                           'indexType' => 'auto',                                                                                                                                                                 'lookup' => array('referable' => true,  'copyAcceptable' => false,  'copyAcceptees' => array()),                                                           'relational' => array('referable' => true, 'referrerKeys' => array(self::TEXT, self::NUMBER, self::CALC, self::LINK, self::ROWNUM)), 'notification' => array('listable' => true)),
		self::ROWNUM =>     array('name' => 'データ番号',      'inputable' => false,                                                                                          'indexType' => Index::INTEGER,  'searchable' => array(Criteria::EQUALS, Criteria::NOT_EQUALS, Criteria::GREATER_EQUALS, Criteria::LESS_EQUALS),                                        'lookup' => array('referable' => true,  'copyAcceptable' => false,  'copyAcceptees' => array()),                                                           'relational' => array('referable' => true, 'referrerKeys' => array(self::NUMBER, self::CALC, self::ROWNUM))),
		self::CREATED_BY => array('name' => '作成者',            'inputable' => false,  'operand' => array(self::TEXT => true),                                                 'indexType' => Index::INTEGER,  'searchable' => array(Criteria::CONTAINS, Criteria::NOT_CONTAINS), 'notification' => array('targettable' => true)),
		self::CREATED_ON => array('name' => '作成日時',          'inputable' => false,  'operand' => array(self::CALC => true, self::TEXT => true, self::DATECALC => true),     'indexType' => Index::DATETIME, 'searchable' => array(Criteria::EQUALS, Criteria::NOT_EQUALS, Criteria::LESS, Criteria::LESS_EQUALS, Criteria::GREATER, Criteria::GREATER_EQUALS)),
		self::UPDATED_BY => array('name' => '更新者',            'inputable' => false,  'operand' => array(self::TEXT => true),                                                 'indexType' => Index::INTEGER,  'searchable' => array(Criteria::CONTAINS, Criteria::NOT_CONTAINS)),
		self::UPDATED_ON => array('name' => '更新日時',          'inputable' => false,  'operand' => array(self::CALC => true, self::TEXT => true, self::DATECALC => true),     'indexType' => Index::DATETIME, 'searchable' => array(Criteria::EQUALS, Criteria::NOT_EQUALS, Criteria::LESS, Criteria::LESS_EQUALS, Criteria::GREATER, Criteria::GREATER_EQUALS)),
		self::LABEL =>      array('name' => '注意書き',            'inputable' => false,                                                                                          'indexType' => null),
		self::SPACE =>      array('name' => '空白',          'inputable' => false,                                                                                          'indexType' => null),
		self::LINE =>       array('name' => '罫線',              'inputable' => false,                                                                                          'indexType' => null),
		self::TAB_GROUP =>      array('name' => 'タブグループ',          'inputable' => false,                                                                                          'indexType' => null),
		self::TAB =>      array('name' => 'タブ',          'inputable' => false,                                                                                          'indexType' => null),
	);
	// '
	/**
	 * @param $columnType
	 * @param \TxCore\Table $table The parent table.
	 * @return \TxCore\Column
	 */
	public static function &factory($columnType)
	{
		$className = 'TxCore\\Column__'.str_camelize($columnType, UPPER_CASE);
		$column = new $className();
		$column->_setType($columnType);
		return $column;
	}

	/**
	 * Create new column.
	 * @param $columnType
	 * @param \TxCore\Table $table The parent table to add columns.
	 * @return \TxCore\Column
	 */
	public static function &create($columnType, Table &$parent)
	{
		$className = 'TxCore\\Column__'.str_camelize($columnType, UPPER_CASE);
		$column = new $className();
		$column->_setId(Engine::factory()->generateObjectId());
		$column->_setType($columnType);
		$column->_setParent($parent);
		$parent->_addColumn($column);
		return $column;
	}

	protected function __construct()
	{
		$this->properties_ = array_map(
					function($prop) { return $prop[1]; }, $this->getPropertyDescriptor());
	}

//	 /**
//	  * @param $columnType
//	  * @param $id
//	  * @return TxCore\Column
//	  */
//	 public static function &_factory($columnType, $id)
//	 {
//		 $className = 'TxCore\\'.str_camelize($columnType, UPPER_CASE).'Column';
//		 $column = new $className(null, $id);
//		 return $column;
//	 }

//	 private function __construct($parent = null, $id = null)
//	 {
//		 if ($id !== null) {
//			 $this->_setId($id);
//		 } else {
//			 $this->_setId(Engine::factory()->generateObjectId());
//		 }
//		 if ($parent !== null) {
//			 $this->_setParent($parent);
//			 $parent->_addColumn($this);
//		 }
//		 // Sets initial properties.
//		 $this->properties_ = array_map(
//			 function($prop) { return $prop[1]; }, $this->getPropertyDescriptor());
//	 }

	public function _setId($value)
	{
		$this->id = Util::toInt($value);
	}

	public function _setParent(Table $table)
	{
		$this->parent = $table;
	}
	public function setProperty($name, $value)
	{
		if (!$this->hasProperty($name)) {
			throw new TxCoreException(
				sprintf('Column %s dose not support "%s" property', $this->getType(), $name),
				TxCoreException::INTERNAL);
		}
		$desc = $this->getPropertyDescriptor();
		if ($value !== null && $desc[$name][0] !== gettype($value)) {
			throw new TxCoreException(
				sprintf('Property "%s" value shoud be %s, but %s was specified (%s)', $name, $desc[$name][0], gettype($value), $this->getType()),
				TxCoreException::INTERNAL);
		}
		if ($value === null) {
			$value = $desc[$name][1];
		}
		$this->properties_[$name] = $value;
//		 switch (gettype($this->properties_[$name])) {
//		 case 'string':
//		 case 'NULL':
//			 $this->properties_[$name] = $value != null ? strval($value) : null;
//			 break;
//		 case 'integer':
//			 $this->properties_[$name] = Util::toInt($value, 0);
//			 break;
//		 case 'boolean':
//			 $this->properties_[$name] = Util::toBool($value, false);
//			 break;
//		 case 'array':
//			 if (!is_array($value)) {
//				 throw new TxCoreException(
//					 sprintf('"%s" property shoud be array', $name),
//					 TxCoreException::INTERNAL);
//			 }
//			 $this->properties_[$name] = $value;
//			 break;
//		 default:
//			 throw new TxCoreException(
//				 sprintf('Unknown property type : %s.', gettype($name)),
//				 TxCoreException::INTERNAL);
//		 }
	}
	public function getProperty($name)
	{
		if (error_reporting() !== 0 && !$this->hasProperty($name)) {
			throw new TxCoreException(
				sprintf('Column %s dose not support "%s" property', $this->getType(), $name),
				TxCoreException::INTERNAL);
		}
		return $this->properties_[$name];
	}
	/**
	 * @return \TxCore\Table
	 */
	public function getParent()
	{
		return $this->parent;
	}
	public function getId()
	{
		return $this->id;
	}
	public function getTypeName()
	{
		return self::$properties[$this->columnType]['name'];
	}
	public function setLabel($value)
	{
		$this->label = $value;
	}
	public function getCode()
	{
		return $this->code;
	}
	public function setCode($value)
	{
		$this->code = $value;
	}
	public function getDefaultValue()
	{
		return $this->getProperty('defaultValue');
	}
	public function setDefaultValue($value)
	{
		$this->setProperty('defaultValue', $value);
	}
	public function getLabel()
	{
		return $this->label;
	}
	public function setPositionX($value)
	{
		$this->setProperty('positionX', Util::toInt($value));
	}
	public function setPositionY($value)
	{
		$this->setProperty('positionY', Util::toInt($value));
	}
	public function getPositionX()
	{
		return $this->getProperty('positionX');
	}
	public function getPositionY()
	{
		return $this->getProperty('positionY');
	}
	public function getShowsLabel()
	{
		return $this->getProperty('showsLabel');
	}
	public function setShowsLabel($value)
	{
		$this->setProperty('showsLabel', Util::toBool($value));
	}
	public function getSizeHeight()
	{
		return $this->getProperty('sizeHeight');
	}
	public function setSizeHeight($value)
	{
		$this->setProperty('sizeHeight', Util::toInt($value));
	}
	public function getSizeWidth()
	{
		return $this->getProperty('sizeWidth');
	}
	public function setSizeWidth($value)
	{
		$this->setProperty('sizeWidth', Util::toInt($value));
	}
	public function isUnique()
	{
		return $this->getProperty('isUnique');
	}
	public function setUnique($value)
	{
//		 $this->isUnique = $value;
		$this->setProperty('isUnique', Util::toBool($value));
	}
	public function isRequired()
	{
		return $this->getProperty('isRequired');
	}
	public function setRequired($value)
	{
		$this->setProperty('isRequired', Util::toBool($value));
	}
	public function getShowsField()
	{
		return $this->getProperty('showsField');
	}
	public function setShowsField($value)
	{
		$this->setProperty('showsField', Util::toBool($value));
	}
	public function getMinLength()
	{
		return $this->getProperty('minLength');
	}
	public function setMinLength($value)
	{
		$this->setProperty('minLength', Util::toInt($value));
	}
	public function getMaxLength()
	{
		return $this->getProperty('maxLength');
	}
	public function setMaxLength($value)
	{
		$this->setProperty('maxLength', Util::toInt($value));
	}
	public function getMinValue()
	{
		return $this->getProperty('minValue');
	}
	public function setMinValue($value)
	{
		$this->setProperty('minValue', $value);
	}
	public function getMaxValue()
	{
		return $this->getProperty('maxValue');
	}
	public function setMaxValue($value)
	{
		$this->setProperty('maxValue', $value);
	}
	public function getExpression()
	{
		return $this->getProperty('expression');
	}
	public function setExpression($value)
	{
		$this->setProperty('expression', $value);
	}
	public function getFormat()
	{
		return $this->getProperty('format');
	}
	public function setFormat($value)
	{
		$this->setProperty('format', $value);
	}
	public function getDateFormat()
	{
		return $this->getProperty('dateFormat');
	}
	public function setDateFormat($value)
	{
		$this->setProperty('dateFormat', $value);
	}
	public function getShowsDigit()
	{
		return $this->getProperty('showsDigit');
	}
	public function setShowsDigit($value)
	{
		$this->setProperty('showsDigit', $value);
	}
//	 public function getDefaultExpression()
//	 {
//		 return $this->getProperty('defaultExpression');
//	 }
//	 public function setDefaultExpression($value)
//	 {
//		 $this->setProperty('defaultExpression', $value);
//	 }
	public function setLabelHtml($value)
	{
		$this->setProperty('labelHtml', $value);
	}
	public function getLabelHtml()
	{
		return $this->getProperty('labelHtml');
	}
	public function setThumbnailMaxSize($value)
	{
		$this->setProperty('thumbnailMaxSize', Util::toInt($value));
	}
	public function getThumbnailMaxSize()
	{
		return $this->getProperty('thumbnailMaxSize');
	}
	public function setImageMaxSize($value)
	{
		$this->setProperty('imageMaxSize', Util::toInt($value));
	}
	public function getImageMaxSize()
	{
		return $this->getProperty('imageMaxSize');
	}
	public function setLinkType($value)
	{
		$this->setProperty('linkType', $value);
	}
	public function getLinkType()
	{
		return $this->getProperty('linkType');
	}
	public function setOptionsAlignment($value)
	{
		$this->setProperty('optionsAlignment', $value);
	}
	/**
	 * @return \TxCore\Table
	 */
	public function getReferenceTable()
	{
		if ($this->referenceTable === null && $this->getProperty('referenceTableId') !== null) {
			$this->referenceTable = Engine::factory()->getTable($this->getProperty('referenceTableId'));
		}
		return $this->referenceTable;
	}
	public function getReferenceTargetColumn()
	{
		$id = $this->getProperty('referenceTargetColumnId');
		$refTable = $this->getReferenceTable();
		return $id && $refTable ? $refTable->getColumnFor($id) : null;
	}

	public function getReferenceTargetColumnRecursively()
	{
		$target = $this->getReferenceTargetColumn();
		if ($target === null) {
			return null;
		} else {
			return $target->_getReferenceTargetColumnRecursively();
		}
	}

	public function _getReferenceTargetColumnRecursively()
	{
		if ($this instanceof Column__Lookup) {
			$target = $this->getReferenceTargetColumn();
			if ($target === null) {
				throw new TxCoreException("Lookup has not reference target column.",
						TxCoreException::CIRCULAR_REFERENCE);
			}
			return $target->_getReferenceTargetColumnRecursively();
		} else {
			return $this;
		}
	}


	public function getReferenceColumns()
	{
		$ids = $this->getProperty('referenceColumnIds');
		$refTable = $this->getReferenceTable();
		return array_map(function($id) use ($refTable) {
			return $refTable ? $refTable->getColumnFor($id): null;
		}, $ids);
	}

    /**
     * @return Column[][]
     * @throws TxCoreException
     */
	public function getReferenceCopyMap()
	{
		$idMap = $this->getProperty('referenceCopyMapIds');
		$refTable = $this->getReferenceTable();
		$selfTable = $this->getParent();
		return array_map(function($ids) use ($refTable, $selfTable) {
			return array( $refTable ? $refTable->getColumnFor($ids[0]) : null, $selfTable->getColumnFor($ids[1]));
		}, $idMap);
	}
	public function getReferenceSortColumn()
	{
		$id = $this->getProperty('referenceSortColumnId');
		return $id && $this->getReferenceTable() ? $this->getReferenceTable()->getColumnFor($id) : null;
	}
	public function setReferenceTable(Table $value = null)
	{
		$this->referenceTable = $value;
		$this->setProperty('referenceTableId', $value ? $value->getId() : null);
	}
	public function setReferenceTargetColumn(Column $column = null)
	{
		$this->setProperty('referenceTargetColumnId', $column ? $column->getId() : null);
	}
	public function getReferenceSourceColumn()
	{
		$id = $this->getProperty('referenceSourceColumnId');
		if ($this->getParent() === null) {
			throw new TxCoreException(null, TxCoreException::INTERNAL);
		}
		return $id ? $this->getParent()->getColumnFor($id) : null;
	}
	public function setReferenceSourceColumn(Column $value = null)
	{
		$this->setProperty('referenceSourceColumnId', $value ? $value->getId() : null);
	}
	public function setReferenceColumns(array $columns)
	{
		$this->setProperty('referenceColumnIds', array_map(function($t) {
			return $t->getId();
		}, $columns));
	}
	public function setReferenceCopyMap(array $map)
	{
		$ids = array_map(function($t) {
			if (!is_array($t) || count($t) !== 2 || !($t[0] instanceof Column)) {
				throw new TxCoreException(
					sprintf('Value of property referenceCopyMap should be array of array'),
					TxCoreException::INTERNAL);
			}
			return array($t[0]->getId(), $t[1]->getId());
		}, $map);
		$this->setProperty('referenceCopyMapIds', $ids);
	}
	public function setReferenceSortColumn(Column $column = null)
	{
		$this->setProperty('referenceSortColumnId', $column ? $column->getId() : null);
	}
	public function getReferenceSortsReverse()
	{
		return $this->getProperty('referenceSortsReverse');
	}
	public function setReferenceSortsReverse($value)
	{
		$this->setProperty('referenceSortsReverse', Util::toBool($value));
	}
	public function getReferenceMaxRow()
	{
		return $this->getProperty('referenceMaxRow');
	}
	public function setReferenceMaxRow($value)
	{
		$this->setProperty('referenceMaxRow', Util::toInt($value));
	}
	public function getReferenceAggregations()
	{
		$idMap = $this->getProperty('referenceAggregations');
		$refTable = $this->getReferenceTable();
		return array_map(function($ids) use ($refTable) {
			return array( $refTable ? $refTable->getColumnFor($ids[0]) : null, $ids[1]);
		}, $idMap);
	}
	public function setReferenceAggregations(array $pair)
	{
		$ids = array_map(function($t) {
			if (!is_array($t) || count($t) !== 2 || !($t[0] instanceof Column)) {
				throw new TxCoreException(
					sprintf('Value of property referenceAggregation should be array of array'),
					TxCoreException::INTERNAL);
			}
			return array($t[0]->getId(), $t[1]);
		}, $pair);
		$this->setProperty('referenceAggregations', $ids);
	}

	public function getDateSourceColumn()
	{
		$id = $this->getProperty('dateSourceColumnId');
		if ($this->getParent() === null) {
			throw new TxCoreException(null, TxCoreException::INTERNAL);
		}
		return $id ? $this->getParent()->getColumnFor($id) : null;
	}

	public function setDateSourceColumn(Column $value = null)
	{
		$this->setProperty('dateSourceColumnId', $value ? $value->getId() : null);
	}

	public function getDatecalcUnit()
	{
		return $this->getProperty('datecalcUnit');
	}
	public function setDatecalcUnit($value)
	{
		$this->setProperty('datecalcUnit', $value);
	}

	public function getDatecalcBeforeAfter()
	{
		return $this->getProperty('datecalcBeforeAfter');
	}
	public function setDatecalcBeforeAfter($value)
	{
		$this->setProperty('datecalcBeforeAfter', $value);
	}

	public function setLayoutParent($value = null)
	{
		$this->layoutParent= $value;
		$this->setProperty('layoutParentId', $value ? $value->getId() : null);
	}
	public function getLayoutParent()
	{
		$id = $this->getProperty('layoutParentId');
		return $id ? $this->getParent()->getColumnFor($id) : null;
	}

	abstract protected function getPropertyDescriptor();

	public function hasProperty($name)
	{
		$desc = $this->getPropertyDescriptor();
		return array_key_exists($name, $desc);
	}

	public function getProperties()
	{
		return $this->properties_;
	}
	public function setRawProperties($value)
	{
		$this->properties_ = ($value == null ? array() : $value) + $this->properties_;
	}
	public function getRawProperties()
	{
		return $this->properties_;
	}
	public function getOptionsAlignment()
	{
//		 return $this->optionsAlignment;
		return $this->getProperty('optionsAlignment');
	}
//	 public function _setTableId($value)
//	 {
//		 $this->tableId = ($value === null ? $value : intval($value));
//	 }
//	 public function getTableId()
//	 {
//		 return $this->tableId;
//	 }
	public function hasValue()
	{
		return $this->getIndexType() !== null;
	}
//	 public function hasDefaultValue()
//	 {
//		 return isset(self::$properties[$this->columnType]['defaultValue']);
//	 }
//	 public function hasCode()
//	 {
//		 static $ignored = array( self::LABEL, self::SPACE, self::LINE );
//			 return !in_array($this->columnType, $ignored, true);
//	 }
	public function _setType($value)
	{
		$this->columnType = $value;
	}
//	 public function getColumnTypeDef()
//	 {
//		 return TxCore_ColumnTypeDefs::valueOf($this->columnType);
//	 }
	public function getType()
	{
		return $this->columnType;
	}

	// -- Property of column type (ixXXable) --------------------------

	public function getRolesInCalendar()
	{
		$roles = array();
		if (in_array($this->getType(), array(self::TEXT, self::LOOKUP, self::RADIO, self::DROPDOWN), true)) {
			$roles[] = 'title';
		}
		if (in_array($this->getType(), array(self::USER_SELECTOR), true)) {
			$roles[] = 'user';
		}
		if (in_array($this->getType(), array(self::TIME), true)) {
			$roles[] = 'time';
		}
		if (in_array($this->getType(), array(self::DATE), true)) {
			$roles[] = 'date';
		}
		return $roles;
	}

	public function isSumable()
	{
		return false;
	}
	public function isListable()
	{
		return !in_array($this->getType(), array(self::RELATIONAL,
			self::RELATIONAL_INPUT,
			self::LABEL, self::SPACE, self::LINE, self::TAB_GROUP, self::TAB), true);
	}

	public function isAutofill()
	{
		return in_array($this->getType(), array(self::ROWNUM,self::CREATED_BY,
			self::CREATED_ON, self::UPDATED_BY, self::UPDATED_ON), true);
	}
	public function isCalculable()
	{
		return false;
	}
	public function canBeOperand($forType = null)
	{
		log_debug('type: '.$this->columnType.' fortype: '.$forType);
		if ($forType === null) {
			return isset(self::$properties[$this->columnType]['operand']);
		}  else {
			return isset(self::$properties[$this->columnType]['operand'][$forType]);
		}
	}
//	 public function canBeOperandFor(Column $column)
//	 {
//		 return isset(self::$properties[$this->columnType]['operand'][$column->getType()]);
//	 }
//	 public function canReferAsOperands()
//	 {
// //		 if ($column takes as operand)
//		 return isset(self::$properties[$this->columnType]['operand']) ?
//			 self::$properties[$this->columnType]['operand'] : array();
//	 }

	public function isSearchable($operator = null)
	{
		if ($operator == null) {
			return count($this->getSearchableOperators()) > 0;
		} else {
			return in_array($operator, $this->getSearchableOperators(), true);
		}
	}

	/**
	 * Returns USER-inputable or not.
	 */
	public function isInputable()
	{
		return self::$properties[$this->columnType]['inputable'];
	}

	public function getSearchableOperators()
	{
		return isset(self::$properties[$this->columnType]['searchable']) ?
			self::$properties[$this->columnType]['searchable'] : array();
	}

	public function hasNotificationConditionalOperator($operator = null)
	{
		if ($operator == null) {
			return count($this->getNotificationConditionalOperators()) > 0;
		} else {
			return in_array($operator, $this->getNotificationConditionalOperators(), true);
		}
	}

	protected function getNotificationConditionalOperators()
	{
		return $this->getSearchableOperators();
	}

	public function getLookupConstraints()
	{
		return isset(self::$properties[$this->columnType]['lookup']) ?
		self::$properties[$this->columnType]['lookup'] : array();
	}

	public function getRelationalConstraints()
	{
		return isset(self::$properties[$this->columnType]['relational']) ?
		self::$properties[$this->columnType]['relational'] : array();
	}

	public function getNotificationConstraints()
	{
		$constraints = isset(self::$properties[$this->columnType]['notification']) ?
		self::$properties[$this->columnType]['notification'] : array();
		$constraints['conditional'] = array();
		$constraints['conditional']['operators'] = $this->getNotificationConditionalOperators();
		return $constraints;
	}

	public function isSortable()
	{
		return in_array($this->getType(), array(
			self::TEXT,
			self::NUMBER,
			self::CALC,
			self::RADIO,
			self::DROPDOWN,
			self::DATE,
			self::TIME,
			self::DATETIME,
			self::DATECALC,
			self::LINK,
			self::LOOKUP,
			self::ROWNUM,
			self::CREATED_BY,
			self::CREATED_ON,
			self::UPDATED_BY,
			self::UPDATED_ON
		), true);
	}


	public function getIndexType()
	{
		return self::$properties[$this->columnType]['indexType'];
	}
//	 public function getDependents()
//	 {
//		 return array();
//	 }
	public function hasOptions()
	{
		return false;
	}
	public function hasReference()
	{
		return false;
	}
	public function hasRelationship()
	{
		return false;
	}
	// override
	public function &getOption($offset)
	{
		$options = $this->getOptions();
		return $options[$offset];
	}
	public function _setOptions($options)
	{
		$this->options = $options;
	}
//	 public function _setRevision($revision)
//	 {
//		 $this->revision = $revision;
//	 }
//	 public function getRevision()
//	 {
//		 return $this->revision;
//	 }

	/**
	 * @param array $filter
	 * @return Option[]
	 * @throws TxCoreException
	 */
	public function &getOptions($filter = array())
	{
		if (!$this->hasOptions()) {
			throw new TxCoreException(
				sprintf('Column %s dose not support options property', $this->getType()),
				TxCoreException::INTERNAL);
		}

		if ($this->options === null) {
			if ($this->id !== null) {
				$this->options = Engine::factory()->getColumnOptions($this);
			} else {
				$this->options = array();
			}
		}

		if (is_not_empty($filter)) {
			$options = array_grep($this->options, function(Option $option) use ($filter) {
				if (isset($filter['name']) && $filter['name'] != $option->getName())
					return false;
				return true;
			});
			return $options;
		}
		return $this->options;
	}
	/**
	 * @return Option
	 */
	public function &addOption()
	{
		$options =& $this->getOptions();
		$option = new Option();
		$option->_setParent($this);
		$options[] = $option;
		return $option;
	}

	/**
	 * @return Option
	 */
	public function removeOption(Option $option)
	{
		$options =& $this->getOptions();
		foreach ($options as $i => $option_) {
			if ($option_->getId() == $option->getId()) {
				$this->removedOptions[] = $option;
				unset($options[$i]);
				return $option;
			}
		}

		throw new TxCoreException(
			sprintf("Removing option dose not found in column %d.", $this->id),
			TxCoreException::INTERNAL);
	}

	/**
	 * @param int $id Option Id
	 * @throws TxCoreException Throws if removed option dose not exist.
	 * @return \TxCore\Option
	 */
	public function getRemovedOptionFor($id, $silently = false)
	{
		$options = $this->_getRemovedOptions();
		foreach ($options as $option) {
			if ($option->getId() === Util::toInt($id))
				return $option;
		}
		if ($silently) {
			return null;
		}
		throw new TxCoreException(
			sprintf("Removed option id %d dose not found on column %d.", $id, $this->id),
			TxCoreException::INTERNAL);
	}

	/**
	 * @param Option $option
	 * @return Option Returns remove cancelled option.
	 * @throws TxCoreException Throws if option dose not exist.
	 */
	public function restoreRemovedOption($option)
	{
//		 if ($option->getParent()->getId() != $this->getId() ||
//		 $option->getParent()->getRevision() != $this->getRevision()) {
//			 throw new TxCoreException(
//					 sprintf("Parent mismatched"),
//					 TxCoreException::INTERNAL);
//		 }

		$options =& $this->_getRemovedOptions();
		foreach ($options as $i => $option_) {
			if ($option->getId() == $option_->getId()) {
				$this->_addOption($option);
				unset($options[$i]);
				return $option;
			}
		}
		throw new TxCoreException(
				sprintf("Removed option id %d dose not found on this column %d.", $id, $this->id),
				TxCoreException::INTERNAL);
	}


	/**
	 * @return \TxCore\Option[]
	 */
	public function &_getRemovedOptions()
	{
		return $this->removedOptions;
	}

//	 public function addOption_old(Option $option)
//	 {
//		 $this->_addOption($option);
//	 }

	public function _addOption(Option $option)
	{
		$options =& $this->getOptions();
		$option->_setParent($this);
		$options[] = $option;
	}

	/**
	 * @param int $id
	 * @param boolean $silently
	 * @return Option
	 */
	public function getOptionFor($id, $silently = false, $useCache = true)
	{
		$options = $this->getOptions();
		$foundOption = null;
		if ($useCache) {
			if ($this->optionsCache === null) {
				$this->optionsCache = array();
				foreach ($options as $option) {
					$this->optionsCache[$option->getId()] = $option;
				}
			}
			if (isset($this->optionsCache[intval($id)])) {
				$foundOption = $this->optionsCache[intval($id)];
			}
		} else {
			foreach ($options as $option) {
				if ($id == $option->getId()) {
					$foundOption = $option;
					break;
				}
			}
		}

		if ($foundOption) {
			return $foundOption;
		} else if ($silently) {
			return null;
		} else {
			throw new TxCoreException(
				sprintf("Option id %d dose not found on column %d.", $id, $this->id),
				TxCoreException::INTERNAL);
		}
	}

	/** @return array */
	public function getReferences()
	{
		return array();
	}

//	 /** @return array */
//	 public function getReferrers()
//	 {
//		 return Engine::factory()->getReferrerColumns($this);
//	 }

    /**
     * @return Table[]
     */
	public function getReferrerTables()
	{
		return Engine::factory()->getReferrerTables($this);
	}

//	 public function setReference(Reference $reference)
//	 {
// //		 if (!$this->hasReference()) {
// //			 throw new TxCoreException(
// //				 sprintf('Column %s dose not support reference property', $this->getType()),
// //				 TxCoreException::INTERNAL);
// //		 }
//		 $reference->_setParent($this);
//		 $this->reference = $reference;
//	 }
//	 /**
//	  * @return Reference
//	  */
//	 public function &getReference()
//	 {
//		 return $this->reference;;
//	 }

	public function handleGetValue($rawValue)
	{
		return $rawValue;
	}

	public function handleGetText($value)
	{
		return $value;
	}

	public function handleGetHtml($value)
	{
		return null;
	}

	public function handleSetValue($value, Row $row)
	{
		return empty_to_null($value);
	}

	public function handleStoreValue($value, Row $row)
	{
		return empty_to_null($value);
	}

	public function handleIndexKey($value)
	{
		return new Index($this->getIndexType(), empty_to_null($value));
	}

	public function handleSearchKey($value)
	{
		return $this->handleIndexKey($value);	// 通常は同じ
	}

	/**
	 * 一時的な条件の書き換え
	 * - validateSearchでも正規化や書き換えはできるが、Criteriaそのものを書き換えるかどうかが異なる
	 *
	 * @param Condition $condition
	 */
	public function handleSearchRewrite(Condition $condition)
	{
		// Override if you need.
	}

	public function handleRemove(Row $row)
	{
		if (!$this->hasValue()) {
			throw new TxCoreException(sprintf('%s has not value', $this->getType()),
				TxCoreException::INTERNAL);
		}
	}

	public function handleCalculate(Row $row, &$errors = array())
	{
		if (!$this->isCalculable()) {
			throw new TxCoreException(
				sprintf('Column %s dose not support calculation', $this->getType()),
				TxCoreException::INTERNAL);
		}
		return false;
	}

	public function handleValidate(Row $row)
	{
		return array();
	}

	public function handleValidateProperties()
	{
		$errors = array();
		array_push_all($errors, $this->validateProperty__Code());
		array_push_all($errors, $this->validateProperty__Label());
		if ($this->hasProperty('layoutParentId')) {
			array_push_all($errors, $this->validateProperty__LayoutParent());

		}
		return $errors;
	}

	public function handleValidateSearch(Condition $condition)
	{
		$requiredOps = array(
			Criteria::LIKE => ':txcore.search.like.required',
			Criteria::NOT_LIKE => ':txcore.search.not_like.required',
			Criteria::CONTAINS => ':txcore.search.contains.required',
			Criteria::NOT_CONTAINS => ':txcore.search.not_contains.required',
			Criteria::GREATER => ':txcore.search.greater.required',
			Criteria::GREATER_EQUALS => ':txcore.search.greater_equals.required',
			Criteria::LESS => ':txcore.search.less.required',
			Criteria::LESS_EQUALS => ':txcore.search.less_equals.required',
		);
		if (isset($requiredOps[$condition->getOperator()]) && is_empty($condition->getValue())) {
			return array(new Message($requiredOps[$condition->getOperator()], array(), $this));
		}
		return array();
	}

	public function handleDrop(User $by)
	{
	}

	public function handleValidateRelease()
	{
		$errors = array();
		if ($this->hasProperty('isUnique') && $this->isUnique()) {
			if ($this->getParent()->isChild(true)) {
				$errors[] = new Message(':txcore.release.unique_column_restricted_in_child', array($this->getLabel()));
			} else if (!Engine::factory()->checkUniqueColumnValueDuplication($this)) {
				$errors[] = new Message(':txcore.release.unique_duplicated', array($this->getLabel()));
			} else if (!Engine::factory()->checkUniqueColumnValueLength($this)) {
				$errors[] = new Message(':txcore.release.unique_too_long', array($this->getLabel()));
			}
		}
		return $errors;
	}

	protected function validateRequired($value)
	{
		$errors = array();
		if ($this->isRequired()) {
			if (is_empty($value)) {
				$errors[] = new Message(":txcore.required", array($this->getLabel()), $this);
			}
		}
		return $errors;
	}

	protected function validateNumeric($value, $context = null)
	{
		if (is_not_empty($value) && !is_numeric($value)) {
			return array(new Message(":txcore.invalid_numeric", array($this->getLabel()), $this));
		}
		if (is_not_empty($value) && !Util::isValidNumericRange($value)) {
			return array(new Message(":txcore.overflow_numeric", array($this->getLabel()), $this));
		}
		return array();
	}

	protected function validateLength($value)
	{
		if ($this->getMaxLength() !== null &&
			$this->getMinLength() !== null) {
			if (mb_strlen($value) > $this->getMaxLength() ||
				mb_strlen($value) < $this->getMinLength()) {
				return array(new Message(":txcore.length", array($this->getLabel(), $this->getMinLength(), $this->getMaxLength()), $this));
			}
		} else if ($this->getMaxLength() !== null) {
			if (mb_strlen($value) > $this->getMaxLength()) {
				return array(new Message(":txcore.maxlength", array($this->getLabel(), $this->getMaxLength()), $this));
			}
		} else if ($this->getMinLength() !== null) {
			if (mb_strlen($value) < $this->getMinLength()) {
				return array(new Message(":txcore.minlength", array($this->getLabel(), $this->getMinLength()), $this));
			}
		}

		return array();
	}

	protected function validateUnique($value, $rownun = null)
	{
		// Nullはチェックから除外
		$errors = array();
		if ($this->isUnique() && is_not_empty($value)) {
			$criteria = new Criteria();
			$criteria->equals($this, $value);
			if ($rownun != null)
				$criteria->notEquals($this->getParent()->getRownum(), $rownun);
			$list = Engine::factory()->searchTableInternal($this->getParent(), $criteria, null, null);
			if ($list->getTotalRowCount() > 0) {
				$errors[] = new Message(":txcore.duplicated", array($this->getLabel()), $this);
			}
		}
		return $errors;
	}

	protected function validatePropertyGreaterEqual($value1, $value2, $label1 = null, $label2 = null, $context = null)
	{
		if (is_not_empty($value1) && is_not_empty($value2) && Math::bccompare($value1, $value2) < 0 ) {
			return array(new Message(":txcore.property.less_than", array($label2), $context));
		}
		return array();
	}

	protected function validatePropertyLessEqual($value1, $value2, $label1 = null, $label2 = null, $context = null)
	{
		if (is_not_empty($value1) && is_not_empty($value2) && Math::bccompare($value1, $value2) > 0 ) {
			return array(new Message(":txcore.property.greater_than", array($label2), $context));
		}
		return array();
	}

	protected function validatePropertyInteger($value, $context = null)
	{
		if (is_not_empty($value) && !is_numeric($value)) {
			return array(new Message(":txcore.property.invalid_integer", array(), $context));
		}
		if (is_not_empty($value) && !preg_match('/^[+-]?(\d{0,20})?$/', $value)) {
			return array(new Message(":txcore.property.overflow_integer", array(), $context));
		}
		return array();
	}

	protected function validatePropertyLength($value, $context = null)
	{
		if (is_not_empty($value) && $value <= 0) {
			return array(new Message(":txcore.property.invalid_length", array(), $context));
		}
		return array();
	}

	protected function validatePropertyLengthGreaterEqual(
			$value1, $value2, $label1 = null, $label2 = null, $context = null)
	{
		if (is_not_empty($value2) && mb_strlen($value1) < $value2 ) {
			return array(new Message(":txcore.property.length_less_than", array($label2), $context));
		}
		return array();
	}

	protected function validatePropertyLengthLessEqual(
			$value1, $value2, $label1 = null, $label2 = null, $context = null)
	{
		if (is_not_empty($value2) && mb_strlen($value1) > $value2 ) {
			return array(new Message(":txcore.property.length_greater_than", array($label2), $context));
		}
		return array();
	}

	protected function validateProperty__Code()
	{
		$reNumber = '[\x{0030}-\x{0039}]'; // 0-9
		$reCode = '[^\x{0000}-\x{0040}\x{005b}-\x{005e}\x{0060}\x{007b}-\x{007f}]' // exclude ASCII, but include _A-z
			.'[^\x{0000}-\x{002f}\x{003a}-\x{0040}\x{005b}-\x{005e}\x{0060}\x{007b}-\x{007f}]*'; // include _0-9A-z
		if (is_empty($this->getCode())) {
			return array(new Message("入力してください。", array(),
					array('column' => $this, 'property' => 'code')));
		} else if (preg_match('/^'.$reNumber .'/u', $this->getCode()) > 0) {
			return array(new Message("１文字目は半角数字以外で入力してください。", array(),
					array('column' => $this, 'property' => 'code')));
		} else if (preg_match('/^'.$reCode .'$/u', $this->getCode()) == 0) {
			return array(new Message("全角文字、半角英数文字、アンダースコア _ で入力してください。", array(),
					array('column' => $this, 'property' => 'code')));
		} else {
			foreach ($this->getParent()->getColumns() as $column) {
				if ($this->getId() == $column->getId()) {
					continue;
				}
				if ($this->getCode() == $column->getCode()) {
					return array(new Message("他のフィールドと重複しています。", array(),
							array('column' => $this, 'property' => 'code')));
				}
			}
		}
		return array();
	}

	protected function validateProperty__Label()
	{
		if (is_empty($this->getLabel())) {
			return array(new Message("入力してください。", array(),
					array('column' => $this, 'property' => 'label')));
		}
		return array();
	}

	protected function validateProperty__LayoutParent()
	{
		$layoutParentId = $this->getProperty('layoutParentId');
		if (is_not_empty($layoutParentId)) {
			try {
				$this->getParent()->getColumnFor($layoutParentId);
			} catch (TxCoreException $e) {
				log_error('Layout parent missing');
				throw $e;
			}

		}
		return array();
	}

	protected function validateProperty__DefaultValue()
	{
		return array(); // override
	}

	public function handleDefaultValue(Row $row)
	{
		if ($this->hasProperty('defaultValue'))
			$row->setValue($this, $this->getDefaultValue());
	}

	public function handleAutoset(Row $row, HandlerContext $context)
	{

	}

	/**
	 * @param Row $row
	 * @param string $value
	 * @param int $offset
	 * @return \TxCore\List_
	 */
	public function handleSearchReferal(Row $row, User $by, Criteria $criteria = null, $offset = 0)
	{
		throw new TxCoreException(
			sprintf('Column %s dose not support referal serach', $this->getType()),
			TxCoreException::INTERNAL);
	}

	public function handleGetValueChange(Row $oldrow, Row $newrow)
	{
		if (!$this->hasValue() || in_array($this->getType(),array(
				Column::CREATED_BY, Column::CREATED_ON,
				Column::ROWNUM))) return null;

		$oldval = $oldrow->getValue($this);
		$newval = $newrow->getValue($this);
		if (!$this->isValueDiff($oldval, $newval)) return null;

		return $this->getValueChange($oldrow, $newrow);
	}

	protected function getValueChange(Row $oldrow, Row $newrow)
	{
		$change = new RowColumnChange();
		$change->setColumnLabel($this->getLabel());
		$change->setColumnType($this->getType());
		$change->setOldText(
			$this->formatValueChangeText($oldrow));
		$change->setNewText(
			$this->formatValueChangeText($newrow));
		return $change;
	}

	protected function isValueDiff($fromValue, $toValue)
	{
		$fromValue = empty_to_null($fromValue);
		$toValue = empty_to_null($toValue);
		return ($fromValue !== $toValue);
	}

	protected function formatValueChangeText(Row $row)
	{
		return $row->getText($this);
	}

	public function handleValueMatch(Condition $cond_, Row $row)
	{
		$cond = clone $cond_;
		$this->handleSearchRewrite($cond);
		switch ($cond->getOperator()) {
			case Criteria::EQUALS:
				return $this->isValueMatch__EQUALS($cond, $row);
			case Criteria::NOT_EQUALS:
				return $this->isValueMatch__NOT_EQUALS($cond, $row);
			case Criteria::LIKE:
				return $this->isValueMatch__LIKE($cond, $row);
			case Criteria::NOT_LIKE:
				return $this->isValueMatch__NOT_LIKE($cond, $row);
			case Criteria::GREATER_EQUALS:
				return $this->isValueMatch__GREATER_EQUALS($cond, $row);
			case Criteria::LESS_EQUALS:
				return $this->isValueMatch__LESS_EQUALS($cond, $row);
			case Criteria::GREATER:
				return $this->isValueMatch__GREATER($cond, $row);
			case Criteria::LESS:
				return $this->isValueMatch__LESS($cond, $row);
			case Criteria::CONTAINS:
				return $this->isValueMatch__CONTAINS($cond, $row);
			case Criteria::NOT_CONTAINS:
				return $this->isValueMatch__NOT_CONTAINS($cond, $row);
		}
		return false;
	}

	protected function isValueMatch__EQUALS(
		Condition $condition, Row $row)
	{
		$index = $this->handleSearchKey($condition->getValue());
		$key = $index->getValue();
		$value = $row->_createIndexKey($this)->getValue();

		if (is_empty($key) && !is_empty($value)) return false;
		if (is_empty($key) && is_empty($value)) return true;
		if (!is_empty($key) && is_empty($value)) return false;

		switch ($index->getType()) {
		case Index::NUMBER:
			return (bccomp($value, $key, 10) === 0);
			break;
		case Index::INTEGER:
			return (bccomp($value, $key, 0) === 0);
			break;
		case Index::DATETIME:
		case Index::DATE:
			$key_ = $this->expandDateExpression($key);
			$unixValue = Util::toUnixTimestamp($value);
			if (is_array($key_)) {
				$unixKey0 = Util::toUnixTimestamp($key_[0]);
				$unixKey1 = Util::toUnixTimestamp($key_[1]);
				return ($unixKey0 <= $unixValue
					&& $unixValue <= $unixKey1);
			} else {
				$unixKey = Util::toUnixTimestamp($key_);
				return $unixValue == $unixKey;
			}
			break;
		case Index::TIME:
			$unixValue = Util::toUnixTimestamp($value);
			$unixKey = Util::toUnixTimestamp($key);
			return ($unixValue == $unixKey);
		case Index::STRING:
			return ($value == $key);
			break;
		default:
			throw new TxCoreException(
				sprintf('invalid index type (%s) was given at Column %d (%s) ', $index->getType(), $this->getId(), $this->getLabel()),
				TxCoreException::INTERNAL);
		}

	}

	protected function isValueMatch__NOT_EQUALS(
		Condition $condition, Row $row)
	{
		return !$this->isValueMatch__EQUALS($condition, $row);
	}

	protected function isValueMatch__LIKE(
		Condition $condition, Row $row)
	{
		if (is_empty($row->getValue($this))) return false;

		$index = $this->handleSearchKey($condition->getValue());
		$key = $index->getValue();
		$value = $row->_createIndexKey($this)->getValue();

		if (is_empty($key)) {
			throw new TxCoreException(
				sprintf('null key given for /%s/ evaluation with this %d (%s).', $condition->getOperator(), $this->getId(), $this->getLabel()),
				TxCoreException::INTERNAL);
		}

		return (mb_strpos($value, $key, 0, 'UTF-8') !== false);
	}

	protected function isValueMatch__NOT_LIKE(
		Condition $condition, Row $row)
	{
		return !$this->isValueMatch__LIKE($condition, $row);
	}

	protected function isValueMatch__GREATER_EQUALS(
		Condition $condition, Row $row)
	{
		if (is_empty($row->getValue($this))) return false;

		$index = $this->handleSearchKey($condition->getValue());
		$key = $index->getValue();
		$value = $row->_createIndexKey($this)->getValue();

		if (is_empty($key)) {
			throw new TxCoreException(
				sprintf('null key given for /%s/ evaluation with this %d (%s).', $condition->getOperator(), $this->getId(), $this->getLabel()),
				TxCoreException::INTERNAL);
		}

		switch ($index->getType()) {
		case Index::NUMBER:
			return (bccomp($value, $key, 10) >= 0);
			break;
		case Index::INTEGER:
			return (bccomp($value, $key, 0) >= 0);
			break;
		case Index::DATETIME:
		case Index::DATE:
			$key_ = $this->expandDateExpression($key);
			$unixValue = Util::toUnixTimestamp($value);
			if (is_array($key_)) {
				$unixKey0 = Util::toUnixTimestamp($key_[0]);
				return ($unixValue >= $unixKey0);
			} else {
				$unixKey = Util::toUnixTimestamp($key_);
				return $unixValue >= $unixKey;
			}
			break;
		case Index::TIME:
			$unixValue = Util::toUnixTimestamp($value);
			$unixKey = Util::toUnixTimestamp($key);
			return ($unixValue >= $unixKey);
			break;
		//case Index::STRING:
		default:
			throw new TxCoreException(
				sprintf('invalid index type (%s) was given at Column %d (%s) ', $index->getType(), $this->getId(), $this->getLabel()),
				TxCoreException::INTERNAL);
		}

	}

	protected function isValueMatch__LESS_EQUALS(
		Condition $condition, Row $row)
	{
		if (is_empty($row->getValue($this))) return false;

		$index = $this->handleSearchKey($condition->getValue());
		$key = $index->getValue();
		$value = $row->_createIndexKey($this)->getValue();

		if (is_empty($key)) {
			throw new TxCoreException(
				sprintf('null key given for /%s/ evaluation with this %d (%s).', $condition->getOperator(), $this->getId(), $this->getLabel()),
				TxCoreException::INTERNAL);
		}

		switch ($index->getType()) {
		case Index::NUMBER:
			return (bccomp($value, $key, 10) <= 0);
			break;
		case Index::INTEGER:
			return (bccomp($value, $key, 0) <= 0);
			break;
		case Index::DATETIME:
		case Index::DATE:
			$key_ = $this->expandDateExpression($key);
			$unixValue = Util::toUnixTimestamp($value);
			if (is_array($key_)) {
				$unixKey1 = Util::toUnixTimestamp($key_[1]);
				return ($unixValue <= $unixKey1);
			} else {
				$unixKey = Util::toUnixTimestamp($key_);
				return $unixValue <= $unixKey;
			}
			break;
		case Index::TIME:
			$unixValue = Util::toUnixTimestamp($value);
			$unixKey = Util::toUnixTimestamp($key);
			return ($unixValue <= $unixKey);
			break;
		//case Index::STRING:
		default:
			throw new TxCoreException(
				sprintf('invalid index type (%s) was given at Column %d (%s) ', $index->getType(), $this->getId(), $this->getLabel()),
				TxCoreException::INTERNAL);
		}
	}

	protected function isValueMatch__GREATER(
		Condition $condition, Row $row)
	{
		if (is_empty($row->getValue($this))) return false;
		return !$this->isValueMatch__LESS_EQUALS($condition, $row);
	}

	protected function isValueMatch__LESS(
		Condition $condition, Row $row)
	{
		if (is_empty($row->getValue($this))) return false;
		return !$this->isValueMatch__GREATER_EQUALS($condition, $row);
	}

	protected function isValueMatch__CONTAINS(
		Condition $condition, Row $row)
	{
		$valueIndex = $row->_createIndexKey($this);
		$values = array_map(function ($index) {
			return $index->getValue();
		}, is_array($valueIndex) ? $valueIndex : array($valueIndex));
		$condValues = $condition->getValue();

		foreach ($condValues as $j => $condValue) {
			$index = $this->handleSearchKey($condValue);
			$key = $index->getValue();
			if (in_array($key, $values)) {
				return true;
			}
		}
		return false;
	}

	protected function isValueMatch__NOT_CONTAINS(
		Condition $condition, Row $row)
	{
		return !$this->isValueMatch__CONTAINS($condition, $row);
	}

	public function handleCompare(Row $row1, Row $row2)
	{
		if (!$this->isSortable()) {
			throw new TxCoreException(
				sprintf('Column %s dose not support compare', $this->getType()),
				TxCoreException::INTERNAL);
		}
		$value1 = $row1->_createIndexKey($this)->getValue();
		$value2 = $row2->_createIndexKey($this)->getValue();
		return strcmp($value1, $value2);
	}

	protected function expandDateExpression($value)
	{
		$now = Engine::factory()->getNow();
		$indexType = $this->getIndexType();
		if ($indexType == Index::DATE) {
			return Condition::expandDateExpression($value, $now);
		} else if ($indexType == Index::DATETIME) {
			return Condition::expandDateTimeExpression($value, $now);
		}
	}

}


class Column__Text extends Column__Calculable
{
	private static $descriptor = array(
		'showsField'=>array('boolean', true),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'sizeWidth'=>array('integer', 1),
		'isUnique'=>array('boolean', false),
		'isRequired'=>array('boolean', false),
		'minLength'=>array('integer', null),
		'maxLength'=>array('integer', null),
		'defaultValue'=>array('string', null),
		'expression'=>array('string', null),
		);

	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
	public function isCalculable()
	{
		return $this->getExpression() !== null;
	}
	public function canBeOperand($forType = null)
	{
		// 式が設定された文字列フィールドは計算に使えない
		if ($forType === self::TEXT && $this->isCalculable()) {
			return false;
		}
		return parent::canBeOperand($forType);
	}
	public function handleValidate(Row $row)
	{
		$value = $row->getValue($this);
		$errors = array();
		array_push_all($errors, $this->validateRequired($value));
		if (is_empty($errors))
			array_push_all($errors, $this->validateLength($value));
		if (is_empty($errors))
			array_push_all($errors, $this->validateUnique($value, $row->getRownum()));

		return $errors;
	}

	public function handleValidateProperties()
	{
		$errors = parent::handleValidateProperties();
		$maxminErrors = array();
		array_push_all($maxminErrors, $this->validatePropertyLength($this->getMinLength(),
		array('column'=>$this, 'property'=>'minLength')));
		array_push_all($maxminErrors, $this->validatePropertyLength($this->getMaxLength(),
		array('column'=>$this, 'property'=>'maxLength')));
		array_push_all($errors, $maxminErrors);

		if (is_empty($maxminErrors)) {
			array_push_all($errors, $this->validatePropertyGreaterEqual(
			$this->getMaxLength(), $this->getMinLength(), '最大文字数', '最小文字数',
			array('column'=>$this, 'property'=>'maxLength')));
		}

		if (is_empty($errors)) {
			if (is_not_empty($this->getDefaultValue())) {
				array_push_all($errors, $this->validatePropertyLengthLessEqual(
					$this->getDefaultValue(), $this->getMaxLength(), '初期値', '最大文字数',
					array('column'=>$this, 'property'=>'defaultValue')));
				array_push_all($errors, $this->validatePropertyLengthGreaterEqual(
					$this->getDefaultValue(), $this->getMinLength(), '初期値', '最小文字数',
					array('column'=>$this, 'property'=>'defaultValue')));
			}
		}

		return $errors;
	}

//	 /**
//	  * Do calcuration with circulation check.
//	  *
//	  * @param Row $row
//	  * @param Column $column
//	  * @throws TxCoreException
//	  */
//	 public function handleCalculate(Row $row, &$errors = array())
//	 {
//		 if ($this->isCalculable()) {
//			 parent::handleCalculate($row, $errors);
//		 }
//	 }
}
class Column__Multitext extends Column
{
	private $descriptor = array(
		'showsField'=>array('boolean', true),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'sizeHeight'=>array('integer', 5),
		'sizeWidth'=>array('integer', 1),
		'isRequired'=>array('boolean', false),
		'defaultValue'=>array('string', null),
	);
	protected function getPropertyDescriptor()
	{
		return $this->descriptor;
	}
	public function handleGetText($value)
	{
		if (is_empty($value)) {
			return $value;
		}
		$text = $value;
		$len = get_config('app', 'multitext_truncates');
		if ($len) {
			$text = mb_truncate($text, $len);
		}
		return $text;
	}
	public function handleIndexKey($value)
	{
		// Remove tags
		return new Index($this->getIndexType(), is_empty($value) ? null :
			Util::normalizeKeyword($value));
	}
	public function handleSetValue($value, Row $row)
	{
		if (is_empty($value))
			return null;
		return str_replace("\r\n", "\n", $value);
	}
	public function handleGetHtml($value)
	{
		return str_replace("\n", "<br>", htmlspecialchars($value));
	}
	public function handleValidate(Row $row)
	{
		$value = $row->getValue($this);
		$errors = array();
		array_push_all($errors, $this->validateRequired($value));
		return $errors;
	}
	protected function isValueMatch__EQUALS(
		Condition $condition, Row $row)
	{
		$key = $condition->getValue();
		$value = $row->getValue($this);

		if (is_empty($key) && !is_empty($value)) return false;
		if (is_empty($key) && is_empty($value)) return true;
		if (!is_empty($key) && is_empty($value)) return false;

		return ($value == $key);

	}
	protected function isValueMatch__LIKE(
		Condition $condition, Row $row)
	{
		if (is_empty($row->getValue($this))) return false;

		$key = $condition->getValue();
		$value = $row->getValue($this);

		if (is_empty($key)) {
			throw new TxCoreException(
				sprintf('null key given for /%s/ evaluation with this %d (%s).', $condition->getOperator(), $this->getId(), $this->getLabel()),
				TxCoreException::INTERNAL);
		}

		return (mb_strpos($value, $key, 0, 'UTF-8') !== false);
	}
	protected function getNotificationConditionalOperators()
	{
        return array(Criteria::EQUALS, Criteria::NOT_EQUALS, Criteria::LIKE, Criteria::NOT_LIKE);
	}
}
class Column__Richtext extends Column
{
	private static $descriptor = array(
		'showsField'=>array('boolean', true),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'sizeHeight'=>array('integer', 5),
		'sizeWidth'=>array('integer', 1),
		'isRequired'=>array('boolean', false),
		'defaultValue'=>array('string', null),
	);
	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
	public function handleGetText($value)
	{
		if (is_empty($value)) {
			return $value;
		}
		$text = html_entity_decode(strip_tags($value), ENT_COMPAT, 'UTF-8');
		$len = get_config('app', 'richtext_truncates');
		if ($len) {
			$text = mb_truncate($text, $len);
		}
		return $text;
	}
	public function handleIndexKey($value)
	{
		return new Index($this->getIndexType() ,is_empty($value) ? null :
			Util::normalizeKeyword($value, true));
	}
	public function handleValidate(Row $row)
	{
		$value = $row->getValue($this);
		$errors = array();
		array_push_all($errors, $this->validateRequired($value));
		return $errors;
	}

	protected function formatValueChangeText(Row $row)
	{
		$text = $row->getValue($this);
		if (is_empty($text)) return $text;
		$text = html_entity_decode(strip_tags($text),
			ENT_COMPAT, 'UTF-8');
		return $text;
	}

	protected function isValueMatch__LIKE(
		Condition $condition, Row $row)
	{
		if (is_empty($row->getValue($this))) return false;

		$key = strip_tags($condition->getValue());
		$value = strip_tags($row->getValue($this));

		if (is_empty($key)) {
			return false;
		}

		return (mb_strpos($value, $key, 0, 'UTF-8') !== false);
	}
}

class Column__Number extends Column
{
	private static $descriptor = array(
		'showsField'=>array('boolean', true),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'sizeWidth'=>array('integer', 1),
		'isUnique'=>array('boolean', false),
		'isRequired'=>array('boolean', false),
		'minValue'=>array('string', null),
		'maxValue'=>array('string', null),
		'defaultValue'=>array('string', null),
		'showsDigit'=>array('boolean', false),
	);
	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
//	 public function getShowsDigit() { return $this->showsDigit; }
//	 public function setShowsDigit($value) { $this->showsDigit = Util::toBool($value); }
	public function handleGetText($rawValue)
	{
		return $this->getProperty('showsDigit') ?
			Util::formatNumber($rawValue) : $rawValue;
	}
	public function handleValidate(Row $row)
	{
		$value = $row->getValue($this);

		$errors = array();
		array_push_all($errors, $this->validateRequired($value));
		if (is_empty($errors))
			array_push_all($errors, $this->validateNumeric($value));
		if (is_empty($errors))
			array_push_all($errors, $this->validateRange($value));
		if (is_empty($errors))
			array_push_all($errors, $this->validateUnique($value, $row->getRownum()));

		// 正規化とまるめ
		if (is_not_empty($value)) {
			$value = Math::bcnormalize($value);
			$row->setValue($this, Math::bcround($value, $this->getParent()->getMathContext()));
		}
		return $errors;
	}

	public function handleValidateProperties()
	{
		$errors = parent::handleValidateProperties();
		$maxminErrors = array();
		array_push_all($maxminErrors, $this->validatePropertyInteger($this->getMinValue(),
			array('column'=>$this, 'property'=>'minValue')));
		array_push_all($maxminErrors, $this->validatePropertyInteger($this->getMaxValue(),
			array('column'=>$this, 'property'=>'maxValue')));
		array_push_all($errors, $maxminErrors);
		array_push_all($errors, $this->validatePropertyNumeric($this->getDefaultValue(),
			array('column'=>$this, 'property'=>'defaultValue')));

		if (is_empty($maxminErrors)) {
			array_push_all($errors, $this->validatePropertyGreaterEqual(
				$this->getMaxValue(), $this->getMinValue(), '最大値', '最小値',
				array('column'=>$this, 'property'=>'maxValue')));
		}

		// 正規化
		if (is_not_empty($this->getMinValue()))
			$this->setMinValue(Math::bcnormalize($this->getMinValue(), 0));
		if (is_not_empty($this->getMaxValue()))
			$this->setMaxValue(Math::bcnormalize($this->getMaxValue(), 0));
		// 正規化とまるめ
		if (is_not_empty($this->getDefaultValue()))
			$this->setDefaultValue(Math::bcround(Math::bcnormalize($this->getDefaultValue()), $this->getParent()->getMathContext()));

		if (is_empty($errors)) {
			array_push_all($errors, $this->validatePropertyLessEqual(
			$this->getDefaultValue(), $this->getMaxValue(), '初期値', '最大値',
			array('column'=>$this, 'property'=>'defaultValue')));
			array_push_all($errors, $this->validatePropertyGreaterEqual(
			$this->getDefaultValue(), $this->getMinValue(), '初期値', '最小値',
			array('column'=>$this, 'property'=>'defaultValue')));
		}

		return $errors;
	}

	public function handleValidateSearch(Condition $condition)
	{
		$errors = parent::handleValidateSearch($condition);
		if (is_empty($errors)) {
			array_push_all($errors, $this->validateNumeric($condition->getValue()));
		}
		return $errors;
	}

	protected function validateRange($value)
	{
		if (is_empty($value))
			return array();

		$value = Math::bcnormalize($value);
		if ($this->getMaxValue() !== null &&
			$this->getMinValue() !== null) {
			if (Math::bccompare($value, $this->getMaxValue()) > 0 ||
				Math::bccompare($value, $this->getMinValue()) < 0) {
				return array(new Message(":txcore.value_range", array($this->getLabel(), $this->getMinValue(), $this->getMaxValue()), $this));
			}
		} else if ($this->getMaxValue() !== null) {
			if (Math::bccompare($value, $this->getMaxValue()) > 0) {
				return array(new Message(":txcore.value_range", array($this->getLabel(), '', $this->getMaxValue()), $this));
			}
		} else if ($this->getMinValue() !== null) {
			if (Math::bccompare($value, $this->getMinValue()) < 0) {
				return array(new Message(":txcore.value_range", array($this->getLabel(), $this->getMinValue(), ''), $this));
			}
		}

		return array();
	}

	protected function validatePropertyNumeric($value, $context = null)
	{
		if (is_not_empty($value) && !is_numeric($value)) {
			return array(new Message(":txcore.property.invalid_numeric", array(), $context));
		}
		if (is_not_empty($value) && !Util::isValidNumericRange($value)) {
			return array(new Message(":txcore.property.overflow_numeric", array(), $context));
		}
		return array();
	}

	public function isSumable()
	{
		return true;
	}
}

class Column__Calc extends Column__Calculable
{
	const FORMAT_NUMBER = 'number';
	const FORMAT_NUMBER_WITH_COMMA = 'number_with_comma';
	const FORMAT_DATE = 'date';
	const FORMAT_DATETIME = 'datetime';
	/** 時刻 */
	const FORMAT_TIME = 'time';
	/** 時間 */
	const FORMAT_HOURS = 'hours';
	/** 期間 */
	const FORMAT_DAYS = 'days';

	private static $descriptor = array(
		'showsField'=>array('boolean', true),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'sizeWidth'=>array('integer', 1),
		'expression'=>array('string', null),
		'format'=>array('string', null),	// タイプ
	);

	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}

	public function handleGetText($rawValue)
	{
		switch ($this->getFormat()) {
			case self::FORMAT_NUMBER:
				return $rawValue;
			case self::FORMAT_NUMBER_WITH_COMMA:
				return Util::formatNumber($rawValue);
			case self::FORMAT_DATE:
				return Util::formatDate(Util::toInt($rawValue), false);
			case self::FORMAT_DATETIME:
				return Util::formatDateTime(Util::toInt($rawValue), false);
			case self::FORMAT_TIME:
				return Util::formatTime(Util::toInt($rawValue), false);
			case self::FORMAT_HOURS:
				return Util::formatHours($rawValue, $this->getParent()->getMathContext());
			case self::FORMAT_DAYS:
				return Util::formatDays($rawValue, $this->getParent()->getMathContext());
			default:
				return $rawValue;
		}
	}

	public function isSumable()
	{
		// ↓要調査
		return !in_array($this->getFormat(self::FORMAT_DATE, self::FORMAT_DATETIME, self::FORMAT_TIME), array(), true);
	}
	public function handleValidateSearch(Condition $condition)
	{
		$errors = parent::handleValidateSearch($condition);
		if (is_empty($errors)) {
			array_push_all($errors, $this->validateNumeric($condition->getValue()));
		}
		return $errors;
	}
	public function handleValidate(Row $row)
	{
		$value = $row->getValue($this);
		$errors = array();
		array_push_all($errors, $this->validateNumeric($value));
		return $errors;
	}
	public function handleCalculate(Row $row, &$errors = array())
	{
		$ret = parent::handleCalculate($row, $errors);
		if ($ret === false || is_empty($ret)) {
			return $ret;
		}
		return Math::bcround($ret, $this->getParent()->getMathContext());
	}
}

class Column__Radio extends Column
{
	private static $descriptor = array(
		'showsField'=>array('boolean', true),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'sizeWidth'=>array('integer', 1),
		'isRequired'=>array('boolean', true),
		'defaultValueId'=>array('integer', null),
		'optionsAlignment'=>array('string', null),
	);
	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
	public function setDefaultValue($value)
	{
		if ($value !== null && !($value instanceof Option))
			throw new TxCoreException(
				sprintf('Checkbox default value should be array of Option.'),
				TxCoreException::INTERNAL);
		parent::setProperty('defaultValueId', $value !== null ? $value->getId() : null);
	}
	public function getDefaultValue()
	{
		$id = parent::getProperty('defaultValueId');
		return $id !== null ? $this->getOptionFor($id) : null;
	}
	public function handleDefaultValue(Row $row)
	{
		$row->setValue($this, $this->getDefaultValue());
	}
	public function handleGetText($value)
	{
		if ($value === null)
			return null;
		return $value->getName();
	}
	public function handleGetValue($rawValue)
	{
		if (is_empty($rawValue))
			return null;
		$option = $this->getOptionFor($rawValue, true);
		if ($option === null) {
			// 消された場合は「削除されした」を代替で出す
			$option = new Option($rawValue);
			$option->setName("*削除されました*");
			$option->_setDeleted(true);
		}
		return $option;
	}
	public function handleStoreValue($option, Row $row)
	{
		if (is_empty($option))
			return null;
		return $option->getId();
	}
	public function handleIndexKey($option)
	{
		return new Index($this->getIndexType(), is_not_empty($option) ? $option->getId() : null);
	}
	public function hasOptions()
	{
		return true;
	}
	public function handleValidate(Row $row)
	{
		$value = $row->getValue($this);

		$errors = array();
		array_push_all($errors, $this->validateRequired($value));
		return $errors;
	}

	protected function isValueDiff($fromValue, $toValue)
	{
		/* @var fromValue Option */
		/* @var toValue Option */
		$fromId = $fromValue ? $fromValue->getId() : null;
		$toId = $toValue ? $toValue->getId() : null;
		return ($fromId !== $toId);
	}

	public function handleCompare(Row $row1, Row $row2)
	{
		$option1 = $row1->getValue($this);
		$option2 = $row2->getValue($this);
		$value1 = $option1 ? $option1->getSeq() : null;
		$value2 = $option2 ? $option2->getSeq() : null;
		if ($value1 !== null && $value2 !== null) {
			return $value1 - $value2;
		}
		if ($value1 !== null && $value2 === null) {
			return 1; // like mysql
		}
		if ($value1 === null && $value2 !== null) {
			return -1;
		}
		return 0;
	}
}

class Column__Checkbox extends Column
{
	private static $descriptor = array(
		'showsField'=>array('boolean', true),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'sizeWidth'=>array('integer', 1),
		'isRequired'=>array('boolean', false),
		'defaultValueIds'=>array('array', array()),
		'optionsAlignment'=>array('string', null),
	);
	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
	public function hasOptions()
	{
		return true;
	}
	public function setDefaultValue($value)
	{
		if (!is_array($value))
			throw new TxCoreException(
				sprintf('Checkbox default value should be array of Option.'),
				TxCoreException::INTERNAL);
		parent::setProperty('defaultValueIds', array_map(function($t) {
			return $t->getId();
		}, $value));
	}
	public function getDefaultValue()
	{
		$this_ = $this;
		$ids = parent::getProperty('defaultValueIds');
		if (is_empty($ids))
			$ids = array();
		return array_map(function($t) use ($this_) { return $this_->getOptionFor($t); }, $ids);
	}
	public function handleDefaultValue(Row $row)
	{
		$row->setValue($this, $this->getDefaultValue());
	}
	public function handleSetValue($value, Row $row)
	{
		if ($value !== null && !is_array($value))
			throw new TxCoreException(
				sprintf('Checkbox value should be array of Option'),
				TxCoreException::INTERNAL);
		return $value === null ? array() : $value;
	}
	public function handleValidate(Row $row)
	{
		$value = $row->getValue($this);
		$errors = array();
		array_push_all($errors, $this->validateRequired($value));
		return $errors;
	}
	public function handleGetText($options)
	{
		if (is_empty($options))
			return array();
		$names = array();
		foreach ($options as $option)
			$names[] = $option->getName();
		return $names;
	}
	public function handleGetValue($jsonValue)
	{
		if (is_empty($jsonValue))
			return array();
		$options = array();
		foreach (Util::decodeJSON($jsonValue) as $optionId) {
			$option = $this->getOptionFor($optionId, true);
			if ($option === null) {
				// 消された場合は「削除されした」を代替で出す
				$option = new Option($optionId);
				$option->setName("*削除されました*");
				$option->_setDeleted(true);
			}
			$options[] = $option;
		}
		// 並びを正規化
		usort($options, function($a, $b) {
			return compare_int($a->getSeq(), $b->getSeq());
		});
		return $options;
	}
	public function handleStoreValue($options, Row $row)
	{
		return Util::encodeJSON($options !== null ? array_map(function($option) {
				return $option->getId();
			}, $options) : array());
	}
	public function handleIndexKey($options)
	{
		if ($options !== null && is_not_empty($options)) {
			$indexType = $this->getIndexType();
			return array_map(function($option) use ($indexType) {
				return new Index($indexType, is_not_empty($option) ? $option->getId() : null);
			}, $options);
		} else {
			return new Index($this->getIndexType(), null);
		}
	}
	public function handleSearchKey($option)
	{
		return new Index($this->getIndexType(), is_not_empty($option) ? $option->getId() : null);
	}
//	 public function setDefaultValue($options)
//	 {
//		 if ($options !== null && !is_array($options))
//			 throw new TxCoreException(
//				 sprintf('Radio default value should be array of Option.'),
//				 TxCoreException::INTERNAL);
//		 parent::setDefaultValue($options);
//	 }
//	 public function _setStoreDefaultValue($value)
//	 {
//		 $this->defaultValue = $this->handleGetValue($value);
//	 }
//	 public function _getStoreDefaultValue()
//	 {
//		 return $this->handleStoreValue($this->defaultValue);
//	 }
	protected function isValueDiff($fromValue, $toValue)
	{
		/* @var fromValue Option[] */
		/* @var toValue Option[] */
		$fromIds = array();
		if (count($fromValue) != count($toValue)) return true;
		foreach ($fromValue as $option) {
			$fromIds[] = $option->getId();
		}
		foreach ($toValue as $option) {
			if (!in_array($option->getId(), $fromIds)) {
				return true;
			}
		}
		return false;
	}

	protected function formatValueChangeText(Row $row)
	{
		return implode("\n", $row->getText($this));
	}

}
class Column__Dropdown extends Column__Radio
{
	private static $descriptor = array(
		'showsField'=>array('boolean', true),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'sizeWidth'=>array('integer', 1),
		'defaultValueId'=>array('integer', null),
		'isRequired'=>array('boolean', false),
	);
	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
}
class Column__Date extends Column
{
	private static $descriptor = array(
		'showsField'=>array('boolean', true),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'sizeWidth'=>array('integer', 1),
		'isUnique'=>array('boolean', false),
		'isRequired'=>array('boolean', false),
		'defaultValue'=>array('string', null),
	);
	public function handleGetValue($value)
	{
		return Util::formatDate($value);
	}
	public function handleDefaultValue(Row $row)
	{
        $intent = Engine::factory()->getIntent();
        if ($intent !== null && $intent->getDate() !== null) {
            $row->setValue($this, $intent->getDate());
        } else {
            $row->setValue($this, Util::formatDate($this->getDefaultValue(), true));
        }
	}
	public function handleSetValue($value, Row $row)
	{
		return str_replace('/', '-', $value);
	}
	public function handleValidate(Row $row)
	{
		$value = $row->getValue($this);
		$errors = array();
		array_push_all($errors, $this->validateRequired($value));
		if (is_empty($errors))
			array_push_all($errors, $this->validateDateFormat($value));
		if (is_empty($errors))
			array_push_all($errors, $this->validateUnique($value, $row->getRownum()));
		return $errors;
	}
	public function handleValidateSearch(Condition $condition)
	{
		$errors = parent::handleValidateSearch($condition);
		if (is_empty($errors)) {
			if (is_not_empty($condition->getValue()) && !Util::isValidDate($condition->getValue(), true)) {
				$errors[] = new Message(':txcore.search.date.format', array());
			}
		}
		return $errors;
	}
	private function validateDateFormat($value)
	{
		$errors = array();
		if (is_not_empty($value) && !Util::isValidDate($value)) {
			$errors[] = new Message(':txcore.invalid_date', array($this->getLabel()), $this);
		}
		return $errors;
	}
//	 public function handleSearchKey($value)
//	 {
//		 if ($value === 'now') {
//			 return new Index($this->getIndexType(), date('Y-m-d'));
//		 } else if ($value === 'current_month') {
//			 return new Index($this->getIndexType(), array(date('Y-m-01'), date('Y-m-d', strtotime('last day'))), true);
//		 } else if ($value === 'last_month') {
//			 return new Index($this->getIndexType(), array(date('Y-m-01'), date('Y-m-d', strtotime('last day of last month'))), true);
//		 } else if ($value === 'current_year') {
//			 return new Index($this->getIndexType(), array(date('Y-01-01'), date('Y-12-31')), true);
//		 } else {
//			 return new Index($this->getIndexType(), $value);
//		 }
//	 }
	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}

}
class Column__Time extends Column
{
	private static $descriptor = array(
		'showsField'=>array('boolean', true),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'sizeWidth'=>array('integer', 1),
		'isRequired'=>array('boolean', false),
		'defaultValue'=>array('string', null),
//		 'defaultExpression'=>array('string', null),
	);
	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
	public function handleDefaultValue(Row $row)
	{
		$row->setValue($this, Util::formatTime($this->getProperty('defaultValue'), true));
	}
	public function handleValidate(Row $row)
	{
		$value = $row->getValue($this);

		$errors = array();
		array_push_all($errors, $this->validateRequired($value));
		if (is_empty($errors))
			array_push_all($errors, $this->validateTimeFormat($value));
		return $errors;
	}
	public function handleValidateSearch(Condition $condition)
	{
		$errors = parent::handleValidateSearch($condition);
		if (is_empty($errors)) {
			if (is_not_empty($condition->getValue()) && !Util::isValidTime($condition->getValue())) {
				$errors[] = new Message(':txcore.search.time.format');
			}
		}
		return $errors;
	}
	private function validateTimeFormat($value)
	{
		$errors = array();
		if (is_not_empty($value) && !Util::isValidTime($value)) {
			$errors[] = new Message(':txcore.invalid_time', array($this->getLabel()), $this);
		}
		return $errors;
	}
//	 public function handleSearchKey($value)
//	 {
//		 if ($value === 'now') {
//			 return new Index($this->getIndexType(), date('Y-m-d'));
//		 } else if ($value === 'today') {
//		 } else {
//			 return new Index($this->getIndexType(), $value);
//		 }
//	 }
//	 public function getDefaultValue()
//	 {
//		 if ($this->getDefaultValue() !== null) {
//			 return $this->handleGetValue($this->defaultValue);
//		 } else if ($this->getDefaultExpression() !== null) {
//			 if ($this->getDefaultExpression() === 'now') {
//				 return Util::formatTime(date('H:i'));
//			 } else {
//				 throw new TxCoreException(
//					 sprintf('Checkbox value should be array of option'),
//					 TxCoreException::INTERNAL);
//			 }
//		 }
//		 return null;
//	 }
}
class Column__Datetime extends Column
{
	private static $descriptor = array(
		'showsField'=>array('boolean', true),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'sizeWidth'=>array('integer', 1),
		'isUnique'=>array('boolean', false),
		'isRequired'=>array('boolean', false),
		'defaultValue'=>array('string', null),
//		 'defaultExpression'=>array('string', null),
	);
	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
	public function handleGetValue($value)
	{
		return Util::formatDateTime($value);
	}
	public function handleDefaultValue(Row $row)
	{
		$row->setValue($this, Util::formatDateTime($this->getDefaultValue(), true));
	}
	public function handleSetValue($value, Row $row)
	{
		return str_replace('/', '-', $value);
	}
	public function handleValidate(Row $row)
	{
		$value = $row->getValue($this);
		$errors = array();
		array_push_all($errors, $this->validateRequired($value));
		if (is_empty($errors))
			array_push_all($errors, $this->validateDatetimeFormat($value));
		if (is_empty($errors))
			array_push_all($errors, $this->validateUnique($value, $row->getRownum()));
		return $errors;
	}
	public function handleValidateSearch(Condition $condition)
	{
		$errors = parent::handleValidateSearch($condition);
		if (is_empty($errors)) {
			if (is_not_empty($condition->getValue()) && !Util::isValidDateTime($condition->getValue(), true)) {
				$errors[] = new Message(':txcore.search.datetime.format', array());
			}
		}
		return $errors;
	}
	private function validateDatetimeFormat($value)
	{
		$errors = array();
		if (is_not_empty($value) && !Util::isValidDateTime($value)) {
			$errors[] = new Message(':txcore.invalid_datetime', array($this->getLabel()), $this);
		}
		return $errors;
	}
//	 public function getDefaultValue()
//	 {
//		 if ($this->getDefaultValue() !== null) {
//			 return $this->handleGetValue($this->defaultValue);
//		 } else if ($this->getDefaultExpression() !== null) {
//			 if ($this->getDefaultExpression() === 'now') {
//				 return Util::formatTime(date('Y-m-d H:i'));
//			 } else {
//				 throw new TxCoreException(
//					 sprintf('Checkbox value should be array of option'),
//					 TxCoreException::INTERNAL);
//			 }
//		 }
//		 return null;
//	 }
}

class LookupValue
{
	private $key;	//=row_id
	private $value;
	private $parent;

	public function getKey()
	{
		return $this->key;
	}
	public function _setKey($value)
	{
		$this->key = $value;
	}
	public function setKey($value)
	{
		$this->key = $value;
	}
	public function _setParent(Column $column)
	{
		$this->parent = $column;
	}
//	 public function setRow(Row $row)
//	 {
//		 $this->key = $row->getId();
// //		 $this->key = $value;
//	 }
	public function setValue($value)
	{
		$this->value = $value;
	}
	public function getValue()
	{
		return $this->value;
	}
}

class Column__File extends Column
{
	private static $descriptor = array(
		'showsField'=>array('boolean', true),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'sizeWidth'=>array('integer', 1),
		'isRequired'=>array('boolean', false),
		'thumbnailMaxSize'=>array('integer', 150),
		'imageMaxSize'=>array('integer', null),
	);
	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
	public function handleGetText($files)
	{
		if ($files === null)
			return array();
		return array_map(function($file) {
			return $file->getFileName(); } , $files);
	}
	public function handleDefaultValue(Row $row)
	{
		$row->setValue($this, array());
	}
	public function handleGetValue($rawValue)
	{
		if ($rawValue === null)
			return array();
		$json = Util::decodeJSON($rawValue);	// Returns array
		$values = array();
		foreach ($json as $fileEntry) {
			$value = new File();
			$value->_setContentType($fileEntry->contentType);
			$value->_setFileName($fileEntry->fileName);
			$value->_setFilekey($fileEntry->filekey);
			 $value->_setSize(@$fileEntry->size);
//			 $value->_setThumbnailkey(@$fileEntry->thumbnailkey);
			$value->_setParent($this);
			$value->_setTemp(false);
			$values[] = $value;
		}
		return $values;
	}
	public function handleStoreValue($values, Row $row)
	{
		$storage = Engine::factory()->getStorage();
		$removedValue = $row->getRemovedValue($this);
		foreach (is_array($removedValue) ? $removedValue : array() as $file) {
			$storage->removeFile($file->getFilekey());
		}
		return Util::encodeJSON($values !== null ? array_map(function(File $file) {
			return array(
				'fileName' => $file->getFileName(),
				'contentType' => $file->getContentType(),
				'filekey' => $file->getFilekey(),
				'size' => $file->getSize(),
//				 'thumbnailkey' => $file->getThumbnailkey(),
			);
		}, $values) : array());
	}
	public function handleSetValue($value, Row $row)
	{

		if ($value !== null && !is_array($value))
			throw new TxCoreException(
				sprintf('File value should be array of File'),
				TxCoreException::INTERNAL);

		$oldValue = $row->getValue($this);
		if ($oldValue !== null) {
			foreach ($oldValue as $oldFile) {
				$found = false;
				foreach ($value as $file) {
					if ($oldFile->getFilekey() == $file->getFilekey()) {
						$found = true;
						break;
					}
				}
				if (!$found) {
					$removedFile = clone $oldFile;
					$removedFile->_setRemoved(true);
					$row->removeFile($this, $removedFile);
				}
			}
		}

		return $value === null ? array() : $value;
	}
	public function handleAutoset(Row $row, HandlerContext $context)
	{
		$files = $row->getValue($this);
		$validFiles = array();
		foreach ($files as $file) {	/* @var $file File */
			if ($file->isTemp()) {
				$storage = Engine::factory()->getStorage();

//				 if (!file_exists($file->getFilekey()))
//					 throw new TxCoreException(sprintf('File dose not exist %s.', $file->getFilekey()),
//						 TxCoreException::INTERNAL);
				if (is_empty($file->getFileName()))
					throw new TxCoreException(sprintf('Not filename was specified.'),
						TxCoreException::INTERNAL);

				if (is_empty($file->getContentType()))
					$file->_setContentType('application/octet-stream');

				$ext = pathinfo($file->getFilekey(), PATHINFO_EXTENSION);
				if (!$ext) { $ext = 'dat';}

				$resizedFilekey = $this->resizeFile($file, $ext);
				$sourceFilekey = $resizedFilekey ? $resizedFilekey : $file->getFilekey();

				$table = $this->getParent();
				$filekey = concat_path($table->getId(), $this->getId(), Util::getUniqeFileName($ext));
				$storage->createFile($filekey, $sourceFilekey);

				$file->_setTemp(false);
				$file->_setFilekey($filekey);
				$file->_setSize($storage->getFileSize($filekey));
				$file->_setParent($this);
			}
			$validFiles[] = $file;
		}
		$row->setValue($this, $files);
	}
	public function handleValidate(Row $row)
	{
		$value = $row->getValue($this);
		$errors = array();
		array_push_all($errors, $this->validateRequired($value));
		foreach ($value as $file) {	/* @var $file File */
			if ($file->isTemp()) {
				array_push_all($errors, $this->validateFileSize($file));
			}
		}
		return $errors;
	}
	public function handleIndexKey($values)
	{
		if ($values !== null && !is_array($values))
			throw new TxCoreException(
				sprintf('File value should be array of File'),
				TxCoreException::INTERNAL);

		// Like 検索しかないので、単一のインデックスを作成する
		$indexValue = join("\t", array_map(function($value) {
				return Util::normalizeKeyword($value->getFileName());
			}, $values === null ? array() : $values));
		return new Index($this->getIndexType(), $indexValue);
	}
	public function handleSearchKey($value)
	{
		return new Index($this->getIndexType(), Util::normalizeKeyword($value));
	}
	public function handleRemove(Row $row)
	{
		$storage = Engine::factory()->getStorage();
		$files = $row->getValue($this);
		if ($files !== null) {
			foreach ($files as $file) {
				$storage->removeFile($file->getFilekey());
			}
		}
		$row->setValue($this, array());
	}
	private function resizeFile(File $file, $ext)
	{
		$resizedFilekey = null;
		if (preg_match(get_config('app', 'file_inline'), $file->getContentType()) === 1) {
			$srcPath = $file->getFilekey();
			$destPath = $file->getFilekey().'.resized.'.$ext;
			$size = $this->getImageMaxSize() ?: 1000; // 規定値
			list($width, $height) = getimagesize($srcPath);
			if($size > 0 && ($width > $size || $height > $size) ) {
				$resizer = new Resizer(Resizer::IMAGICK);
				$resizer->setAllowScaleUp(false);
				$resizer->setStyle(Resizer::CONTAIN);
				list ($type) = $resizer->resize($srcPath, $destPath, $size, $size);
				$resizedFilekey = $destPath;
			}
		}
		return $resizedFilekey;
	}
	private function validateFileSize(File $file)
	{
		$errors = array();
		if (preg_match(get_config('app', 'file_inline'), $file->getContentType()) === 1) {
			if($this->getImageMaxSize() === null || $this->getImageMaxSize() > 0) {
				$limit = get_config('app', 'data_file_imagesize_limit');
				if ($limit != null) {
					list($width, $height) = getimagesize($file->getFilekey());
					if ($width > $limit || $height > $limit) {
						$errors[] = new Message(":txcore.maximagesize",
								array($this->getLabel(), $file->getFileName(), $limit), $this);
					}
				}
			}
		}
		return $errors;
	}

	protected function isValueDiff($fromValue, $toValue)
	{
		/* @var fromValue File[] */
		/* @var toValue File[] */
		$fromIds = array();
		if (count($fromValue) != count($toValue)) return true;
		foreach ($fromValue as $file) {
			$fromIds[] = $file->getFilekey();
		}
		foreach ($toValue as $file) {
			if (!in_array($file->getFilekey(), $fromIds)) {
				return true;
			}
		}
		return false;
	}

	protected function formatValueChangeText(Row $row)
	{
		return implode("\n", $row->getText($this));
	}

	protected function isValueMatch__LIKE(
		Condition $condition, Row $row)
	{
		if (is_empty($row->getValue($this))) return false;

		$key = $condition->getValue();
		$value = $row->getValue($this);

		if (is_empty($key)) {
			throw new TxCoreException(
				sprintf('null key given for /%s/ evaluation with this %d (%s).', $condition->getOperator(), $this->getId(), $this->getLabel()),
				TxCoreException::INTERNAL);
		}

		$found = false;
		foreach ($value as $file) {
			if (mb_strpos($file->getFileName(),
					$key, 0, 'UTF-8') !== false) {
				$found = true;
			}
		}

		return $found;
	}
}

class Column__Link extends Column
{
	const TELNO = 'tel';
	const URL = 'url';
	const EMAIL = 'email';
	const LOCAL_URL = 'local_url';

	private static $descriptor = array(
		'showsField'=>array('boolean', true),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'sizeWidth'=>array('integer', 1),
		'isUnique'=>array('boolean', false),
		'isRequired'=>array('boolean', false),
		'minLength'=>array('integer', null),
		'maxLength'=>array('integer', null),
		'defaultValue'=>array('string', null),
		'linkType'=>array('string', null),
	);
	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
	public function handleGetHtml($value)
	{
		if (is_empty($value))
			return null;
		$linkType = $this->getProperty('linkType');
		if ($linkType == self::TELNO) {
			return '<a href="callto:'.htmlspecialchars($value).'">'.htmlspecialchars($value).'</a>';
		} else if ($linkType == self::URL) {
			return '<a href="'.htmlspecialchars($value).'" target="_new">'.htmlspecialchars($value).'</a>';
		} else if ($linkType == self::EMAIL) {
			return '<a href="mailto:'.htmlspecialchars($value).'">'.htmlspecialchars($value).'</a>';
		} else if ($linkType == self::LOCAL_URL) {
			return '<a href="file://'.htmlspecialchars($value).'">'.htmlspecialchars($value).'</a>';
		}
	}
	public function handleValidate(Row $row)
	{
		$value = $row->getValue($this);
		$errors = array();
		array_push_all($errors, $this->validateRequired($value));
		if (is_empty($errors))
			array_push_all($errors, $this->validateLength($value));
		if (is_empty($errors))
			array_push_all($errors, $this->validateUnique($value, $row->getRownum()));
		if (is_empty($errors))
			array_push_all($errors, $this->validateLinkFormat($value));
		return $errors;
	}
	public function handleValidateProperties()
	{
		$errors = parent::handleValidateProperties();
		if (is_empty($this->getLinkType())) {
			array_push_all($errors, new Message("linkType is required.", array(), $this));
		} else if (is_not_empty($this->getDefaultValue())) {
			switch ($this->getLinkType()) {
				case self::URL :
					if (!Util::isValidUrl($this->getDefaultValue())) {
						array_push_all($errors, array(new Message(":txcore.property.invalid_url", array(),
						array('column'=>$this, 'property'=>'defaultValue'))));
					}
					break;
				case self::EMAIL :
					if (!Util::isValidEmail($this->getDefaultValue())) {
						array_push_all($errors, array(new Message(":txcore.property.invalid_email", array(),
						array('column'=>$this, 'property'=>'defaultValue'))));
					}
					break;
				case self::LOCAL_URL :
				case self::TELNO :
				default:
			}
		}

		$maxminErrors = array();
		array_push_all($maxminErrors, $this->validatePropertyLength($this->getMinLength(),
		array('column'=>$this, 'property'=>'minLength')));
		array_push_all($maxminErrors, $this->validatePropertyLength($this->getMaxLength(),
		array('column'=>$this, 'property'=>'maxLength')));
		array_push_all($errors, $maxminErrors);

		if (is_empty($maxminErrors)) {
			array_push_all($errors, $this->validatePropertyGreaterEqual(
			$this->getMaxLength(), $this->getMinLength(), '最大文字数', '最小文字数',
			array('column'=>$this, 'property'=>'maxLength')));
		}

		if (is_empty($errors)) {
			if (is_not_empty($this->getDefaultValue())) {
				array_push_all($errors, $this->validatePropertyLengthLessEqual(
					$this->getDefaultValue(), $this->getMaxLength(), '初期値', '最大文字数',
					array('column'=>$this, 'property'=>'defaultValue')));
				array_push_all($errors, $this->validatePropertyLengthGreaterEqual(
					$this->getDefaultValue(), $this->getMinLength(), '初期値', '最小文字数',
					array('column'=>$this, 'property'=>'defaultValue')));
			}
		}

		return $errors;
	}

	protected function validateLinkFormat($value)
	{
		$errors = array();
		if (is_not_empty($value)) {
			switch ($this->getLinkType()) {
				case self::URL :
					if (!Util::isValidUrl($value)) {
						$errors[] = new Message(":txcore.invalid_url", array($this->getLabel()), $this);
					}
					break;
				case self::EMAIL :
					if (!Util::isValidEmail($value)) {
						$errors[] = new Message(":txcore.invalid_email", array($this->getLabel()), $this);
					}
					break;
				case self::LOCAL_URL :
				case self::TELNO :
				default:
			}
		}
		return $errors;
	}
}

class Column__Relational extends Column
{
	private static $descriptor = array(
		'showsField'=>array('boolean', true),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
//		 'sizeWidth'=>array('integer', null),
//		 'referenceTable'=>array('object', null),
		'referenceTableId'=>array('integer', null),
//		 'referenceTargetColumn'=>array('object', null),	// Alias
		'referenceTargetColumnId'=>array('integer', null),
//		 'referenceSourceColumn'=>array('object', null),	// Alias
		'referenceSourceColumnId'=>array('integer', null),
//		 'referenceColumns' => array('array', array()),	// Alias
		'referenceColumnIds' => array('array', array()),
//		 'referenceSortColumn'=>array('object', null),	// Alias
		'referenceSortColumnId'=>array('integer', null),
		'referenceSortsReverse'=>array('boolean', false),
		'referenceMaxRow'=>array('integer', null),
		'referenceAggregations' => array('array', array()),
	);

	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
	public function hasReference()
	{
		return true;
	}
	public function handleSearchReferal(Row $row, User $by, Criteria $criteria = null, $offset = 0)
	{
		if ($criteria === null) {
			$criteria = new Criteria();
			$criteria->setListColumns($this->getReferenceColumns());
			$criteria->equals($this->getReferenceTargetColumn(), $row->getValue($this->getReferenceSourceColumn()));
			if ($this->getReferenceSortColumn() !== null)
				$criteria->addSort($this->getReferenceSortColumn(), $this->getReferenceSortsReverse());
		}
		return $this->getReferenceTable()->search($criteria, $by, $offset, $this->getReferenceMaxRow());
	}

	public function getReferences()
	{
		if (!$this->hasReference()) {
			throw new TxCoreException(
					sprintf('Column %s dose not support references property', $this->getType()),
					TxCoreException::INTERNAL);
		}

		if ($this->getReferenceTargetColumn() === null) {
			throw new TxCoreException(
				sprintf('No reference column'),
				TxCoreException::INTERNAL);
		}

		$references = array();
		$references[] = new Reference(array($this->getReferenceTargetColumn(), $this));
		$references[] = new Reference(array($this->getReferenceSortColumn(), $this));
		$this_ = $this;
		$references = array_merge($references, array_map(function($column) use ($this_) {
			return new Reference(array($column, $this_));
		}, $this->getReferenceColumns()));
		$references = array_merge($references, array_map(function($columns) use ($this_) {	
			return new Reference(array($columns[0], $this_));
		}, $this->getReferenceAggregations()));

		return $references;

	}
}

/**
 * このカラムにセットされる値は、一時的なデータ保持のための値である。
 */
class Column__RelationalInput extends Column
{
	private static $descriptor = array(
		'showsField'=>array('boolean', true),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'referenceTableId'=>array('integer', null),
		'referenceColumnIds' => array('array', array()),
		'referenceSortColumnId'=>array('integer', null),
		'referenceSortsReverse'=>array('boolean', false),
	);

	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
	public function hasReference()
	{
		return true;
	}
	public function hasRelationship()
	{
		return true;
	}
	public function handleSetValue($value, Row $row)
	{
		if ($value !== null && !is_array($value))
			throw new TxCoreException(
				sprintf('RelationalInput value should be array of Option'),
				TxCoreException::INTERNAL);
		return $value === null ? array() : $value;
	}
	public function handleSearchReferal(Row $row, User $by, Criteria $criteria = null, $offset = 0)
	{
		if ($criteria === null) {
			$criteria = new Criteria();
			$criteria->setListColumns($this->getReferenceColumns());
			if ($this->getReferenceSortColumn() !== null)
				$criteria->addSort($this->getReferenceSortColumn(), $this->getReferenceSortsReverse());
		}
		return $this->getReferenceTable()->searchForRelationships($row, $this, $criteria, $by, $offset, null);
	}

	public function getReferences()
	{
		$references = array();
		$references[] = new Reference(array($this->getReferenceSortColumn(), $this));
		$this_ = $this;
		$references = array_merge($references, array_map(function($column) use ($this_) {
			return new Reference(array($column, $this_));
		}, $this->getReferenceColumns()));

		return $references;

	}

	public function handleValidateProperties()
	{
		$errors = parent::handleValidateProperties();
		array_push_all($errors, $this->validateProperty__ReferenceTable());
		return $errors;
	}

	public function handleValidateRelease()
	{
		$errors = array();
		if ($this->getParent()->isChild(true)) {
			$errors[] = new Message(':txcore.release.column_restricted_in_child', array($this->getLabel()));
			return $errors;
		}
		array_push_all($errors, $this->validateProperty__ReferenceTable());
		foreach ($errors as $i => $error) {
			$label = $this->getLabel();
			$name = $this->getReferenceTable()->getName();
			$text = $error->getMessage();
			$errors[$i] = new Message(':txcore.release.reference_table_inconsistent', array($label, $name, $text));
		}
		return $errors;
	}

	protected function validateProperty__ReferenceTable()
	{
		$referenceTable = $this->getReferenceTable();
		if (is_empty($referenceTable)) {
			return array(new Message("入力してください。", array(),
					array('column' => $this, 'property' => 'code')));
		} else {
			$filter = array('type' => Column::RELATIONAL_INPUT);
			foreach ($this->getParent()->getColumns($filter) as $column) {
				if ($this->getId() == $column->getId()) {
					continue;
				}
				if ($column->getReferenceTable() === null) {
					continue;
				}
				if ($referenceTable->getId()
					== $column->getReferenceTable()->getId()) {
					return array(new Message("他のフィールドと重複しています。", array(),
							array('column' => $this, 'property' => 'referenceTableId')));
				}
			}

			$parentTables = $referenceTable->getParentTables();
			foreach ($parentTables as $parentTable) {
				if ($parentTable->getId() != $this->getParent()->getId()) {
					return array(new Message("他のデータベースの子としてすでに指定されています。", array(),
							array('column' => $this, 'property' => 'referenceTableId')));
				}
			}

			if ($referenceTable->isParent(true)) {
				return array(new Message("他のデータベースの親としてすでに指定されています。", array(),
						array('column' => $this, 'property' => 'referenceTableId')));
			}

			$referenceTrunk = Engine::factory()->getTable($referenceTable->getId(), Engine::TRUNK);
			if ($referenceTable->hasUniqueConstraint()
				|| $referenceTrunk->hasUniqueConstraint()) {
				return array(new Message("重複禁止設定されたフィールドを子として設定することはできません。", array(),
					array('column' => $this, 'property' => 'referenceTableId')));
			}

			$list = Engine::factory()->searchTableInternal($referenceTable,
				new Criteria(), 0, 1);
			$count = Engine::factory()->countRelationshipsByColumn($this);
			if ($list->getTotalRowCount() > 0 && $count == 0) {
				return array(new Message("すでにデータの存在するデータベースを子として設定することはできません。", array(),
					array('column' => $this, 'property' => 'referenceTableId')));
			}
		}
		return array();
	}

	public function handleDrop(User $by)
	{
		$referenceTable = $this->getReferenceTable();
		Engine::factory()->clearTableData($referenceTable, $by);
	}

	public function handleGetValueChange(Row $oldrow, Row $newrow)
	{
		$rows = $newrow->getValue($this);
		if (is_empty($rows)) {
			return null;
		}
		foreach ($rows as $row) {
			switch ($row->getAction()) {
			case Row::ACTION_DELETE:
			case Row::ACTION_UPDATE:
			case Row::ACTION_INSERT:
				return $this->getValueChange($oldrow, $newrow);
			}
		}
		return null;
	}

	protected function getValueChange(Row $oldrow, Row $newrow)
	{
		$change = new RowColumnChange();
		$change->setColumnLabel($this->getLabel());
		$change->setColumnType($this->getType());

		$change->setOldText('-');
		$deleteCount = 0;
		$updateCount = 0;
		$insertCount = 0;
		$rows = $newrow->getValue($this);
		foreach ($rows as $row) {
			switch ($row->getAction()) {
			case Row::ACTION_DELETE:
				$deleteCount++;
				break;
			case Row::ACTION_UPDATE:
				$updateCount++;
				break;
			case Row::ACTION_INSERT:
				$insertCount++;
				break;
			}
		}
		$change->setNewText(
			"追加:" . $insertCount . "件, "
			."更新:" . $updateCount . "件, "
			."削除:" . $deleteCount . "件"
		);
		return $change;
	}

}
class Column__Lookup extends Column
{
	private $_originalReference = null;

	private static $descriptor = array(
		'showsField'=> array('boolean', true),
		'showsLabel'=> array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=> array('integer', null),
		'positionX'=> array('integer', null),
		'sizeWidth'=> array('integer', null),
		'isRequired'=> array('boolean', false),
//		 'referenceTable'=>array('object', null),
		'referenceTableId'=>array('integer', null),
//		 'referenceTargetColumn'=>array('object', null),
		'referenceTargetColumnId'=>array('integer', null),
//		 'referenceSourceColumn'=>array('object', null),
//		 'referenceSourceColumnId'=>array('integer', null),
//		 'referenceColumns' => array('array', array()),
		'referenceColumnIds' => array('array', array()),
//		 'referenceSortColumn'=> array('object', null),
//		 'referenceSortColumnId'=> array('integer', null),
//		 'referenceCopyMap'=> array('array', array()),
		'referenceCopyMapIds'=> array('array', array()),
	);

	public function hasReference()
	{
		return true;
	}
	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
	public function handleGetText($value)
	{
		if ($value === null)
			return null;
		return $value->getValue();
	}
	public function getSearchableOperators()
	{
		if ($this->getIndexType() === Index::STRING) {
			return self::$properties[Column::TEXT]['searchable'];
		} else if ($this->getIndexType() === Index::NUMBER ||
				$this->getIndexType() === Index::INTEGER) {
			return self::$properties[Column::NUMBER]['searchable'];
		} else {
			throw new TxCoreException(
				sprintf('Unexpect index type : %s', $this->getIndexType()),
				TxCoreException::INTERNAL);
		}
	}
	public function handleGetValue($rawValue)
	{
		if ($rawValue === null) {
			return null;
		}
		$json = Util::decodeJSON($rawValue);	// Returns array
		if (!is_object($json)) {
			throw new TxCoreException(
				sprintf('Fail to parse raw value : '.$rawValue),
				TxCoreException::INTERNAL);
		}
		$value = new LookupValue();
		$value->_setKey($json->key);
		$value->_setParent($this);
		$value->setValue($json->value);
		return $value;
	}

	public function handleStoreValue($value, Row $row)
	{
		if ($value === null)
			$value = new LookupValue();

		if (!($value instanceof LookupValue))
			throw new TxCoreException(
				sprintf('Lookup value should be instance of LookupValue'),
				TxCoreException::INTERNAL);

		return Util::encodeJSON(array(
			'key'=>$value->getKey(), 'value'=>$value->getValue()));
	}
	public function handleSearchKey($value)
	{
		$value_ = new LookupValue();
		$value_->setValue($value);
		return $this->handleIndexKey($value_);
	}
	public function handleIndexKey($value)
	{
		if ($value !== null && !($value instanceof LookupValue))
			throw new TxCoreException(
				sprintf('Lookup value should be instance of LookupValue'),
				TxCoreException::INTERNAL);
		return new Index($this->getIndexType(), $value ? $value->getValue() : null);
	}
	public function handleValidate(Row $row)
	{
		$value = $row->getValue($this);

		$errors = array();

		array_push_all($errors, $this->validateRequiredLookup($value));
		return $errors;
	}
	protected function completeLookup($value)
	{
		if (is_not_empty($value) && is_not_empty($value->getKey())) {
			$row = $this->getReferenceTable()->getRowFor($value->getKey());
			$value->setValue($row->getValue($this->getReferenceTargetColumn()));
		}
	}
	protected function validateRequiredLookup($value)
	{
		if ($this->isRequired()) {
			if (is_empty($value) || is_empty($value->getKey()) || is_empty($value->getValue()))
				return array(new Message(":txcore.required", array($this->getLabel()), $this));
		}
		return array();
	}
	/**
	 * @see TxCore.Column::getIndexType()
	 */
	public function getIndexType()
	{
		// 参照先のカラムの型を自分の型にする
		$indexType = null;
		$target = $this->getReferenceTargetColumn();
		if ($target === null) {
			throw new TxCoreException("Lookup has not reference target column.",
				TxCoreException::CIRCULAR_REFERENCE);
		}

		$indexType = $target->getIndexType();
		return $indexType;
	}

	public function handleSearchReferal(Row $row, User $by, Criteria $criteria = null, $offset = 0)
	{
		$value = $row->getValue($this);
		if ($criteria === null) {
			$criteria = new Criteria();
			$criteria->equals($this->getReferenceTargetColumn(), $value ? $value->getValue() : null);
		}

		$criteria->setListColumns(array_merge(array($this->getReferenceTargetColumn()), $this->getReferenceColumns()));
		return $this->getReferenceTable()->search($criteria, $by, $offset, 10);
	}

	private function _getOriginalReferenceSilently()
	{
		if ($this->_originalReference == null) {
			$referenced = $this->getReferenceTargetColumn();
			if ($referenced) {
				$origin = $referenced->_getReferenceTargetColumnRecursively();
				if ($origin) {
					$this->_originalReference = $origin;
				}
			}
		}
		return $this->_originalReference;
	}

	public function getLookupConstraints()
	{

		$reference = null;
		$origin = $this->_getOriginalReferenceSilently();
		if ($origin) {
			$reference = array();
			$reference['type'] = $origin->getType();
			if ($reference['type'] == self::LINK) {
				$reference['linkType'] = $origin->getLinkType();
			}
		}

		return array_merge(
			array('lookupOrigin' => $reference ),
			parent::getLookupConstraints());
	}

	public function getRelationalConstraints()
	{
		$reference = null;
		$origin = $this->_getOriginalReferenceSilently();
		if ($origin) {
			$reference = array();
			$reference['type'] = $origin->getType();
			if ($reference['type'] == self::LINK) {
				$reference['linkType'] = $origin->getLinkType();
			}
		}

		return array_merge(
			array('lookupOrigin' => $reference ),
			parent::getRelationalConstraints());
	}

	public function getReferences()
	{
		if (!$this->hasReference()) {
			throw new TxCoreException(
					sprintf('Column %s dose not support references property', $this->getType()),
					TxCoreException::INTERNAL);
		}

		if ($this->getReferenceTargetColumn() === null) {
			throw new TxCoreException(
				sprintf('No reference column'),
				TxCoreException::INTERNAL);
		}

		$references = array();
		$references[] = new Reference(array($this->getReferenceTargetColumn(), $this));

		$this_ = $this;
		$references = array_merge($references, array_map(function($columns) use ($this_) {
			return new Reference(array($columns[0], $this_));
		}, $this->getReferenceCopyMap()));
		$references = array_merge($references, array_map(function($column) use ($this_) {
			return new Reference(array($column, $this_));
		}, $this->getReferenceColumns()));

		return $references;

	}

	protected function isValueDiff($fromValue, $toValue)
	{
		if (parent::isValueDiff(
			$fromValue ? $fromValue->getValue() : null,
			$toValue ? $toValue->getValue() : null)) {
			return true;
		}
		/* @var fromValue LookupValue */
		/* @var toValue LookupValue */
		$fromId = $fromValue ? $fromValue->getKey() : null;
		$toId = $toValue ? $toValue->getKey() : null;
		return ($fromId !== $toId);
	}

}
// class LookupResult
// {
//	 private $successfully = false;
//	 private $list;
//	 public function _setSuccessful($value)
//	 {
//		 $this->successfully = $value;
//	 }
//	 public function isSuccessful()
//	 {
//		 return $this->successfully ;
//	 }
//	 public function _setList($value)
//	 {
//		 $this->list = $value;
//	 }
//	 public function getSearchResult()
//	 {
//		 return $this->list;
//	 }
// }
class Column__Rownum extends Column
{
	private static $descriptor = array(
		'showsField'=>array('boolean', false),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'sizeWidth'=>array('integer', 1),
	);
	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
	public function handleValidateSearch(Condition $condition)
	{
		$errors = parent::handleValidateSearch($condition);
		if (is_empty($errors)) {
			array_push_all($errors, $this->validateRonum($condition->getValue()));
		}
		return $errors;
	}

	protected function validateRonum($value)
	{
		if (is_not_empty($value) && !is_numeric($value)) {
			return array(new Message(":txcore.invalid_numeric", array($this->getLabel()), $this));
		}
		return array();
	}

	public function handleAutoset(Row $row, HandlerContext $context)
	{
		if ($context->getAction() == 'insert') {
			$id = Engine::factory()->generateRownum($this->parent->getId());
			$row->setValue($this, strval($id));
			$row->setRowNo($id);
		}
	}
}

class Column__CreatedBy extends Column
{
	private static $descriptor = array(
		'showsField'=>array('boolean', false),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'sizeWidth'=>array('integer', 1),
	);
	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}

    /**
     * @param User $value
     * @return string
     * @see Column::handleGetText
     */
	public function handleGetText($value)
	{
		return $value !== null ? $value->getScreenName(): null;
	}
	public function handleSetValue($value, Row $row)
	{
		if ($value != null && !($value instanceof User)) {
			throw new TxCoreException("Create-by value should be User.",
				TxCoreException::INTERNAL);
		}
		return $value;
	}
	public function handleGetValue($rawValue)
	{
		if ($rawValue === null) {
			return null;
		}
		return Engine::factory()->getUser($rawValue, false, true);
	}
	public function handleAutoset(Row $row, HandlerContext $context)
	{
		if ($context->getAction() == 'insert') {
			$row->setValue($this, $context->getBy());
		}
	}

    /**
     * @param User $user
     * @see Column::handleStoreValue
     * @return mixed
     */
	public function handleStoreValue($user, Row $row)
	{
		return $user ? $user->getId() : null;
	}

    /**
     * @param User $user
     * @see Column::handleIndexKey
     * @return Index
     */
	public function handleIndexKey($user)
	{
		return new Index($this->getIndexType(), $user ? $user->getId() : null);
	}

	protected function isValueDiff($fromValue, $toValue)
	{
		/* @var fromValue User */
		/* @var toValue User */
		$fromId = $fromValue ? $fromValue->getId() : null;
		$toId = $toValue ? $toValue->getId() : null;
		return ($fromId !== $toId);
	}
}

class Column__CreatedOn extends Column
{
	private static $descriptor = array(
		'showsField'=>array('boolean', false),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'sizeWidth'=>array('integer', 1),
	);
	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
	public function handleGetValue($value)
	{
		return Util::formatDateTime($value);
	}
	public function handleAutoset(Row $row, HandlerContext $context)
	{
		if ($context->getAction() == 'insert') {
			$row->setValue($this, date('Y-m-d H:i', Engine::factory()->getNow()));
		}
	}
}

class Column__UpdatedBy extends Column__CreatedBy
{
	private static $descriptor = array(
		'showsField'=>array('boolean', false),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'sizeWidth'=>array('integer', 1),
	);
	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
	public function handleAutoset(Row $row, HandlerContext $context)
	{
		if ($context->getAction() == 'insert' ||
			$context->getAction() == 'update') {
			$row->setValue($this, $context->getBy());
		}
	}
	protected function isValueDiff($fromValue, $toValue)
	{
		/* @var fromValue User */
		/* @var toValue User */
		$fromId = $fromValue ? $fromValue->getId() : null;
		$toId = $toValue ? $toValue->getId() : null;
		return ($fromId !== $toId);
	}
}

class Column__UpdatedOn extends Column__CreatedOn
{
	private static $descriptor = array(
		'showsField'=>array('boolean', false),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'sizeWidth'=>array('integer', 1),
	);
	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
	public function handleAutoset(Row $row, HandlerContext $context)
	{
		if ($context->getAction() == 'insert' ||
			$context->getAction() == 'update') {
			$row->setValue($this, date('Y-m-d H:i', Engine::factory()->getNow()));
		}
	}
}
class Column__Label extends Column
{
	private static $descriptor = array(
		'showsField'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'labelHtml'=>array('string', null),
	);
	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
}
class Column__Space extends Column
{
	private static $descriptor = array(
		'showsField'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'sizeHeight'=>array('integer', null),
	);
	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
}
class Column__Line extends Column
{
	private static $descriptor = array(
		'showsField'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
	);
	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
}
class Column__TabGroup extends Column
{
	private static $descriptor = array(
		'showsField'=>array('boolean', true),
		'showsLabel'=>array('boolean', true),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
	);
	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
}
class Column__Tab extends Column
{
	private static $descriptor = array(
		'showsField'=>array('boolean', true),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		//'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
	);
	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}
	protected function validateProperty__LayoutParent()
	{
		$layoutParentId = $this->getProperty('layoutParentId');
		if (is_empty($layoutParentId)) {
			throw new TxCoreException(
				sprintf('empty layoutParentId on column %d.',
				$this->getId()));
		} else {
			try {
				$this->getParent()->getColumnFor($layoutParentId);
			} catch (TxCoreException $e) {
				log_error('Layout parent missing');
				throw $e;
			}
		}
		return array();
	}
}

/**
 * Delegation of column calculate feature.
 */
abstract class Column__Calculable extends Column
{
	/**
	 * @var bool
	 */
	protected $recursive = false;

	public function handleValidateProperties()
	{
		$this->recursive = false;
		$errors = parent::handleValidateProperties();
		if ($this->isCalculable()) {
			$dummyRow = $this->parent->addRow();
			$this->handleCalculate($dummyRow, $errors);
		}
		return $errors;
	}

	/**
	 * Do calcuration with circulation check.
	 *
	 * @param Row $row
	 * @param Column $column
	 * @throws TxCoreException
	 */
	public function handleCalculate(Row $row, &$errors = array())
	{
		// 自分が再度呼ばれた場合再帰としてエラーにする
		if ($this->recursive) {
			throw new TxCoreException("Circular reference calculation.",
				TxCoreException::CIRCULAR_REFERENCE);
		}

		if ($this->getExpression() === null)
			throw new TxCoreException(sprintf("Column %s has not expression.", $this->getType()),
				TxCoreException::INTERNAL);
		$this->recursive = true;
		$ret = $this->evalExpression($row, $this, $errors);
		$this->recursive = false;
		return $ret;
	}

	protected function evalExpression(Row $row, Column $column, &$errors = array())
	{
		$expression = $column->getExpression();
		$vars = $this->getDependents($row, $column, $errors);
		if ($vars === false) {
			log_debug(sprintf('Calculation failed : dependent calculation.'));
			$row->setValue($column, null);
			return false;
		}
		$rpn = $this->getParser($column);
		$ret = $rpn->evaluate($expression, $vars);
		//log_debug('Calculation result : '.$ret);
		$row->setValue($column, empty_to_null($ret));
		return true;
	}

	/**
	 * Retrive operand columns in expression.
	 * @param Row $row
	 */
	protected function getDependents(Row $row, Column $calculable, &$errors = array())
	{
		// 式をパースして変数のみをチェック
		$expression = $calculable->getExpression();
		$parser = $this->getParser($calculable);
		try {
			$tokens = $parser->parse($expression);
		} catch (\TxCore\TxCoreException $e) {
			log_debug($e->getMessage());
			$errors[] = new Message(':txcore.calculable.invalid_formula', array($this->getLabel()));
			return false;
		}

		$vars = array();
		foreach ($tokens as $token) {
			if ($token->type !== RPN::VARIABLE) {
				continue;
			}

			// Codeで検索
			$perands = $calculable->getParent()->getColumns(array('code'=>$token->value));
			if (is_empty($perands)) {
				log_warn(sprintf("Calculation failed : Code %s dose not exist (%s).",
					$token->value, $expression));
				$errors[] = new Message(':txcore.calculable.invalid_code', array($this->getLabel(), $token->value));
				return false;
			}

			$column = $perands[0];

			if (!$column->canBeOperand($calculable->getType())) {
				log_warn(sprintf("Calculation failed : Code %s was unsupported column type %s (%s).",
					$token->value, $column->getType(), $expression));
				$errors[] = new Message(':txcore.calculable.invalid_operand', array($this->getLabel(), $column->getTypeName()));
				return false;
			}

			switch ($column->getType()) {
				case Column::CALC:
					// 計算がオペランドの場合は、先に計算させる（オペランドの計算が失敗した場合は自分も失敗する）
					if (!$column->handleCalculate($row, $errors)) {
						log_warn(sprintf('Calculation failed : Dependent %s caluculation was failed.', $column->getCode()));
						return false;
					}
					$vars[$column->getCode()] = $row->getValue($column);
					break;
				case Column::DATECALC:
					// 日時計算がオペランドの場合は、先に計算させる（オペランドの計算が失敗した場合は自分も失敗する）
					if (!$column->handleCalculate($row, $errors)) {
						log_warn(sprintf('Calculation failed : Dependent %s caluculation was failed.', $column->getCode()));
						return false;
					}
					$vars[$column->getCode()] = $row->getValue($column);
					break;
				case Column::DATE:
				case Column::DATETIME:
				case Column::CREATED_ON:
				case Column::UPDATED_ON:
				case Column::TIME:
					// 日付類は、Unixtimestampに変換して計算させる
					$vars[$column->getCode()] = Util::toUnixTimestamp($row->getValue($column));
					break;
				case Column::CREATED_BY:
				case Column::UPDATED_BY:
					$value = $row->getValue($column);
					if ($value !== null) /* @var $value User */
						$vars[$column->getCode()] = $value->getScreenName();
					break;
				default:
					$vars[$column->getCode()] = $row->getValue($column);
					break;
			}
		}
		return $vars;
	}

	protected function getParser(Column $column)
	{
		$rpn = new RPN();
		if ($column->getType() == Column::CALC || $column->getType() == Column::DATECALC) {
			$rpn->setMathContext(new MathContext($column->getParent()->getRoundScale(), $column->getParent()->getRoundType()));
			$rpn->setType(RPN::MATHMATIC);
		} else if ($column->getType() == Column::TEXT) {
			$rpn->setType(RPN::INTERPOLATE);
		}
		return $rpn;
	}

	public function isCalculable()
	{
		return true;
	}
}

/**
 * @see Column
 */
class Column__Anonymous extends Column
{
	const FORMAT_HOURS = 'hours';
	const FORMAT_DAYS = 'days';
	const FORMAT_DECIMAL = 'decimal';

	private $subtotalFormat;
	public function __construct($id, $label, $subtotalFormat = null)
	{
		$this->id = $id;
		$this->label = $label;
		$this->subtotalFormat = $subtotalFormat;
	}

	protected function getPropertyDescriptor()
	{
		return array();
	}

	public function handleGetValue($value)
	{
		if ($this->subtotalFormat === self::FORMAT_HOURS) {
			return Math::bcround(bcdiv($value, 3600, 10), $this->getParent()->getMathContext());
		} else if ($this->subtotalFormat === self::FORMAT_DAYS) {
			return Math::bcround(bcdiv($value, 3600 * 24, 10), $this->getParent()->getMathContext());
		} else if ($this->subtotalFormat === self::FORMAT_DECIMAL) {
			return Math::bcround($value, $this->getParent()->getMathContext());
		} else {
			return $value;
		}
	}

	public function handleGetText($value)
	{
		if ($this->subtotalFormat === self::FORMAT_HOURS) {
			return $value.'時間';
		} else if ($this->subtotalFormat === self::FORMAT_DAYS) {
			return $value.'日';
		} else {
			return $value;
		}
	}

}

class Column__Datecalc extends Column__Calculable
{
	const FORMAT_DATE = 'date';
	const FORMAT_DATETIME = 'datetime';
	const FORMAT_TIME = 'time';

	private static $descriptor = array(
		'showsField'=>array('boolean', true),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'sizeWidth'=>array('integer', 1),
		'expression'=>array('string', null),
		'dateFormat'=>array('string', null),
		'datecalcUnit'=>array('string', null),
		'datecalcBeforeAfter'=>array('string', null),
		'dateSourceColumnId'=>array('integer', null),
	);

	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}

	public function handleGetText($rawValue)
	{
		switch ($this->getDateFormat()) {
			case self::FORMAT_DATE:
				return Util::formatDate(Util::toInt($rawValue), false);
			case self::FORMAT_DATETIME:
				return Util::formatDateTime(Util::toInt($rawValue), false);
			case self::FORMAT_TIME:
				return Util::formatTime(Util::toInt($rawValue), false);
			default:
				return $rawValue;
		}
	}

	public function isSumable()
	{
		return in_array($this->getDateFormat(), array(self::FORMAT_DATE, self::FORMAT_DATETIME, self::FORMAT_TIME), true);
	}

	public function handleValidateSearch(Condition $condition)
	{
		$value = $condition->getValue();
		$errors = parent::handleValidateSearch($condition);
		if (is_empty($errors)) {
			array_push_all($errors, $this->validateDatecalcSearch($condition->getValue()));
		}
		return $errors;
	}

	private function validateDatecalcSearch($value)
	{
		$errors = array();
		if (is_not_empty($value)) {
			switch ($this->getDateFormat()) {
				case self::FORMAT_DATE:
					if (!Util::isValidDate($value, true)) {
						$errors[] = new Message(':txcore.search.date.format', array());
					}
					break;
				case self::FORMAT_DATETIME:
					if (!Util::isValidDateTime($value, true)) {
						$errors[] = new Message(':txcore.search.datetime.format', array());
					}
					break;
				case self::FORMAT_TIME:
					if (!Util::isValidTime($value, true)) {
						$errors[] = new Message(':txcore.search.time.format', array());
					}
					break;
				default:
			}
		}
		return $errors;
	}

	public function handleValidate(Row $row)
	{
		$value = $row->getValue($this);
		$errors = array();
		array_push_all($errors, $this->validateNumeric($value));
		return $errors;
	}

	public function handleIndexKey($value)
	{
		return new Index($this->getIndexType(), empty_to_null($value));
	}

	public function handleSearchKey($value)
	{
		return $this->handleIndexKey($value);	// 通常は同じ
	}

	public function handleCalculate(Row $row, &$errors = array())
	{
		// 自分が再度呼ばれた場合再帰としてエラーにする
		if ($this->recursive) {
			throw new TxCoreException("Circular reference calculation.",
				TxCoreException::CIRCULAR_REFERENCE);
		}

		if ($this->getExpression() === null)
			throw new TxCoreException(sprintf("Column %s has not expression.", $this->getType()),
				TxCoreException::INTERNAL);

		$this->recursive = true;

		$ret = false;

		$exp = $this->evalExpression($row, $this, $errors);
		//log_debug(gettype($exp));
		if (is_null($exp)||!is_numeric($exp)||!preg_match('/\A[0-9.+-]+\z/', $exp)) {
			$row->setValue($this, null);
		} else {
			$exp = Math::bcround($exp, $this->getParent()->getMathContext());
			$unit = $this->getDatecalcUnit();
			$ba = $this->getDatecalcBeforeAfter();
			$src = $this->getDateSourceColumn();
			if (!$src)
				throw new TxCoreException(sprintf("No reference column"),
					TxCoreException::INTERNAL);
			$code = $src->getCode();
			$ut = $this->codeToUnixtime($row, $src, $code);

			if (!is_null($ut)) {
				$date = DateTimeCalc::create('@' . $ut);
				$date->add($exp, $unit, $ba);
				$value = empty_to_null($date->format('U'));
				$row->setValue($this, $value);
				$ret = !is_null($value);
			} else {
				$row->setValue($this, null);
			}
		}

		$this->recursive = false;

		return $ret;
	}

	protected function codeToUnixtime(Row $row, Column $column, $code)
	{
		$value = null;
		switch ($column->getType()) {
			case Column::DATE:
			case Column::DATETIME:
			case Column::TIME:
			case Column::DATECALC:
			case Column::CREATED_ON:
			case Column::UPDATED_ON:
				// 日付類は、Unixtimestampに変換して計算させる
				$value = Util::toUnixTimestamp($row->getValue($column));
				break;
		}
		return $value;
	}

	protected function evalExpression(Row $row, Column $column, &$errors = array())
	{
		$expression = $column->getExpression();
		$vars = $this->getDependents($row, $column, $errors);
		if ($vars === false) {
			$vars = array();
		}
		$rpn = $this->getParser($column);
		$ret = $rpn->evaluate($expression, $vars);
		return $ret;
	}

	protected function isValueMatch__EQUALS(
		Condition $condition, Row $row)
	{
		$column = $condition->getColumn();
		$index = $column->handleSearchKey($condition->getValue());
		$key = $index->getValue();
		$value = $row->_createIndexKey($column)->getValue();

		if (is_empty($key) && !is_empty($value)) return false;
		if (is_empty($key) && is_empty($value)) return true;
		if (!is_empty($key) && is_empty($value)) return false;

		$dtformat = $column->getDateFormat();
		$key_ = $this->expandDateCalcExpression($key, $dtformat);
		if (is_array($key_)) {
			$unixKey0 = Util::toUnixTimestamp($key_[0]);
			$unixKey1 = Util::toUnixTimestamp($key_[1]);
			return ($unixKey0 <= $value && $value <= $unixKey1);
		} else {
			$unixKey = Util::toUnixTimestamp($key_);
			return $value == $unixKey;
		}

	}

	protected function isValueMatch__GREATER_EQUALS(
		Condition $condition, Row $row)
	{
		if (is_empty($row->getValue($this))) return false;

		$column = $condition->getColumn();
		$index = $column->handleSearchKey($condition->getValue());
		$key = $index->getValue();
		$value = $row->_createIndexKey($column)->getValue();

		if (is_empty($key)) {
			throw new TxCoreException(
				sprintf('null key given for /%s/ evaluation with column %d (%s).', $condition->getOperator(), $column->getId(), $column->getLabel()),
				TxCoreException::INTERNAL);
		}

		$dtformat = $column->getDateFormat();
		$key_ = $this->expandDateCalcExpression($key, $dtformat);
		if (is_array($key_)) {
			$unixKey0 = Util::toUnixTimestamp($key_[0]);
			return ($value >= $unixKey0);
		} else {
			$unixKey = Util::toUnixTimestamp($key_);
			return $value >= $unixKey;
		}

	}

	protected function isValueMatch__LESS_EQUALS(
		Condition $condition, Row $row)
	{
		if (is_empty($row->getValue($this))) return false;

		$column = $condition->getColumn();
		$index = $column->handleSearchKey($condition->getValue());
		$key = $index->getValue();
		$value = $row->_createIndexKey($column)->getValue();

		if (is_empty($key)) {
			throw new TxCoreException(
				sprintf('null key given for /%s/ evaluation with column %d (%s).', $condition->getOperator(), $column->getId(), $column->getLabel()),
				TxCoreException::INTERNAL);
		}

		$dtformat = $column->getDateFormat();
		$key_ = $this->expandDateCalcExpression($key, $dtformat);
		if (is_array($key_)) {
			$unixKey1 = Util::toUnixTimestamp($key_[1]);
			return ($value <= $unixKey1);
		} else {
			$unixKey = Util::toUnixTimestamp($key_);
			return $value <= $unixKey;
		}
	}

	private function expandDateCalcExpression($value, $dtformat)
	{
		$now = Engine::factory()->getNow();
		switch ($dtformat) {
		case 'time':
			$key_ = $value;
			break;
		case 'datetime':
			$key_ = Condition::expandDateTimeExpression($value, $now);
			break;
		case 'date':
			$key_ = Condition::expandDateExpression($value, $now);
			break;
		}
		return $key_;
	}

}


class Column__UserSelector extends Column
{
	private static $descriptor = array(
		'showsField'=>array('boolean', true),
		'showsLabel'=>array('boolean', true),
		'layoutParentId'=>array('integer', null),
		'positionY'=>array('integer', null),
		'positionX'=>array('integer', null),
		'sizeWidth'=>array('integer', 1),
		'isRequired'=>array('boolean', false)
	);

	protected function getPropertyDescriptor()
	{
		return self::$descriptor;
	}

	/**
	 * @param $value
	 * @param Row $row
	 * @return array
	 * @throws TxCoreException
	 */
	public function handleSetValue($value, Row $row)
	{
		if ($value !== null && !is_array($value))
			throw new TxCoreException(
				sprintf('User-selector value should be array of User'),
				TxCoreException::INTERNAL);
		return $value === null ? array() : $value;
	}

	/**
	 * @param Row $row
	 * @return array
	 */
	public function handleValidate(Row $row)
	{
		$value = $row->getValue($this);	/** @var $value User[] */
		$errors = array();
		array_push_all($errors, $this->validateRequired($value));

		$normalized = $this->normalizeUnique($value);
		$row->setValue($this, $normalized);
		return $errors;
	}

	/**
	 * @param User[] $users
	 * @return array
	 */
	public function handleGetText($users)
	{
		if (is_empty($users))
			return array();
		$names = array();
		foreach ($users as $user)
			$names[] = $user->getScreenName();
		return $names;
	}

	public function handleGetValue($jsonValue)
	{
		if (is_empty($jsonValue))
			return array();
		$users = array();
		foreach (Util::decodeJSON($jsonValue) as $userId) {
			$user = Engine::factory()->getUser($userId, false, true);
			if ($user === null) {
				// 消された場合は「削除されした」を代替で出す
				$user = new User();
				$user->_setId($userId);
				$user->setScreenName("*削除されました*");
				$user->_setDeleted(true);
			}
			$users[] = $user;
		}
//		// 並びを正規化
//		usort($users, function(User $a, User $b) {
//			return compare_int($a->getId(), $b->getId());
//		});
		return $users;
	}

	/**
	 * @param User[] $users
	 * @return string
	 */
	public function handleStoreValue($users, Row $row)
	{
		return Util::encodeJSON($users !== null ? array_map(function($user) {  /**@var $user User */
			return $user->getId();
		}, $users) : array());
	}

	/**
	 * @param User[] $users
	 * @return array|Index
	 */
	public function handleIndexKey($users)
	{
		if ($users !== null && is_not_empty($users)) {
			$indexType = $this->getIndexType();
			return array_map(function($user) use ($indexType) {	/** @var $user User */
				return new Index($indexType, is_not_empty($user) ? $user->getId() : null);
			}, $users);
		} else {
			return new Index($this->getIndexType(), null);
		}
	}

	/**
	 * @param Row $row
	 */
	public function handleDefaultValue(Row $row)
	{
        $defaultValue = array();
        $intent = Engine::factory()->getIntent();
        if ($intent !== null && $intent->getUser() !== null) {
            $user = Engine::factory()->getUser($intent->getUser(), false, true);
            if ($user !== null) {
                $defaultValue[] = $user;
            }
        }
        $row->setValue($this, $defaultValue);
	}

	/**
	 * @param User $users
	 * @return Index
	 */
	public function handleSearchKey($users)
	{
		return new Index($this->getIndexType(), is_not_empty($users) ? $users->getId() : null);
//		return new Index($this->getIndexType(), is_not_empty($user) ? $user->getId() : null);
	}

	/**
	 * @param User[] $value
	 * @return User[]
	 */
	private function normalizeUnique($value)
	{
		$map = array();
		$uniqueUsers = array();
		foreach ($value as $user) {
			if (!isset($map[$user->getId()])) {
				$uniqueUsers[] = $user;
				$map[$user->getId()] = true;
			}
		}
		return $uniqueUsers;
	}

	/**
	 * @param Condition $condition
	 */
	public function handleSearchRewrite(Condition $condition)
	{
		$users = array();
		foreach ($condition->getValue() as $entity) {
			if ($entity instanceof User) {
				$users[$entity->getId()] = $entity;
			} else if ($entity instanceof Group) {
				$usersInGroup = Engine::factory()->getGroupUsers($entity);
				foreach ($usersInGroup as $user) {
					$users[$user->getId()] = $user;
				}
			}
		}

		// ユーザーなしのグループが指定された場合、クエリー生成で落ちるのでダミーのユーザーを作る
		if (is_empty($users)) {
			$dummy = new User();
			$dummy->_setId(-1);
			$users[] = $dummy;
		}

		$condition->_setValue(array_values($users));
	}

	protected function isValueDiff($fromValue, $toValue)
	{
		/* @var fromValue User[] */
		/* @var toValue User[] */
		$fromIds = array();
		if (count($fromValue) != count($toValue)) return true;
		foreach ($fromValue as $user) {
			$fromIds[] = $user->getId();
		}
		foreach ($toValue as $user) {
			if (!in_array($user->getId(), $fromIds)) {
				return true;
			}
		}
		return false;
	}

	protected function formatValueChangeText(Row $row)
	{
		return implode("\n", $row->getText($this));
	}

}

class Column__AggregateType extends Column
{
	public function __construct($id, $label)
	{
		$this->id = $id;
		$this->label = $label;
	}

	protected function getPropertyDescriptor()
	{
		return array();
	}
}
