<?php

namespace TxCore;

class NotificationConditionalRule extends NotificationRule
{

    protected $criteria;
    protected $message;

    protected $recipientsCache = null;

    public function getCriteria()
    {
        return $this->criteria;
    }
    public function setCriteria($value)
    {
        $this->criteria = $value;
    }
    public function getMessage()
    {
        return $this->message;
    }
    public function setMessage($value)
    {
        $this->message = $value;
    }

    /**
     * @return Message[]
     */
    public function validate()
    {
        $errors = parent::validate();
        if (count($errors)) return $errors;

        if (is_empty($this->getMessage())) {
            return array(new Message(
                ':table_modify.notification.message.required', array(), $this));
        }

        if (count($this->getCriteria()->getConditions()) == 0) {
            return array(new Message(
                ':table_modify.notification.conditions.required', array(), $this));
        }

        $errors = $this->getCriteria()->validate();
        if (count($errors)) return $errors;

        return array();
    }

    public function matches(Row $newrow, Row $oldrow = null)
    {
        $criteria = $this->getCriteria();

        if ($oldrow) {
            if ($criteria->matches($oldrow)) {
                return false; // already matched.
            }
        }
        return $criteria->matches($newrow);
    }

    public function getRecipients(User $by, Row $row)
    {
        $cache = $this->getRecipientsCache();
        $recipients = $cache['users'];
        $columns = $cache['columns'];
        $users = array();

        // extract users from column values
        foreach ($columns as $column) {
            $value = $row->getValue($column);
            if (!is_array($value)) {
                $value = array($value);
            }
            foreach ($value as $user) {
                if ($user !== null && !$user->_isDeleted()) {
                    $users[$user->getId()] = $user;
                }
            }
        }

        $table = $row->getParent();
        foreach ($users as $user) {
            if (!array_key_exists($user->getId(), $recipients)) {
                if ($this->getParent()->allows('select', $user)) {
                    $recipients[$user->getId()] = $user;
                }
            }
        }

        if (isset($recipients[$by->getId()])) {
            unset($recipients[$by->getId()]);
        }

        return $recipients;

    }

    private function getRecipientsCache()
    {
        if ($this->recipientsCache === null) {
            $this->recipientsCache = $this->createRecipientsCache();
        }
        return $this->recipientsCache;
    }

    private function createRecipientsCache()
    {
        $cache = array('users'=>array(), 'columns'=>array());

        $users = array();
        $columns = array();
        $groups = array();
        $usersInGroups = array();

        // extract users and groups and columns
        /* @var $this TxCore\NotificationBasicRule */
        if($this->getTargetingType() == self::TARGETING_USERGROUP) {
            foreach ($this->getTargets() as $entity) {
                /* @var $entity User|Group */
                if ($entity instanceof User) {
                    $users[$entity->getId()] = $entity;
                } else if ($entity instanceof Group) {
                    $groups[$entity->getId()] = $entity;
                    $usersInGroups[$entity->getId()] = array();
                }
            }
        } else if($this->getTargetingType() == self::TARGETING_COLUMN) {
            foreach ($this->getTargets() as $entity) {
                /* @var $entity Column */
                $columns[$entity->getId()] = $entity;
            }
        }

        // extract users from groups
        foreach ($groups as $id => $entity) {
            $usersInGroup = Engine::factory()->getGroupUsers($entity);
            foreach ($usersInGroup as $user) {
                $users[$user->getId()] = $user;
            }
        }

        foreach ($users as $user) {
            if ($this->getParent()->allows('select', $user)) {
                $cache['users'][$user->getId()] = $user;
            }
        }
        foreach ($columns as $column) {
            $cache['columns'][$column->getId()] = $column;
        }

        return $cache;
    }

}

