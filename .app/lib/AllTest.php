<?php


// define('PATH', '/Users/kenji/Projects/barge/lib/simpletest/');
// ini_set('include_path',get_include_path().PATH_SEPARATOR.PATH);
// require_once('/Users/kenji/Repos/TxDB/.app/test-loader.php');
// require_once('autorun.php');

// define('PATH', '/usr/local/simpletest/');
// ini_set('include_path',get_include_path().PATH_SEPARATOR.PATH);
// require_once('/usr/local/pleiades/xampp/htdocs/txdb/.app/loader-ut.php');
// require_once('autorun.php');



class AllTest extends \TestSuite 
{
	public function __construct() 
	{
		parent::__construct();
// 		$this->addFile(dirname(__FILE__).'/file_test.php');
		$this->addFile(dirname(__FILE__).'/JSON/Test/JSONTest.php');
		$this->addFile(dirname(__FILE__).'/TxCore/Math/Test/RPNTest.php');
		$this->addFile(dirname(__FILE__).'/TxCore/Math/Test/MathTest.php');
		$this->addFile(dirname(__FILE__).'/TxCore/Test/EngineTest.php');
		$this->addFile(dirname(__FILE__).'/TxCore/Test/RowTest.php');
		$this->addFile(dirname(__FILE__).'/TxCore/Test/ColumnTest.php');
		$this->addFile(dirname(__FILE__).'/TxCore/Test/IconTest.php');
		$this->addFile(dirname(__FILE__).'/TxCore/Test/TableTest.php');
		$this->addFile(dirname(__FILE__).'/TxCore/Test/Table_searchTest.php');
		$this->addFile(dirname(__FILE__).'/TxCore/Test/FileTest.php');
		$this->addFile(dirname(__FILE__).'/TxCore/Test/Engine_updateTableRowTest.php');
		$this->addFile(dirname(__FILE__).'/TxCore/Test/Engine_WorkTest.php');
// 		$this->addFile(dirname(__FILE__).'/TxCore/Test/ApiTest.php');
	}
}
