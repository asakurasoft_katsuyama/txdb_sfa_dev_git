--
-- 標準テンプレート: 空のDB
-- 注１
--     table_def.id, column_def.id, option.id は sequence.name='object_id'のsequence.current
--     で発番されるため、sequence.currentには、それらの最大値以上の値をセットする必要があります。
-- 注２
--     以下のようにSQLで直接table_defを登録する場合は、
--     icon_filekeyのファイル名のアイコン画像を、/asset/icons/配下に別途設置する必要があります。
--

INSERT INTO `column_def` (`id`, `revision`, `table_id`, `column_type`, `code`, `label`, `properties`) VALUES (2,1,1,'rownum','データ番号','データ番号','{\"showsField\":false,\"showsLabel\":true,\"positionY\":null,\"positionX\":null,\"sizeWidth\":1}');
INSERT INTO `column_def` (`id`, `revision`, `table_id`, `column_type`, `code`, `label`, `properties`) VALUES (2,2,1,'rownum','データ番号','データ番号','{\"showsField\":false,\"showsLabel\":true,\"positionY\":null,\"positionX\":null,\"sizeWidth\":1}');
INSERT INTO `column_def` (`id`, `revision`, `table_id`, `column_type`, `code`, `label`, `properties`) VALUES (3,1,1,'created_by','作成者','作成者','{\"showsField\":false,\"showsLabel\":true,\"positionY\":null,\"positionX\":null,\"sizeWidth\":1}');
INSERT INTO `column_def` (`id`, `revision`, `table_id`, `column_type`, `code`, `label`, `properties`) VALUES (3,2,1,'created_by','作成者','作成者','{\"showsField\":false,\"showsLabel\":true,\"positionY\":null,\"positionX\":null,\"sizeWidth\":1}');
INSERT INTO `column_def` (`id`, `revision`, `table_id`, `column_type`, `code`, `label`, `properties`) VALUES (4,1,1,'created_on','作成日時','作成日時','{\"showsField\":false,\"showsLabel\":true,\"positionY\":null,\"positionX\":null,\"sizeWidth\":1}');
INSERT INTO `column_def` (`id`, `revision`, `table_id`, `column_type`, `code`, `label`, `properties`) VALUES (4,2,1,'created_on','作成日時','作成日時','{\"showsField\":false,\"showsLabel\":true,\"positionY\":null,\"positionX\":null,\"sizeWidth\":1}');
INSERT INTO `column_def` (`id`, `revision`, `table_id`, `column_type`, `code`, `label`, `properties`) VALUES (5,1,1,'updated_by','更新者','更新者','{\"showsField\":false,\"showsLabel\":true,\"positionY\":null,\"positionX\":null,\"sizeWidth\":1}');
INSERT INTO `column_def` (`id`, `revision`, `table_id`, `column_type`, `code`, `label`, `properties`) VALUES (5,2,1,'updated_by','更新者','更新者','{\"showsField\":false,\"showsLabel\":true,\"positionY\":null,\"positionX\":null,\"sizeWidth\":1}');
INSERT INTO `column_def` (`id`, `revision`, `table_id`, `column_type`, `code`, `label`, `properties`) VALUES (6,1,1,'updated_on','更新日時','更新日時','{\"showsField\":false,\"showsLabel\":true,\"positionY\":null,\"positionX\":null,\"sizeWidth\":1}');
INSERT INTO `column_def` (`id`, `revision`, `table_id`, `column_type`, `code`, `label`, `properties`) VALUES (6,2,1,'updated_on','更新日時','更新日時','{\"showsField\":false,\"showsLabel\":true,\"positionY\":null,\"positionX\":null,\"sizeWidth\":1}');

INSERT INTO `release_control` (`table_id`, `trunk_revision`, `release_revision`, `has_pending_release`) VALUES (1,2,1,0);
INSERT INTO `revision_no` (`table_id`, `revision_no`) VALUES (1,2);
INSERT INTO `row_no` (`table_id`, `row_no`) VALUES (1,0);

INSERT INTO `table_def` (`id`, `revision`, `name`, `description`, `icon_filekey`, `is_public`, `is_template`, `is_default`, `is_api_enabled`, `api_key`, `round_type`, `round_scale`, `owner`) VALUES (1,1,'空のDB',NULL,'bd544992fa2e598f0d1a3be9d18cde899aa9b359.png',0,1,1,0,'e329ab105c821573f63dd6854b3646c18d468342','default',4,1);
INSERT INTO `table_def` (`id`, `revision`, `name`, `description`, `icon_filekey`, `is_public`, `is_template`, `is_default`, `is_api_enabled`, `api_key`, `round_type`, `round_scale`, `owner`) VALUES (1,2,'空のDB',NULL,'bd544992fa2e598f0d1a3be9d18cde899aa9b359.png',0,0,NULL,0,'e329ab105c821573f63dd6854b3646c18d468342','default',4,1);

UPDATE `sequence` SET `current` = 6 WHERE `name` = 'object_id';

