--
-- データベース: `txdb`
--

DELIMITER $$
--
-- 関数
--
DROP FUNCTION IF EXISTS `get_new_object_id`$$
CREATE FUNCTION `get_new_object_id`() RETURNS int(11)
BEGIN
    RETURN nextval('object_id');
END$$

DROP FUNCTION IF EXISTS `get_new_revision_no`$$
CREATE FUNCTION `get_new_revision_no`(_table_id INT) RETURNS int(11)
BEGIN
DECLARE max INT(11);
  SELECT revision_no into max FROM revision_no WHERE table_id = _table_id FOR UPDATE;
  SET max := max + 1;
  UPDATE revision_no SET revision_no = max where table_id = _table_id;
  RETURN max;
END$$

DROP FUNCTION IF EXISTS `get_new_row_no`$$
CREATE FUNCTION `get_new_row_no`(_table_id INT) RETURNS bigint(20)
BEGIN
DECLARE max bigint(20);
  SELECT row_no into max FROM row_no WHERE table_id = _table_id FOR UPDATE;
  SET max := max + 1;
  UPDATE row_no SET row_no = max where table_id = _table_id;
  RETURN max;
END$$

DROP FUNCTION IF EXISTS `nextval`$$
CREATE FUNCTION `nextval`(seq_name VARCHAR(45)) RETURNS int(11)
BEGIN
DECLARE max INT(11);
  SELECT current INTO max FROM sequence WHERE name = seq_name FOR UPDATE;
  SET max := max + 1;
  UPDATE sequence SET current = max WHERE name = seq_name;
  RETURN max;
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- テーブルの構造 `column_def`
--

DROP TABLE IF EXISTS `column_def`;
CREATE TABLE IF NOT EXISTS `column_def` (
  `id` int(11) NOT NULL,
  `revision` int(11) NOT NULL COMMENT 'table_defのrevisionと同じ値',
  `table_id` int(11) DEFAULT NULL,
  `column_type` varchar(45) DEFAULT NULL COMMENT 'カラムの種別（例：text, multitext）',
  `code` varchar(64) DEFAULT NULL COMMENT '数式やAPIで参照するための検索キー（マルチバイト可、ただし使用できるASCII文字はアンダースコアのみ）',
  `label` varchar(128) DEFAULT NULL COMMENT 'カラムの日本語名',
  `properties` longtext COMMENT 'カラムの設定内容（JSON形式）',
  PRIMARY KEY (`id`,`revision`),
  KEY `TAB_REV` (`table_id`,`revision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT 'カラムの定義（詳細はColumn.phpを参照）';

-- --------------------------------------------------------

--
-- テーブルの構造 `data`
--

DROP TABLE IF EXISTS `data`;
CREATE TABLE IF NOT EXISTS `data` (
  `row_id` bigint(20) NOT NULL,
  `column_id` int(11) NOT NULL,
  `table_id` int(11) DEFAULT NULL,
  `value` longtext CHARACTER SET utf8 COMMENT '値（カラムによっては、JSON形式になる場合もある）',
  PRIMARY KEY (`row_id`,`column_id`),
  KEY `COL` (`column_id`),
  KEY `TAB` (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT 'ROW（レコード）の各カラムごとの値';

-- --------------------------------------------------------

--
-- テーブルの構造 `filter`
--

DROP TABLE IF EXISTS `filter`;
CREATE TABLE IF NOT EXISTS `filter` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `table_id` int(11) DEFAULT NULL,
  `name` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '抽出条件名',
  `expression` longtext CHARACTER SET utf8 COMMENT '絞り込み条件とソート条件（JSON形式）',
  `created_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT '保存された抽出条件設定';

-- --------------------------------------------------------

--
-- テーブルの構造 `icon`
--

DROP TABLE IF EXISTS `icon`;
CREATE TABLE IF NOT EXISTS `icon` (
  `id` int(11),
  `filekey` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT '画像ファイル名',
  `seq` int(11) DEFAULT NULL COMMENT '表示順',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT 'プリセットアイコン';

-- --------------------------------------------------------

--
-- テーブルの構造 `indexes`
--

DROP TABLE IF EXISTS `indexes`;
CREATE TABLE IF NOT EXISTS `indexes` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `table_id` int(11) DEFAULT NULL,
  `column_id` int(11) DEFAULT NULL,
  `string_value` mediumtext COLLATE utf8_bin COMMENT '文字列値インデックス',
  `integer_value` bigint(20) DEFAULT NULL COMMENT '整数インデックス',
  `number_value` decimal(30,10) DEFAULT NULL COMMENT'数値インデックス',
  `date_value` date DEFAULT NULL COMMENT '日付インデックス',
  `time_value` time DEFAULT NULL COMMENT '時刻インデックス',
  `datetime_value` datetime DEFAULT NULL COMMENT '日時インデックス',
  `row_id` bigint(20) DEFAULT NULL COMMENT '検索結果となるid',
  PRIMARY KEY (`id`),
  KEY `COL_INT` (`column_id`,`integer_value`),
  KEY `COL_DATE` (`column_id`,`date_value`),
  KEY `COL_TIME` (`column_id`,`time_value`),
  KEY `COL_DATETIME` (`column_id`,`datetime_value`),
  KEY `COL` (`column_id`),
  KEY `COL_ROW` (`column_id`,`row_id`),
  KEY `COL_NUM` (`column_id`,`number_value`),
  KEY `ROW` (`row_id`),
  KEY `TAB` (`table_id`),
  KEY `COL_STR` (`column_id`,`string_value`(255))
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT 'ROW（レコード）を検索するためのインデックス';

-- --------------------------------------------------------

--
-- テーブルの構造 `option`
--

DROP TABLE IF EXISTS `option`;
CREATE TABLE IF NOT EXISTS `option` (
  `id` int(11) NOT NULL,
  `revision` int(11) NOT NULL COMMENT 'table_defのrevisionと同じ値',
  `table_id` int(11) DEFAULT NULL,
  `column_id` int(11) DEFAULT NULL,
  `name` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT '選択肢名',
  `seq` int(11) DEFAULT NULL COMMENT '表示順',
  PRIMARY KEY (`id`,`revision`),
  KEY `COL_REV` (`column_id`,`revision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='ラジオボタン、チェックボックス、ドロップダウン用の選択肢';

-- --------------------------------------------------------

--
-- テーブルの構造 `reference`
--

DROP TABLE IF EXISTS `reference`;
CREATE TABLE IF NOT EXISTS `reference` (
  `column_id` int(11) NOT NULL COMMENT '参照元カラム',
  `revision` int(11) NOT NULL COMMENT '参照元リビジョン',
  `no` int(11) NOT NULL COMMENT 'PK用の連番',
  `table_id` int(11) DEFAULT NULL COMMENT '参照元テーブル',
  `target_column_id` int(11) DEFAULT NULL COMMENT '参照先カラム',
  `target_table_id` int(11) DEFAULT NULL COMMENT '参照先テーブル',
  PRIMARY KEY (`column_id`,`revision`,`no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT '参照制約';

-- --------------------------------------------------------

--
-- テーブルの構造 `release_control`
--

DROP TABLE IF EXISTS `release_control`;
CREATE TABLE IF NOT EXISTS `release_control` (
  `table_id` int(11) NOT NULL,
  `trunk_revision` int(11) DEFAULT NULL COMMENT 'デザイン変更中のリビジョン',
  `release_revision` int(11) DEFAULT NULL COMMENT 'リリース済みのリビジョン',
  `has_pending_release` tinyint(1) DEFAULT NULL COMMENT '適用されていない変更があるかどうか',
  PRIMARY KEY (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT 'テーブル変更のリリース管理のための情報';

-- --------------------------------------------------------

--
-- テーブルの構造 `revision_no`
--

DROP TABLE IF EXISTS `revision_no`;
CREATE TABLE IF NOT EXISTS `revision_no` (
  `table_id` int(11) NOT NULL,
  `revision_no` int(11) DEFAULT NULL COMMENT '最後に発番された番号',
  PRIMARY KEY (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='リビジョン番号の発番テーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `row`
--

DROP TABLE IF EXISTS `row`;
CREATE TABLE IF NOT EXISTS `row` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `table_id` int(11) DEFAULT NULL,
  `row_no` bigint(20) DEFAULT NULL COMMENT 'このレコードのrownum（レコード番号）カラムの値と同じ値',
  `version` int(11) DEFAULT NULL COMMENT '変更された回数',
  PRIMARY KEY (`id`),
  KEY `TAB` (`table_id`),
  KEY `TAB_ROW` (`table_id`,`row_no`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT 'ROW（レコード）';

-- --------------------------------------------------------

--
-- テーブルの構造 `row_no`
--

DROP TABLE IF EXISTS `row_no`;
CREATE TABLE IF NOT EXISTS `row_no` (
  `table_id` int(11) NOT NULL,
  `row_no` bigint(20) DEFAULT NULL COMMENT '最後に発番された番号',
  PRIMARY KEY (`table_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT 'レコード番号（テーブル内の連番）発番用テーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `sequence`
--

DROP TABLE IF EXISTS `sequence`;
CREATE TABLE IF NOT EXISTS `sequence` (
  `name` varchar(45) CHARACTER SET utf8 NOT NULL,
  `current` int(11) DEFAULT NULL COMMENT '最後に発番された番号',
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT '単純な連番発番用テーブル';

-- --------------------------------------------------------

--
-- テーブルの構造 `table_def`
--

DROP TABLE IF EXISTS `table_def`;
CREATE TABLE IF NOT EXISTS `table_def` (
  `id` int(11) NOT NULL,
  `revision` int(11) NOT NULL,
  `name` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT 'テーブル名',
  `description` mediumtext CHARACTER SET utf8 COMMENT 'テーブルの説明文（今のところ画面項目として設けられていない）',
  `icon_filekey` varchar(64) CHARACTER SET utf8 DEFAULT NULL COMMENT 'アイコンファイル名',
  `is_public` tinyint(1) DEFAULT NULL COMMENT '公開状態（1:公開）',
  `is_template` tinyint(1) DEFAULT NULL COMMENT '標準テンプレートかどうか（1: yes）',
  `is_default` tinyint(1) DEFAULT NULL COMMENT '標準テンプレートのうちデフォルトかどうか（1: yes）',
  `is_api_enabled` tinyint(1) DEFAULT NULL COMMENT 'APIの有効状態（1:有効）',
  `api_key` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT 'APIの認証キー',
  `is_write_api_enabled` tinyint(1) DEFAULT 0 COMMENT '書き込み用APIの有効状態（1:有効）',
  `write_api_key` varchar(128) CHARACTER SET utf8 DEFAULT NULL COMMENT '書き込み用APIの認証キー',
  `round_type` varchar(45) CHARACTER SET utf8 DEFAULT NULL COMMENT 'まるめの種類（default: 最近接偶数への丸め、roundhalfup: 四捨五入、roundup:切り上げ、rounddown:切り捨て）',
  `round_scale` int(11) DEFAULT NULL COMMENT 'まるめの際の小数部分の桁数',
  `owner` int(11) DEFAULT NULL COMMENT 'テーブルの作成者',
  `permission_control` tinyint(1) DEFAULT 0 COMMENT 'アクセス制限の状態（0:制限しない）',
  `is_comment_following_enabled` tinyint(1) DEFAULT 0 COMMENT 'コメントの継続通知（1:有効）',
  `is_comment_enabled` tinyint(1) DEFAULT 0 COMMENT 'コメント機能の有効状態（1:有効）',
  `is_calendar_enabled` tinyint(1) DEFAULT 0 COMMENT 'カレンダー表示の有効状態（1:有効）',
  `is_read_check_enabled` tinyint(1) DEFAULT 0 COMMENT '回覧板の有効状態（1:有効）',
  `is_six_columns_enabled` tinyint(1) DEFAULT 0 COMMENT '6列編集の有効状態（1:有効）',
  PRIMARY KEY (`id`,`revision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT 'テーブル定義';

-- --------------------------------------------------------

--
-- テーブルの構造 `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(45) CHARACTER SET utf8 NOT NULL COMMENT 'user: 一般, admin: 管理者',
  `login_id` varchar(64) COLLATE utf8_bin NOT NULL COMMENT 'ログインid',
  `password` varchar(64) COLLATE utf8_bin NOT NULL COMMENT 'パスワード',
  `screen_name` varchar(64) CHARACTER SET utf8 NOT NULL COMMENT 'ユーザー名',
  `profile` mediumtext CHARACTER SET utf8 COMMENT '所属等',
  `created_on` datetime DEFAULT NULL COMMENT '登録日時',
  `updated_on` datetime DEFAULT NULL COMMENT '更新日時',
  `seq` int(11) DEFAULT NULL COMMENT '表示順',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT 'ユーザー';

-- --------------------------------------------------------

--
-- テーブルの構造 `view_column`
--

DROP TABLE IF EXISTS `view_column`;
CREATE TABLE IF NOT EXISTS `view_column` (
  `table_id` int(11) NOT NULL,
  `revision` int(11) NOT NULL,
  `column_id` int(11) NOT NULL COMMENT '表示カラム',
  `seq` int(11) DEFAULT NULL COMMENT '表示順',
  `width` int(11) DEFAULT NULL COMMENT '表示幅',
  PRIMARY KEY (`table_id`,`column_id`,`revision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT 'レコード一覧画面での表示列';

-- --------------------------------------------------------

--
-- テーブルの構造 `permission`
--
DROP TABLE IF EXISTS `permission`;
CREATE TABLE IF NOT EXISTS `permission` (
  `table_id` int(11) NOT NULL,
  `revision` int(11) NOT NULL,
  `user_id` int(11) NOT NULL COMMENT '登録ユーザ',
  `is_readable` tinyint(1) DEFAULT 0 COMMENT '閲覧権限（1:有効）',
  `is_writable` tinyint(1) DEFAULT 0 COMMENT '編集権限（1:有効）',
  PRIMARY KEY (`table_id`,`user_id`,`revision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT 'レコードの操作権限';

-- --------------------------------------------------------

--
-- テーブルの構造 `group`
--
DROP TABLE IF EXISTS `group`;
CREATE TABLE IF NOT EXISTS `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(64) CHARACTER SET utf8 NOT NULL COMMENT 'グループ名',
  `created_on` datetime DEFAULT NULL COMMENT '登録日時',
  `updated_on` datetime DEFAULT NULL COMMENT '更新日時',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='グループ';

-- --------------------------------------------------------

--
-- テーブルの構造 `user_group`
--
DROP TABLE IF EXISTS `user_group`;
CREATE TABLE IF NOT EXISTS `user_group` (
  `user_id` int(11) NOT NULL COMMENT 'ユーザーID',
  `group_id` int(11) NOT NULL COMMENT 'グループID',
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='ユーザーグループ';

-- --------------------------------------------------------

--
-- テーブルの構造 `comment`
--
DROP TABLE IF EXISTS `comment`;
CREATE TABLE IF NOT EXISTS `comment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `no` int(11) DEFAULT NULL COMMENT 'No.',
  `table_id` int(11) DEFAULT NULL COMMENT 'テーブルID',
  `row_id` int(11) DEFAULT NULL COMMENT '行ID',
  `text` mediumtext COMMENT '本文',
  `posted_time` datetime DEFAULT NULL COMMENT '投稿日時',
  `posted_user_id` int(11) DEFAULT NULL COMMENT '投稿者ID',
  `notification_targets` mediumtext COMMENT '通知先（JSON配列）',
  PRIMARY KEY (`id`),
  KEY `TID` (`table_id`),
  KEY `RID` (`row_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='コメント';

-- --------------------------------------------------------

--
-- テーブルの構造 `notification_basic_rule`
--

DROP TABLE IF EXISTS `notification_basic_rule`;
CREATE TABLE IF NOT EXISTS `notification_basic_rule` (
  `table_id` int(11) NOT NULL,
  `revision` int(11) NOT NULL,
  `no` int(11) NOT NULL COMMENT '連番(兼 表示順)',
  `targeting_type` varchar(16) NOT NULL COMMENT '通知先種別 user_group/column',
  `targets` mediumtext COMMENT '通知先（JSON）',
  `is_on_row_insert` tinyint(1) DEFAULT 0 COMMENT 'データ登録通知（1:有効）',
  `is_on_row_update` tinyint(1) DEFAULT 0 COMMENT 'データ更新通知（1:有効）',
  `is_on_comment_insert` tinyint(1) DEFAULT 0 COMMENT 'コメント追加通知（1:有効）',
  PRIMARY KEY (`table_id`,`revision`,`no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT '基本通知';

-- --------------------------------------------------------

--
-- テーブルの構造 `notification_conditional_rule`
--

DROP TABLE IF EXISTS `notification_conditional_rule`;
CREATE TABLE IF NOT EXISTS `notification_conditional_rule` (
  `table_id` int(11) NOT NULL,
  `revision` int(11) NOT NULL,
  `no` int(11) NOT NULL COMMENT '連番(兼 表示順)',
  `targeting_type` varchar(16) NOT NULL COMMENT '通知先種別 user_group/column',
  `targets` mediumtext COMMENT '通知先（JSON）',
  `expression` longtext CHARACTER SET utf8 COMMENT '通知の判定条件。（JSON）',
  `message` text DEFAULT NULL COMMENT 'ユーザー指定のメッセージ',
  PRIMARY KEY (`table_id`,`revision`,`no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT '条件通知';

-- --------------------------------------------------------

--
-- テーブルの構造 `notification_view_column`
--

DROP TABLE IF EXISTS `notification_view_column`;
CREATE TABLE IF NOT EXISTS `notification_view_column` (
  `table_id` int(11) NOT NULL,
  `revision` int(11) NOT NULL,
  `column_id` int(11) NOT NULL COMMENT '表示カラム',
  `seq` int(11) DEFAULT NULL COMMENT '表示順',
  PRIMARY KEY (`table_id`,`column_id`,`revision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT '通知表示で参照されるカラム';

-- --------------------------------------------------------

--
-- テーブルの構造 `notification`
--

DROP TABLE IF EXISTS `notification`;
CREATE TABLE IF NOT EXISTS `notification` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `notifier_id` int(11) NOT NULL COMMENT '通知元ユーザー',
  `notified_on` datetime DEFAULT NULL COMMENT '通知日時',
  `message` text DEFAULT NULL COMMENT 'メッセージ',
  `changes` longtext CHARACTER SET utf8 DEFAULT NULL COMMENT '変更内容(JSON形式)',
  `table_id` int(11) DEFAULT NULL,
  `row_id` bigint(20) DEFAULT NULL,
  `comment_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `TID` (`table_id`),
  KEY `RID` (`row_id`),
  KEY `CID` (`comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT '通知';

-- --------------------------------------------------------

--
-- テーブルの構造 `notification_receipt`
--

DROP TABLE IF EXISTS `notification_receipt`;
CREATE TABLE IF NOT EXISTS `notification_receipt` (
  `notification_id` bigint(20) NOT NULL,
  `recipient_id` int(11) NOT NULL COMMENT '通知先ユーザー',
  `is_read` tinyint(1) DEFAULT 0 COMMENT '既読状態（1:既読）',
  `notified_on` datetime DEFAULT NULL COMMENT '通知日時',
  `table_id` int(11) DEFAULT NULL,
  `row_id` bigint(20) DEFAULT NULL,
  `comment_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`notification_id`,`recipient_id`),
  KEY `RCP_RED_NTO_NID` (`recipient_id`,`is_read`,`notified_on`,`notification_id`),
  KEY `TID` (`table_id`),
  KEY `RID` (`row_id`),
  KEY `CID` (`comment_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT '通知受け取り';



-- --------------------------------------------------------

--
-- テーブルの構造 `view_calendar_setting`
--
DROP TABLE IF EXISTS `view_calendar_setting`;
CREATE TABLE `view_calendar_setting` (
  `table_id` int(11) NOT NULL,
  `revision` int(11) NOT NULL,
  `date_column_id` int(11) DEFAULT NULL COMMENT '日付に対応するカラム',
  `user_column_id` int(11) DEFAULT NULL COMMENT 'ユーザーに対応するカラム',
  `start_time_column_id` int(11) DEFAULT NULL COMMENT '開始時刻に対応するカラム',
  `end_time_column_id` int(11) DEFAULT NULL COMMENT '終了時刻に対応するカラム',
  `title_column_id` int(11) DEFAULT NULL COMMENT 'タイトル表示するカラム',
  PRIMARY KEY (`table_id`,`revision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `relationship`
--

DROP TABLE IF EXISTS `relationship`;
CREATE TABLE IF NOT EXISTS `relationship` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `relationship_type` varchar(45) NOT NULL COMMENT '関連の種類（relational_input:明細入力）',
  `source_row_id` bigint(20) NOT NULL COMMENT '参照元レコード',
  `target_row_id` bigint(20) NOT NULL COMMENT '参照先レコード',
  `source_table_id` int(11) DEFAULT NULL COMMENT '参照元テーブル',
  `target_table_id` int(11) DEFAULT NULL COMMENT '参照先テーブル',
  `source_column_id` int(11) DEFAULT NULL COMMENT '参照元カラム',
  PRIMARY KEY (`id`),
  KEY `SRI_SCI` (`source_row_id`,`source_column_id`),
  KEY `SRI_TRI_SCI` (`source_row_id`,`target_row_id`,`source_column_id`),
  KEY `SCI` (`source_column_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT 'レコード間の関連';

-- --------------------------------------------------------

--
-- テーブルの構造 `read_check_setting`
--
DROP TABLE IF EXISTS `read_check_setting`;
CREATE TABLE `read_check_setting` (
  `table_id` int(11) NOT NULL,
  `revision` int(11) NOT NULL,
  `is_autoread_enabled` tinyint(1) DEFAULT 0 COMMENT '自動で既読にするか（1:自動）',
  `is_onlist_enabled` tinyint(1) DEFAULT 0 COMMENT 'リストから既読に出来るようにするか（1:有効）',
  `check_targets` mediumtext COMMENT 'チェック対象（JSON配列）',
  PRIMARY KEY (`table_id`,`revision`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT '回覧板設定';



-- --------------------------------------------------------

--
-- テーブルの構造 `read_log`
--
DROP TABLE IF EXISTS `read_log`;
CREATE TABLE `read_log` (
  `table_id` int(11) NOT NULL,
  `row_id` bigint(20) NOT NULL,
  `read_user_id` int(11) NOT NULL,
  `read_on` datetime DEFAULT NULL,
  `comment` text,
  `is_read` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`table_id`,`row_id`,`read_user_id`),
  KEY `RID` (`row_id`),
  KEY `UID` (`read_user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT '既読管理';

-- --------------------------------------------------------

--
-- テーブルのデータ
--

INSERT INTO `sequence` (`name`, `current`) VALUES
('object_id', 0);

INSERT INTO `user` (`role`, `login_id`, `password`, `screen_name`, `profile`, `created_on`, `updated_on`) VALUES
('admin', 'admin@example.com', 'admin@example.com', '管理者', null, now(), now());

INSERT INTO `icon` (`id`, `filekey`, `seq`) VALUES
(1, 'db-icon-01.png', 1),
(2, 'db-icon-02.png', 2),
(3, 'db-icon-03.png', 3),
(4, 'db-icon-04.png', 4),
(5, 'db-icon-05.png', 5),
(6, 'db-icon-06.png', 6),
(7, 'db-icon-07.png', 7),
(8, 'db-icon-08.png', 8),
(9, 'db-icon-09.png', 9),
(10, 'db-icon-10.png', 10),
(11, 'db-icon-11.png', 11),
(12, 'db-icon-12.png', 12),
(13, 'db-icon-13.png', 13),
(14, 'db-icon-14.png', 14),
(15, 'db-icon-15.png', 15),
(16, 'db-icon-16.png', 16),
(17, 'db-icon-17.png', 17),
(18, 'db-icon-18.png', 18),
(19, 'db-icon-19.png', 19),
(20, 'db-icon-20.png', 20),
(21, 'db-icon-21.png', 21),
(22, 'db-icon-22.png', 22),
(23, 'db-icon-23.png', 23),
(24, 'db-icon-24.png', 24),
(25, 'db-icon-25.png', 25),
(26, 'db-icon-26.png', 26),
(27, 'db-icon-27.png', 27),
(28, 'db-icon-28.png', 28),
(29, 'db-icon-29.png', 29),
(30, 'db-icon-30.png', 30),
(31, 'db-icon-31.png', 31),
(32, 'db-icon-32.png', 32),
(33, 'db-icon-33.png', 33),
(34, 'db-icon-34.png', 34),
(35, 'db-icon-35.png', 35),
(36, 'db-icon-36.png', 36),
(37, 'db-icon-37.png', 37),
(38, 'db-icon-38.png', 38),
(39, 'db-icon-39.png', 39),
(40, 'db-icon-40.png', 40),
(41, 'db-icon-41.png', 41),
(42, 'db-icon-42.png', 42),
(43, 'db-icon-43.png', 43),
(44, 'db-icon-44.png', 44),
(45, 'db-icon-45.png', 45),
(46, 'db-icon-46.png', 46),
(47, 'db-icon-47.png', 47),
(48, 'db-icon-48.png', 48),
(49, 'db-icon-49.png', 49),
(50, 'db-icon-50.png', 50),
(51, 'db-icon-51.png', 51),
(52, 'db-icon-52.png', 52),
(53, 'db-icon-53.png', 53),
(54, 'db-icon-54.png', 54),
(55, 'db-icon-55.png', 55),
(56, 'db-icon-56.png', 56),
(57, 'db-icon-57.png', 57),
(58, 'db-icon-58.png', 58),
(59, 'db-icon-59.png', 59),
(60, 'db-icon-60.png', 60);

