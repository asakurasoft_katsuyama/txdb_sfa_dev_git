<!DOCTYPE html>
<html lang="ja">
<head>
{include file="include/head.tpl" title="ログイン"}
{*
<link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/css/base.css?{get_ver_query()|escape}">
*}
<link rel="stylesheet" type="text/css" href="/css/page/login.css?{get_ver_query()|escape}">
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="login">

	<div id="content">
		{if count($messages) gt 0}
		<div class="alert alert-warning">
			<strong>エラー:</strong>
			{foreach $messages as $message}
			{$message->message|escape}<br> {/foreach}
		</div>
		{/if}
		<h1 id="logo"><img src="{get_config('app', 'login_logo_image', '/asset/img/logo_b@2x.png')}" alt="{get_config('app', 'login_logo_alt', '使えるくらうどDBにログイン')}"></h1>
		<form class="form form-login" action="/login/authenticate" method="post">
			<div class="form-group">
			    <label for="login-input">ログインID</label>
			    <input type="text" class="form-control" id="login-input" name="login" value="{$form->login|escape}">
			</div>
			<div class="form-group">
			    <label for="login-password">パスワード</label>
			    <input type="password" class="form-control" id="login-password" name="password" value="{$form->password|escape}">
			</div>
			<button class="btn btn-lg btn-primary btn-block" type="submit">ログイン</button>
	       	<div class="checkbox">
			    <label>
			      <input type="checkbox" value="1" name="saveLogin"{if $form->saveLogin}checked="checked"{/if}> ログインIDを記憶する
			    </label>
			  </div>
		</form>
	</div>
</div>

<div id="footer">
	{include file="include/footer.tpl"}
</div>

{include file="include/js_bottom.tpl"}
</body>
</html>
