<div class="navbar navbar-default effect1" role="navigation">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button id="toggle-button" type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="nav">MENU</span>
      </button>
      <div class="navbar-brand" id="logo"><img src="{get_config('app', 'logo_image', '/asset/img/logo@2x.png')}" height="auto" width="auto" style="height:auto;" alt="logo"></div>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        <li{if isset($active) and $active eq 'home'} class="active"{/if}><a class="db-list" href="/my/"><i class="fa fa-pencil-square-o"></i> データベース入力</a></li>
        <li{if isset($active) and $active eq 'design'} class="active"{/if}><a class="db-design" href="/my/table/"><i class="fa fa-file-text-o"></i> データベース作成</a></li>
		{if get_config('app', 'features.notification')}
        <li{if isset($active) and $active eq 'notification'} class="active"{/if}><a class="db-notification" href="/my/notification/list_unread"><i class="btn-icon fa fa-bell-o {if get_config('app','features.hide_bellicon',1)}hidebellicon{/if}"></i> <span class="btn-text {if get_config('app','features.hide_tsuuchi_text',1)}hidetsuuchi{/if}">通知</span> <span class="badge global-notifications-count-wrapper" style="display:none"><span class="global-notifications-count"></span></span></a></li>
		{/if}
        <!--<li>設定</li>-->
      </ul>

      <div id="logout-box" class="nav navbar-nav navbar-right dropdown">
          <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">{$user->getScreenName()|escape} <span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="/admin/user/">ユーザー管理</a></li>
            {if $user->isAdmin()}
            <li><a href="/admin/group/">グループ管理</a></li>
            {/if}
            <li role="separator" class="divider"></li>
            <li><a href="{get_config('app', 'terms_html_url', 'http://tsukaerudb.asakurasoft7.jp/terms.html')}" target="_blank">利用規約</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="/my/logout">ログアウト</a></li>
          </ul>
      </div>
{*<!--
      <div id="logout-box" class="nav navbar-nav navbar-right dropdown">
        <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown">{$user->getScreenName()|escape} <span class="caret"></span></a>
        <ul class="dropdown-menu" role="menu">
          <li role="presentation">
            <a href="/admin/user/">ユーザー管理</a>
            {if $user->isAdmin()}
            <a href="/admin/group/">グループ管理</a>
            {/if}
          </li>
          <li class="terms" role="presentation"><a id="terms" href="http://tsukaerudb.asakurasoft7.jp/terms.html">利用規約</a></li>
          <li class="logout" role="presentation"><a id="logout" href="/my/logout">ログアウト</a></li>
        </ul>
      </div>
 -->*}
    </div>{** .navbar-collapse **}
  </div>{** /.container-fluid **}
</div>

{if isset($active) and $active eq 'home'}
<div class="db-list_top" id="page-nav_top">
  <div id="db-list_top">
    <div class="container"><i class="fa fa-pencil-square-o"></i> データベース入力</div>
  </div>
</div>
{/if}
{if isset($active) and $active eq 'design'}
<div class="db-design_top" id="page-nav_top">
  <div id="db-design_top">
    <div class="container"><i class="fa fa-file-text-o"></i> データベース作成</div>
  </div>
</div>
{/if}
{if isset($active) and $active eq 'notification'}
<div class="db-notification_top" id="page-nav_top">
  <div id="db-notification_top">
    <div class="container"><i class="fa fa-bell-o"></i> 通知一覧</div>
  </div>
</div>
{/if}
