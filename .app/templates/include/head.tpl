{if !isset($features)}{$features=[]}{/if}
<meta charset="utf-8" />
{if $title}<title>{$title} | {get_config('app', 'service_name', '使えるくらうどDB')}</title>{/if}
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
<!--[if lt IE 9]>
<script src="/js/html5.js" type="text/javascript"></script>
<script src="/js/html5shiv.js" type="text/javascript"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="/css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="/css/base.css?{get_ver_query()|escape}">
<link rel="stylesheet" type="text/css" href="/css/cloud-db.css?{get_ver_query()|escape}">
<link href="//netdna.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">
{**<link rel="stylesheet" type="text/css" href="/css/bootstrap-theme.min.css">**}

{if in_array('datepicker', $features, true)}
<link rel="stylesheet" type="text/css" href="/css/bootstrap-datetimepicker.min.css">
{/if}
{if in_array('lightbox', $features, true)}
<link rel="stylesheet" type="text/css" href="/css/lightbox/ekko-lightbox.min.css">
{/if}
{if in_array('select2', $features, true)}
<link rel="stylesheet" type="text/css" href="/js/select2/select2.css">
{/if}
{if in_array('nvd3', $features, true)}
<link rel="stylesheet" type="text/css" href="/css/nvd3/nv.d3.min.css">
{/if}
{if in_array('fileupload', $features, true)}
<link href="/css/fileupload/jquery.fileupload.css" rel="stylesheet" >
{/if}
{if in_array('typeahead', $features, true)}
<link href="/css/typeahead/typeaheadjs.css" rel="stylesheet" >
{/if}
{include "include/head__extension.tpl"}
