{if !isset($features)}{$features=[]}{/if}
<script type="text/javascript" src="/js/jquery-1.11.0.min.js"></script>
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/knockout-2.2.1.js"></script>
<script type="text/javascript" src="/js/knockout-switch-case.min.js"></script>
<script type="text/javascript" src="/js/view/base.js?{get_ver_query()|escape}"></script>
<script type="text/javascript" src="/js/jquery.blockUI.min.js"></script>
{if in_array('datepicker', $features, true)}
<script type="text/javascript" src="/js/moment.min.js"></script>{** https://github.com/moment/moment **}
<script type="text/javascript" src="/js/moment.lang.ja.js"></script>{** https://github.com/moment/moment **}
<script type="text/javascript" src="/js/bootstrap-datetimepicker.js"></script>{** http://eonasdan.github.io/bootstrap-datetimepicker/ **}
{/if}
{if in_array('moment', $features, true)}
<script type="text/javascript" src="/js/moment.min.js"></script>{** https://github.com/moment/moment **}
<script type="text/javascript" src="/js/moment.lang.ja.js"></script>{** https://github.com/moment/moment **}
{/if}
{if in_array('lightbox', $features, true)}
<script type="text/javascript" src="/js/lightbox/ekko-lightbox.min.js"></script>
{/if}
{if in_array('fixedheader', $features, true)}
<script type="text/javascript" src="/js/table-fixed-header.js"></script>
{/if}
{if in_array('ckeditor', $features, true)}
{if preg_match('/(iPhone|iPad)/i', $smarty.server.HTTP_USER_AGENT) }
<script type="text/javascript" src="/js/ckeditor_ios/ckeditor.js"></script>	{** CKEditor **}
{else}
<script type="text/javascript" src="/js/ckeditor/ckeditor.js"></script>	{** CKEditor **}
{/if}
<script type="text/javascript" src="/js/ckeditor/adapters/jquery.js"></script>
{/if}
{if in_array('sortable', $features, true)}
<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
<script type="text/javascript" src="/js/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="/js/knockout-sortable.min.js"></script>
{/if}
{if in_array('fileupload', $features, true)}
<script type="text/javascript" src="/js/fileupload/vendor/jquery.ui.widget.js"></script>	{** jquery.fileupload **}
<script type="text/javascript" src="/js/fileupload/jquery.iframe-transport.js"></script>
<script type="text/javascript" src="/js/fileupload/jquery.fileupload.js"></script>
{/if}
{if in_array('select2', $features, true)}
<script type="text/javascript" src="/js/select2/select2.min.js"></script>
{/if}
{if in_array('nvd3', $features, true)}
<script type="text/javascript" src="/js/nvd3/lib/d3.v3.js"></script>
{**<script type="text/javascript" src="/js/nvd3/nv.d3.min.js"></script>**}{** ver. 1.1.15bマルチバイトのlegend幅に問題  **}
<script type="text/javascript" src="/js/nvd3/nv.d3.js"></script>{** ver. 1.1.11b **}
{/if}
{if in_array('typeahead', $features, true)}
<script type="text/javascript" src="/js/typeahead/typeahead.bundle.js"></script>
{/if}
<script type="text/javascript" src="/js/js.cookie.min.js"></script>
<script type="text/javascript" src="/js/terms.js"></script>
{if in_array('qst', $features, true)}
<script type="text/javascript" src="/js/qst.min.js"></script>
{/if}
{if $user}
<script>
$(function(){
    var cookieName = "{get_config('app', 'terms_cookie_name', 'tsukaeru_cloud_db_terms_agreement_v_1_0_0')}";
    var cookieExpire = {get_config('app', 'terms_cookie_expire', 365)};
    var terms = "{get_config('app', 'terms_url', '//tsukaerudb.asakurasoft7.jp/db_terms.html')}";
    var service = "{get_config('app', 'terms_service_name', '使えるくらうどDB')}";
    txdb.refreshNotificationsCount();
    TsukaeruCloudDBTerms.confirm(cookieName, cookieExpire, terms, service);
});
</script>
{/if}
{include "include/js_bottom__extension.tpl"}
