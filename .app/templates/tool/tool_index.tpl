<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
{include file="include/head.tpl" title=""}
<link rel="stylesheet" type="text/css" href="/css/bootstrap-datetimepicker.min.css">
<style type="text/css">
.field-number input,
.field-calc input {
	text-align: right;
}
.field-date, .field-time {
	max-width: 200px !important;
}
.field-datetime {
	max-width: 400px !important;
}
.field-number input,
.field-calc input {
	max-width: 300px !important;
}
</style>
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="home"}
</div>

<div id="content">
	<div class="container">

		<div class="panel panel-default">
			<form role="form" action="/tool/create" method="post">
				<div class="form-group">
					<label class="control-label">Table name</label>
					<input type="text" class="form-control" name="name" value="New table">
				</div>
				<div class="form-group">
					<label class="control-label">Columns</label>
					<div class="controls">
						<table class="table">
							<thead>
							<tr>
								<th></th>
								<th>Type</th>
								<th>Code / Label</th>
								<th>List view</th>
								<th>Post.</th>
								<th>Required</th>
								<th>Options</th>
							</tr>
							</thead>
							<tbody>
							{foreach [
								 [name=>'文字列_一行', type=>'text', default=>true, required=>true, expression=>'', length=>true, span=>true ]
								,[name=>'文字列_複数行', type=>'multitext', required=>true, default=>true, height=>true, span=>true ]
								,[name=>'装飾テキスト', type=>'richtext', required=>true, default=>true, height=>true, span=>true ]
								,[name=>'数値', type=>'number', required=>true, default=>true, span=>true ]
								,[name=>'計算', type=>'calc', expression=>'数値*2', span=>true ]
								,[name=>'選択肢', type=>'radio', required=>true, option=>true, span=>true ]
								,[name=>'選択肢(複数)', type=>'checkbox', required=>true, option=>true, span=>true ]
								,[name=>'プルダウン選択肢', type=>'dropdown', required=>true, option=>true, span=>true ]
								,[name=>'日付', type=>'date', required=>true , span=>true ]
								,[name=>'時刻', type=>'time', required=>true , span=>true ]
								,[name=>'日時', type=>'datetime', required=>true , span=>true ]
								,[name=>'添付ファイル   ', type=>'file', thumb=>true, required=>true , span=>true ]
								,[name=>'リンク      ', type=>'link', link=>true, length=>true, required=>true , span=>true ]
								,[name=>'関係データ一覧', type=>'relational', ref=>true, refTarget=>true, refSource=>true, refRows=>true]
								,[name=>'参照コピー', type=>'lookup', ref=>true, required=>true ,refTarget=>true, refCopy=>true, span=>true ]
								,[name=>'注意書き', type=>'label', labelHtml=>true, list=>false]
								,[name=>'空白', type=>'space', list=>false, height=>true]
								,[name=>'罫線', type=>'line', list=>false]
								] as $_c}
							<tr>
								<input type="hidden" class="" name="columns[{$_c@index}][type]" value="{$_c.type}">
								<td><input type="checkbox" class="" name="columns[{$_c@index}][checked]" value="1" checked="checked"></td>
								<td>{$_c.name|trim} <span style="color: #ccc">x</span>
									<input type="text" class="" name="columns[{$_c@index}][count]" value="" size="1" style="border:none; border-bottom: 1px solid #ccc;"></td>
								<td><input type="text" class="" name="columns[{$_c@index}][code]" value="{$_c.name|trim}"></td>
								<td>
									{if !isset($_c.list)}
									<input type="checkbox" class="" name="columns[{$_c@index}][list]" value="1">
									{/if}
								</td>
								<td>
									<input type="text" name="columns[{$_c@index}][y]" size="1" value="{$_c@iteration}"style="border: none; width: 20px"> /
									<input type="text" name="columns[{$_c@index}][x]" size="1" value="1" style="border: none; width: 20px">
									{if isset($_c.span)}
									<input type="text" name="columns[{$_c@index}][span]" size="1" value="1" style="border:none; border-bottom: 1px solid #ccc; none; width: 10px">
									{/if}
									{if isset($_c.height)}
									x <input type="text" name="columns[{$_c@index}][height]" size="1" value="" style="border:none; border-bottom: 1px solid #ccc; width: 20px"> Rows
									{/if}
								</td>
								<td>
									{if isset($_c.required)}
										<input type="checkbox" class="" name="columns[{$_c@index}][required]" value="1">
									{/if}
								</td>
								<td>
									{if isset($_c.default)}
										<input type="text" class="" name="columns[{$_c@index}][default]" placeholder="default ...">
									{/if}
									{if isset($_c.expression)}
										<input type="text" class="" name="columns[{$_c@index}][expression]" placeholder="expression" value="{$_c.expression}">
									{/if}
									{if isset($_c.option)}
										<input type="text" class="" name="columns[{$_c@index}][option]" placeholder="Num options" value="3" size="1"
										style="border:none; border-bottom: 1px solid #ccc;"> options
									{/if}
									{if isset($_c.thumb)}
										Thumbing <input type="text" class="" name="columns[{$_c@index}][thumb]" placeholder="Thumbnail" value="200" size="1"
										style="border:none; border-bottom: 1px solid #ccc;"> px
									{/if}
									{if isset($_c.length)}
										<input type="text" class="" name="columns[{$_c@index}][minLength]" placeholder="min" value="" size="2"
												style="border:none; border-bottom: 1px solid #ccc;">〜
										<input type="text" class="" name="columns[{$_c@index}][maxLength]" placeholder="max" value="" size="2"
												style="border:none; border-bottom: 1px solid #ccc;">
									{/if}
									{if isset($_c.labelHtml)}
										<input type="text" class="" name="columns[{$_c@index}][labelHtml]"
										value="&lt;font color=red&gt;Label&lt;/font&gt;">
									{/if}
									{if isset($_c.link)}
										<select name="columns[{$_c@index}][link]">
											<option value="email">E-mail
										</select>
									{/if}
									{if isset($_c.refSource)}
										Me <input type="text" class="" name="columns[{$_c@index}][refSource]" placeholder="source index" value="0" size="1"> th
									{/if}
									{if isset($_c.ref)}
										　⇆　
										<select name="columns[{$_c@index}][refTable]">
											{foreach $tables as $_table}
											<option value="{$_table->getId()|escape}">{$_table->getName()|escape}
											{/foreach}
										</select>
									{/if}
									{if isset($_c.refTarget)}
										<input type="text" class="" name="columns[{$_c@index}][refTarget]" placeholder="target index" value="0" size="1"> th
									{/if}
									{if isset($_c.refRows)}
										( <input type="text" class="" name="columns[{$_c@index}][refRows]" placeholder="Rows" value="5" size="1"> rows )
									{/if}
									{if isset($_c.refCopy)}
										/ <input type="text" class="" name="columns[{$_c@index}][refCopy]" placeholder="Rows" value="0->0," size="10">
									{/if}

								</td>
							</tr>
							{/foreach}
							</tbody>
						</table>
					</div>
				</div>
				<button class="btn btn-info">保存</button>
			</form>
		</div>

	</div>
</div>
<div id="footer">
	{include file="include/footer.tpl"}
</div>
<script type="text/javascript">
</script>
</body>
</html>
