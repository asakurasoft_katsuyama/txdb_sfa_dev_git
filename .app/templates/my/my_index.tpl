<!DOCTYPE html>
<html lang="ja">
<head>
{include file="include/head.tpl" title="ホーム"}
<link rel="stylesheet" type="text/css" href="/css/page/my_index.css?{get_ver_query()|escape}">
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="home"}
</div>

<div id="content" class="db-list_content" >
	<div class="container">
		<section class="db-list public-db-list">
			<div class="section-header">
				<h4><i class="fa fa-users"></i> 共有データベース</h4>
			</div>
			<div class="row" id="db-common">
				{foreach $publicTables as $table}
				<div class="col-xs-4 col-md-2 col-sm-2 db-common-parts">
					<div class="thumbnail">
						<div class="images"><a href="/my/data/?t={$table->getId()|escape}"><img src="{$table->getIcon()->getUrl()|escape}"></a></div>
						<div class="caption">
							<h4>{$table->getName()|escape}</h4>
							<p>{if $table->getOwner()}{$table->getOwner()->getScreenName()|escape}{/if}</p>
						</div>
					</div>
				</div>
				{foreachelse}
				<div class="col-xs-12 col-md-12">
					<p>選択できるデータベースはありません</p>
				</div>
				{/foreach}
			</div>
		</section>
		<section class="db-list private-db-list">
			<div class="section-header">
				<h4><i class="fa fa-user"></i> 個人データベース</h4>
			</div>
			<div class="row" id="db-alone">
				{foreach $privateTables as $table}
				<div class="col-xs-4 col-md-2 col-sm-2 db-alone-parts">
					<div class="thumbnail">
						<div class="images"><a href="/my/data/?t={$table->getId()|escape}"><img src="{$table->getIcon()->getUrl()|escape}"></a></div>
						<div class="caption">
							<h4>{$table->getName()|escape}</h4>
							<p>{if $table->getOwner()}{$table->getOwner()->getScreenName()|escape}{/if}</p>
						</div>
					</div>
				</div>
				{foreachelse}
				<div class="col-xs-12 col-md-12">
					<p>選択できるデータベースはありません</p>
				</div>
				{/foreach}
			</div>
		</section>
	</div>
</div>
<div id="footer">
	{include file="include/footer.tpl"}
</div>

{include file="include/js_bottom.tpl"}
</body>
</html>
