<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
{include file="include/head.tpl" title="通知一覧"}
<link rel="stylesheet" type="text/css" href="/css/page/notification_index.css?{get_ver_query()|escape}">
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
    {include file="include/header.tpl" active="notification"}
</div>

<div id="content" class="db-notification_content">
    <div class="container notification-search">
        <h4>通知一覧</h4>
        <div class="notification-search-form">
            <label class="radio-inline">
                <input type="radio" value="unread" data-bind="checked: read">
                未読
            </label>
            <label class="radio-inline">
                <input type="radio" value="read" data-bind="checked: read">
                既読
            </label>
        </div>
        <!-- ko if: totalRowCount() > 0 -->
        <div class="row notification-action-form">
            <div class="col-xs-6">
                <div class="checkbox">
                    <label>
                        <input type="checkbox" value="1" data-bind="click: toggleAllCheck, checked: allChecked"> 一括選択
                    </label>
                </div>
            </div>
            <div class="col-xs-6 notification-batch-actions">
                <div class="notification-btn notification-batch-action">
            <!-- ko if: read() == 'unread' -->
                <a data-bind="click: toggleReadInBatch">一括で既読にする</a>
            <!-- /ko -->
            <!-- ko if: read() == 'read' -->
                <a data-bind="click: toggleReadInBatch">一括で未読に戻す</a>
            <!-- /ko -->
                </div>
            </div>
        </div>
        <!-- /ko -->
        <div class="notification-search-result">
            <!-- ko if: $root.totalRowCount() == 0 && loadingDone() -->
                <!-- ko if: $root.read() == 'unread' -->
                <p>未読通知はありません。</p>
                <!-- /ko -->
                <!-- ko if: $root.read() == 'read' -->
                <p>既読通知はありません。</p>
                <!-- /ko -->
            <!-- /ko -->
            <!-- ko if: $root.totalRowCount() > 0 -->
            <div data-bind=" foreach: $root.rows">
                <div class="notification-wrapper" data-bind="attr:{ id: 'notification__wrapper_'+$data.notification.id }">
                    <div class="notification row" data-bind="attr:{ id: 'notification_'+$data.notification.id }">
                        <div class="col-notification col-checkbox">
                            <label class="checkbox-inline">
                                <input type="checkbox" data-bind="checked: checked, click: $root.updateAllCheck.bind($root)">
                            </label>
                        </div>
                        <div class="col-notification col-content">
                            <div class="notification-header">
                                <div class="row">
                                    <div class="notification-title col-md-8">
                                        <!-- ko if: $data.notification.notifier -->
                                            {if get_config('app', 'features.notifier_profile', 1) }
                                                [<!-- ko if: $data.notification.notifier.profile --><span class="notification-notifier-profile" data-bind="text: $data.notification.notifier.profile "></span> <!-- /ko --><span class="notification-notifier" data-bind="text: $data.notification.notifier.screenName "></span>]
                                            {else}
                                                [<span class="notification-notifier" data-bind="text: $data.notification.notifier.screenName "></span>]
                                            {/if}
                                        <!-- /ko -->
                                        <!-- ko ifnot: $data.notification.notifier -->
                                            [<span class="notification-notifier notification-notifier-deleted">*削除されたユーザー*</span>]
                                        <!-- /ko -->
                                        <span data-bind="text: $data.notification.table.name "></span>
                                    </div>
                                    <div class="notification-info col-md-4">
                                        <span class="notification-time-wrapper">
                                        通知日時: <span class="notification-time" data-bind="text: $data.notification.notifiedOn "></span>
                                        </span>
                                    </div>
                                </div>
                                <!-- ko if: $data.notification.row.columns.length > 0 -->
                                <div class="row" data-bind="foreach: $data.notification.row.columns ">
                                    <div class="col-xs-12 notification-field" data-bind="css: 'col-sm-' + (12/$parent.notification.row.columns.length) ">
                                        <span class="notification-label" data-bind="text: label "></span>:
                                        <span class="notification-value" data-bind="text: $parent.notification.row.values[$index()].text "></span>
                                    </div>
                                </div>
                                <!-- /ko -->
                            </div><!-- notification-header -->
                            <hr>
                            <div class="notification-body">
                                <div class="notification-message">
                                    <span data-bind="text: $data.notification.message "></span>
                                </div>
                                <!-- ko if: $data.notification.changes && $data.notification.changes.length > 0 -->
                                <div class="notification-changes">
                                    変更されたフィールド:
                                    <div data-bind="foreach: $data.notification.changes ">
                                        <div class="notification-change">
                                            <div class="notification-change-label">
                                                【<span data-bind="text: column.label "></span>】
                                            </div>
                                            <div class="notification-change-old">
                                                <span class="notification-change-label">(変更前)</span>
                                                <span class="notification-change-value" data-bind="text: text.old "></span>
                                            </div>
                                            <div class="notification-change-new">
                                                <span class="notification-change-label">(変更後)</span>
                                                <span class="notification-change-value" data-bind="text: text.new "></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /ko -->
                                <!-- ko if: $data.notification.comment -->
                                <div class="notification-comment">
                                    <span data-bind="foreach: $data.notification.comment.notificationTargets ">
                                        @<span data-bind="text: name"></span>

                                    </span>
                                    <span data-bind="text: $data.notification.comment.text"></span>
                                </div>
                                <!-- /ko -->
                            </div><!-- notification-body -->
                        </div><!-- col -->
                        <div class="col-notification col-action">
                            <div class="notification-btn notification-btn-open">
                                <a data-bind="click: $root.openData.bind($root, $data)"><i class="notification-btn-icon fa fa-file-text-o"></i> <span class="notification-btn-text">該当データへ</span></a>
                            </div>
                            <!-- ko if: !isRead -->
                            <div class="notification-btn notification-btn-read">
                                <a data-bind="click: $root.toggleRead.bind($root, $data)"><i class="notification-btn-icon fa fa-check"></i> <span class="notification-btn-text">既読にする</span></a>
                            </div>
                            <!-- /ko -->
                            <!-- ko if: isRead -->
                            <div class="notification-btn notification-btn-unread">
                                <a data-bind="click: $root.toggleRead.bind($root, $data)"><i class="notification-btn-icon fa fa-undo"></i> <span class="notification-btn-text">未読に戻す</span></a>
                            </div>
                            <!-- /ko -->
                        </div><!-- col -->
                    </div><!--notification -->
                </div><!--notification-wrapper -->
            </div>
            <!-- /ko --><!-- row exists -->
        </div><!-- result -->
        <!-- ko if: $root.totalRowCount() > 0 -->
        <ul class="pager">
              <li data-bind=" css: { disabled: !hasPrev() } "><a href="#" data-bind=" click: prevList"><i class="fa fa-backward"></i> 前へ</a></li>
              <li data-bind=" css: { disabled: !hasNext() } "><a href="#" data-bind=" click: nextList ">次へ <i class="fa fa-forward"></i></a></li>
        </ul>
        <!--/ko -->
    </div>
</div>
<div id="footer">
    {include file="include/footer.tpl"}
</div>

{include file="include/js_bottom.tpl" features=['lineup']}
<script type="text/javascript" src="/js/view/base/list_model.js?{get_ver_query()|escape}"></script>
<script type="text/javascript" src="/js/view/notification_index.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
    var options = {
        read: '{$read|default:'unread'}',
        offset: {$limit[0]|default:0},
        limit: {$limit[1]|default:20}
    };
    viewModel = new ViewModel(options);
    ko.applyBindings(viewModel);
});
</script>
</body>
</html>
