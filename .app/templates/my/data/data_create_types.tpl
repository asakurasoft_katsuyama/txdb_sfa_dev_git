

{** 文字列 =========================================================== **}
<script type="text/html" id="create-text-tpl">
	<div class="form-group field-text" data-bind="css:{ required:isRequired}">

		<label class="control-label" data-bind="attr:{ 'for':idName()}, text:label, visible: showsLabel"></label>
		<!-- ko if: expression != null -->
			<input type="text" class="form-control" data-bind="attr: { id: idName() }, value: value" disabled="disabled">
		<!-- /ko -->
		<!-- ko if: expression == null -->
			<input type="text" class="form-control" data-bind="attr: { id: idName(), maxlength: maxLength }, value: value, disable: disabled">
		<!-- /ko -->
		<!-- ko if: hintText -->
		<span class="help-block" data-bind="text: hintText"></span>
		<!-- /ko -->
		<!-- ko if: error -->
		<div class="alert alert-warning" data-bind="text: error"></div>
		<!-- /ko -->
	</div>
</script>

{** 文字列（複数行） =========================================================== **}
<script type="text/html" id="create-multitext-tpl">
	<div class="form-group field-multitext" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="attr:{ 'for':idName()}, text:label, visible: showsLabel"></label>
		<textarea class="form-control" data-bind=" attr: { id: idName(), rows: sizeHeight }, value:value, disable: disabled"></textarea>
		<!-- ko if: error -->
		<div class="alert alert-warning" data-bind="text: error"></div>
		<!-- /ko -->
	</div>
</script>

{** リッチエディター =========================================================== **}
<script type="text/html" id="create-richtext-tpl">
	<div class="form-group field-richtext" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="attr:{ 'for':idName()}, text:label, visible: showsLabel"></label>
		<textarea class="form-control richeditor" data-bind="attr: { id:idName(),rows:sizeHeight},richeditor:value,richTextOptions:{ readOnly:disabled()}"></textarea>
		<!-- ko if: error -->
		<div class="alert alert-warning" data-bind="text: error"></div>
		<!-- /ko -->
	</div>
</script>

{** 数値 =========================================================== **}
<script type="text/html" id="create-number-tpl">
	<div class="form-group field-number" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="attr:{ 'for':idName()}, text:label, visible: showsLabel"></label>
		<input type="text" class="form-control" data-bind="attr: { id: idName() }, value: value, disable: disabled">{**再計算あり**}
		<!-- ko if: hintText -->
		<span class="help-block" data-bind="text: hintText"></span>
		<!-- /ko -->
		<!-- ko if: error -->
		<div class="alert alert-warning" data-bind="text: error"></div>
		<!-- /ko -->
	</div>
</script>

{** 計算 =========================================================== **}
<script type="text/html" id="create-calc-tpl">
	<div class="form-group field-calc" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="attr:{ 'for':idName()}, text:label, visible: showsLabel"></label>
		<input type="text" class="form-control" data-bind="value: textWithError" disabled="disabled">
		<!-- <input type="hidden" class="form-control" data-bind="value: value"> -->
	</div>
</script>

{** ラジオボタン =========================================================== **}
<script type="text/html" id="create-radio-tpl">
	<div class="form-group field-radio" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="attr:{ 'for':idName()}, text:label, visible: showsLabel"></label>
		<div class="controls" data-bind="foreach: options" >
			<label data-bind="css: $parent.optionsAlignment == 'vertical'? 'radio-block' : 'radio-inline'">
				<input type="radio" data-bind="attr:{ value: id}, checked: $parent.value, disable: $parent.disabled">
				<span data-bind=" text: name "></span>
			</label>
		</div>
		<!-- ko if: error -->
		<div class="alert alert-warning" data-bind="text: error"></div>
		<!-- /ko -->
	</div>
</script>

{** チェックボックス =========================================================== **}
<script type="text/html" id="create-checkbox-tpl">
	<div class="form-group field-checkbox" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="attr:{ 'for':idName()}, text:label, visible: showsLabel"></label>
		<div class="controls" data-bind="foreach: options" >
			<label data-bind="css: $parent.optionsAlignment == 'vertical'? 'checkbox-block' : 'checkbox-inline'">
				<input type="checkbox" data-bind="attr:{ value: id}, checked: $parent.value, disable: $parent.disabled">
				<span data-bind=" text: name "></span>
			</label>
		</div>
		<!-- ko if: error -->
		<div class="alert alert-warning" data-bind="text: error"></div>
		<!-- /ko -->
	</div>
</script>

{** ドロップダウン =========================================================== **}
<script type="text/html" id="create-dropdown-tpl">
	<div class="form-group field-dropdown" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="attr:{ 'for':idName()}, text:label, visible: showsLabel"></label>
		<select class="form-control" data-bind="options: options, optionsText: 'name', optionsValue: 'id', value: value, disable: disabled"></select>
		<!-- ko if: error -->
		<div class="alert alert-warning" data-bind="text: error"></div>
		<!-- /ko -->
	</div>
</script>

{** ユーザー選択 =========================================================== **}
<script type="text/html" id="create-user_selector-tpl">
<div class="form-group field-user_selector" data-bind="css:{ required:isRequired}">
	<label class="control-label" data-bind="attr:{ 'for':idName()}, text:label, visible: showsLabel"></label>
	<div class="controls">
		<select data-bind="attr: { id: idName() },
			foreach: userOptions, event: { 'select2-selecting': selected }, selectedOptions: value, select2: true" multiple="multiple">
			<optgroup data-bind="attr: { label: label}, foreach: children">
				<option data-bind="text: name, value: id" ></option>
			</optgroup>
		</select>
	</div>
	<!-- ko if: error -->
	<div class="alert alert-warning" data-bind="text: error"></div>
	<!-- /ko -->
</div>
</script>


{** 日付 =========================================================== **}
<script type="text/html" id="create-date-tpl">
	<div class="form-group field-date" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="attr:{ 'for':idName()}, text:label, visible: showsLabel"></label>
		<div class="input-group datepicker">
			<input type="text" class="form-control" data-bind="attr: { id: idName() }, datepicker: value, disable: disabled">{**再計算あり**}
			<div class="input-group-btn">
				<button class="btn"><span class="glyphicon glyphicon-calendar"></span></button><button class="btn datetime-clear"><i class="glyphicon glyphicon-remove"></i></button>
			</div>
		</div>
		<!-- ko if: error -->
		<div class="alert alert-warning" data-bind="text: error"></div>
		<!-- /ko -->
	</div>
</script>

{** 時刻 =========================================================== **}
<script type="text/html" id="create-time-tpl">
	<div class="form-group field-time" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="attr:{ 'for':idName()}, text:label, visible: showsLabel"></label>
		<div class="input-group timepicker">
			<input type="text" class="form-control" data-bind="attr: { id: idName() }, timepicker: value, disable: disabled">{**再計算あり**}
			<div class="input-group-btn">
				<button class="btn"><span class="glyphicon glyphicon-time"></span></button><button class="btn datetime-clear"><i class="glyphicon glyphicon-remove"></i></button>
			</div>
		</div>
		<!-- ko if: error -->
		<div class="alert alert-warning" data-bind="text: error"></div>
		<!-- /ko -->
	</div>
</script>

{** 日時 =========================================================== **}
<script type="text/html" id="create-datetime-tpl">
	<div class="form-group field-datetime" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="attr:{ 'for':idName()}, text:label, visible: showsLabel"></label>
		<div class="input-group datetimepicker">
			<input type="text" class="form-control" data-bind="attr: { id: idName() }, datetimepicker: value, disable: disabled">{**再計算あり**}
			<div class="input-group-btn">
				<button class="btn"><span class="glyphicon glyphicon-calendar"></span></button><button class="btn datetime-clear"><i class="glyphicon glyphicon-remove"></i></button>
			</div>
		</div>
		<!-- ko if: error -->
		<div class="alert alert-warning" data-bind="text: error"></div>
		<!-- /ko -->
	</div>
</script>

{** 日時計算 =========================================================== **}
<script type="text/html" id="create-datecalc-tpl">
	<div class="form-group field-date" data-bind="css: { required: isRequired}" >
		<label class="control-label" data-bind="attr: { 'for': idName()}, text: label, visible: showsLabel"></label>
		<input type="text" class="form-control" data-bind="value: textWithError" disabled="disabled">
	</div>
</script>

{** 添付ファイル =========================================================== **}
<script type="text/html" id="create-file-tpl">
	<div class="form-group field-file" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="attr:{ 'for':idName()}, text:label, visible: showsLabel"></label>
		{**<input type="file" class="form-control" data-bind="attr: { id: idName() }">**}
		<table class="table">
			<tbody data-bind="foreach: value">
				<tr>
					<td data-bind="text: fileName">&nbsp;</td>
					<!-- ko if:isSaved -->
					<td><button class="btn btn-default" data-bind="click: $parent.removeFile.bind($parent)">削除</button></td>
					<!-- /ko -->
					<!-- ko ifnot:isSaved -->
					<td><button class="btn btn-default" data-bind="click: $parent.removeFile.bind($parent)">キャンセル</button></td>
					<!-- /ko -->
				</tr>
			</tbody>
		</table>
		<div>
			<span class="btn btn-default fileinput-button">
				<span>参照</span>
				<input class="fileupload" type="file" multiple="multiple" name="files[]" data-url="/my/data/upload_file.json" data-bind="attr: { id: idName() } " />
			</span>
			<p class="help-block">({file_size_convert(get_config('app','data_file_size_limit'))}まで)</p>
		</div>
		<!-- ko if: error -->
		<div class="alert alert-warning" data-bind="text: error"></div>
		<!-- /ko -->
	</div>
</script>

{** リンク =========================================================== **}
<script type="text/html" id="create-link-tpl">
	<div class="form-group field-link" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="attr:{ 'for':idName()}, text:label, visible: showsLabel"></label>
		<input type="text" class="form-control" data-bind="attr: { id: idName(), maxlength: maxLength }, value: value, disable: disabled">{**再計算あり（関連レコード)**}
		<!-- ko if: hintText -->
		<span class="help-block" data-bind="text: hintText"></span>
		<!-- /ko -->
		<!-- ko if: error -->
		<div class="alert alert-warning" data-bind="text: error"></div>
		<!-- /ko -->
	</div>
</script>

{** 関連レコード =========================================================== **}
<script type="text/html" id="create-relational-tpl">
	<div class="form-group field-relational">
		<label class="control-label" data-bind="attr:{ 'for':idName()}, text:label, visible: showsLabel"></label>
		<!-- ko ifnot: error -->
			<table class="table table-striped" data-bind="with: list">
				<thead>
					<tr>
						<th></th>
						<!-- ko foreach: columns -->
						<th data-bind="text: label"></th>
						<!-- /ko -->
					</tr>
				</thead>
				<!-- ko if:totalRowCount() > 0 -->
				<tbody>
					<!-- ko foreach: rows -->
			    	<tr>
			    		<td></td>
			    		<!-- ko foreach: values -->
			  				<!-- ko template: { name: function() { return ($('#index-'+type+'-2-tpl').length > 0) ? 'index-'+type+'-2-tpl' : 'index-'+type+'-tpl';} } -->
			  				<!-- /ko -->
			  			<!-- /ko -->
					</tr>
					<!-- /ko -->
					<!-- ko if: $parents[0].aggregateList.totalRowCount() > 0 -->
					<tr>
						<!-- ko foreach: $parents[0].aggregateList.columns -->
						<td style="border-right: none; background-color: #ffffff;height:2em;"></td>
						<!-- /ko -->
					</tr>
					<tr>
						<!-- ko foreach: $parents[0].aggregateList.columns -->
						<th data-bind="text: label" style="background-color: #ffffff;"></th>
						<!-- /ko -->
					</tr>
						<!-- ko foreach: $parents[0].aggregateList.rows -->
					<tr>
							<!-- ko foreach: values -->
							<!-- ko template: { name: function() { return ($('#index-'+type+'-2-tpl').length > 0) ? 'index-'+type+'-2-tpl' : 'index-'+type+'-tpl';} } --><!-- /ko -->
							<!-- /ko -->
					</tr>
						<!-- /ko -->
					<!-- /ko -->
				</tbody>
				<!-- /ko -->
				<!-- ko if:totalRowCount() == 0 -->
				<tbody><tr><td colspan="100">データがありません。</td></tr></tbody>
				<!-- /ko -->
			</table>
			<ul class="pager" data-bind="with: list ">
	  			<li data-bind="css: { disabled: !hasPrev() } "><a href="#" data-bind=" click: prevList">前へ</a></li>
	  			<li data-bind="css: { disabled: !hasNext() } "><a href="#" data-bind=" click: nextList ">次へ</a></li>
			</ul>
		<!-- /ko -->
		<!-- ko if: error -->
		<div class="alert alert-warning" data-bind="text: error"></div>
		<!-- /ko -->
	</div>
</script>

{** 明細入力 =========================================================== **}
<script type="text/html" id="create-relational_input-tpl">
	<div class="form-group field-relational_input">
		<label class="control-label" data-bind="attr:{ 'for':idName()}, text:label, visible: showsLabel"></label>
		<!-- ko ifnot: error -->
			<!-- ko if: isReadable -->
			<div class="header-controls">
				<button class="btn btn-primary" data-bind="click: addRow, disable: !isWritable()">追加</button>
			</div>
			<table class="table table-striped" data-bind="">
				<thead>
					<tr>
						<!-- ko foreach: referenceColumns -->
						<th data-bind="text: label"></th>
						<!-- /ko -->
						<th class="nested-row-controls"></th>
					</tr>
				</thead>
				<!-- ko if: hasRows() -->
				<tbody data-bind=" foreach: value">
				<!-- ko if: action() != 'delete' -->
					<tr>
							<!-- ko foreach: $parent.getRowValues($data) -->
								<!-- ko template: { name: $parents[1].getTemplateName(type) } -->
								<!-- /ko -->
							<!-- /ko -->
						<td class="nested-row-controls">
							<button class="btn btn-default" data-bind="click: $parent.editRow.bind($parent), disable: !$parent.isWritable()">編集</button>
							<button class="btn btn-default" data-bind="click: $parent.removeRow.bind($parent), disable: !$parent.isWritable()">削除</button>
						</td>
					</tr>
				<!-- /ko -->
				</tbody>
				<!-- /ko -->
				<!-- ko if: !hasRows() -->
				<tbody><tr><td colspan="100">データがありません。</td></tr></tbody>
				<!-- /ko -->
			</table>
			<!-- /ko --><!-- isReadable -->
			<!-- ko ifnot: isReadable -->
				<div class="alert alert-warning">権限がありません。</div>
			<!-- /ko -->
		<!-- /ko -->
		<!-- ko if: error -->
		<div class="alert alert-warning" data-bind="text: error"></div>
		<!-- /ko -->
	</div>
</script>

{** ルックアップ =========================================================== **}
<script type="text/html" id="create-lookup-tpl">
	<div class="form-group field-lookup" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="attr:{ 'for':idName()}, text:label, visible: showsLabel"></label>
		<div class="input-group">
			<input type="text" class="form-control" data-bind="attr: { id: idName(), disable: disabled }, value: value, typeahead: value">
			<div class="input-group-btn">
				<button class="btn btn-info" data-bind="click: openLookup, attr: { disable: disabled }">確定</button><button class="btn btn-info" data-bind="click: clearLookup, attr: { disable: disabled }">クリア</button>
			</div>
		</div>
		<!-- ko if: error -->
		<div class="alert alert-warning" data-bind="text: error"></div>
		<!-- /ko -->
		<!-- ko if: message -->
		<div class="alert alert-info" data-bind="text: message"></div>
		<!-- /ko -->
	</div>

	{** モーダル ============================================== **}
	<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-bind="attr:{ id: modalName() }">
		<div class="modal-dialog modal-lg">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">データ選択</h4>
				</div>
				<div class="modal-body">
					<button class="btn btn-default" data-bind="click: openFilter">抽出条件</button>
					<table class="table table-striped" data-bind="with: list">
						<thead>
							<tr>
								<th>&nbsp;</th>
								<!-- ko foreach: columns -->
								<th data-bind="text: label"></th>
								<!-- /ko -->
							</tr>
						</thead>
						<tbody data-bind=" foreach: rows">
					    	<tr>
					    		<td><a href="#" data-bind="click: $parents[1].selectRow.bind($parents[1])">選択</a></td>
					    		<!-- ko foreach: values -->
					    			<!-- ko template: { name: function() { return 'index-'+type+'-tpl';} } -->
			  						<!-- /ko -->
					    		<!-- /ko -->
							</tr>
						</tbody>
					</table>
					<ul class="pager" data-bind="with: list ">
			  			<li data-bind="css: { disabled: !hasPrev() } "><a href="#" data-bind=" click: prevList">前へ</a></li>
			  			<li data-bind="css: { disabled: !hasNext() } "><a href="#" data-bind=" click: nextList ">次へ</a></li>
					</ul>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
				</div>
			</div>
		</div>
	</div>

	{** 抽出条件モーダル ============================================== **}
	<!-- Modal -->
	<div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-bind="attr:{ id: modalName('_filter') },with: filterModal">
	  <div class="modal-dialog">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	        <h4 class="modal-title" id="myModalLabel">抽出条件設定</h4>
	      </div>
	      <div class="modal-body">
	      	<!-- ko if: errors().length > 0 -->
				<div class="alert alert-warning" data-bind="foreach: errors">
					<span data-bind="text: $data.message"></span><br>
				</div>
			<!-- /ko -->
	      	<form action="">
		        <section>
		        	<div class="section-header">
		        		<h5>絞り込み</h5>
		        	</div>
		        	<div class="section-body">
		        		<div class="form-group">
		        			<div class="controls">
		        				<select class="form-control input-sm" data-bind="options: boolTypeOptions, optionsText: 'name', optionsValue: 'id', value: boolOperator"></select>
		        			</div>
		        		</div>
		        		<div class="form-group" data-bind="foreach: conditionTerms">
		        			<div class="row">
		        				<div class="col-xs-2">
		        					<select class="form-control input-sm" data-bind="options: $parent.searchableOptions, optionsText: 'label', optionsValue: 'id', value: id"></select>
		        				</div>
		        				<div class="col-xs-2" data-bind="if: operatorOptions">
	        						<select class="form-control input-sm" data-bind="options: operatorOptions, optionsText: 'name', optionsValue: 'id', value: operator"></select>
	        					</div>
	        					<div class="col-xs-5">
	        						<div class="row">
	        						<!-- ko if: datePresetOptions -->
	        							<div class="col-xs-5">
		        							<select class="form-control input-sm" data-bind="options: datePresetOptions, optionsText: 'name', optionsValue: 'id', value: datePreset"></select>
		        						</div>
	        						<!-- /ko -->
	        						<!-- ko switch: valueType -->
		        						<!-- ko case: 'options' -->
	        							<div class="col-xs-12">
		        							<select class="" data-bind="options: valueOptionsOptions, optionsText: 'name', optionsValue: 'id', selectedOptions: values, select2: true" multiple="true"></select>
	        							</div>
		        						<!-- /ko -->
										<!-- ko case: 'userGroupOptions' -->
										<div class="col-xs-12">
											<select class="" data-bind="foreach: userGroupOptions, optionsText: 'name', optionsValue: 'id', selectedOptions: values, select2: true" multiple="true">
												<optgroup data-bind="attr: { label: label}, foreach: children">
													<option data-bind="text: name, value: id" ></option>
												</optgroup>
											</select>
										</div>
										<!-- /ko -->
		        						<!-- ko case: 'date' -->
		        						<div class="col-xs-7">
		       								<div class="input-group datepicker">
												<input type="text" class="form-control" data-bind="datepicker: value">
												<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
											</div>
										</div>
		        						<!-- /ko -->
		        						<!-- ko case: 'datetime' -->
		        						<div class="col-xs-7">
		       								<div class="input-group datepicker">
												<input type="text" class="form-control" data-bind="datetimepicker: value">
												<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
											</div>
										</div>
										<!-- /ko -->
		        						<!-- ko case: 'time' -->
		        						<div class="col-xs-12">
		       								<div class="input-group datepicker">
												<input type="text" class="form-control" data-bind="timepicker: value">
												<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
											</div>
										</div>
		        						<!-- /ko -->
		        						<!-- ko case: 'days' -->
		        						<div class="col-xs-7">
		        							<select class="form-control input-sm" data-bind="options: valueOptionsDays, optionsText: 'name', optionsValue: 'id', value: value"></select>
		        						</div>
		        						<!-- /ko -->
		        						<!-- ko case: 'text' -->
		        						<div class="col-xs-12">
	       									<input type="text" class="form-control input-sm" data-bind="value: value">
	       								</div>
		        						<!-- /ko -->
	        						<!-- /ko -->
	        						</div>
		        				</div>
		        				<div class="col-xs-3">
	        						<button class="btn btn-sm btn-default" data-bind="click: $parent.addConditionTerm.bind($parent, $data)">追加</button>
	        						<button class="btn btn-sm btn-default" data-bind="click: $parent.removeConditionTerm.bind($parent, $data)">削除</button>
		        				</div>
		        			</div>
		        		</div>
		        	</div>
		        </section>
		        <section>
		        	<div class="section-header">
		        		<h5>ソート</h5>
		        	</div>
		        	<div class="section-body" data-bind="foreach: sortTerms">
	       				<div class="row">
	       					<div class="col-xs-4">
	       						<select class="form-control input-sm" data-bind="options: $parent.sortableOptions, optionsText: 'label', optionsValue: 'id', value: id"></select>
	       						{**<span data-bind="text: id"></span>**}
	       					</div>
	       					<div class="col-xs-4">
	       						<select class="form-control input-sm" data-bind="options: $parent.sortTypeOptions, optionsText: 'name', optionsValue: 'id', value: type"></select>
	       					</div>
	       					<div class="col-xs-4">
	       						<button class="btn btn-sm btn-default" data-bind="click: $parent.addSortTerm.bind($parent, $data)">追加</button>
	       						<button class="btn btn-sm btn-default" data-bind="click: $parent.removeSortTerm.bind($parent, $data)">削除</button>
	       					</div>
	        			</div>
		        	</div>
		        </section>
	        </form>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-primary" data-bind="click: apply">適用</button>
	        <button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
	      </div>
	    </div>
	  </div>
</div>

</script>

{** レコード番号 =========================================================== **}
<script type="text/html" id="create-rownum-tpl">
	<div class="form-group field-rownum required">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<p class="form-control-static">（自動）</p>
	</div>
</script>

{** 作成者 =========================================================== **}
<script type="text/html" id="create-created_by-tpl">
	<div class="form-group field-created_by required">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<p class="form-control-static">（自動）</p>
	</div>
</script>

{** 作成日時 =========================================================== **}
<script type="text/html" id="create-created_on-tpl">
	<div class="form-group field-created_on required">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<p class="form-control-static">（自動）</p>
	</div>
</script>

{** 更新者 =========================================================== **}
<script type="text/html" id="create-updated_by-tpl">
	<div class="form-group field-updated_by required">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<p class="form-control-static">（自動）</p>
	</div>
</script>

{** 更新日時 =========================================================== **}
<script type="text/html" id="create-updated_on-tpl">
	<div class="form-group field-updated_on required">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<p class="form-control-static">（自動）</p>
	</div>
</script>

{** ラベル =========================================================== **}
<script type="text/html" id="create-label-tpl">
	<div class="form-group field-label">
		<div class="form-control-static extension-point" data-bind="html: labelHtml"></div>
	</div>
</script>

{** スペース =========================================================== **}
<script type="text/html" id="create-space-tpl">
	<div class="form-group field-space extension-point" data-bind="style:{ 'min-height': sizeHeight + 'em' }">
	</div>
</script>


{** 罫線 =========================================================== **}
<script type="text/html" id="create-line-tpl">
	<div class="form-group field-line">
		<hr>
	</div>
</script>

{** タブグループ =========================================================== **}
<script type="text/html" id="create-tab_group-tpl">
	<div class="field-tab_group">
		<div class="header">
			<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		</div>
		<ul class="nav nav-tabs">
		<!-- ko foreach: tabs -->
			<li data-bind="css: { active: $data == $parent.activeTab()}"><a class="field-tab-label" data-bind="attr: { href: '#' + idName() + '_tab-pane'}, css: { error: error }" data-toggle="tab"><span data-bind="text: label"></span></a></li>
		<!-- /ko -->
		</ul>
		<div class="tab-content">
		<!-- ko foreach: tabs -->
			<div class="tab-pane" data-bind="tab:{ afterRender: afterRender.bind($data) }, css: { active: $data == $parent.activeTab() }, attr: { id: idName() + '_tab-pane'}, foreach: matrix ">
						<div class="row db-record_block" data-bind="foreach: $data">
							<div data-bind="css2: $data.span, css2Prefix: 'col-sm-', attr:{ 'data-fieldcode': $data.field.code }">
								<!--  ko with: $data.field -->
									<!-- ko template: { name: function() { return 'create-'+type+'-tpl';}, afterRender: afterRender.bind($data) } -->
									<!-- /ko -->
								<!--  /ko -->
							</div>
						</div>
			</div><!-- tab-pane -->
		<!-- /ko -->
		</div><!-- tab-content -->
	</div>
</script>
