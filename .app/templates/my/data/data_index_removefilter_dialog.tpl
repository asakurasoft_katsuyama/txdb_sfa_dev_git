<div class="modal fade" id="removefilterModal" tabindex="-1" role="dialog" aria-labelledby="removefilterModalLabel" aria-hidden="true" data-bind="with: filterModal.filterlistModal.removefilterModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="removefilterModalLabel">削除確認</h4>
			</div>
			<div class="modal-body">
				この抽出条件を削除します。よろしいですか？<br>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-bind="click: remove">削除する</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
			</div>
		</div>
	</div>
</div>
