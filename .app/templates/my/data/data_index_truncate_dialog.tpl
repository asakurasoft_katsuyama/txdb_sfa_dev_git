<div class="modal fade" id="truncateModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-bind="with: truncateModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">データの一括削除</h4>
			</div>
			<div class="modal-body">
				現在の抽出条件に一致するデータを削除します。よろしいですか？<br>
				※ 最大{get_config('app','data_truncate_num_limit')}件が削除されます。
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-bind="click: execute">削除する</button>
				<button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
			</div>
		</div>
	</div>
</div>
