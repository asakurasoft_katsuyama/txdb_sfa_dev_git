<div class="modal fade" id="readCommentModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
	 aria-hidden="true" data-bind="with: readCommentModal">
	<form data-bind="submit: update">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h4 class="modal-title" id="myModalLabel">コメントの変更</h4>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-group">
							<div class="controls">
								<label class="control-label">コメント</label>
								<input type="text" class="form-control" data-bind="value: comment">
							</div>
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary save">変更</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
				</div>
			</div>
		</div>
	</form>
</div>
