<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
{include file="include/head.tpl" title="データ編集" features=['datepicker', 'select2', 'fileupload', 'lightbox', 'typeahead']}
<link rel="stylesheet" type="text/css" href="/css/page/data_index.css?{get_ver_query()|escape}">
<link rel="stylesheet" type="text/css" href="/css/page/data_create.css?{get_ver_query()|escape}">
{if preg_match('/(iPhone|iPad)/i', $smarty.server.HTTP_USER_AGENT) }
<link rel="stylesheet" type="text/css" href="/css/page/data_create_ios.css?{get_ver_query()|escape}">
{/if}

</head>
<body data-bind="ajaxLoadingSpy: ajaxLoading">

<div id="content" class="db-list_content">
	<div class="container">
		<ol class="breadcrumb">
			<li>データベース入力</li>
			<li>{$table->getName()|escape}</li>
		</ol>
		<nav class="page-nav clearfix" data-spy="affix" data-offset-top="157">
				<button class="btn btn-primary big-btn save" data-bind="click: save, disable: ajaxLoading">決定</button>
					<button class="btn btn-default big-btn" data-bind="click: cancel">キャンセル</button>
				<div id="row-header-menu-space" class="extension-point"></div>
		</nav>

		<div class="panel-list" data-bind="foreach: forms">
			<div class="panel panel-default" data-target="form" data-bind="attr:{ 'data-formname': formName}">
				<div class="panel-body">
					<!-- ko if: error -->
					<div class="alert alert-danger clearfix" data-bind="text: error"></div>
					<!-- /ko -->

					<div class="form" role="form" id="data-create-form" data-bind="foreach: matrix">
						<div class="row db-record_block" data-bind="foreach: $data">
							<div data-bind="css2: $data.span, css2Prefix: 'col-sm-', attr:{ 'data-fieldcode': $data.field.code}">
								<!--  ko with: $data.field -->
									<!-- ko template: { name: function() { return 'create-'+type+'-tpl';}, afterRender: afterRender.bind($data) } -->
									<!-- /ko -->
								<!--  /ko -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
				
	</div>
</div>

{include file="my/data/data_create_types.tpl"}
{include file="my/data/data_index_types.tpl"}
{include file="include/js_bottom.tpl" features=['datepicker', 'ckeditor', 'fileupload', 'select2', 'lightbox', 'typeahead']}
<script type="text/javascript" src="/js/view/base/filter_model.js?{get_ver_query()|escape}"></script>
<script type="text/javascript" src="/js/view/base/list_model.js?{get_ver_query()|escape}"></script>
<script type="text/javascript" src="/js/view/data_create.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	var options = {
		tableId: {$table->getId()|escape},
		{if isset($row)}rowId: {$row->getId()|default:'null'|escape},{/if}
		nested: true,
		rowKey: '{$rowKey|escape}'
	};
	viewModel = new ViewModel(options);
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
