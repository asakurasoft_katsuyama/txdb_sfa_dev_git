<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
{include file="include/head.tpl" title="データ表示" features=['lightbox', 'select2']}
<link rel="stylesheet" type="text/css" href="/css/lightbox/ekko-lightbox.min.css">
<link rel="stylesheet" type="text/css" href="/css/page/data_index.css?{get_ver_query()|escape}">
<link rel="stylesheet" type="text/css" href="/css/page/data_view.css?{get_ver_query()|escape}">
</head>
<body>

<div id="content" class="db-list_content" >
	<div class="container">
		<ol class="breadcrumb">
			<li>データベース入力</li>
			<li>{$table->getName()|escape}</li>
		</ol>


		<nav class="page-nav">
			<a class="btn btn-default" href="javascript: window.close();">閉じる</a>
			<div id="row-header-menu-space" class="extension-point"></div>
		</nav><br class="clearfix" />

		<div class="panel panel-default data-view-panel">
			<div class="panel-body">
				<div class="form">
				<!--  ko foreach: fields -->
					<div class="row">
					<!--  ko foreach: $data -->
						<div data-bind="css2: $data.span, css2Prefix: 'col-sm-', attr:{ 'data-fieldcode': $data.field.code}">
							<!--  ko with: $data.field -->
								<!-- ko template: { name: function() { return 'view-'+type+'-tpl';}, afterRender: afterRender.bind($data) } -->
			  					<!-- /ko -->
							<!--  /ko -->
						</div>
					<!--  /ko -->
					</div>
				<!--  /ko -->
				</div>
			</div>
		</div>
	</div>
</div>

{include file="my/data/data_view_types.tpl"}
{include file="my/data/data_index_types.tpl"}
{include file="include/js_bottom.tpl" features=['lightbox', 'select2']}
<script type="text/javascript" src="/js/lightbox/ekko-lightbox.min.js"></script>
<script type="text/javascript" src="/js/view/base/list_model.js?{get_ver_query()|escape}"></script>
<script type="text/javascript" src="/js/view/data_view.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	viewModel = new ViewModel({$table->getId()|escape}, {$row->getId()|escape}, {$offset|escape}, {$notificationId|escape}, true);
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
