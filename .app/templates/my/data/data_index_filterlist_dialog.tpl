<!-- Modal -->
<div class="modal fade" id="filterlistModal" tabindex="-1" role="dialog" aria-labelledby="filterlistModalLabel" aria-hidden="true" data-bind="with: filterModal">
  <div class="modal-dialog" data-bind="with: filterlistModal">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="filterlistModalLabel">抽出条件一覧</h4>
      </div>
      <div class="modal-body">
      	<!-- ko if: errors().length > 0 -->
			<div class="alert alert-warning" data-bind="foreach: errors">
				<span data-bind="text: $data.message"></span><br>
			</div>
		<!-- /ko -->
		<p>抽出条件一覧（全ユーザー共通）を編集できます。<br></p>
      	<form action="">
       		<div class="form-group">
       			<div class="controls">
       				<!-- ko if:filters().length == 0 -->
       				<div class="alert alert-info">
       					<p>登録されていません。</p>
       				</div>
       				<!-- /ko -->
       				<!-- ko if:filters().length > 0 -->
       				<table class="table">
       					<tbody data-bind="foreach: filters">
       						<tr>
       							<!-- ko if:isEditing() -->
       							<td><input type="text" data-bind="value: name" class="form-control"></td>
       							<td><button class="btn btn-default" data-bind="click: $parent.save.bind($parent)">保存</button></td>
       							<td><button class="btn btn-default" data-bind="click: $parent.cancel.bind($parent)">戻す</button></td>
       							<td><button class="btn btn-default" data-bind="click: $parent.remove.bind($parent)">削除</button></td>
       							<!-- /ko -->
       							<!-- ko ifnot:isEditing() -->
       							<td data-bind="text: name"></td>
       							<td></td>
       							<td><button class="btn btn-default" data-bind="click: $parent.edit.bind($parent)">変更</button></td>
       							<td><button class="btn btn-default" data-bind="click: $parent.remove.bind($parent)">削除</button></td>
       							<!-- /ko -->
       						</tr>
       					</tbody>
       				</table>
       				<!-- /ko -->
       			</div>
       		</div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
      </div>
    </div>
  </div>
</div>

