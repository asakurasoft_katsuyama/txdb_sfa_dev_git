<div class="modal fade" id="subtotalModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-bind="with: subtotalModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">集計方法</h4>
			</div>
			<div class="modal-body">
				<form class="form-inline">
					<select data-bind="options: funcOptions, optionsValue: 'id', optionsText: 'name', value: func" class="form-control"></select>
					<!-- ko if: fieldOptions() != null -->
					<select data-bind="options: fieldOptions, optionsValue:'id', optionsText:'label', value: field" class="form-control"></select>
					<!-- /ko -->
				</form>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary" data-bind="click: apply">OK</button>
				<button type="button" class="btn btn-danger" data-dismiss="modal">キャンセル</button>
			</div>
		</div>
	</div>
</div>
