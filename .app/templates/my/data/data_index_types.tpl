{** このテンプレートは、data_createでも使われているので注意! **}

<script type="text/html" id="index-text-tpl">
<td class="col-text">
	<span data-bind="text: text"></span>
</td>
</script>

<script type="text/html" id="index-multitext-tpl">
<td class="col-multitext">
	<span data-bind="text: text"></span>
</td>
</script>

<script type="text/html" id="index-multitext-2-tpl">
<td class="col-multitext">
	<p class="form-control-static" data-bind="html: html"></p>
</td>
</script>

<script type="text/html" id="index-richtext-tpl">
<td class="col-richtext">
	<span data-bind="text: text"></span>
</td>
</script>

<script type="text/html" id="index-richtext-2-tpl">
<td class="col-richtext">
	<div class="form-control-static" data-bind="html: value"></div>
</td>
</script>

<script type="text/html" id="index-number-tpl">
<td class="col-number">
	<span data-bind="text: text"></span>
</td>
</script>

<script type="text/html" id="index-calc-tpl">
<td class="col-calc">
	<span data-bind="text: text"></span>
</td>
</script>


<script type="text/html" id="index-radio-tpl">
<td class="col-radio">
	<span data-bind="text: text"></span>
</td>
</script>

<script type="text/html" id="index-checkbox-tpl">
<td class="col-checkbox">
	<!-- ko foreach: text -->
	<span data-bind="text: $data"></span><br>
	<!-- /ko -->
</td>
</script>

<script type="text/html" id="index-dropdown-tpl">
<td class="col-dropdown">
	<span data-bind="text: text"></span>
</td>
</script>
<script type="text/html" id="index-file-tpl">
<td class="col-file">
	<ul data-bind="foreach: value" class="list-unstyled" >
		<!-- ko switch: true -->
		<!-- ko case: thumbnailUrl -->
			<li><div class="thumbnail"><a href="#" data-bind="lightbox: { source:$root.namedFileUrl($data) , type:'image' }"><img data-bind="attr : { src: thumbnailUrl }" ></a></div></li>
		<!-- /ko -->
		<!-- ko case: $else -->
			<li><a href="#" data-bind="text: fileName, click: $root.downloadFile.bind($root)"></a></li>
		<!-- /ko -->
		<!-- /ko -->
	</ul>
</td>
</script>

<script type="text/html" id="index-file-inputting-tpl">
<td class="col-file">
	<ul data-bind="foreach: value" class="list-unstyled" >
		<li><span data-bind="text: fileName"></span></li>
	</ul>
</td>
</script>

<script type="text/html" id="index-user_selector-tpl">
<td class="col-user_selector">
	<!-- ko foreach: text -->
	<span data-bind="text: $data"></span><br>
	<!-- /ko -->
</td>
</script>

<script type="text/html" id="index-date-tpl">
<td class="col-date">
	<span data-bind="text: text"></span>
</td>
</script>

<script type="text/html" id="index-time-tpl">
<td class="col-time">
	<span data-bind="text: text"></span>
</td>
</script>

<script type="text/html" id="index-datetime-tpl">
<td class="col-datetime">
	<span data-bind="text: text"></span>
</td>
</script>

<script type="text/html" id="index-datecalc-tpl">
<td class="col-datecalc">
	<span data-bind="text: text"></span>
</td>
</script>

<script type="text/html" id="index-lookup-tpl">
<td class="col-lookup">
	<span data-bind="text: text"></span>
</td>
</script>

<script type="text/html" id="index-link-tpl">
<td class="col-link">
	<span data-bind="html: html "></span>
</td>
</script>

<script type="text/html" id="index-rownum-tpl">
<td class="col-rownum">
	<span data-bind="text: text"></span>
</td>
</script>

<script type="text/html" id="index-created_by-tpl">
<td class="col-created_by">
	<span data-bind="text: text"></span>
</td>
</script>

<script type="text/html" id="index-created_on-tpl">
<td class="col-created_on">
	<span data-bind="text: text"></span>
</td>
</script>

<script type="text/html" id="index-updated_by-tpl">
<td class="col-updated_by">
	<span data-bind="text: text"></span>
</td>
</script>

<script type="text/html" id="index-updated_on-tpl">
<td class="col-updated_on">
	<span data-bind="text: text"></span>
</td>
</script>

<script type="text/html" id="index-aggregate_type-tpl">
<td class="col-aggregate_type">
	<span data-bind="text: text"></span>
</td>
</script>
