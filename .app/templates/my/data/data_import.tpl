<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
{include file="include/head.tpl" title="インポート" features=['fileupload']}
<style>
.import-columns .required:after {
  content: "*";
  color: red;
}
</style>
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="home"}
</div>

<div id="content" class="db-list_content" >
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="/my/">データベース入力</a></li>
			<li><a href="/my/data/?t={$table->getId()}">{$table->getName()|escape}</a></li>
		  	<li class="active">インポート</li>
		</ol>

		<div class="page-header">
			<h1>インポート</h1>
		</div>
		<!-- ko if: error -->
		<div class="alert alert-warning" data-bind="text: error"></div>
		<!-- /ko -->
		<!-- ko if: message -->
		<div class="alert alert-info" data-bind="text: message"></div>
		<!-- /ko -->
		<form id="data-import-form1" class="form" role="form">
			<fieldset>
				<legend>1. 最新のインポート用フォーマットをダウンロードし、インポートするデータを入力してください。</legend>
				<ul class="import-columns">
					{foreach $fields as $column}
						{if $column->getType() eq "checkbox"}
							{foreach $column->getOptions() as $option}
								<li><span{if $column->isRequired()} class="required"{/if}>{$column->getLabel()|escape}</span>[{$option->getName()}]</li>
							{/foreach}
						{else}
						<li><span{if $column->hasProperty('isRequired') && $column->isRequired()} class="required"{/if}>{$column->getLabel()|escape}</span></li>
						{/if}
					{/foreach}
				</ul>
				<div class="form-group">
					<label class="control-label">文字コード</label>
					<select class="form-control" data-bind="value: downloadCharset">
						<option value="Shift_JIS">Shift_JIS</option>
						<option value="UTF-8">UTF-8</option>
					</select>
				</div>
			</fieldset>
			<button type="button" class="btn btn-default btn-primary" data-bind="click: doDownload">フォーマットのダウンロード</button>
		</form>
		<form id="data-import-form2" class="form" role="form">
			<fieldset>
				<legend>2．インポートするファイルを選択し、インポートを実行してください。</legend>
				<div class="form-group">
					<div>
						<span class="btn btn-default fileinput-button">
							<span>参照</span>
							<input type="file" class="form-control fileupload" data-url="/my/data/upload_import_file.json" name="files[]">
						</span>
						<p class="help-block">(100MB、{get_config('app','data_import_num_limit')}データまで)</p>
					</div>
					<!-- ko if:file -->
					<table class="table" data-bind="with: file">
						<tbody>
							<tr>
								<td data-bind="text: fileName">&nbsp;</td>
								<td><button class="btn btn-default" data-bind="click: $parent.removeFile">キャンセル</button></td>
							</tr>
						</tbody>
					</table>
					<!-- /ko -->
				</div>
				<div class="form-group">
					<label class="control-label">文字コード</label>
					<select class="form-control" data-bind="value: importCharset">
						<option value="Shift_JIS">Shift_JIS</option>
						<option value="UTF-8">UTF-8</option>
					</select>
				</div>
				<div class="form-group">
					<label class="control-label">更新の際の検索キー</label>
					<div class="form-inline">
						すでに存在する
						<select class="form-control" data-bind="value: keyColumnId, options: keyOptions, optionsValue:'id', optionsText:'label'">
						</select>
						が指定された場合はそのデータが更新されます。
					</div>
					<!-- ko switch: useRownumKey -->
					<p class="help-block" data-bind="case: true">
						※ CSV中、上記検索キーの値として空欄が指定された場合は、そのデータは追加対象となります。
					</p>
					<p class="help-block" data-bind="case: false">
						※ CSV中、上記検索キーの値として空欄が指定された場合は、そのデータは追加対象となります。（ただし、検索キーが入力必須フィールドである場合は空欄にはできません。）<br>
						<span style="color:red">
						※ CSV中、「<span data-bind="text: rownumColumn.label"></span>」フィールドの列の値は無視されます。<br>
						※ CSV中、上記検索キーの値としてデータベースへまだ登録されていない値がある場合、そのデータは追加対象となります。
						</span>
					</p>
					<!-- /ko -->
				</div>
			</fieldset>
			<button type="button" class="btn btn-default btn-primary" data-bind="click: doImport">実行</button>
			<a class="btn btn-warning" href="/my/data/?t={$table->getId()}">戻る</a>
		</form>
		<div>&nbsp;</div>
	</div>
</div>
<div id="footer">
	{include file="include/footer.tpl"}
</div>

{include file="include/js_bottom.tpl" features=['fileupload']}
<script type="text/javascript" src="/js/view/data_import.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	var options ={
		tableId : {$table->getId()}
	};
	viewModel = new ViewModel(options);
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
