<!-- Modal -->
<div class="modal fade" id="savefilterModal" tabindex="-1" role="dialog" aria-labelledby="savefilterModalLabel" aria-hidden="true" data-bind="with: filterModal">
  <div class="modal-dialog" data-bind="with: savefilterModal">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="savefilterModalLabel">抽出条件の保存</h4>
      </div>
      <div class="modal-body">
      	<!-- ko if: errors().length > 0 -->
			<div class="alert alert-warning" data-bind="foreach: errors">
				<span data-bind="text: $data.message"></span><br>
			</div>
		<!-- /ko -->
		<p>抽出条件一覧（全ユーザー共通）に保存します。<br></p>
      	<form action="">
       		<div class="form-group">
       			<label class="control-label">名前を付けて新規保存</label>
       			<div class="controls">
       				<div class="row">
       					<div class="col-xs-10">
		       				<input type="text" class="form-control" data-bind="value: name" maxlength="64">
       					</div>
       					<div class="col-xs-2">
       						<button type="button" data-bind="click: saveAs" class="btn btn-default">保存</button>
       					</div>
       				</div>
       			</div>
       		</div>
       		<!-- ko if: filters().length > 0 -->
       		<div class="form-group">
       			<label class="control-label">上書き保存</label>
       			<div class="controls">
       				<table class="table">
       					<tbody data-bind="foreach: filters">
       						<tr>
       							<td data-bind="text: name"></td>
       							<td><button class="btn btn-default" data-bind="click: $parent.saveOverwrite.bind($parent)">保存</button></td>
       						</tr>
       					</tbody>
       				</table>
       			</div>
       		</div>
       		<!-- /ko -->
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
      </div>
    </div>
  </div>
</div>

