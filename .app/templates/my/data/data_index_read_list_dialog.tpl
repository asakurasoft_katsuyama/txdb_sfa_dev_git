<div class="modal fade" id="readListModal" tabindex="-1" role="dialog"
	 aria-labelledby="myModalLabel" aria-hidden="true" data-bind="with: readListModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="myModalLabel">回覧板</h4>
			</div>
			<div class="modal-body" data-bind="with: list">
				<div class="panel panel-default">
					<table class="table table-striped">
						<thead>
						<tr>
							<th class="col-state">閲覧済み</th>
							<th class="col-user">ユーザー</th>
							<th class="col-read-on">閲覧日</th>
							<th class="col-comment">コメント</th>
						</tr>
						</thead>
						<tbody data-bind="foreach: rows">
						<tr>
							<td class="col-state">
								<!-- ko if: readOn !== null -->
								<i class="fa fa-check" ></i>
								<!-- /ko -->
							</td>
							<td class="col-user">
								<!-- ko if: readUser -->
								<span data-bind="text: readUser.screenName"></span>
								<!-- /ko -->
								<!-- ko ifnot: readUser -->
								<span>*削除されたユーザー*</span>
								<!-- /ko -->
							</td>
							<td class="col-read-on" data-bind="text: ko.utils.formatDate(readOn, 'YYYY-MM-DD HH:mm')"></td>
							<td class="col-comment" data-bind="text: comment">コメント</td>
						</tr>
						</tbody>
					</table>
				</div>
				<ul class="pager">
					<li data-bind="css: { disabled: !hasPrev() } "><a href="#" data-bind=" click: prevList">前へ</a></li>
					<li data-bind="css: { disabled: !hasNext() } "><a href="#" data-bind=" click: nextList ">次へ</a></li>
				</ul>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">閉じる</button>
			</div>
		</div>
	</div>
</div>
