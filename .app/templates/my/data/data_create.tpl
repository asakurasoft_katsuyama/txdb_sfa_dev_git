<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
{include file="include/head.tpl" title="データ編集" features=['datepicker', 'select2', 'fileupload', 'lightbox', 'typeahead']}
<link rel="stylesheet" type="text/css" href="/css/page/data_index.css?{get_ver_query()|escape}">
<link rel="stylesheet" type="text/css" href="/css/page/data_create.css?{get_ver_query()|escape}">
{if preg_match('/(iPhone|iPad)/i', $smarty.server.HTTP_USER_AGENT) }
<link rel="stylesheet" type="text/css" href="/css/page/data_create_ios.css?{get_ver_query()|escape}">
{/if}
{include "include/css_head__extension.tpl"}
</head>
<body data-bind="ajaxLoadingSpy: ajaxLoading">

<div id="header">
	{include file="include/header.tpl" active="home"}
</div>

<div id="content" class="db-list_content">
	<div class="container">
		{if get_config('app', 'features.breadcrumb_edit', 1) }
		<ol class="breadcrumb">
			<li><a href="/my/">データベース入力</a></li>
			<li><a href="/my/data/?t={$table->getId()}">{$table->getName()|escape}</a></li>
		</ol>
		{/if}
		{if get_config('app', 'features.headding_edit', 1) }
		<div class="database-title">
			<h3>{$table->getName()|escape}</h3>
		</div>
		{/if}
		<nav class="page-nav clearfix" data-spy="affix" data-offset-top="157">
				<button class="btn btn-primary big-btn save" data-bind="click: save, disable: ajaxLoading">保存</button>
					<button class="btn btn-warning big-btn" data-bind="click: cancel">戻る</button>
				<div id="row-header-menu-space" class="extension-point"></div>
		</nav>

		<div class="panel-list" data-bind="foreach: forms">
			<div class="panel panel-default" data-target="form" data-bind="attr:{ 'data-formname': formName}">
				<div class="panel-body">
					<!-- ko if: error -->
					<div class="alert alert-danger clearfix" data-bind="text: error"></div>
					<!-- /ko -->

					<div class="form" role="form" id="data-create-form" data-bind="foreach: matrix">
						<div class="row db-record_block" data-bind="foreach: $data">
							<div data-bind="css2: $data.span, css2Prefix: 'col-sm-', attr:{ 'data-fieldcode': $data.field.code}">
								<!--  ko with: $data.field -->
									<!-- ko template: { name: function() { return 'create-'+type+'-tpl';}, afterRender: afterRender.bind($data) } -->
									<!-- /ko -->
								<!--  /ko -->
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- ko if: $parent.mode == 'new' -->
			<nav class="bottom-navbar nav navbar">
				<button class="btn btn-primary " data-bind="click: $parent.addRow.bind($parent)"><i class="glyphicon glyphicon glyphicon-chevron-down"></i> 追加</button>
				<button class="btn btn-primary " data-bind="click: $parent.addCopyRow.bind($parent)"><i class="glyphicon glyphicon glyphicon-chevron-down"></i> データをコピーして追加</button>
				<button class="btn btn-primary " data-bind="click: $parent.removeRow.bind($parent), disable: $parent.forms().length <= 1">削除</button>
			</nav>
			<!-- /ko -->
		</div>

	</div>
</div>
<div id="footer">
	{include file="include/footer.tpl"}
</div>

{include file="my/data/data_create_types.tpl"}
{include file="my/data/data_index_types.tpl"}
{include file="include/js_bottom.tpl" features=['datepicker', 'ckeditor', 'fileupload', 'select2', 'lightbox', 'typeahead']}
<script type="text/javascript" src="/js/view/base/filter_model.js?{get_ver_query()|escape}"></script>
<script type="text/javascript" src="/js/view/base/list_model.js?{get_ver_query()|escape}"></script>
<script type="text/javascript" src="/js/view/data_create.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	var options = {
		tableId: {$table->getId()|escape},
		{if isset($row)}rowId: {$row->getId()|default:'null'|escape},{/if}
		{if isset($replication) && $replication > 0}replication: true,{/if}
		offset: {$offset|default:0|escape}
	};
	viewModel = new ViewModel(options);
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
