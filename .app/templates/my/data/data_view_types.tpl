

{** 文字列 =========================================================== **}
<script type="text/html" id="view-text-tpl">
	<div class="form-group field-text">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<p class="form-control-static" data-bind="text: text"></p>
	</div>
</script>


{** 文字列（複数行） =========================================================== **}
<script type="text/html" id="view-multitext-tpl">
	<div class="form-group field-multitext">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<p class="form-control-static" data-bind="html: html"></p>
	</div>
</script>


{** リッチエディター =========================================================== **}
<script type="text/html" id="view-richtext-tpl">
	<div class="form-group field-richtext">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<div class="form-control-static" data-bind="html: value"></div>
	</div>
</script>

{** 数値 =========================================================== **}
<script type="text/html" id="view-number-tpl">
	<div class="form-group field-number">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<p class="form-control-static" data-bind="text: text"></<p>
	</div>
</script>

{** 計算 =========================================================== **}
<script type="text/html" id="view-calc-tpl">
	<div class="form-group field-calc">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<p class="form-control-static" data-bind="text: text"></<p>
	</div>
</script>

{** ラジオボタン =========================================================== **}
<script type="text/html" id="view-radio-tpl">
	<div class="form-group field-radio">
		<label class="control-label" data-bind="text: label, visible: showsLabel"></label>
		<p class="form-control-static" data-bind="text: text"></<p>
	</div>
</script>

{** チェックボックス =========================================================== **}
<script type="text/html" id="view-checkbox-tpl">
	<div class="form-group field-checkbox">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<div class="form-control-static" data-bind="foreach: text">
			<span data-bind="text: $data"></span>&nbsp;&nbsp;
			<!-- ko if:$parent.optionsAlignment == 'vertical' --><br><!-- /ko -->
		</div>
	</div>
</script>

{** ドロップダウン =========================================================== **}
<script type="text/html" id="view-dropdown-tpl">
	<div class="form-group field-dropdown">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<p class="form-control-static" data-bind="text: text"></<p>
	</div>
</script>

{** ユーザー選択 =========================================================== **}
<script type="text/html" id="view-user_selector-tpl">
<div class="form-group field-dropdown">
	<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
	<div class="form-control-static" data-bind="foreach: text">
		<span data-bind="text: $data"></span>&nbsp;&nbsp;
	</div>
</div>
</script>

{** 日付 =========================================================== **}
<script type="text/html" id="view-date-tpl">
	<div class="form-group field-date">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<p class="form-control-static" data-bind="text: text"></p>
	</div>
</script>

{** 時刻 =========================================================== **}
<script type="text/html" id="view-time-tpl">
	<div class="form-group field-time">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<p class="form-control-static" data-bind="text: text"></p>
	</div>
</script>

{** 日時 =========================================================== **}
<script type="text/html" id="view-datetime-tpl">
	<div class="form-group field-datetime">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<p class="form-control-static" data-bind="text: text"></p>
	</div>
</script>

{** 日時計算 =========================================================== **}
<script type="text/html" id="view-datecalc-tpl">
	<div class="form-group field-datecalc">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<p class="form-control-static" data-bind="text: text"></<p>
	</div>
</script>

{** 添付ファイル =========================================================== **}
<script type="text/html" id="view-file-tpl">
	<div class="form-group field-file">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<div class="form-control-static">
			<ul class="list-unstyled" data-bind="foreach: value" >
				<!-- ko switch: true -->
				<!-- ko case: thumbnailUrl -->
					<li><div class="thumbnail"><a href="#" data-bind="lightbox: { source:$root.namedFileUrl($data) , type:'image' }"><img data-bind="attr : { src: thumbnailUrl }" ></a></div></li>
				<!-- /ko -->
				<!-- ko case: $else -->
					<li><a href="#" data-bind="text: fileName, click: $root.downloadFile.bind($root)"></a></li>
				<!-- /ko -->
				<!-- /ko -->
			</ul>
		</div>
	</div>
</script>

{** リンク =========================================================== **}
<script type="text/html" id="view-link-tpl">
	<div class="form-group field-link">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<p class="form-control-static" data-bind="html: html"></p>
	</div>
</script>

{** 関連レコード =========================================================== **}
<script type="text/html" id="view-relational-tpl">
	<div class="form-group field-relational">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<!-- ko ifnot: error -->
		<table class="table table-striped" data-bind="with: list">
			<thead>
				<tr>
					<th></th>
					<!-- ko foreach: columns -->
					<th data-bind="text: label"></th>
					<!-- /ko -->
				</tr>
			</thead>
			<!-- ko if:totalRowCount() > 0 -->
			<tbody>
				<!-- ko foreach: rows -->
		    	<tr>
		    		<td><a href="#" data-bind="click: $parents[1].openReferer.bind($parents[1])">参照</a></td>
		    		<!-- ko foreach: values -->
		  			<!-- ko template: { name: function() { return ($('#index-'+type+'-2-tpl').length > 0) ? 'index-'+type+'-2-tpl' : 'index-'+type+'-tpl';} } --><!-- /ko -->
		  			<!-- /ko -->
				</tr>
				<!-- /ko -->
				<!-- ko if: $parents[0].aggregateList.totalRowCount() > 0 -->
				<tr>
					<!-- ko foreach: $parents[0].aggregateList.columns -->
					<td style="border-right: none; background-color: #ffffff;height:2em;"></td>
					<!-- /ko -->
				</tr>
				<tr>
					<!-- ko foreach: $parents[0].aggregateList.columns -->
					<th data-bind="text: label" style="background-color: #ffffff;"></th>
					<!-- /ko -->
				</tr>
					<!-- ko foreach: $parents[0].aggregateList.rows -->
				<tr>
						<!-- ko foreach: values -->
						<!-- ko template: { name: function() { return ($('#index-'+type+'-2-tpl').length > 0) ? 'index-'+type+'-2-tpl' : 'index-'+type+'-tpl';} } --><!-- /ko -->
						<!-- /ko -->
				</tr>
					<!-- /ko -->
				<!-- /ko -->
			</tbody>
			<!-- /ko -->
			<!-- ko if:totalRowCount() == 0 -->
			<tbody><tr><td colspan="100">データがありません。</td></tr></tbody>
			<!-- /ko -->
		</table>
		<ul class="pager" data-bind="with: list">
  			<li data-bind="css: { disabled: !hasPrev() } "><a href="#" data-bind=" click: prevList">前へ</a></li>
  			<li data-bind="css: { disabled: !hasNext() } "><a href="#" data-bind=" click: nextList ">次へ</a></li>
		</ul>
		<!-- /ko -->
		<!-- ko if: error -->
		<div class="alert alert-warning" data-bind="text: error"></div>
		<!-- /ko -->
	</div>
</script>

{** 明細入力 =========================================================== **}
<script type="text/html" id="view-relational_input-tpl">
	<div class="form-group field-relational_input">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<!-- ko ifnot: error -->
		<table class="table table-striped" data-bind="with: list">
			<thead>
				<tr>
					<th></th>
					<!-- ko foreach: columns -->
					<th data-bind="text: label"></th>
					<!-- /ko -->
				</tr>
			</thead>
			<!-- ko if:totalRowCount() > 0 -->
			<tbody data-bind=" foreach: rows">
		    	<tr>
		    		<td><a href="#" data-bind="click: $parents[1].openReferer.bind($parents[1])">閲覧</a></td>
		    		<!-- ko foreach: values -->
		  			<!-- ko template: { name: function() { return ($('#index-'+type+'-2-tpl').length > 0) ? 'index-'+type+'-2-tpl' : 'index-'+type+'-tpl';} } --><!-- /ko -->
		  			<!-- /ko -->
				</tr>
			</tbody>
			<!-- /ko -->
			<!-- ko if:totalRowCount() == 0 -->
			<tbody><tr><td colspan="100">データがありません。</td></tr></tbody>
			<!-- /ko -->
		</table>
		<!-- /ko -->
		<!-- ko if: error -->
		<div class="alert alert-warning" data-bind="text: error"></div>
		<!-- /ko -->
	</div>
</script>

{** ルックアップ =========================================================== **}
<script type="text/html" id="view-lookup-tpl">
	<div class="form-group field-lookup">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<p class="form-control-static" data-bind="css: { emptylink: isEmpty }"><a href="#" data-bind="click: openReferer, text: text" target="_new"></a></p>
	</div>
</script>

{** レコード番号 =========================================================== **}
<script type="text/html" id="view-rownum-tpl">
	<div class="form-group field-rownum">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<p class="form-control-static" data-bind="text: text"></p>
	</div>
</script>

{** 作成者 =========================================================== **}
<script type="text/html" id="view-created_by-tpl">
	<div class="form-group field-created_by">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<p class="form-control-static" data-bind="text: text"></p>
	</div>
</script>

{** 作成日時 =========================================================== **}
<script type="text/html" id="view-created_on-tpl">
	<div class="form-group field-created_on">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<p class="form-control-static" data-bind="text: text"></p>
	</div>
</script>

{** 更新者 =========================================================== **}
<script type="text/html" id="view-updated_by-tpl">
	<div class="form-group field-updated_by">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<p class="form-control-static" data-bind="text: text"></p>
	</div>
</script>

{** 更新日時 =========================================================== **}
<script type="text/html" id="view-updated_on-tpl">
	<div class="form-group field-updated_on">
		<label class="control-label" data-bind="text:label, visible: showsLabel"></label>
		<p class="form-control-static" data-bind="text: text"></p>
	</div>
</script>

{** ラベル =========================================================== **}
<script type="text/html" id="view-label-tpl">
	<div class="form-group field-label">
		<div class="form-control-static extension-point" data-bind="html: labelHtml"></div>
	</div>
</script>

{** スペース =========================================================== **}
<script type="text/html" id="view-space-tpl">
	<div class="form-group field-space extension-point" data-bind="style:{ 'min-height': sizeHeight + 'em' }">
	</div>
</script>

{** 罫線 =========================================================== **}
<script type="text/html" id="view-line-tpl">
	<div class="form-group field-line">
		<hr>
	</div>
</script>

{** タブグループ =========================================================== **}
<script type="text/html" id="view-tab_group-tpl">
	<div class="field-tab_group">
		<div class="header">
			<label class="control-label" data-bind="visible: showsLabel, text:label"></label>
		</div>
		<ul class="nav nav-tabs">
		<!-- ko foreach: tabs -->
			<li data-bind="css: { active: $data == $parent.activeTab() }"><a class="field-tab-label" data-bind="attr: { href: '#' + idName() + '_tab-pane'}" data-toggle="tab"><span data-bind="text: label"></span></a></li>
		<!-- /ko -->
		</ul>
		<div class="tab-content">
		<!-- ko foreach: tabs -->
			<div class="tab-pane" data-bind="tab:{ afterRender: afterRender.bind($data) }, css: { active: $data == $parent.activeTab() }, attr: { id: idName() + '_tab-pane'} ">
				<!--  ko foreach: matrix -->
					<div class="row">
					<!--  ko foreach: $data -->
						<div data-bind="css2: $data.span, css2Prefix: 'col-sm-', attr:{ 'data-fieldcode': $data.field.code}">
							<!--  ko with: $data.field -->
								<!-- ko template: { name: function() { return 'view-'+type+'-tpl';}, afterRender: afterRender.bind($data) } -->
			  					<!-- /ko -->
							<!--  /ko -->
						</div>
					<!--  /ko -->
					</div>
				<!--  /ko -->
			</div><!-- tab-pane -->
		<!-- /ko -->
		</div><!-- tab-content -->
	</div>
</script>
