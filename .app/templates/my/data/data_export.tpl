<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
{include file="include/head.tpl" title="エクスポート"}
<link rel="stylesheet" type="text/css" href="/css/page/data_view.css?{get_ver_query()|escape}">
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="home"}
</div>

<div id="content" class="db-list_content" >
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="/my/">データベース入力</a></li>
			<li><a href="/my/data/?t={$table->getId()}">{$table->getName()|escape}</a></li>
		  	<li class="active">エクスポート</li>
		</ol>

		<div class="page-header">
			<h1>エクスポート</h1>
		</div>
		<!-- ko if: message -->
		<div class="alert alert-info" data-bind="text: message"></div>
		<!-- /ko -->
		<form id="data-export-form" class="form" role="form" id="data-create-form">
			<fieldset>
				<legend>CSVのフォーマット</legend>
				<div class="form-group">
					<label class="control-label">文字コード</label>
					<select class="form-control" data-bind="value: charset">
						<option value="Shift_JIS">Shift_JIS</option>
						<option value="UTF-8">UTF-8</option>
					</select>
				</div>
				<div class="checkbox">
				    <label>
				      <input type="checkbox" data-bind="checked: heading, disable: disableHeading"> 先頭行を項目名にする
				    </label>
				</div>
				<div class="checkbox">
				    <label>
				      <input type="checkbox" data-bind="checked: toImport"> インポートに使用する
				    </label>
				</div>
			</fieldset>
			<button type="button" class="btn btn-default btn-primary" data-bind="click: doExport">実行</button>
			<a class="btn btn-warning" href="/my/data/index?t={$table->getId()}">戻る</a>
		</form>
		<div>&nbsp;</div>
	</div>
</div>
<div id="footer">
	{include file="include/footer.tpl"}
</div>



{include file="include/js_bottom.tpl"}
<script type="text/javascript" src="/js/view/data_export.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	var options ={
		tableId : {$table->getId()}
	};
	viewModel = new ViewModel(options);
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
