<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
{include file="include/head.tpl" title="データ一覧" features=['lightbox', 'datepicker', 'select2']}
<link rel="stylesheet" type="text/css" href="/css/page/data_index.css?{get_ver_query()|escape}">
<style type="text/css">
	table .header-fixed {
		position: fixed;
		top: 40px;
		/*left: 0;*/
		/*right: 0;*/
		z-index: 1020; /* 10 less than .navbar-fixed to prevent any overlap */
		border-bottom: 1px solid #d5d5d5;
		-webkit-border-radius: 0;
		-moz-border-radius: 0;
		border-radius: 0;
	}
</style>
{include "include/css_head__extension.tpl"}
</head>
<body>
<div id="header">
	{include file="include/header.tpl" active="home"}
</div>
<div id="content" class="db-list_content" >
	<div class="container">
		{if get_config('app', 'features.breadcrumb_index', 1) }
		<ol class="breadcrumb">
			<li><a href="/my/">データベース入力</a></li>
			<li class="active">{$table->getName()|escape}</li>
		</ol>
		{/if}
		{if get_config('app', 'features.headding_index', 1) }
		<div class="database-title">
			<h3>{$table->getName()|escape}</h3>
		</div>
		{/if}

	{** カレンダー表示 *******************}
	{if $view eq 'calendar'}

		<!-- ko with: calendar -->

		<div class="nav primary-nav clearfix">
			<div class="user-selector-nav pull-left"><!--  -->
				<form class="form-inline">
					<select class="" data-bind="foreach: userGroupOptions, optionsText: 'name', optionsValue: 'id', selectedOptions: userGroupsTemp, select2: true" multiple="true">
						<optgroup data-bind="attr: { label: label}, foreach: children">
							<option data-bind="text: name, value: id" ></option>
						</optgroup>
					</select>
					<button class="btn btn-primary" data-bind="click: applyFilter">
						{assign var="labeluserselect" value=get_config('app','calendar_userselect_label','確定')}
						<i class="fa fa-search"></i>
						{$labeluserselect}<!-- 絞り込み -->
					</button>
				</form>
			</div>

			<div class="nav date-control-nav pull-left"><!--  -->
				{assign var="labelcal" value=get_config('app','calender_label','')}
				<form class="date-selector form-inline pull-left">
					<div class="input-group datepicker js-start-date" data-bind="event:{ 'dp.change': setStartDate }">
						<input type="text" class="form-control" data-bind="datepicker_v2: startDate">
						<div class="input-group-btn">
							<button class="btn"><span class="glyphicon glyphicon-calendar"></span>{$labelcal}</button>
						</div>
					</div>
				</form>
				<button class="prev-week-btn btn btn-primary" data-bind="click: prevWeek">
					<i class="fa fa-backward"></i>
					前の週
				</button>
				<button class="next-week-btn btn btn-primary" data-bind="click: nextWeek">
					次の週
					<i class="fa fa-forward"></i>
				</button>
			</div>


			<div class="view-selector-nav pull-right">
				<select class="form-control" data-bind="event: { change: $parent.changeView.bind($parent) }">
					<option value="list">一覧表示</option>
					{if $table->isCalendarEnabled()}
					<option value="calendar" selected>カレンダー表示</option>
					{/if}
				</select>
			</div>
		</div>

		<div class="calendar-block">
			<table class="calendar table" >
				<thead>
					<th></th>
					<!-- ko foreach: dateEntries -->
						<th data-bind="css: { today: isToday  }">
							<span data-bind="text: dateFormatted "></span><span data-bind="text: '(' + dayOfWeek + ')' "></span>
						</th>
					<!-- /ko -->
				</thead>
				<tbody data-bind="foreach: userEntries">
					<th data-bind="text: user.screenName"></th>
					<!-- ko foreach: dateEntries -->
					<td data-bind="css: { today: isToday, 'rows-exist': items.length > 0 }">
						<!-- ko foreach: items -->
						<!-- ko if :startTime || endTime-->
							<div class="time"><span class="time-start" data-bind="text: startTime"></span> - <span class="time-end" data-bind="text: endTime"></span></div>
						<!-- /ko -->
							<a href="#" class="title" data-bind="click: $parents[2].viewData.bind($parents[2])"><i class="btn-icon fa fa-file-text-o"></i> <span class="btn-text" data-bind="text: title"></span></a>
						<!-- /ko --><br>
						{if $isWritable && !$table->isChild() }<button class="add-btn btn btn-sm" data-bind="click: $parents[1].addData.bind($parents[1], $parents[0].user)"><i class="fa fa-plus"></i> <span class="btn-text">追加</span></button>{/if}
					</td>
					<!-- /ko -->
				</tbody>
			</table>

			<div class="nav secondary-nav">
				<button class="prev-users-btn btn btn-primary" data-bind="click: prevUser, enable: hasPrevUser">
					<i class="fa fa-backward"></i>
					前の10人
				</button>
				<button class="next-users-btn btn btn-primary" data-bind="click: nextUser, enable: hasNextUser">
					次の10人
					<i class="fa fa-forward"></i>
				</button>
			</div>
		</div>
		<!-- /ko -->

	{/if}

	{** 一覧表示 ************************}
	{if $view eq 'list'}
	<div class="page-nav_block">

	<!-- ui変更 -->
			<div class="pull-right view-change-nav">
				{if $table->isCalendarEnabled()}
					<select class="form-control" data-bind="event: { change: changeView }">
						<option value="list" selected>一覧表示</option>
						<option value="calendar">カレンダー表示</option>
					</select>
				{/if}
			</div>


		{if $isWritable && !$table->isChild() }
		<nav class="page-nav">
			<a class="btn btn-default save" href="/my/data/create?t={$table->getId()|escape}">新規作成</a>
		</nav>
		{/if}
		<nav class="page-nav"><!-- <nav class="page-nav clearfix"> -->
			<form class="form form-inline pull-left">
				<button class="btn btn-default" data-target="#filterModal" data-bind="click: openFilter"><i class="fa fa-search"></i> 検索条件を入力</button>
				<select id="condition-selector" class="form-control" data-bind="populateInitialValue:true, options: filterOptions, optionsText: 'name', optionsValue: 'id', value: filter"></select>
			</form>
		</nav>

		{if $user->isAdmin() or ($table->getOwner() and $user->getId() eq $table->getOwner()->getId()) }
		<nav id="import-nav-container" class="page-nav">
			{if !$table->isChild() }
			<a class="btn btn-default" href="/my/data/import?t={$table->getId()}" data-bind="visible: !importDisabled()">インポート</a>
			<a class="btn btn-default" href="/my/data/export?t={$table->getId()}" data-bind="visible: !exportDisabled()">エクスポート</a>
			{/if}
			{if get_config('app', 'features.delete_all', 1) && !$table->isChild() && !$table->isParent() }
			<button class="btn btn-danger" data-bind=" click: openTruncate" data-target="#truncateModal">一括削除</button>
			{/if}
		</nav>
		{/if}

		<nav class="list-nav">
			<form class="form form-inline ">
				<div id="number-of-records-nav" class="form-group">
					<span data-bind="text: totalRowCount"></span> 件中 <span data-bind="text: offsetStart"></span>〜<span data-bind="text: offsetEnd"></span>件
				</div>
				<div id="limit-selector-nav" class="form-group">
					<label for="limitselector">表示件数</label>
					<select id="limitselector" data-bind="populateInitialValue:true, options: rowLimitOptions, value: rowLimit" class="form-control"></select>
				</div>
			</form>
		</nav>
	</div><!--  -->



		<div class="panel panel-default" style="display: table; width: auto; ">
		  <table class="table table-striped table-fixed-header" style="table-layout: fixed; width: auto; "
				 data-bind="style: { width: fixedWidthMode() ? (tableWidth()+'px'):'' }"
				 _fixedhead="rows:1">
		  	<thead class="header">
		  		<tr>
					{if $isReadable && $table->isReadCheckEnabled() && !$table->isChild()}<th data-bind="style: { width : fixedWidthMode() ? '50px' : '' }, dummy: addFixedColumn()" class="col-read-check">{get_config('app', 'readcheck_label', '閲覧済み')}</th>{/if}
					{if $isReadable && $table->isCommentEnabled() && !$table->isChild()}<th data-bind="style: { width : fixedWidthMode() ? '50px' : '' }, dummy: addFixedColumn()" class="col-comments-count">コメント</th>{/if}
			  		{if $isReadable}<th data-bind="style: { width : fixedWidthMode() ? '50px' : '' }, dummy: addFixedColumn()"></th>{/if}
			  		{if $isWritable && !$table->isChild() }<th data-bind="style: { width : fixedWidthMode() ? '50px' : '' }, dummy: addFixedColumn()"></th>{/if}
			  		<!-- ko foreach: columns -->
			  		<th data-bind="css: $root.sortClassName($data), style: { 'width': $parent.fixedWidthMode() ? (columnWidth+'px'):'' }">
			  			<!-- ko if: $data.isSortable -->
			  			<span class="txdb-table-label" data-bind="text: $data.label, click: $root.sortByColumn.bind($root)"></span>
			  			<!-- /ko -->
			  			<!-- ko if: $data.isSortable == false -->
			  			<!-- ko if: $data.hasOptions -->
			  			<span class="txdb-table-label" data-bind="text: $data.label, click: $root.openSubtotal.bind($root)"></span>
			  			<!-- /ko -->
			  			<!-- ko if: $data.hasOptions == false -->
			  			<span data-bind="text: $data.label"></span>
			  			<!-- /ko -->
			  			<!-- /ko -->
			  			<!-- ko if: $data.hasOptions -->
				  		<a href="#" class="btn-graph" data-bind="click: $root.openSubtotal.bind($root)"><span><i class="fa fa-bar-chart-o"></i></span></a>
				  		<!-- /ko -->
			  			<!-- ko if: $data.isSortable -->
				  		<a href="#" class="btn-sort" data-bind="click: $root.sortByColumn.bind($root)"><span></span></a>
				  		<!-- /ko -->
			  		</th>
					<!-- /ko -->
				</tr>
		  	</thead>
		  	<tbody data-bind=" foreach: rows">
		    	<tr>
					{if $isReadable && $table->isReadCheckEnabled() && !$table->isChild() }
					<td class="col-read-check" title="回覧">
						{if $table->isReadCheckTarget($user)}
							{if $table->getReadCheckSettings()->isOnlistEnabled()}
								<a href="#" class="read-state btn" data-bind="click: $root.toggleRead.bind($root, $data), css:{ 'has-read': hasRead }"><i class="fa fa-check" ></i></a>
							{else}
								<span class="read-state" data-bind="css:{ 'has-read': hasRead }"><i class="fa fa-check" ></i></span>
							{/if}
							<br>
						{/if}
						<a href="#" class="read-users-count" data-bind="click: $root.openReadList.bind($root, $data)"><span data-bind="text: readUsersCount"></span>人</a>
					</td>
					{/if}
					{if $isReadable && $table->isCommentEnabled() && !$table->isChild() }
					<td class="col-comments-count" title="コメント数">
						<!-- ko if:commentsCount > 0 -->
						<i class="fa fa-comment-o"></i> <span data-bind="text: commentsCount"></span>
						<!-- /ko -->
					</td>
					{/if}
			  		{if $isReadable}<td class="col-view hyouji"><a href="#" data-bind="click: $root.openView.bind($root, $index())">閲覧</a></td>{/if}
			  		{if $isWritable && !$table->isChild() }<td class="col-view henshu"><a href="#" data-bind="click: $root.openEdit.bind($root, $index())">編集</a></td>{/if}
			  		<!-- ko foreach: $data.values -->
			  			<!-- ko template: { name: function() { return 'index-'+type+'-tpl';} } -->
			  			<!-- /ko -->
			  		<!-- /ko -->
				</tr>
			</tbody>
		  </table>
		</div>
		<ul class="pager">
  			<li data-bind=" css: { disabled: !hasPrev() } "><a href="#" data-bind=" click: prevList"><i class="fa fa-backward"></i> 前へ</a></li>
  			<li data-bind=" css: { disabled: !hasNext() } "><a href="#" data-bind=" click: nextList ">次へ <i class="fa fa-forward"></i></a></li>
		</ul>
	{/if}
	</div>
</div>
<div id="footer">
	{include file="include/footer.tpl"}
</div>

{include file="my/data/data_index_types.tpl"}
{include file="my/data/data_index_filter_dialog.tpl"}
{include file="my/data/data_index_savefilter_dialog.tpl"}
{include file="my/data/data_index_truncate_dialog.tpl"}
{include file="my/data/data_index_subtotal_dialog.tpl"}
{include file="my/data/data_index_filterlist_dialog.tpl"}
{include file="my/data/data_index_removefilter_dialog.tpl"}
{include file="my/data/data_index_read_list_dialog.tpl"}

{include file="include/js_bottom.tpl" features=['lightbox', 'datepicker', 'select2', 'fixedheader', 'qst']}
<script type="text/javascript" src="/js/view/base/filter_model.js?{get_ver_query()|escape}"></script>
<script type="text/javascript" src="/js/view/base/list_model.js?{get_ver_query()|escape}"></script>
<script type="text/javascript" src="/js/view/data_index.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	var options = {
		tableId: {$table->getId()},
		offset: {$limit[0]|default:0},
		limit: {$limit[1]|default:20},
		filter: {$limit[2]|default:'null'},
		view: '{$view|escape}'
	};

	viewModel = new ViewModel(options);
	ko.applyBindings(viewModel);
	viewModel.afterBinding();

	$(window).on('scroll', function() {
		$('#header').css({
			'width': '100%',
			'marginLeft': $(this).scrollLeft() + 'px'
		});
	});
});
</script>

</body>
</html>
