{*
 * 同じものが、data_create_types.tpl の中にもあるので注意 !!!
 * 絞り込み条件については、同じものが、table_modify_notification.tpl の中にもあるので注意 !!!
 *}
<!-- Modal -->
<div class="modal fade" id="filterModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-bind="with: filterModal">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">検索条件</h4>
      </div>
      <div class="modal-body">
      	<!-- ko if: errors().length > 0 -->
			<div class="alert alert-warning" data-bind="foreach: errors">
				<span data-bind="text: $data.message"></span><br>
			</div>
		<!-- /ko -->
      	<form action="">
	        <section>
	        	<div class="section-header">
	        		<h5>絞り込み</h5>
	        	</div>
	        	<div class="section-body">
	        		<div class="form-group">
	        			<div class="controls">
	        				<select class="form-control input-sm" data-bind="options: boolTypeOptions, optionsText: 'name', optionsValue: 'id', value: boolOperator, attr: { title: boolOperatorText } " ></select>
	        			</div>
	        		</div>
	        		<div class="form-group" data-bind="foreach: conditionTerms">
	        			<div class="row">
	        				<div class="col-xs-2">
	        					<select class="form-control input-sm" data-bind="options: $parent.searchableOptions, optionsText: 'label', optionsValue: 'id', value: id, attr: { title: idText } "></select>
	        				</div>
	        				<div class="col-xs-2" data-bind="if: operatorOptions">
        						<select class="form-control input-sm" data-bind="options: operatorOptions, optionsText: 'name', optionsValue: 'id', value: operator, attr: { title: operatorText } "></select>
        					</div>
        					<div class="col-xs-5">
        						<div class="row">
        						<!-- ko if: datePresetOptions -->
        							<div class="col-xs-5">
	        							<select class="form-control input-sm" data-bind="options: datePresetOptions, optionsText: 'name', optionsValue: 'id', value: datePreset, attr: { title: datePresetOptionText } "></select>
	        						</div>
        						<!-- /ko -->
        						<!-- ko switch: valueType -->
	        						<!-- ko case: 'options' -->
        							<div class="col-xs-12">
	        							<select class="" data-bind="options: valueOptionsOptions, optionsText: 'name', optionsValue: 'id', selectedOptions: values, select2: true" multiple="true"></select>
        							</div>
	        						<!-- /ko -->
									<!-- ko case: 'userGroupOptions' -->
									<div class="col-xs-12">
										<select class="" data-bind="foreach: userGroupOptions, optionsText: 'name', optionsValue: 'id', selectedOptions: values, select2: true" multiple="true">
											<optgroup data-bind="attr: { label: label}, foreach: children">
												<option data-bind="text: name, value: id" ></option>
											</optgroup>
										</select>
									</div>
									<!-- /ko -->
	        						<!-- ko case: 'date' -->
	        						<div class="col-xs-7">
	       								<div class="input-group datepicker">
											<input type="text" class="form-control" data-bind="datepicker: value">
											<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
	        						<!-- /ko -->
	        						<!-- ko case: 'datetime' -->
	        						<div class="col-xs-7">
	       								<div class="input-group datepicker">
											<input type="text" class="form-control" data-bind="datetimepicker: value">
											<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
									<!-- /ko -->
	        						<!-- ko case: 'time' -->
	        						<div class="col-xs-12">
	       								<div class="input-group datepicker">
											<input type="text" class="form-control" data-bind="timepicker: value">
											<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
										</div>
									</div>
	        						<!-- /ko -->
	        						<!-- ko case: 'days' -->
	        						<div class="col-xs-7">
	        							<select class="form-control input-sm" data-bind="options: valueOptionsDays, optionsText: 'name', optionsValue: 'id', value: value, attr: { title: daysOptionText } "></select>
	        						</div>
	        						<!-- /ko -->
	        						<!-- ko case: 'text' -->
	        						<div class="col-xs-12">
       									<input type="text" class="form-control input-sm" data-bind="value: value">
       								</div>
	        						<!-- /ko -->
        						<!-- /ko -->
        						</div>
	        				</div>
	        				<div class="col-xs-3">
        						<button class="btn btn-sm btn-default" data-bind="click: $parent.addConditionTerm.bind($parent, $data)">追加</button>
        						<button class="btn btn-sm btn-default" data-bind="click: $parent.removeConditionTerm.bind($parent, $data)">削除</button>
	        				</div>
	        			</div>
	        		</div>
	        	</div>
	        </section>
	        <section>
	        	<div class="section-header">
	        		<h5>ソート</h5>
	        	</div>
	        	<div class="section-body" data-bind="foreach: sortTerms">
       				<div class="row">
       					<div class="col-xs-4">
       						<select class="form-control input-sm" data-bind="options: $parent.sortableOptions, optionsText: 'label', optionsValue: 'id', value: id"></select>
       						{**<span data-bind="text: id"></span>**}
       					</div>
       					<div class="col-xs-4">
       						<select class="form-control input-sm" data-bind="options: $parent.sortTypeOptions, optionsText: 'name', optionsValue: 'id', value: type"></select>
       					</div>
       					<div class="col-xs-4">
       						<button class="btn btn-sm btn-default" data-bind="click: $parent.addSortTerm.bind($parent, $data)">追加</button>
       						<button class="btn btn-sm btn-default" data-bind="click: $parent.removeSortTerm.bind($parent, $data)">削除</button>
       					</div>
        			</div>
	        	</div>
	        </section>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary big-btn save" data-bind="click: apply">適用</button>
        <button type="button" class="btn btn-default big-btn cancel" data-dismiss="modal">キャンセル</button>
        <div class="btn-group">
		  <button type="button" class="btn btn-default dropdown-toggle big-btn" data-toggle="dropdown">
		    検索条件一覧の編集 <span class="caret"></span>
		  </button>
		  <ul class="dropdown-menu" role="menu">
			<li><a href="#" data-bind="click: openSavefilter">現在の条件を保存して適用</a></li>
		    <li class="divider"></li>
			<li><a href="#" data-bind="click: openFilterlist">一覧の編集</a></li>
		  </ul>
		</div>
      </div>
    </div>
  </div>
</div>
