<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
{include file="include/head.tpl" title="" features=['nvd3']}
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="home"}
</div>

<div id="content" class="db-list_content" >
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="/my/">データベース入力</a></li>
			<li><a href="/my/data/?t={$table->getId()}">{$table->getName()|escape}</a></li>
		  	<li class="active">集計結果</li>
		</ol>

		<div class="page-header">
			<h1>円グラフ</h1>
		</div>

		<div class="row">
			<div class="col-md-6">

			<div id='chart' class='span4'>
  				<svg style="height:500px;width:100%"></svg>
			</div>

			</div>
			<div class="col-md-6">
				<table class="table table-striped" data-bind="with: list">
					<thead>
						<tr data-bind="foreach: columns">
							<th data-bind="text: label" class="align-right"></th>
						</tr>
					</thead>
					<tbody data-bind=" foreach: rows">
				    	<tr>
					    	<td>
								<span data-bind="text: values[0].text"></span>
							</td>
							<td class="align-right">
								<span data-bind="text: values[1].text"></span>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
		<a class="btn btn-primary" data-bind="click: openBack">戻る</a>
	</div>
</div>
<div id="footer">
	{include file="include/footer.tpl"}
</div>

{include file="include/js_bottom.tpl" features=['nvd3']}
<script type="text/javascript" src="/js/view/base/list_model.js?{get_ver_query()|escape}"></script>
<script type="text/javascript" src="/js/view/data_subtotal.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	var options ={
		t : '{$form->t}',
		af: '{$form->af}',
		fn: '{$form->fn}',
		gf: '{$form->gf}'
	};
	viewModel = new ViewModel(options);
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
