<!DOCTYPE html>
<html lang="ja">
<head>
{include file="include/head.tpl" title="フォームの設定" features=['datepicker', 'select2']}
<link rel="stylesheet" type="text/css" href="/css/page/table_modify_form.css?{get_ver_query()|escape}">
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="design"}
</div>

<div id="content" class="db-design_content" >
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="/my/table/">データベース作成</a></li>
			<li><a href="/my/table/modify?t={$table->getId()}">{$table->getName()|escape}</a></li>
			<li class="active">フォームの設定</li>
		</ol>

		<nav class="page-nav">
			<button class="btn btn-primary big-btn save" data-bind="click: save">保存</button>
			<a href="/my/table/modify?t={$table->getId()}" class="btn btn-default big-btn cancel" data-bind="">キャンセル</a>
			<button class="btn btn-default big-btn" data-bind="click: preview">プレビュー</button>
		</nav><br class="clearfix" />

		<div class="row">
			<div id="tx-side" class="col-xs-4 col-md-3">
				<div id="tx-side-affix" class="tx-items-container panel panel-default">
					<div class="panel-body">
						<ul id="tx-items" >
						<!-- ko foreach: items -->
							<li class="tx-item" data-bind="draggable: { data: $data,
								connectClass: 'tx-row-connect', options: $root.draggableOptions, isEnabled: isMenuEnabled($data) },
								attr: { 'data-uiitemid': uiItemIdName }, css: styleMenu($data) ">
								<a class="btn btn-default fielditem" data-bind="css: $root.itemClassName($data)"><span data-bind="text: label"></span></a>
							</li>
						<!-- /ko -->
						<!-- ko foreach: baseItems -->
							<li class="tx-item" data-bind="draggable: { data: $data,
								connectClass: 'tx-row-connect', options: $root.draggableOptions,
								isEnabled: baseItemDisabled } , css: { disabled: showsField },
								attr: { 'data-uiitemid': uiItemIdName } ">
								<a class="btn btn-default fielditem" data-bind="css: $root.itemClassName($data)"><span data-bind="text: label"></span></a>
							</li>
						<!-- /ko -->
						</ul>
					</div>
				</div>
			</div>
			<div id="tx-main" class="col-xs-8 col-md-9">
				<div class="panel panel-default">
					<div class="panel-body">
						<form class="form" role="form" id="table-form-modify-form" data-bind="">
							<!-- ko foreach: rows -->
							<ul class="row tx-row tx-row-connect" data-bind="sortable: { data: fields, options: $root.sortableOptions,
									connectClass: 'tx-row-connect', beforeMove: $root.beforeMove, afterMove: $root.afterMove,
									dragged: $root.dragged, allowDrop: allowDrop } ,
									css: { 'tx-row-dummy': isEmpty }">
								<li class="tx-field-holder" data-bind="css: styleWidth, field: $data,
									attr: { 'data-uiitemid': uiItemIdName } ">
									{include file="my/table/table_modify_form_types.tpl"}
									<div data-bind="attr: { id: idName() + '-popover' },
										closablePopover: { content: error, placement: 'bottom' }, closablePopoverShows: error "></div>
								</li>
							</ul>
							<!-- /ko -->
						</form>
					</div><!-- /panel-body -->
				</div><!-- /panel -->

			</div><!-- col -->
		</div><!-- row -->
	</div><!-- /container -->
</div><!-- /content -->

<div id="footer">
	{include file="include/footer.tpl"}
</div>

{include file="my/table/table_modify_form_modals.tpl"}

{include file="include/js_bottom.tpl" features=['datepicker', 'select2', 'sortable', 'ckeditor']}

<script type="text/javascript" src="/js/view/table_modify_form.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	viewModel = new ViewModel(
		{$table->getId()|escape},
		{if $table->isChild(true)}true{else}false{/if},
		{if $table->isSixColumnsEnabled()}true{else}false{/if}
	);
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
