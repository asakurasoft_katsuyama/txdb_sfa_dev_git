<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
{include file="include/head.tpl" title=$table->getName()}
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="design"}
</div>

<div id="content" class="db-design_content" >
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="/my/table/">データベース作成</a></li>
			<li class="active">{$table->getName()|escape}</li>
		</ol>

		{if $finishedMessage !== null }
		<div class="alert alert-info alert-dismissable">
			<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
			{$finishedMessage|escape}
		</div>
		{/if}
		<form class="set-up">
			<div class="form-group">
					<p>
						<button type="button" class="btn btn-primary big-btn save" data-toggle="modal" data-target="#modalApply" {if !$table->hasPendingRelease()}disabled="disabled"{/if}>変更内容を適用する</button>
					</p>
					<p>
						<button type="button" class="btn btn-default big-btn cancel" data-toggle="modal" data-target="#modalDiscard" {if !$table->hasPendingRelease()}disabled="disabled"{/if}>変更内容を破棄する</button>
					</p>
			</div>
		</form>
		<div class="row">
			<div class="col-md-2">
				<p>
					<img class="icon" src="{$table->getIcon()->getUrl()|escape}">
				</p>
			</div>
			<div class="col-md-7">
				<form class="form-horizontal" role="form">
					<div class="form-group">
						<label class="col-md-3 control-label">データベースのid</label>
						<div class="col-md-9">
							<p class="form-control-static">{$table->getId()|escape}</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">データベース名</label>
						<div class="col-md-9">
							<p class="form-control-static">{$table->getName()|escape}</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">作成者名</label>
						<div class="col-md-9">
							<p class="form-control-static">
								{if $table->getOwner()}
									{$table->getOwner()->getScreenName()|escape}
								{/if}
							</p>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-3 control-label">公開設定</label>
						<div class="col-md-9">
							<p class="form-control-static">
								{if $table->isPublic()}
									公開
								{else}
									非公開
								{/if}
							</p>
						</div>
					</div>
				</form>
			</div>
			<div class="col-md-3">
				<p class="pull-right"><a href="/my/data/?t={$table->getId()}">
					<button type="button" class="btn btn-primary save">
					このデータベースの入力画面へ
					</button></a>
				</p>
			</div>
		</div>
		<form class="set-up">
			<div class="form-group">
					<p>
						<a class="btn btn-default" href="/my/table/modifyInfo?t={$table->getId()}">基本設定</a>
					</p>
					<p>
						<a class="btn btn-default" href="/my/table/modifyForm?t={$table->getId()}">フォームの設定</a>
					</p>
					<p>
						<a class="btn btn-default" href="/my/table/modifyView?t={$table->getId()}">データベース一覧の表示列</a>
					</p>
				{if get_config('app', 'features.calendar')}
					<p>
						<a class="btn btn-default" href="/my/table/modifyCalendar?t={$table->getId()}">カレンダー表示の設定</a>
					</p>
				{/if}
				{if get_config('app', 'features.notification') && !$table->isChild() }
					<p>
						<a class="btn btn-default" href="/my/table/modifyNotification?t={$table->getId()}">通知の設定</a>
					</p>
				{/if}
					<p>
						<a class="btn btn-default" href="/my/table/modifyConf?t={$table->getId()}">高度な設定</a>
					</p>
					<p>
						<a class="btn btn-default" href="/my/table/modifyPermission?t={$table->getId()}">アクセス権の設定</a>
					</p>
				{if get_config('app', 'features.read_check') && !$table->isChild() }
					<p>
						<a class="btn btn-default" href="/my/table/modifyReadCheck?t={$table->getId()}">回覧板の設定</a>
					</p>
				{/if}
			</div>

			<div class="form-group">
					<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#modalDelete">このデータベースの削除</button>
			</div>
		</form>

	</div>
</div>
<div id="footer">
	{include file="include/footer.tpl"}
</div>

<!-- Modal -->
<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">データベースの削除</h4>
			</div>
			<div class="modal-body">
				このデータベースを削除します。よろしいですか？
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger big-btn" data-bind="click: drop">削除</button>
				<button type="button" class="btn btn-default big-btn cancel" data-dismiss="modal">キャンセル</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalApply" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">変更内容の適用</h4>
			</div>
			<div class="modal-body">
				設定の変更内容をこのデータベースに適用します。よろしいですか？
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary big-btn save" data-bind="click: release">適用する</button>
				<button type="button" class="btn btn-default big-btn cancel" data-dismiss="modal">キャンセル</button>
			</div>
		</div>
	</div>
</div>

<div class="modal fade" id="modalDiscard" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">変更内容の破棄</h4>
			</div>
			<div class="modal-body">
				このデータベースの変更内容を破棄します。よろしいですか？
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary big-btn" data-bind="click: rollback">破棄する</button>
				<button type="button" class="btn btn-default big-btn cancel" data-dismiss="modal">キャンセル</button>
			</div>
		</div>
	</div>
</div>


{include file="include/js_bottom.tpl"}
<script type="text/javascript" src="/js/view/table_modify.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	viewModel = new ViewModel({$table->getId()});
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
