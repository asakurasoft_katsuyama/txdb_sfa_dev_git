<!DOCTYPE html>
<html lang="ja">
<head>
{include file="include/head.tpl" title="データベースの新規作成"}
<link rel="stylesheet" type="text/css" href="/css/page/my_index.css?{get_ver_query()|escape}">
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="design"}
</div>

<nav class="page-nav">

</nav>

<div id="content" class="db-design_content" >
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="/my/table/">データベース作成</a></li>
			<li class="active">新規作成</li>
		</ol>

		<section class="db-list">
			<div class="section-header">
				<h4><i class="fa fa-file-o"></i> 標準ひな型から作成</h4>
			</div>
			<div class="row">
				{foreach $templates as $table}
				{if !$table->isDefault()}{continue}{/if}
				<div class="col-xs-4 col-md-2">
					<div class="thumbnail">
						<a href="/my/table/modify_info?t={$table->getId()|escape}&amp;create=true&amp;base=template">
							<img src="{$table->getIcon()->getUrl()|escape}"></a>
						<div class="caption">
							<h4>{$table->getName()|escape}</h4>
						</div>
					</div>
				</div>
				{/foreach}
			</div>
			<div class="row">
				{foreach $templates as $table}
				{if $table->isDefault()}{continue}{/if}
				<div class="col-xs-4 col-md-2">
					<div class="thumbnail">
						<a href="/my/table/modify_info?t={$table->getId()|escape}&amp;create=true&amp;base=template"><img src="{$table->getIcon()->getUrl()|escape}"></a>
						<div class="caption">
							<h4>{$table->getName()|escape}</h4>
						</div>
					</div>
				</div>
				{/foreach}
			</div>
		</section>
		<section class="db-list">
			<div class="section-header">
				<h4><i class="fa fa-files-o"></i> 既存データベースをコピーして作成</h4>
			</div>
			<div class="row">
				{foreach $userTables as $table}
				<div class="col-xs-4 col-md-2">
					<div class="thumbnail">
						<a href="/my/table/modify_info?t={$table->getId()|escape}&amp;create=true&amp;base=table"><img src="{$table->getIcon()->getUrl()|escape}"></a>
						<div class="caption">
							<h4>{$table->getName()|escape}</h4>
							<p>{if $table->getOwner()}{$table->getOwner()->getScreenName()|escape}{/if}</p>
						</div>
					</div>
				</div>
				{foreachelse}
				<div class="col-xs-12 col-md-12">
					<p>選択できるデータベースはありません</p>
				</div>
				{/foreach}
			</div>
		</section>
	</div>
</div>
<div id="footer">
	{include file="include/footer.tpl"}
</div>

{include file="include/js_bottom.tpl"}
</body>
</html>
