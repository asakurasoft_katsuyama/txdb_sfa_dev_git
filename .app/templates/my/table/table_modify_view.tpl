<!DOCTYPE html>
<html lang="ja">
<head>
{include file="include/head.tpl" title="データベース一覧の表示列の選択" }
<link rel="stylesheet" type="text/css" href="/css/page/table_modify_view.css?{get_ver_query()|escape}">
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="design"}
</div>

<div id="content" class="db-design_content" >
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="/my/table/">データベース作成</a></li>
			<li><a href="/my/table/modify?t={$table->getId()}">{$table->getName()|escape}</a></li>
			<li class="active">データベース一覧の表示列の選択</li>
		</ol>

		<nav class="page-nav">
			<button class="btn btn-primary big-btn save" data-bind="click: save">保存</button>
			<a href="/my/table/modify?t={$table->getId()}" class="btn btn-default big-btn cancel" data-bind="">キャンセル</a>
			<p>データベース一覧に表示する列をドラッグ＆ドロップで選択できます。</p>
		</nav><br class="clearfix" />

		<div class="row">
			<div id="tx-side" class="col-xs-6 col-md-3">
				<div id="tx-side-affix" class="tx-items-container panel panel-default">
					<div class="panel-body">
						<ul id="tx-items">
						<!-- ko foreach: baseItems -->
							<li class="tx-item" data-bind="draggable: { data: $data,
								connectClass: 'tx-row', options: $root.draggableOptions,
								isEnabled: baseItemDisabled } , css: { disabled: selected } ">
								<a class="btn btn-default"><span data-bind="text: label"></span></a>
							</li>
						<!-- /ko -->
						</ul>
					</div>
				</div>
			</div>
			<div id="tx-main" class="col-xs-6 col-md-9">
				<div class="panel panel-default">
					<div class="panel-body">
						<form class="form" role="form" data-bind="">
						<table class="table">
							<tbody class="tx-row" data-bind="sortable: { data: viewColumns, options: $root.sortableOptions,
									afterMove: $root.afterMove } ">
								<tr data-bind="css: { 'tx-sort-disabled': type == null }">
								<!-- ko if: type -->
									<th class="tx-field-holder" data-bind="viewColumn: $data">
										<div class="tx-field-container">
											<span data-bind="text: label"></span>
										</div>
									</th>
								<!-- /ko -->
								<!-- ko ifnot: type -->
									<th class="tx-field-holder tx-field-holder-dummy">
										<div class="tx-field-container">
											<span></span>
										</div>
									</th>
								<!-- /ko -->
								</tr>
							</tbody>
						</table>
						</form>
					</div><!-- /panel-body -->
				</div><!-- /panel -->

			</div><!-- col -->
		</div><!-- row -->
	</div><!-- /container -->
</div><!-- /content -->

<div id="footer">
	{include file="include/footer.tpl"}
</div>

<div class="modal fade" id="setting-modal" tabindex="-1" role="dialog" aria-hidden="true" data-bind="with: settingModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title">設定</h4>
			</div>
			<div class="modal-body">
				<!-- ko if : errorMessage -->
				<div class="alert alert-warning" data-bind="text: errorMessage"></div>
				<!-- /ko  -->
				<div class="form-group">
					<label for="name" class="control-label">データ一覧画面での表示幅（ピクセル）</label>
					<div class="controls">
						<input name="name" type="text" class="form-control" data-bind="value: columnWidth" maxlength="4">
						<span class="help-block">Android, iPad以外の場合のみ有効</span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary big-btn" data-bind="click: update">OK</button>
				<button type="button" class="btn btn-default big-btn cancel" data-dismiss="modal">キャンセル</button>
			</div>
		</div>
	</div>
</div>

{include file="include/js_bottom.tpl" features=['sortable']}

<script type="text/javascript" src="/js/view/table_modify_view.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	viewModel = new ViewModel({$table->getId()|escape});
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
