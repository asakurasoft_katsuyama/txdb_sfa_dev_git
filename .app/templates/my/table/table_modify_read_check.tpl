<!DOCTYPE html>
<html lang="ja">
<head>
{include file="include/head.tpl" title="回覧板の設定" features=['select2']}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="design"}
</div>

<div id="content" class="db-design_content" >
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="/my/table/">データベース作成</a></li>
			<li><a href="/my/table/modify?t={$table->getId()}">{$table->getName()|escape}</a></li>
			<li class="active">回覧板の設定</li>
		</ol>
		<div class="alert alert-warning" data-bind="visible: errors().length > 0, foreach: errors">
			<span data-bind="text: message"></span><br>
		</div>
		<form id="calendar-settings-form" class="form" role="form" data-bind="submit: save">
			<div class="row">
				<div class="col-md-12">

					<div class="form-group">
						<label class="control-label">回覧板の設定</label>
						<div class="checkbox">
							<label for="enable-read-check-field">回覧板を有効にする</label>
							<input type="checkbox" data-bind="checked: isReadCheckEnabled" id="enable-read-check-field">
						</div>
					</div>

					<div class="form-group">
						<label class="control-label">オプション</label>
						<div class="checkbox">
							<label for="onlist-field">データ一覧画面から閲覧済みにできるようにする</label>
							<input type="checkbox" data-bind="checked: isOnlistEnabled, enable: isReadCheckEnabled" id="onlist-field">
						</div>
						<div class="checkbox">
							<label for="autoread-field">データ詳細画面を表示したタイミングで自動的に閲覧済みにする</label>
							<input type="checkbox" data-bind="checked: isAutoreadEnabled, enable: isReadCheckEnabled" id="autoread-field">
						</div>
					</div>


					<div class="form-group">
						<label class="control-label">閲覧チェックを行うユーザー／グループ</label>
						<div class="radio">
							<label><input type="radio" value="all" data-bind="checked: targetType, enable: isReadCheckEnabled">
								すべてのユーザ
							</label>
						</div>
						<div class="radio">
							<label><input type="radio" value="user-specified" data-bind="checked: targetType, enable: isReadCheckEnabled">
								グループまたはユーザを指定
							</label>
						</div>
						<select data-bind="foreach: userGroupOptions, optionsText: 'name', optionsValue: 'id', selectedOptions: checkTargets, select2: true, enable: isReadCheckEnabled && targetType() === 'user-specified'" multiple="true">
							<optgroup data-bind="attr: { label: label}, foreach: children">
								<option data-bind="text: name, value: id" ></option>
							</optgroup>
						</select>
					</div>

					{**

					<div class="form-group required">
						<label class="control-label">カレンダーの日付に対応するフィールド</label>
						<select class="form-control" data-bind="value: dateColumnId, options: dateColumnOptions, optionsText:'label', optionsValue:'id', enable: isCalendarEnabled">
						</select>
					</div>

					<div class="form-group required">
						<label class="control-label">ユーザーに対応するフィールド</label>
						<select class="form-control" data-bind="value: userColumnId, options: userColumnOptions, optionsText:'label', optionsValue:'id', enable: isCalendarEnabled">
						</select>
					</div>

					<div class="form-group">
						<label class="control-label">カレンダーの開始時刻に対応するフィールド</label>
						<select class="form-control" data-bind="value: startTimeColumnId, options: timeColumnOptions, optionsText:'label', optionsValue:'id', enable: isCalendarEnabled">
						</select>
					</div>

					<div class="form-group">
						<label class="control-label">カレンダーの終了時刻に対応するフィールド</label>
						<select class="form-control" data-bind="value: endTimeColumnId, options: timeColumnOptions, optionsText:'label', optionsValue:'id', enable: isCalendarEnabled">
						</select>
					</div>
					<div class="form-group required">
						<label class="control-label">データへのリンクのタイトルにするフィールド</label>
						<select class="form-control" data-bind="value: titleColumnId, options: titleColumnOptions, optionsText:'label', optionsValue:'id', enable: isCalendarEnabled">
						</select>
					</div>

*}
					<div class="form-group">
						<button type="submit" class="btn btn-primary big-btn save">保存</button>
						<a class="btn btn-default big-btn cancel" href="/my/table/modify?t={$table->getId()}">キャンセル</a>
					</div>

				</div><!-- /col -->
			</div><!-- /row -->
		</form>
	</div><!-- /container -->
</div><!-- /content -->

<div id="footer">
	{include file="include/footer.tpl"}
</div>

{include file="include/js_bottom.tpl" features=['select2']}
<script type="text/javascript" src="/js/view/table_modify_read_check.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	viewModel = new ViewModel({$table->getId()|escape});
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
