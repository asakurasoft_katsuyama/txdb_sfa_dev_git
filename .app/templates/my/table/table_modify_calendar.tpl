<!DOCTYPE html>
<html lang="ja">
<head>
{include file="include/head.tpl" title="カレンダー表示の設定"}
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="design"}
</div>

<div id="content" class="db-design_content" >
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="/my/table/">データベース作成</a></li>
			<li><a href="/my/table/modify?t={$table->getId()}">{$table->getName()|escape}</a></li>
			<li class="active">カレンダー表示の設定</li>
		</ol>
		<div class="alert alert-warning" data-bind="visible: errors().length > 0, foreach: errors">
			<span data-bind="text: message"></span><br>
		</div>
		<form id="calendar-settings-form" class="form" role="form" data-bind="submit: save">
			<div class="row">
				<div class="col-md-12">

					<div class="form-group">
						<label class="control-label">カレンダービュー機能</label>
						<div class="checkbox">
							<label for="enable-calendar-field">カレンダービュー機能を使用する</label>
							<input type="checkbox" data-bind="checked: isCalendarEnabled" id="enable-calendar-field">
							<p class="help-block">有効にすると一覧表示とカレンダー表示を切り換えれるようになります。</p>
						</div>
					</div>

					<div class="form-group required">
						<label class="control-label">カレンダーの日付に対応するフィールド</label>
						<select class="form-control" data-bind="value: dateColumnId, options: dateColumnOptions, optionsText:'label', optionsValue:'id', enable: isCalendarEnabled">
						</select>
					</div>

					<div class="form-group required">
						<label class="control-label">ユーザーに対応するフィールド</label>
						<select class="form-control" data-bind="value: userColumnId, options: userColumnOptions, optionsText:'label', optionsValue:'id', enable: isCalendarEnabled">
						</select>
					</div>

					<div class="form-group">
						<label class="control-label">カレンダーの開始時刻に対応するフィールド</label>
						<select class="form-control" data-bind="value: startTimeColumnId, options: timeColumnOptions, optionsText:'label', optionsValue:'id', enable: isCalendarEnabled">
						</select>
					</div>

					<div class="form-group">
						<label class="control-label">カレンダーの終了時刻に対応するフィールド</label>
						<select class="form-control" data-bind="value: endTimeColumnId, options: timeColumnOptions, optionsText:'label', optionsValue:'id', enable: isCalendarEnabled">
						</select>
					</div>
					<div class="form-group required">
						<label class="control-label">データへのリンクのタイトルにするフィールド</label>
						<select class="form-control" data-bind="value: titleColumnId, options: titleColumnOptions, optionsText:'label', optionsValue:'id', enable: isCalendarEnabled">
						</select>
					</div>

					<div class="form-group">
						<button type="submit" class="btn btn-primary big-btn save">保存</button>
						<a class="btn btn-default big-btn cancel" href="/my/table/modify?t={$table->getId()}">キャンセル</a>
					</div>
				</div><!-- /col -->
			</div><!-- /row -->
		</form>
	</div><!-- /container -->
</div><!-- /content -->

<div id="footer">
	{include file="include/footer.tpl"}
</div>

{include file="include/js_bottom.tpl"}
<script type="text/javascript" src="/js/view/table_modify_calendar.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	viewModel = new ViewModel({$table->getId()|escape});
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
