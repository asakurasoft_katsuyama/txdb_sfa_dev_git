<!DOCTYPE html>
<html lang="ja">
<head>
{include file="include/head.tpl" title="データベース設定一覧"}
<link rel="stylesheet" type="text/css" href="/css/page/my_index.css?{get_ver_query()|escape}">
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="design"}
</div>

<div id="content" class="db-design_content" >
	<div class="container">

		<nav class="page-nav">
			<p class="text-left">
				<a class="btn btn-default big-btn" href="/my/table/create"{if $tableCount >= $tableLimit} disabled="disabled"{/if}>新規作成</a> {if $tableCount > 0}データベース数:{$tableCount|escape} / {/if}上限:{$tableLimit|escape}
			</p>
			<p class="text-left">

			</p>
		</nav><br class="clearfix" />

		<hr class="hr">

		<section class="db-list">
			<div class="section-header">
				<h4><i class="fa fa-users"></i> 共有データベース</h4>
			</div>
			<div class="row" id="db-common">
				{foreach $publicTables as $table}
				<div class="col-xs-4 col-md-2 col-sm-2 db-common-parts">
					<div class="thumbnail">
						<div class="images"><a href="/my/table/modify?t={$table->getId()|escape}"><img src="{$table->getIcon()->getUrl()|escape}"></a></div>
						<div class="caption">
							<h4>{$table->getName()|escape}</h4>
							<p>{if $table->getOwner()}{$table->getOwner()->getScreenName()|escape}{/if}</p>
						</div>
					</div>
				</div>
				{foreachelse}
				<div class="col-xs-12 col-md-12">
					<p>選択できるデータベースはありません</p>
				</div>
				{/foreach}
			</div>
		</section>
		<section class="db-list">
			<div class="section-header">
				<h4><i class="fa fa-user"></i> 個人データベース</h4>
			</div>
			<div class="row" id="db-alone">
				{foreach $privateTables as $table}
				<div class="col-xs-4 col-md-2 col-sm-2 db-alone-parts">
					<div class="thumbnail">
						<div class="images"><a href="/my/table/modify?t={$table->getId()|escape}"><img src="{$table->getIcon()->getUrl()|escape}"></a></div>
						<div class="caption">
							<h4>{$table->getName()|escape}</h4>
							<p>{if $table->getOwner()}{$table->getOwner()->getScreenName()|escape}{/if}</p>
						</div>
					</div>
				</div>
				{foreachelse}
				<div class="col-xs-12 col-md-12">
					<p>選択できるデータベースはありません</p>
				</div>
			{/foreach}
			</div>
		</section>
	</div>
</div>
<div id="footer">
	{include file="include/footer.tpl"}
</div>

{include file="include/js_bottom.tpl"}
</body>
</html>
