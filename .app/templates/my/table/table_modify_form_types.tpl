

{** 文字列 =========================================================== **}
<!-- ko if: $data.type == 'text'  -->
	<div class="form-group field-text" data-bind="css:{ required: isRequired }">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<input type="text" class="form-control" data-bind="attr: { id: idName, readonly: isCalculative }, value: defaultValue">
		<span class="help-block" data-bind="visible: hintText, text: hintText"></span>
	</div>
<!-- /ko -->

{** 文字列（複数行） =========================================================== **}
<!-- ko if: $data.type == 'multitext'  -->
	<div class="form-group field-multitext" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<textarea class="form-control" data-bind=" attr: { id: idName, rows: sizeHeight }, value:defaultValue "></textarea>
	</div>
<!-- /ko -->

{** リッチエディター =========================================================== **}
<!-- ko if: $data.type == 'richtext'  -->
	<div class="form-group field-richtext" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<div class="pseudo-richeditor">
			<div class="pseudo-richeditor-header">
				<div class="btn-toolbar" role="toolbar">
					<div class="btn-group">
						<button class="btn btn-default btn-sm" disabled>&nbsp;</button>
						<button class="btn btn-default btn-sm" disabled>&nbsp;</button>
					</div>
					<div class="btn-group">
						<button class="btn btn-default btn-sm" disabled>&nbsp;</button>
						<button class="btn btn-default btn-sm" disabled>&nbsp;</button>
						<button class="btn btn-default btn-sm" disabled>&nbsp;</button>
					</div>
					<div class="btn-group">
						<button class="btn btn-default btn-sm" disabled>&nbsp;</button>
						<button class="btn btn-default btn-sm" disabled>&nbsp;</button>
					</div>
				</div>
			</div>
			<div class="pseudo-richeditor-body"
				data-bind="html: defaultValue, style: { height: sizeHeight() * 20 + 'px' } "></div>
		</div>
	</div>
<!-- /ko -->

{** 数値 =========================================================== **}
<!-- ko if: $data.type == 'number'  -->
	<div class="form-group field-number" data-bind="css:{ required: isRequired}">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<input type="text" class="form-control" data-bind="attr: { id: idName }, value: defaultValue">
		<span class="help-block" data-bind="visible: hintText, text: hintText"></span>
	</div>
<!-- /ko -->

{** 計算 =========================================================== **}
<!-- ko if: $data.type == 'calc'  -->
	<div class="form-group field-calc">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<input type="text" class="form-control" readonly="readonly">
	</div>
<!-- /ko -->

{** ラジオボタン =========================================================== **}
<!-- ko if: $data.type == 'radio' -->
	<div class="form-group field-radio" data-bind="css:{ required: isRequired}">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<div class="controls" data-bind="foreach: options" >
			<label data-bind="css: $parent.optionsAlignment() == 'vertical'? 'radio-block' : 'radio-inline'">
				<input type="radio" data-bind="attr:{ value: id}, checked: $parent.defaultValue">
				<span data-bind=" text: name "></span>
			</label>
		</div>
	</div>
<!-- /ko -->

{** チェックボックス =========================================================== **}
<!-- ko if: $data.type == 'checkbox' -->
	<div class="form-group field-checkbox" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<div class="controls" data-bind="foreach: options" >
			<label data-bind="css: $parent.optionsAlignment() == 'vertical'? 'checkbox-block' : 'checkbox-inline'">
				<input type="checkbox" data-bind="attr:{ value: id}, checked: isDefault">
				<span data-bind=" text: name "></span>
			</label>
		</div>
	</div>
<!-- /ko -->

{** ユーザー選択 =========================================================== **}
<!-- ko if: $data.type == 'user_selector' -->
<div class="form-group field-user_selector" data-bind="css:{ required:isRequired}">
	<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
	<input type="text" class="form-control" data-bind="attr: { id: idName }">
</div>
<!-- /ko -->

{** ドロップダウン =========================================================== **}
<!-- ko if: $data.type == 'dropdown' -->
	<div class="form-group field-dropdown" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<select class="form-control" data-bind="options: optionsList, optionsText: 'name', optionsValue: 'id', value: defaultValue"></select>
	</div>
<!-- /ko -->

{** 日付 =========================================================== **}
<!-- ko if: $data.type == 'date' -->
	<div class="form-group field-date" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<div class="input-group datepicker">
			<input type="text" class="form-control" data-bind="attr: { id: idName }, value: defaultDate">
			<div class="input-group-btn">
				<button class="btn"><span class="glyphicon glyphicon-calendar"></span></button><button class="btn datetime-clear"><i class="glyphicon glyphicon-remove"></i></button>
			</div>
		</div>
	</div>
<!-- /ko -->

{** 時刻 =========================================================== **}
<!-- ko if: $data.type == 'time' -->
	<div class="form-group field-time" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<div class="input-group timepicker">
			<input type="text" class="form-control" data-bind="attr: { id: idName }, value: defaultTime">
			<div class="input-group-btn">
				<button class="btn"><span class="glyphicon glyphicon-time"></span></button><button class="btn datetime-clear"><i class="glyphicon glyphicon-remove"></i></button>
			</div>
		</div>
	</div>
<!-- /ko -->

{** 日時 =========================================================== **}
<!-- ko if: $data.type == 'datetime' -->
	<div class="form-group field-datetime" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<div class="input-group datetimepicker">
			<input type="text" class="form-control" data-bind="attr: { id: idName }, value: defaultDatetime">
			<div class="input-group-btn">
				<button class="btn"><span class="glyphicon glyphicon-calendar"></span></button><button class="btn datetime-clear"><i class="glyphicon glyphicon-remove"></i></button>
			</div>
		</div>
	</div>
<!-- /ko -->

{** 日時計算 =========================================================== **}
<!-- ko if: $data.type == 'datecalc'  -->
	<div class="form-group field-datecalc">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<input type="text" class="form-control" readonly="readonly">
	</div>
<!-- /ko -->

{** 添付ファイル =========================================================== **}
<!-- ko if: $data.type == 'file' -->
	<div class="form-group field-link" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<div>
			<span class="btn btn-default fileinput-button">
				<span>参照</span>
			</span>
			<p class="help-block">({file_size_convert(get_config('app','data_file_size_limit'))}まで)</p>
		</div>
	</div>
<!-- /ko -->

{** リンク =========================================================== **}
<!-- ko if: $data.type == 'link' -->
	<div class="form-group field-link" data-bind="css:{ required:isRequired}">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<input type="text" class="form-control" data-bind="attr: { id: idName }, value: defaultValue">
		<span class="help-block" data-bind="visible: hintText, text: hintText"></span>
	</div>
<!-- /ko -->

{** 関連レコード =========================================================== **}
<!-- ko if: $data.type == 'relational' -->
	<div class="form-group field-relational">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<!-- ko if: referalError -->
		<div class="alert alert-warning">表示が制限されています。</div>
		<!-- /ko -->
		<!-- ko ifnot: referalError -->
		<table class="table table-bordered">
			<thead>
				<tr data-bind="foreach: referenceColumnLabels">
					<th data-bind="text: $data">1</th>
				</tr>
			</thead>
		</table>
		<!-- /ko -->
	</div>
<!-- /ko -->

{** 明細入力 =========================================================== **}
<!-- ko if: $data.type == 'relational_input' -->
	<div class="form-group field-relational_input">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<!-- ko if: referalError -->
		<div class="alert alert-warning">表示が制限されています。</div>
		<!-- /ko -->
		<!-- ko ifnot: referalError -->
		<table class="table table-bordered">
			<thead>
				<tr data-bind="foreach: referenceColumnLabels">
					<th data-bind="text: $data">1</th>
				</tr>
			</thead>
		</table>
		<!-- /ko -->
	</div>
<!-- /ko -->

{** ルックアップ =========================================================== **}
<!-- ko if: $data.type == 'lookup' -->
	<div class="form-group field-lookup" data-bind="css:{ required:isRequired }">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<div class="input-group">
			<input type="text" class="form-control" data-bind="attr: { id: idName, disabled: referalError }">
			<div class="input-group-btn">
				<button type="button" class="btn btn-info" data-bind="attr: { disabled: referalError }">確定</button><button type="button" class="btn btn-info" data-bind="attr: { disabled: referalError }">クリア</button>
			</div>
		</div>
	</div>
<!-- /ko -->

{** レコード番号 =========================================================== **}
<!-- ko if: $data.type == 'rownum' -->
	<div class="form-group field-rownum required">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<p class="form-control-static">（自動）</p>
	</div>
<!-- /ko -->

{** 作成者 =========================================================== **}
<!-- ko if: $data.type == 'created_by' -->
	<div class="form-group field-created_by required">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<p class="form-control-static">（自動）</p>
	</div>
<!-- /ko -->

{** 作成日時 =========================================================== **}
<!-- ko if: $data.type == 'created_on' -->
	<div class="form-group field-created_on required">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<p class="form-control-static">（自動）</p>
	</div>
<!-- /ko -->

{** 更新者 =========================================================== **}
<!-- ko if: $data.type == 'updated_by' -->
	<div class="form-group field-updated_by required">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<p class="form-control-static">（自動）</p>
	</div>
<!-- /ko -->

{** 更新日時 =========================================================== **}
<!-- ko if: $data.type == 'updated_on' -->
	<div class="form-group field-updated_on required">
		<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		<p class="form-control-static">（自動）</p>
	</div>
<!-- /ko -->

{** ラベル =========================================================== **}
<!-- ko if: $data.type == 'label' -->
	<div class="form-group field-label">
		<div class="form-control-static" data-bind="html: labelHtml"></div>
	</div>
<!-- /ko -->

{** スペース =========================================================== **}
<!-- ko if: $data.type == 'space' -->
	<div class="form-group field-space" data-bind="style:{ height: sizeHeight() + 'em' }">
	</div>
<!-- /ko -->


{** 罫線 =========================================================== **}
<!-- ko if: $data.type == 'line' -->
	<div class="form-group field-line">
		<hr>
	</div>
<!-- /ko -->
{** タブグループ =========================================================== **}
{if !isset($nesting) || $nesting < 1}
<!-- ko if: $data.type == 'tab_group' -->
	<div class="field-tab_group">
		<div class="header">
			<label class="control-label" data-bind="visible: showsLabel, attr:{ 'for':idName}, text:label"></label>
		</div>
		<ul class="nav nav-tabs">
		<!-- ko foreach: tabs -->
			<li data-bind="css: { active: $data == $parent.activeTab() }"><a class="field-tab-label" data-bind="attr: { href: '#' + idName() + '_tab-pane'}" data-toggle="tab"><span data-bind="text: label"></span></a></li>
		<!-- /ko -->
		</ul>
		<div class="tab-content">
		<!-- ko foreach: tabs -->
			<div class="tab-pane" data-bind="css: { active: $data == $parent.activeTab() }, attr: { id: idName() + '_tab-pane'} ">
			<!-- ko foreach: rows -->
				<ul class="row tx-row tx-row-connect" data-bind="sortable: { data: fields, options: $root.sortableOptions,
						connectClass: 'tx-row-connect', beforeMove: $root.beforeMove, afterMove: $root.afterMove,
						dragged: $root.dragged, allowDrop: allowDrop } ,
						css: { 'tx-row-dummy': isEmpty }">
					<li class="tx-field-holder" data-bind="css: styleWidth, field: $data,
						attr: { 'data-uiitemid': uiItemIdName } ">
						{include file="my/table/table_modify_form_types.tpl" nesting=1}
						<div data-bind="attr: { id: idName() + '-popover' },
							closablePopover: { content: error, placement: 'bottom' }, closablePopoverShows: error "></div>
					</li>
				</ul>
			<!-- /ko -->
			</div><!-- tab-pane -->
		<!-- /ko -->
		</div><!-- tab-content -->
	</div>
<!-- /ko -->
{/if}
