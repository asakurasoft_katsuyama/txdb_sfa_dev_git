<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
{include file="include/head.tpl" title="" features=['datepicker', 'select2', 'fileupload', 'typeahead']}
<link rel="stylesheet" type="text/css" href="/css/page/data_create.css?{get_ver_query()|escape}">
{include "include/css_head__extension.tpl"}
</head>
<body>

{**
<div id="header">
	{include file="include/header.tpl" active="home"}
</div>
**}

<div id="content" class="db-design_content" >
	<div class="container">
{**
		<ol class="breadcrumb">
			<li><a href="/my/data/?t={$table->getId()}">{$table->getName()|escape}</a></li>
		</ol>

		<nav class="page-nav">
			<button class="btn btn-primary" data-bind="click: save">保存</button>
			<button class="btn btn-default" data-bind="click: cancel">キャンセル</button>
		</nav>
**}
		<div class="panel-list" data-bind="foreach: forms">
			<div class="panel panel-default">
				<div class="panel-body">
					<!-- ko if: error -->
					<div class="alert alert-warning" data-bind="text: error"></div>
					<!-- /ko -->

					<div class="form" role="form" id="data-create-form" data-bind="foreach: matrix">
						<div class="row" data-bind="foreach: $data">
							<div data-bind="css2: $data.span, css2Prefix: 'col-sm-', attr:{ 'data-fieldcode': $data.field.code}">
								<!--  ko with: $data.field -->
									<!-- ko template: { name: function() { return 'create-'+type+'-tpl';}, afterRender: afterRender.bind($data) } -->
									<!-- /ko -->
								<!--  /ko -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
</div>

{**
<div id="footer">
	{include file="include/footer.tpl"}
</div>
**}
{include file="my/data/data_create_types.tpl"}
{include file="my/data/data_index_types.tpl"}
{include file="include/js_bottom.tpl" features=['datepicker', 'select2', 'ckeditor', 'fileupload', 'typeahead']}
<script type="text/javascript" src="/js/view/base/list_model.js?{get_ver_query()|escape}"></script>
<script type="text/javascript" src="/js/view/base/filter_model.js?{get_ver_query()|escape}"></script>
<script type="text/javascript" src="/js/view/data_create.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	var options = {
 		tableId: {$table->getId()|escape},
 		readOnly: true
// 		tableId: {$table->getId()|escape},
// 		{if isset($row)}rowId: {$row->getId()|default:'null'|escape},{/if}
// 		offset: {$offset|default:0|escape}
	};
	viewModel = new ViewModel(options);
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
