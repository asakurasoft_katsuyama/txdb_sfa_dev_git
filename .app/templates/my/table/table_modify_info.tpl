<!DOCTYPE html>
<html lang="ja">
<head>
{include file="include/head.tpl" title="データベースの基本設定" features=['fileupload']}
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="design"}
</div>

<div id="content" class="db-design_content" >
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="/my/table/">データベース作成</a></li>
			{if $create}
			<li><a href="/my/table/create">新規作成</a></li>
			{else}
			<li><a href="/my/table/modify?t={$table->getId()}">{$table->getName()|escape}</a></li>
			{/if}
			<li class="active">データベースの基本設定</li>
		</ol>
		<div class="alert alert-warning" data-bind="visible: errors().length > 0, foreach: errors">
			<span data-bind="text: message"></span><br>
		</div>
		<form class="form" role="form" enctype="multipart/form-data" method="POST" data-bind="submit: save">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group required">
						<label for="name" class="control-label"><span>データベース名</span></label>
						<input type="text" class="form-control" id="name" data-bind="value: name">
					</div>
					<div class="form-group">
						<label class="control-label">アイコン</label>
						<div class="icon-select col-md-4">
							<div class="radio">
								<label>
									<input type="radio" name="iconType" value="preset" data-bind="checked: iconType">
									アイコンを選択する
								</label>
							</div>
							<p>
								<a href="#" data-bind="click: openIconModal"><img class="icon" data-bind="attr: { src: iconUrl }" ></a>
							</p>
							<div>
								<button type="button" class="btn btn-default" data-bind="click: openIconModal">選択</button>
							</div>
						</div>

						<div class="icon-select col-md-4">
						<div class="radio">
							<label>
								<input type="radio" name="iconType" value="upload" data-bind="checked: iconType">
								アイコンをアップロード
							</label>
						</div>
						<table class="table" data-bind="with: iconFile">
							<tbody>
								<tr>
									<td data-bind="text: fileName"></td>
									<td><button class="btn btn-default" data-bind="click: $parent.removeIconFile">キャンセル</button></td>
								</tr>
							</tbody>
						</table>
						<div>
							<span class="btn btn-default fileinput-button">
								<span>参照</span>
								<input class="icon-fileupload" type="file" name="files[]">
							</span>
							<p class="help-block">(1MBまで)</p>
						</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label">共有設定</label>
						<div class="radio">
							<label>
								<input type="radio" name="isPublic" value="1"  data-bind="checked: isPublic">
								共有データベース
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" name="isPublic" value=""  data-bind="checked: isPublic">
								個人データベース
							</label>
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary big-btn save">{if $create}データベースを作成する{else}保存{/if}</button>
						{if $create}
						<a class="btn btn-default big-btn cancel" href="/my/table/create">キャンセル</a>
						{else}
						<a class="btn btn-default big-btn cancel" href="/my/table/modify?t={$table->getId()}">キャンセル</a>
						{/if}
					</div>
				</div><!-- /col -->
			</div><!-- /row -->
		</form>
	</div><!-- /container -->
</div><!-- /content -->

<div id="footer">
	{include file="include/footer.tpl"}
</div>

<div data-bind="with: iconModal" class="modal fade" id="iconModal" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" >アイコン選択</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<form class="form">
						<div class="form-group">
							<label class="control-label">選択中のアイコン</label>
							<div class="row">
								<div class="col-xs-2">
									<div class="thumbnail">
										<a href="#"  data-dismiss="modal"><img data-bind="attr: { src: currentIconUrl }"></a>
									</div>
								</div>
							</div>
						</div>
						<div class="form-group">
							<label class="control-label">利用できるアイコン</label>
							<div class="row" data-bind=" foreach: icons">
								<div class="col-xs-2">
									<div class="thumbnail">
										<a href="#" data-bind="click: $parent.action"><img data-bind="attr: { src: url }"></a>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">キャンセル</button>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>

{include file="include/js_bottom.tpl" features=['fileupload']}
<script type="text/javascript" src="/js/view/table_modify_info.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	viewModel = new ViewModel({$table->getId()|escape}, {if $create}true, '{$base|escape}'{else}false{/if});
	ko.applyBindings(viewModel);
});
</script>

</body>
</html>
