<!DOCTYPE html>
<html lang="ja">
<head>
{include file="include/head.tpl" title="高度な設定"}
<style>
.field-digit input {
	text-align: right;
	max-width: 200px !important;
}
.field-apikey input {
	cursor: text !important;
	background-color: transparent !important;
}
</style>
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="design"}
</div>

<div id="content" class="db-design_content" >
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="/my/table/">データベース作成</a></li>
			<li><a href="/my/table/modify?t={$table->getId()}">{$table->getName()|escape}</a></li>
			<li class="active">高度な設定</li>
		</ol>
		<div class="alert alert-warning" data-bind="visible: errors().length > 0, foreach: errors">
			<span data-bind="text: $data"></span><br>
		</div>
		<form id="config-form" class="form" role="form" data-bind="submit: save">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group field-digit required">
						<label class="control-label">小数部桁数</label>
						<input name="roundScale" type="text" class="form-control" data-bind="value: roundScale">
						<p class="help-block">0以上、10以下</p>

					</div>
					<div class="form-group">
						<label class="control-label">まるめ処理</label>
						<select name="roundType" class="form-control" data-bind="value: roundType">
							<option value="default">最近接偶数へのまるめ</option>
							<option value="roundhalfup">四捨五入</option>
							<option value="roundup">切り上げ</option>
							<option value="rounddown">切り捨て</option>
						</select>
					</div>
					{if get_config('app', 'features.commenting') && !$table->isChild() }
					<div class="form-group">
						<label class="control-label">コメント機能</label>
						<div class="checkbox">
							<label for="enable-comment-field">コメント機能を使用する</label>
							<input type="checkbox" data-bind="checked: isCommentEnabled" id="enable-comment-field">
						</div>
					</div>
					{/if}
					{**
					<div class="form-group">
						<label class="control-label">カレンダービュー機能</label>
						<div class="checkbox">
							<label for="enable-calendar-field">カレンダービュー機能を使用する</label>
							<input type="checkbox" data-bind="checked: isCalendarEnabled" id="enable-calendar-field">
						</div>
					</div>
					**}
					{if get_config('app', 'features.sixcolumns')}
					<div class="form-group">
						<label class="control-label">フォーム列数</label>
						<div class="checkbox">
							<label for="enable-sixcols-field">6列を使用する</label>
							<input type="checkbox" data-bind="checked: isSixColumnsEnabled" id="enable-sixcols-field">
						</div>
					</div>
					{/if}
					<div class="form-group">
						<label class="control-label">API状態</label>
						<div class="radio">
							<label>
								<input type="radio" name="apiState" value="public" data-bind="checked: apiState">
								公開
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" name="apiState" value="private" data-bind="checked: apiState">
								非公開
							</label>
						</div>
						<p class="help-block">公開に設定するとAPIの利用が可能になります。</p>
					</div>
					<div class="form-group">
						<label class="control-label">API利用形態</label>
						<div class="radio">
							<label>
								<input type="radio" name="apiMode" value="readonly" data-bind="checked: apiMode">
								読み取りのみ
							</label>
							<p class="help-block">
								読み取り用のAPIの利用が可能になります。
							</p>
						</div>
						<div class="radio">
							<label>
								<input type="radio" name="apiMode" value="writable" data-bind="checked: apiMode, disable: isChild">
								読み取り＋書き込み
							</label>
							<p class="help-block">
								読み取り用のAPIに加えて書き込み用のAPIの利用が可能になります。
							</p>
						</div>
					</div>
					<div class="form-group field-apikey required">
						<label class="control-label">APIキー</label>
						<div class="input-group">
							<input type="text" class="form-control" name="apiKey" data-bind="value: apiKey" readonly="readonly">
							<div class="input-group-btn">
								<button class="btn btn-default" data-bind="click: generateApiKey">再生成</button>
							</div>
						</div>
					</div>
					<div class="form-group field-apikey required">
						<label class="control-label">書き込み用APIキー</label>
						<div class="input-group">
							<input type="text" class="form-control" name="writeApiKey" data-bind="value: writeApiKey" readonly="readonly">
							<div class="input-group-btn">
								<button class="btn btn-default" data-bind="click: generateWriteApiKey">再生成</button>
							</div>
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary big-btn save">保存</button>
						<a class="btn btn-default big-btn cancel" href="/my/table/modify?t={$table->getId()}">キャンセル</a>
					</div>
				</div><!-- /col -->
			</div><!-- /row -->
		</form>
	</div><!-- /container -->
</div><!-- /content -->

<div id="footer">
	{include file="include/footer.tpl"}
</div>

{include file="include/js_bottom.tpl"}
<script type="text/javascript" src="/js/view/table_modify_conf.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	viewModel = new ViewModel({$table->getId()|escape});
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
