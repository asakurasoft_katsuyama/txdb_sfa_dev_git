<!DOCTYPE html>
<html lang="ja">
<head>
{include file="include/head.tpl" title="通知設定" features=['select2','datepicker']}
<link rel="stylesheet" type="text/css" href="/css/page/table_modify_notification.css?{get_ver_query()|escape}">
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="design"}
</div>

<div id="content" class="db-design_content" >
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="/my/table/">データベース作成</a></li>
			<li><a href="/my/table/modify?t={$table->getId()}">{$table->getName()|escape}</a></li>
			<li class="active">通知設定</li>
		</ol>
		<section class="notification-settings">
		<!-- ko if: errors().length > 0 -->
			<div class="alert alert-warning" data-bind="foreach: errors">
				<span data-bind="text: $data.message"></span><br>
			</div>
		<!-- /ko -->
			<form id="notification-settings-form" class="form" role="form" data-bind="submit: save">
				<div class="form-group basic-rules">
					<label class="control-label">基本通知</label>
					<!-- ko if: basicRules().length > 0 -->
					<div class="panel panel-default">
						<table class="table">
							<tbody data-bind="foreach: basicRules">
								<tr>
									<td>
										<!-- ko if: errors().length > 0 -->
										<div class="alert alert-warning" data-bind="foreach: errors">
											<span data-bind="text: $data.message"></span><br>
										</div>
										<!-- /ko -->
										<div class="row">
											<div class="col-xs-4">
												<select class="form-control input-sm" data-bind="options: $parent.targetingTypeOptions, optionsText: 'label', optionsValue: 'id', value: targetingType"></select>
											</div>
											<!-- ko if: targetingType() == "user_group"  -->
											<div class="col-xs-3">
												<select class="" data-bind="foreach: $parent.userGroupOptions, optionsText: 'name', optionsValue: 'id', selectedOptions: targetIds, select2: true" multiple="true">
													<optgroup data-bind="attr: { label: label}, foreach: children">
														<option data-bind="text: name, value: id" ></option>
													</optgroup>
												</select>
											</div>
											<!-- /ko -->
											<!-- ko if: targetingType() == 'column'  -->
											<div class="col-xs-3">
												<select class="form-control input-sm" data-bind="options: $parent.targettableColumnOptions, optionsText: 'label', optionsValue: 'id', value: targetColumnId"></select>
											</div>
											<!-- /ko -->
											<div class="col-xs-5">
												<div class="events">
													<div class="checkbox-inline">
														<label>
															<input type="checkbox" data-bind="checked: isOnRowInsert">
															データ登録
														</label>
													</div>
													<div class="checkbox-inline">
														<label>
															<input type="checkbox" data-bind="checked: isOnRowUpdate">
															データ更新
														</label>
													</div>
													<div class="checkbox-inline">
														<label>
															<input type="checkbox" data-bind="checked: isOnCommentInsert">
															コメント追加
														</label>
													</div>
												</div>
											</div>
										</div><!-- row -->
									</td>
									<td class="col-action">
										<button class="btn btn-default" data-bind="click: $parent.addBasicRule.bind($parent, $data)">追加</button>
									</td>
									<td class="col-action">
										<button class="btn btn-default" data-bind="click: $parent.removeBasicRule.bind($parent, $data)">削除</button>
									</td>
								</tr>
							</tbody><!-- rules -->
						</table>
					</div><!-- panel -->
					<!-- /ko -->
					<!-- ko ifnot: basicRules().length > 0 -->
					<div>
						<button class="btn btn-default" data-bind="click: addBasicRule"><span>追加</span></button>
					</div>
					<!-- /ko -->
				</div>
				<div class="form-group">
					<label class="contro-label">コメントの継続通知</label>
					<div class="checkbox">
						<label>
							<input type="checkbox" data-bind="checked: isCommentFollowingEnabled">
							あるデータへコメントが追加されたとき、そのデータへコメントしたことのあるユーザーへ通知を送る。
						</label>
					</div>
				</div>
				<div class="form-group conditional-rules">
					<label class="control-label">条件指定通知</label>
					<!-- ko if: conditionalRules().length > 0 -->
					<div class="panel panel-default">
						<table class="table">
							<tbody data-bind="foreach: conditionalRules">
								<tr>
									<td>
										<!-- ko if: errors().length > 0 -->
										<div class="alert alert-warning" data-bind="foreach: errors">
											<span data-bind="text: $data.message"></span><br>
										</div>
										<!-- /ko -->
										<div class="form-group">
											<div class="row">
												<div class="col-xs-4">
													<select class="form-control input-sm" data-bind="options: $parent.targetingTypeOptions, optionsText: 'label', optionsValue: 'id', value: targetingType"></select>
												</div>
												<!-- ko if: targetingType() == "user_group"  -->
												<div class="col-xs-3">
													<select class="" data-bind="foreach: $parent.userGroupOptions, optionsText: 'name', optionsValue: 'id', selectedOptions: targetIds, select2: true" multiple="true">
														<optgroup data-bind="attr: { label: label}, foreach: children">
															<option data-bind="text: name, value: id" ></option>
														</optgroup>
													</select>
												</div>
												<!-- /ko -->
												<!-- ko if: targetingType() == 'column'  -->
												<div class="col-xs-3">
													<select class="form-control input-sm" data-bind="options: $parent.targettableColumnOptions, optionsText: 'label', optionsValue: 'id', value: targetColumnId"></select>
												</div>
												<!-- /ko -->
											</div><!-- row -->
										</div><!-- form-group -->
										<div class="form-group">
											<div class="row">
												<div class="col-xs-1 label-wrapper">
													<h6>通知内容</h6>
												</div>
												<div class="col-xs-11">
													<input class="form-control input-sm" type="text" data-bind="value: message">
												</div>
											</div>
										</div>
										<div>
											<div class="row">
												<div class="col-xs-1 label-wrapper">
													<h6>通知条件</h6>
												</div>
												<div class="col-xs-11">
{** see data_index_filter_dialog.tpl **}
		        		<div class="form-group">
		        			<div class="controls">
		        				<select class="form-control input-sm" data-bind="options: boolTypeOptions, optionsText: 'name', optionsValue: 'id', value: boolOperator"></select>
		        			</div>
		        		</div>
		        		<div class="form-group" data-bind="foreach: conditionTerms">
		        			<div class="row">
		        				<div class="col-xs-2">
		        					<select class="form-control input-sm" data-bind="options: $parent.searchableOptions, optionsText: 'label', optionsValue: 'id', value: id"></select>
		        				</div>
		        				<div class="col-xs-2" data-bind="if: operatorOptions">
	        						<select class="form-control input-sm" data-bind="options: operatorOptions, optionsText: 'name', optionsValue: 'id', value: operator"></select>
	        					</div>
	        					<div class="col-xs-5">
	        						<div class="row">
	        						<!-- ko if: datePresetOptions -->
	        							<div class="col-xs-5">
		        							<select class="form-control input-sm" data-bind="options: datePresetOptions, optionsText: 'name', optionsValue: 'id', value: datePreset"></select>
		        						</div>
	        						<!-- /ko -->
	        						<!-- ko switch: valueType -->
		        						<!-- ko case: 'options' -->
	        							<div class="col-xs-12">
		        							<select class="" data-bind="options: valueOptionsOptions, optionsText: 'name', optionsValue: 'id', selectedOptions: values, select2: true" multiple="true"></select>
	        							</div>
		        						<!-- /ko -->
										<!-- ko case: 'userGroupOptions' -->
										<div class="col-xs-12">
											<select class="" data-bind="foreach: userGroupOptions, optionsText: 'name', optionsValue: 'id', selectedOptions: values, select2: true" multiple="true">
												<optgroup data-bind="attr: { label: label}, foreach: children">
													<option data-bind="text: name, value: id" ></option>
												</optgroup>
											</select>
										</div>
										<!-- /ko -->
		        						<!-- ko case: 'date' -->
		        						<div class="col-xs-7">
		       								<div class="input-group datepicker">
												<input type="text" class="form-control" data-bind="datepicker: value">
												<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
											</div>
										</div>
		        						<!-- /ko -->
		        						<!-- ko case: 'datetime' -->
		        						<div class="col-xs-7">
		       								<div class="input-group datepicker">
												<input type="text" class="form-control" data-bind="datetimepicker: value">
												<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
											</div>
										</div>
										<!-- /ko -->
		        						<!-- ko case: 'time' -->
		        						<div class="col-xs-12">
		       								<div class="input-group datepicker">
												<input type="text" class="form-control" data-bind="timepicker: value">
												<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
											</div>
										</div>
		        						<!-- /ko -->
		        						<!-- ko case: 'days' -->
		        						<div class="col-xs-7">
		        							<select class="form-control input-sm" data-bind="options: valueOptionsDays, optionsText: 'name', optionsValue: 'id', value: value"></select>
		        						</div>
		        						<!-- /ko -->
		        						<!-- ko case: 'text' -->
		        						<div class="col-xs-12">
	       									<input type="text" class="form-control input-sm" data-bind="value: value">
	       								</div>
		        						<!-- /ko -->
	        						<!-- /ko -->
	        						</div>
		        				</div>
		        				<div class="col-xs-3">
	        						<button class="btn btn-sm btn-default" data-bind="click: $parent.addConditionTerm.bind($parent, $data)">追加</button>
	        						<button class="btn btn-sm btn-default" data-bind="click: $parent.removeConditionTerm.bind($parent, $data)">削除</button>
		        				</div>
		        			</div>
		        		</div>
{** /see  **}
												</div>
											</div><!-- row -->
										</div>
									</td>
									<td class="col-action">
										<button class="btn btn-default" data-bind="click: $parent.addConditionalRule.bind($parent, $data)">追加</button>
									</td>
									<td class="col-action">
										<button class="btn btn-default" data-bind="click: $parent.removeConditionalRule.bind($parent, $data)">削除</button>
									</td>
								</tr>
							</tbody><!-- rules -->
						</table>
					</div><!-- panel -->
					<!-- /ko -->
					<!-- ko ifnot: conditionalRules().length > 0 -->
					<div>
						<button class="btn btn-default" data-bind="click: addConditionalRule"><span>追加</span></button>
					</div>
					<!-- /ko -->
				</div>
				<div class="form-group">
					<label class="control-label">通知に表示する付加情報</label>
					<!-- ko if: viewColumns().length > 0 -->
					<div class="row">
						<div class="col-md-6">
							<div class="panel panel-default">
								<table class="table">
									<tbody data-bind="foreach: viewColumns">
										<tr>
											<td>
												<select class="form-control input-sm" data-bind="options: $parent.viewColumnOptions, optionsText: 'label', optionsValue: 'id', value: id"></select>
											</td>
											<td class="col-action">
												<button class="btn btn-default" data-bind="click: $parent.addViewColumn.bind($parent, $data)">追加</button>
											</td>
											<td class="col-action">
												<button class="btn btn-default" data-bind="click: $parent.removeViewColumn.bind($parent, $data)">削除</button>
											</td>
										</tr>
									</tbody><!-- viewColumns -->
								</table>
							</div><!-- panel -->
						</div><!-- col -->
					</div><!-- row -->
					<!-- /ko -->
					<!-- ko ifnot: viewColumns().length > 0 -->
					<div>
						<button class="btn btn-default" data-bind="click: addViewColumn"><span>追加</span></button>
					</div>
					<!-- /ko -->
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary big-btn save">保存</button>
					<a class="btn btn-default big-btn cancel" href="/my/table/modify?t={$table->getId()}">キャンセル</a>
				</div>
			</form>
		</section>
	</div><!-- container -->
</div><!-- content -->

<div id="footer">
	{include file="include/footer.tpl"}
</div>

{include file="include/js_bottom.tpl" features=['select2','datepicker']}
<script type="text/javascript" src="/js/view/table_modify_notification.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	viewModel = new ViewModel({$table->getId()|escape});
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
