<!DOCTYPE html>
<html lang="ja">
<head>
{include file="include/head.tpl" title="アクセス権の設定"}
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="design"}
</div>

<div id="content" class="db-design_content" >
	<div class="container">
		<ol class="breadcrumb">
			<li><a href="/my/table/">データベース作成</a></li>
			<li><a href="/my/table/modify?t={$table->getId()}">{$table->getName()|escape}</a></li>
			<li class="active">アクセス権の設定</li>
		</ol>
		<div class="alert alert-warning" data-bind="visible: errors().length > 0, foreach: errors">
			<span data-bind="text: $data"></span><br>
		</div>
		<form class="form" role="form" data-bind="submit: save">
			<div class="row">
				<div class="col-md-12">
					<div class="form-group">
						<label class="control-label">アクセス制限の状態</label>
						<div class="radio">
							<label>
								<input type="radio" name="permissionControlState" value="unlimited" data-bind="checked: permissionControlState">
								制限しない
							</label>
						</div>
						<p class="help-block">すべてのユーザがデータの閲覧、編集を行うことができます。</p>
						<div class="radio">
							<label>
								<input type="radio" name="permissionControlState" value="limited" data-bind="checked: permissionControlState">
								制限する
							</label>
						</div>
						<p class="help-block">ユーザごとにデータの閲覧、編集権限を設定することができます。</p>
					</div>
				</div><!-- /col -->
			</div><!-- /row -->

			<div class="form-group">
				<label class="control-label">ユーザごとのアクセス権設定</label>
				<nav class="page-nav">
					<div class="form-inline">
							<input type="text" class="form-control" placeholder="キーワード" data-bind="value: searchQuery">
							<button type="submit" class="btn btn-default" data-bind="click: search" >検索</button>
							<button type="button" class="btn btn-default" data-bind="click: clearSearch" >クリア</button>
					</div>
				</nav>

				<nav class="list-nav">
					<span data-bind="text: totalRowCount"></span> 件中 <span data-bind="text: offsetStart"></span>〜<span data-bind="text: offsetEnd"></span>件
				</nav>
				<div class="panel panel-default">
					<table class="table table-striped">
						<thead>
							<tr>
								<th class="col-md-4">表示名</th>
								<th class="col-md-4">所属等</th>
								<th class="col-md-2">閲覧 <br>（<input type="checkbox" data-bind="checked: isAllReadable">表示行をチェック）</td></th>
								<th class="col-md-2">編集 <br>（<input type="checkbox" data-bind="checked: isAllWritable">表示行をチェック）</td></th>
							</tr>
						</thead>
						<tbody data-bind=" foreach: rows">
							<!-- ko if: $data.role != 'admin' -->
							<!-- ko if: $data.display -->
							<tr>
								<td><span data-bind="text: $data.screenName "></span></td>
								<td><span data-bind="text: $data.profile "></span></td>
								<td><input type="checkbox" data-bind="checked: $data.isReadable, disable: disableIsReadable"></td>
								<td><input type="checkbox" data-bind="checked: $data.isWritable"></td>
							</tr>
							<!-- /ko -->
							<!-- /ko -->
						</tbody>
					</table>
				</div>
				<ul class="pager">
					<li data-bind=" css: { disabled: !$root.hasPrev() } "><a href="#" data-bind=" click: prevList">前へ</a></li>
					<li data-bind=" css: { disabled: !$root.hasNext() } "><a href="#" data-bind=" click: nextList">次へ</a></li>
				</ul>
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary big-btn save">保存</button>
				<a class="btn btn-default big-btn cancel" href="/my/table/modify?t={$table->getId()}">キャンセル</a>
			</div>
		</form>
	</div><!-- /container -->
</div><!-- /content -->

<div id="footer">
	{include file="include/footer.tpl"}
</div>

{include file="include/js_bottom.tpl"}
<script type="text/javascript" src="/js/view/table_modify_permission.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	var option = {
		tableId: {$table->getId()},
		searchQuery: '{$searchQuery|default:''}',
		offset: {$limit[0]|default:0},
		limit: {$limit[1]|default:20}
	}
	viewModel = new ViewModel(option);
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
