<{"script"} id="label-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title">注意書きの設定</h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group">
						<label class="control-label">注意書き</label>
						<textarea class="form-control richeditor" data-bind=" richeditor: labelHtml.temp "></textarea>
						<div class="alert alert-warning" data-bind="text: labelHtml.error, visible: labelHtml.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

<{"script"} id="space-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title">空白の設定</h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

<{"script"} id="text-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title">文字列（一行）フィールド</h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp ">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isCalculative.temp ">
								<span>自動計算する</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: isCalculative.error, visible: isCalculative.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isRequired.temp, disable: isCalculative.temp">
								<span>必須項目にする</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: isRequired.error, visible: isRequired.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isUnique.temp, disable: isUniqueRestricted">
								<span>値の重複を禁止する</span>
							</label>
							<div class="alert alert-warning" data-bind="text: isUnique.error, visible: isUnique.error"></div>
							<p class="help-block">値の重複を禁止する場合は、最大文字数が64文字までに制限されます。</p>
						</div>
					</div>
					<div class="form-group field-number">
						<label class="control-label">最小文字数(整数)</label>
						<input type="text" class="form-control" data-bind="value: minLength.temp, disable: isCalculative.temp">
						<div class="alert alert-warning" data-bind="text: minLength.error, visible: minLength.error"></div>
					</div>
					<div class="form-group field-number">
						<label class="control-label">最大文字数(整数)</label>
						<input type="text" class="form-control" data-bind="value: maxLength.temp, disable: (isCalculative.temp() || isUnique.temp())">
						<div class="alert alert-warning" data-bind="text: maxLength.error, visible: maxLength.error"></div>
					</div>
					<div class="form-group">
						<label class="control-label">初期値</label>
						<input type="text" class="form-control" data-bind="value: defaultValue.temp, disable: isCalculative.temp">
						<div class="alert alert-warning" data-bind="text: defaultValue.error, visible: defaultValue.error"></div>
					</div>
					<div class="form-group">
						<label class="control-label">自動計算の式</label>
						<input type="text" class="form-control" data-bind="value: expression.temp, enable: isCalculative.temp">
						<div class="alert alert-warning" data-bind="text: expression.error, visible: expression.error"></div>
						<p class="help-block">
							<dl>
								<dt>X,Yがフィールドキーの場合の例</dt>
								<dd>X &amp; Y: 文字列を連結します。</dd>
							</dl>
						</p>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

<{"script"} id="richtext-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title">装飾テキストフィールド</h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp ">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isRequired.temp">
								<span>必須項目にする</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: isRequired.error, visible: isRequired.error"></div>
					</div>
					<div class="form-group">
						<label class="control-label">初期値</label>
						<textarea class="form-control richeditor" data-bind=" richeditor: defaultValue.temp "></textarea>
						<div class="alert alert-warning" data-bind="text: defaultValue.error, visible: defaultValue.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

<{"script"} id="multitext-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title">文字列（複数行）フィールド</h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp ">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isRequired.temp">
								<span>必須項目にする</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: isRequired.error, visible: isRequired.error"></div>
					</div>
					<div class="form-group">
						<label class="control-label">初期値</label>
						<textarea class="form-control" data-bind=" value: defaultValue.temp"></textarea>
						<div class="alert alert-warning" data-bind="text: defaultValue.error, visible: defaultValue.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

<{"script"} id="number-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title">数値フィールド</h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp ">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: showsDigit.temp ">
								<span>桁区切りを表示する</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsDigit.error, visible: showsDigit.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isRequired.temp">
								<span>必須項目にする</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: isRequired.error, visible: isRequired.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isUnique.temp, disable: isUniqueRestricted">
								<span>値の重複を禁止する</span>
							</label>
							<div class="alert alert-warning" data-bind="text: isUnique.error, visible: isUnique.error"></div>
						</div>
					</div>
					<div class="form-group field-number">
						<label class="control-label">最小値(整数)</label>
						<input type="text" class="form-control" data-bind="value: minValue.temp">
						<div class="alert alert-warning" data-bind="text: minValue.error, visible: minValue.error"></div>
					</div>
					<div class="form-group field-number">
						<label class="control-label">最大値(整数)</label>
						<input type="text" class="form-control" data-bind="value: maxValue.temp">
						<div class="alert alert-warning" data-bind="text: maxValue.error, visible: maxValue.error"></div>
					</div>
					<div class="form-group">
						<label class="control-label">初期値</label>
						<input type="text" class="form-control" data-bind="value: defaultValue.temp">
						<div class="alert alert-warning" data-bind="text: defaultValue.error, visible: defaultValue.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

<{"script"} id="calc-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title">計算フィールド</h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp ">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">自動計算の式</label>
						<input type="text" class="form-control" data-bind="value: expression.temp">
						<div class="alert alert-warning" data-bind="text: expression.error, visible: expression.error"></div>
						<p class="help-block">
							<dl>
								<dt>X,Yがフィールドキーの場合の例</dt>
								<dd>X + Y : 足し算</dd>
								<dd>X - Y : 引き算</dd>
								<dd>X * Y : かけ算</dd>
								<dd>X / Y : わり算</dd>
								<dd>X ^ 3 : Xの値を3乗する</dd>
								<dd>(X + Y) * 2: XとYを足し算した結果に2をかける</dd>
							</dl>
						</p>
					</div>
					<div class="form-group">
						<label class="control-label">表示形式</label>
						<div class="radio">
							<label>
								<input type="radio" value="number"  data-bind="checked: format.temp">
								数値（例: 1000）
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" value="number_with_comma"  data-bind="checked: format.temp">
								数値（例: 1,000）
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" value="datetime" data-bind="checked: format.temp">
								日時（例: 2014-12-31 10:30）
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" value="date"  data-bind="checked: format.temp">
								日付（例: 2014-12-31）
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" value="time"  data-bind="checked: format.temp">
								時刻（例: 10:30）
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" value="hours"  data-bind="checked: format.temp">
								時間（例: 10.5時間）
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" value="days"  data-bind="checked: format.temp">
								期間（例: 10.5日）
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: format.error, visible: format.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

<{"script"} id="radio-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title"><span>選択肢フィールド</span></h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">選択肢と順番</label>
						<!-- ko if: options.temp().length > 0 -->
						<table class="table">
							<tbody data-bind="sortable: { data: options.temp, options: sortableOptions }" >
								<tr>
									<td><span class="tx-list-sort-handle"><span class="glyphicon glyphicon-sort"></span></span></td>
									<td><input type="text" class="form-control" data-bind=" value: name.temp "></td>
									<td><button class="btn btn-default" data-bind="click: $parent.addOption">追加</button></td>
									<td><button class="btn btn-default" data-bind="click: $parent.removeOption">削除</button></td>
								</tr>
							</tbody>
						</table>
						<!-- /ko -->
						<!-- ko ifnot: options.temp().length > 0 -->
						<button class="btn btn-default" data-bind="click: addOption"><span>追加</span></button>
						<!-- /ko -->
						<div class="alert alert-warning" data-bind="text: options.error, visible: options.error"></div>
					</div>
					<div class="form-group">
						<label class="control-label">選択肢の並べ方</label>
						<div class="radio">
							<label>
								<input type="radio" value="horizontal"  data-bind="checked: optionsAlignment.temp">
								横
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" value="vertical"  data-bind="checked: optionsAlignment.temp">
								縦
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: optionsAlignment.error, visible: optionsAlignment.error"></div>
					</div>
					<div class="form-group">
						<label class="control-label">初期値</label>
						<select class="form-control" data-bind="foreach: optionsTempList, value: defaultValue.temp">
							<option data-bind="value: $data.id, text: $data.name.temp"></option>
						</select>
						<div class="alert alert-warning" data-bind="text: defaultValue.error, visible: defaultValue.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>


<{"script"} id="checkbox-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title"><span>選択肢(複数)フィールド</span></h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isRequired.temp">
								<span>必須項目にする</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: isRequired.error, visible: isRequired.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">選択肢と順番</label>
						<!-- ko if: options.temp().length > 0 -->
						<table class="table">
							<thead>
								<tr>
									<th></th>
									<th>初期値</th>
									<th></th>
									<th></th>
									<th></th>
								</tr>
							</thead>
							<tbody data-bind="sortable: { data: options.temp, options: sortableOptions }" >
								<tr>
									<td><span class="tx-list-sort-handle"><span class="glyphicon glyphicon-sort"></span></span></td>
									<td><span class="checkbox"><input type="checkbox" data-bind="checked: isDefault.temp"></span></td>
									<td><input type="text" class="form-control" data-bind="value: name.temp"></td>
									<td><button class="btn btn-default" data-bind="click: $parent.addOption">追加</button></td>
									<td><button class="btn btn-default" data-bind="click: $parent.removeOption">削除</button></td>
								</tr>
							</tbody>
						</table>
						<!-- /ko -->
						<!-- ko ifnot: options.temp().length > 0 -->
						<button class="btn btn-default" data-bind="click: addOption"><span>追加</span></button>
						<!-- /ko -->
						<div class="alert alert-warning" data-bind="text: options.error, visible: options.error"></div>
					</div>
					<div class="form-group">
						<label class="control-label">選択肢の並べ方</label>
						<div class="radio">
							<label>
								<input type="radio" value="horizontal"  data-bind="checked: optionsAlignment.temp">
								横
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" value="vertical"  data-bind="checked: optionsAlignment.temp">
								縦
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: optionsAlignment.error, visible: optionsAlignment.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>


<{"script"} id="dropdown-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title"><span>プルダウン選択肢フィールド</span></h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isRequired.temp">
								<span>必須項目にする</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: isRequired.error, visible: isRequired.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">選択肢と順番</label>
						<!-- ko if: options.temp().length > 0 -->
						<table class="table">
							<tbody data-bind="sortable: { data: options.temp, options: sortableOptions }" >
								<tr>
									<td><span class="tx-list-sort-handle"><span class="glyphicon glyphicon-sort"></span></span></td>
									<td><input type="text" class="form-control" data-bind=" value: name.temp "></td>
									<td><button class="btn btn-default" data-bind="click: $parent.addOption">追加</button></td>
									<td><button class="btn btn-default" data-bind="click: $parent.removeOption">削除</button></td>
								</tr>
							</tbody>
						</table>
						<!-- /ko -->
						<!-- ko ifnot: options.temp().length > 0 -->
						<button class="btn btn-default" data-bind="click: addOption"><span>追加</span></button>
						<!-- /ko -->
						<div class="alert alert-warning" data-bind="text: options.error, visible: options.error"></div>
					</div>
					<div class="form-group">
						<label class="control-label">初期値</label>
						<select class="form-control" data-bind="foreach: optionsTempList, value: defaultValue.temp">
							<option data-bind="value: $data.id, text: $data.name.temp"></option>
						</select>
						<div class="alert alert-warning" data-bind="text: defaultValue.error, visible: defaultValue.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

<{"script"} id="user_selector-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title"><span>ユーザー選択</span></h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isRequired.temp">
								<span>必須項目にする</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: isRequired.error, visible: isRequired.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>


<{"script"} id="date-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title">日付フィールド</h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp ">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isRequired.temp">
								<span>必須項目にする</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: isRequired.error, visible: isRequired.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isUnique.temp, disable: isUniqueRestricted">
								<span>値の重複を禁止する</span>
							</label>
							<div class="alert alert-warning" data-bind="text: isUnique.error, visible: isUnique.error"></div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label" >初期値</label>
						<div class="input-group datepicker">
							<input type="text" class="form-control" data-bind="datepicker: defaultValue.temp, disable: isDefaultNow.temp">
							<div class="input-group-btn">
								<button class="btn"><span class="glyphicon glyphicon-calendar"></span></button>
								<button class="btn datetime-clear">クリア</button>
							</div>
						</div>
						<div class="alert alert-warning" data-bind="text: defaultValue.error, visible: defaultValue.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isDefaultNow.temp">
								<span>データ登録時の日付を初期値にする</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: isDefaultNow.error, visible: isDefaultNow.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

<{"script"} id="time-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title">時刻フィールド</h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp ">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isRequired.temp">
								<span>必須項目にする</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: isRequired.error, visible: isRequired.error"></div>
					</div>
					<div class="form-group">
						<label class="control-label" >初期値</label>
						<div class="input-group timepicker">
							<input type="text" class="form-control" data-bind="timepicker: defaultValue.temp, disable: isDefaultNow.temp">
							<div class="input-group-btn">
								<button class="btn"><span class="glyphicon glyphicon-time"></span></button>
								<button class="btn datetime-clear">クリア</button>
							</div>
						</div>
						<div class="alert alert-warning" data-bind="text: defaultValue.error, visible: defaultValue.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isDefaultNow.temp">
								<span>データ登録時の時刻を初期値にする</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: isDefaultNow.error, visible: isDefaultNow.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

<{"script"} id="datetime-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title">日時フィールド</h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp ">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isRequired.temp">
								<span>必須項目にする</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: isRequired.error, visible: isRequired.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isUnique.temp, disable: isUniqueRestricted">
								<span>値の重複を禁止する</span>
							</label>
							<div class="alert alert-warning" data-bind="text: isUnique.error, visible: isUnique.error"></div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label" >初期値</label>
						<div class="input-group datetimepicker">
							<input type="text" class="form-control" data-bind="datetimepicker: defaultValue.temp, disable: isDefaultNow.temp">
							<div class="input-group-btn">
								<button class="btn"><span class="glyphicon glyphicon-calendar"></span></button>
								<button class="btn datetime-clear">クリア</button>
							</div>
						</div>
						<div class="alert alert-warning" data-bind="text: defaultValue.error, visible: defaultValue.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isDefaultNow.temp">
								<span>データ登録時の日時を初期値にする</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: isDefaultNow.error, visible: isDefaultNow.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

<{"script"} id="datecalc-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title">日時計算フィールド</h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp ">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">参照するフィールド</label>
						<select class="form-control"
							data-bind="options: dateSourceColumnOptions, value: dateSourceColumnId.temp, optionsText: 'label', optionsValue: 'id' ">
						</select>
						<div class="alert alert-warning" data-bind="text: dateSourceColumnId.error, visible: dateSourceColumnId.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">設定</label>
						<div class="row">
							<div class="col-md-4">
								<input type="text" class="form-control" data-bind="value: expression.temp">
								<div class="alert alert-warning" data-bind="text: expression.error, visible: expression.error"></div>
								<p class="help-block">数値, フィールドキー</p>
							</div>
							<div class="col-md-4">
								<select class="form-control" data-bind="options: datecalcUnitOptions, value: datecalcUnit.temp, optionsText: 'label', optionsValue: 'id' ">
								</select>
								<div class="alert alert-warning" data-bind="text: datecalcUnit.error, visible: datecalcUnit.error"></div>
								<p class="help-block">期間</p>
							</div>
							<div class="col-md-4">
								<select class="form-control" data-bind="options: datecalcBeforeAfterOptions, value: datecalcBeforeAfter.temp, optionsText: 'label', optionsValue: 'id' ">
								</select>
								<div class="alert alert-warning" data-bind="text: datecalcBeforeAfter.error, visible: datecalcBeforeAfter.error"></div>
								<p class="help-block">前後</p>
							</div>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label">表示形式</label>
						<div class="radio">
							<label>
								<input type="radio" value="datetime" data-bind="checked: dateFormat.temp">
								日時（例: 2014-12-31 10:30）
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" value="date"  data-bind="checked: dateFormat.temp">
								日付（例: 2014-12-31）
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" value="time"  data-bind="checked: dateFormat.temp">
								時刻（例: 10:30）
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: dateFormat.error, visible: dateFormat.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

<{"script"} id="file-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title"><span>添付ファイルフィールド</span></h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isRequired.temp">
								<span>必須項目にする</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: isRequired.error, visible: isRequired.error"></div>
					</div>
					<div class="form-group">
						<label class="control-label">サムネイルの長辺の長さ（ピクセル）</label>
						<select class="form-control" data-bind="value: thumbnailMaxSize.temp">
							<option value="50">50</option>
							<option value="150">150</option>
							<option value="250">250</option>
							<option value="500">500</option>
						</select>
						<div class="alert alert-warning" data-bind="text: thumbnailMaxSize.error, visible: thumbnailMaxSize.error"></div>
					</div>
					<div class="form-group">
						<label class="control-label">画像の長辺の最大長さ（ピクセル）</label>
						<select class="form-control" data-bind="value: imageMaxSize.temp">
							<option value="">規定値 - 1000</option>
							<option value="400">400</option>
							<option value="600">600</option>
							<option value="800">800</option>
							<option value="-1">無制限</option>
						</select>
						<p class="help-block">画像の長辺がこの値を超える場合、添付時に縮小されます。</p>
						<div class="alert alert-warning" data-bind="text: imageMaxSize.error, visible: imageMaxSize.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

<{"script"} id="link-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title">リンクフィールド</h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp ">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isRequired.temp">
								<span>必須項目にする</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: isRequired.error, visible: isRequired.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isUnique.temp, disable: isUniqueRestricted">
								<span>値の重複を禁止する</span>
							</label>
							<div class="alert alert-warning" data-bind="text: isUnique.error, visible: isUnique.error"></div>
							<p class="help-block">値の重複を禁止する場合は、最大文字数が64文字までに制限されます。</p>
						</div>
					</div>
					<div class="form-group">
						<label class="control-label">リンクの種類</label>
						<!-- ko ifnot: linkTypeFixed -->
						<div class="radio">
							<label>
								<input type="radio" value="url"  data-bind="checked: linkType.temp">
								Webサイトのアドレス
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" value="tel"  data-bind="checked: linkType.temp">
								電話番号
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" value="email" data-bind="checked: linkType.temp">
								メールアドレス
							</label>
						</div>
						<div class="radio">
							<label>
								<input type="radio" value="local_url" data-bind="checked: linkType.temp">
								ローカルアドレス
							</label>
						</div>
						<!-- /ko -->
						<!-- ko if: linkTypeFixed -->
							<p class="form-control-static">
								<!-- ko if: linkType() == 'url' -->Webサイトのアドレス<!-- /ko -->
								<!-- ko if: linkType() == 'tel' -->電話番号<!-- /ko -->
								<!-- ko if: linkType() == 'email' -->メールアドレス<!-- /ko -->
								<!-- ko if: linkType() == 'local_url' -->ローカルアドレス<!-- /ko -->
							</p>
						<!-- /ko -->
						<div class="alert alert-warning" data-bind="text: linkType.error, visible: linkType.error"></div>
						<p class="help-block">フォームの保存後は、リンクの種類は変更できません。</p>
					</div>
					<div class="form-group field-number">
						<label class="control-label">最小文字数(整数)</label>
						<input type="text" class="form-control" data-bind="value: minLength.temp">
						<div class="alert alert-warning" data-bind="text: minLength.error, visible: minLength.error"></div>
					</div>
					<div class="form-group field-number">
						<label class="control-label">最大文字数(整数)</label>
						<input type="text" class="form-control" data-bind="value: maxLength.temp, disable: isUnique.temp">
						<div class="alert alert-warning" data-bind="text: maxLength.error, visible: maxLength.error"></div>
					</div>
					<div class="form-group">
						<label class="control-label">初期値</label>
						<input type="text" class="form-control" data-bind="value: defaultValue.temp">
						<div class="alert alert-warning" data-bind="text: defaultValue.error, visible: defaultValue.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

<{"script"} id="lookup-modal-tpl" type="text/html">
<div class="modal fade" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title">参照コピーフィールド</h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp ">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="checked: isRequired.temp">
								<span>必須項目にする</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: isRequired.error, visible: isRequired.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">関連付けるデータベース</label>
						<!-- ko ifnot: referenceFixed -->
						<div>
							<input type="hidden" style="width:100%" data-bind="
								value: referenceTableId.temp,
								select2: { options: select2OptionsForTable, lookupKey: 'id' } ">
						</div>
						<!-- /ko -->
						<!-- ko if: referenceFixed -->
						<p class="form-control-static" data-bind="text: referenceTableName"></p>
						<!-- /ko -->
						<div class="alert alert-warning" data-bind="text: referenceTableId.error, visible: referenceTableId.error"></div>
						<p class="help-block">フォームの保存後は、関連付けるデータベースは変更できません。</p>
					</div>
					<div class="form-group required">
						<label class="control-label">関連付けるコピー元フィールド</label>
						<!-- ko ifnot: referenceFixed -->
						<select class="form-control" data-bind="options: referenceTargetColumnOptions,
								value: referenceTargetColumnId.temp, optionsText: 'label', optionsValue: 'id' ">
						</select>
						<!-- /ko -->
						<!-- ko if: referenceFixed -->
						<p class="form-control-static" data-bind="text: referenceTargetColumnName"></p>
						<!-- /ko -->
						<div class="alert alert-warning" data-bind="text: referenceTargetColumnId.error, visible: referenceTargetColumnId.error"></div>
						<p class="help-block">フォームの保存後は、関連付けるフィールドは変更できません。</p>
					</div>
					<div class="form-group">
						<label class="control-label">ほかのフィールドのコピー</label>
						<!-- ko if: referenceCopyMaps.temp().length > 0 -->
						<table class="table">
							<tbody data-bind="sortable: { data: referenceCopyMaps.temp, options: sortableOptions, connectClass:null }" >
								<tr>
									<td><span class="tx-list-sort-handle"><span class="glyphicon glyphicon-sort"></span></span></td>
									<td>
										<select class="form-control" data-bind="options: acceptantOptions,
												value: acceptantId.temp, optionsText: 'label', optionsValue: 'id' "></select>
									</td>
									<td>&larr;</td>
									<td>
										<select class="form-control" data-bind="options: accepteeOptions,
												value: accepteeId.temp, optionsText: 'label', optionsValue: 'id' "></select>
									</td>
									<td><button class="btn btn-default" data-bind="click: $parent.addCopyMap">追加</button></td>
									<td><button class="btn btn-default" data-bind="click: $parent.removeCopyMap">削除</button></td>
								</tr>
							</tbody>
						</table>
						<!-- /ko -->
						<!-- ko ifnot: referenceCopyMaps.temp().length > 0 -->
						<button class="btn btn-default" data-bind="click: addCopyMap"><span>追加</span></button>
						<!-- /ko -->
						<div class="alert alert-warning" data-bind="text: referenceCopyMaps.error, visible: referenceCopyMaps.error"></div>
					</div>
					<div class="form-group">
						<label class="control-label">コピー元のデータ選択時に表示するフィールド</label>
						<!-- ko if: referenceColumnIds.temp().length > 0 -->
						<table class="table">
							<tbody data-bind="sortable: { data: referenceColumnIds.temp, options: sortableOptions, connectClass:null }">
								<tr>
									<td><span class="tx-list-sort-handle"><span class="glyphicon glyphicon-sort"></span></span></td>
									<td>
										<select class="form-control" data-bind="options: $parent.referenceListableOptions,
												value: id, optionsText: 'label', optionsValue: 'id' "></select>
									</td>
									<td><button class="btn btn-default" data-bind="click: $parent.addReferenceColumn">追加</button></td>
									<td><button class="btn btn-default" data-bind="click: $parent.removeReferenceColumn">削除</button></td>
								</tr>
							</tbody>
						</table>
						<!-- /ko -->
						<!-- ko ifnot: referenceColumnIds.temp().length > 0 -->
						<button class="btn btn-default" data-bind="click: addReferenceColumn"><span>追加</span></button>
						<!-- /ko -->
						<div class="alert alert-warning" data-bind="text: referenceColumnIds.error, visible: referenceColumnIds.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

<{"script"} id="relational-modal-tpl" type="text/html">
<div class="modal fade" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title">関係データ一覧</h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp ">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">参照先のデータベース</label>
						<div>
							<input type="hidden" style="width:100%" data-bind="
								value: referenceTableId.temp,
								select2: { options: select2OptionsForTable, lookupKey: 'id' } ">
						</div>
						<div class="alert alert-warning" data-bind="text: referenceTableId.error, visible: referenceTableId.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">検索条件</label>
						<table class="table">
							<tbody>
								<tr>
									<td>
										<select class="form-control" data-bind="options: filteredReferenceSourceColumnOptions,
												value: referenceSourceColumnId.temp, optionsText: 'label', optionsValue: 'id' "></select>
										<div class="alert alert-warning" data-bind="text: referenceSourceColumnId.error, visible: referenceSourceColumnId.error"></div>
									</td>
									<td>=</td>
									<td>
										<select class="form-control" data-bind="options: filteredReferenceTargetColumnOptions,
												value: referenceTargetColumnId.temp, optionsText: 'label', optionsValue: 'id' "></select>
										<div class="alert alert-warning" data-bind="text: referenceTargetColumnId.error, visible: referenceTargetColumnId.error"></div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="form-group required">
						<label class="control-label">表示するフィールド</label>
						<!-- ko if: referenceColumnIds.temp().length > 0 -->
						<table class="table">
							<tbody data-bind="sortable: { data: referenceColumnIds.temp, options: sortableOptions }" >
								<tr>
									<td><span class="tx-list-sort-handle"><span class="glyphicon glyphicon-sort"></span></span></td>
									<td>
										<select class="form-control" data-bind="options: $parent.referenceListableOptions,
												value: id, optionsText: 'label', optionsValue: 'id' "></select>
									</td>
									<td><button class="btn btn-default" data-bind="click: $parent.addReferenceColumn">追加</button></td>
									<td><button class="btn btn-default" data-bind="click: $parent.removeReferenceColumn">削除</button></td>
								</tr>
							</tbody>
						</table>
						<!-- /ko -->
						<!-- ko ifnot: referenceColumnIds.temp().length > 0 -->
						<button class="btn btn-default" data-bind="click: addReferenceColumn"><span>追加</span></button>
						<!-- /ko -->
						<div class="alert alert-warning" data-bind="text: referenceColumnIds.error, visible: referenceColumnIds.error"></div>
					</div>
					<div class="form-group">
						<label class="control-label">集計するフィールド</label>
						<!-- ko if: referenceAggregations.temp().length > 0 -->
						<table class="table">
							<tbody data-bind="foreach: referenceAggregations.temp" >
								<tr>
									<td>
										<select class="form-control" data-bind="options: $parent.referenceAggregatableOptions,
												value: id, optionsText: 'label', optionsValue: 'id' "></select>
									</td>
									<td style="white-space:nowrap;">
										<label class="checkbox-inline">
											<input type="checkbox" value="sum" data-bind="checked: fn">合計
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" value="avg" data-bind="checked: fn">平均
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" value="max" data-bind="checked: fn">最大
										</label>
										<label class="checkbox-inline">
											<input type="checkbox" value="min" data-bind="checked: fn">最小
										</label>
									</td>
									<td><button class="btn btn-default" data-bind="click: $parent.addReferenceAggregationColumn">追加</button></td>
									<td><button class="btn btn-default" data-bind="click: $parent.removeReferenceAggregationColumn">削除</button></td>
								</tr>
							</tbody>
						</table>
						<!-- /ko -->
						<!-- ko ifnot: referenceAggregations.temp().length > 0 -->
						<button class="btn btn-default" data-bind="click: addReferenceAggregationColumn"><span>追加</span></button>
						<!-- /ko -->
						<div class="alert alert-warning" data-bind="text: referenceAggregations.error, visible: referenceAggregations.error"></div>
					</div>
					<div class="form-group">
						<label class="control-label">データのソート</label>
						<table class="table">
							<tbody>
								<tr>
									<td>
										<select class="form-control" data-bind="options: referenceSortableOptions,
												optionsText: 'label', optionsValue: 'id', value: referenceSortColumnId.temp"></select>
										<div class="alert alert-warning" data-bind="text: referenceSortColumnId.error, visible: referenceSortColumnId.error"></div>
									</td>
									<td>
										<select class="form-control" data-bind="value: referenceSortsReverse.temp">
											<option value="1">降順</option>
											<option value="">昇順</option>
										</select>
										<div class="alert alert-warning" data-bind="text: referenceSortsReverse.error, visible: referenceSortsReverse.error"></div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="form-group">
						<label class="control-label">一度に表示する最大データ数</label>
						<select class="form-control" data-bind="value: referenceMaxRow.temp">
							<option value="5">5</option>
							<option value="10">10</option>
							<option value="20">20</option>
							<option value="30">30</option>
						</select>
						<div class="alert alert-warning" data-bind="text: referenceMaxRow.error, visible: referenceMaxRow.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

<{"script"} id="relational_input-modal-tpl" type="text/html">
<div class="modal fade" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title">明細入力</h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp ">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">関連付けるデータベース</label>
						<!-- ko ifnot: referenceFixed -->
						<div>
							<input type="hidden" style="width:100%" data-bind="
								value: referenceTableId.temp,
								select2: { options: select2OptionsForTable, lookupKey: 'id' } ">
						</div>
						<!-- /ko -->
						<!-- ko if: referenceFixed -->
						<p class="form-control-static" data-bind="text: referenceTableName"></p>
						<!-- /ko -->
						<div class="alert alert-warning" data-bind="text: referenceTableId.error, visible: referenceTableId.error"></div>
						<p class="help-block">フォームの保存後は、関連付けるデータベースは変更できません。</p>
					</div>
					<div class="form-group required">
						<label class="control-label">表示するフィールド</label>
						<!-- ko if: referenceColumnIds.temp().length > 0 -->
						<table class="table">
							<tbody data-bind="sortable: { data: referenceColumnIds.temp, options: sortableOptions }" >
								<tr>
									<td><span class="tx-list-sort-handle"><span class="glyphicon glyphicon-sort"></span></span></td>
									<td>
										<select class="form-control" data-bind="options: $parent.referenceListableOptions,
												value: id, optionsText: 'label', optionsValue: 'id' "></select>
									</td>
									<td><button class="btn btn-default" data-bind="click: $parent.addReferenceColumn">追加</button></td>
									<td><button class="btn btn-default" data-bind="click: $parent.removeReferenceColumn">削除</button></td>
								</tr>
							</tbody>
						</table>
						<!-- /ko -->
						<!-- ko ifnot: referenceColumnIds.temp().length > 0 -->
						<button class="btn btn-default" data-bind="click: addReferenceColumn"><span>追加</span></button>
						<!-- /ko -->
						<div class="alert alert-warning" data-bind="text: referenceColumnIds.error, visible: referenceColumnIds.error"></div>
					</div>
					<div class="form-group">
						<label class="control-label">データのソート</label>
						<table class="table">
							<tbody>
								<tr>
									<td>
										<select class="form-control" data-bind="options: referenceSortableOptions,
												optionsText: 'label', optionsValue: 'id', value: referenceSortColumnId.temp"></select>
										<div class="alert alert-warning" data-bind="text: referenceSortColumnId.error, visible: referenceSortColumnId.error"></div>
									</td>
									<td>
										<select class="form-control" data-bind="value: referenceSortsReverse.temp">
											<option value="1">降順</option>
											<option value="">昇順</option>
										</select>
										<div class="alert alert-warning" data-bind="text: referenceSortsReverse.error, visible: referenceSortsReverse.error"></div>
									</td>
								</tr>
							</tbody>
						</table>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>


<{"script"} id="rownum-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title">データ番号フィールド</h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

<{"script"} id="created_by-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title">作成者フィールド</h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

<{"script"} id="created_on-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title">作成日時フィールド</h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

<{"script"} id="updated_by-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title">更新者フィールド</h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

<{"script"} id="updated_on-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title">更新日時フィールド</h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

<{"script"} id="tab_group-modal-tpl" type="text/html">
<div class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" aria-hidden="true" data-bind="click: cancel">&times;</button>
				<h4 class="modal-title"><span>タブグループ</span></h4>
			</div>
			<div class="modal-body">
				<form action="#" data-bind="submit: save">
					<div class="form-group required">
						<label class="control-label">名前</label>
						<input type="text" class="form-control" data-bind="value: label.temp">
						<div class="alert alert-warning" data-bind="text: label.error, visible: label.error"></div>
					</div>
					<div class="form-group">
						<div class="checkbox">
							<label>
								<input type="checkbox" data-bind="inverseChecked: showsLabel.temp">
								<span>フィールド名を表示しない</span>
							</label>
						</div>
						<div class="alert alert-warning" data-bind="text: showsLabel.error, visible: showsLabel.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">タブ</label>
						<!-- ko if: tabs.temp().length > 0 -->
						<table class="table">
							<tbody data-bind="foreach: tabs.temp" >
								<tr>
									<td><input type="text" class="form-control" data-bind=" value: label.temp "></td>
									<td><button class="btn btn-default" data-bind="click: $parent.addTab">追加</button></td>
									<td><button class="btn btn-default" data-bind="click: $parent.removeTab">削除</button></td>
								</tr>
							</tbody>
						</table>
						<!-- /ko -->
						<!-- ko ifnot: tabs.temp().length > 0 -->
						<button class="btn btn-default" data-bind="click: addTab"><span>追加</span></button>
						<!-- /ko -->
						<div class="alert alert-warning" data-bind="text: tabs.error, visible: tabs.error"></div>
					</div>
					<div class="form-group required">
						<label class="control-label">キー</label>
						<input type="text" class="form-control" data-bind="value: code.temp">
						<div class="alert alert-warning" data-bind="text: code.error, visible: code.error"></div>
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<a href="#" class="btn btn-primary" data-bind="click: save">OK</a>
				<a href="#" class="btn" data-bind="click: cancel">キャンセル</a>
			</div>
		</div><!-- /modal-content -->
	</div><!-- /modal-dialog -->
</div>
</{"script"}>

