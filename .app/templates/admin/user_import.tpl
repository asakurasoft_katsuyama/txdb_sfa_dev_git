<!DOCTYPE html>
<html lang="ja">
<head>
<meta charset="utf-8" />
{include file="include/head.tpl" title="ユーザー情報のCSV登録" features=['fileupload']}
<style>
.import-columns .required:after {
  content: "*";
  color: red;
}
</style>
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="setting"}
</div>

<div id="content">
	<div class="container bs">
		<div class="row">
			<div class="col-sm-3">
				<ul class="nav nav-pills nav-stacked">
					<li class="active"><a href="/admin/user/">ユーザー管理</a></li>
					<li><a href="/admin/group/">グループ管理</a></li>
				</ul>
			</div>
			<div class="col-sm-9">
				<div class="panel panel-default">
					<div class="panel-heading">ユーザー情報のCSV登録</div>
					<div class="panel-body">
						<!-- ko if: error -->
						<div class="alert alert-warning" data-bind="text: error"></div>
						<!-- /ko -->
						<!-- ko if: message -->
						<div class="alert alert-info" data-bind="text: message"></div>
						<!-- /ko -->
						<form id="user-import-form1" class="form" role="form">
							<fieldset>
								<legend>1. 最新のインポート用のフォーマットをダウンロードし、インポートするデータを入力してください。</legend>
								<ul class="import-columns">
									<li><span class="required">ログインid</span></li>
									<li><span class="required">パスワード</span></li>
									<li><span class="required">表示名</span></li>
									<li><span>所属等</span></li>
								</ul>
								<div class="form-group">
									<label class="control-label">文字コード</label>
									<select class="form-control" data-bind="value: downloadCharset">
										<option value="Shift_JIS">Shift_JIS</option>
										<option value="UTF-8">UTF-8</option>
									</select>
								</div>
							</fieldset>
							<button type="button" class="btn btn-default btn-primary" data-bind="click: doDownload">フォーマットのダウンロード</button>
						</form>
						<form id="user-import-form2" class="form" role="form">
							<fieldset>
								<legend>2．インポートするファイルを選択し、インポートを実行してください。</legend>
								<div class="form-group">
									<div>
										<span class="btn btn-default fileinput-button">
											<span>参照</span>
											<input type="file" class="form-control fileupload" data-url="/admin/user/upload_import_file.json" name="files[]">
										</span>
										<p class="help-block">(100MBまで)</p>
									</div>
									<!-- ko if:file -->
									<table class="table" data-bind="with: file">
										<tbody>
											<tr>
												<td data-bind="text: fileName">&nbsp;</td>
												<td><button class="btn btn-default" data-bind="click: $parent.removeFile">キャンセル</button></td>
											</tr>
										</tbody>
									</table>
									<!-- /ko -->
								</div>
								<div class="form-group">
									<label class="control-label">文字コード</label>
									<select class="form-control" data-bind="value: importCharset">
										<option value="Shift_JIS">Shift_JIS</option>
										<option value="UTF-8">UTF-8</option>
									</select>
									<p class="help-block">すでに存在するログインidが指定された場合は、そのユーザー情報が更新されます。</p>
								</div>
							</fieldset>
							<button type="button" class="btn btn-default btn-primary" data-bind="click: doImport">実行</button>
							<a class="btn btn-link" href="/admin/user/">キャンセル</a>
						</form>
					</div><!-- panel-body -->
				</div><!-- panel -->
			</div><!-- col -->
		</div><!-- row -->
	</div>
</div>
<div id="footer">
	{include file="include/footer.tpl"}
</div>

{include file="include/js_bottom.tpl" features=['fileupload']}
<script type="text/javascript" src="/js/view/user_import.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	var options ={
	};
	viewModel = new ViewModel(options);
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
