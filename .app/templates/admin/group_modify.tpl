<!DOCTYPE html>
<html lang="ja">
<head>
{if $groupId == null}
{include file="include/head.tpl" title="グループの登録"}
{else}
{include file="include/head.tpl" title="グループ名の設定"}
{/if}
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="setting"}
</div>

<div id="content">
	<div class="container bs">
		<div class="row">
			<div class="col-sm-3">
				<ul class="nav nav-pills nav-stacked">
					<li><a href="/admin/user/">ユーザー管理</a></li>
					<li class="active"><a href="/admin/group/">グループ管理</a></li>
				</ul>
			</div>
			<div class="col-sm-9">
				<div class="panel panel-default">
					{if $groupId == null}
					<div class="panel-heading">グループの登録</div>
					{else}
					<div class="panel-heading">グループ名の設定</div>
					{/if}
					<div class="panel-body user-block">
						<div class="alert alert-warning" data-bind="visible: errors().length > 0, foreach: errors">
							<span data-bind="text: message"></span><br>
						</div>
						<form class="form-horizontal" role="form" data-bind="submit: save">
							<div class="form-group required">
								<label for="name" class="col-sm-3 control-label">グループ名</label>
								<div class="col-sm-6">
									<input name="name" type="text" class="form-control" data-bind="value: name">
								</div>
							</div>


							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-6">
									{if $groupId == null}
									<button type="submit" class="btn btn-primary">登録</button>
									{else}
									<button type="submit" class="btn btn-primary">更新</button>
									{/if}
									<a class="btn btn-default" href="#" data-bind="click: cancel">キャンセル</a>
								</div>
							</div>
						</form>
					</div><!-- panel-body -->
				</div><!-- /panel -->
			</div>
		</div><!-- /row -->
	</div><!-- /container -->
</div><!-- /content -->

<div id="footer">
	{include file="include/footer.tpl"}
</div>

{include file="include/js_bottom.tpl"}
<script type="text/javascript" src="/js/view/group_modify.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	viewModel = new ViewModel({$groupId|default:'null'});
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
