<!DOCTYPE html>
<html lang="ja">
<head>
{include file="include/head.tpl" title="グループ管理" features=['select2']}
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="setting"}
</div>

<div id="content">
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<ul class="nav nav-pills nav-stacked">
					<li><a href="/admin/user/">ユーザー管理</a></li>
					<li class="active"><a href="#">グループ管理</a></li>
				</ul>
			</div>
			<div class="col-sm-9">

				<nav class="page-nav">
					<form class="form-inline" role="search">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="グループ名" data-bind="value: searchQuery">
						</div>
						<div class="form-group">
							<select class="" data-bind="options: userOptions, optionsText: 'name', optionsValue: 'id', value: criterialUserId, select2: true"
									style="min-width: 200px;"></select>
						</div>
						<button type="submit" class="btn btn-default" data-bind="click: $root.search" >検索</button>
					</form>
				</nav>

				<nav class="page-nav">
					<a class="btn btn-default" href="/admin/group/create">新規登録</a>
					<a class="btn btn-default" href="/admin/group/import">CSV登録</a>
				</nav>


				<nav class="list-nav">
					<span data-bind="text: totalRowCount"></span> 件
				</nav>

				<div class="panel panel-default">
					<table class="table table-striped">
						<thead>
							<tr>
								<th>グループ名</th>
								<th></th>
							</tr>
						</thead>
						<tbody data-bind=" foreach: $root.rows">
							<tr>
								<td><span data-bind="text: $data.name"></span></td>
								<td>
									<div class="btn-group">
										<button type="button" class="btn btn-default btn-xs dropdown-toggle"
												data-toggle="dropdown"> 設定 <span class="caret"></span></button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a href="#" data-bind="click: $root.openEditUsers.bind($root, $index())">所属ユーザーの設定</a></li>
											<li><a href="#" data-bind="click: $root.openEdit.bind($root, $index())">グループ名の設定</a></li>
											<li><a href="#" data-bind="click: $root.openDelete.bind($root, $index())">グループ削除</a></li>
										</ul>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div><!-- /row -->
	</div><!-- /container -->
</div><!-- /content -->
<div id="footer">
	{include file="include/footer.tpl"}
</div>

{include file="include/js_bottom.tpl" features=['select2']}
<script type="text/javascript" src="/js/view/base/list_model.js?{get_ver_query()|escape}"></script>
<script type="text/javascript" src="/js/view/group_index.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	var option = {
		searchQuery: '{$searchCriteria.query|default:''}',
		criterialUserId: '{$searchCriteria.criterialUserId|default:''}'
	};
	viewModel = new ViewModel(option);
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
