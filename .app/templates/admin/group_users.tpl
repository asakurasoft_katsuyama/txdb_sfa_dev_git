<!DOCTYPE html>
<html lang="ja">
<head>
{include file="include/head.tpl" title="所属ユーザーの設定"}
<style type="text/css">
select {
	height: 400px !important;
}
</style>
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="setting"}
</div>

<div id="content">
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<ul class="nav nav-pills nav-stacked">
					<li><a href="/admin/user/">ユーザー管理</a></li>
					<li class="active"><a href="#">グループ管理</a></li>
				</ul>
			</div>
			<div class="col-sm-9">

				<ol class="breadcrumb">
					<li><a href="/admin/group/">グループ管理</a></li>
					<li class="active">{$group->getName()|escape}</li>
				</ol>

				<div class="page-header">
					<h2>所属ユーザーの設定</h2>
				</div>

				<!-- ko if: message -->
				<div class="alert alert-info" data-bind="text: message"></div>
				<!-- /ko -->

				<form class="form" role="form" data-bind="submit: save">
					<div class="form-group">
						<div class="row">
							<div class="col-xs-5">
								<select class="form-control" data-bind="options: leftUserOptions, optionsText: 'name',
								selectedOptions: leftSelection" multiple="true"></select>
							</div>
							<div class="col-xs-2" style="margin-top: 120px; text-align: center;">
								<button class="btn btn-primary" data-bind="click: add, enable: enableAdd">追加 &gt;</button>
								<button class="btn btn-primary" data-bind="click: remove, enable: enableRemove">&lt; 削除</button>
							</div>
							<div class="col-xs-5">
								<select class="form-control" data-bind="options: rightUserOptions, optionsText: 'name',
								selectedOptions: rightSelection" multiple="true"></select>
							</div>
						</div>
					</div>
					<div class="form-group">
						<button type="submit" class="btn btn-primary">更新</button>
						<a class="btn btn-default" href="/admin/group/">キャンセル</a>
					</div>
				</form>
			</div>
		</div><!-- /row -->
	</div><!-- /container -->
</div><!-- /content -->

<div id="footer">
	{include file="include/footer.tpl"}
</div>

{include file="include/js_bottom.tpl"}
<script type="text/javascript" src="/js/view/group_users.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	viewModel = new ViewModel({$group->getId()|escape});
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
