<!DOCTYPE html>
<html lang="ja">
<head>
{include file="include/head.tpl" title="ユーザー一覧"}
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="setting"}
</div>

<div id="content">
	<div class="container bs">
		<div class="row">
			<div class="col-sm-3">
				<ul class="nav nav-pills nav-stacked">
					<li class="active"><a href="#">ユーザー管理</a></li>
					<li><a href="/admin/group/">グループ管理</a></li>
				</ul>
			</div>
			<div class="col-sm-9">

				<nav class="page-nav">
					<form class="form-inline" role="search">
						<div class="form-group">
							<input type="text" class="form-control" placeholder="キーワード" data-bind="value: searchQuery">
						</div>
						<button type="submit" class="btn btn-default" data-bind="click: $root.search" >検索</button>
						<button type="button" class="btn btn-default" data-bind="click: $root.clearSearch" >クリア</button>
					</form>
				</nav>

				<nav class="page-nav">
					<a class="btn btn-default" href="/admin/user/create" data-bind="{ css : { disabled: {if $userCount >= $userLimit}true{else}false{/if} } }">新規登録</a>
					<a class="btn btn-default" href="/admin/user/import">CSV登録</a>
				</nav>

				<nav class="list-nav">
					<p class="pull-right">
						ユーザー数上限:{$userLimit|escape}
					</p>
					<span data-bind="text: totalRowCount"></span> 件中 <span data-bind="text: offsetStart"></span>〜<span data-bind="text: offsetEnd"></span>件
				</nav>

				<div class="panel panel-default">
					<table class="table table-striped">
						<thead>
							<tr>
								<th></th>
								<th>表示名</th>
								<th>ログインid</th>
								<th>パスワード</th>
								<th>所属等</th>
								<th></th>
							</tr>
						</thead>
						<tbody data-bind=" foreach: $root.rows">
							<tr>
								<td><a href="#" data-bind="click: $root.openEdit.bind($root, $index())">変更</a></td>
								<td><span data-bind="text: $data.screenName "></span></td>
								<td><span data-bind="text: $data.loginId "></span></td>
								<td><span data-bind="text: $data.password "></span></td>
								<td><span data-bind="text: $data.profile "></span></td>
								<td>
									<!-- ko if: $data.role != 'admin' -->
									<a href="#" data-bind="click: $root.openDelete.bind($root, $index())">削除</a>
									<!-- /ko -->
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<ul class="pager">
					<li data-bind=" css: { disabled: !$root.hasPrev() } "><a href="#" data-bind=" click: prevList">前へ</a></li>
					<li data-bind=" css: { disabled: !$root.hasNext() } "><a href="#" data-bind=" click: nextList ">次へ</a></li>
				</ul>
			</div>
		</div><!-- /row -->
	</div><!-- /container -->
</div><!-- /content -->
<div id="footer">
	{include file="include/footer.tpl"}
</div>

{include file="include/js_bottom.tpl"}
<script type="text/javascript" src="/js/view/base/list_model.js?{get_ver_query()|escape}"></script>
<script type="text/javascript" src="/js/view/user_index.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	var option = {
		searchQuery: '{$searchQuery|default:''}',
		offset: {$limit[0]|default:0},
		limit: {$limit[1]|default:20}
	}
	viewModel = new ViewModel(option);
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
