<!DOCTYPE html>
<html lang="ja">
<head>
{if $userId == null}
{include file="include/head.tpl" title="ユーザーの登録"}
{else}
{include file="include/head.tpl" title="ユーザー情報の変更"}
{/if}
<link rel="stylesheet" type="text/css" href="/css/page/user_modify.css?{get_ver_query()|escape}">
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="setting"}
</div>

<div id="content">
	<div class="container bs">
		<div class="row">
			<div class="col-sm-3">
				<ul class="nav nav-pills nav-stacked">
					{if $user->isAdmin()}
					<li class="active"><a href="/admin/user/">ユーザー管理</a></li>
					<li><a href="/admin/group/">グループ管理</a></li>
					{else}
					<li class="active"><a href="/admin/user/">ユーザー情報</a></li>
					{/if}
				</ul>
			</div>
			<div class="col-sm-9">
				<div class="panel panel-default">
					{if $userId == null}
					<div class="panel-heading">ユーザーの登録</div>
					{else}
					<div class="panel-heading">ユーザー情報の変更</div>
					{/if}
					<div class="panel-body user-block">
						<div class="alert alert-warning" data-bind="visible: errors().length > 0, foreach: errors">
							<span data-bind="text: message"></span><br>
						</div>
						<form class="form-horizontal" role="form" data-bind="submit: save">
							<div class="form-group required">
								<label for="screenName" class="col-sm-3 control-label">表示名</label>
								<div class="col-sm-6">
									<input name="screenName" type="text" class="form-control" id="screenName" data-bind="value: screenName">
								</div>
							</div>

							<div class="form-group required">
								<label for="loginId" class="col-sm-3 control-label">ログインid</label>
								<div class="col-sm-6">
									<input name="loginId" type="text" class="form-control" id="loginId" data-bind="value: loginId, valueUpdate: 'keyup'">
								</div>
							</div>

							<div class="form-group required">
								<label for="password" class="col-sm-3 control-label">パスワード</label>
								<div class="col-sm-6">
									<input name="password" type="text" class="form-control" id="password" data-bind="value: password">
								</div>
							</div>

							<div class="form-group">
								<label for="profile" class="col-sm-3 control-label">所属等</label>
								<div class="col-sm-6">
									<input name="profile" type="text" class="form-control" id="profile" data-bind="value: profile">
								</div>
							</div>

							{if $user->isAdmin()}
							<div class="form-group field-seq">
								<label for="seq" class="col-sm-3 control-label">表示順</label>
								<div class="col-sm-6">
									<input name="seq" type="text" class="form-control" id="seq" data-bind="value: seq">
									<p class="help-block">カレンダー内の表示順のみ対応</p>
								</div>
							</div>
							{/if}

							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-6">
									{if $userId == null}
									<button type="submit" class="btn btn-primary">登録</button>
									{else}
									<button type="submit" class="btn btn-primary">更新</button>
									{/if}
									{if $user->isAdmin()}
									<a class="btn btn-default" href="#" data-bind="click: cancel">キャンセル</a>
									{/if}
								</div>
							</div>
						</form>
					</div><!-- panel-body -->
				</div><!-- /panel -->
			</div>
		</div><!-- /row -->
	</div><!-- /container -->
</div><!-- /content -->

<div id="footer">
	{include file="include/footer.tpl"}
</div>

{include file="include/js_bottom.tpl"}
<script type="text/javascript" src="/js/view/user_modify.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	viewModel = new ViewModel({$userId|default:'null'});
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
