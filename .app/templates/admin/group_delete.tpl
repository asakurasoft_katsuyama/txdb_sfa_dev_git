<!DOCTYPE html>
<html lang="ja">
<head>
{include file="include/head.tpl" title="グループの削除"}
{include "include/css_head__extension.tpl"}
</head>
<body>

<div id="header">
	{include file="include/header.tpl" active="setting"}
</div>

<div id="content">
	<div class="container">
		<div class="row">
			<div class="col-sm-3">
				<ul class="nav nav-pills nav-stacked">
					<li><a href="/admin/user/">ユーザー管理</a></li>
					<li class="active"><a href="/admin/group/">グループ管理</a></li>
				</ul>
			</div>
			<div class="col-sm-9">
				<div class="panel panel-default">
					<div class="panel-heading">グループ削除の確認</div>
					<div class="panel-body">
						<p class="alert alert-warning">以下のグループを削除しようとしています。よろしいですか？<br></p>
						<form class="form" role="form" data-bind="submit: save">
							<div class="form-group">
								<label class="control-label">グループ名</label>
								<p class="form-control-static">{$group->getName()|escape}</p>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-primary">削除</button>
								<a class="btn btn-default" href="/admin/group/">キャンセル</a>
							</div>
						</form>
					</div><!-- /panel-body -->
				</div><!-- /panel -->
			</div>
		</div><!-- /row -->
	</div><!-- /container -->
</div><!-- /content -->

<div id="footer">
	{include file="include/footer.tpl"}
</div>

{include file="include/js_bottom.tpl"}
<script type="text/javascript" src="/js/view/group_delete.js?{get_ver_query()|escape}"></script>
<script type="text/javascript">
$(function() {
	viewModel = new ViewModel({$group->getId()|escape});
	ko.applyBindings(viewModel);
});
</script>
</body>
</html>
