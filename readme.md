## Getting started

1. MySQLデータベースとユーザーを作成する

2. 初期化用のSQLを流す

	以下のSQLを流す:
	
	* .app/install/schema.sql
	* .app/insalll/data.sql

3. 設定ファイル（.app/config.ini）を編集する
	
	以下のような項目を設定する:
	
	* DB設定（DB名、ユーザー名、パスワード）
	* ログレベル
	* API用のホストURL
	* ユーザー数上限
	* テーブル数上限

4. ドキュメントルートにすべてのファイルを設置する

5. ルートにブラウザでアクセスする

